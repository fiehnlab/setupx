
<%@page import="org.setupx.repository.core.communication.ncbi.NCBI_Tree"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Enumeration"%>

<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>

		        <table border="2" width="100%">
		        	 <tr>
		        		<th colspan="21">
		        				<%=currentSplitPointName%>  
		        		</th>
				      <tr>
				      <% if (NCBIConnector.determineNCBI_Information(NCBIConnector.determineNCBI_Id(currentSplitPointName)).isSpecies()){
				          %>
					  <tr>
					  	<td>
		        				<%=NCBI_Tree.findExperiments(thisID) %> Experiments
					  	</td>
					  </tr>
				          <%
				      } %>
				      
<%	        
try { 
    if (key != null) splitpoints.remove(key);
} catch (Exception exception){
    exception.printStackTrace();
}

Logger.log(this, currentSplitPointName);
level++;

keys = splitpoints.keys();

while (keys.hasMoreElements()){
    key = ((NCBIEntry)keys.nextElement());
    splitpoint = (NCBIEntry)splitpoints.get(key);
	try {
	    relatedSplitpoint = NCBI_Tree.findGroup(splitpoint.getName()).getTaxIdInt();
		//Logger.log(this, level  + " me: " + thisID + " key: " + key.getTaxID() + "\t splitpoint " + splitpoint.getTaxID() + " relatedSplitpoint: " + relatedSplitpoint);

		//if (relatedSplitpoint == splitpoint.getTaxIdInt() && splitpoint.getTaxIdInt() != 1){
	    //    Logger.log(this, level  + " NOT GOOD : relatedSplitpoint == thisID " + relatedSplitpoint + " == " + thisID + " --> " + (relatedSplitpoint == thisID));
		//} else 
		if (relatedSplitpoint == thisID ){
	        Logger.log(this, level  + " relatedSplitpoint == thisID " + relatedSplitpoint + " == " + thisID + " --> " + (relatedSplitpoint == thisID));
			Logger.log(this, "storePos: " + level + ": " + key);
			tmp[level] = currentSplitPointName ;
			keyTMP[level] = keys;
			identical = (currentSplitPointName.compareTo(key.getName()) == 0);

			Logger.log(this, "currentSplitPointName: " + currentSplitPointName + " : " + key.getName() + " " + identical);

			// get ready for the next level
			currentSplitPointName = key.getName(); //splitpoint.getName(); 
			 
	        if ( !identical && key.getName().compareTo("root") != 0  && key.getName().compareTo("unidentified") != 0) {
		        %>	
						<td valign="bottom">
						<span style="white-space:nowrap"> 