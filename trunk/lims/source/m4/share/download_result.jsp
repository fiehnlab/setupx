<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<%@ include file="checknonlogin.jsp"%>
<%@page import="org.setupx.repository.core.communication.export.reporting.Report"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>

<%

	Report report = null;
	try {
	    report = Promt.loadReport(Long.parseLong(request.getParameter("id"))) ;
	    Logger.log(this, "loaded id via: " + report.getPromtID());
	} catch (Exception e){
	    report = (Report)session.getAttribute(org.setupx.repository.web.WebConstants.SESS_REPORT);
	    Logger.log(this, "loaded: " + report.getPromtID());
	}
	int samplesTotal = new SXQuery().findSampleIDsByPromtID(report.getPromtID()).size(); %>

%>

<%@ include file="header_standard.jsp"%>

		<table align='center' border='0' cellpadding='4' width="60%">
				<tr><th colspan="21">download actual data created by the instrument.
				<tr><td colspan="21"><font class="small">Each of the downloads contains one file per sample for that specific file-type unless you have chooses the Archive which contains all files available for each sample in this experiment. The selected files are compressed and delivered as a zip-file. <br>Alternatively you can also check out an individual sample and download the files for that specific file.  
				<tr><td width="10"><a href="download.jsp?id=<%=report.getPromtID()%>&type=txt"><img  border='0' src="pics/doc-txt.png"></a><td align="center" style="background-color: white">TXT<td>Tab delimited files comprising the result files of the ChromaTOF software for all detected peaks including metadata.<br><font class="small">The average size for a file like this is 2.3MB. ( all samples ~<%=2.3*samplesTotal %>MB uncompressed total)</font>
				<tr><td><a href="download.jsp?id=<%=report.getPromtID()%>&type=peg"><img  border='0' src="pics/doc-unknown.png"></a><td align="center"  style="background-color: white">PEG<td>GCTOF mass spectrometry raw data files without any data processing - Leco-Format<br><font class="small">The average size for a file like this is 15MB. ( all samples ~<%=15*samplesTotal %>MB uncompressed total)</font>
				<tr><td><a href="download.jsp?id=<%=report.getPromtID()%>&type=smp"><img  border='0' src="pics/doc-unknown.png"></a><td align="center"  style="background-color: white">SMP<td>Processed GCTOF data files including metadata - Leco-Format<br><font class="small">The average size for a file like this is 20MB. ( all samples ~<%=20*samplesTotal %>MB uncompressed total)</font>
				<tr><td><a href="download.jsp?id=<%=report.getPromtID()%>&type=cdf"><img  border='0' src="pics/doc-unknown.png"></a><td  align="center"  style="background-color: white">netCDF <nobr><font class="small">(ANDI MS)</font></nobr><td>				
					GCTOF mass spectrometry raw data files without any data processing. Defined by the American Society for Testing and Materials (ASTM) for the exchange of chromatographic rawdata. 
					<br><font class="small">The average size for a file like this is 60MB. ( all samples ~<%=60*samplesTotal %>MB uncompressed total)</font>
					<br>Thermo info:<a href="http://www.thermo.com/com/cda/resources/resources_detail/1,2166,112316,00.html" target="new"><img  border='0' src="pics/info2.gif"></a>
					ASTM info:<a href="http://www.astm.org/cgi-bin/SoftCart.exe/DIGITAL_LIBRARY/STP/PAGES/STP13234S.htm?L+mystore+diox4467" target="new"><img  border='0' src="pics/info2.gif"></a>

					
				<tr><td><a href="download.jsp?id=<%=report.getPromtID()%>&type=txt"><img  border='0' src="pics/package-windows-zip.png"></a><td align="center" style="background-color: white">Archive<td><font>Archive containing all files available for each sample in this experiment. <b>Attention: </b> This file contains all data above and thereby be large.</font>
			</table>			

<%@ include file="footer.jsp"%>
			