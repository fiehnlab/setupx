<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
</head>
<body>


<%@include file="incl_xls_header.jsp"%>


<%
	Vector rows = (Vector)session.getAttribute("data"); 
    Vector labels = (Vector)session.getAttribute("labels"); 
    Hashtable relations = (Hashtable) session.getAttribute("relations"); 
%>


<table align='center' border='0' cellpadding='4' width="60%" border = 1 >
<tr>	
	<th colspan="21">Assign data to experiment / samples.

<tr>	
	<td colspan="21">
				The following table shows how the information will be merged with the existing data for each sample. 
				The information has <b>not</b> been added to added to the sample yet. So feel free to go back to any of the 
				previous steps and do anykind of modification.

  <tr>
    <th><b>sample ID</b></th>
    <th>class ID</th>
	<% for(int i = 0; i < labels.size(); i++){
	    String label = "" + labels.get(i);
	    %>
	    <th><%=label %></th>
	    <%
	} %>
  </tr>

  <%
  Enumeration keys = relations.keys();
  while (keys.hasMoreElements()){
      Object key = keys.nextElement();
	  
      try {
    	  Vector row = (Vector) rows.get(Integer.parseInt("" + relations.get(key)));
    	  Logger.log(this, key + " : " + relations.get(key));
    	  
          int clazz = new SXQuery().findClazzIDBySampleID(Integer.parseInt(""+key));%>
          <tr>
            <td><%=key%><font class="small"><br>&nbsp;&nbsp;<%=new SXQuery().findSampleLabelBySampleID(Long.parseLong(key.toString())) %> <a href="xls_remove.jsp?assignment=<%=key %>">X</a> </td>
			
            <td align="center" style="background-color:<%=Util.getColor(clazz)%>"> <%=clazz%> </td>
            <%for (int i = 0; i < row.size(); i++){ %>
    	        <td><%=row.get(i) %></td>
            <%} %>
          </tr>
          <%
      } catch (Exception e){
          
      }
      }
  %>

</table>

<%@include file="incl_xls_footer.jsp"%>

</body>
</html>