<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Hashtable"%>
<%
 // create a new queryparamter 
 
	Logger.log(this, "starting");
 
 // load the existing paramter as the hashset from the session
 	Hashtable parameters = null;


	try {
	    parameters = (Hashtable)session.getAttribute("msi_parameter");
		Logger.log(this, "number of current parameters in the hashset before adding is: " + parameters.size());
	    
	}catch (Exception e){
	    parameters = new Hashtable();
		Logger.log(this, "got new parameters");
	}

			
	// add a paramter 
	String msi = request.getParameter("msi");
	String value = request.getParameter("value");

	Logger.log(this, "adding parameter: " + msi + " -  " + value);
	
	parameters.put(msi, value);
	
	// store it in the session again
	session.setAttribute("msi_parameter", parameters);
 
	response.sendRedirect("msi_query.jsp");

%>
