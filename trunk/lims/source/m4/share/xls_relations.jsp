<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Vector"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>


<table border="1">
<%

Hashtable relations = (Hashtable) session.getAttribute("relations");
Vector rows = (Vector)session.getAttribute("data");

Session hqlSession = CoreObject.createSession();

Enumeration enumeration = relations.keys();
while(enumeration.hasMoreElements()){
    String sampleID = enumeration.nextElement().toString();
    
    String queryAttributes = "select value from formobject where parent = " + sampleID + " and value != \"\" ";
    
    List attributes = hqlSession.createSQLQuery(queryAttributes).addScalar("value",Hibernate.STRING).list();

    %>
    
    <tr>
    <td>suggestion:
    <%
    // create a suggestion
    Iterator i1 = attributes.iterator();
    while(i1.hasNext()){
        String i1_value = i1.next().toString();
        
        i1_value = Util.replace(i1_value, '\n', "");
        i1_value = Util.replace(i1_value, '\r', "");
        
		char aa = ' ';
		try {
		    aa = i1_value.charAt(i1_value.length() -1 ); 
		} catch (Exception e){
		    
		}
        int character = (int) aa;
	    Logger.debug(this, "char:  \"" + aa + "\" :"  + character);
        if (aa == '\n' || aa == '\r' || character == 48 ) {
		    Logger.debug(this, "cutting: \"" + i1_value + "\"");
		    i1_value = i1_value.substring(0, i1_value.length());
		    Logger.debug(this, "result: \"" + i1_value + "\"");
		}
        int i1_value_int = -1;
        try {
            i1_value_int = Integer.parseInt(i1_value);
        } catch (Exception e){
//            e.printStackTrace();
        }
        %>
        <%        
		String row_value, value1, value2;
        
        // loop over all existing values of each row
        for (int i = 0; i < rows.size(); i++){
            //Logger.log(this, "receiving Vector from rows at pos: " + i + "/" + rows.size() );
            Vector row = (Vector)rows.get(i);
            boolean keepLooping = true;
            
            for (int j = 0; j < row.size() && keepLooping; j++){
                //Logger.log(this, "receiving Value from row at pos: " + j + "/" + row.size());
                row_value = row.get(j).toString();

                row_value = Util.replace(row_value, '\n', "");
                row_value = Util.replace(row_value, '\r', "");

                aa = ' ';
                try {
                    aa = row_value.charAt(row_value.length()-1); 
                } catch (Exception e){
                    
                }
                character = (int) aa;
                if (aa == '\n' || aa == '\r' || character == 48 ) {
        		    //Logger.debug(this, "cutting " + row_value);
        		    row_value = row_value.substring(0, row_value.length());
        		}

                int row_value_int = 0; 
                try {
                    row_value_int = Integer.parseInt(row_value.substring(0, row_value.indexOf('.')));
                } catch (Exception e){
                    
                }
                value1 = Util.replace(row_value.toLowerCase().toString(),' ',"");
                value2 = Util.replace(i1_value.toLowerCase().toString(),' ',"");
                
				/*
                if (value1.length() == value2.length()) {
                    Logger.log(this, "\"" + value1 + "\" (" + j + ": " + row_value_int + ", " + row_value +") vs. " + i + ": \""  + value2 + "\"" );
                }*/
                
                if (
                        (value1.compareTo(value2) == 0) 
                     || (
                             (row_value_int != 0) 
                     			&& 
                     		 (row_value_int == i1_value_int)
                     	)
                    ){
	                    Logger.log(this, "adding " + sampleID + " pos " + i);
	                    relations.put(sampleID, i + "");
	                    keepLooping = false;
	                }
                }
            }
        }

    //Logger.log(this, "done");
    
    
    

    
    
    %>
    	<td>sxid: <b><%=sampleID %></b>
    	<td>attributes: <%=attributes.toString() %>



    <%
}


session.setAttribute("relations", relations);
%>





</body>
</html>


<meta http-equiv="refresh" content="3; URL=xlsimport6.jsp">