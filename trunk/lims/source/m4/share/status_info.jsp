<%@ page contentType="text/html" %>
<%@ page import="org.setupx.repository.web.WebConstants"%>
<%@ page import="org.setupx.repository.web.QueryServlet"%>
<%@ page import="org.setupx.repository.web.forms.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.multi.*"%>
<%@ page import="org.setupx.repository.web.forms.clazzes.*"%>
<%@ page import="org.setupx.repository.web.forms.page.*"%>
<%@ page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@ page import="org.setupx.repository.core.communication.exporting.*"%>
<%@ page import="org.setupx.repository.core.communication.ncbi.local.*"%>
<%@ page import="org.setupx.repository.core.communication.ncbi.*"%>
<%@ page import="org.setupx.repository.core.util.logging.Logger.*"%>

<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%>: NCBI Cached Entries </h2><br/>
			List of species cached in the System. 
			<font size="-2"></font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">

		<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<%@ include file="checklogin.jsp"%>
<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>

	<table align="center" width="60%" cellpadding="4">
	<tr>
		<td colspan="15"></td>
	</tr>
	
	
		<tr>
    		<th align="center"></td>
    		<th align="center" colspan="14">NCBI Lookup
		</tr>
		<tr>
			<td align="center">
			<td colspan="21" align="center"><font class="small">To lookup items in the Taxonomy enter the name of the species or the synonym. </font></td>	
		
		<tr>
			<td ></td>
			<td align="center" colspan="21"><table>
				<%
				String lookup = "";
				try {
					org.setupx.repository.core.util.logging.Logger.debug(this, "start looking up " );
				    lookup = request.getParameter("lookup");
					if (lookup.compareTo("") != 0){
						int _nID = 0;
						NCBIEntry _nElement = null;

						_nID= org.setupx.repository.core.communication.ncbi.NCBIConnector.determineNCBI_Id(lookup);
					    _nElement= org.setupx.repository.core.communication.ncbi.NCBIConnector.determineNCBI_Information(_nID);

					    lookup =_nElement.getName();

					    if (_nElement.getTaxIdInt() == 32644){
					        throw new Exception("species is unknown.");
					    }

					%>

					<tr	>
							<form action="abbreviation.jsp">
						<td colspan="14">	
							<strong>NCBI Classification:</strong> <i><strong><%=_nElement.getName() %></strong> (<%=_nElement.getRank() %>)</i>       <a href="<%=NBCISpeciesInputfield.createLinkExternNCBI(_nElement.getTaxID())%>" target=_new><%=NBCISpeciesInputfield.ncbiIMG %></a>

						<% if (_nElement.isSpecies()){ %>							
						<td><input type="text" align="middle" name="abbreviation">
							<input type="hidden" value="<%=_nElement.getTaxID() %>" name="ncbiid">
						<td><input type="submit"  value="create Abbreviation for <%=_nElement.getName()%> "></td>
						<%} %>	
							</form>						

						</td>
					</tr>
					<% 
					}
				} catch (Exception e){
				    e.printStackTrace();
				}
				 %>

				
				<tr	>
						<form>
					<td colspan="14">Type in the species name or NCBI id that you are looking for.</td>
					<td><input type="text" name="lookup" value="<%=lookup%>" > </td>
					<td><input type="submit" value="lookup"> </td>
						</form>
				</tr>
				</table>
			</td>
		</tr>

	
	
    	<tr>
    		<th align="center"></td>
    		<th align="center" colspan="14">NCBI <strong>second level </strong>cache 
		</tr>
		<tr>
			<td align="center">
			<td colspan="21"><font class="small">The second level cache contains a large number of species. </font>
		
		
		<%  
		
		java.util.Hashtable entries = org.setupx.repository.core.communication.ncbi.NCBIConnector.getCache_entries();
		java.util.Hashtable ids = org.setupx.repository.core.communication.ncbi.NCBIConnector.getCache_ids();
		%>
		<tr>
			<td ></td>
			<td ></td>
			<td ><strong>NCBI second level</strong><br> cache currentrly contains </td>
			<td colspan="14">
				<b><%=LocalDatabaseInstance.names_real.size() %></b> real names
				<br><b><%=LocalDatabaseInstance.names_common.size() %></b> common names
				<br><b><%=LocalDatabaseInstance.names_misspelling.size() %></b> misspellings
				<br><b><%=LocalDatabaseInstance.names_synonym.size() %></b> synonyms
				<br><b><%=LocalDatabaseInstance.names_equi.size() %></b> equivalents
		</tr>
		




		<tr>
    		<th align="center"></td>
    		<th align="center" colspan="14">NCBI <strong>first level </strong>cache 
		</tr>
		
		<tr>
			<td ></td>
			<td colspan="14">NCBI first level cache contains right now <b><%=entries.size() %></b> entries and <b><%=ids.size() %></b> related IDs.</td>

		</tr>
		
		<!--  entries  -->
		<%
		    Class c = NCBIAbbreviationMapping.class;
			org.hibernate.Session s = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

			java.util.Enumeration sortedEnum = entries.keys();
			
			/*
			java.util.List keyList = java.util.Collections.list(keys);
			java.util.Collections.sort(keyList);
			java.util.Enumeration sortedEnum = java.util.Collections.enumeration(keyList);
			*/
			
		 	int col = 1;
			while (sortedEnum.hasMoreElements()){
			    String key = (String)sortedEnum	.nextElement();
			    NCBIEntry entry = (NCBIEntry)entries.get(key);
			    col = col * -1;
			    String bgcolor = "style=\"background-color:#FFFFFF\"";
			    if (col > 0) bgcolor = "";
			    if ((entry.isSpecies() && entry.getSpeciesType() == NCBIEntry.UNKNOWN)) bgcolor = "style=\"background-color:#FF545A\"";
			    if (
				 (entry.getName().toLowerCase().indexOf("plasmid") < 0)
			     && (entry.getName().toLowerCase().indexOf("plasposon") < 0) 
			     && (entry.getName().toLowerCase().indexOf("vector") < 0) 
			    // filter non species out
			     && (entry.getRank().compareTo("species") == 0)
			    ){
			        // meaning that it is not a vector
			        			    %>
			<tr>
				<td><a id="<%=entry.getTaxID()%>"></a></td>
				<td align="center"><%if (entry.isSpecies()) { %>			<img src='pics/check.gif' border="0" valign="middle" align="center"><%} else {%> <img src='pics/circle.gif' border="0" valign="middle" align="center"> <%} %></td>
				
				<td <%=bgcolor %> >
					<a href="species_detail.jsp?ncbiID=<%=entry.getTaxID() %>"><%=entry.getName() %></a></td>
				
				<td <%=bgcolor %> ><%=entry.getRank() %></td>
				
				<td <%=bgcolor %> align="center">
					<% if (entry.isSpecies() && entry.getSpeciesType() == NCBIEntry.UNKNOWN){ %> 	
						<img src='pics/warning.gif' border="0" valign="middle" align="center">
					<%} else if (entry.isSpecies()) { %>
						<b><%=entry.getSpeciesTypeName() %></b>
					<%} %>
				</td>
				
				<td <%=bgcolor %> ><%=entry.getTaxID() %></td>
				
				<%if (entry.isSpecies()) { %>
					<td <%=bgcolor %> align="center"><%try { %><%=entry.getChilds().size() %><% } catch (Exception e){ } %></td>
					<td <%=bgcolor %> align="center"><a href="http://images.google.com/images?q=<%=entry.getName()%>" target=_new><%=NBCISpeciesInputfield.cameraIMG %></a></td>
					<td <%=bgcolor %> align="center"><a href="<%=NBCISpeciesInputfield.createLinkExternNCBI(entry.getTaxID())%>" target=_new><%=NBCISpeciesInputfield.ncbiIMG %></a></td>
      				<form action="abbreviation.jsp">
      				
      				<td <%=bgcolor %> width="3">
      					<input type="text" align="middle" name="abbreviation">
      					<input type="hidden" value="<%=entry.getTaxID() %>" name="ncbiid">
      				</td>	
      				<td <%=bgcolor %> width="3" align="center"><input type="submit"  value="create"></td>
      				</form>	
      				<td <%=bgcolor %> > <a href="abbreviation.jsp">List</a></td>	
      				<td></td>	
      			<%} else {%>
      				<td <%=bgcolor %> ></td>	
      				<td <%=bgcolor %> ></td>	
      				<td <%=bgcolor %> ></td>	
      				<td <%=bgcolor %> ></td>	
      				<td <%=bgcolor %> ></td>	
      				<td <%=bgcolor %> ></td>	
      				<td></td>	
      			<%} %>
				
			</tr>		
			
			<% 
			
		    java.util.List list = s.createCriteria(c).add(org.hibernate.criterion.Expression.like("ncbiID",new Integer(entry.getTaxIdInt()))).list();

			java.util.Iterator iter = list.iterator();
			while(iter.hasNext()){
			    NCBIAbbreviationMapping mapping = (NCBIAbbreviationMapping) iter.next();
				%>
				<tr>
      				<td ></td>	
      				<td <%=bgcolor %> ></td>	
      				<td <%=bgcolor %> ></td>	
      				<td <%=bgcolor %> ></td>	
      				<td <%=bgcolor %> colspan="4">
						<strong>abbreviation</strong>
      				<td <%=bgcolor %> ></td>	
      				<td <%=bgcolor %> ></td>	
      				<td <%=bgcolor %> ><i><%=mapping.getAbbreviation() %></i>
      				<td <%=bgcolor %> > <a href="abbreviation.jsp">List</a></td>	
      				<td ></td>	
				</tr>
				<%		
			}
			}
			}
		%>
	</table>
	




<%@ include file="../footer.jsp"%>
