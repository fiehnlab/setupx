<%@page import="java.util.List"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Hashtable"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.core.query.QueryMaster"%>
<%@page import="javax.management.Query"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.Iterator"%>

<%@page import="java.util.Date"%>	
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ include file="incl_searchtypes.jsp"%>
<%@ include file="checklogin.jsp"%>


<meta http-equiv="refresh" content="15; URL=query_cache.jsp">



<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Queryies</h2>
			Data on the current status of the queries.<br/>
			<font size="-2"></font>
		</th>

		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>

		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>


<table align="center">
  <tr>
	<td rowspan="8201" valign="top"><%@ include file="navi_tree.jsp"%>		
    <th>Cached Query Results  <a href="query_cache_reset.jsp">RESET</a></th>
    <th>Submitted Queries</th>
    <th>Most recent queries</th>
  </tr>
  <tr >
    <td valign="top">
    
    <!-------------------------------------->
    <table border="1">
 	<tr>	
		<th>querytype
		<th>term
		<th>results
	<tr>
	<%	
	for (int i = 0; i < QueryMaster.cache.typeResults.length && i < 100; i++) {
	    Hashtable searchResults = QueryMaster.cache.typeResults[i];
	    
	
	    try {
	        if (searchResults == null || searchResults.size() == 0) throw new Exception("empty");
	        
	        Enumeration keys = searchResults.keys();
	        %>
	        <tr>
	        	<td style="background-color:white;" align="center" valign="top" rowspan="<%=searchResults.size() + 1%>">
	        		<%try { 
	        		    searchTypes[i].toString();
	        			%>
		        		<img  border='0' src="pics/<%=searchTypesImg[i]%>">
		        		<br>
		        		<%=searchTypes[i]%>
		        	<%}catch (Exception e){%>
		        		<%=i%>
		        	<%} 
				while (keys.hasMoreElements()) {
	            String key = (String) keys.nextElement();
	            int numberOfResults = ((List)searchResults.get(key)).size();
	            
	            %>
	            <tr>
	            	<td align="center"><a href="advancedquery_dev.jsp?term=<%=key %>&type=<%=i%>"><%=key %></a>
					<td align="center"><%if(numberOfResults != 0){ %><%=numberOfResults%><%} %>
					<%
	        }
	    } catch (Exception e) {
	    }
	}
	%>
	</table>
    <!-------------------------------------->
    
    



    </td>
    <td valign="top">
    <!-------------------------------------->
	<%
	String qMAX = "select count(*) as max from query";
	int maxX = Integer.parseInt(CoreObject.createSession().createSQLQuery(qMAX).addScalar("max", Hibernate.STRING).list().get(0).toString());
	
	String qX = 	"select searchTerm as term, count(uoid) as count from query group by searchTerm order by count(uoid) desc limit 100";
	List lX = CoreObject.createSession().createSQLQuery(qX).addScalar("term", Hibernate.STRING).addScalar("count", Hibernate.STRING).list(); 
	Iterator iX = lX.iterator();
	%>
	<table border="1">
		<tr>
			<th width="40">term
			<th>hits
			<th>percentage
		<%while (iX.hasNext()){
			Object[] o = (Object[])iX.next();
			if (o[0] != null){
				%>
				<tr>
					<td><a href="advancedquery_dev.jsp?term=<%=o[0].toString()%>"><%=o[0].toString()%>
					<td><%=o[1].toString()%>
					<td align="center">
						<% 
							int percent1 = 100 * Integer.parseInt(o[1].toString()) / maxX ;
							int percent2 = 100 - percent1;
						%>
						<table>
							<td align="center" style="background-color:#32CD32" width="<%=((percent1 * 3) + 1)%>">
							<td style="background-color:white" width="<%=percent2 * 3 - (3*80)%> " align="center" > <strong><%=percent1 %>%</strong></td>
							</td>
						</table>
							
<%			}
		} %>
	</table>
    <!-------------------------------------->
	</td>




    </td>
    <td valign="top">
    <!-------------------------------------->
	<%
	String accessUserQuery = "select a.date as date , a.querytype as querytype, a.searchTerm , (select count(b.searchTerm) from query as b where b.searchTerm = a.searchterm) as count, (select u.displayName from user as u where u.uoid = a.userID ) as username from query as a order by date desc LIMIT 30";
	List accessUser = CoreObject.createSession().createSQLQuery(accessUserQuery)
		.addScalar("date", Hibernate.LONG)
		.addScalar("querytype", Hibernate.LONG)
		.addScalar("searchterm", Hibernate.STRING)
		.addScalar("count", Hibernate.LONG)
		.addScalar("username", Hibernate.STRING)
		.list(); 
	Iterator accessIter = accessUser.iterator();
	%>
	<table border="1">
		<tr>
			<th>Username
			<th>request submitted
			<th>search term
			<th>type
			<th>queryed
		<%while (accessIter.hasNext()){
			Object[] o = (Object[])accessIter.next();
			%>
			<tr>
				<td align="center"><b><%=o[4]%>
				<td><%=new Date(((Long)o[0]).longValue()).toLocaleString()%>
				<td align="center"><a href="advancedquery_dev.jsp?term=<%=o[2]%>&type=<%=o[1]%>"><%=o[2]%></a>
				<td align="center">
					<%try { 
	        			%>
		        		<%=searchTypes[(Integer.parseInt("" + o[1]))].toString()%>
		        	<%}catch (Exception e){%>
		        		<%=o[1]%>
		        	<%} %>
				<td align="center"><%=o[3]%>
		<%} %>
	</table>
    <!-------------------------------------->
	</td>
  </tr>
</table>




