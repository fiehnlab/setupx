<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.web.pub.data.PubCompound"%>
<%@page import="org.setupx.repository.web.pub.data.PubAttribute"%>
<%@page import="org.setupx.repository.core.query.QueryMaster"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.web.pub.data.PubSample"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="org.setupx.repository.web.pub.data.PubDataSet"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<%@include file="incl_xls_header.jsp"%>




<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<th colspan="21">Select the row containing labels.
	<tr>	
		<td colspan="20">
			Please select the row containing the name of the compound. When the a compound is recognized in that line it will be highlighted in green. If the
			One of the columns has to be labeled <code>setupx</code> (not case sensitive) and has to contain the IDs for the samples. Based on this all compounds 
			recognized will be marked green. If the intensity is to low it will be marked yellow.
			<br>
			If there are compound in the list that you want to add to the syste, please press the icon below the name of the compound. It will then be added to the list of 
			known compounds.

	<tr>	
		<td colspan="20">
			<b>Please provide a label for the dataset </b> and save it done.
			<form target="">
			<br>
			<input width="200"  type="text" name="datasetLabels" value="enter label">
			<input width="200"  type="text" disabled="disabled" name="date" value="<%=new Date().toLocaleString() %>">
			<input type="submit" src="pics/go.gif" > 
			<input type="hidden" name="save" value="true">
			<br>
			</form>
	
</table>




<%
			boolean saveData = false;
			String datasetlabel = "unlabeled";
			
			try{
			    saveData = (request.getParameter("save").compareTo("true") == 0 );
			    datasetlabel = request.getParameter("datasetLabels");
			    if (datasetlabel.compareTo("null") == 0) datasetlabel = "-no label-";
			} catch (Exception e){
			    
			}
			
			
			PubDataSet pubDataSet = new PubDataSet(datasetlabel, 0L, user.getUOID(), new Date());
			HashSet  pubSamples = new HashSet();

            Vector rows = new Vector();
            rows = (Vector) session.getAttribute("data");
            Logger.debug(this, "got rows");

            int posSetupXID = -1;
            try {
                // trying to find it in the session
                posSetupXID = Integer.parseInt("" + session.getAttribute("posSetupXID"));            
            }catch (Exception E){
                
            }
            try {
                // trying to find it in the session
                int tmp = Integer.parseInt(request.getParameter("sxid"));
                if (tmp > 0){
                    posSetupXID = tmp;
                }
                session.setAttribute("posSetupXID", "" + tmp);
            }catch (Exception E){
                
            }

            
            Hashtable posKnowncompounds = new Hashtable();
            
            Session hqlsession= CoreObject.createSession();

            Vector labels = new Vector();
            // labels by arrow from another row

            try {
                int labelsPosition = Integer.parseInt(""
                        + request.getParameter("labels"));
                labels = (Vector) rows.get(labelsPosition);
                rows.remove(labelsPosition);
            } catch (Exception xe) {
                try {
                    labels = (Vector) session.getAttribute("labels");
                    if (labels == null)
                        throw new Exception("no labels");
                } catch (Exception e) {
                    labels = new Vector();
                }
            }
            session.setAttribute("labels", labels);
            Logger.debug(this, "got labels");
            
            
%>


<table align='center' border='0' cellpadding='4' width="60%" border = 1 >
<tr>

		

<%
		            // find the max length of the vectors
		            int maxRow = 0;
		            for (int i = 0; i < rows.size(); i++) {
		                try {
		                    if (((Vector) rows.get(i)).size() > maxRow)
		                        maxRow = ((Vector) rows.get(i)).size();
		                } catch (Exception e) {
		                    // some dont have that many elements in a row
		                }
		            }

		            if (labels.size() == 0) {
		                labels.setSize(maxRow);
		            }
		%>
        <tr>
	        <td>
		        <a href="pubxlsimportFlip.jsp"><img src='pics/transpose.gif' border="0" valign="middle" align="center" title="transpose the array"></a>
    	    </td>
		<%
		            Logger.debug(this, "looping over labels " + labels.size());
		            for (int i = 0; i < labels.size(); i++) {
		                String label = "" + labels.get(i);
		                boolean compoundKnown = false;
		                if (label.compareTo("null") == 0)
		                    label = "";
		                if (label.toLowerCase().indexOf("setupx") > -1) {
		                    // this is gonna be the ID
		                    posSetupXID = i;
		                    session.setAttribute("posSetupXID", "" + posSetupXID);
		                } else {
		                    try {
		                        // checking if the label is a known compound
		                        String query_count = "select count(*) as c from pubattribute where label = \"" + label + "\"";
					            int tmpCount = Integer.parseInt("" + hqlsession.createSQLQuery(query_count).addScalar("c", Hibernate.INTEGER).list().get(0).toString());
		                        
		                        if (tmpCount > 0) {
		                            compoundKnown = true;
		                            posKnowncompounds.put("" + i, label);
		                        }
		                    } catch (Exception e) {

		                    }
		                }		
		%>
	        	<%
	        	if (posSetupXID == i) {
	        	%>
		        <th align="center" style="background-color: yellow;">
		        	<font style="background-color: yellow;">
			        	<%=label%>
		        	</font>
		   	    </th>
	        	<%
	        	} else if (compoundKnown) {
	        	%>
		        <th align="center" style="background-color: green;">
		        	<font>
			        	<%=label%>
		        	</font>
		   	    </th>
	        	<%
	        	} else {
	        	%>
		        <td align="center" valign="bottom">
		        	<%=label%>
		        	<!-- create a compound -->
		        	<table width=100% >
		        	<tr>
		        	<td  align="right">
			        	<a href="createComound.jsp?label=<%=label %>">
									<img src='pics/aq_generate.gif' border="0" valign="middle" align="center" title="add this ot the list of known compounds">
						</a>
		        	<td  align="left">
			        	<a href="pubxlsimport3.jsp?sxid=<%=i%>">	
									<img src='pics/editcomment.gif' border="0" valign="middle" align="center" title="mark this as the setupX sampleID.">
						</a>
					</table>
		   	    </th>
	        	<%
	        	}
	        	%>		        	
		<%
		}
		%>
    	</tr>
        <%
                    Iterator rowIterator = rows.iterator();
                    int rowCounter = -1;
                	while (rowIterator.hasNext() && (rowCounter < 25 || saveData)){
                        rowCounter++;
                        Vector row = (Vector) rowIterator.next();
        %>
		<tr>
            <th>
            	<table>
            		<tr>
            			<th>
            				<a href="?labels=<%=rowCounter%>">
								<img src='pics/title.gif' border="0" valign="middle" align="center">
							</a>
            			</th>
            			<th><%=(rowCounter + 1)%></th>
            		</tr>
            	</table>	
            </th>
	        </td>
		<%
        boolean knownSample = false;
		PubSample pubSample = null;
		Set pubAttributes = new HashSet();
		for (int i = 0; i < row.size(); i++) {
        Object v = row.get(i);
        
		%>
		<%
		                        try {
			                        if (i == posSetupXID) {
			                            // just checking if exists
			                            
			                             v = v.toString().substring(0,v.toString().lastIndexOf('.'));
			                            
			                             pubDataSet.setRelatedExperimentID(new SXQuery().findPromtIDbySample(Integer.parseInt(v.toString())));
			                             knownSample = true;
			                             pubSample = new PubSample(Integer.parseInt(v.toString()),"",null);
										 %> 
										        <th align="center" style="background-color: green;">
										 		<%=v%>
										 <%
	                             	} else {
	                                    if (knownSample && posKnowncompounds.containsKey("" + i) ) {
	                                        double valueJ = Double.parseDouble(v.toString());
	                                        if (valueJ > 0) {
	                                            
											String label = posKnowncompounds.get("" + i).toString();
											PubAttribute attribute = new PubAttribute(label, (long)valueJ);
											if (saveData) attribute.update(true);
											pubAttributes.add(attribute);
											
											
												if (rowCounter < 25) {
												    // ignore output when more then 25
											        %>
												<td align="center" style="background-color: white">
														<img src='pics/check.gif' border="0" valign="middle" align="center" title="<%=v%>">
											        <%
												}
	                                    	} else {
												if (rowCounter < 25) {
												    // ignore output when more then 25
											        %><td align="center"><img src='pics/circle.gif' border="0" valign="middle" align="center"/><%=v%> <%
												}
	                                    	}
	                                    } else {
											if (rowCounter < 25) {
											    // ignore output when more then 25
										        %><td align="center"><font class="small" style="color: gray;"> <%=v%></font><%
										        }
	                                    }
	                             	}
			                     } catch (Exception e) {
			                         e.printStackTrace();
										if (rowCounter < 25) {
										 %> 
												 	<td align="center" style="background-color: red">
												 		<font><%=v%></font> 
		
										 <%
										}
								 }
								 %>
		</td>
		<%
		}
		
		if (pubSample != null){
			pubSample.setPubAttributes(pubAttributes);
			if (saveData) pubSample.update(true);
			pubSamples.add(pubSample);
		}
		%>
	       	</td>
		</tr>
		<%
		} 
                	
    	if (rowCounter >= 25) {%>
    	<tr>
    		<td colspan="10"> 
    		There are additional lines - only the first 25 are shown.
    		</td>
   		</tr>
    	<%} 


        pubDataSet.setPubSamples(pubSamples);
		if (saveData) pubDataSet.update(true);
		// row iterator
		%>
</table>

<%if (saveData) { %>
    <meta http-equiv="refresh" content="0; URL=pubdata_detail.jsp?id=<%=pubDataSet.getRelatedExperimentID() %>">
<%} %>

<%@include file="incl_pubxls_footer.jsp"%>

</body>
</html>