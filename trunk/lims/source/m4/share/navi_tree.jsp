
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.util.Link"%>
<%@page import="java.util.Vector"%>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%
Vector linksPersonal = new Vector();
linksPersonal.add(new Link("new Experiment", "load?id=0"));
linksPersonal.add(new Link("my Experiments","main.jsp"));
linksPersonal.add(new Link("public data", "main_public.jsp"));


Vector linksLabTech = new Vector();
linksLabTech.add(new Link("administration","admin.jsp"));
linksLabTech.add(new Link("lab status","adminstatus.jsp"));
linksLabTech.add(new Link("timeline","timeline.jsp"));
linksLabTech.add(new Link("tracking","sample_status_create.jsp", "pics/barcode.jpg"));
linksLabTech.add(new Link("acquistion","aq/query.jsp"));

Vector linksPub = new Vector();
linksPub.add(new Link("publish data", "pubxlsimport0.jsp"));
// linksPub.add(new Link("public experiemnt", "pubexperiment.jsp?id=338050"));
// linksPub.add(new Link("public sample", "pubsample.jsp?uoid=1642"));
linksPub.add(new Link("public data", "main_public.jsp"));


Vector linksQuery = new Vector();
linksQuery.add(new Link("Query Frontend", "q_main_ajax.jsp", "pics/details.gif"));
linksQuery.add(new Link("Query Stats","query_cache.jsp","pics/stats.gif"));
// linksQuery.add(new Link("Query Test", "querytest.jsp"));
linksQuery.add(new Link("Access Statistic","access2.jsp"));




if (user.isMasterUser(user.getUOID())) linksLabTech.add(new Link("users","user.jsp","pics/icon_forum.gif"));
if (user.isMasterUser(user.getUOID())) linksLabTech.add(new Link("load nonedit mode", "load_nonedit.jsp", "pics/warning.gif"));
linksLabTech.add(new Link("export by regrouping", "export_C_promtselect.jsp"));

Vector linksAdmin = new Vector();

linksAdmin.add(new Link("machine methods", "machine_method_assign.jsp"));
linksAdmin.add(new Link("machine setup", "machine.jsp"));
linksAdmin.add(new Link("messages", "messages.jsp?type=1"));
linksAdmin.add(new Link("taxonomy abbreviation", "abbreviation.jsp"));
linksAdmin.add(new Link("taxonomy ", "status_info.jsp"));
linksAdmin.add(new Link("ontology test","obo_test.jsp?term=seed&ontology=po_anatomy.obo"));
linksAdmin.add(new Link("assign samples", "assign_sample.jsp"));
linksAdmin.add(new Link("assign sample", "assign_samples.jsp"));
linksAdmin.add(new Link("data curation","curator.jsp"));
linksAdmin.add(new Link("custom metadata","metadata_list.jsp"));

        
        
Vector links = new Vector();
links.add(new Link("main","main.jsp","pics/icon_home.gif"));
links.add(linksQuery);
links.add(linksPub);
links.add(linksPersonal);
if (user.isLabTechnician()) links.add(linksLabTech);
if (user.isMasterUser(user.getUOID()))links.add(linksAdmin);

links.add(new Link("exit","login.jsp"));




%>
<table border=0 cellpadding="0" cellspacing="0" style="background-color: white">
<%

Iterator iterator = links.iterator();
while (iterator.hasNext()){
    Object object = iterator.next();
   
    if (object instanceof Link){
        Link link = (Link)object;
        %>
    		<tr>
    			<td style="background-color: white">
    				<% if(iterator.hasNext()){%>
    					<img  border='0' src="pics/JSTree-intersec.gif">
    				<%}else{ %> 
    					<img  border='0' src="pics/JSTree-last.gif">
    				<%} %>
    			<td style="background-color: white" colspan="21">
    				<%@ include file="incl_link.jsp"%>

        <%
    } else {
        Vector subLinks = (Vector)object;
        Iterator subIterator = subLinks.iterator();
        %>
		<tr>
			<td style="background-color: white">
				<% if(iterator.hasNext()){%>
					<img  border='0' src="pics/JSTree-plus.gif">
				<%} else { %>
					<img  border='0' src="pics/JSTree-plusl.gif">
				<%} %>
			<td style="background-color: white">
					<img  border='0' src="pics/JSTree_first.gif">
        <%
        while(subIterator.hasNext()){
            Link link = (Link)subIterator.next();
            %>
    		<tr>
    			<td style="background-color: white">
    				<% if(iterator.hasNext()){%>
    					<img  border='0' src="pics/JSTree-vertic.gif">
    				<%} %>
    			<td style="background-color: white">
    				<% if(subIterator.hasNext()){%>
    					<img  border='0' src="pics/JSTree-intersec.gif">
    				<%}else{ %> 
    					<img  border='0' src="pics/JSTree-last.gif">
    				<%} %>
    			<td style="background-color: white"><%@ include file="incl_link.jsp"%>
    			
            <%
        }
    }
}
%>

</table>


			
			
			