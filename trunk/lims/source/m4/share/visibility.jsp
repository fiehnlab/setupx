<%@ page import="org.setupx.repository.core.document.*"%>
<!-- document requires the varialbles from the file result.jsp !!! -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page language="java" %>

<form name="sample" method="post" action="popupcalsample.html">

	<h2 class="tableheader1" id="visibility" align="center">Notificaton - Visibility</h2>
	<table width="80%" align="center">
	<tr>
		<td>
	You can define wheter you you want be notified. 
	The following table showes the different steps. By choosing the different ckeckboxes in the first column, you can select when you want to be notified by an email. 
	The email will be send when document (<%=doc.getTypeTagName()%>) has reached the next level.
	For example, when you want to be notified when your <%=doc.getTypeTagName()%> has been processed, just check the box in the row 
	<%=DocumentExperiment.getStatusDescriptionStatic(DocumentExperiment.STATUS_PROCESSED)%>. 

	<p>As well as you can define when you want to be notified, you can define when your document can be looked after by others. 
	You are always able to see your own document. But by selecting the different checkboxes, you
	can make your own document visible to other.</p> 
	<p>	There is one aditional step in the rows for defining the vivibility. The row scheduled. This helps you to make your experiments, ... visible to others from this defined time.</p>
	
	
	You can define wheter you you want be notified. The following table showes the different steps. By choosing the different ckeckboxes in the first column, you can select when you want to be notified by an email. The email will be send when document (<%=doc.getTypeTagName()%>) has reached the next level.
	For example, when you want to be notified when your <%=doc.getTypeTagName()%> has been processed, just check the box in the row <%=DocumentExperiment.getStatusDescriptionStatic(DocumentExperiment.STATUS_PROCESSED)%>. 	</p>

		
		</td>
	</tr>
	</table>
	<table width="80%" border="1" align="center">
		<th>step</th>
		<th width="20%">Notification</th>
		<th width="20%">visible (protected)</th>
		<th width="20%">visible (global)</th>
		<tr>
			<td><%=DocumentExperiment.getStatusDescriptionStatic(DocumentExperiment.STATUS_PRIVATE)%>&nbsp<a href="info.jsp?infoID=72" target="info">info</a></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td><%=DocumentExperiment.getStatusDescriptionStatic(DocumentExperiment.STATUS_WAITING_PREPERATION)%></td>
			<td><input type="checkbox" checked name=""/>&nbsp<a href="info.jsp?infoID=61" target="info">info</a></td>
			<td><input type="checkbox" name=""/>&nbsp<a href="info.jsp?infoID=62" target="info">info</a></td>
			<td><input type="checkbox" name=""/>&nbsp<a href="info.jsp?infoID=63" target="info">info</a></td>
		</tr>
		<tr>
			<td><%=DocumentExperiment.getStatusDescriptionStatic(DocumentExperiment.STATUS_WAITING_MEASUREMENT_OPERATOR)%></td>
			<td><input type="checkbox" checked name=""/>&nbsp<a href="info.jsp?infoID=64" target="info">info</a></td>
			<td><input type="checkbox" name=""/>&nbsp<a href="info.jsp?infoID=65" target="info">info</a></td>
			<td><input type="checkbox" name=""/>&nbsp<a href="info.jsp?infoID=66" target="info">info</a></td>
		</tr>
		<tr>
			<td><%=DocumentExperiment.getStatusDescriptionStatic(DocumentExperiment.STATUS_WAITING_MEASUREMENT_RUN)%></td>
			<td><input type="checkbox" checked name=""/>&nbsp<a href="info.jsp?infoID=67" target="info">info</a></td>
			<td><input type="checkbox" name=""/>&nbsp<a href="info.jsp?infoID=68" target="info">info</a></td>
			<td><input type="checkbox" name=""/>&nbsp<a href="info.jsp?infoID=69" target="info">info</a></td>
		</tr>
		<tr>
			<td><%=DocumentExperiment.getStatusDescriptionStatic(DocumentExperiment.STATUS_PROCESSED)%></td>
			<td><input type="checkbox" checked name=""/>&nbsp<a href="info.jsp?infoID=70" target="info">info</a></td>
			<td><input type="checkbox" checked name=""/>&nbsp<a href="info.jsp?infoID=71" target="info">info</a></td>
			<td><input type="checkbox" name=""/>&nbsp<a href="info.jsp?infoID=72" target="info">info</a></td>
		</tr>
		<tr>
			<td>scheduled</td>
			<td></td>
			<td><div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
	<input type="checkbox" name=""/>&nbsp<input type="text" name="T1" size="10">  <a href="javascript:show_calendar('sample.T1');" onMouseOver="window.status='Date Picker'; overlib('Click here to choose a date from a calendar.'); return true;" onMouseOut="window.status=''; nd(); return true;"><img src="pics\show-calendar.gif" width=24 height=22 border=0></a>

</td>
			<td>
	<input type="checkbox" name=""/>&nbsp<input type="text" name="T2" size="10">  <a href="javascript:show_calendar('sample.T2');" onMouseOver="window.status='Date Picker'; overlib('Click here to choose a date from a calendar.'); return true;" onMouseOut="window.status=''; nd(); return true;"><img src="pics\show-calendar.gif" width=24 height=22 border=0></a>
			</td>
		</tr>
	
	</table>
	
	
</form>
	
