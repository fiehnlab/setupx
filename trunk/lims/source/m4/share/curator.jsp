<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.List"%>

<%@ include file="checklogin.jsp"%>
<%@ include file="header_standard.jsp"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<%@page import="org.hibernate.Session"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<table align="center" width="60%">	
	<tr>	
		<th rowspan="321">&nbsp;&nbsp;
		<th>Question
		<th>Number of different values
    	<th>curate Values
    	<th>curate Question <font class="small"><br>only "custom fields" can be reassigned</font>
    	<th>MSI Fields
		<th rowspan="321">&nbsp;
    	
		
<%
String query = "select question, count(distinct(value)) as number, helptext, description, msiattribute from formobject where question != \"\" and value != \"\" and value != \"false\" and value != \"true\" group by msiattribute, question, description order by question";

List questions = CoreObject.createSession()
		.createSQLQuery(query)
		.addScalar("question", Hibernate.STRING)
		.addScalar("number", Hibernate.STRING)
		.addScalar("helptext", Hibernate.STRING)
		.addScalar("description", Hibernate.STRING)
		.addScalar("msiattribute", Hibernate.STRING)
		.list();

// get all values to it
for (int i = 0; i < questions.size(); i++){
    Object[] objects = (Object[])questions.get(i);
    String question = objects[0].toString();
    String number = objects[1].toString();
    String descr = objects[3].toString();
    String msiattribute = objects[4] + "";
    
    %>
    	<tr>
    		<td><%=question %>
    		<font class="small"><br>&nbsp;&nbsp;&nbsp;<%=descr %></font>
    		<td align="center"><%=number %>
    		<td style="background-color: white;" align="center"><a href="curator_values.jsp?question=<%=question%>"><img  border='0' src="pics/aq_generate.gif"></a>
    		
    		
    		<%
   			// if it is a custom field
   			if (descr.compareTo("custom field") == 0){
    		%>
    			<td style="background-color: white;" align="center"><a href="curator_question.jsp?question=<%=question%>"><img  border='0' src="pics/aq_generate.gif"></a>
    		<%} else { %>
	    		<td style="background-color: white;" align="center">
    		<%} %>
    		
    		<%
    		// if it is a msi field
    		%>
    		<td align="center" style="background-color: white">
    		<%
    			try {
    			    Session s = CoreObject.createSession();
    			    if (msiattribute.compareTo("null") == 0) throw new Exception();
    			    long msiID = Long.parseLong(msiattribute);
    			    MSIAttribute attribute = (MSIAttribute)CoreObject.persistence_loadByID(MSIAttribute.class,s,msiID);
    			    %><a href="msi_detail_attribute.jsp?id=<%=attribute.getUOID()%>"><%=attribute.getMsiLabel()%><%
    			}catch (Exception e){
    			    e.printStackTrace();
    			    %><a href="msi_assign_fields.jsp?question=<%=question%>"><img src="pics/link.gif" border="0"><%
    			}
    		
    		%>
    <%}%>
</table>
<%@ include file="footer.jsp"%>
