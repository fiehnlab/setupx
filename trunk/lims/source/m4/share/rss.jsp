<%@ include file="checknonlogin.jsp"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<%@ include file="header_standard.jsp"%>

<br>
<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td rowspan="21">
		<th width="3" style="background-color: white"><img border="0" src="pics/rss.gif"></th>
		<th><h3>RSS Feeds</h3><font class="small">RSS Feeds currently available on <%=org.setupx.repository.Config.SYSTEM_NAME%>.</font></th>
		<td rowspan="21">
	<tr>
		<td colspan="2">
			RSS is a family of Web feed formats used to publish frequently updated content such as blog entries, news headlines or podcasts. 
			<font class="small"><br>An RSS document, which is called a "feed," "web feed," or "channel," contains either a summary of content 
			from an associated web site or the full text. RSS makes it possible for people to keep up with their favorite web sites in an 
			automated manner that's easier than checking them manually.<br>
			RSS content can be read using software called an "RSS reader," "feed reader" or an "aggregator." The user subscribes to a feed 
			by entering the feed's link into the reader or by clicking an RSS icon in a browser that initiates the subscription process. 
			The reader checks the user's subscribed feeds regularly for new content, downloading any updates that it finds.<br><br></font>
	<tr>
		<th width="3" style="background-color: white"><a href=""><img border="0" src="pics/rss3.gif"></a>
		<th>List of MSI Attributes
	<tr>
		<td colspan="2">
			The List contains the current number of MSI Attributes. In order to be able to exchange data in between labs in a machine readable 
			format the attributes (that were defined by the relevant committees) were stored. The list of Attributes is dynamic and is updated 
			whenever an attribute is added to the set of standardized attributes. <a href="msi_attributes_rss.jsp"><img border="0" src="pics/move_right.gif"></a>
	<tr>
		<th width="3" style="background-color: white"><a href=""><img border="0" src="pics/rss3.gif"></a>
		<th>Publicly available study designs in <%=org.setupx.repository.Config.SYSTEM_NAME%>
	<tr>
		<td colspan="2">
			A subset of the experiments is publicly available - including the whole experimental design and the annotated GC-TOF result data. 
			The RSS Feed supports a live tracking of what is publicly available. <a href="main_rss.jsp"><img border="0" src="pics/move_right.gif"></a>
	<tr>
		<th width="3" style="background-color: white">
			<a href="http://www.dmoz.org/Computers/Software/Internet/Clients/WWW/Feed_Readers/"><img border="0" src="pics/move_right.gif"></a>
		<td colspan="2">List of RSS Feed Readers
	<tr>
		<th colspan="2">&nbsp;
</table>
<br>
<%@ include file="footer.jsp"%>
 