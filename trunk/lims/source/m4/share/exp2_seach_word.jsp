<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>



<%


	String searchword = request.getParameter("search");

	int promtID = Integer.parseInt(request.getParameter("id"));
	
	List clazzIDs = new SXQuery().findClazzIDsByPromtID(promtID);
	
	Iterator iterator = clazzIDs.iterator();
	
	// nr for the group 
	int groupNr = 0;
	
	while (iterator.hasNext()){
	    
	    int clazzID = Integer.parseInt("" + iterator.next());
	    
	    String description = new SXQuery().findClassInformationString(clazzID);
	    
	    if (description.indexOf(searchword) > -1){
	        
	        // add it to the selection.
	        Logger.log(this, "found it in: " + description);
	        
	        // marking this class as group
	        List samples = new SXQuery().findSampleIDsByClazzID(clazzID);
	        Iterator i = samples.iterator();
	        while (i.hasNext()){
	            
	            try {
		            int sampleID = Integer.parseInt("" + i.next());
		            
		            String acqName = "" + new SXQuery().findScannedPair(sampleID).get(0);
		            
		            request.getSession().setAttribute("" + acqName, "" + groupNr);

		            Logger.log(this, " assigning" + acqName + "  " + groupNr);
	            } catch (Exception e){
	                e.printStackTrace();
	            }
	        }
	        
	        // increase the counter by 1
	        groupNr = groupNr + 1;
            Logger.log(this, " group nr:  " + groupNr);
	    }
	}
%>


<meta http-equiv="refresh" content="1; URL=exp2_regroup.jsp?id=<%=promtID%>">