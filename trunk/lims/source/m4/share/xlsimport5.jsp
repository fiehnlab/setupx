	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Vector"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.util.Util"%>

<%@page import="java.util.Hashtable"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

</head>
<body>

<%@include file="incl_xls_header.jsp"%>

<%
            long promtID = 0L;
            try {
                String idString = request.getParameter("id");
                idString = Util.replace(idString, " ", "");
                promtID = Long.parseLong(idString);
                session.setAttribute("promtID", "" + promtID);
            } catch (Exception e) {
                try {
                    promtID  = Integer.parseInt("" + session.getAttribute("promtID"));
                } catch (Exception e2){
                    promtID = 0L;
                }
            }
%>

<form>
<table align='center' border='0' cellpadding='4' width="60%" border = 1 >
<tr>	
	<th colspan="21">Assign data to experiment / samples.

<tr>	
	<td colspan="21">
				What is the experiment ID that your samples are part of?

<tr>
	<td colspan="21">
			<select name="id" onChange="document.forms[0].submit()">
				<option>
					-- select your experiment --
				</option>
		<%
		 Iterator iteratorAccess = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID()).iterator();
		 while (iteratorAccess.hasNext()){
		     PromtUserAccessRight access = (PromtUserAccessRight)iteratorAccess.next();
		     	if (access.getAccessCode() >= PromtUserAccessRight.WRITE){
				%>
				<option value="<%=access.getExperimentID()%>"
					<% if(access.getExperimentID() == promtID){%> selected <%} %>
				>
					[<%=access.getExperimentID() %>]&nbsp;<%=new SXQuery().findPromtTitleByPromtID(access.getExperimentID())%>  
				</option>
				<%  
		     	}
		 }
		 %>
		</select>
	</td>


<%
if (promtID != 0) {
	int pos = 0;
	try {
	    pos = Integer.parseInt("" + request.getParameter("labelPos"));
	} catch (Exception e) {
	    // nope
	}
	
	// list of all the different informations from the xls file
	Vector rows = (Vector) session.getAttribute("data");
	Vector labels = (Vector) session.getAttribute("labels");
	
	// wich label are we showing
%>
<tr>	
	<td colspan="21">
				<p>The table below shows all the samples of the experiment that you selected and all the data 
				that you uploaded. The goal is to match the data to each of the samples.
				<p><b>Each column</b> represents one of the datasets and <b>each row</b> represents one sample. 
				<p>If the matrix shows a checked box it means that you want to assign combine these two datasets.
				
		
				In the row below you can see values that you uploaded from your xls sheet. 
				Only one of the attributes is show - but feel free to select your prefered attribute 
				in order to be able to assign it to the samples.
<tr>	
	<td colspan="21">
				If your data is not sorted you can try out the <b>auto assign function</b> that will sort the data
				for you and matches the existing data with yours.
				<p>
				<a href="xls_relations.jsp">Start the autoassign process.</a>

<tr>	
	<td colspan="21">
				&nbsp;&nbsp;&nbsp;
<tr>	
	    <td>
	    <td colspan="2">
	    <select name="labelPos" onChange="document.forms[0].submit()">
	    	<%
	    	for (int i = 0; i < labels.size(); i++) {
	    	%>
			<option value="<%=i%>"
				<% if(i == pos){%> selected <%} %>
			>
				<%=labels.get(i)%>
			</option>
			<%
			}
			%>
		</select>
	    </td>
	    <%
		for (int i = 0; i < rows.size(); i++) {
		     // each row
		     try {%>
			    <td align="center">
			    	<%=((Vector) rows.get(i)).get(pos)%>
			    </td>
		    <%} catch (Exception e) {%>
			    <td>
			    	---
			    </td>
		    <%
			}
		}
		
		//  in case there is a real id start looking for the samples
		List sampleIDs = new SXQuery().findSampleIDsByPromtID(promtID);
		Iterator sampleIDIterator = sampleIDs.iterator();
		long tmpClassID = 0;

	     // check if we have relations
	     Hashtable relations = null;
	     try {
	         relations = (Hashtable) session.getAttribute("relations");
	         
	         if (relations == null || relations.size() == 0){
	             throw new Exception("no relations in session");
	         }
	     } catch (Exception e) {
	         relations = new Hashtable();
	         for (int i = 0; i < sampleIDs.size(); i++) {
	             relations.put(sampleIDs.get(i) + "", i + "");
                 Logger.debug(this, "creating new Relation: " + sampleIDs.get(i) + "  " + i );
	         }
	         session.setAttribute("relations", relations);
	     }

		// loop over all relations and kick out the ones that are illegal
		Enumeration sampleIDsEnumeration = relations.keys();
		while (sampleIDsEnumeration.hasMoreElements()){
		    int _sID = Integer.parseInt("" + sampleIDsEnumeration.nextElement());
		    boolean ok = false;
		    for (int as = 0; !ok && as < sampleIDs.size() ; as++){
			    Logger.debug(this, "checking: " + _sID + " vs.  " + sampleIDs.get(as));
			    if (sampleIDs.get(as).toString().compareTo("" + _sID) == 0){
			        ok = true;
			 	}
		    }
		    if (!ok){
			    Logger.debug(this, "removing relation for sample:" + _sID );
		        relations.remove("" + _sID);
		    }
		}
	     
        Logger.debug(this, "number of relations before update of the relations:  " + relations.size());
        
        
        
        
        
         // update the relations
              List clazzIDs = new SXQuery().findClazzIDsByPromtID(promtID);
              for (int i = 0; i < clazzIDs.size(); i++) {
                  try {
                      Logger.debug(this, "------------------------------------------------");
                      Logger.debug(this, "clazzIDs.get(i) " + clazzIDs.get(i));
                      long moveClassID = Long.parseLong("" + clazzIDs.get(i));
                      Logger.debug(this, "moveClassID " + moveClassID);
                      int moveMotion = Integer.parseInt("" + request.getParameter("" + moveClassID));
                      Logger.debug(this, "moveMotion " + moveMotion);

                      // get the sample IDs and move them to each direction
                      List sampleId4Clazz = new SXQuery().findSampleIDsByClazzID(((int) moveClassID));

                      for (int s = 0; s < sampleId4Clazz.size(); s++) {
                          if (moveMotion != 0) {
                              int oldPos = Integer.parseInt("" + relations.get("" + sampleId4Clazz.get(s)));
                              relations.put("" + sampleId4Clazz.get(s), (oldPos + moveMotion) + "");
                              Logger.log(this, "moving " + relations.get(sampleId4Clazz.get(s) + "" + moveMotion));
                          }

                          // if flip selected (moveMotion = 0)
                          if (moveMotion == 0  && s < sampleId4Clazz.size() / 2) {
                              Logger.debug(this, " s: " + s);
                              Logger.debug(this, " size: "  + sampleId4Clazz.size());
                              int oppPos = sampleId4Clazz.size() - s - 1;
                              Logger.debug(this, " oppPos in Class: "  + oppPos);

                              long oppositSample = Long.parseLong("" + sampleId4Clazz.get(oppPos));
                              Logger.debug(this, " opposit sampleID: " + oppositSample);

                              long thisSample = Long.parseLong("" + sampleId4Clazz.get(s));
                              Logger.debug(this, " this sampleID : " + thisSample);

                              Object object1 = null;
                              Object object2 = null;

                              object1 = relations.get("" + oppositSample);
                              object2 = relations.get("" + thisSample);

                              Logger.debug(this, oppositSample + " at " + object1);
                              Logger.debug(this, thisSample + " at " + object2);

                              Logger.debug(this, thisSample + " at " + object1);
                              relations.put("" + thisSample, object1);

                              Logger.debug(this, oppositSample + " at" + object2);
                              relations.put("" + oppositSample, object2);

                              Logger.debug(this, " done ");
                          }
                      }

                  } catch (Exception e) {
                      Logger.debug(this, "" + e);
                  }
                  Logger.debug(this, "relations: " + relations.size());
                  session.setAttribute("relations", relations);
              }
              while (sampleIDIterator.hasNext()) {
                  long sampleID = Long.parseLong(sampleIDIterator.next().toString());
                  long classID = new SXQuery().findClazzIDBySampleID((int) sampleID);
                  String classColor = Util.getColor((int) classID);

                  // find assigned information
                  if (classID != tmpClassID) {
    %>
						<tr>
							<td colspan="<%= rows.size() + 4%>" align="center" style="background-color:<%=classColor%>">&nbsp;	   		   	<a name="<%=classID%>"/>	
			<% } %>
	<tr>
		<td align="center" style="background-color:<%=classColor%>">
  				&nbsp;&nbsp;&nbsp;&nbsp;
  			</td>
		<td align="center">
			<b><%=sampleID%></b>
		</td>
		<td align="center">
			<%=new SXQuery().findSampleLabelBySampleID(sampleID)%>	
		</td>
		
		<!--  the radio buttons -->
		<%
		int relPos = 0;
		try {
			relPos = Integer.parseInt("" + relations.get(sampleID + ""));
		} catch (Exception e){
	         for (int i = 0; i < sampleIDs.size(); i++) {
	             relations.put(sampleIDs.get(i) + "", i + "");
                 Logger.debug(this, "creating new Relation: " + sampleIDs.get(i) + "  " + i );
	         }
		}
		
		
		for (int i = 0; i < rows.size(); i++) {
		%>
			<td align="center" style="background-color:white">
			<%
               try {
                   if (relPos == i) {
                       %>
						<img border="0" valign="middle" align="center" background="white" src="pics/checked.jpg"> 
						<%
					} else {
						%>
							<% if (relPos >= rows.size() || relPos < 0){ %>
								<img border="0" valign="middle" align="center" background="white" src="pics/blank.jpg"> 
							<% } else { %>
								<img border="0" valign="middle" align="center" background="white" src="pics/blank_point.jpg"> 
							<% } %>
						<%
					}
			} catch (Exception e) {
					%>
					<img border="0" valign="middle" align="center" background="white" src="pics/x-amber.jpg"> 
					<%
			}
					%>
				</td>
			<%
		}
		if (relPos >= rows.size() || relPos < 0){
			%>
			<td align="center" style="background-color:white;">
				<img border="0" valign="middle" align="center" background="white" src="pics/x.gif"> 
			</td>
			<% 
		} else {
			%>
			<td align="center" style="background-color:white;">
				<img border="0" valign="middle" align="center" background="white" src="pics/check.gif"> 
			</td>
			<% 
		}

			// class specific buttons 
			if (tmpClassID != classID) {
			    tmpClassID = classID;
			    int numberOfSamples = new SXQuery().findSampleIDsByClazzID((int) classID).size();
				%>
		    <td rowspan="<%=numberOfSamples %>">
		    	<table height="100%">
		    		<tr><td>
						<a href="xlsimport5.jsp?<%=classID%>=-5&id=<%=promtID %>&<%=new Date().getTime() %>&labelPos=<%=pos%>#<%=classID%>">
			    			<img border="0" valign="middle" align="center" src="pics/move_left_o.gif"> 
		    			</a>
		    		<tr><td>
						<a href="xlsimport5.jsp?<%=classID%>=-1&id=<%=promtID %>&<%=new Date().getTime() %>&labelPos=<%=pos%>#<%=classID%>">
			    			<img border="0" valign="middle" align="center" src="pics/move_left.gif"> 
		    			</a>
		    		<tr><td>
						<a href="xlsimport5.jsp?<%=classID%>=0&id=<%=promtID %>&<%=new Date().getTime() %>&labelPos=<%=pos%>#<%=classID%>">
			    			<img border="0" valign="middle" align="center" src="pics/flip.gif"> 
		    			</a>
		    		<tr><td>
						<a href="xlsimport5.jsp?<%=classID%>=1&id=<%=promtID %>&<%=new Date().getTime() %>&labelPos=<%=pos%>#<%=classID%>">
			    			<img border="0" valign="middle" align="center" src="pics/move_right.gif"> 
		    			</a>
		    		<tr><td>
						<a href="xlsimport5.jsp?<%=classID%>=5&id=<%=promtID %>&<%=new Date().getTime() %>&labelPos=<%=pos%>#<%=classID%>">
			    			<img border="0" valign="middle" align="center" src="pics/move_right_o.gif"> 
		    			</a>
		    	</table>
			</td>		    		
		    <%
		    				    }
		    				    %>
		</tr>

		<%
		            }
		            }
		%>
</form>    



<%@include file="incl_xls_footer.jsp"%>

</body>
</html>