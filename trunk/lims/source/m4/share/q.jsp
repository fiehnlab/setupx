<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>

<%@include file="incl_searchheader.jsp"%>

<%

// DEPRECATED - has been included in the QueryMaster !! NOT MAINTAINED



// -----------------------------
//      general search jsp
// -----------------------------

int promtID = 0;
String searchTerm = request.getParameter("term");    
int selectedSearchTypeInt = 99;
try {
	selectedSearchTypeInt = (Integer.parseInt(request.getParameter("type") + ""));
} catch (Exception e){
	e.printStackTrace();	    
}

String selectedSearchTypeString = "";
if (selectedSearchTypeInt != 99){
    selectedSearchTypeString = searchTypes[selectedSearchTypeInt];
} else {
    selectedSearchTypeString = "";
}


Session s = CoreObject.createSession();

// remove starting and ending space characters
boolean loopStart = true;
boolean loopEnd = true;
try {
	while (loopStart){
	    if (searchTerm.charAt(0)==' '){
	        searchTerm = searchTerm.substring(1, searchTerm.length());
	    }else {
	        loopStart = false;
	    }
	}
	while (loopEnd){
	    
	    if (searchTerm.charAt(searchTerm.length()-1)==' '){
	        searchTerm = searchTerm.substring(0, searchTerm.length()-1);
	    }else {
	        loopEnd = false;
	    }
	}
} catch (Exception e){
    
}



%>

<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Vector"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.web.pub.data.cache.VariationsCache"%>
<%@page import="org.hibernate.criterion.Expression"%>
<%@page import="java.util.HashSet"%>
<%@page import="org.hibernate.Criteria"%>
<table align='center' border='0' cellpadding='4' width="60%">
	<th colspan="21"><h3>Please select the category that you want to search for "<%=searchTerm%>" </h3>
	<%if (selectedSearchTypeString.length() > 0) {%><font class="small">[search by: <%=selectedSearchTypeString%>]53</font><%} %>

<%
//-----------------------------
// check for users



//-----------------------------
//compounds
int queryType = 1;
if (selectedSearchTypeInt == queryType || selectedSearchTypeInt == 99){ 	
	String queryCompoundNames = "select label from pubattribute where label like \"%" + searchTerm + "%\" group by label";
	List compoudNameslist = s.createSQLQuery(queryCompoundNames).addScalar("label", Hibernate.STRING).list(); 
	
	Iterator compoudNamesiterator = compoudNameslist.iterator();
	
	// in case it is exactly one!
	if (compoudNameslist.size() == 1 && selectedSearchTypeInt == queryType){
	    %>
		<%@ include file="incl_q_forward.jsp"%>
	   	<meta http-equiv="refresh" content="0; URL=q2.jsp?type=<%=queryType%>&term=<%=compoudNamesiterator.next().toString()%>">
    	<%
	}

	// show it only if some are found
	if (compoudNamesiterator.hasNext()){
		%><%@ include file="incl_q_table_header.jsp"%><%
	}
	
	while (compoudNamesiterator.hasNext()){
		String label = compoudNamesiterator.next().toString();
	    %>
	    <tr>
	    	<td>
	    	<td><a href="q2.jsp?type=<%=queryType%>&term=<%=label%>"><%=label%></a>
	    	<td>
	    <% 
	}
}

//-----------------------------
//factors
	queryType = 2;
	if (selectedSearchTypeInt == queryType || selectedSearchTypeInt == 99){ 	
	    
	//    String basic = "select * from cacheobjects where value LIKE \"%" + searchTerm + "%\"";
	List result = null;
	try {
		Criteria c = s.createCriteria(VariationsCache.class).add(Expression.like("value", "%" + searchTerm + "%"));
		try {
		    String variation = request.getParameter("var");
		    if (variation != null && variation.length() > 0){
		        Logger.debug(this, "adding Expression.eq(\"question\", " + variation + ")");
		        c.add(Expression.eq("question", variation));
		    }
		} catch (Exception e){
		    e.printStackTrace();
		}
		result = c.list();
	}catch (Exception exception){
	    result = new Vector();
	}

	if (result.size() > 0){ 
	
		%>
		<%@ include file="incl_q_table_header.jsp"%>
		<%
	    HashSet usedExperiments = new HashSet();
		java.util.Iterator iter = result.iterator();
		while (iter.hasNext()){
		    VariationsCache variationsCache = (VariationsCache )iter.next();
	
		    try {
				long classID = variationsCache.getClassid();
			    long experienmentID = new SXQuery().findPromtIDbyClazz((int)classID);
			    if (! usedExperiments.contains(experienmentID + "")){
				    usedExperiments.add(experienmentID + "");
				    
				    String value = variationsCache.getValue();
				    String title = new SXQuery().findPromtTitleByPromtID(experienmentID);
				    String _abstract = new SXQuery().findPromtAbstractByPromtID(experienmentID);
				    //String design = new SXQuery().findPromtStructure(experienmentID);
				    String species = new SXQuery().findSpeciesbyPromtID(experienmentID).get(0).toString();
				    //String classDef = new SXQuery().findClassInformationString(classID);
				    String question = variationsCache.getQuestion();
		
				    %>
					<tr>
						<td></td>
						<td><b><a href="load?id=<%=experienmentID%>&action=<%=PromtUserAccessRight.READ%>"><%=title %></b></a>&nbsp;[<%=experienmentID%>]
						<br>&nbsp;&nbsp;&nbsp;&nbsp;<font class="small">
						<%if (_abstract.length() > 150){ %>
							<%=_abstract.substring(0, 140)%> [...]
						<%}else{ %>
							<%=_abstract%>
						<%} %></font>
						<br>&nbsp;&nbsp;&nbsp;&nbsp;<font style="font-size: x-small; font-weight: bolde;"><%=question %>:</font>
							<% 
								Logger.debug(this, "-------------");
								Logger.debug(this, "--" + value + ": " + searchTerm);
								int posSearchTerm = value.toLowerCase().indexOf(searchTerm.toLowerCase());
								Logger.debug(this, posSearchTerm +"");
								int start = 0;
								String startAdd = "";
								int end = value.length();
								String endAdd = "";
								Logger.debug(this, end +"");
								if (posSearchTerm > 20) {
								    start = posSearchTerm-10;
								    startAdd = "...";
								}
								Logger.debug(this, start +"");
								if (value.length() - posSearchTerm - searchTerm.length() > 20) {
								    end = posSearchTerm + searchTerm.length() + 10;
								    endAdd = "...";
								}
								Logger.debug(this, end +"");
							%>
							<font style="font-size: x-small;"><%=startAdd%><%=value.substring(start, posSearchTerm) %></font><font style="font-size: x-small; background-color: white;"><%=value.substring(posSearchTerm, posSearchTerm+searchTerm.length()) %></font><font style="font-size: x-small;"><%=value.substring(posSearchTerm+searchTerm.length(),end)%><%=endAdd%></font>
							<br>&nbsp;&nbsp;&nbsp;&nbsp;<font style="font-size: x-small; font-weight: bolde;">species: <%=species %></font>
						</td>
						<td></td>
					</tr>
			<%	
				}
		    } catch (Exception e4){
		        e4.printStackTrace();
		    }
		}
	} 
}
	// -----------------------------
	// title, abstract, id
	queryType = 3;
	if (selectedSearchTypeInt == queryType || selectedSearchTypeInt == 99){ 	
	%>
	<%@ include file="incl_q_table_header.jsp"%>
	<%	
	// searching by title and abstract 
	
	String basic  = "select promt.uoid from formobject as promt, " + 
	"formobject as page, " + 
	"formobject as element1 , " + 
	"formobject as element2 " + 
	"where promt.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.Promt\" " + 
	"and page.parent = promt.uoid " + 
	"and element1.parent = page.uoid " + 
	"and element2.parent = page.uoid " + 
	"and element1.question = \"Title\" " + 
	"and element2.question = \"abstract\" " + 
	"and (element1.value LIKE \"%" + searchTerm + "%\" OR element2.value LIKE \"%" + searchTerm + "%\") " +
	"order by promt.uoid DESC";
	
	List result = null;
	try {
		result = s.createSQLQuery(basic).addScalar("promt.uoid", org.hibernate.Hibernate.STRING).list();
	}catch (Exception exception){
	    result = new Vector();
	    
	}
	
	java.util.Iterator iter = result.iterator();
	while (iter.hasNext()){
	    promtID = Integer.parseInt(iter.next().toString());

	    try {
	    	String _title = new SXQuery().findPromtTitleByPromtID(promtID);
	    	String _abstract = new SXQuery().findPromtAbstractByPromtID(promtID);
	    	if (_title.compareTo("-unknown-") == 0) throw new Exception("not found");
	        %>
			<%@ include file="incl_experiment.jsp"%>
			<%
	    } catch (Exception e4){
	    }
	}
}





if (searchTerm.length() > 3){
    int sampleID = 0;
    String _title = "";
    String _abstract = "";

//     ------------------
//     find promt by ID 
//     ------------------
    try {
        promtID = new SXQuery().findPromtIDbySample(Integer.parseInt(searchTerm));
        %>
    	<tr>
    		<td>
    		<td>
    		<%
    			_title = new SXQuery().findPromtTitleByPromtID(promtID);
    			_abstract = new SXQuery().findPromtAbstractByPromtID(promtID);
    			 %>
    			 <b>Sample</b>[<a href="sample_detail.jsp?id=<%=searchTerm %>"><%=searchTerm  %></a>] as part of <b><%=_title %></b>[<a href="load?id=<%=promtID %>&action=<%=PromtUserAccessRight.READ%>"><%=promtID %></a>]<br><%=_abstract%>
    		</td>
    		<td>
    	</tr>
        <% 
    } catch (Exception e1){
        // dont do stuff
    }


    try {
    	promtID = new SXQuery().findPromtIDbyClazz(Integer.parseInt(searchTerm));
        %>
 			<tr>
				<td></td>
				<td><b><a href="load?id=<%=promtID%>&action=<%=PromtUserAccessRight.READ%>"><%=_title %></b><a></a>[<%=promtID%>]<br>
				&nbsp;&nbsp;&nbsp;&nbsp;<font class="small">
				<%if (_abstract.length() > 150){ %>
					<%=_abstract.substring(0, 140)%> [...]
				<%}else{ %>
					<%=_abstract%>
				<%} %></font>
				</td>
				<td></td>
			</tr>
   <% 
    } catch (Exception e3){
    }


    try {
        promtID = Integer.parseInt(searchTerm);
    	_title = new SXQuery().findPromtTitleByPromtID(promtID);
    	_abstract = new SXQuery().findPromtAbstractByPromtID(promtID);
    	if (_title.compareTo("-unknown-") == 0) throw new Exception("not found");
        %>
		<%@ include file="incl_experiment.jsp"%>
		<%
    } catch (Exception e4){
        e4.printStackTrace();
    }
}


// -----------------------------
// species
queryType = 0;
if (selectedSearchTypeInt == queryType || selectedSearchTypeInt == 99){ 	
	String querySpeciesNames = "select label.value as ncbilabel from formobject as ncbi, formobject as label where ncbi.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\" and label.PARENT = ncbi.uoid and label.value != \"\""
		+ " and label.internalPosition = 0 "
		+ " and label.value like \"%" + searchTerm + "%\" "
		+ " group by label.value ORDER by label.value ";
	
	List speciesNamesList = s.createSQLQuery(querySpeciesNames).addScalar("ncbilabel", org.hibernate.Hibernate.STRING).list();
	try {
	    String _speciesTMPname = NCBIConnector.determineNCBI_Information(NCBIConnector.determineNCBI_Id(searchTerm)).getName();
		if (! speciesNamesList.contains(_speciesTMPname) && _speciesTMPname.compareTo("unidentified") != 0) speciesNamesList.add(_speciesTMPname);
	} catch (Exception e){
	    
	}
	
	Iterator speciesNamesiterator = speciesNamesList.iterator();
	
	// in case it is exactly one!
	if (speciesNamesList.size() == 1 && selectedSearchTypeInt == queryType){
	    %>
		<%@ include file="incl_q_forward.jsp"%>
	    <meta http-equiv="refresh" content="1; URL=q2.jsp?type=<%=queryType%>&term=<%=speciesNamesiterator.next().toString()%>">
    	<%
	} else if (speciesNamesList.size() > 0 && speciesNamesList.get(0).toString().compareTo("unidentified") != 0){
		%>
		<%@ include file="incl_q_table_header.jsp"%>
        <% 
		while (speciesNamesiterator.hasNext()){
		    String speciesNameString = speciesNamesiterator.next().toString();
		    //if (speciesNameString.compareTo("unidentified") != 0){
		        %>
		        <tr>
		        	<td>
		        	<td><a href="q2.jsp?type=<%=queryType%>&term=<%=speciesNameString %>"><%=speciesNameString %></a>
		        	<td>
		        <% 
		    //}
		}
	}

}
	


// -----------------------------
// samples

queryType = 4;
if (selectedSearchTypeInt == queryType || selectedSearchTypeInt == 99){ 
    String url = "q2.jsp?type="  + queryType + "&term=" + searchTerm ;
    if (searchTerm.length() < 3){
        %>
		<%@ include file="incl_q_table_header.jsp"%>
		<tr>
			<td>
			<td><i>The search for specific samples requieres a searchstring that is at least 3 characters long.</i>
			<td>
		<%
    } else if (selectedSearchTypeInt == queryType){
	    %>
		<%@ include file="incl_q_forward.jsp"%>
	    <meta http-equiv="refresh" content="1; URL=<%=url%>">
    	<%
   	} else {
   	    %>
		<%@ include file="incl_q_table_header.jsp"%>
		<tr>
			<td>
			<td><a href="<%=url%>"><i>Search for all samples matching "<%=searchTerm %>" </i></a> 
			<td>
   	    <%
   	}
}


%>
</table>





<%@ include file="footer.jsp"%>