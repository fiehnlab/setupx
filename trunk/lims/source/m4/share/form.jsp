<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page language="java" %>

<%@ include file="checklogin.jsp"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.web.forms.inputfield.InputField"%>
<%@page import="org.exolab.castor.xml.schema.Form"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.multi.*"%>
<%@ page import="org.setupx.repository.web.forms.page.*"%>

<script language="JavaScript" src="calendar1.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/	>

<script language="JavaScript" src="calendar1.js"></script>

		
<% 

	// the actual experiment received from the session
	Promt promt = (Promt)session.getAttribute(WebConstants.SESS_FORM_PROMT);

	// the number of the page which is displayed now
	Integer pos = (Integer)session.getAttribute("pos");

	// in case that there is no page - the user is not logged in ...
	if (user == null) { %>
		<p align="center">
		<b>you are not logged IN</b>
		</p>
	
	<jsp:forward page="login.jsp"/>
	
	<% 
	} 

    long timeLastSaved = Long.parseLong("" + request.getSession().getAttribute("TIME"));  

    %>
    <font style="font-size: xx-small">autosave at <%=new Date(timeLastSaved).toGMTString()%></font>
    <%
	
	// the page that will be displayed
	Page pagE = (Page)promt.getField(pos.intValue());
	
	NBCISpeciesInputfield speciesInputfield = promt.findNBCISpeciesInputfield();
	speciesInputfield.updateRelations(speciesInputfield.entry);
%>


<!--  head containing logo and description -->
<table align='center' border='0' cellpadding='2' width='90%'>
<tr>
		<td align="center">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h3> 
				<%=promt.getName()%>
				<% if (promt.getUOID() != 0){ %> [<%=promt.getUOID() %>]
				<%} %>
			</h3>
			<h5>
				<%=promt.getDescription()%></h5>
			<font size="-2"><%=promt.getLongDescription()%></font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			


		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
</tr>



</table>
<!-------------------------------------------------------------------------------------- -->
<!-- table containing all labels from each page in this promt -->
<%		String tableBackgroundColor = "#1B5289"; //"#E7f3ff"; // "white";// "#6682B6" ; %>
 
<form name="default" action="form" method="POST"> 
	<input id="form_target" name="form_target" type="hidden" value="<%=pos.intValue()%>"/> 
	<input name="form_lastpage" type="hidden" value="<%=pos.intValue()%>"/> 

 
<table border="0" width="70%" align='center' cellpadding="0"  cellspacing="0" background="<%=tableBackgroundColor %>">
  <tr>
	<td style="background-color: <%=tableBackgroundColor %>" colspan="21">&nbsp;
  <tr>
	<td style="background-color: <%=tableBackgroundColor %>">&nbsp;


	<%
	FormObject[] pages = promt.getFields();
	for (int i = 0; i < pages.length; i++) {

	    // temporary page for the head row containing all pages 
	    FormObject _page = pages[i];
    	%>

	<!-- only active pages will be displayed -->
	<% if (_page.isActive()) { %>

		<!-- header for the table - including highlighting the current page-->
		<td width="<%=100/pages.length%>%" valign="top">
			<table width="100%" cellpadding="0"  cellspacing="0" height="40">
			<tr>
					<%
					// in - mid - out 
					String term = "";
					term = "mid";
					
					// is this active?
					String digit1 = "0";
					// is this + 1 active?
					String digit2 = "0";

					if (i == pos.intValue()) digit1 = "1";
				    if (i+1 == pos.intValue()) digit2 = "1";
				    else digit2 = "0";
					
					String color = "F4F0E8";
					String fontcolor = "black";
					if (pos.intValue() != i) {
					    color = "6699CC";
					    fontcolor = "black";
					}
					%>


				<!--  opening image -->
				<%if (i == 0){ %>	
				<td width="2" valign="top" style="background-color: <%=color %>" height="40">
					<img src="pics/ongAlt_in<%=digit1 %>.gif" style="background-color:<%=tableBackgroundColor %>">
				<%} %>
				

				<!--img valid or not-->
				<td width="2" style="background-color: <%=color %>" height="40">
					<nobr>&nbsp;
						<%if(_page.isValid()){%>
							<img src="pics/ok.gif"/>
						<%}else{%>
							<img src="pics/err.gif"/>
						<%}%>
					</nobr>
				</td>
				
				<!-- the label of the page inbetween the images -->
				<td valign="middle" style="background-color: <%=color %>" align="center">
					<input type="submit" value="<%=_page.getName()%>" onclick="document.getElementById('form_target').value='<%=i%>';" />
					<% /*%><font color="<%=fontcolor %>"><%=_page.getName()%></font> <%*/ %>

					<!-- 
						<%if(!_page.isValid()){%>
						<br>	
						<font style="font-size:xx-small; color: black; background-color: red;">
						<%=_page.validate()%>
						</font>
						<%}%>
					 -->

				<!-- image ending the tab -->
				<%if (digit2.compareTo("1") == 0) color = "#F4F0E8";%>
				<td width="2" valign="top" style="background-color: <%=color %>" align="right">
					<%if (i != pages.length-1){ %>	
						<img src="pics/ongAlt_<%=term %><%=digit1 %><%=digit2 %>.gif" style="background-color:<%=tableBackgroundColor %>"><br>				
					<%} else {%>
						<img src="pics/ongAlt_out<%=digit1 %>.gif" style="background-color:<%=tableBackgroundColor %>">
					<%} %>
			</table>
			</td>
	<%}%>
<%}%>
<td style="background-color: <%=tableBackgroundColor %>">&nbsp;
</tr>

</table>

<!-- end of table for each pageheader -->





<!-------------------------------------------------------------------------------------- -->
<!-- content for the specific page-->

<!-- new - like createHTML -->
<table width="90%" cellpadding='4' align='center'>
	<tr>
		<td width="120" valign="top">
		<table >
		<tr>
			<td style=background-color:#84B6E7>
				<b>					
				<%=pagE.getName()%>  
				</b>
				<br/>
				<font class="small">
				<%=pagE.getDescription()%>
				</font>
				<p>		
				<%=pagE.getLongDescription()%>
				<p>
			</td>
		</tr>
		<tr>
			<td style="background-color:#FFFFFF">
				<span class="portlet_header">Help:</span>
				<br>
				<font class="small">
					If you have any problems with filling out this form just contact the supporting staff members. We are very interested in your comments and suggestions.
				<p>
					<a href="mailto:<%=org.setupx.repository.Config.OPERATOR_ADMIN.toString()%>">
						<img align='center' src="pics/ask.jpg"/>
					</a>
				</p>
				</font>
				If you use the system the first time, you might want to have a look at the introduction.

				<B><a href="intro.jsp" target="new">Introduction</a></B>
				</p><br>
			</td>
		</tr>

		<tr>
			<td>
				<span class="portlet_header">User-Information:</span>
				<br>
				<font class="small">You are currently logged in:</font>			
				<%=user.getDisplay()%><br>
				<font class="small"><%=user.toStringHTML()%></font>
			</td>
		</tr>
		<tr>
			<td style="background-color:#FFFFFF">
				<span class="portlet_header">Logout:</span>
				If you want to log out.
				<a href="form?logout">
					<img src='pics/go_exit.gif' border="0" valign="middle" align="center">
				</a>
			</td>
		</tr>


		</table>

		</td>

		<!--- main content -->
		<td valign="top" >
			<table border="0" width="100%" cellpadding='2' valign="top">
				<%=pagE.createHTMLChilds(8)%>
			</table>
		</td>

		<!--- right content -->
		<td witdh="100" valign="top" >
			<table>

			<%if(pagE.isSummaryActive()){%>
			<tr>
				<td>
				<%=promt.createSummary().createHTMLsummaryShort()%>
				<%}%>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
			<input name="" type="submit" value="NEXT"/>
		</td>
	</tr>

	

</table>
</form>
<!-- main content end -->
</td>
</tr>
</table>

<%@ include file="footer.jsp"%>



<% Logger.debug(this, "finished creating " + this); %>