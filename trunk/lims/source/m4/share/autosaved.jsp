<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.forms.relation.Relation"%>
<%@page import="org.setupx.repository.web.forms.relation.AbstractRelation"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.SpeciesInputfield2"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<html>
<% 
Promt promt = (Promt)session.getAttribute(WebConstants.SESS_FORM_PROMT);
promt.isValid();

SpeciesInputfield2 inputfield2 = promt.findSpeciesInputfield2();

inputfield2.setValue(inputfield2.getValue());

AbstractRelation[] relations = inputfield2.getRelations();
for (int i = 0; i < relations.length; i++){
    relations[i].update( "" + inputfield2.getValue());
    Logger.log(this, inputfield2.getValue() +"");
}
%>

<meta http-equiv="refresh" content="1; URL=form">
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table align="center">
	<tr>
		<td width="300" align="center">Your experiment has been saved.
		</td>
		<td>
			<a href="form.jsp">	
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

</body>
</html>