<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Vector"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%@ include file="checklogin.jsp"%>



<br>
<%
List ids = (Vector)session.getAttribute("selectedPromts");
%>


<%
// checking if ids are beeing added
try {
    String value = request.getParameter("add");
	if (value.length() > 1){
	    Vector selectedIDs = null;
	    try {
	        selectedIDs = (Vector)session.getAttribute("selectedClasses");
	        if (selectedIDs == null) throw new NullPointerException();
	    } catch (Exception e){
	        selectedIDs = new Vector();
	    }
	    
	    if (!selectedIDs.contains(value)){
	    	selectedIDs.add(value);
	    }
	    session.setAttribute("selectedClasses",selectedIDs ); 
	} 
} catch (Exception e){
    e.printStackTrace();
}

// clear selection
try {
    String value = request.getParameter("clear");
    if (value.compareTo("true") == 0){
        session.setAttribute("selectedClasses",null);
    }
} catch (Exception e){
    e.printStackTrace();
}
%>

<%@ include file="header_standard.jsp"%>

<table align="center" width="80%" cellpadding="7">
	<tr>
		<th rowspan="301">&nbsp;&nbsp;&nbsp;
		<th colspan="21">
			Please select the classes that you want to combine in a single export. 
			<p>When you are ready click <a href="export_C_check.jsp">here</a>. 
			<p>In case you want to change your selection of experiments  click <a href="export_C_promtselect.jsp">here</a>.


<%
	Vector selectedIDs = null;
	try {
	    selectedIDs = (Vector)session.getAttribute("selectedClasses");
	    if (selectedIDs == null) throw new NullPointerException();
	} catch (Exception e){
	    selectedIDs = new Vector();
	}

	for (int i = 0 ; i < ids.size(); i++){
	    long promtID = Long.parseLong(""  + ids.get(i) );
%>
	<tr>
		<td style="background-color: white;">
			<%=promtID %>
		<td style="background-color: <%if(! new SXQuery().determinePromtFinished(promtID)){ %>red;<%}else{ %> white;<%} %>">
			<nobr><b><%=new SXQuery().findPromtTitleByPromtID(promtID) %></b></nobr>
		<%if(! new SXQuery().determinePromtFinished(promtID)){ %>
			<nobr><b>WARNING:</b> This experiment is not finished!</nobr>
		<%}
		
		List classIDs = new SXQuery().findClazzIDsByPromtID(promtID);

		%>
		<td style="background-color: white;">
			Select all <a href="export_C_selection1.jsp?add=<%=promtID %>"><%=classIDs.size() %> classes</a>.
			


		<%		
		for (int j = 0; j < classIDs.size(); j++){
		    long classID = Long.parseLong("" + classIDs.get(j));
			%>
			<td align="center" <%if (selectedIDs.contains(""+classID) ){%> style="background-color: #669933"<%} %>  >
				<a href="?add=<%=classID %>" ><%=classID%></a>
					<br>				
					<%if (selectedIDs.contains(""+classID) ){%>
						<a href="?clear=true"><img border="0" src="pics/delete-all-o.gif"></a>
					<%} else { %>
						<a href="?add=<%=classID%>"><img border="0" src="pics/c_1.gif"></a>
					<%} %>
					<br>
					<nobr><b><%=new SXQuery().findSampleIDsByClazzID((int)classID).size() %> Samples</b></nobr>
			<td align="center" <%if (selectedIDs.contains(""+classID) ){%> style="background-color: #99CC66"<%} %>  >
				<font class="small"><%=new SXQuery().findClassInformationString(classID)%></font>
			<%
		}
	}
%>
	<tr>
		<th colspan="21">&nbsp;

</body>
</html>