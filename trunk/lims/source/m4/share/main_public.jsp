<%@ include file="checknonlogin.jsp"%>

<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>



<table align="center" width="60%" cellpadding="7">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th colspan="2" >
			<h2>Public Experiments</h2>
				<br>The following experiments are shared with the public. <br>
				Feel free to download look at the experimental design and download all the releated data.
		</th>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} else { %>
<%@ include file="navi_public.jsp"%>
<%} %>


<table align="center" width="60%" cellpadding="7">
	<tr>
		<td colspan="3" align="center" >
			You can download the entire experiment. We offer a the <b>experimental design</b> in XML structure following 
			the MSI Schema, <b>result files</b> generated by the annotation software BinBase and also the <b>Instrument Data</b> 
			in various formats (ChromaTOF, PEG, SMP and netCDF). Feel free to download the data and give us feedback on the data.
			This information is also available as <a href="main_rss.jsp">RSS Feed</a>. 
		</td>
		<td>
			<a href="main_rss.jsp"> <img src="pics/rss.gif" border="0"></a>.
		</td>
	</tr>

</table>


<table align='center' border='0' cellpadding='4' width="60%">
							<tr>
								<th colspan="4"><h3>Content
								<%

								// query is used for the chart in repository_species
								String __query = "select label.value as value, count(promt.uoid) as c, sum(cache.numberofScannedSamples) as samples " +  
								" from formobject as promt " +
								" inner join formobject as page " +
								" on page.parent = promt.uoid " +
								" inner join formobject as species " +
								" on species.parent = page.uoid  " +
								" inner join formobject as ncbispecies " +
								" on ncbispecies.parent = species.uoid  " +
								" inner join formobject as label  " +
								" on label.parent = ncbispecies.uoid  " +
								" inner join cache as cache " +
								" on cache.experimentID = promt.uoid " +
								" inner join formobject as f " +
								" on f.uoid = cache.experimentID " +
								" inner join pubdata as p " + 
								" on p.relatedExperimentID = cache.experimentID " +
								" where ncbispecies.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\"   " +
								" and label.value != \"\"   " +
								" and cache.numberofScannedSamples > 0 " +
								" group by label.value " +
								" order by label.value ";

								int MINNUMBER_OF_EXPERIMENTS = 0;
								boolean IN3D = true;

								%>
									<%@ include file="repository_species.jsp"%>
									<br>
									<table align="center" width="80%" cellpadding="7">
										<tr>
											<td align="center">
												<img src="<%=url1%>" border="0">
										<tr>
											<td align="center">
												<img src="<%=url2%>" border="0">
										<tr>
											<td align="center">
												<font class="small">* charts contain species with > <%=MINNUMBER_OF_EXPERIMENTS %> experiments.</font>
										<tr>
											<td>
												<ul>
												
												
										<form action="compound_compare_species.jsp">
										<table>
										<%
										// list of the species
												
										listIter = _list.iterator();
										int radioCount = 0;
										while (listIter.hasNext()){
										    radioCount++;
										    Object[] objects = (Object[])listIter.next();
											
										    String label = objects[1] + "";
										    String value = objects[0] + ""; 
										    String nr_samples = objects[2] + "";
										    %>
										    
										    
											<tr>												
											<td>
												<a href="advancedquery_dev.jsp?term=<%=label %>&&type=0"><%=label %></a>
													<font class="small"> 
														<br>&nbsp; <%=value%> Experiments 
														<br>&nbsp; <%=nr_samples %> Samples
													</font>
											<td><input type="radio" value="<%=NCBIConnector.determineNCBI_Id(label)%>" name="g1" <%if (radioCount == 1) {%>checked="checked"<%} %> >
											<td><input type="radio" value="<%=NCBIConnector.determineNCBI_Id(label)%>" name="g2" <%if (radioCount == 2) {%>checked="checked"<%} %>>
											<td><input type="radio" value="<%=NCBIConnector.determineNCBI_Id(label)%>" name="g3" <%if (radioCount == 3) {%>checked="checked"<%} %>>
											<td width="80%">
												
									    <%
										}										
									    %>
									    <tr>
									    	<td>
									    	<td colspan="4"><input type="submit" title=""> 
									    	<br><font style="font-size: xx-small">Select three different species and submit the query to compare the detected compounds. 
									</form>		
									</table>
								</table>
</table>




<table align="center" width="80%" cellpadding="7">

<%@ include file="incl_pubExperiment.jsp"%>
	
	<tr>
		<td colspan="15"></td>
	</tr>
</table>


<%@ include file="footer.jsp"%>
