<%@page import="org.setupx.repository.core.communication.status.StatusTracker"%>
<%@page import="java.util.List"%>

<%
Logger.log(this, "start creating list: " + new Date().toGMTString() );
List[] lists = new List[]{
        StatusTracker.findShipped(),
        StatusTracker.findStored(),
		StatusTracker.findPrepared(),
		StatusTracker.findScheduled(),
		StatusTracker.findRun(),
		StatusTracker.findPostProcessing()
		};
Logger.log(this, "done creating list: " + new Date().toGMTString() );

String[] labels = new String[]{
        "shipped",
        "arrived",
        "prepared",
        "scheduled",
        "finished gc run",
        "postprocessing"
		};




int totalSize = 0;
for (int i = 0; i < lists.length; i++){
	totalSize = totalSize + lists[i].size();
}

%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>


<%@page import="java.util.Date"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<table width="100%">


<tr>
<%	
	String[] colors = new String[]{"#ff6600","#cc6600","#996600","#666600","#336600","#006600"};
	for (int i = 0; i < lists.length; i++){
		if (lists[i].size() != 0){
	   		%>		
			<td width="<%=(100*lists[i].size())/totalSize %>%" align="center" style="background-color:<%=colors[i] %>" > 
				<font class="small" color="white">
					<%=(100*lists[i].size())/totalSize %>%
				</font> 
									
			</td>
		    <%
		} else {
		    %>
			<td align="center" style="background-color:<%=colors[i] %>" >
				<font class="small" color="white">
					&nbsp;
				</font> 
			 
		    <%
		}
	}
	
	boolean IN3D = true;
	String URL_START = "http://chart.apis.google.com/chart?";
	String CHART_NAME_1 = "chtt=Progress ";
	String CHART_TYPE_PIE = "cht=p&chco=0000ff";
	if (IN3D) CHART_TYPE_PIE = "cht=p3&chco=0000ff";
	String CHART_RESOLUTION = "chs=600x300";
	if (IN3D) CHART_RESOLUTION = "chs=600x200";
	String AND = "&";
	String url_value_1 = "chd=t:";
	String url_label_1 = "chl=";

    // http://chart.apis.google.com/chart?cht=p3&chd=s:Uf9a&chs=200x100&chl=A|B|C|D

	for (int i = 0; i < lists.length; i++){
	    url_label_1 = url_label_1.concat( labels[i] ).concat(" " + (100*lists[i].size())/totalSize + "% ");
	    url_value_1 = url_value_1.concat("" + (lists[i].size() / 10));

		if (i+1 < lists.length) {
		    url_value_1 = url_value_1.concat(",");
	        url_label_1 = url_label_1.concat("|");
		}
    }

	String url1 = URL_START.concat(CHART_TYPE_PIE).concat(AND).concat(CHART_NAME_1).concat(AND).concat(url_value_1).concat(AND).concat(CHART_RESOLUTION).concat(AND).concat(url_label_1);
	
	%>
<tr>
	<td colspan="21" align="center">
	<img src="<%=url1 %>"/>

<tr>

	<td colspan="21">
		<table>
		<%
		for (int i = 0; i < lists.length; i++){
		%>	
			<tr>
				<td align="center" style="background-color:<%=colors[i] %>" >
					<% if(lists[i].size() > 0){ %>
					<font class="small" color="white">
						&nbsp;<%=(100*lists[i].size())/totalSize %>%&nbsp;
					</font>
					<%} %>
				<td>
				<font class="small"> 
					<b><%=labels[i] %></b>
				

				<td>
				<% if(lists[i].size() > 0) {%>	
					<font class="small"> 
						<a href="sample_status_overview.jsp?show=true">
						</a>
						<nobr><%=lists[i].size()%> Samples</nobr> 
						<!-- very slow:  actually due to a problem with a set of samples whichs samples are not related to the REAL experiment" <nobr><% //=filter(lists[i]).size() %> Experiment(s)</nobr>  -->
					</font>
				<%} %>
		<%} %>

		</table>
	</td>
</tr>

</table>

<%! 

		/* creates a list of promtID that the sample in the list are related to. */
      public static HashSet filter(List ___list) {
    	HashSet experimentIDs = new HashSet();
    	HashSet sampleIDs = new HashSet();
    	Iterator iterator = ___list.iterator();
    	while(iterator.hasNext()){
    	    try {
    	        Long sampleID = new Long(iterator.next() + "");
    	        if (sampleID.longValue() > 721){
	    	        if (! sampleIDs.contains(sampleID)){
	    	            Logger.debug(null, "sample " + sampleID + " not cached - experimentIDs : " + experimentIDs.size() + " sampleIDs : " + sampleIDs.size() + " " );
	            	    long promtID = findPromtIDbySample(sampleID.longValue());
	            	    sampleIDs.addAll(new SXQuery().findSampleIDsByPromtID(promtID));
	            	    if (! experimentIDs.contains("" + promtID)){
	            	        experimentIDs.add("" + promtID); 
	            	    }
	    	        } else {
	    	            // Logger.debug(null, "sample " + sampleID + " is cached");
	    	        }
    			} else {
    	            // Logger.debug(null, "sample " + sampleID + " is cached");
    	        }
    	    } catch (Exception e){
    	        // e.printStackTrace();
    	    }
    	}
    	return experimentIDs;
     }

	public static long findPromtIDbySample(long i){
	    String basic = "select root(uoid) as root from formobject where uoid = " + i;
	    List result = CoreObject.createSession().createSQLQuery(basic).addScalar("root", Hibernate.LONG).list();
        return (Long.parseLong(result.get(0) + ""));
	}


%>
