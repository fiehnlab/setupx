<%@ include file="checknonlogin.jsp"%>
<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.List,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.communication.export.reporting.*,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.core.communication.binbase.BBConnector"%>
<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.core.communication.leco.LecoACQFile"%>
<%@page import="org.setupx.repository.core.communication.exporting.AcquisitionParameter"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.communication.status.StatusTracker"%>
<%@page import="org.setupx.repository.core.communication.status.Status"%>
<%@page import="org.setupx.repository.core.communication.status.Timestamped"%>
<%@page import="org.setupx.repository.core.communication.status.UserAssigned"%>
<%@page import="org.setupx.repository.core.communication.status.SampleArrivedStatus"%>
<%@page import="org.setupx.repository.core.communication.status.LabStatus"%>
<%@page import="org.setupx.repository.core.communication.status.FilecreatedStatus"%>
<%@page import="org.setupx.repository.core.communication.status.SampleShippedStatus"%>
<%@page import="org.setupx.repository.core.communication.status.PostprocessEventStatus"%>
<%@page import="org.setupx.repository.core.communication.status.SampleRunStatus"%>
<%@page import="org.setupx.repository.core.communication.status.ResultReceivedStatus"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Comparator"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.communication.status.DynamicStatus"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%


String[] colors = new String[]{"RED", "#ff6600","#cc6600","#996600","#666600","#336600","#006600", "GREEN"};

org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

Report report = (Report)session.getAttribute(org.setupx.repository.web.WebConstants.SESS_REPORT);
List clazzIDs = new SXQuery().findClazzIDsByPromtID(report.getPromtID());



// additional check for access right
try {
    int access = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID(), report.getPromtID()).getAccessCode();
    if (access < 20){
		throw new PersistenceActionFindException("The user has insufficient right not access this information.");
    }
} catch (Exception exception){
    request.getSession().setAttribute(org.setupx.repository.web.WebConstants.SESS_FORM_MESSAGE, exception.getMessage());
    %>
    <jsp:forward page="error.jsp"/>
    <% 
}

%>	


<body>
<!--  head containing logo and description -->

	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2><%=new SXQuery().findPromtTitleByPromtID(report.getPromtID()) %></h2>
					Experiment <%=report.getPromtID()%><br/>
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>

	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} %>
	

<table align="center" width="60%" cellpadding="5">	
	<!--  back to the menu -->


	<% if (!user.isLabTechnician()){ %>
	<tr>
		<td colspan="1" align="right" style="background-color:#FFFFFF"><a href="main.jsp"><img  border='0' src="pics/back.gif"></td>
		<td colspan="8" style="background-color:#6682B6" ></td>
		<td colspan="1" style="background-color:#6682B6" ></td>
	</tr>
	<%} %>





	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<th align="left" colspan="11">Information: <br><font class="small">&nbsp;&nbsp;information related to this experiment in the LIMS</font> </td>
		<th align="center"><img border='0' src="pics/aq_help.gif"></td>		
	</tr>

	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td width="0">Title</td>
		<td colspan="7" width="2100" style="background-color:#FFFFFF"><%=new SXQuery().findPromtTitleByPromtID(report.getPromtID()) %></td>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td width="0">Abstract</td>
		<td colspan="7" style="background-color:#FFFFFF">
				<%
			String myabstract = new SXQuery().findPromtAbstractByPromtID(report.getPromtID());
			myabstract = Util.replace( myabstract, "\n", "<p>");
		
			long promtID = report.getPromtID();
		%>

		<%=myabstract%>
		</td>
		
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td width="0">Structure</td>
		<td colspan="7" style="background-color:#FFFFFF">
			<%@ include file="incl_experiment_structure.jsp"%>
		</td>
		
	</tr>	
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td >Species</td>
		<% 
			String speciesString = new SXQuery().findSpeciesbyPromtID(promtID).get(0).toString();
			int ncbiID = NCBIConnector.determineNCBI_Id(speciesString);
		%>
		<td colspan="7" style="background-color:white;" ><b><%=speciesString%></b> <a href="species_detail.jsp?ncbiID=<%=ncbiID %>"><img  border='0' src="pics/link.gif"></a>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>

	

<%		if (user.isPublic()){ %>
	
	<%
String comment = (String) hqlSession.createSQLQuery("select element3.value as comment from formobject as promt, formobject as page, " +
        "formobject as element3 where promt.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.Promt\" " +
        "and page.parent = promt.uoid and element3.parent = page.uoid and element3.question = \"comment\" and " + 
        "element3.discriminator =\"org.setupx.repository.web.forms.inputfield.BigStringInputField\" " + 
        "and promt.uoid = \""+report.getPromtID()+ "\"").addScalar("comment", org.hibernate.Hibernate.STRING).list().get(0);

comment = Util.replace(comment,'\n',"<br>");
%>
	
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td width="0">Comment 
			</td>
			<td colspan="7" style="background-color:#FFFFFF"><%=comment %>
			<% 
			if (comment.length() > 2){ %><br><%} %>
				<font class="small">-- add your comment: --</font>
				<br>
				<form action="addcomment.jsp?pid=<%=promtID%>">
					<input type="hidden" name="pid" value="<%=promtID%>"/> 
					<%=user.getDisplayName() %>: <input type="text" name="comment" size="30">
					<%=DateFormat.getDateInstance(DateFormat.MEDIUM, java.util.Locale.US).format(new Date()) %>
					<input title="add comment" src='pics/editcomment.gif' type="image" size="4" value="connect"/>
				</form>
				
				
			</td>
			
		</tr>	

<% }// public user%>
	


	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td width="0">Classes</td>
		<td colspan="7" style="background-color:#FFFFFF"><%=clazzIDs.size() %></td>
		
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td width="0">Samples</td>
		<% 			int samplesTotal = new SXQuery().findSampleIDsByPromtID(report.getPromtID()).size(); %>
		<td colspan="7" style="background-color:#FFFFFF"><%=samplesTotal%></td>
		
	</tr>
	
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td width="0">Collaboration / Owner</td>
		<td colspan="7" style="background-color:#FFFFFF"><%=new SXQuery().findPromtCollaborationHTML(promtID)%></td>
		
	</tr>
			<% int _x = new SXQuery().determineSamplesFinishedByPromtID(report.getPromtID()); 
			if (_x > 0){ %>
	<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td width="0">Status</td>
			<td colspan="7" style="background-color:#FFFFFF">
			<table width="100%">
				<% SXQueryCacheObject queryCacheObject = SXQueryCacheObject.findByExperimentID(report.getPromtID()); %>
				<%@ include file="incl_processbar.jsp"%> 
			</table>
			</td>
			
		</tr>
		    <%} %>



   	<%
   	int publicDataCount = 0;
   	String countPubs = "";
   	try {
    	countPubs = "select data.uoid as s from pubdata as data, formobject as f where data.relatedExperimentID = f.uoid and f.uoid = " + promtID;
    	
		publicDataCount = hqlSession.createSQLQuery(countPubs).addScalar("s", Hibernate.STRING).list().size();
   	} catch (Exception e){
   	}

   	
%>
   	
    <!--  link to results -->
	<%
	String pubQuery = "select d.published as date, d.publishedUserID as userID, sample.uoid as sampleID, d.label as com, a.label as label from pubsample as sample, pubdata as d, pubattribute as a where d.relatedExperimentID = " + report.getPromtID()  +" and d.uoid = sample.parent and a.parent = sample.uoid group by a.label order by a.label ";
	Logger.log(this, pubQuery);
	List pubList = CoreObject.createSession()
					.createSQLQuery(pubQuery)
					.addScalar("date", Hibernate.DATE)
					.addScalar("userID", Hibernate.LONG)
					.addScalar("sampleID", Hibernate.LONG)
					.addScalar("label", Hibernate.STRING)
					.addScalar("com", Hibernate.STRING)
					.list();
	if (pubList.size() > 0){
	    %>
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td >substances<br><font class="small">&nbsp;&nbsp;substances detected</font> </td>
			<td colspan="7" style="background-color:white;" >		
				<table width="100%">
					<tr>
						<td rowspan="100" style="background-color:white; " valign="top" align="center" width="200">
							<img  border='0' src="pics/compound.png">
							<br>
							List of compound detected in this experiment. See each sample and 
							each compound here <a href="pubsample.jsp?promt=<%=report.getPromtID() %>&show=false"><img border='0' src="pics/arrow-icon.gif"></a>
							
						<th>link to data
						<th colspan="2">published
				<%
				boolean showCompound = Boolean.valueOf(request.getParameter("showCompound")).booleanValue();
						
				for (int pl = 0; ( pl < 5 || showCompound) && pl < pubList.size(); pl++){
				    Object[] objects = (Object[])pubList.get(pl);
				    %>
					<tr>
						<td><a href="pubsample.jsp?<%=objects[3]%>&promt=<%=promtID %>&show=false"><%=objects[3] %></a>
						<td colspan="2"><font class="small"> on <%=((Date)objects[0]).toString() %></font>
				    <%
				}  
					if (!showCompound && pubList.size() > 5){
				%>
					<tr>
						<td>
						<td>
						<td><font class="small"><a href="load?id=<%=promtID %>&action=20&showCompound=true"> show all <%=pubList.size() %> compounds</a> </font>
				<%} %>
			</table>
			<td style="background-color:#6682B6" width="20"></td>
		</tr>
	    <%
	}
	%>
   	
		    		

<%		if (user.getUsername().compareTo("public") != 0){ %>
	


	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<th align="left" colspan="11">Edit, Process, ...: <br><font class="small">&nbsp;&nbsp;download the result files, edit the experiment, assign it to other users, ...</font> </td>
		<th align="center"><img border='0' src="pics/aq_help.gif"></td>		
	</tr>



		<% 
		// -----------------------------------------------------------------------------------------------------------------			
		//							edit button for all
		// -----------------------------------------------------------------------------------------------------------------				
		%>
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td width="0">Report</td>
			<td width="0" style="background-color:#FFFFFF" align="center">
				<a href="pubexperiment.jsp?id=<%=promtID %>"><img  border='0' src="pics/go_report.gif"></a>
			</td>
			<td colspan="6" style="background-color:#FFFFFF" >
					
		</tr>
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td width="0"></td>
			<td width="0" style="background-color:#FFFFFF" align="center">
				<a href="report_raw.jsp?id=<%=promtID %>"><img  border='0' src="pics/go_report.gif"></a>
			</td>
			<td colspan="6" style="background-color:#FFFFFF" >
					raw design of the experiment.
		</tr>
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td width="0">Edit</td>
			<td align="center" style="background-color:#FFFFFF">
					<a href="load?id=<%=report.getPromtID()%>&action=40">
						<img  border='0' src="pics/go.gif">
					</a>
			</td>
			<td colspan="6" style="background-color:#FFFFFF" >
					Edit metadata of your experiment. This will get you back to the inital entry forms that you started with.
					<%if (report.getPromtID() < 318159 ){%><font class="small"><br>Experiment can not be edited anymore.</font><%} %>
			</td>
			
		</tr>
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td width="0">Add additional metadata</td>
			<td width="0" style="background-color:#FFFFFF" align="center">
				<a href="xlsimport0.jsp?promt=<%=report.getPromtID()%>&action=40"><img  border='0' src="pics/go_export_old.gif"></a>
			</td>
			<td colspan="6" style="background-color:#FFFFFF" >
					Add additional metadata to this experiment. <br>Helps you merge your <b>existing data</b> that into <%=org.setupx.repository.Config.SYSTEM_NAME%>.
					<br><b>TIP:</b> <font class="small">If you sort your data in the XLS sheet in the same order as your sample are sorted in the <%=org.setupx.repository.Config.SYSTEM_NAME%> it will help you assigning them a lot.</font>
					</td>
		</tr>
<% if (user.isMasterUser(user.getUOID())){ %>
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td width="0"></td>
			<td width="0" style="background-color:#FFFFFF" align="center">
				<a href="metadata_list.jsp"><img  border='0' src="pics/go_report.gif"></a>
			</td>
			<td colspan="6" style="background-color:#FFFFFF" >
					List of different attributes added by users.
		</tr>
<%} %>

		

<% } // public %>
	


		<% 
		// -----------------------------------------------------------------------------------------------------------------			
		//							Download
		// -----------------------------------------------------------------------------------------------------------------				
		%>		
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td >Downloads<br><font class="small">&nbsp;&nbsp;</font> </td>
			<td align="center" style="background-color:#FFFFFF">
					<a href="download_related_data.jsp"><img  border='0' src="pics/go_download.gif"></a>
			<td colspan="6" style="background-color:#FFFFFF" >
					download data related to this experiment
		</tr>






		<% 
		// -----------------------------------------------------------------------------------------------------------------			
		//							Export
		// -----------------------------------------------------------------------------------------------------------------				
		%>		
<%		if (user.getUsername().compareTo("public") != 0){ %>
	


			<% try{
			    PromtUserAccessRight.checkAccess(user.getUOID(), report.getPromtID(), PromtUserAccessRight.EXPORT);
			    %>
			<tr>
				<td style="background-color:#6682B6" width="20"></td>
				<td width="0">Export</td>
				<td align="center" style="background-color:#FFFFFF">
				<a href="index_export.jsp">
					<img  border='0' src="pics/go_export.gif"></a></td>
				<td colspan="6" style="background-color:#FFFFFF" >
						Export this experiment including all samples or just a subset of the samples to the connected annotaion system (BinBase).
						</td>
				
			</tr>

			<% 
			}catch (Exception e)			    {%> 
			<tr>
				<td style="background-color:#6682B6" width="20"></td>
				<td width="0">Export</td>
				<td align="center" style="background-color:#FFFFFF"><a href="load?id=<%=report.getPromtID()%>&action=40"><img  border='0' src="pics/go_locked.gif"></a></td>
				</td>
				<td colspan="6" style="background-color:#FFFFFF" >
						Export this experiment including all samples or just a subset of the samples to the connected annotaion system (BinBase).
						</td>
				
			</tr>
			<%}%>



			


		<% 
		// -----------------------------------------------------------------------------------------------------------------			
		//							share button for techs
		// -----------------------------------------------------------------------------------------------------------------				
		%>

	<% if (user.isLabTechnician()){ %>
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td width="0">Share</td>
			<td align="center" style="background-color:#FFFFFF"><a href="accessright_experiment.jsp?pid=<%=report.getPromtID()%>"><img  border='0' src="pics/go_share.gif"></a></td>
			<td colspan="6" style="background-color:#FFFFFF" >
					Share this experiment with other users.
					</td>
			
		</tr>
	<%} %>


		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td width="0">Labeling</td>
			<td width="0" style="background-color:#FFFFFF" align="center">
					<img  border='0' src="pics/go_blank.gif"></a>
			</td>
			<td colspan="6" style="background-color:#FFFFFF" >
					Print labels for theses samples.
					<font class="small">  (Access restricted to lab workstations.)</font>
					</td>
			
		</tr>
		
		
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td width="0">Tracking</td>
			<td width="0" style="background-color:#FFFFFF" align="center">
					<a href="sample_status_create.jsp?id=<%=promtID%>">
						<img  border='0' src="pics/go_track.gif">
					</a>
			</td>
			<td colspan="6" style="background-color:#FFFFFF" >
					Track this experiment
					</td>
			
		</tr>
<%	} // public end %>
	
		


	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<th align="left" colspan="11">Classes & Samples: 
			<br><font class="small">&nbsp;&nbsp;list of classes including their samples
			<br>&nbsp;&nbsp;Each of the different colors represents a different class.</font> 
		</td>
		<th align="center"><img border='0' src="pics/aq_help.gif"></td>		
	</tr>


		<% 
		// -----------------------------------------------------------------------------------------------------------------			
		//							START Class Header
		// -----------------------------------------------------------------------------------------------------------------				
		%>
	
	<!--  each class -->
	<%  
	// determine if we want to see the files ...
	boolean showfiles = true;
	
	try{
	    String filesParameter = request.getParameter("files");
	    if (filesParameter.compareTo("null") != 0){
		    showfiles = new  Boolean(filesParameter).booleanValue();
	    } 
	} catch (Exception e){
	    
	}
	
	
	//	 endings for filecheck
	String[] endings = new String[]{"peg","smp", "txt", "cdf"}; //"smp", 
	String[] paths = new String[]{"/mnt/gctof/ucdavis/data", "/mnt/gctof/ucdavis/data", "/mnt/binbase/data","/mnt/binbase/data"}; //"/mnt/gctof/setupx/fsa",

	java.util.Iterator iterator = clazzIDs.iterator();
    int i = 0;
	while (iterator.hasNext()){
	    i++;
		long clazzID = Long.parseLong("" + iterator.next());%>
		<!-- clazz: <%=clazzID%> -->
		
	<tr>
		<td width="20"></td>
		<th colspan="8"></th>
		<td ></td>
	</tr>
		<tr>
			<td style="background-color:<%=Util.getColor((int)clazzID*2)%>"></td>
			<th ><%=clazzID%>  <font size="-2"><%=i%>/<%=clazzIDs.size() %></font></th>
			<td colspan="3" style="background-color:<%=Util.getColor((int)clazzID*2)%>">
			<table>
		<% 
		// -----------------------------------------------------------------------------------------------------------------			
		//							Lables for Class
		// -----------------------------------------------------------------------------------------------------------------				
		%>
			<% 
		java.util.Iterator iterator1 = new SXQuery().findLabelsforClazz(clazzID).iterator();
		while (iterator1.hasNext()) {
		  Object[] element = (Object[]) iterator1.next();
		%>
			<tr>
				<td align="right"  style="background-color:<%=Util.getColor((int)clazzID*2)%>">-</td>
				<td align="right"  style="background-color:<%=Util.getColor((int)clazzID*2)%>"><font class="small"><b><%=element[0].toString()%></b></font></td>
				<td align="left"  style="background-color:<%=Util.getColor((int)clazzID*2)%>"><font class="small"><%=element[1].toString()%></font></td>
			</tr>
		
		  <%
		}
		%>
		</table>
	
			
			</td>
			<td style="background-color:<%=Util.getColor((int)clazzID*2)%>">
				<!--  emptfy for the filename -->
			</td>
			<td style="background-color:<%=Util.getColor((int)clazzID*2)%>">

			</td>
			<td style="background-color:<%=Util.getColor((int)clazzID*2)%>">


			</td>
			<td valign="bottom" style="background-color:<%=Util.getColor((int)clazzID*2)%>">
			
			
				<%// FILES ---------------------------
				if (false){%>
					<table width="0" align="center">
							<tr>
							<%
							for (int _i = 0; _i < endings.length; _i++){
							    String ending = endings[_i];
							    %>
								<th width="25"><%=ending %></th>
							<% 
							}
							%>
							</tr>
					</table>
				<%} %>
			<td></td>
		</tr>
		
<% 

// getting the samplesIDs 
List sampleIDs = new SXQuery().findSampleIDsByClazzID((int)clazzID);

java.util.Iterator samples = sampleIDs.iterator();
int j = 0;

while (samples.hasNext()){
    	long sampleID = Long.parseLong("" + samples.next());
%>
		<% 
		// -----------------------------------------------------------------------------------------------------------------			
		//							Sample START
		// -----------------------------------------------------------------------------------------------------------------				
		%>

		<!--  new Style -->
		<%
							// should be replaced by getStatusNEWEST() - meaning that only the newest status is beeing shown and only the 
							// newsest one is actually needed.
			
		Status status = null;
		try {
			status = StatusTracker.getLatestStatus(sampleID);		
		} catch (Exception e){
		    status = null;
		}

		%>
		<tr>
			<td rowspan="2" style="background-color:<%=Util.getColor((int)clazzID*2)%>"></td>
			<td rowspan="2" align="center">
				<a name="<%=sampleID%>"><%=sampleID%></a>
			</td>
			<td width="2" align="center" style="background-color:#FFFFFF" colspan="5">
					<%

					
					String commentValue = "select child.value from " + FormObject.class.getName() + " as child, " 
					+ Sample.class.getName() + " as sample " + 
					"where child.question = 'comment' " + 
					"and child.parent = sample " + 
					"and sample = '" + sampleID + "'  ";

					String labelValue = "select child.value from " + FormObject.class.getName() + " as child, " 
					+ Sample.class.getName() + " as sample " + 
					"where child.question = 'label' " + 
					"and child.parent = sample " + 
					"and sample = '" + sampleID + "'  ";
				
			        
					String sampleLabel = "" + hqlSession.createQuery(labelValue).list().get(0);
					String sampleComment = "" + hqlSession.createQuery(commentValue).list().get(0);
					
					if (!sampleComment.startsWith("-your comm")){ 
					    out.print(sampleComment);
					}

					if (!sampleLabel.startsWith("sx")){ 
					    out.print("<br><font class=\"small\">" + sampleLabel + "</font>");
					}
					
					%>
			<td colspan="2">
				<%if (status instanceof Timestamped){ %>
				<font class="small">
					<br>Last activity: 
					<%=DateFormat.getDateInstance(DateFormat.MEDIUM, java.util.Locale.US).format(((Timestamped)status).getDate()) %>
				</font>
				<%} %>

			<% if (status != null){ 
			   // Status status = (Status)statusList.get(0);	
			    // --------------ICON---------------------
				if (status instanceof SampleArrivedStatus){%>
			        <td rowspan="2" style="background-color:white;" align="center"><img  border='0' src="pics/frost.jpg">	        
				<%} else if (status instanceof LabStatus){%>
			        <td rowspan="2" style="background-color:white;" align="center"><img  border='0' src="pics/sample_prep.jpg">	        
				<%} else if (status instanceof FilecreatedStatus){%>
			        <td rowspan="2" style="background-color:white;" align="center"><img  border='0' src="pics/file.png">	        
				<%} else if (status instanceof SampleShippedStatus){%>
			        <td rowspan="2" style="background-color:white;" align="center"><img  border='0' src="pics/package.gif">	        
				<%} else if (status instanceof ResultReceivedStatus){%>
			        <td rowspan="2" style="background-color:white;" align="center">
				<%} else if (status instanceof PostprocessEventStatus){%>
			        <td rowspan="2" style="background-color:white;" align="center"><img  border='0' src="pics/chart.png">	        
				<%} else if (status instanceof SampleRunStatus){%>
			        <td rowspan="2" style="background-color:white;" align="center"><img  border='0' src="pics/run.jpg">	        
				<%} else {
				    Logger.log(this, "" + status.getClass());
					%>
			        <td rowspan="2" style="background-color:white;" >
			    <%}%>
		    <% } else {
		    	%>
		    	<td rowspan="2" style="background-color:white;" ><%
		    
		    }%>
		</tr>
		<tr>
			<td width="2" align="center" style="background-color:#FFFFFF">
				<a href="sample_detail.jsp?id=<%=sampleID%>"><img  border='0' src="pics/details.gif"></a>
			</td>
			<td colspan="4" style="background-color:#FFFFFF">
								<font class="small">


							<% 
				boolean sheduled = false;
				String sheduledName = "";
				String _name = "";
				boolean checkForSheduledName = false;
				if (_x >0){
					try { 
					_name = new SXQuery().findAcquisitionNameBySampleID(sampleID); 
					List listOfScannedSamples = new SXQuery().findScannedPair(sampleID);
				    sheduled = true;
					%>
						<%=_name%>
						
						<% if (listOfScannedSamples.size() > 1){ %>
							<font class="small"><br>(<b><%=listOfScannedSamples.size() %></b> <a href="sample_detail.jsp?id=<%=sampleID%>">runs</a> in total)</font>
						<%} %>
					</td>
				<%
					} catch (PersistenceActionFindException e){
					   checkForSheduledName = true;
					}
				} 
				if (checkForSheduledName) {
				    // check if sample has been sheduled.
					if (!sheduled){
						try{ 
						    sheduledName = LecoACQFile.determine(sampleID + "" , AcquisitionParameter.NAME);
						    sheduled = true;
							%>
							<i><%=sheduledName%></i>
							<font class="small"><br>sheduled</font>
							<%
						} catch (Exception e){
						    e.printStackTrace();
						} 
					}

				}
				%>



					</font>
			
			
			<td width="0" align="center">
				<table border="1" style="border-color:black;">
				<tr>
				<%
				String _status_string = "";
			    int col = 1;
				if (true){ 
				    //Status status = (Status)statusList.get(0);
					if (status instanceof SampleArrivedStatus){
					    col = 2;
					    _status_string = "arrived in lab";
					} else if (status instanceof LabStatus){
					    col = 3;
					    _status_string = "prepared in lab";
					} else if (status instanceof FilecreatedStatus){
					    col = 5;
					    _status_string = "datafile created";
					} else if (status instanceof SampleShippedStatus){
					    col = 1;
					    _status_string = "sample shipped";
					} else if (status instanceof ResultReceivedStatus){
					    col = 7;
					    _status_string = "result received";
					} else if (status instanceof PostprocessEventStatus){
					    col = 6;
					    _status_string = "postprocessing";
					} else if (status instanceof SampleRunStatus){
					    col = 5;
					    _status_string = "finished gcms run";
					} else {

					}
					}
					for (int statTab = 1; statTab <= 7; statTab++){
					    if (statTab <= col){
							%> <td style="background-color:<%=colors[statTab]%>">&nbsp;&nbsp;&nbsp; <%
					    } else {
							%> <td style="background-color:white;">&nbsp;&nbsp;&nbsp; <%
					    }
					}
					%> 
				</tr>				
				</table>
				
			<td aling="center">
				<b><%=_status_string %></b>
		</tr>


	<% } %>
	<!--  end of samples -->  
	<% } %>





</table>
</body>





<%@ include file="footer.jsp"%>
