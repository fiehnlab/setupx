<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.communication.leco.ColumnInformation"%>
<%@ page language="java" %>
<%@ include file="checklogin.jsp"%>
<%@ include file="header_standard.jsp"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<form action="exp2_submission_regrouped.jsp">
	<table align="center" width="60%" cellpadding="5">	
	 
	<tr>
		<td align="center">
			<input type="checkbox" name="import" value="true" checked="checked" >
		</td>
		<td>
			import
		</td>
	</tr>	 
	<tr>
		<td align="center">
			<input type="checkbox" name="export" value="true" checked="checked" disabled="disabled">
		</td>
		<td>
			export
		</td>
	</tr>	 
	<%
	ColumnInformation[] columnInformations = new ColumnInformation[]{
	        ColumnInformation.MDM35,
	        ColumnInformation.RTX5,
	        ColumnInformation.RTX5GCxGC, 
	        ColumnInformation.TESTCOLUMN};
	%>

<%
	// find the ID 
	long promtID = Long.parseLong(request.getParameter("promt"));
%>

	<input type="hidden" name="promt" value="<%=promtID%>"></td>
	<%
	for (int i = 0; i < columnInformations.length; i++){
	%>
	<tr>
		<td align="center">
			<input type="radio" name="col" value="<%=columnInformations[i].getBbid()%>" 
			<%if (columnInformations[i].getLable().compareTo("RTX 5") == 0 ){  %>checked="checked"  <%} %>> 
		</td>
		<td>
					<%=columnInformations[i].getLable() %><br>
					<font class="small"><%=columnInformations[i].getDescription() %></font> 
		</td>
	</tr>	 
	<% 
	}
	%>
	<tr>
		<td align="center">
			<input type="text" name="email" value="<%=user.getMailAddress().getEmailAddress()%>"> 
		</td>
		<td>
					eMail address<br>
					<font class="small">Please enter the email address that you want the results to delivered to. </font> 
		</td>
	</tr>	 
	<tr>
		<td>

		</td>
		<td>
			<input title="search" src='pics/go_export.gif' type="image" size="4" value="connect"/>
		</td>
	</tr>	 
	</table>
</form>

		
<%@ include file="footer.jsp"%>


		