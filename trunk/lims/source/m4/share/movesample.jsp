<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<%@ include file="checklogin.jsp"%>

<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.SQLQuery"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.setupx.repository.web.forms.FormObject"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>

<%
	int sampleID = Integer.parseInt(request.getParameter("id"));
	try {
	    int targetClass = Integer.parseInt(request.getParameter("c"));
	    
	    if (targetClass > 0){
	        // update a sample 
	        Query query = CoreObject.createSession().createQuery("update " + FormObject.class.getName() + " set parent = " + targetClass + " where uoid = " + sampleID);
	        query.executeUpdate();
	    }
	    
	} catch (Exception e){
		e.printStackTrace();
	}
	

	int targetPromt = 0;
	try {
		// secondary experiment
		targetPromt = Integer.parseInt(request.getParameter("t"));
	} catch (Exception e){
		e.printStackTrace();
	}

	
	int classID = new SXQuery().findClazzIDBySampleID(sampleID);
	int promptID = new SXQuery().findPromtIDbySample(sampleID);

	List samplesIDs = new SXQuery().findSampleIDsByPromtID(promptID);
	List classIDs = new SXQuery().findClazzIDsByPromtID(promptID);
	
%>


<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>






<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width='60%'>
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Rearrange Sample</h2>
			Rearrange samples within an experiment or move them from one experiment to another.
			<br/>
			<font size="-2">Below you find all samples in the selected experiement <b>"<%=new SXQuery().findPromtTitleByPromtID(promptID) %>"</b>. You can change certain samples
			from one class to other simply by checking the combination of sample & class.</font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>

<table align="center" width="60%" cellpadding="2">
	<tr><td>

<table style="background-color: white" align="center">
	<tr>
		<th colspan="2">Sample
		<th colspan="33">Targe Classes
		


	<%
	Iterator sampleIterator = samplesIDs.iterator();
	int count = 0;
	while(sampleIterator.hasNext()){
	    count++;
		int tSampleID = Integer.parseInt(sampleIterator.next().toString());
		int tClazzIDAssigned = new SXQuery().findClazzIDBySampleID(tSampleID);
		%>

	<tr>
		<% if (sampleID == tSampleID){ %>
			<td style="background-color: yellow;">
		<%} else { %>
			<td>
		<%} %>
			<a href="sample_detail.jsp?id=<%=tSampleID %>"><img border='0' src="pics/details.gif"></a>
				<a id="<%=tSampleID %>"/>
				<%=tSampleID %>
			
		</td>
		
		<% if (sampleID == tSampleID){ %>
		<td style="background-color: yellow;">
		<%} else { %>
		<td>
		<%} %>
			<%=new SXQuery().findSampleLabelBySampleID(tSampleID)%>
		</td>
		<%
		Iterator clazzIterator = classIDs.iterator();
		Iterator[] iterators ;
		if (targetPromt != promptID){
			Iterator targetIterator = new SXQuery().findClazzIDsByPromtID(targetPromt).iterator();
			iterators = new Iterator[]{clazzIterator, targetIterator};
		} else {
			iterators = new Iterator[]{clazzIterator};
		}
		
		for (int i = 0; i < iterators.length; i++){
		    Iterator iterTemp = iterators[i];
			%>
		    <td style="background-color: white;">&nbsp;&nbsp;
			<%
		    while(iterTemp.hasNext()){
				int tClazzID = Integer.parseInt(iterTemp.next().toString());
				if (tClazzID == classID || sampleID == tSampleID){ %>
					<td style="background-color: yellow;">
				<%}else { %>
					<td>
				<%}%>
			
				<%
					if (tClazzIDAssigned == tClazzID){
					    %> 	<img border='0' src="pics/check.gif">
						<% 
					} else {
					    %>
						    <a href="?id=<%=tSampleID %>&c=<%=tClazzID %>&t=<%=promptID%>#<%=tSampleID %>" title="move sample <%=tSampleID %> to class <%=tClazzID %>">
						    	<img border='0' src="pics/o.gif">
						    </a>
					    <% 
					}
				%>
			</td>
			<%}
		}
 %>
	</tr>
	<%} %>
</table>




<table style="background-color: white" align="center">
  <tr>
  	<th colspan="21">
  		In case you want to move one of the samples from this experiment to another class in another experiment please<br>
  		select the experiment from the list of your experiments below and the classes of your experiment will be listed<br>
  		too. 


  <tr>
	<td colspan="21">
		<form action="movesample.jsp">
			<select name="t" onChange="document.forms[1].submit()">
				<option>
					-- select your experiment --
				</option>
		<%
		 Iterator iteratorAccess = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID()).iterator();
		 while (iteratorAccess.hasNext()){
		     PromtUserAccessRight access = (PromtUserAccessRight)iteratorAccess.next();
		     	if (access.getAccessCode() >= PromtUserAccessRight.WRITE){
				%>
				<option value="<%=access.getExperimentID()%>">
					[<%=access.getExperimentID() %>]&nbsp;<%=new SXQuery().findPromtTitleByPromtID(access.getExperimentID())%>  
				</option>
				<%  
		     	}
		 }
		 %>
		</select>
		<input type="hidden" name="id" value="<%=sampleID%>">
		</form>
	</td>

	</td>
  </tr>
</table>
