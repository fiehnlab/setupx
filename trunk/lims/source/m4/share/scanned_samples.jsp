<%@ include file="checklogin.jsp"%>

<%@ include file="head_blank.jsp"%>

<%@ include file="navi_admin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.communication.importing.metadata.*,
                 org.setupx.repository.server.persistence.*"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<table align='center' border='0' cellpadding='4' width="60%">
<%
org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

Iterator iter = hqlSession.createSQLQuery("select label, sampleID from scanned_samples where sampleID != \"1\" and sampleID != \"\" GROUP by label order by uoid desc  ")
.addScalar("label", org.hibernate.Hibernate.STRING)
.addScalar("sampleID", org.hibernate.Hibernate.STRING)
.list()
.iterator();

int i = 0;
while(iter.hasNext()){
Object[] array = (Object[])iter.next();
%>

<% if (i%20 == 0) {%>
	<tr>
		<td></td>
		<th align="center">name</th>
		<th align="center" colspan="2">sample</th>
		<th align="center" colspan="2">experiment</th>
		<th align="center">class</th>
		<th align="center">sample label</th>
		<th align="center">machine details</th>
	</tr>
<%} 
i++;
%>
<%
int promtID = 0;
try {
    promtID = new SXQuery().findPromtIDbySample(Integer.parseInt("" + array[1])); 
}catch (Exception e){
    
}

%>
	<tr>
		<td></td>
		<td align="center"><%=array[0]%></td>
		<td align="center"><%=array[1]%></td>
		<td align="center" style="background-color:#FFFFFF"><a href="sample_detail.jsp?id=<%=array[1]%>"><img  border='0' src="pics/details.gif"></a></td>
		<td align="center"><%=promtID %></td>
		<td align="center" style="background-color:#FFFFFF"><a href="load?id=<%=promtID%>&action=20"><img  border='0' src="pics/details.gif"></a></td>
		<td align="center">
				<% try { %>		
					<%=new SXQuery().findClazzIDBySampleID(Integer.parseInt("" + array[1])) %>
				<% } catch (PersistenceActionFindException e){%>
					----
				<% }%>
		</td>
		<td style="background-color:#FFFFFF">
				<% try { %>
					<a href="sample_detail.jsp?id=<%=array[1]%>"><%=new SXQuery().findSampleLabelBySampleID(Integer.parseInt("" + array[1])) %></a>
				<% } catch (PersistenceActionFindException e){%>
					----
				<% }%>
		</td>
		<td align="center" style="background-color:#FFFFFF"><a href="run_detail.jsp?id=<%=array[1]%>"><img  border='0' src="pics/details.gif"></a></td>	
	</tr>
    <%
}
%>

</table>

<%@ include file="footer.jsp"%>