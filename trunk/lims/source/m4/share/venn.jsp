<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.List"%>

<%@page import="java.util.Hashtable"%>



<link href="stylesheet.css" rel="stylesheet" type="text/css"/>



<%
    // filtering the groups and create subgroups out of them
    Vector groupA = new Vector();
    Vector groupB = new Vector();
    Vector groupC = new Vector();
    Vector groupAB = new Vector();
    Vector groupAC = new Vector();
    Vector groupBC = new Vector();
    Vector groupABC = new Vector();

    Enumeration keyEnumeration = datasets.keys();
    Vector all = new Vector();

    List[] lists = new List[3];
    String[] listNames = new String[7];

    int stp = 0;

    while (keyEnumeration.hasMoreElements()) {
        String key = keyEnumeration.nextElement().toString();

        Logger.log(this, " working on key: " + key);
        List list = (List) datasets.get(key);
        listNames[stp] = key;
        lists[stp] = list;
        stp++;

        Logger.log(this, " working on data: " + list.size()
                + " elements.");
        all.addAll(list);
    }

    Iterator listIterator = all.iterator();
    while (listIterator.hasNext()) {

        String term = listIterator.next().toString();
        Logger.log(this, "working on term: " + term);

        if (lists[0].contains(term) && lists[1].contains(term)
                && lists[2].contains(term)) {
            if (!groupABC.contains(term))
                groupABC.add(term);
        } else if (lists[0].contains(term) && lists[1].contains(term)) {
            if (!groupAB.contains(term))
                groupAB.add(term);
        } else if (lists[1].contains(term) && lists[2].contains(term)) {
            if (!groupBC.contains(term))
                groupBC.add(term);
        } else if (lists[0].contains(term) && lists[2].contains(term)) {
            if (!groupAC.contains(term))
                groupAC.add(term);
        } else if (lists[0].contains(term)) {
            if (!groupA.contains(term))
                groupA.add(term);
        } else if (lists[1].contains(term)) {
            if (!groupB.contains(term))
                groupB.add(term);
        } else if (lists[2].contains(term)) {
            if (!groupC.contains(term))
                groupC.add(term);
        }
    }

    /**
     Supply one data set where:

     * the first three values specify the relative sizes of three circles, A, B, and C
     * the fourth value specifies the area of A intersecting B
     * the fifth value specifies the area of A intersecting C
     * the sixth value specifies the area of B intersecting C
     * the seventh value specifies the area of A intersecting B intersecting C

     chtt=<chart title>
     chdl=<first data set label>|<n data set label>
     */

    int[] sizes = new int[] {
            groupA.size() + groupAB.size() + groupAC.size() + groupABC.size(), // all of A
            groupB.size() + groupAB.size() + groupBC.size() + groupABC.size(), // all of B
            groupC.size() + groupBC.size() + groupAC.size() + groupABC.size(), // all of C
            groupAB.size(), 
            groupAC.size(),
            groupBC.size(), 
            groupABC.size() };

    listNames[3] = listNames[0] + " & " + listNames[1];
    listNames[4] = listNames[0] + " & " + listNames[2];
    listNames[5] = listNames[1] + " & " + listNames[2];
    listNames[6] = listNames[0] + " & " + listNames[1] + " & " + listNames[2];

    String data = "chd=t:";

    for (int xxa = 0; xxa < sizes.length; xxa++) {
        data = data + sizes[xxa];
        Logger.log(this, " size:" + sizes[xxa]);
        if (xxa + 1 < sizes.length)
            data = data + ",";
    }

    String labels = "chdl=" + listNames[0] + "|" + listNames[1] + "|"
            + listNames[2] + "";
    String size = "chs=520x300";

    List[] listAll = new List[] { groupA, groupB, groupC, groupAB,
            groupAC, groupBC, groupABC };

    // get top 10 compounds

    // determine compounds found per species
%>





<table align="center" width="60%" cellpadding="7">

	<tr>
		<th width="100%" align="center">
			Comparisson of detected compounds per species
	<tr>
		<td width="100%" align="center" style="background-color: white;">
			<img align="center" src="http://chart.apis.google.com/chart?cht=v&<%=size %>&<%=data %>&<%=labels %>">    
	<tr>
		<td width="100%" align="center">
			<b>graph: Detected compounds</b> Detected compounds per species and the overlap of compounds detected.
</table>
<br>
<table align="center">
    <tr>
<%
    for (int lCounter = 0; lCounter < listAll.length; lCounter++) {
%>
    	<td valign="top">
	    <table>
	    <tr>
	    	<th>
	    	<%=listNames[lCounter]%>
	    	<br>
	    	<font class="small">
	    	group <%=lCounter%> size: <%=listAll[lCounter].size()%>
	    	</font>
	    	
	    <%
	        Iterator groupIterator = listAll[lCounter].iterator();
	            while (groupIterator.hasNext()) {
	    %>
	        <tr>
	        	<td>
					<font class="small">
						<%
							String compoundTerm = groupIterator.next().toString();
						%>
						<a href="advancedquery_dev.jsp?term=<%=compoundTerm %>&type=1"><%=compoundTerm %></a>
	       			</font>
	       	<%
	       	    }
	       	%></table>
	    </td><%
	        }
	    %>

</table>










</body>
</html>