<%@page import="org.setupx.repository.web.forms.FormObject"%>
<%@page import="org.exolab.castor.xml.schema.Form"%>
<%@page import="org.setupx.repository.web.forms.inputfield.StringInputfield"%>
<%@page import="org.setupx.repository.web.forms.inputfield.InputField"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>

<%
    // find the experiemnt ID
    Promt promt = (Promt) session.getAttribute(WebConstants.SESS_FORM_PROMT);

    // list current attributes and option to remove them in case they are "custom"
    Sample[] samples = promt.getSamples();

    // get the attribute name
    int attName = Integer.parseInt(request.getParameter("pos").toString());
    String attLabel = request.getParameter("label").toString();

    // attatch this to each sample
    %>
    <%=attName%>
    <%

    try {
	    // loop over every sample and detatch it from the sample
	    for (int i = 0; i < samples.length; i++){
	        InputField field = (InputField)samples[i].getField(attName);
	        if (field.getQuestion().compareTo(attLabel) == 0){
	
	                samples[i].remove(field);
	        }else {
	            Logger.warning(this, "trying to remove incorrect object");
	        }
	    }  
	} catch(Exception e){
	     
	}
%>

<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<jsp:forward page="sample_sutom_meta.jsp"/>
