<%long id = Long.parseLong(request.getParameter("id"));%>
    
    
<%@page import="org.w3c.dom.EntityReference"%>
<%@page import="org.w3c.dom.ProcessingInstruction"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.setupx.repository.core.util.xml.XMLFile"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.setupx.repository.core.util.xml.XMLUtil"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.Iterator"%>

<%
	// create an xml output representing the attributes devclared as MSI fields in SetupX

	Document doc = XMLUtil.createNewDocument();
	doc.appendChild(doc.createElement("export"));
	
	Node root = doc.getDocumentElement();
	
	// load all attributes
	Session s = CoreObject.createSession();
	    MSIAttribute attribute = (MSIAttribute)CoreObject.persistence_loadByID(MSIAttribute.class, s, id);
			    
	    Element element = doc.createElement("attribute");

	    String key = "id";
	    String value = attribute.getMsiLabel();
        element.setAttributeNS("msi", key, value);

        key = "version";
        SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
        value = fmt.format(attribute.getVersion());        
        element.setAttributeNS("msi",key, value);
        
        


        if (attribute.getQuestion().length() > 0){   
	        Element description = doc.createElement( "label");
	        description.appendChild(doc.createTextNode(attribute.getQuestion()));
	        element.appendChild(description);
        }


        if (attribute.getExtension().length() > 0){   
	        Element description = doc.createElement("extension");
	        description.appendChild(doc.createTextNode(attribute.getExtension()));
	        element.appendChild(description);
        }

        if (attribute.getDescribtion().length() > 1){   
	        Element description = doc.createElement("description");
	        description.appendChild(doc.createTextNode(attribute.getDescribtion()));
	        element.appendChild(description);
        }

        if (attribute.getComment().length() > 1){   
	        Element comment = doc.createElement( "comment");
	        comment.appendChild(doc.createTextNode(attribute.getComment()));
	        element.appendChild(comment);
        }
        
	    Element sxElement = doc.createElement("repositoryinformation");
	    key = "uoid";
	    value = attribute.getUOID() + "";
	    sxElement.setAttribute( key, value);

	    key = "submitter";
	    value = attribute.getUserID() + "";
	    sxElement.setAttribute( key, value);
		element.appendChild(sxElement);

		root.appendChild(element);
		out.clear();
		response.setContentType("text/xml");
		response.setCharacterEncoding("UTF-8");
		out.write(new XMLFile(doc).content());
	%>

	            	
