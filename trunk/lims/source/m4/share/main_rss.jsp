<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.setupx.repository.core.util.xml.XMLUtil"%>
<%@page import="org.setupx.repository.core.util.xml.XMLFile"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<%@ include file="checknonlogin.jsp"%>

<%
	Document doc = XMLUtil.createNewDocument();

	String data = "type=\"text/xsl\" href=\"rss.xslt\"";
	
	doc.appendChild(doc.createElement("rss"));
	Element element;
	
	Element root = doc.getDocumentElement();
	root.setAttribute("version", "2.0");
	
	// @see http://cyber.law.harvard.edu/rss/rss.html#optionalChannelElements
	
	// the channel
	Node channel = doc.createElement("channel");
	root.appendChild(channel);
	// title	The name of the channel. It's how people refer to your service. If you have an HTML website that contains the same information as your RSS file, the title of your channel should be the same as the title of your website. 	GoUpstate.com News Headlines
	element = doc.createElement("title");
	element.appendChild(doc.createTextNode("Public study designs in the metabolomic repository " + Config.SYSTEM_NAME));
	channel.appendChild(element);

	// link	The URL to the HTML website corresponding to the channel.	http://www.goupstate.com/
	element = doc.createElement("link");
	element.appendChild(doc.createTextNode("/"));
	channel.appendChild(element);
	
	// description       	Phrase or sentence describing the channel.	The latest news from GoUpstate.com, a Spartanburg Herald-Journal Web site.
	element = doc.createElement("description");
	element.appendChild(doc.createTextNode("The latest study designs with a public access in " + Config.SYSTEM_NAME + "."));
	//channel.appendChild(element);

	// language	The language the channel is written in. This allows aggregators to group all Italian language sites, for example, on a single page. A list of allowable values for this element, as provided by Netscape, is here. You may also use values defined by the W3C.	en-us
	element = doc.createElement("language");
	element.appendChild(doc.createTextNode("en-us"));
	channel.appendChild(element);
	
	// copyright	Copyright notice for content in the channel.	Copyright 2002, Spartanburg Herald-Journal
	element = doc.createElement("copyright");
	element.appendChild(doc.createTextNode("This work is licensed under a Creative Commons Attribution 3.0 United States License."));
	channel.appendChild(element);
	
	// managingEditor	Email address for person responsible for editorial content.	geo@herald.com (George Matesky)
	element = doc.createElement("managingEditor");
	element.appendChild(doc.createTextNode(Config.OPERATOR_MANAGER.getEmailAddress()));
	channel.appendChild(element);
	
	// webMaster	Email address for person responsible for technical issues relating to channel.	betty@herald.com (Betty Guernsey)
	element = doc.createElement("webMaster");
	element.appendChild(doc.createTextNode(Config.OPERATOR_MANAGER.getEmailAddress()));
	channel.appendChild(element);
	
	// pubDate	The publication date for the content in the channel. For example, the New York Times publishes on a daily basis, the publication date flips once every 24 hours. That's when the pubDate of the channel changes. All date-times in RSS conform to the Date and Time Specification of RFC 822, with the exception that the year may be expressed with two characters or four characters (four preferred).	Sat, 07 Sep 2002 00:00:01 GMT

	// lastBuildDate	The last time the content of the channel changed.	Sat, 07 Sep 2002 09:42:31 GMT

	// category	Specify one or more categories that the channel belongs to. Follows the same rules as the <item>-level category element. More info.	<category>Newspapers</category>

	// generator	A string indicating the program used to generate the channel.	MightyInHouse Content System v2.3
	element = doc.createElement("generator");
	element.appendChild(doc.createTextNode("SetupX"));
	channel.appendChild(element);
	
	// docs	A URL that points to the documentation for the format used in the RSS file. It's probably a pointer to this page. It's for people who might stumble across an RSS file on a Web server 25 years from now and wonder what it is.	http://blogs.law.harvard.edu/tech/rss

	// cloud	Allows processes to register with a cloud to be notified of updates to the channel, implementing a lightweight publish-subscribe protocol for RSS feeds. More info here.	<cloud domain="rpc.sys.com" port="80" path="/RPC2" registerProcedure="pingMe" protocol="soap"/>
	
	// ttl	ttl stands for time to live. It's a number of minutes that indicates how long a channel can be cached before refreshing from the source. More info here.	<ttl>60</ttl>
	element= doc.createElement("ttl");
	element.appendChild(doc.createTextNode("2"));
	channel.appendChild(element);

	DateFormat formatIn = new SimpleDateFormat("yyyy-MM-dd");
	DateFormat formatOut = new SimpleDateFormat("EEE', 'dd' 'MMM' 'yyyy' 'HH:mm:ss' 'Z", Locale.US);// DateFormat.getDateInstance(DateFormat.FULL);
	Session hqlSession = CoreObject.createSession();

	
	// image	Specifies a GIF, JPEG or PNG image that can be displayed with the channel. More info here.	
	element = doc.createElement("image");
	Element element2 = doc.createElement("url");
	element2.appendChild(doc.createTextNode("pics/logo.gif"));
	element.appendChild(element2);
	
	element2= doc.createElement("title");
	element2.appendChild(doc.createTextNode("SetupX"));
	element.appendChild(element2);
	
	element2 = doc.createElement("link");
	element2.appendChild(doc.createTextNode("/"));
	
	element.appendChild(element2);

	channel.appendChild(element);

	
	// rating	The PICS rating for the channel.	

	// textInput	Specifies a text input box that can be displayed with the channel. More info here.	
	
	// skipHours	A hint for aggregators telling them which hours they can skip. More info here.	
	
	// skipDays	A hint for aggregators telling them which days they can skip. More info here.	

	
	
	

	List publicData = null;
    	String publicDataQuery = "";
    	try {
    	    publicDataQuery = "select user.displayName, data.relatedExperimentID, data.label, data.published from pubdata as data, formobject as f, user where data.relatedExperimentID = f.uoid and user.uoid = data.publishedUserID group by data.relatedExperimentID"	;
	    	
			publicData = hqlSession.createSQLQuery(publicDataQuery)
				.addScalar("displayName", Hibernate.STRING)
				.addScalar("relatedExperimentID", Hibernate.STRING)
				.addScalar("label", Hibernate.STRING)
				.addScalar("published", Hibernate.DATE)
				.list();
    	} catch (Exception e){
    	}

    	
    	if (publicData.size() > 0) {
    	    for (int xa = 0; xa < publicData.size(); xa++){
        	    Object[] objects = (Object[])publicData.get(xa);
            	String a0 = objects[0].toString(); 
            	String a1 = objects[1].toString(); 
            	String a2 = objects[2].toString(); 
            	String a3 = objects[3].toString();
            	

    			long promtID = Long.parseLong(a1);

    			PromtUserAccessRight accessRight = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID(), promtID);
    			
    		    Element item  = doc.createElement("item");
    		    channel.appendChild(item);

    		    //title	The title of the item.	Venice Film Festival Tries to Quit Sinking		
    		    element = doc.createElement("title");
    		    element.appendChild(doc.createTextNode(new SXQuery().findPromtTitleByPromtID(promtID)));
    			item.appendChild(element);
    			
    			//link	The URL of the item.	http://nytimes.com/2004/12/07FEST.html
    		    element = doc.createElement("link");
    		    element.appendChild(doc.createTextNode("pubexperiment.jsp?id=" +  promtID + ""));
    			item.appendChild(element);		
    			
			    element = doc.createElement("pubDate");
			    element.appendChild(doc.createTextNode(formatOut.format(formatIn.parse(a3))));
				item.appendChild(element);		
				
			    element = doc.createElement("description");
				element.appendChild(doc.createTextNode(Util.replace(new SXQuery().findPromtAbstractByPromtID(promtID),'\n',"<br>")));
				item.appendChild(element);		
				
				//guid	A string that uniquely identifies the item. More.	http://inessential.com/2002/09/01.php#a2
			    element = doc.createElement("guid");
			    element.appendChild(doc.createTextNode("load?id=" +  promtID + "&action=20"));
				item.appendChild(element);

    	    }
    	}
	out.clear();
	response.setContentType("text/xml");
	response.setCharacterEncoding("UTF-8");
	out.write(new XMLFile(doc).content());
	%>

            	
    	