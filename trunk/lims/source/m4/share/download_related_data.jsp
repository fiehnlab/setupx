<%@page import="org.setupx.repository.web.WebConstants"%>
<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ include file="checknonlogin.jsp"%>


<%@ include file="header_standard.jsp"%>
<%
Report report = (Report)session.getAttribute(org.setupx.repository.web.WebConstants.SESS_REPORT);
%>

<!-- MACHINE DATA -->
		<%@page import="java.text.DateFormat"%>
<%@page import="org.setupx.repository.core.communication.binbase.BBConnector"%>
<%@page import="org.setupx.repository.core.communication.export.reporting.Report"%>


<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.Config"%>
<%@page import="java.util.Date"%>


<table align="center" width="60%" cellpadding="5">	



	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<th align="left" colspan="11">Download<br><font class="small">&nbsp;&nbsp;download the result files and all other data related to this experiment...</font> </td>
		<th align="center"><img border='0' src="pics/aq_help.gif"></td>		
	</tr>

	<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td >instrument results<br><font class="small">&nbsp;&nbsp;</font> </td>
			<td align="center" style="background-color:#FFFFFF">
					<a href="download_result.jsp"><img  border='0' src="pics/go_download.gif"></a>
			<td colspan="6" style="background-color:#FFFFFF" >
					download raw data created by the instrument.
		</tr>



<!-- ANNOTATION RESULT -->
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td >annoation result<br><font class="small">&nbsp;&nbsp;annotation software</font> </td>
				<% 
				if (user.isPublic()){
					String filename = Config.DIRECTORY_RELATED + File.separator + report.getPromtID() + File.separator + report.getPromtID() + "_public.zip";
					File file = new File(filename);
					if (file.exists()){
					    %>
						<td style="background-color: white" align="center"><a href="download?<%=WebConstants.PARAM_FILENAME%>=<%=file.getName() %>&<%=WebConstants.PARAM_PROMTID%>=<%=report.getPromtID()%>"><img  border='0' src="pics/go_download.gif"></a></td>
					    <td align="left" style="background-color:#FFFFFF">
							The result dataset has been created.
					    <%
					} else {
					    %>
					    <td style="background-color: white" align="center"><img  border='0' src="pics/go_blank.gif"></td>
					    <td align="center" style="background-color:#FFFFFF">
					    	<i>There has not been any data processed.</i>
					    
					    <%
					}
				} else {
				    // user is known
				    %>
					<td align="center" style="background-color:#FFFFFF">				    
					<% boolean fileExist = BBConnector.determineBBResultsArrived(report.getPromtID()); %>
					<% if(fileExist){%> 
						<a href="load?id=<%=report.getPromtID()%>&action=<%=org.setupx.repository.core.user.access.PromtUserAccessRight.DOWNLOAD_RESULT%>"><img  border='0' src="pics/go_download.gif"></a>
					<% } else {%>
						<img  border='0' src="pics/go_blank.gif">			
					<% } %>
					<td colspan="6" style="background-color:#FFFFFF" >
					Download results created by the annotation software.
					<% if(fileExist){%> 
						The result dataset has been created on <b><%=DateFormat.getDateInstance(DateFormat.MEDIUM, java.util.Locale.US).format(new Date(BBConnector.getBBResultFile(report.getPromtID()).lastModified())) %></b>.
					<% } else {%>
						<i>There has not been any data processed.</i>
					<% } %>
					</td>
					<%
				}
			%>
				
		</tr>






<!-- XML EXPORT -->
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td width="0">MSI Export</td>
			<td width="0" style="background-color:#FFFFFF" align="center">
				<% if (report.getPromtID() > 318150){  %>
					<a href="promtxmlexport.jsp?id=<%=report.getPromtID()%>">
				<%} %>	
					<img  border='0' src="pics/go_msi.gif"></a>
			</td>
			<td colspan="6" style="background-color:#FFFFFF" >
					Creates an XML representation of the whole experiment for the exchange with any <a href="http://msi-workgroups.sourceforge.net/" target="new">MSI</a>-compliant databases.<font class="small">  (Current export is a release candidate and is only available for dedicated experiments.)</font>
					</td>
			
		</tr>




<!--  EXPERIMENTAL DESIGN -->
		<% if (report.getPromtID() > 318150){  %>
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td width="0">Experimental Design</td>
			<td width="0" style="background-color:#FFFFFF" align="center">
					<a href="report_xls.jsp?id=<%=report.getPromtID()%>">
					<img  border='0' src="pics/xls.gif"></a>
			</td>
			<td colspan="6" style="background-color:#FFFFFF" >Create an Excel Sheet containing the experimental Design.</td>
			
		</tr>
		<%} %>	
		

		
		
		<% 
		// -----------------------------------------------------------------------------------------------------------------			
		//							List of Files for Download
		// -----------------------------------------------------------------------------------------------------------------				
		
		if (false){
		%>
		
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td width="0">Download</td>
			<td align="left" colspan="7" style="background-color:#FFFFFF">
				<%
				try {
					// files related to the experiment
					File[] files = new File(Config.DIRECTORY_RELATED + File.separator + report.getPromtID() + File.separator).listFiles();
					if (files.length > 0){
					    for (int relatedFilesCount = 0; relatedFilesCount < files.length;  relatedFilesCount++){
					        File _currentFile = files[relatedFilesCount];
					        %>
					        <img  border='0' src="pics/go_download.gif">
					        <%=_currentFile.getName() %> 
							<font class ="small">created on <b><%=DateFormat.getDateInstance(DateFormat.MEDIUM, java.util.Locale.US).format(new Date(_currentFile.lastModified())) %></b></font>
							<% 
					    }
					} else {%>
					<img  border='0' src="pics/go_blank.gif">			
				<% } 
				} catch (Exception e){
					e.printStackTrace();
				}
				%>
			</td>
			<td >
			</td>
		</tr>

		<%} %>


</table>
<%@ include file="footer.jsp"%>		