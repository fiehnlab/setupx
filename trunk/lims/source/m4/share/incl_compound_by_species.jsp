<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>


<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<table>

<%
Iterator ids = new SXQuery().findPromtIDbySpecies(speciesID).iterator();

StringBuffer query = new StringBuffer("select a.label as label from pubsample as sample, pubdata as d, pubattribute as a ");
query.append("where d.uoid = sample.parent and a.parent = sample.uoid ");
boolean idsAvaialable = false;
if (ids.hasNext()) {
    idsAvaialable = true; 
    query.append("and (");
}

if (! ids.hasNext()) {
    query.append("and d.relatedExperimentID = \"0\" " );
}

while(ids.hasNext()){
    query.append("d.relatedExperimentID = " + ids.next() + " " );
	if (ids.hasNext()) query.append(" or ");    
}

if (idsAvaialable){
    query.append(") ");
}
query.append(" group by a.label order by a.label");

Logger.debug(this, "query: " + query.toString());

Session ses = CoreObject.createSession();
List lX=  ses.createSQLQuery(query.toString()).addScalar("label", Hibernate.STRING).list();

Iterator compoundsIter = lX.iterator();

while (compoundsIter.hasNext()){
    String cName = compoundsIter.next().toString();
    %>
<tr>
	<td><a href="advancedquery_dev.jsp?term=<%=cName%>&type=1"><img src='pics/arrow-icon.gif' border="0" valign="middle" align="center"></a> 
    <td> <%=cName %>
    <%
}

if (lX.size() < 1){
    %>
    <tr>
	<td>Up to now no compounds have been made publicly available for this species.  
    <td><img src='pics/locked.jpg' border="0" valign="middle" align="center"></a>
    <%
}
%>
</table>