<%@page
	import="org.setupx.repository.core.communication.importing.logfile.ScannedPair"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>

<%

			Vector labelVector = new Vector();

			int promtID = Integer.parseInt(request.getParameter("id"));

			
            // get all the samples for that specific class ...
            Iterator sampleIDs = new SXQuery().findSampleIDsByPromtID(promtID).iterator();
            
            while (sampleIDs.hasNext()){
                int sampleID = Integer.parseInt(sampleIDs.next() + "");
                
                try {
                    // find the latest run for that one
                    String runLabel = new SXQuery().findScannedPair(sampleID).get(0) + "";
                    
            		List list = new SXQuery().findScannedPair(sampleID);
                    Iterator scannedPairs = list.iterator();
                    boolean remove = false;
                    while (scannedPairs.hasNext()){
                        String acqname = (String) scannedPairs.next();
                        

                        Logger.debug(this, sampleID + " " + acqname + " " + remove);
                        
                        if (remove){
                            // and assign them to the class with the groupID
                			request.getSession().setAttribute("" + acqname, -1 + "");
                            Logger.debug(this, "removing: " + "" + acqname + " " + -1);
                        }

                        if (scannedPairs.hasNext()) remove = true;
					}
                }catch (Exception e){
                    e.printStackTrace();
                }
            } 

%>

<%@page import="java.util.Vector"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<meta http-equiv="refresh" content="1; URL=exp2_regroup.jsp?id=<%=promtID%>">