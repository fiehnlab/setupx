<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>




<%

// ?userID=85&att='admin'&value='true'
        
long userID = Long.parseLong(request.getParameter("userID"));

String attribute = request.getParameter("att");

String value = request.getParameter("value");
boolean booleanValue = Boolean.valueOf(value).booleanValue();

try {
    UserDO modUser = (UserDO)UserDO.persistence_loadByID(UserDO.class, CoreObject.createSession(), userID);
    Logger.log(this, "changing userattribute of user: " + modUser.toString());
    Logger.log(this, "attribute: "  + attribute );
    Logger.log(this, "value: "  + value + " (" + booleanValue + ")");
    
    
	if (attribute.compareTo("admin") == 0){
		modUser.setAdmin(booleanValue);
	} else if (attribute.compareTo("lab") == 0){
		modUser.setLabtechician(booleanValue);
	}
	
	modUser.update(false);
} catch (Exception e){
    e.printStackTrace();
}
%>


<meta http-equiv="refresh" content="1; URL=user.jsp">