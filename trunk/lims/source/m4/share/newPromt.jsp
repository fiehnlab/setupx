<link href="stylesheet.css" rel="stylesheet" type="text/css"/	>
<%@ include file="checklogin.jsp"%>

<table align="center" width="60%" cellpadding="10">
	<tr>
		<td rowspan="21">
			<img src="pics/logo.gif">
		<td>
			<h4>Your new experiment</h4>
		<td rowspan="21">
			<img src="pics/BiocratesNL.jpg">
	<tr>
		<td>
			<p>We created a new blank experiment for you. Please fill in as much information possible so that we have a detailed description of your experiment.
			<p>Your experiment will be saved every from time to time in order to make sure that your experiment data is stored even if you have to close your browser application.
			<p>If you have any questions, we are happy to help you. We continuously improve <%=org.setupx.repository.Config.SYSTEM_NAME%> to match study designs and to include query functions. We rely on your feedback!
	</tr>
	<tr>
		<td>
			Start editing your experiment.<br>
			<a href="form.jsp"><img src="pics/go.gif" border="0"></a>
</table>

