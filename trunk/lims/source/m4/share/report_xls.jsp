<%@page import="java.util.HashSet"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.DownloadServlet"%>
<%@page import="org.setupx.repository.core.communication.export.FileExport"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.core.communication.export.xls.XLSFileExport"%>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<%@ include file="checknonlogin.jsp"%>

<% 
try {
	long promtID = Integer.parseInt("" + request.getParameter("id"));

	File targetFile = FileExport.createFileName(promtID, "xls");
	
	if (! targetFile.exists()){
		Promt promt = Promt.load(promtID, true);

		try {
			FileExport fileExport = new XLSFileExport(targetFile);
			fileExport.export(promt);
			fileExport.createExportFile();
			//pageContext.forward(fileExport.createDownloadLink());
			String link_ = fileExport.createDownloadLink();
		    %>
		    <meta http-equiv="refresh" content="1; URL=<%=link_%>">
		    <%
		} catch (Exception e){
			//pageContext.forward(fileExport.createDownloadLink());
		    %>
		    <meta http-equiv="refresh" content="1; URL=pubexperiment_extended.jsp?id=<%=promtID %>">
		    <%

		}
	} else {
	    %><meta http-equiv="refresh" content="1; URL=<%="download?" + WebConstants.PARAM_FILENAME + "=" + targetFile.getName() + "&" + WebConstants.PARAM_PROMTID + "=" + promtID %>"><%
	}
} catch (Exception e){
	//pageContext.forward(fileExport.createDownloadLink());
    %>
    <meta http-equiv="refresh" content="0; URL=pubexperiment_extended.jsp?id=<%=request.getParameter("id") %>">
    <%

}
 %>
 <table cellpadding="30" cellspacing="50" align="center" >
 	<tr>
 		<td>
 			<a href="main_public.jsp"><img src="pics/back.gif" border="0"></a>
 		</td>
 	</tr>
 </table>
