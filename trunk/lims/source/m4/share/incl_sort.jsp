<%@page import="org.hibernate.Criteria"%>
<%@page import="java.util.Date"%>
<%@page import="org.tempuri.DataSet"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Order"%>
<%
String order = "id";    
try {
    order = request.getParameter("order");        
    if (order == null || order.length() < 2 || order.compareTo("null") == 0) throw new Exception();
} catch (Exception e){
    order = "id";    
}

String upDown;
try {
    upDown = request.getParameter("se");        
    if (upDown == null || upDown.length() < 2 || upDown.compareTo("null") == 0) throw new Exception();
} catch (Exception e){
    upDown = "asc";    
}

Order theOrder;
if (upDown.compareTo("asc") == 0 ){
    theOrder = Order.asc(order);
    downUp = "desc";
} else {
    theOrder = Order.desc(order);
    downUp = "asc";
}

Criteria criteria = hqlSession.createCriteria(c).addOrder(theOrder);
//hqlSession.close();
%>