<%@page import="java.util.HashSet"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.Vector"%>
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
<%

	Vector knownVectors = new Vector();

	String _c_query = "select nameIn.value as n, emailIn.value as e from formobject as uIn, formobject as nameIn, formobject as emailIn " 
		+ " where uIn.discriminator like \"%user%\" " 
		+ " and nameIn.PARENT = uIn.uoid and emailIn.PARENT = uIn.uoid and nameIn.value != \"\" "
		+ " and emailIn.question = \"eMail-Address\" and (nameIn.question = \"Firstname and Lastname\" or nameIn.question = \"Name\") " 
		+ " and nameIn.value not like \"%unknown%\" and root(uIn.uoid) = " + promtID;
	
	Iterator _user_names_Iter = CoreObject.createSession().createSQLQuery(_c_query).addScalar("n", Hibernate.STRING).addScalar("e", Hibernate.STRING).list().iterator();
	
	while (_user_names_Iter.hasNext()){
	Object[] _as_objects = (Object[])_user_names_Iter.next();
	String __realName = "" + _as_objects[0];
	if (__realName.indexOf('(') > 0){
	    __realName = __realName.substring(0, __realName.indexOf('('));
	}

		if (!knownVectors.contains(__realName)){
			knownVectors.add(__realName);
		%>
			<td valign="middle">
				<a href="mailto:<%=_as_objects[1]%>">
					<img src='pics/mail.gif' border="0" valign="middle" align="center">
				</a>
			<td>
			&nbsp;<%=__realName%>,&nbsp;<%
		}
	}

	HashSet users = new SXQuery().findPromtCollaboration(promtID);

	Iterator iterator = users.iterator();
	while(iterator.hasNext()){
	    UserDO userDO = (UserDO)iterator.next();
	    if (userDO.getUsername().compareTo("public") != 0 ){
	    	if (!knownVectors.contains(userDO.getDisplayName())){
		    %>
				<td valign="middle">
				<a href="mailto:<%=userDO.getMailAddress().getEmailAddress()%>">
			    	<img src='pics/mail.gif' border="0" valign="middle" align="center">
			    </a>
			    <td>
			    &nbsp;<%=userDO.getDisplayName()%>,&nbsp;<%
			}
	    }
	}

%>
</table>