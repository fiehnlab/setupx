<%@ include file="checknonlogin.jsp"%>

<%@ page import="java.util.*,
                 org.setupx.repository.core.communication.importing.metadata.*,
                 org.setupx.repository.server.persistence.*"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<body>
<% 

int ID = Integer.parseInt(request.getParameter("id"));

org.setupx.repository.core.communication.importing.metadata.Data data = null;
try {
    data =  org.setupx.repository.core.communication.importing.metadata.CustomerMetaDataRetrieval.querySample(ID);
} catch (Exception e){
    e.printStackTrace();
}

int clazz = new SXQuery().findClazzIDBySampleID(ID);
int promt = new SXQuery().findPromtIDbyClazz(clazz);
try {
    int access = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID(), promt).getAccessCode();
    if (access < 20){
		throw new PersistenceActionFindException("The user has insufficient right not access this information.");
    }
} catch (Exception exception){
    request.getSession().setAttribute(org.setupx.repository.web.WebConstants.SESS_FORM_MESSAGE, exception.getMessage());
    %>
    <jsp:forward page="error.jsp"/>
    <% 
}

List s = new SXQuery().findSampleIDsByClazzID(clazz);
%>


	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Sample <%=ID %></h2> 
			Report for sample containing Labinformation and Biological Background<br/>
			<font class="small">Report also available in XML for MSI reporting.<br><a href="msi_sample_xml.jsp?id=<%=ID %>"> <img src="pics/xml3.gif"></a></font>
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>


	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} %>
	











<%
	boolean running = new SXQuery().determineSampleRun(ID);
	boolean scheduled = new SXQuery().determineSampleScheduled(ID);
 %>







<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>


	<tr>
		<th align="left" colspan="11">Lab Information: <br><font class="small">&nbsp;&nbsp;information related to this sample stored in the LIMS</font> </th>
		<th align="center"><img border='0' src="pics/aq_help.gif"></th>		
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">Label<br><font class="small">&nbsp;&nbsp;the label that was printed on the sample</font> </td>
		<td style="background-color:white;" colspan="6"><%=new SXQuery().findSampleLabelBySampleID(ID)%></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>

	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">sheduled: <br><font class="small">&nbsp;&nbsp;has the sample been sheduled to be run yet</font> </td>
		<td colspan="6" style="background-color:#FFFFFF" ><table>
				<tr>
					<td style="background-color:#FFFFFF" align="center" ><font class="small">yes</font>
					<td style="background-color:#FFFFFF" align="center" ><font class="small">no</font>
				<tr>
			<% if(scheduled){%> 
				<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/checked.jpg"></a>
				<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/blank.jpg"></a>			
				<td style="background-color:#FFFFFF" align="center" ><%=new SXQuery().findAcquisitionNameBySampleID(ID)%> 
			<%} else {%>
				<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/blank.jpg"></a>			
				<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/checked.jpg"></a>
			<%}%>
			</tr></table>
		</td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">run: <br><font class="small">&nbsp;&nbsp;did the sample actually run yet</font> </td>
		<td colspan="6" style="background-color:#FFFFFF" >
			<table>
				<tr>
					<td style="background-color:#FFFFFF" align="center" ><font class="small">yes</font>
					<td style="background-color:#FFFFFF" align="center" ><font class="small">no</font>
				<tr>
			<%List allRuns = new SXQuery().findScannedPair(ID);
				if (allRuns.size() > 0){
				    %>
						<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/checked.jpg"></a>
						<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/blank.jpg"></a>			
						<td colspan="4" style="background-color:#FFFFFF" >
				    	<font class="small">The sample was run <%=allRuns.size()%> times.<br>
				    	<table width="100%">
				    	<% Iterator iterator = allRuns.iterator();
				    		while(iterator.hasNext()){%>
				    		    <tr>
					    		    <td style="background-color:#FFFFFF" align="center"><%=iterator.next()%></td>
				    		    </tr>
				    		<% }%>
				    	</table>
				    	</td>
				    <% 
				} else {
				    %>
						<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/blank.jpg"></a>			
						<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/checked.jpg"></a>
						<td colspan="4" style="background-color:#FFFFFF" >
				    	</td>
				    
				    
				    
				    <%
				}
			
			// possible unrecognized samples 
			Vector possibleMatches = new Vector();
//			possibleMatches.add("abc");
			
			// find all files that match 
			// -a scheduled sample
			// -the base of an older sample
			// -...
			
			if (user.isLabtechician()){
				String _acqname = new SXQuery().findAcquisitionNameBySampleID(ID);
				
	            _acqname = _acqname.replace(':','_');
				
	            String acqpart = _acqname.substring(0, _acqname.indexOf('_'));
				
	            for (int counter = 0; counter < 5; counter++){
	                SampleFile sampleFile = new SampleFile(acqpart + ":" + counter);
	    			boolean exists = sampleFile.exists(SampleFile.TXT);  
	    			if (	exists 
	    			        && !allRuns.contains(sampleFile.getACQName().replace(':','_')) 
	    			    	&& !allRuns.contains(sampleFile.getACQName().replace('_',':'))){
    			    	possibleMatches.add(sampleFile.getACQName());
	    			}
	    			    	
	            }
				
				
				
				if (possibleMatches.size() > 0){
				    
				    // possible match
				  	%>
				  		<tr>
				  			<td>
				  			<td>
							<td colspan="4" style="background-color:#FFFFFF" >
					    	<font class="small">Possible matches
					<%
						Iterator possMatchIterator = possibleMatches.iterator();
						while (possMatchIterator.hasNext()){
						    String possMatch = possMatchIterator.next() + "";
							
							%>	
						  		<tr>
									<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/checked.jpg"></a>
									<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/blank.jpg"></a>			
									<td colspan="4" style="background-color:#FFFFFF" align="center" >
									<%=possMatch %>
									
									<font class="small">
										<a href="assign_sample2.jsp?sampleID=<%=ID %>&filename=<%=possMatch %>">
											assign this file as an acutal run 
										</a>
						    		</font>
						    <%
						}
				}
			    
			}
			
			%>
			
			
			
			
			
				    	</table>
		</td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">collaborations<br><font class="small">&nbsp;&nbsp;</font> </td>
		<td style="background-color:white;" colspan="6"><%=new SXQuery().findPromtCollaborationHTML(promt)%></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>

	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">progress<br><font class="small">&nbsp;&nbsp;progress of this experiment</font> </td>
		<td colspan="6" style="background-color:white;" >
				<% SXQueryCacheObject queryCacheObject = SXQueryCacheObject.findByExperimentID(promt); %>
				<table width="100%"><tr><td><%@ include file="incl_processbar.jsp"%> </table> 
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
			
	<!--  link to results -->
	<%
	String pubQuery = "select d.published as date, d.publishedUserID as userID, sample.uoid as sampleID, d.label as label  from pubsample as sample, pubdata as d where sample.setupXsampleID = " + ID + " and d.uoid = sample.parent";
	List pubList = CoreObject.createSession()
					.createSQLQuery(pubQuery)
					.addScalar("date", Hibernate.DATE)
					.addScalar("userID", Hibernate.LONG)
					.addScalar("sampleID", Hibernate.LONG)
					.addScalar("label", Hibernate.STRING)
					.list();
	%>

	<!--  link to results -->
	<%
	String compounds = "select a.label as label, a.comment, a.value, sample.uoid as compoundID from pubsample as sample, pubdata as d, pubattribute as a where sample.setupXsampleID = " + ID + " and d.uoid = sample.parent 	    and a.parent = sample.uoid 	    order by a.label ";
	List compList = CoreObject.createSession()
					.createSQLQuery(compounds)
					.addScalar("label", Hibernate.STRING)
					.addScalar("comment", Hibernate.STRING)
					.addScalar("value", Hibernate.STRING)
					.addScalar("compoundID", Hibernate.LONG)
					.list();
	if (compList.size() > 0){
	    %>
		<tr>
			<td style="background-color:#6682B6" width="20" rowspan="2"></td>
			<td rowspan="2" colspan="4">substances<br><font class="small">&nbsp;&nbsp;substances detected in this sample and published in datasets</font> </td>
			<td colspan="6" style="background-color:white;" >		
				<table width="100%">
					<tr>
						<td rowspan="21" style="background-color:white; " valign="top" align="center">
							<img  border='0' src="pics/shared.gif">
						<th>link to data
						<th>
				
				<%
				boolean showCompound = Boolean.valueOf(request.getParameter("showCompound")).booleanValue();

				for (int pl = 0; ( pl < 5 || showCompound) && pl < compList.size(); pl++){
				    Object[] objects = (Object[])compList.get(pl);
				    %>
					<tr>
						<td><a href="pubsample.jsp?uoid=<%=objects[3] %>"><%=objects[0] %></a>
						<td>
							<font class="small">
								<a href="pubsample.jsp?uoid=<%=objects[3] %>"><%=objects[2] %></a> &nbsp; 
								<a href="pubsample.jsp?uoid=<%=objects[3] %>"><%=objects[3] %></a>
							</font>
				    <%
				}
				if (!showCompound && compList.size() > 5){
				%>
					<tr>
						<td colspan="2"><font class="small"><a href="?id=<%= ID %>&showCompound=true"> show all <%=compList.size() %> compounds.</a> </font>
				<%} %>

			</table>
			<td style="background-color:#6682B6" width="20"></td>
			</tr>
			<tr>
				<td colspan="6" style="background-color:white;" >
				
			<%
		if (pubList.size() > 0){
	    %>
				<table width="100%">
						<th colspan="2">Datasets published for this sample.
				<%
				showCompound = Boolean.valueOf(request.getParameter("showCompound")).booleanValue();

				for (int pl = 0; ( pl < 5 || showCompound) && pl < pubList.size(); pl++){
				    Object[] objects = (Object[])pubList.get(pl);
				    %>
					<tr>
						<td align="center"><%=objects[3] %>
						<td>by <%=((UserDO)UserDO.persistence_loadByID(UserDO.class, DynamicStatus.createSession(), Long.parseLong(objects[1].toString()))).getDisplay()%>
						<br><font class="small"> on <%=((Date)objects[0]).toString() %></font>
				    <%
				}  
				if (!showCompound && pubList.size() > 5){
				%>
					<tr>
						<td colspan="2"><font class="small"><a href="?id=<%= ID %>&showCompound=true"> show all <%=pubList.size() %> public datasets.</a> </font>
				<%} %>
			</table>
			<td style="background-color:#6682B6" width="20"></td>
		</tr>
	    <%
	}
		%>
	    <%
	}
	%>




	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">files<br><font class="small">&nbsp;&nbsp;Files created be the connected instrument. To download the actual files use the link below.</font> </td>
		<td colspan="6" align="left" style="background-color:white;" >
				<table align="left">
				<tr>
					<%
					for (int _i = 0; _i < SampleFile.TYPES.length; _i++){
					    short type = SampleFile.TYPES[_i];
					    String ending = SampleFile.getExtension((short)_i);
					    %>
						<th width="25"><%=ending %></th>
					<% 
					}
					%>
					<th ></th>
				</tr>
				<tr>
				<%
				SampleFile sampleFile = null;
				boolean finished = true;
				try {
				    sampleFile = new SampleFile(ID);
				    Logger.log(this, "finished run as " + sampleFile);
				} catch (Exception e){
				    finished = false;
				}
				for (int _i = 0; _i < SampleFile.TYPES.length; _i++){
				    short type = SampleFile.TYPES[_i];
				    
				    %>
				    <td align="center" valign="top">
						<% 
						if(finished && sampleFile.exists(type)){ %>
							<img  border='0' src="pics/checked.jpg" title="<%=sampleFile.getFile(type).getName()%>">
							<br>
							<a href="download.jsp?id=<%=ID%>&type=<%=sampleFile.getExtension(type)%>">
								<img  border='0' src="pics/icon_download.gif" title="<%=sampleFile.getExtension(type)%>">
							</a>
						<%} else {%>
							<img  border='0' src="pics/x-amber.jpg" title="missing file">
						<%} %>
					</td>
				    <%			    
				}
				%>
					<td>Each of the downloads contains the data for this sample. <br><font class="small">You can also download the data for the entire experiment by opening <a href="load?id=<%=promt %>&action=20">the experiment view</a> and select your prefered filetype.</font>
				</tr>						
				</table>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	


	
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>


	
	<tr>
		<th align="left" colspan="11">Biological information<br><font class="small">&nbsp;&nbsp;information stored about the orign of this sample</font> </th>
		<th align="center"><img border='0' src="pics/aq_help.gif"></th>		
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">Species<br><font class="small">&nbsp;&nbsp;</font> </td>
		<td colspan="6" style="background-color:white;" ><%=new SXQuery().findSpeciesbyPromtID(promt) %>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>

<% 
	Sample sample = Sample.loadByID(ID);
	String label_value = "";
	String label_key = "";
	
	for (int mySampleCounter = 0; mySampleCounter < sample.getFields().length ; mySampleCounter++){
	    if (sample.getField(mySampleCounter) instanceof StringInputfield){
	        label_key = "" +  ((StringInputfield)sample.getField(mySampleCounter)).getQuestion();
	        label_value = "" + ((StringInputfield)sample.getField(mySampleCounter)).getValue();
	        if (label_value.length() > 0){
	        %>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">
			<%=label_key %>:
			<br><font class="small">&nbsp;&nbsp;<%=((StringInputfield)sample.getField(mySampleCounter)).getDescription() %></font> 
			<%if (((StringInputfield)sample.getField(mySampleCounter)).getMSIAttribute() != null){%>
				<font style="font-size: xx-small;">MSI-Attribute</font>
			<%} %>
		</td>
		<td colspan="6" style="background-color:white" >
			<%=label_value %>:&nbsp
			
			<%
			try {
				if (((StringInputfield)sample.getField(mySampleCounter)).getMSIAttribute() != null){%>
					<%=(((StringInputfield)sample.getField(mySampleCounter)).getMSIAttribute().getExtension())%>
				<%}
			} catch (Exception e){
				Logger.err(this, "can not find extension for msi attribute.");
			}%>
			<%	/*
			if (((StringInputfield)sample.getField(mySampleCounter)).getDescription().compareTo("custom field") == 0) {%><br><font class="small">This attribute is added as additional metadata. It is not part of the standard requierement for study design in <%=org.setupx.repository.Config.SYSTEM_NAME%>.</font><%} 
			*/
			%>
		
		</td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	        <%
	        }
	    }
	}
%>	



	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>


	<tr>
		<th align="left" colspan="11">Class information<br><font class="small">&nbsp;&nbsp;information about the class (set of replicates)</font> </th>
		<th align="center"><img border='0' src="pics/aq_help.gif"></th>		
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">ClassID:<br><font class="small">&nbsp;&nbsp;id for this class</font> </td>
		<td style="background-color:white;" colspan="6" ><%=new SXQuery().findClazzIDBySampleID(ID) %></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">description <br><font class="small">&nbsp;&nbsp;description for this class</font> </td>
		<td colspan="6" style="background-color:white;"  ><%=Util.replace(  new SXQuery().findClassInformationString(new SXQuery().findClazzIDBySampleID(ID)), "\n", "<p>")%></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	
	<% if (user.isLabTechnician()){ %>
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td colspan="4">Move<br><font class="small">&nbsp;&nbsp;move sample</font> </td>
			<td colspan="6" style="background-color:white;"  >In case the sample is located in the wrong class you can move it to another class.</td>
			<td style="background-color:#6682B6" width="20"></td>
		</tr>	
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td colspan="4"></td>
			<td colspan="6" style="background-color:white;"  >
							<%@ include file="incl_move_sample.jsp"%>
			<td style="background-color:#6682B6" width="20"></td>
		</tr>	
	<% }%>
	




	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>


	<tr>
		<th align="left" colspan="11">Experimental Design<br><font class="small">&nbsp;&nbsp;information about the whole experiment</font> </th>
		<th align="center"><img border='0' src="pics/aq_help.gif"></th>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">ID:<br><font class="small">&nbsp;&nbsp;id for this class</font> </td>
		<td style="background-color:white;"  colspan="6"><%=promt %></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">Title:<br><font class="small">&nbsp;&nbsp;Title of this experiment</font> </td>
		<td style="background-color:white;"  colspan="5"><%=new SXQuery().findPromtTitleByPromtID(promt)%></td>
		<td align="center" style="background-color:#FFFFFF"><a href="load?id=<%=promt%>&action=20"><img  border='0' src="pics/go.gif"></a>	
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">abstract<br><font class="small">&nbsp;&nbsp;Abstract for this experiment</font> </td>
		<%
			String myabstract = new SXQuery().findPromtAbstractByPromtID(promt);
			myabstract = Util.replace( myabstract, "\n", "<p>");
		
		%>

		<td style="background-color:white;"  colspan="6"><%=myabstract%></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">design<br><font class="small">&nbsp;&nbsp;experimental design of this experiment</font> </td>
		<td style="background-color:white;"  colspan="6">
				<% int promtID = promt; %>
				<%@ include file="incl_experiment_structure.jsp"%>
		</td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	









<%
	try {
		String acqname = "" + allRuns.get(0);
		acqname = acqname.substring(0, acqname.indexOf(":"));
		LecoACQFile lecofile = LecoACQFile.determineFile(acqname);
		Hashtable ht = lecofile.getValuesPerACQname(acqname);
	%>


	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>


	<tr>
		<th align="left" colspan="11">run details
			<br>
			<font class="small">&nbsp;&nbsp;Information for the actual run on the GC
			&nbsp;&nbsp;The data has been retrieved from 
				<%=lecofile.sourceFile.getName() %>.
			<br>&nbsp;&nbsp;File was modified on <%=new Date(lecofile.sourceFile.lastModified()).toLocaleString()%>. 
			<br><br><img border='0' src="pics/warning.gif">Use data with care - due to version changes of the GC there can be missmappings.
				
				</font> </th>
				
		<th align="center"><img border='0' src="pics/aq_help.gif"></th>		
	</tr>


		<% 
		
		Enumeration enumeration = ht.keys();
		while(enumeration.hasMoreElements()) { 
			String key = (String)enumeration.nextElement();
			String value = (String)ht.get(key);		
			value = value.replace('\\',' ');
			if (key.length() > 0 && value.length()>0){%>
			<tr>
				<td style="background-color:#6682B6" width="20"></td>
				<td colspan="4"><%=key%><br><font class="small">&nbsp;&nbsp;</font> </td>
				<td style="background-color:white;"  colspan="6"><%=value%></td>
				<td style="background-color:#6682B6" width="20"></td>
			</tr>
			<%} 
			} 
		}
	catch (Exception e){
	    e.printStackTrace();
	    
	}
	%>

	
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	
	
	<tr>
		<th align="left" colspan="11"><a id="hist">Sample History<br><font class="small">&nbsp;&nbsp;tracking information</font> </th>
		<th align="center"><img border='0' src="pics/aq_help.gif"></th>
	</tr>
	
	
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="10">
			<table width="100%">
				<%@ include file="sample_status_list.jsp"%>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>


	<% if(user.isLabTechnician()){ %>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">Status<br><font class="small">&nbsp;&nbsp;change status of this sample</font> </td>
		<td style="background-color:white;"  colspan="6"><a href="sample_status_create.jsp?id=<%=sampleID%>"><img  border='0' src="pics/go_track.gif"></a>	
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<%} %>
	




	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td width="50%"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
</table>









<%@ include file="footer.jsp"%>
</body>
<%@page import="org.setupx.repository.core.communication.importing.logfile.ScannedPair"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>
<%@page import="org.setupx.repository.web.forms.inputfield.StringInputfield"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.core.communication.leco.LecoACQFile"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.communication.file.SampleFile"%>
</html>
