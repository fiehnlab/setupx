<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.web.forms.inputfield.StringInputfield"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>

<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.Config"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<%@include file="incl_xls_header.jsp"%>

<table align='center' border='0' cellpadding='4' width="60%" border = 1 >
<tr>	
<%
	Session mySession = CoreObject.createSession();
	long tmpPromtID = 0L;
	long promtID = 0L;
//	Promt promt = null;
	Vector rows = (Vector)session.getAttribute("data"); 
    Vector labels = (Vector)session.getAttribute("labels"); 
    Hashtable relations = (Hashtable) session.getAttribute("relations"); 

    if (rows.size() == 0 || labels.size() == 0 || relations.size() == 0 ) throw new Exception("not all data has been assigned correctly.");
    
    Enumeration keys = relations.keys();
    try {

	    while (keys.hasMoreElements()){
	        Object key = keys.nextElement();
	        long sampleID = Long.parseLong(""+key);
	        Logger.log(this, "SampleID: " + sampleID );

	        promtID = new SXQuery().findPromtIDbySample(sampleID);
	        Logger.log(this, "promtID: " + promtID);
	        /*
	        if (tmpPromtID != promtID){
	            tmpPromtID = promtID;
	            
	            // load the promt
	            promt = Promt.load(promtID, true);
	        }
	        */
	        
	        try {  
	      	  Vector row = (Vector) rows.get(Integer.parseInt("" + relations.get(key)));
	          //int clazz = new SXQuery().findClazzIDBySampleID(Integer.parseInt(""+key));
	          
	          for (int i = 0; i < row.size(); i++){
	              Object value = row.get(i);
	              StringInputfield newField = new StringInputfield("" + labels.get(i),"custom field");
				  newField.setSize_x(8);
				  newField.setRestriction(null);
				  newField.setRequiered(false);
	              newField.setValue(value);
	              newField.setLongDescription("this is a custom field beeing added from the xls import");

				  newField.update(true);
				  long childID = newField.getUOID();
				  Logger.log(this, "id: " + childID);
				  
				  // update parent id of newField
				  int newPos = Integer.parseInt(mySession.createSQLQuery("select max(internalPosition) as m from formobject where parent = " + sampleID).addScalar("m", Hibernate.STRING).list().get(0).toString()) + 1; 
				  Logger.log(this, "new Position: " + newPos);
				  
				  String hqlUpdate = "update FormObject c set c.parent = :parent, c.internalPosition = :internalPosition where c = :uoid";
				  int updatedEntities = mySession.createQuery(hqlUpdate).setString("parent", sampleID + "").setString("internalPosition", newPos + "").setString("uoid", childID + "").executeUpdate();

				  Logger.log(this, "performed update.");


	          }
	        } catch (Exception e){
			   e.printStackTrace();            
	        }
		}
	    %>
	    		  	<tr>
			    <th>Data has been merged - you will be forwarded to the <a href="load?id=<%=promtID%>&action=40">edit mode</a>.</th>
			</tr>
	</table>
	    <meta http-equiv="refresh" content="4; URL=load?id=<%=promtID%>&action=40">
	
	    <%

//    	promt.update(true);
	    } catch (Exception e){
	        %>
	    		<th colspan="21">Error assigning date to samples.
	<tr>	
		<td colspan="21">
					The data can not be combined. (Error: <%=e.getMessage() %>
					Please contact the <a href="mailto:<%=Config.SYSTEM_EMAIL %>">admin</a>.
	
	<% }%>


	</table>
<%@include file="incl_xls_footer.jsp"%>
</body>
