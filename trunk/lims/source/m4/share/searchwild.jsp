<%@ include file="checklogin.jsp"%>


<%@page import="java.util.List"%>
<%@page import="java.util.Vector"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>
<%@ page import="org.setupx.repository.server.persistence.SXQuery" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%
String searchTerm = request.getParameter("wildsearch");

// remove starting and ending space characters
boolean loopStart = true;
boolean loopEnd = true;
while (loopStart){
    
    if (searchTerm.charAt(0)==' '){
        searchTerm = searchTerm.substring(1, searchTerm.length());
    }else {
        loopStart = false;
    }
}
while (loopEnd){
    
    if (searchTerm.charAt(searchTerm.length()-1)==' '){
        searchTerm = searchTerm.substring(0, searchTerm.length()-1);
    }else {
        loopEnd = false;
    }
}

%>

<body>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Search Result for <%=searchTerm%></h2></th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>
	
	
<%@ include file="navi_admin.jsp"%>

	

<table align='center' border='0' cellpadding='4' width="60%">


<tr>
<th></th>
<th>Type</th>
<th>ID</th>
<th>Info</th>
<th>load Sample</th>
<th>load Experiment</th>
<th></th>
</tr>

	
<% 
int sampleID = 0;
int promtID = 0;
String _title = "";
String _abstract = "";


%>



<%

// ------------------
// find promt by ID 
// ------------------

try {
    promtID = new org.setupx.repository.server.persistence.SXQuery().findPromtIDbySample(Integer.parseInt(searchTerm));
    %>
	<tr>
		<td></td>
	    <td><b>Sample</b></td>
	    <td><%=searchTerm%></td>
		<td>
		<%
			_title = new SXQuery().findPromtTitleByPromtID(promtID);
			_abstract = new SXQuery().findPromtAbstractByPromtID(promtID);
			 %>
			 <b><%=_title %></b>[<%=promtID %>]<br><%=_abstract%>
		</td>
	    <td><a href="sample_detail.jsp?id=<%=searchTerm %>"><img  border='0' src="pics/go.gif"></a></td>
	    <td><a href="load?id=<%=promtID %>&action=<%=org.setupx.repository.core.user.access.PromtUserAccessRight.READ%>"><img  border='0' src="pics/go.gif"></a></td>

		<td></td>
	</tr>
    <% 
} catch (Exception e1){
    // dont do stuff
}

//------------------
//find promt by name 
//------------------
try {
	sampleID = new SXQuery().findSampleIDByAcquisitionName(searchTerm); 
    promtID = new org.setupx.repository.server.persistence.SXQuery().findPromtIDbySample(sampleID);
    %>
	<tr>
		<td></td>
	    <td><b>Sample</b></td>
	    <td><%=searchTerm%></td>
		<td>
		<%
			_title = new SXQuery().findPromtTitleByPromtID(promtID);
			_abstract = new SXQuery().findPromtAbstractByPromtID(promtID);
			 %>
			 <b><%=_title %></b>[<%=promtID %>]<br><%=_abstract%>
		</td>
	    <td><a href="sample_detail.jsp?id=<%=sampleID %>"><img  border='0' src="pics/go.gif"></a></td>
	    <td><a href="load?id=<%=promtID %>&action=<%=org.setupx.repository.core.user.access.PromtUserAccessRight.READ%>"><img  border='0' src="pics/go.gif"></a></td>

		<td></td>
	</tr>
    <% 
} catch (Exception e2){

}

try {
	promtID = new SXQuery().findPromtIDbyClazz(Integer.parseInt(searchTerm));
    %>
	<tr>
		<td></td>
	    <td><b>Class</b></td>
	    <td><%=searchTerm%></td>
		<td>
		<%
			_title = new SXQuery().findPromtTitleByPromtID(promtID);
			_abstract = new SXQuery().findPromtAbstractByPromtID(promtID);
			 %>
			 <b><%=_title %></b>[<%=promtID %>]<br><%=_abstract%>
		</td>
	    <td><img  border='0' src="pics/go_blank.gif"></td>
	    <td><a href="load?id=<%=promtID %>&action=<%=org.setupx.repository.core.user.access.PromtUserAccessRight.READ%>"><img  border='0' src="pics/go.gif"></a></td>
		<td></td>
	</tr>
<% 
} catch (Exception e3){
}


//------------------
//find sample 
//------------------

try {
	_title = new SXQuery().findPromtTitleByPromtID(Integer.parseInt(searchTerm));
	_abstract = new SXQuery().findPromtAbstractByPromtID(Integer.parseInt(searchTerm));
	if (_title.compareTo("-unknown-") == 0) throw new Exception("not found");
	
    %>
			<tr>
				<td></td>
			    <td><b>Experiment</b></td>
			    <td><%=searchTerm %></td>
				<td><b><%=_title %></b>[<%=searchTerm%>]<br><%=_abstract%>
				</td>
			    <td><img  border='0' src="pics/go_blank.gif"></td>
			    <td><a href="load?id=<%=searchTerm%>&action=<%=org.setupx.repository.core.user.access.PromtUserAccessRight.READ%>"><img  border='0' src="pics/go.gif"></a></td>
				<td></td>
			</tr>
<% 
} catch (Exception e4){
}
%>



<% 
org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

String basic  = "select promt.uoid from formobject as promt, " + 
	"formobject as page, " + 
	"formobject as element1 , " + 
	"formobject as element2 " + 
	"where promt.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.Promt\" " + 
	"and page.parent = promt.uoid " + 
	"and element1.parent = page.uoid " + 
	"and element2.parent = page.uoid " + 
	"and element1.question = \"Title\" " + 
	"and element2.question = \"abstract\" " + 
	"and (element1.value LIKE \"%" + searchTerm + "%\" OR element2.value LIKE \"%" + searchTerm + "%\") " +
	"order by promt.uoid DESC";

java.util.List result = null;
try {
	result = hqlSession.createSQLQuery(basic).addScalar("promt.uoid", org.hibernate.Hibernate.STRING).list();
}catch (Exception exception){
    result = new Vector();
    
}

	hqlSession.close();
	
	java.util.Iterator iter = result.iterator();
	while (iter.hasNext()){
	    String resultPromtID = (String)iter.next();

	    try {
	    	_title = new org.setupx.repository.server.persistence.SXQuery().findPromtTitleByPromtID(Integer.parseInt(resultPromtID));
	    	_abstract = new org.setupx.repository.server.persistence.SXQuery().findPromtAbstractByPromtID(Integer.parseInt(resultPromtID));
	    	if (_title.compareTo("-unknown-") == 0) throw new Exception("not found");
	        %>
			<tr>
				<td></td>
			    <td><b>Experiment</b></td>
			    <td><%=resultPromtID %></td>
				<td><b><%=_title %></b>[<%=resultPromtID%>]<br><%=_abstract%>
				</td>
			    <td><img  border='0' src="pics/go_blank.gif"></td>
			    <td><a href="load?id=<%=resultPromtID%>&action=<%=org.setupx.repository.core.user.access.PromtUserAccessRight.READ%>"><img  border='0' src="pics/go.gif"></a></td>
				<td></td>
			</tr>
		<%
			
	    } catch (Exception e4){
	    }
	}
	%>
	
<% 
hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

basic  = 	"select sample.uoid, child.value from formobject as sample, formobject as child " + 
			"where child.parent = sample.uoid " + 
			"and child.value LIKE \"%" + searchTerm + "%\" " +
			"and sample.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.Sample\" " + 
			"order by sample.uoid DESC";

try {
	result = hqlSession.createSQLQuery(basic).addScalar("sample.uoid", org.hibernate.Hibernate.STRING).addScalar("child.value", org.hibernate.Hibernate.STRING).list();
}catch (Exception exception){
    result = new Vector();
    
}
	hqlSession.close();
	
	iter = result.iterator();
	while (iter.hasNext()){
	Object[] objects = (Object[])iter.next();
	    sampleID = Integer.parseInt(objects[0] + "");
	    String sampleLabel = (String)objects[1];

	    try {
	        promtID = new org.setupx.repository.server.persistence.SXQuery().findPromtIDbySample(sampleID);
	    	_title = new org.setupx.repository.server.persistence.SXQuery().findPromtTitleByPromtID(promtID);
	    	_abstract = new org.setupx.repository.server.persistence.SXQuery().findPromtAbstractByPromtID(promtID);
	    	if (_title.compareTo("-unknown-") == 0) throw new Exception("not found");
	        %>
			<tr>
				<td></td>
			    <td><b>Sample</b></td>
			    <td><%=sampleLabel%><br><%=sampleID%></td>
				<td><b><%=_title %></b>[<%=promtID%>]<br><%=_abstract%>
				</td>
			    <td><a href="sample_detail.jsp?id=<%=sampleID%>"><img  border='0' src="pics/go.gif"></a></td>
			    <td><a href="load?id=<%=promtID%>&action=<%=org.setupx.repository.core.user.access.PromtUserAccessRight.READ%>"><img  border='0' src="pics/go.gif"></a></td>
				<td></td>
			</tr>
		<%
			
	    } catch (Exception e4){
	    }
	}
	%>
	
	
	

<tr>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
	
</table>
<%@ include file="footer.jsp"%>

	