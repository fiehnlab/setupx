<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="java.text.SimpleDateFormat"%>

			<td width="40%" valign="top">
				<table width="100%">
					<tr>
						<th width="100" style="background-color: gray;"><%=attribute.getQuestion() %></th>
					<tr>
						<td>
							<font style="font-size: xx-small">description:<br></font>							
							<%=attribute.getDescribtion() %>
					<tr>
				    	<td>
				    		<font style="font-size: xx-small">comment:<br></font>							
				    		<%=attribute.getComment() %>
				</table>
			<td width="40%" valign="top">
				<table width="100%">
					<tr>
						<th><nobr><b>MSI Label:</b></nobr>  
						<td><%=attribute.getMsiLabel() %>
					<tr>
						<%if (attribute.getExtension() != null && attribute.getExtension().compareTo("null") != 0 && attribute.getExtension().compareTo("") != 0){ %>
						<th>Extension: 
						<td><%=attribute.getExtension() %>	    
					    <%}%>
				    <%
				    
				    List l = s.createSQLQuery("select uoid, question from formobject where parent > 0 and msiattribute = " + attribute.getUOID())
				    			.addScalar("uoid", Hibernate.STRING)
				    			.addScalar("question", Hibernate.STRING)
				    			.list();
				    
				    %>
	    
					<tr>
						<th>Entries: 
				    	<td><%=l.size() %> 
					<tr>
						<th>Version: 
						<td>
							<font style="font-size: x-small">
								SX-ID:<%=attribute.getUOID() %><br>
							</font>						
							<%
							SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
							%>
							<font style="font-size: x-small">
							<%=fmt.format(attribute.getVersion())%>
							</font>
							<font style="font-size: xx-small">
							<br>
							<%=((UserDO)CoreObject.persistence_loadByID(UserDO.class, s, attribute.getUserID())).getDisplay() %>
							</font>
				</table>
	    
