<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<% 	
	// import ?
	boolean importString = Boolean.valueOf(request.getParameter("import")).booleanValue();

	// export ?
	boolean exportString = Boolean.valueOf(request.getParameter("export")).booleanValue();

	// export ?
	String col = request.getParameter("col");
	
	// find the ID 
	long promtID = Long.parseLong(request.getParameter("id"));

	// send it to the export
	%>



<%@ include file="checklogin.jsp"%>
<%@ include file="header_standard.jsp"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<form action="export_import.jsp">
	<table align="center" width="60%" cellpadding="5">	


	<%
	// determine col info
	ColumnInformation[] columnInformations = new ColumnInformation[]{
	        ColumnInformation.MDM35,
	        ColumnInformation.RTX5,
	        ColumnInformation.RTX5GCxGC, 
	        ColumnInformation.TESTCOLUMN};
	
	for(int i = 0; i < columnInformations.length; i++){
	    if (columnInformations[i].getBbid().compareTo(col) == 0){
			boolean finished = true; //new SXQuery().determinePromtFinished(promtID);
			if (finished){
		        Promt.export(promtID, columnInformations[i], importString);
				%>
				<tr>
					<td>
						<b>Data has been exported.</b> <p>
						Reference: <code>Promt.export(<%=promtID %>,<%=columnInformations[i].getBbid()%>,<%=importString %>);</code>
						<%
			} else {
			    %>	The experiment is not finished and therefore can not be exported yet.<%
			}
	    }
	}
	%>
	
	
	
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector"%>
<%@page import="org.setupx.repository.core.communication.leco.ColumnInformation"%>
	