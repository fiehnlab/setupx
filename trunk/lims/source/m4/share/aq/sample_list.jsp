<%@page contentType="text/html" %>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.QueryServlet"%>
<%@ page import="org.setupx.repository.web.forms.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.multi.*"%>
<%@ page import="org.setupx.repository.web.forms.clazzes.*"%>
<%@ page import="org.setupx.repository.web.forms.page.*"%>

<%@ include file="header.jsp"%>

<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<table align='center' border='0' cellpadding='4' width='800'>
<tr>
<th><h1>Acquistion</h1>selecting samples for the Acquistion Tasks</th>
</tr>
<tr><td>
<table border="1">
<tr><td>
<!-- left side -->
<table widt="100%">
<tr><td><h3>Search Result:</h3>These following samples match your description. All samples that have not been run yet are selected by <b>default</b>. You can still rerun the samples 
that ran in a previous sequence. Just select the samples that you want to run and move them to the right.

<table border ="1">
<tr>
<th></th>
<th>Sample ID</th>
<th>Label</th>
<th>Class ID</th>
<th>last Run</th>
</tr>
<form action="select" method="POST">

<% 
// samples that were found by the queryservlet
java.util.HashSet samplesFound = (java.util.HashSet)session.getAttribute(WebConstants.SESS_SAMPLES); 		


// samples that were already selected for acquisition
java.util.HashSet samplesSelected = (java.util.HashSet)session.getAttribute(WebConstants.SESS_SAMPLES_SELECTED);

if (samplesSelected == null) samplesSelected = new java.util.HashSet();
	if (samplesFound != null && samplesFound.size() != 0) {
        java.util.Iterator iterator = samplesFound.iterator();
        while (iterator.hasNext()) {
            Sample sample = (Sample) iterator.next();  
			// just in case it has not been selected already
			if (SampleComparator.contains(samplesSelected,sample)){
 
			} 
			else {

// acq information
String acqName = "";
String checked = "checked";
try {
    acqName = new SXQuery().findAcquisitionNameBySampleID(sample.getUOID());
    checked = "";
}catch (Exception e){
    
}

%>
<tr>
	<td><input type="checkbox" name="sample<%=sample.getUOID()%>" value="<%=sample.getUOID()%>" <%=checked %> /></td>
	<td>ID:<%=sample.getUOID()%></td>
	<td><%=sample.getComment()%></td>
	<td><%=sample.determineClazzID()%></td>
	<td><%=acqName %></td> 
</tr>
<% 			
        
			}
		}
	} else {
%>
<tr>
<td>--</td>
<td>--no samples found --</td>
<td><img src='../pics/blank.jpg' border="0" valign="middle" align="center"></td>
<td><img src='../pics/blank.jpg' border="0" valign="middle" align="center"></td>
</tr>
<%
}
%>



<!-- middle spalte -->
</th></tr>
</table>
</td>
<td>
<table>
<tr><td><input type="image" src='../pics/RightArrowBlue.jpg' alt="Add"></td></tr>
</table>
</td>

</form>
<td>
<!-- left side -->
<table>
<tr><td><h3>Samples selected for Acquisition:</h3>These following samples are selected for acquistion.

<table border ="1">
<tr>
<th>ID</th>
<th></th>
<th></th>
<th></th>
</tr>

<%

	if (samplesSelected != null && (samplesSelected.size() != 0) ) {
        java.util.Iterator iterator = samplesSelected.iterator();
        while (iterator.hasNext()) {
            Sample sample = (Sample) iterator.next(); %>
<tr>
	<td>ID:<%=sample.getUOID()%></td>
	<td><%=sample.getComment()%></td>
	<td><%=sample.determineClazzID()%></td>
</tr>
</tr>

<% 			
        }
}else {
%>
<tr>
<td>-- no samples selected yet --</td>
<td>--------</td>
<td><img src='../pics/blank.jpg' border="0" valign="middle" align="center"></td>
<td><img src='../pics/blank.jpg' border="0" valign="middle" align="center"></td>
</tr>
<%
        }

%>
</table>
</table>
</td></tr>

<tr>
<td align="center" colspan="3">
<table > 
<th colspan="3">Action</th>
<tr><td><a href="query.jsp"><img src='../pics/aq_find.gif' border="0" valign="middle" align="center"></a></td><td>Search more samples</td><td></td></tr>
<tr><td><a href="clean.jsp"><img src='../pics/editer.gif' border="0" valign="middle" align="center"></a></td><td>Clean selection</td><td></td></tr>
<tr><td><a href="jobinformation.jsp"><img src='../pics/aq_generate.gif' border="0" valign="middle" align="center"></a></td><td>Create Acquistion Task </td><td></td></tr>
<tr><td><a href="help.jsp"><img src='../pics/aq_help.gif' border="0" valign="middle" align="center"></a></td><td>Help</td><td></td></tr>
<tr><td><a href=".."><img src='../pics/aq_exit.gif' border="0" valign="middle" align="center"></a></td><td><b>Exit</b></td><td></td></tr>
</table></td>
</tr>
</table>
</table>

<%@ include file="../footer.jsp"%>

