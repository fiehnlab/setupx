<%@page contentType="text/html" %>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.QueryServlet"%>
<%@ page import="org.setupx.repository.web.forms.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.multi.*"%>
<%@ page import="org.setupx.repository.web.forms.clazzes.*"%>
<%@ page import="org.setupx.repository.web.forms.page.*"%>

<link href="../stylesheet.css" rel="stylesheet" type="text/css"/>
<%@ include file="header.jsp"%>


<table align='center' border='0' cellpadding='4' width='800'>
<tr>
<td align="center"><a href="query.jsp"><img src='../pics/search2.gif' border="0" align="middle"></a></td>
<th colspan="3	"><h1 >Query for Samples</h1>query for samples to add them to the acquisition</th>
<td></td>
</tr>
<tr><td ></td><td ></td><td></td><td></td><td ></td></tr>

<tr>
	<form action="query" method="POST">
	<td width="50"></td>
	<td colspan="3">
	<fieldset>
	<legend>search by <b>Experiment</b></legend>
	<table width="100%"><tr>
		<td class="inFieldset" ><input name="<%=WebConstants.QUERYSERVLET_QUERYTYPE%>" type="hidden" value="<%=QueryServlet.SAMPLE_BY_EXPERIMENT%>"/>Experiment ID:</td>
		<td class="inFieldset" ><input type="text" name="<%=WebConstants.QUERYSERVLET_EXPERIMENT_ID%>"/></td>
		<td class="inFieldset" ><input type="image" src='../pics/aq_find.gif' alt="Query"></td>
	</tr></table></fieldset></td>
	</form>
	<td width="50"></td>
</tr>	
<tr>
	<form action="query" method="POST">
	<td></td>
	<td colspan="3">
	<fieldset>
	<legend>search by <b>Class</b></legend>
	<table width="100%"><tr>
		<td class="inFieldset" ><input name="<%=WebConstants.QUERYSERVLET_QUERYTYPE%>" type="hidden" value="<%=QueryServlet.SAMPLE_BY_CLASS%>"/>Class ID:</td>
		<td class="inFieldset" ><input type="text" name="<%=WebConstants.QUERYSERVLET_CLAZZ_ID%>"/><p></td>
		<td class="inFieldset" ><input type="image" src='../pics/aq_find.gif' alt="Query"></td>
	</tr></table></fieldset></td>
	</form>
	<td></td>
</tr>	
<tr>
	<form action="query" method="POST">
	<td></td>
	<td colspan="3">
	<fieldset>
	<legend>search by <b>Sample</legend>
	<table width="100%"><tr>
		<td class="inFieldset" ><input name="<%=WebConstants.QUERYSERVLET_QUERYTYPE%>" type="hidden" value="<%=QueryServlet.SAMPLE_BY_SAMPLE%>"/>Sample ID:</td>
		<td class="inFieldset" ><input type="text" name="<%=WebConstants.QUERYSERVLET_SAMPLE_ID%>"/><p></td>
		<td class="inFieldset" ><input type="image" src='../pics/aq_find.gif' alt="Query"></td>
	</tr></table></fieldset></td>
	</form>
	<td></td>
</tr>	




<tr>
	<form action="query" method="POST">
	<td></td>
	<td colspan="3">
	<fieldset>
	<legend>search by <b>Date</b></legend>
	<table width="100%"><tr>
		<td class="inFieldset" ><input name="<%=WebConstants.QUERYSERVLET_QUERYTYPE%>" type="hidden" value="<%=QueryServlet.SAMPLE_BY_DATE%>"/>Date Range:</td>
		<td class="inFieldset" ><input type="text" name="<%=WebConstants.QUERYSERVLET_DATE1%>"/><p></td>
		<td class="inFieldset" ></td>
	</tr>
	<tr>
		<td class="inFieldset" ></td>
		<td class="inFieldset" ><input type="text" name="<%=WebConstants.QUERYSERVLET_DATE2%>"/><p></td>
		<td class="inFieldset" ><input type="image" src='../pics/aq_find.gif' alt="Query"></td>
	</tr></table></fieldset></td>
	</form>
	<td></td>
</tr>	
<tr>
	<form action="query" method="POST">
	<td></td>
	<td colspan="3">
	<fieldset>
	<legend>search by <b>Owner</b></legend>
	<table width="100%"><tr>
		<td class="inFieldset" ><input name="<%=WebConstants.QUERYSERVLET_QUERYTYPE%>" type="hidden" value="<%=QueryServlet.SAMPLE_BY_OWNER%>"/>Owner:</td>
		<td class="inFieldset" ><input type="text" name="<%=WebConstants.QUERYSERVLET_OWNER_NAME%>"/><p></td>
		<td class="inFieldset" ><input type="image" src='../pics/aq_find.gif' alt="Query"></td>
	</tr></table></fieldset></td>
	</form>
	<td></td>
</tr>	
<tr>
	<form action="query" method="POST">
	<td></td>
	<td colspan="3">
	<fieldset>
	<legend>Search <b>all</b> samples</legend>
	<table width="100%"><tr>
		<td class="inFieldset" ><input name="<%=WebConstants.QUERYSERVLET_QUERYTYPE%>" type="hidden" value="<%=QueryServlet.SAMPLE_ALL%>"/></td>
		<td class="inFieldset" ></td>
		<td class="inFieldset" ><input type="image" src='../pics/aq_find.gif' alt="Query"></td>
	</tr></table></fieldset></td>
	</form>
	<td></td>
</tr>	
<tr><td ></td><td ></td><td></td><td></td><td ></td></tr>
</table>


<%@ include file="../footer.jsp"%>
