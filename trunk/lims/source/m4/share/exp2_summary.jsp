
<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.communication.importing.logfile.*,
				 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 	org.setupx.repository.core.communication.leco.*,
                 	edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.*,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

</head>


<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width='90%'>
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Export</h2>
			Summary
			<br/>
			<font size="-2"></font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>




	<% try {
%>
<body>
<table align="center" width="20%" cellpadding="7">	<tr>
  <tr>
    <th></th>
    <th>Name</th>
    <th></th>
    <th>Label</th>
    <th>Class</th>
    <th>Group</th>
    <th></th>
  </tr>

<%
java.util.Hashtable groups = new java.util.Hashtable();
int promtID = 0;
String columnID ;
try {
	promtID  = Integer.parseInt(request.getParameter("promt"));
	columnID = request.getParameter("column");
	if (columnID.compareTo("null") == 0) throw new ColumnInformationNotAvailableException("");
} catch (Exception e){
    e.printStackTrace();
    org.setupx.repository.core.util.logging.Logger.warning(this, "unable to find column - using RTX5");
    columnID = "production_rtx5";
}



Enumeration  enumeration = request.getParameterNames();

org.setupx.repository.core.util.logging.Logger.log(this, "exporting: column: " + columnID);

// enum over paramenter names
while (enumeration.hasMoreElements()){
	String labelString = "" + enumeration.nextElement();
	
	// not promt and not column
    if (labelString.compareTo("promt") !=0 && labelString.compareTo("column") !=0 && labelString.length() > 4 && !labelString.startsWith("group") ){
        try {
            String label = labelString;
    	    org.setupx.repository.core.util.logging.Logger.debug(this,"label: " + label);
            int name = Integer.parseInt(request.getParameter(labelString));
    	    
            
    	    int clazz = new SXQuery().findClazzIDBySampleID(name);
    	    if (clazz == 0) throw new Exception("unable to determine ClazzID for " + name);
    	    org.setupx.repository.core.util.logging.Logger.debug(this,"clazz: " + clazz);
    	    
            // modifications
            //int groupID =  Integer.parseInt(request.getParameter("group" + clazz)+"");
    	    //if (groupID == 0) throw new Exception("unable to determine GroupID for " + name);
			int groupID = clazz;
    	    
    	    
    	    //if (!groups.containsKey("" + groupID)) {
    	    //    groups.put(""+ groupID, new java.util.Hashtable());
    	    //}
    	    if (!groups.containsKey("" + clazz)) {
    	        groups.put(""+ clazz, new java.util.Hashtable());
    	    }

			java.util.Hashtable groupValues = (java.util.Hashtable)groups.get("" + clazz);
    	    //java.util.Hashtable groupValues = (java.util.Hashtable)groups.get("" + groupID);
    	    groupValues.put(""+ label, ""+name);
    	    %>
    	    <tr>
    	      <td></td>    	      
    	      <td valign="middle" align="center" style="background-color:<%=org.setupx.repository.core.util.Util.getColor(groupID*2)%>" ></td>
    	      
    	      <td><%=name%></td>
	          <td align="center"><a href="sample_detail.jsp?id=<%=name%>"><img src='pics/details.gif' border="0" valign="middle" align="center"></a></td>        
	          <td><%=label %></td>
    	      <td valign="middle" align="center" style="background-color:<%=org.setupx.repository.core.util.Util.getColor(groupID*2)%>" ><%=clazz%></td>
    	      <!--  <td valign="middle" align="center" style="background-color:<%=org.setupx.repository.core.util.Util.getColor(groupID*2)%>" ><%=groupID%></td>-->
    	      <td></td>
    	    </tr>
    	  <% 
        } catch (NumberFormatException e){
            // dont do anything - its just an illeagal parameter
        }
	}
}
%>
</table>


<%
ColumnInformation columnInformation = ColumnDetector.getColumnInformationByID(columnID);
org.setupx.repository.core.communication.binbase.trigger.ClassSet export_classset = new org.setupx.repository.core.communication.binbase.trigger.ClassSet(columnInformation);
export_classset.setId(""+promtID);

Enumeration keys = groups.keys();

ExperimentClass[] export_groups = new ExperimentClass[groups.size()];

int clazzPos = 0;
while (keys.hasMoreElements()){
    String group_key = (String)keys.nextElement();
	
    java.util.Hashtable hashtable = (java.util.Hashtable)groups.get(group_key); 
	Enumeration sampleIDs = hashtable.keys();
	
	int samplePos = 0;
	ExperimentSample[] samples = new ExperimentSample[hashtable.size()];	

	while (sampleIDs.hasMoreElements()){

	    // hotfix - id and label swapped - 
		String sampleID = (String)sampleIDs.nextElement();
		String label = (String)hashtable.get(sampleID);

		samples[samplePos] = new ExperimentSample();
		samples[samplePos].setId(label);
		samples[samplePos].setName(sampleID);

		samplePos++;
	}
	export_groups[clazzPos] = new ExperimentClass();
	export_groups[clazzPos].setId(group_key);
	export_groups[clazzPos].setSamples(samples);
	clazzPos++;
}

export_classset.setClasses(export_groups);
export_classset.setIncrease(0);

BinBaseServiceConnector binBaseSerivce = new BinBaseServiceConnector(org.setupx.repository.Config.BINBASE_SERVER);

// start export
//    binBaseSerivce.launchExport((org.setupx.repository.core.communication.binbase.trigger.ClassSet)export_classset);    
binBaseSerivce.launchExport((org.setupx.repository.core.communication.binbase.trigger.ClassSet)export_classset, true);

    %>
	
	<table align="center" width="60%" cellpadding="7">	<tr>
	<th  colspan="4">Export Experiment</th>
</tr>
<tr>
	<td colspan="1" align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/download.gif"></td>
	<td colspan="2" width = 90%><h3>The export of your study has been scheduled.</h3><p>
Data have been sent to BinBase for annotation. When the annotation is finished, you will be able to download your results from <%=org.setupx.repository.Config.SYSTEM_NAME%>. 
<br>Additionally, you may consult with the metabolomics laboratory team for quality controls.

		<p>
	</td>
	<td colspan="1"></td>
</tr>
<tr>
	<td colspan="1"></td>
	<td colspan="2"></td>
	<td colspan="1"></td>
</tr>
<tr>
	<td colspan="1" align="right" style="background-color:#FFFFFF"><a href="main.jsp"><img  border='0' src="pics/back.gif"></td>
	<td colspan="2"></td>
	<td colspan="1"></td>
</tr>
</table>
<% 	
	
} catch (Exception e){
    e.printStackTrace();
    
    
    %>
    
<body>
	<table align="center" width="60%" cellpadding="7">	<tr>
		<th  colspan="4">Export Experiment</th>
	</tr>
	<tr>
		<td colspan="1" align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/error.jpg"></td>
		<td colspan="2" width = 90%><h3>The Export of your Experiment failed.</h3>
		<p>The System was not able to start the Export with the given setting.
			<p><b><%=org.setupx.repository.core.util.Util.getClassName(e)%></b>
			<hr>
			<br><font class="small"> 
			<%e.printStackTrace(new java.io.PrintWriter(out)); %>
			</font>
			<hr>
			<%
				Enumeration enumeration = request.getParameterNames();
				while(enumeration.hasMoreElements()){
				    String name = "" + enumeration.nextElement();
				    String value = request.getParameter(name);
				    %>
				    <tr><td><%=name %><td><%=value %></tr>

			<%
				}
			%>


			<hr>
			<p><font class="small">Problem: <%=e.getLocalizedMessage()%></font></td>
			<% e.printStackTrace(); %>
		<td colspan="1"></td>
	</tr>
	<tr>
		<td colspan="1"></td>
		<td colspan="2"></td>
		<td colspan="1"></td>
	</tr>
	<tr>
		<td colspan="1" align="right" style="background-color:#FFFFFF"><a href="main.jsp"><img  border='0' src="pics/back.gif"></td>
		<td colspan="2"></td>
		<td colspan="1"></td>
	</tr>
</table>
	<%
}
%>


<%@ include file="footer.jsp"%>
