

<!--  navigation in the incl_xls_footer  -->
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Enumeration"%>

<% 
	Vector links = new Vector();
	links.add(new String[]{"pubxlsimport0.jsp", "upload file", "pics/go.gif"});
	links.add(new String[]{"pubxlsimport2.jsp?reset=yes", "reset values", "pics/go.gif"});
	links.add(new String[]{"pubxlsimport2.jsp", "select data area", "pics/go.gif"});
	links.add(new String[]{"pubxlsimport3.jsp", "select and submit", "pics/go.gif"});

	Logger.log(this,Util.getClassName(this));
	// cut off _jsp
	
	int internalPageCounter = Integer.parseInt("" + Util.getClassName(this).substring("pubxlsimport".length(), "pubxlsimport".length() + 1 ));
	int posOK = 0;
	if (session.getAttribute("data") == null) posOK = 0;
	else if(session.getAttribute("labels") == null) posOK = 3;
	//else if(session.getAttribute("promtID") == null) posOK = 5;
	else if(session.getAttribute("relations") == null) posOK = 5;
	else if(internalPageCounter == 6) posOK = 7;
	else {posOK = 6;}
%>

<%@page import="java.util.Vector"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<br>
<br>
<table align="center">
	<tr>
		<th colspan="21"><font class="small">Each of the buttons below represents one of the steps to merge the data with your existing experiment.<br>If a button shows a lock it means that you can not jump to that step yet - you have to complete the steps before first.</font>
	<tr>
	
<% 
Enumeration keyEnumeration = links.elements();
int ewrr = -1;
while(keyEnumeration.hasMoreElements()){
    ewrr++;
	String[] myValues = (String[])keyEnumeration.nextElement();
    String link =  myValues[0];
    String label = myValues[1];
    String img = myValues[2];
%>
		<td>
			<table width="100%">	
				<tr>
					<td align="center"><%=label %></td>
				</tr>
				<tr>
					<td align="center" style="background-color:#FFFFFF">

						<% if (ewrr == internalPageCounter){ %>
							<img src='pics/go_blank.gif' border="0" valign="middle" align="center">
						<%} else if (ewrr <= posOK){ %>
						<a href="<%=link%>">
							<img src='<%=img%>' border="0" valign="middle" align="center">
						</a>
						<%} else { %>
							<img src='pics/go_locked.gif' border="0" valign="middle" align="center">
						<%} %>
					</td>
				</tr>
			</table>			
		</td>
<%} %>
	</tr>
</table>

<%@include file="footer.jsp"%>
