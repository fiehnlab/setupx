<%@ include file="checklogin.jsp"%>

<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.communication.leco.Method"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.Config"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%></title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<body>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">	
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Result Files</h2><br>Files containing the results from BinBase.</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>


<%@ include file="navi_admin.jsp"%>

<table align='center' border='0' cellpadding='4' width="60%">

		  <tr>
		  	<th>	      
		  	<th>Filename	      
		  	<th>Experiment	      
		  	<th>Samples   
		  	<th>Classes
		  	<th>Summary   
		  	<th>Download
		  	<th>	      
		  </tr>
<%
java.io.File[] files = new java.io.File(Config.DIRECTORY_BB_RESULTS).listFiles();
List list = new SXQuery().findPromtIDs();

for (int i = 0; i < files.length; i++){
  java.io.File file = files[i];
  boolean ok = true;
  
  if (file.isDirectory()) ok = false;

  
  if (ok) {
      // cut the id out
      String id = file.getName().substring(0, file.getName().lastIndexOf('.'));

      long promtID = 0;
      try {
	      promtID = Long.parseLong(id);
      }catch(NumberFormatException exception){
	  }

      boolean isExperiment = false;
      Iterator iterator_ = list.iterator();
      while (iterator_.hasNext()) {
          if ((id).compareTo("" + iterator_.next()) == 0){
              isExperiment = true;
          }
      }

	      %>
		<tr>
	      <td>
	      <%
      if (isExperiment){
		     String titleString = new SXQuery().findPromtTitleByPromtID(promtID);
		     String abstractString = new SXQuery().findPromtAbstractByPromtID(promtID);
			 int classes = new SXQuery().findClazzIDsByPromtID(promtID).size();
			 int samplesTotal = new SXQuery().findSampleIDsByPromtID(promtID).size();
			 
			 
			 PromtUserAccessRight accessRight = null;
			 try {
			     accessRight = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID(),promtID);
			 } catch (Exception e ){
				 accessRight = new PromtUserAccessRight();
				 accessRight.setAccessCode(0);
			 }
          %>
        <td align="center" style="background-color:#FFFFFF"><img border='0' src="pics/zip.gif">
		<th align="right">
			<b>ID:<%=promtID %></b>
			<% if (accessRight.getAccessCode()>0) {%>
				<%=Promt.createShortAbstractHTML(promtID, 60)%>
			<%} else { %>
				<i> non public experiment </i> <img  border='0' src="pics/warning.gif"> <br><font class='small'><I>This experiment is not publicly available yet.</I></font>
			<%} %>
		</th>

		<!-- number of samples -->
		<td align="center" style="background-color:#FFFFFF"><%=samplesTotal %></td>

		<!-- number of classes -->
		<td align="center" style="background-color:#FFFFFF"><%=classes %></td>

			<% if (accessRight.getAccessCode()>PromtUserAccessRight.READ) {%>
				<td align="center" style="background-color:#FFFFFF"><a href="load?id=<%=promtID %>&action=<%=PromtUserAccessRight.READ%>"><img  border='0' src="pics/go.gif"></a></td>
			<%} else { %>
				<td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_locked.gif"></td>
			<%} %>

			<% if (accessRight.getAccessCode()>PromtUserAccessRight.DOWNLOAD_RESULT) {%>
				<td align="center" style="background-color:#FFFFFF"><a href="load?id=<%=promtID %>&action=<%=PromtUserAccessRight.DOWNLOAD_RESULT%>"><img  border='0' src="pics/go_download.gif"></a></td>
			<%} else { %>
				<td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_locked.gif"></td>
			<%} %>


   	 	<td></td>
          <%
      } else {
          %>
	      <td align="center"><%=id%>
	      <th align="right"><i>Resultfile is not related to <b>only one experiment</b></i> <img  border='0' src="pics/warning.gif"> <br><font class='small'>This file might contain data that was merged from more than one experiment.</font>
	      <td align="center" style="background-color:#FFFFFF">
	      <td align="center" style="background-color:#FFFFFF">
	      <td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_blank.gif"></td>
	      <td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_locked.gif"></td>
	      <td>
          <%
      }
  }        
}
%>
	<tr>
	      <td align="center" style="background-color:#FFFFFF">
	      <td align="center" style="background-color:#FFFFFF">
	      <td align="center" style="background-color:#FFFFFF">
	      <td align="center" style="background-color:#FFFFFF">
	      <td align="center" style="background-color:#FFFFFF">
	      <td align="center" style="background-color:#FFFFFF">
	      <td align="center" style="background-color:#FFFFFF">
	      <td align="center" style="background-color:#FFFFFF">
	</td>

</table>

<%@ include file="footer.jsp"%>