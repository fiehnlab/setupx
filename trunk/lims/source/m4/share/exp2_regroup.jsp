<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.communication.importing.logfile.*,
				 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.communication.leco.*,
                 org.setupx.repository.core.util.*,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.communication.file.SampleFile"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%

// max number of groups
int MAX_NR_GROUPS = 4;
try {
    MAX_NR_GROUPS = Integer.parseInt("" + request.getSession().getAttribute("MAX_NR_GROUPS"));
} catch (Exception e){
    e.printStackTrace();
}

int promtID = Integer.parseInt(request.getParameter("id"));
%>
<body>



<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width='60%'>
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Export</h2>
			<br/>
			<font size="-2">Below you find all samples including every available run for each sample related to the selected experiment.<br>Choose all samples that you want to include in the result.</font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>



<!--  column information -->
	<table align="center" width="60%" cellpadding="2">






	<table align="center" width="60%" cellpadding="2">	<tr>
		<th colspan="10">Select Samples for Export <%=promtID %></th>
	</tr>
	<tr>
		<td colspan="10"><b>preselection of samples:</b>
	<tr>
		<td colspan="10">
		- all samples of <a href="exp2_regroup_1.jsp?id=<%=promtID %>&grouping=1">each class together</a>
		<br>
		- every <a href="exp2_regroup_1.jsp?id=<%=promtID %>&grouping=2">1st run with every 1st, every 2nd with every 2nd,</a> ...		
		<br>
		- every <a href="exp2_regroup_1.jsp?id=<%=promtID %>&grouping=3">1st sample of a class with every 1st, every 2nd of class with every 2nd</a>, ...
		<br>
		- group every 
			<a href="exp2_regroup_1.jsp?id=<%=promtID %>&grouping=5&groupNumbers=2">2nd</a>,  
			<a href="exp2_regroup_1.jsp?id=<%=promtID %>&grouping=5&groupNumbers=3">3rd</a>,
			<a href="exp2_regroup_1.jsp?id=<%=promtID %>&grouping=5&groupNumbers=4">4th</a> or 
			<a href="exp2_regroup_1.jsp?id=<%=promtID %>&grouping=5&groupNumbers=5">5th</a> class with each other.
		<br>
		<form method="post" action="exp2_seach_word.jsp">
		- every class containing the word <input type="text" value="searchword" name="search"/> in its descritption.
		<input type="hidden" name="id" value="<%=promtID %>">
		</form> 
		
		<br>
		<br>	
		- <a href="exp2_selectReruns.jsp?id=<%=promtID %>">only the latest runs.</a> <font class="small">&nbsp;&nbsp;&nbsp;Will take all samples out of your selection that are not the LAST run for that sample.</font>
			
		
	</tr>
	<form method="post" action="exp2_setting_regrouped.jsp">
	
<%


List classList = new SXQuery().findClazzIDsByPromtID(promtID);
Iterator iterator = classList.iterator();

HashSet hash = new HashSet();


// each class
int staticCount = -1;
while (iterator.hasNext()) {
    staticCount++;
    int classID = Integer.parseInt("" + iterator.next());

    // class labels
    java.util.Iterator iterator1 = new SXQuery().findLabelsforClazz(classID).iterator();
    %>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<%
		int group = staticCount;
		String linkOtherClasses = "";
		try {
		    try {
			    group = Integer.parseInt(request.getParameter("group" + classID));    
		    } catch (NumberFormatException e){
		        //group stays staticcount
		    }
		    
		    // create a string containing ALL OTHER classes with there grouping
		    java.util.Iterator allClasses = new SXQuery().findClazzIDsByPromtID(promtID).iterator();
		    int initialID = -1;
		    while (allClasses.hasNext()){
		        initialID++;
		        long _id = Long.parseLong("" + allClasses.next());
		        if (_id != classID){

		            int _tmpGroup = initialID;
					try {
					    // in case the ids are not initaliesed.
			            _tmpGroup = Integer.parseInt(request.getParameter("group" + _id) );
					} catch (Exception e){
					    //org.setupx.repository.core.util.logging.Logger.debug(this, e.getMessage());
					}
		            linkOtherClasses = linkOtherClasses + "&group" + _id + "=" + _tmpGroup;    
					
		            //org.setupx.repository.core.util.logging.Logger.debug(this, "LINK OTHERS: " + linkOtherClasses);
		        }
		    }
		    
		} catch (Exception e){
			e.printStackTrace();
		}
		
		%>
		<th></th>
		<th colspan="3" align="center" style="background-color:<%=Util.getColor(classID*2)%>">Class <b><%=classID %></b></th>

		<th colspan="6">
		</th>
		<th></th>
	</tr>
	<tr>
	<td colspan="1"><a name=<%=classID %>></td>
	<td style="background-color:<%=org.setupx.repository.core.util.Util.getColor(classID*2)%>"  colspan="1"></td>
	<td colspan="2">
	<td colspan="6">
		<table>
    <% 
    while (iterator1.hasNext()) {
    Object[] element = (Object[]) iterator1.next();
      %>
			<tr>
					<td><%=element[0].toString()%>:</td>
					<td><b><%=element[1].toString()%></b></td>
			</tr>
      <%
    }
 	%>
			</table>
	    </td>
	  <td></td>
	</tr>
	
	
	<% // number of buttons to select an entire class %>
	
	<tr>
		<td colspan="10" align="right"><font class="small">select the entire class&nbsp;</font>
		<td>
			<a href="exp2_class_select.jsp?id=<%=promtID%>&cid=<%=classID %>&g=-1">
			<img src="pics/trash.gif" border="0"> 
			</a>
		<%
			for (int classSelecionCounter = 0; classSelecionCounter < MAX_NR_GROUPS; classSelecionCounter ++){
			%>
			<td>
				<a href="exp2_class_select.jsp?id=<%=promtID%>&cid=<%=classID %>&g=<%=classSelecionCounter %>">
				<img src="pics/blank.jpg" border="0"> 
				</a>
			<%
			}
		%>
	<%

    // samples in that class
    Iterator sampleIDs = new SXQuery().findSampleIDsByClazzID(classID).iterator();
    while (sampleIDs.hasNext()){
        int sampleID = Integer.parseInt("" +  sampleIDs.next());
        //String label = new SXQuery().findSampleLabelBySampleID(sampleID);
        
		List list = new SXQuery().findScannedPair(sampleID);
        Iterator scannedPairs = list.iterator();
        boolean checked = true;
        %>

		<% if(list.size() == 0){%>
	        <tr>
	        <td ></td>        
	        <td style="background-color:<%=org.setupx.repository.core.util.Util.getColor(classID*2)%>" align="center"></td>        
	        <td align="center" style="background-color:#FFFFFF"></td>        
	        <td align="center">Sample <b><%=sampleID%></b></td>        
	        <td align="center"><%=new SXQuery().findSampleLabelBySampleID(sampleID) %></td>        
	        <td align="center" colspan="3"><b>Sample has not been run yet.</b></td>        
			<td align="center"><img src='pics/hprio_tsk.gif' border="0" valign="middle" align="center"></td>        
	        <td align="center" style="background-color:#FFFFFF"><input type="hidden" disabled="disabled"></td>
	        <td></td>        
	        </tr>
		<%} %>        
        
        
        <%
        
        
        
        
        boolean first= true;
        while (scannedPairs.hasNext()) {
            
            
			String acqname = (String) scannedPairs.next();
			if (!hash.contains(acqname)){
	            hash.add(acqname);
				boolean exists = SampleFile.exists(acqname, SampleFile.TXT);  

	            %>
	        <tr>
	        <td></td>        
			<td style="background-color:<%=org.setupx.repository.core.util.Util.getColor(classID*2)%>" align="center"></td>
			<td width="2" align="center" style="background-color:#FFFFFF"><% if (first){ %><a href="sample_detail.jsp?id=<%=sampleID%>"><img src='pics/details.gif' border="0" valign="middle" align="center"></a><%}%></td>        
	        <td align="center"><% if (first){ %>Sample <b><%=sampleID%> <%}%></b></td>        
	        <td align="center"><% if (first){ %><%=new SXQuery().findSampleLabelBySampleID(sampleID) %> <%}%></td>        

	        <!--  hotfix -->
	        <td align="center" <%if (!exists){%> style="background-color:#FF545A" <%} %> colspan="3"><%=acqname%></td>

			<%if (exists){ %>
	        <td align="center" valign="middle" width="2">
				<a href="run_detail_acq.jsp?acq=<%=acqname%>">	
		    	    <img src='pics/details.gif' border="0" valign="middle" align="center">
		        </a>
	        </td>        
	        <td align="center" style="background-color:#FFFFFF"><input type="hidden" name="<%=acqname%>" value="<%=sampleID%>" <%if (checked){ %>checked="checked" <%} %>></td>        
	        <%} else { %>
			<td align="center" valign="middle">
				<img src='pics/warning.gif' border="0" valign="middle" align="center">
				<font class="error">Sample ran successfully but file was not converted.</font>
			</td>
	        <td align="center" style="background-color:#FFFFFF"><input type="hidden" disabled="true" name="<%=acqname%>" value="<%=sampleID%>" <%if (checked){ %>checked="checked" <%} %>></td>        
	        <%}%>
	        
		                
	        <% 
	        // find the current assignment of the sample
	        //
	        // the value can actually be in the request ...
	        int assignedGroup = 0;
	        try {
	            // .. like here.
		        Object o = request.getParameter("" + acqname);
		        assignedGroup = Integer.parseInt("" + o);
		        request.getSession().setAttribute("" + acqname, "" + assignedGroup);
	        } catch (Exception e){
	            assignedGroup = -1; // TODO
	            Logger.log(this, "request: no assigned group for " + acqname);
	        }
	        
	        try {
	            // ... or in the session like here.
	            assignedGroup =  Integer.parseInt("" + request.getSession().getAttribute("" + acqname));
			} catch (Exception e){
	            assignedGroup = -1; // TODO
	            Logger.log(this, "session: no assigned group for " + acqname);
	      	}

			%>
	        <td align="center" >
        	<a href="exp2_regroup.jsp?<%=acqname %>=-1&id=<%=promtID%>#<%=classID%>">
        		<% if (assignedGroup > -1){ %>
        		<img src='pics/trash.gif' border="0" valign="middle" align="center">
        		<% } %>
        	</a>
			<%
			
		    //Logger.debug(this, "MAX_NR_GROUPS:" + MAX_NR_GROUPS + " - assignedGroup: " + assignedGroup);
			if (assignedGroup >= MAX_NR_GROUPS - 1) {
			    MAX_NR_GROUPS = assignedGroup + 2;
			    request.getSession().setAttribute("MAX_NR_GROUPS", MAX_NR_GROUPS + "");
			    Logger.debug(this, "setting new MAX_NR_GROUPS");
			}
			    
			
	        for (int sampleGroupCount  = 0; sampleGroupCount < MAX_NR_GROUPS; sampleGroupCount++){
		        
	        	if (assignedGroup < 0){ %>
		        <td align="center" >
		        	<a href="exp2_regroup.jsp?<%=acqname %>=<%=sampleGroupCount %>&id=<%=promtID%>#<%=classID%>">
		        		<img src='pics/x-amber.jpg' border="0" valign="middle" align="center">
		        	</a>
			<% }else if (assignedGroup == sampleGroupCount){ %>
			        <td align="center" >
			        	<a name=<%=sampleID %>>
			        		<img src='pics/checked.jpg' border="0" valign="middle" align="center">
			        	</a>
				<% }else{ %>
			        <td align="center" style="background-color: <%=Util.getColor((sampleGroupCount+1)*2)%>">
			        	<a href="exp2_regroup.jsp?<%=acqname %>=<%=sampleGroupCount %>&id=<%=promtID%>#<%=classID%>">
			        		<img src='pics/blank.jpg' border="0" valign="middle" align="center">
			        	</a>
      		  	<% } 
      		} %>
	        
	        
	        <td></td>        
	        </tr>
			<%
            if (checked){   
                checked = false;
            }
        	}
			first = false;
        } 
        
    }

}
// looping over all samples - making sure that only samples from the hash are actually in the session
Enumeration e = request.getSession().getAttributeNames();
while (e.hasMoreElements()){
    String _ename = e.nextElement().toString();
    if (hash.contains(_ename)){
        
    } else {
        Logger.debug(this, "unknown: " + _ename + " : " + request.getSession().getAttribute(_ename));
        try {
            int _tmpID = Integer.parseInt(request.getSession().getAttribute(_ename) + "");
            if (_tmpID > -1 && _tmpID < 200 && _ename.compareTo("MAX_NR_GROUPS") != 0){
                request.getSession().removeAttribute(_ename);
                Logger.debug(this, "removing attribute: " + _ename);
            }
        } catch (Exception exception){
            
        }
    }
}




%>
	  		<tr>
  					<td><input type="hidden" name="promt" value="<%=promtID%>"></td>
  					<td colspan="6" align="right">Send selection to Export.  </td>
  					<td colspan="2" align="left"><input src='pics/go_export.gif' type="image" value="connect"/></td>
  					<td></td>
	  		</tr>
	<tr>
		<td colspan="1"></td>
		<td colspan="8">
		<td colspan="1"></td>
	</tr>
	<%
	%>
</table>
</form>
</body>

<%@ include file="footer.jsp"%>
