<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.communication.importing.metadata.*,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.exolab.castor.xml.AccessRights"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ include file="incl_setaccessright.jsp"%>
<%
//list of all experiments
List promtIDs = new SXQuery().findPromtIDs();
int userID = Integer.parseInt("" + request.getParameter("uid")); 

%>

	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>

		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%> Administration</h2>
			Access to Experiment for User <%=userID %><br/>
		</th>

		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>
	
	
	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} %>
	
	<table align="center" width="80%" cellpadding="7">
	<tr>
		<td colspan="15"></td>
	</tr>
	<tr>
		<td colspan="2" align="center" style="background-color:#FFFFFF"><a href="main.jsp"><img  border='0' src="pics/back.gif"></a></td>	
		<th colspan="20">Experiment Access for User <%=userID %></th>
	</tr>
		<tr>
		<td></td>
		<th></th>
		<td width="6%" align="center"><font>samples</font></td>
		<td width="6%" align="center"><font>classes</font></td>
		<td width="6%" align="center"><font>scheduled</font></td>
		<td width="6%" align="center"><font>running</font></td>
		<td width="6%" align="center"><font>finished</font></td>
		<td width="6%" align="center"><font>
			<% if (user.isLabTechnician() ){ %>
				Edit
			<% } else { %> 
				Edit 
			<%} %></font></td>
		<td width="6%" align="center"><font>Summary</font></td>
		<td width="6%" align="center">Track It!</td>
		<td width="3%"><b></td>
	</tr>
	

	<%
	// find all exp where access right > 0
	List accessList = new SXQuery().findPromtUserAccessRightForUserID(userID);

	Session hqlSession = CoreObject.createSession();
	Iterator accessIterator = accessList.iterator();
	while (accessIterator.hasNext()){
	    PromtUserAccessRight promtUserAccessRight = (PromtUserAccessRight)accessIterator.next();
	    
	    if (promtUserAccessRight.getAccessCode() > 0){
	        long promtID = promtUserAccessRight.getExperimentID();
	         
	        %>
			<%@ include file="incl_experiment_line.jsp"%>
		
	        <%
	    }
	}
%>	



</table>


<%@ include file="footer.jsp"%>
