<% // requies userID and promtID %>
<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.List,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.server.persistence.*"%>

<%@ page language="java" %>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<table align="center" style="background-color:#FFFFFF">
<tr>
<%

if (user.isAdmin()) {
	try {
		int _userID = Integer.parseInt("" + request.getParameter("uid")); 
		int _promtID = Integer.parseInt("" + request.getParameter("pid")); 
		int _accesscode = Integer.parseInt("" + request.getParameter("ac")); 
		PromtUserAccessRight _access = null;
		
		try {
		    _access = new SXQuery().findPromtUserAccessRightForUserID(_userID, _promtID);
		} catch (PersistenceActionFindException exception){
		    _access = new PromtUserAccessRight();
		    _access.setUserID(_userID);
		    _access.setExperimentID(_promtID);
		}
		_access.setAccessCode(_accesscode);
		_access.update(true);
	}	
	catch (Exception exception){
	}
}

%>



