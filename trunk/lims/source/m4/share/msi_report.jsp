<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Hashtable"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>

<%@ include file="incl_header_msi.jsp"%>


<%
	Session hibernateSession = CoreObject.createSession();
	long id = Long.parseLong(request.getParameter("id"));
%>

<table align="center" width="60%" cellpadding="5">
	<tr>
		<th colspan="21">
			
			<h2>
				<a href="msi_report_xml.jsp?id=<%=id %>"><img src="pics/xml3.gif" border=0></a>
				<%=new SXQuery().findPromtTitleByPromtID(id) %>
			</h2>
			<font style="font-size: xx-small;"><%=new SXQuery().findPromtAbstractByPromtID(id) %></font>
			<p>Report of Study (SX#. <%=id %>) fulfilling the Standard reporting requirements for samples in metabolomics experiments.
<%
	
	String query = "select value, msiattribute from formobject where msiattribute is not null and active = 1 and root(uoid) = " + id;

	Session s = CoreObject.createSession();
	List list = s.createSQLQuery(query)
		.addScalar("value", Hibernate.STRING)
		.addScalar("msiattribute", Hibernate.STRING)
		.list();
	
	
    Logger.debug(this, "result size: " +list.size());
    Hashtable cache = new Hashtable();
	Iterator iterator = list.iterator();
	while(iterator.hasNext()){
	    Object[] objects = (Object[])iterator.next();  
	    try {
		    String value = objects[0].toString();
		    if (value != null && value.compareTo("null") != 0 && value.length() > 0){
			    try {
				    Logger.debug(this, value + " cached? " + ((Vector)cache.get(objects[1].toString())).contains(value));
				} catch (Exception e){
				    //Logger.debug(this, "creating new Vector for " + objects[1].toString());
				    cache.put(objects[1].toString(), new Vector());
				    ((Vector)cache.get(objects[1].toString())).add(value);
				    Logger.debug(this, "added value (2) " + value);
			    }
			    
			    if (((Vector)cache.get(objects[1].toString())).contains(value)){
	
			    } else {
				    if (value.length() > 1){
					    long msiLong = Long.parseLong(objects[1].toString());
					    try {
					        ((Vector)cache.get(objects[1].toString())).add(value);
						    Logger.debug(this, "added value (1) \"" + value + "\"");
						} catch (Exception e){
						    Logger.debug(this, "creating new Vector for " + objects[1].toString());
						    cache.put(objects[1].toString(), new Vector());
						    ((Vector)cache.get(objects[1].toString())).add(value);
						    Logger.debug(this, "added value (2) \"" + value + "\"");
					    }
				    }    
			    } // end else
		    }// end if
	    }catch (Exception e){
	        
	    }		
	}
%>

<%

			Enumeration keys = cache.keys();
			while (keys.hasMoreElements()){
			    String key = keys.nextElement().toString();
				long msiLong = Long.parseLong(key);
				try {
					MSIAttribute _attribute = (MSIAttribute)CoreObject.persistence_loadByID(MSIAttribute.class, hibernateSession, msiLong);
					Vector values = (Vector)cache.get(key);
					//if (values.size() != 0) throw new Exception("is too small");
					%>
					<tr>
						<td><%=_attribute .getQuestion() %>
							<font style="font-size: xx-small;">
								<br>
								<b><%=_attribute.getMsiLabel() %></b>
								<br>
								<%=_attribute.getDescribtion() %>
							</font>
						<td>
							<%for (int a = 0; a < values.size(); a++){ %>
								<nobr><%=values.get(a) %> <%=_attribute.getExtension() %> </nobr><br>
							<%} %>
					<%
				} catch (Exception e){
				    
				}
			}
%>


<tr>
	<td>Classes:
	<td><a href="msi_classes.jsp?id=<%=id %>"><img src="pics/go.gif" border="0">
</table>






</body>
</html>