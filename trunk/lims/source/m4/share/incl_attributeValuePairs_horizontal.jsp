<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.communication.msi.AttributeValuePair"%>

<%
try {
    Logger.debug(this, "starting with " + _attributeValuePairs.length);
} catch (NullPointerException E){
    _attributeValuePairs = new AttributeValuePair[0];
}
	for (int asx = 0; asx < _attributeValuePairs.length; asx++){
	    %>
<td <%=color %>>
	<%try { %> 
	<%=_attributeValuePairs[asx].getValue()%> <%=_attributeValuePairs[asx].getExtension()%>
	<% if (_showLabels > 0){ %> 
		<font style="font-size: xx-small"> 
			<br>
			(<%=_attributeValuePairs[asx].getLabel1()%> - <%=_attributeValuePairs[asx].getLabel2()%>)
		</font> 
	<%	
	   }
	} catch (Exception e){
	    	e.printStackTrace();
	}
}
%>

<%Logger.debug(this, "finished");%>
