
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Iterator"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%></title>
</head>

<body>



<% // any session will be deleted
session.invalidate();



//Create a new session for the user.
session = request.getSession(true); 


%>


<table width="100%" height="100%" cellpadding="10" cellspacing="80" border="1">
	<tr align="center" valign="middle" > 
		<td width="100%" valign="middle">

<table align='center' border='0' cellpadding='4' width="85%">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<td align="center">
			<font style="font-variant: small-caps; font-size: 40pt;"><b><%=org.setupx.repository.Config.SYSTEM_NAME%></b></font><br>
			<font style="font-variant: small-caps; font-size: 20pt;">study design database for metabolomic projects</font>
		</td>
		<td width="2">
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>
<br>
			<table align="center" width="100%" border="0" >
				<tr>
				
<%
	Session hqlSession = PersistenceConfiguration.createSessionFactory().openSession();
//	int num_samples = (int)Integer.parseInt("" + hqlSession.createSQLQuery("select count(DISTINCT s.sampleID) as result from scanned_samples as s where s.label != '' and s.sampleID != ' ' and s.sampleID != '1'").addScalar("result", org.hibernate.Hibernate.INTEGER).uniqueResult());
	long num_experiment = Long.parseLong("" + hqlSession.createQuery("select count(*) from " + Promt.class.getName()).uniqueResult());
	long num_users = Long.parseLong("" + hqlSession.createQuery("select count(*) from " + UserDO.class.getName()).uniqueResult());
%>
				
				
					<!--  left column -->
					<td width="25%" valign="top">
						<table width="100%" >
							<tr>
								<th ><h3>data content
							<tr>
								<td>
									<strong><font class="small">as of <%=DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, Locale.US).format(new Date())%></font></strong>
									<font class="small">(California Time)</font>
								

							<tr>
								<td style="background-color:#FFFFFF">Public Samples
							<tr>
								<td style="background-color:#FFFFFF"><font class="small">
									<font class="small">
									<%
									String pubsamples = "select count(distinct setupXsampleID) as num from pubsample";				
									String numPubSamples = hqlSession.createSQLQuery(pubsamples).addScalar("num", org.hibernate.Hibernate.STRING).list().get(0).toString();
										%>	
										<%=numPubSamples  %> Samples incl. publicly available Annotation data. Browse the <a href="main_public.jsp">public experiments</a>.
									</font>
								</td>
							<tr>
								<td><strong><font class="small">&nbsp;</font></strong>


							<tr>
								<td style="background-color:#FFFFFF"><a href="advancedquery_dev.jsp?term=&type=1"><img align="bottom" border='0' src="pics/display.gif"></a>&nbsp;Compounds 
							<tr>
								<td style="background-color:#FFFFFF"><font class="small">
									<font class="small">
									<%
									String queryComp = "select distinct label as comp from pubattribute order by label limit 0,10";				
									Iterator iteratorComp = hqlSession.createSQLQuery(queryComp).addScalar("comp", org.hibernate.Hibernate.STRING).list().iterator();
									while (iteratorComp.hasNext()){
										String label = iteratorComp.next().toString();
										%>	
										<nobr><a href="advancedquery_dev.jsp?term=<%=label %>&type=1"><%=label %></a></nobr>
										<% if (iteratorComp.hasNext()) {%> 
											&nbsp;
											<%}
										} %>
										<nobr>&nbsp;...</nobr>
									</font>
								</td>
							<tr>


							<tr>
								<td><strong><font class="small">&nbsp;</font></strong>

							<tr>
								<td style="background-color:#FFFFFF"><a href="advancedquery_dev.jsp?term=&type=0"><img align="bottom" border='0' src="pics/display.gif"></a>&nbsp;Species
							<tr>
								<td style="background-color:#FFFFFF"><font class="small">
									<font class="small">
									<%
									String query = "select label.value as ncbilabel from formobject as ncbi, formobject as label where ncbi.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\" and label.PARENT = ncbi.uoid and label.value != \"\""
										+ " and label.internalPosition = 0 "
										+ " group by label.value ORDER by label.value limit 0, 10";				
									Iterator iterator2 = hqlSession.createSQLQuery(query).addScalar("ncbilabel", org.hibernate.Hibernate.STRING).list().iterator();
									while (iterator2.hasNext()){
										String label = iterator2.next().toString();
										if (label.indexOf("-") == -1){
											%>	
											<nobr><a href="advancedquery_dev.jsp?term=<%=label %>&type=0"><%=label %></a></nobr>
											<% if (iterator2.hasNext()) {%> 
												&nbsp;
											<%}
											} 
										}%>
										&nbsp;<nobr>...</nobr>
									</font>
								</td>
							<tr>
				<tr>
				<td colspan="21"></td>
				</tr>
								
							<tr>
								<th ><h3>news


							<tr>
								<td><strong><font class="small">Nov 1st, 2007 </font></strong>
							<tr>
								<td style="background-color:#FFFFFF">Metabolomics Standards Initiative (MSI)
							<tr>
								<td style="background-color:#FFFFFF"><font class="small"><%=org.setupx.repository.Config.SYSTEM_NAME%> supports the <a href="msi.jsp">Standards </a><img align="bottom" border='0' src="pics/arrow-icon.gif">for defined by the Metabolomics Standards Initiative (MSI).    
							<tr>
								<td><hr noshade="noshade">


							<tr>
								<td><strong><font class="small">Nov 1st, 2007 </font></strong>
							<tr>
								<td style="background-color:#FFFFFF"><img src="pics/rss3.gif"> Feeds 
							<tr>
								<td style="background-color:#FFFFFF"><font class="small"><a href="rss.jsp"> RSS Feeds </a><img align="bottom" border='0' src="pics/arrow-icon.gif"> are made available on <%=org.setupx.repository.Config.SYSTEM_NAME%>. <a href="http://www.google.com/search?q=define:+rss&hl=de&lr=&oi=definel&defl=en">What is RSS?</a>  
							<tr>
								<td><hr noshade="noshade">



							<tr>
								<td style="background-color:#FFFFFF">Software
							<tr>
								<td style="background-color:#FFFFFF"><font class="small">First Beta Version of <a href="m1.jnlp"><%=org.setupx.repository.Config.SYSTEM_NAME%> Orbiter</a><img align="bottom" border='0' src="pics/arrow-icon.gif"> is available. The Orbiter is used for communication with Laboratory Printer to label 3/8 inch Tags for Vials.
							<tr>
								<td><hr noshade="noshade">

								
							<tr>
								<td><strong><font class="small">June 1st, 2007 </font></strong>
							<tr>
								<td style="background-color:#FFFFFF">Query
							<tr>
								<td style="background-color:#FFFFFF"><font class="small"><%=org.setupx.repository.Config.SYSTEM_NAME%> has been updated in order to support public experiments and ontologies.
							<tr>
								<td><hr noshade="noshade">

								
							<tr>
								<td><strong><font class="small">May 20th, 2007 </font></strong>
							<tr>
								<td style="background-color:#FFFFFF">Tracking
							<tr>
								<td style="background-color:#FFFFFF"><font class="small">System has been equipped with a new tracking system in order to provide a full overview of the status of the lab.
							<tr>
								<td><hr noshade="noshade">

<!-- 
							<tr>
								<td><strong><font class="small">January 3-7, 2007</font></strong>
							<tr>
								<td style="background-color:#FFFFFF">Pacific Symposium on Biocomputing
							<tr>
								<td style="background-color:#FFFFFF"><font class="small">Paper for the <a href="http://psb.stanford.edu/">PSB</a> (January 3-7, 2007) has been accepted - read the abstract & the full paper <a href="paper.jsp">online <img align="bottom" border='0' src="pics/arrow-icon.gif"></a>.
							<tr>
								<td><hr noshade="noshade">
 -->
								
						</table>




					<!--  center column -->
					<td width="50%" valign="top">
						<table width="100%" bgcolor="white">
							<tr>
								<th colspan="4"><h3>Query
							<tr>
								<td colspan="4" align="center">
									<table width="100%" style="background-color: white" cellspacing="3">
												<% 
													boolean inclImage = false;
												%>
												<%@ include file="incl_q_input.jsp"%>
									</table>
							<tr>
								<th colspan="4"><h3>Welcome to <%=org.setupx.repository.Config.SYSTEM_NAME%>
							<tr>
								<td colspan="4">


							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><img  border='0' src="pics/go_search.gif">
								<td style="background-color:#FFFFFF"><b>What is in <%=org.setupx.repository.Config.SYSTEM_NAME%>?</b>
								<td style="background-color:#FFFFFF">
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">
								
								 <%=org.setupx.repository.Config.SYSTEM_NAME%> is a system that enables investigators to detail and set up a biological experiment. Right now it contains the following samples:
								 <br>
								 
								 <p align="center">
								 <% boolean show = false; %>
								 <a href="content_species.jsp#kingdom">
								 	<%@ include file="incl_topSpecies.jsp"%>
								 </a>	
								 </p>
								 
								<td style="background-color:#FFFFFF">
								<td style="background-color:#FFFFFF">


							<tr>
								<td colspan="4">


							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><img  border='0' src="pics/go_report.gif">
								<td style="background-color:#FFFFFF"><b>Who may submit experiments?</b>
								<td style="background-color:#FFFFFF">	
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">
								
								<b><i>You.</i></b> The front end is intuitive so it should be easy for you to enter your own study design in <%=org.setupx.repository.Config.SYSTEM_NAME%>. For collaborations with the Fiehn laboratory, consent on research strategies and cost-sharing is necessary between the principal investigators. 
								<td style="background-color:#FFFFFF">


							<tr>
								<td colspan="4">


							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><img  border='0' src="pics/go_locked.gif">
								<td style="background-color:#FFFFFF"><b>"I want to keep my data for myself!"</b>
								<td style="background-color:#FFFFFF">
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">Your data are safe and are not automatically available to the public. However, we request that data become publicly available when results are published in peer-reviewed scientific journals. If you consent, data may be shared with other databases, e.g. by request of regulatory or funding agencies.
								<td style="background-color:#FFFFFF">



							<tr>
								<td colspan="4">


							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><img  border='0' src="pics/go_msi.gif">
								<td style="background-color:#FFFFFF"><b>Share your Experiment!</b>
								<td style="background-color:#FFFFFF">
							<tr>
								<td style="background-color:#FFFFFF" colspan="2"><%=org.setupx.repository.Config.SYSTEM_NAME%> is part of ongoing efforts to standardize reporting requirements for metabolomic experiments, developed by the <a href="msi.jsp">MSI</a>. Check out the <a href="main_public.jsp">existing public experiments <img align="bottom" border='0' src="pics/arrow-icon.gif"></a>.
								<td style="background-color:#FFFFFF">

							<tr>
								<td colspan="4">

							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><img  border='0' src="pics/go_export.gif	">
								<td style="background-color:#FFFFFF"><b>Automated Anotation</b>
								<td style="background-color:#FFFFFF">
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">Annotation of GC-TOF mass spectral data is performed by the linked BinBase database. Experiments are scheduled for annotation from the <%=org.setupx.repository.Config.SYSTEM_NAME%> interface, and BinBase exports finished results again to <%=org.setupx.repository.Config.SYSTEM_NAME%> for user downloads.
								<td style="background-color:#FFFFFF">

							<tr>
								<td colspan="4">


							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><img  border='0' src="pics/go_new.gif">
								<td style="background-color:#FFFFFF"><b>Get started!</b>
								<td style="background-color:#FFFFFF">
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">To get started we will provide you with an account. Please enter your user information in the box on the right hand side, and we will get in contact with you as soon as possible.  

								<td style="background-color:#FFFFFF">
							<tr>
								<td colspan="4">


							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><a href="mailto:<%=org.setupx.repository.Config.SYSTEM_EMAIL %>"><img  border='0' src="pics/help.gif"></a>
								<td style="background-color:#FFFFFF"><b>Support</b>
								<td style="background-color:#FFFFFF">
								<td style="background-color:#FFFFFF">
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">If you have any questions, we are happy to help you. We continuously improve <%=org.setupx.repository.Config.SYSTEM_NAME%> to match study designs and to include query functions. We rely on your feedback! 
								<td style="background-color:#FFFFFF">



						</table>



					<!--  right column -->
					<td width="25%" valign="top">
						<table width="100%">
							<tr>
								<th colspan="2"><h3>Public Data</h3>
							</tr>
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">
									<font style="font-size: x-small;">A subset of the <%=num_experiment %> experiments is publicly available - including the whole experimental design and the annotated GC-TOF result data. 
									Use the link below to <b>query</b> the whole sytem for the samples that you are interessted in. 
							<tr>
								<td align="right" colspan="2" style="background-color:#FFFFFF" >
									<a href="main_rss.jsp"><img  border='0' src="pics/rss3.gif"></a> 
									<a href="main_public.jsp"><img  border='0' src="pics/go_search.gif"></a> 
								</td>

							</tr>
						</table>
						<br>
						<table width="100%">
						<form action="login" method="POST">
							<tr>
								<th colspan="2"><h3>Existing Users</h3>
							</tr>
							<tr><td style="background-color:#FFFFFF" colspan="2"><font class="small">Please enter your userID & password. In case you no not remember, please contact <a href="mailto:<%=org.setupx.repository.Config.SYSTEM_EMAIL %>">us</a>.
							<tr><td>login:    </td> 
								<td align="right"><input type="text" size="20" value="" name="<%=WebConstants.PARAM_ID%>"/></td>
							</tr>
							<tr><td>password: </td> 
								<td align="right"><input type="password"	 size="20" value="" name="<%=WebConstants.PARAM_PASSWORD%>"/></td>
							</tr>
							<tr><td align="right"></td> 
							<td align="right"><input type="image" src="pics/go.gif" value="login"/> </td>
						</tr>
						</form>
						</table>
						<br>
						<table width="100%">
						<form action="requestaccount.jsp">
							<tr ><th colspan="2"><h3>New Users*</h3></tr>
							<tr><td style="background-color:#FFFFFF" colspan="2"><font class="small">To create a new user account please enter a few informations about yourself and you will receive your personal account by email.
							<tr><td colspan="1">name
								<td align="right"><input type="text" name="name">
							<tr><td colspan="1">organisation
								<td align="right"><input type="text" name="orga">
							<tr><td colspan="1">eMail
								<td align="right"><input type="text" name="email">
							<tr><td align="right"></td> 
							<td align="right"><input type="image" src="pics/go.gif" value="login"/> </td>
							<tr><td align="left" colspan="2">
								<font class="small">* Accounts are only needed for <b>active collaborators</b> who submit samples for data acquisition to either the UCD metabolomics core laboratory or the UCD Fiehn metabolomics research laboratory.</font>
						</form>
						</table>
				</tr>
				<tr>
				<td colspan="21"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>