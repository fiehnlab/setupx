<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.Iterator"%>

<%@ include file="incl_searchheader.jsp"%>



<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<table align='center' border='0' cellpadding='4' width="60%">
	<%
	// q_variations.jsp?var=variations[k]&term=searchTerm
	String searchTerm = request.getParameter("term");    
	String variation = request.getParameter("var");

	Session s = CoreObject.createSession();
	StringBuffer query = new StringBuffer("select question as q, value as v, classid as classID from variationNonFiltered where ");
	if (variation != null && variation.length() > 0) {
	    query.append("question = \"" + variation + "\" and ");
	}
	query.append("value like \"%" + searchTerm + "%\" group by classid, value order by question, value");
	List list = s.createSQLQuery(query.toString())
								.addScalar("q", Hibernate.STRING)
								.addScalar("v", Hibernate.STRING)
								.addScalar("classid", Hibernate.LONG)
								.list(); 
	Iterator iterator = list.iterator();
	while(iterator.hasNext()){
	    Object[] objects = (Object[])iterator.next() ;
	    try {	    
			long classID = Long.parseLong("" + objects[2]);
		    long experienmentID = new SXQuery().findPromtIDbyClazz((int)classID);
		    String title = new SXQuery().findPromtTitleByPromtID(experienmentID);
		    String design = new SXQuery().findPromtStructure(experienmentID);
		    String species = new SXQuery().findSpeciesbyPromtID(experienmentID).get(0).toString();
		    String classDef = new SXQuery().findClassInformationString(classID);
		    %>
		<tr>
		    <td><%=objects[0]%> 
		    <td><%=objects[1]%> 
		    <td>Class <%=classID %>
		    <%if (classDef.length() < 100){ %>
		    	<br><font class="small"><%=Util.replace(classDef, "\n", "<br>")%></font>
		    <%} %>
		    <td>Exp <%=experienmentID %>
		    <td><%=title%>
		    <td><%=species%>
		    <td><%=design%>
		    <%
		}catch (Exception e){
		    
		}
	}
	%>