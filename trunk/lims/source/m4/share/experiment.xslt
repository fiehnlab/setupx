<!-- edited with XMLSPY v5 rel. 4 U (http://www.xmlspy.com) by bla (foo) -->
<xsl:stylesheet version="1.0" xmlns:m0="http://www.mpimp-golm.mpg.de/m1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ino="http://namespaces.softwareag.com/tamino/response2">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<!-- the main experiment-->
	<xsl:template match="experiment">
		<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
		<table border="1" align="center" width="80%">
			<tr>
				<td colspan="7">
					<h3>experiment id:<xsl:value-of select="/experiment/@ino:id"/>
					</h3>
				</td>
			</tr>
			<tr>
				<form action="generate">
					<td/>
					<td align="center" colspan="2">
						<input type="hidden" value="@referenceID"/>
						<input type="image" src="pics/acquisition.gif"/>
					</td>
				</form>
				<form action="info.jsp">
					<td align="left" colspan="3">generate acquistion file for leco software</td>
					<td align="center">
						<input type="image" src="pics/info.gif"/>
					</td>
				</form>
			</tr>
			<tr>
				<form action="TODO">
					<td/>
					<td align="center" colspan="2">
						<input type="hidden" value="@referenceID"/>
						<input type="image" src="pics/xml.gif"/>
					</td>
				</form>
				<form action="info.jsp">
					<td align="left" colspan="3">generate XML document of this experiment</td>
					<td align="center">
						<input type="image" src="pics/info.gif" alt="info"/>
					</td>
				</form>
			</tr>
			<tr>
				<td colspan="7">
					<h4>definition</h4>
				</td>
			</tr>
			<xsl:apply-templates select="definition/@*"/>
			<form action="infostatus.jsp" method="post">
				<tr>
					<td colspan="7">
						<h4>owner</h4>
					</td>
					<xsl:apply-templates select="owner"/>
				</tr>
				<tr>
					<td colspan="8">
						<h4>samples</h4>
					</td>
					<tr>
						<th>name</th>
						<th>ok</th>
						<th>failed</th>
						<th>problematic</th>
						<th>not measured</th>
						<th>comment</th>
						<th>export</th>
					</tr>
					<!-- DEBUGGIN JUST A SAMLE THAT HAS ALREADY BEEN MEASSURED -->
					<tr>
						<td align="center">000000eaTest</td>
						<td width="80" align="center">
							<input checked="checked" type="radio" name="test"/>
						</td>
						<td width="80" align="center">
							<input type="radio" name="test"/>
						</td>
						<td width="80" align="center">
							<input type="radio" name="test"/>
						</td>
						<td width="80" align="center">
							<input disabled="disabled" type="radio" name="{@referenceID}"/>
						</td>
						<td align="center">
							<input name="comment" type="text"/>
							<input type="button" value="editor" name="edit"/>
						</td>
						<td>
							TEST
						</td>
					</tr>
					<!-- DEBUGGIN JUST A SAMLE THAT HAS ALREADY BEEN MEASSURED -->
					<xsl:apply-templates select="sample"/>
				</tr>
			</form>
			<tr>
				<td colspan="7">
					<h4>status</h4>
				</td>
				<xsl:apply-templates select="information/@status"/>
			</tr>
			<tr>
				<td colspan="7">
					<h4>processing instructions</h4>
				</td>
				<xsl:apply-templates select="processing"/>
			</tr>
		</table>
	</xsl:template>
	<!-- template to decribe a person -->
	<xsl:template match="owner">
		<tr>
			<td colspan="6">
				<center>
					<xsl:value-of select="firstname"/>_<xsl:value-of select="lastname"/>
				</center>
			</td>
		</tr>
	</xsl:template>
	<!-- template to decribe single sample     experiment/sample/@referenceID         -->
	<xsl:template match="sample">
		<tr>
			<td align="center">
				<a href="query?name={@referenceLabel}&amp;id={@referenceID}&amp;docType=2&amp;queryType=1">
					<center>
						<xsl:value-of select="@referenceLabel"/>
					</center>
				</a>
			</td>
			<td width="80" align="center">
				<input disabled="disabled" type="radio" name="{@referenceID}"/>
			</td>
			<td width="80" align="center">
				<input disabled="disabled" type="radio" name="{@referenceID}"/>
			</td>
			<td width="80" align="center">
				<input disabled="disabled" type="radio" name="{@referenceID}"/>
			</td>
			<td width="80" align="center">
				<input checked="checked" type="radio" name="{@referenceID}"/>
			</td>
			<td align="center">
				<input name="comment" type="text"/>
				<input type="button" value="editor" name="edit"/>
			</td>
			<form action="TODO">
				<td>
					<input type="hidden" value="@referenceID"/>
					<input type="image" src="pics/XML.gif"/>
				</td>
			</form>
		</tr>
	</xsl:template>



	<xsl:template match="information/@status">
		<tr>
			<td>status</td>
			<td colspan="5">
				<center>The experiment is currenty in <a href="infostatus.jsp">status</a>:  .<b>
						<xsl:value-of select="."/>
					</b>
				</center>
			</td>
			<form action="info.jsp">
				<td align="center">
					<input type="image" src="pics/info.gif"/>
				</td>
			</form>
		</tr>
		<tr>
			<td>
			</td>
			<td colspan="5" align="left">
				<table border="1" width="60%">
					<tbody>
						<tr>
							<td>private</td>
							<td align="center">
								<xsl:choose>
									<xsl:when test=".=1">
										<img src="pics/check.gif"/>
									</xsl:when>
									<xsl:otherwise>
										<img src="pics/o.gif"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<form action="info.jsp">
								<td align="center">
									<input type="image" src="pics/info.gif"/>
								</td>
							</form>
						</tr>
						<tr>
							<td>waiting for operator doing the preperation </td>
							<td align="center">
								<xsl:choose>
									<xsl:when test=".>1">
										<img src="pics/check.gif"/>
									</xsl:when>
									<xsl:otherwise>
										<img src="pics/x.gif"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<form action="info.jsp">
								<td align="center">
									<input type="image" src="pics/info.gif"/>
								</td>
							</form>
						</tr>
						<tr>
							<td>waiting for operator to be measured</td>
							<td align="center">
								<xsl:choose>
									<xsl:when test=".>2">
										<img src="pics/check.gif"/>
									</xsl:when>
									<xsl:otherwise>
										<img src="pics/x.gif"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<form action="info.jsp">
								<td align="center">
									<input type="image" src="pics/info.gif"/>
								</td>
							</form>
						</tr>
						<tr>
							<td>waiting for machine to be measured</td>
							<td align="center">
								<xsl:choose>
									<xsl:when test=".>3">
										<img src="pics/check.gif"/>
									</xsl:when>
									<xsl:otherwise>
										<img src="pics/x.gif"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<form action="info.jsp">
								<td align="center">
									<input type="image" src="pics/info.gif"/>
								</td>
							</form>
						</tr>
						<tr>
							<td>processed</td>
							<td align="center">
								<xsl:choose>
									<xsl:when test=".>4 ">
										<img src="pics/check.gif"/>
									</xsl:when>
									<xsl:otherwise>
										<img src="pics/x.gif"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<form action="info.jsp">
								<td align="center">
									<input type="image" src="pics/info.gif"/>
								</td>
							</form>
						</tr>
					</tbody>
				</table>
			</td>
			<td>
			</td>
		</tr>
	</xsl:template>
	<!--   the old one   
	<xsl:template match="*">
		<tr>
			<td colspan="6">
				<center>
					<xsl:value-of select="."/>
					<xsl:value-of select="local-name(*)"/>
				</center>
			</td>
		</tr>
	</xsl:template>
	-->
	<xsl:template match="*|@*">
		<tr>
			<td>
				<b>
					<xsl:value-of select="local-name(.)"/>
				</b>
			</td>
			<td>
				<!-- TODO - add a chooser - if there are other elements do not print there values ... -->
				<xsl:value-of select="."/>
				<table border="1">
					<tbody>
						<xsl:apply-templates select="*|@*"/>
					</tbody>
				</table>
			</td>
		</tr>
	</xsl:template>
	<!-- template to decribe the definiton-->
	<xsl:template match="definition/@*">
		<tr>
			<td colspan="3" align="right" valign="top">
				<xsl:value-of select="local-name(.)"/>
			</td>
			<xsl:choose>
				<xsl:when test="local-name(.) = 'general-comment'">
					<td colspan="3" align="center">
						<textarea name="" cols="80" rows="8" readonly="true">
							<xsl:value-of select="."/>
						</textarea>
					</td>
				</xsl:when>
				<xsl:otherwise>
					<td colspan="3">
						<xsl:value-of select="."/>
					</td>
				</xsl:otherwise>
			</xsl:choose>
			<td/>
		</tr>
	</xsl:template>
	<!-- template to decribe the processing -->
	<xsl:template match="processing">
		<tr>
			<td colspan="7">
				<center>
					<xsl:value-of select="."/>
					<xsl:value-of select="local-name(*)"/>
				</center>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="comment"> comment	TODO !!!!!!!!!!!!!!!
	</xsl:template>
</xsl:stylesheet>
