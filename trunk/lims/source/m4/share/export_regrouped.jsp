
<%@ include file="checklogin.jsp"%>

<%@page import="edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass"%>
<%@page import="java.util.HashSet"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.core.communication.leco.ColumnInformationNotAvailableException"%>


<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.binbase.trigger.ClassSet"%>
<%@page import="org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector"%>
<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.core.communication.leco.ColumnDetector"%>
<%@page import="org.setupx.repository.core.communication.leco.ColumnInformation"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

</head>


<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width='60%'>
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Export</h2>
			Summary
			<br/>
			<font size="-2"></font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>




<%
Hashtable classes = new Hashtable();

int promtID;
String columnID;
try {
	promtID  = Integer.parseInt(request.getParameter("promt"));
	columnID = request.getParameter("column");
	if (columnID.compareTo("null") == 0) throw new ColumnInformationNotAvailableException("");
} catch (Exception e){
    org.setupx.repository.core.util.logging.Logger.warning(this, "unable to find column - using RTX5");
    columnID = "production_rtx5";
}
%>




<%
Enumeration attributNames = request.getSession().getAttributeNames();

while (attributNames.hasMoreElements()){
    String name = attributNames.nextElement().toString();

    String value = request.getSession().getValue(name).toString();
    Logger.log(this, name + ": " + value);

	try {
	    Integer.parseInt("" + value);
	    // creating new empty hashset
	    
	    if (name.compareTo("pos") != 0 && !value.startsWith("-1") && name.compareTo("MAX_NR_GROUPS") != 0){
			if (!classes.containsKey("" + value)){
			    classes.put(""+value, new HashSet());
			    Logger.log(this, "creating group " + value);
			}

			HashSet set = (HashSet)classes.get("" + value);
		    set.add("" + name);
		    Logger.log(this, "adding " + name + " to " + value);
	    }
	} catch (Exception e){
	    
	}
}
%>



<%
ExperimentClass[] export_groups = new ExperimentClass[classes.size()];

%>
<%@page import="edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<table align='center' border='0' cellpadding='4' width='60%'>
		<tr>
	    	<td style="background-color: white" rowspan="21">
 	    	<td style="background-color: white" >group
 	    	<td style="background-color: white" colspan="21" align="center">Assigned Samples 

	    	
			


<% for (int i = 0; i < classes.size(); i++){
	    HashSet set = (HashSet)classes.get("" +i);
	    if (set != null){
	    %>
	    <tr>
	    	<th>group <%=i%>
	    	<%
			    ExperimentSample[] samples = new ExperimentSample[set.size()];	
	    		Iterator iter = set.iterator();
	    		int c = -1;
	    		while (iter.hasNext()){
	    		    String v = iter.next().toString() ;
	    		    c++;
	    			samples[c] = new ExperimentSample();
	    			Logger.debug(this, "looking for fileID for " + v);
	    			try {
		    			samples[c].setId("" + new SXQuery().findSampleIDByAcquisitionName(v));
		    			samples[c].setName(v);
	    			} catch (Exception e){
	    			    e.printStackTrace();
		    			samples[c].setId("-unknown-");
		    			samples[c].setName(v);
	    			}
	    			%>
	    		    <td><%=samples[c].getId()%><font class="small"><br><%=samples[c].getName()%></font>
	    		    <%
	    		}
    			export_groups[i] = new ExperimentClass();
    			export_groups[i].setId("" + i + "_" + new Date().getTime());
    			
    			/*
	    			long timestamp = new Date().getTime();
	    			export_groups[i].setId("" + timestamp); // 
	    			Logger.log(this, "creating id for class " + timestamp);
    			*/
    			
    			export_groups[i].setSamples(samples);
	    	%>
	<%	}
	}
%>

<%
ColumnInformation columnInformation = ColumnDetector.getColumnInformationByID(columnID);
Logger.debug(this, "columnID:" + columnID);
Logger.debug(this, "columnInformation:" + columnInformation);


ClassSet export_classset = new ClassSet(columnInformation);

export_classset.setClasses(export_groups);
String filename = "regrouped_" + user.getUsername() + "_" + new Date().getTime();
export_classset.setId(filename);
export_classset.setIncrease(0);
%>
</table>
<%

BinBaseServiceConnector binBaseSerivce = new BinBaseServiceConnector(Config.BINBASE_SERVER);
binBaseSerivce.launchExport((ClassSet)export_classset, false);
%>
Finished
<%@ include file="footer.jsp"%>
