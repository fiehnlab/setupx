<form name="sample" method="post" action="upload">
	<h2 class="tableheader1" id="related" align="center">related documents</h2>
	<table width="80%" align="center">
		<th colspan="4">Currently the following documents are stored on the server:</th>
		<tr>
			<td colspan="4">The files, that are related with this experiment in any way, are stored on the server, so that everyone that has the possibility to access the system can up- and download the documents.</td>
		</tr>
		<tr>
			<td colspan="4"><small>If you are looking for documents that are related to one of the samples, have a look at the detailview of the sample. All documents will be listed there.</small></td>
		</tr>
		<tr>
			<td width="30"></td>
			<td><img src="pics/GenDoc2.gif" alt="selected document"</td>
			<td><a href="pics/GenDoc2.gif">Final_Report.doc</a></td>
			<td width="30"></td>
		</tr>
		<tr>
			<td ></td>
			<td><img src="pics/GenDoc2.gif" alt="selected document"</td>
			<td><a href="pics/GenDoc2.gif">target.doc</a></td>
			<td></td>
		</tr>
		
		
		<th colspan="4">Upload document</th>
		<tr>
			<td colspan="4">To transfer a file, containing information about this experiment, just select the file and press upload. Then the file will be transfered to the server and will be visible to all users that have the right to look at this experiment.</td>
</tr>
		<tr>
			<td ></td>
			<td colspan="2"">
				<form action="upload" method="post" enctype="multipart/form-data">
					<div align="center">
					    Select your file to upload on the server: <br></br>
					      <input type="file" name="Name" accept="Mime-Type"/>
						<input type="submit" value="upload"/>
					</div>
				</form>
			</td>
			<td/>
		</tr>
	</table>
</form>
