<%@page import="org.setupx.repository.core.query.QueryMaster"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>

<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.web.forms.label.Label"%>
<%




	List _list = CoreObject.createSession()
				.createSQLQuery(__query)
				.addScalar("c", Hibernate.INTEGER)
				.addScalar("value", Hibernate.STRING)
				.addScalar("samples", Hibernate.INTEGER)
				.list();
	
	Iterator listIter = _list.iterator();
	
	// data and values
	String url_value_1 = "chd=t:";
	String url_value_2 = "chd=t:";
	String url_label_1 = "chl=";
	String url_label_2 = "chl=";
	boolean isFirst = true;

	while (listIter.hasNext()){
	    Object[] objects = (Object[])listIter.next();
		
	    String label = objects[1] + "";
	    String value = objects[0] + ""; 
	    String nr_samples = objects[2] + ""; 
	        
	    // creating the pie chart
	    // http://chart.apis.google.com/chart?cht=p3&chd=s:Uf9a&chs=200x100&chl=A|B|C|D
	    //  chd=t:10.0,58.0,95.0|30.0,8.0,63.0
	    int numberOfExp = Integer.parseInt(value);
	    
	    if (numberOfExp > MINNUMBER_OF_EXPERIMENTS
	    	&&
	    	label.compareTo("Bos taurus") != 0
	    ){
		    if (!isFirst) {
		        url_value_1 = url_value_1.concat(",");
		        url_value_2 = url_value_2.concat(",");
		    }
		    url_value_1 = url_value_1.concat(value);
		    url_value_2 = url_value_2.concat((Integer.parseInt(nr_samples)/10	) + ""	 );
		    
		    if (!isFirst) {
		        url_label_1 = url_label_1.concat("|");
		        url_label_2 = url_label_2.concat("|");
		    }
		    
		    url_label_1 = url_label_1.concat(label + " (" + value +  ")");
		    url_label_2 = url_label_2.concat(label + " (" + nr_samples +  ")");
		    
		    if(isFirst) isFirst = false;
	    }

	    Logger.log(this, "label: " + label + " value: " + value);
	}

    
	String URL_START = "http://chart.apis.google.com/chart?";
	String CHART_NAME_1 = "chtt=experiments per species";
	String CHART_NAME_2 = "chtt=samples per species";
	String CHART_TYPE_PIE = "cht=p&chco=0000ff";
	if (IN3D) CHART_TYPE_PIE = "cht=p3&chco=0000ff";
	String CHART_RESOLUTION = "chs=600x250";
	if (IN3D) CHART_RESOLUTION = "chs=600x200";
	String AND = "&";
	
    // http://chart.apis.google.com/chart?cht=p3&chd=s:Uf9a&chs=200x100&chl=A|B|C|D
	String url1 = URL_START.concat(CHART_TYPE_PIE).concat(AND).concat(CHART_NAME_1).concat(AND).concat(url_value_1).concat(AND).concat(CHART_RESOLUTION).concat(AND).concat(url_label_1);
	String url2 = URL_START.concat(CHART_TYPE_PIE).concat(AND).concat(CHART_NAME_2).concat(AND).concat(url_value_2).concat(AND).concat(CHART_RESOLUTION).concat(AND).concat(url_label_2);
%>


	<%@page import="org.setupx.repository.core.util.logging.Logger"%>

