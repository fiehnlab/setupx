<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 org.setupx.repository.core.communication.binbase.BBConnector,
                 java.util.List,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBI_Tree"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


	


<!--  head containing logo and description --> 
	<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td align="center" width="2">
			<a href=".">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%></h2>
			Browse the repository<br/>
			<font size="-2"></font>
		</th>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<%@ include file="navi_admin.jsp"%>

	<table align='center' border='0' cellpadding='4' width="60%">

		<tr>
			<th colspan="15">Species stored in <%=org.setupx.repository.Config.SYSTEM_NAME%>:	</th>
		</tr>
	    
		<tr>
			<td></td>
			<td colspan="4" align="center">
				<font class="small">
				<%
				String query = "select label.value as ncbilabel from formobject as ncbi, formobject as label where ncbi.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\" and label.PARENT = ncbi.uoid and label.value != \"\""
				+ " and label.internalPosition = 0 "
				+ " group by label.value ORDER by label.value ";

				
				org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();
				Iterator iterator2 = hqlSession.createSQLQuery(query).addScalar("ncbilabel", org.hibernate.Hibernate.STRING).list().iterator();
				while (iterator2.hasNext()){
					String label = iterator2.next().toString();
					%>	
					<nobr><a href="browse.jsp?species=<%=label %>"><img src='pics/arrow-icon.gif' border="0" valign="middle" align="center"><%=label %></a></nobr> 
					
					<% if (iterator2.hasNext()) {%> 
						&nbsp;
						<%}
					} %>
				</font>
			</td>
			<td></td>
		</tr>	    

		<tr>
			<td></td>
			<td colspan="4"></td>
			<td></td>
		</tr>
		
		<% 
		String search_string = "man";
			try {
				  search_string = request.getParameter("species");
				  if (search_string.compareTo("null") == 0) throw new Exception();
			}catch (Exception e){
			    search_string = "Arabidopsis thaliana";
			}
		  
	      int ncbiSpeciesID;
	      ncbiSpeciesID = NCBIConnector.determineNCBI_Id(search_string);
	      
	      NCBIEntry entry = NCBIConnector.determineNCBI_Information(ncbiSpeciesID);

		  List promtIDList = new SXQuery().findPromtIDbySpecies(ncbiSpeciesID);
	      Iterator iterator = promtIDList.iterator();
	      %>

	      <tr>
	      	<th>
	      	<th colspan="4">Result for : <%=search_string %></th>
	      	<th>
	      </tr>
	      <tr>
	      	<td>
	      	<td>Species name:</td>
			<td colspan="3" align="center" style="background-color:#FFFFFF"><b><%=entry.getName() %></b>
			<td>
		  </tr>
	      <tr>
	      	<td>
	      	<td>Rank</td>
			<td colspan="3" align="center" style="background-color:#FFFFFF"><%=entry.getRank() %>
			<td>
		  </tr>
	      <tr>
	      	<td>
	      	<td>Type</td>
			<td colspan="3" align="center" style="background-color:#FFFFFF"><%=entry.getSpeciesTypeName() %>
			<td>
		  </tr>	
	      <tr>
	      	<td>
	      	<td>Parent</td>
			<td colspan="3" align="center" style="background-color:#FFFFFF">
					<a href="browse.jsp?species=<%=entry.getParent().getName() %>"> <%=entry.getParent().getName() %> </a>
					<font class="small"> [<%=entry.getParent().getSpeciesTypeName() %>]
					<%
						int _size = new SXQuery().findPromtIDbySpecies(entry.getParent().getTaxID()).size(); 
						if (_size > 0){
						    %>
						    <strong><%=_size %> Experiments</strong>
						    <% 
						}
					%></font>
			<td>
		  </tr>
	      <tr>
	      	<td>
	      	<td>Splitpoint</td>
			<td colspan="3" align="center" style="background-color:#FFFFFF"><%=NCBI_Tree.findGroup(entry.getParent().getName()).getName()%> 
			<td>
		  </tr>
		  <!-- 
		  <tr>		  
			<td></td>
			<td>Experiments:</td>
			<td colspan="3" align="center" style="background-color:#FFFFFF"><b><%=promtIDList.size() %>
	      	<td>
	      </tr> -->

		  <tr>
		  	<th>	      
		  	<th>Experiment	      
		  	<th>Samples   
		  	<th>Classes
		  	<th>Summary   
		  	<th>	      
		  </tr>
		  <tr>
		  	<th>	      
		  	<th colspan="4"><font class="small">The following experiments have been completed. </font>   
		  	<th>	      
		  </tr>
		      
	      <% 
	      while (iterator.hasNext()) {
	         long promtID = Long.parseLong(""+ iterator.next());
	         
	         // checking if the experiment is done
			 int finishedSamples = new SXQuery().determineSamplesFinishedByPromtID(promtID);
	         
	         if (finishedSamples > 0){
				     String titleString = new SXQuery().findPromtTitleByPromtID(promtID);
				     String abstractString = new SXQuery().findPromtAbstractByPromtID(promtID);
					 int classes = new SXQuery().findClazzIDsByPromtID(promtID).size();
					 int samplesTotal = new SXQuery().findSampleIDsByPromtID(promtID).size();
					 
					 
					 PromtUserAccessRight accessRight = null;
					 try {
					     accessRight = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID(),promtID);
					 } catch (Exception e ){
						 accessRight = new PromtUserAccessRight();
						 accessRight.setAccessCode(0);
					 }
					 
				  %>     
		   	 	<td></td>
				<th align="center">
					<font class="small"><b>ID:<%=promtID %></b></font>
					<% if (accessRight.getAccessCode()>0) {%>
						<%=Promt.createShortAbstractHTML(promtID, 160)%>
					<%} else { %>
						<font color="grey"><i> non public experiment </i> <img  border='0' src="pics/warning.gif"> <br></font><font class='small' color="grey"><I>This experiment is not publicly available yet.</I></font>
					<%} %>
				</th>
		
				<!-- number of samples -->
				<td align="center" style="background-color:#FFFFFF"><%=samplesTotal %></td>
		
				<!-- number of classes -->
				<td align="center" style="background-color:#FFFFFF"><%=classes %></td>
		
					<% if (accessRight.getAccessCode()>0) {%>
						<td align="center" style="background-color:#FFFFFF"><a href="load?id=<%=promtID %>&action=<%=PromtUserAccessRight.READ%>"><img  border='0' src="pics/go.gif"></a></td>
					<%} else { %>
						<td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_locked.gif"></td>
					<%} %>
		   	 	<td></td>
		   	 	</tr>
			<%   
	         } // only when finished
			}
	      %>
	    </tr>
		<tr>
			<th colspan="15"></th>
		</tr>
		<tr>
			<td></td>
			<td colspan="4"></td>
			<td></td>
		</tr>

	    
	    
	</table>

<%@ include file="footer.jsp"%>
