<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.web.forms.FormObject"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Clazz"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="java.util.Date"%>
<%@page import="org.setupx.repository.core.communication.status.Status"%>
<%@page import="org.setupx.repository.core.communication.status.SampleShippedStatus"%>
<%@page import="org.setupx.repository.core.communication.status.SampleArrivedStatus"%>
<%@page import="org.setupx.repository.core.communication.status.LabStatus"%>
<%@page import="org.setupx.repository.core.communication.status.PersistentStatus"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.apache.tools.ant.taskdefs.WhichResource"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 org.setupx.repository.core.communication.binbase.BBConnector,
                 java.util.List,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.util.logging.Logger,
                 org.hibernate.criterion.Expression,
                 org.setupx.repository.server.persistence.*"%>

<%@page import="org.setupx.repository.web.forms.PromtCreator"%>
<%@page import="org.setupx.repository.core.communication.status.ResultReceivedStatus"%>
<html>

<%@ include file="checklogin.jsp"%>


<%@page import="java.util.HashSet"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>




<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">

			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%></h2>
			Tracking<br/>
			<font size="-2">Below you should enter the ID of the sample, class, experiment of which you want to update status.</font>
		</th>

		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>

		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>
<%
	long id = 0;
	int type = 0;
	try {
		id = Integer.parseInt(Util.replace("" + request.getParameter("id")," ",""));
		org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();
		String typeString = (String) hqlSession.createSQLQuery("select discriminator FROM formobject where uoid = \"" + id + "\"").addScalar("discriminator", org.hibernate.Hibernate.STRING).list().get(0);
		
	           if (typeString.compareTo( Promt.class.getName()) == 0){  
			type = 1;
		} else if (typeString.compareTo( Clazz.class.getName()) == 0){
			type = 2;
		} else if (typeString.compareTo(Sample.class.getName()) == 0){
			type = 3;
		}
	}catch (Exception e){
	    id = 0;
        e.printStackTrace();
	}
	
	int selectedEventType = 0;
	try{
	    selectedEventType = Integer.parseInt(Util.replace("" + request.getParameter("eventtype")," ",""));
	} catch(Exception e){
	    //e.printStackTrace();
	    Logger.err(this, "no event defined");
	}
	Logger.debug(this, "selectedEventType: " + selectedEventType);
	
	
	// SAVING

	try {
		String go = request.getParameter("go");
		Logger.debug(this, go);
		if ("ok".compareTo(go) == 0){
			Logger.debug(this, "going to save.");
			// yeayea save it!
			PersistentStatus status = null;
		    
		    // determine the new type of status
		    //selectedEventType
		    switch(selectedEventType){
		    case 0:
		        // shipping
		        status = new SampleShippedStatus(0, user, read(request,"ta1", "-no message(1)-"));
		        break;
		    case 1:
		        // arrived
		        String _location = read(request,"loc1", "");
		        if (_location.compareTo("other") == 0 || _location.length()<2){
		            _location = read(request,"loc2","--");
		        }

		        status = new SampleArrivedStatus(0, user, read(request,"ta2", "-no message(2)-"), _location);
		        break;
		    case 2:
		        // lab
		        status = new LabStatus(0, user, read(request,"ta3", "-no message(3)-"));
		        break;
		    }
		    
		    // determine for what it is (class, sample, promt, ...)
		    // type
			switch(type){
			case 1:
			    // promt
			    Iterator sampleIDs = new SXQuery().findSampleIDsByPromtID((int)(id)).iterator();
				while(sampleIDs.hasNext()){
				    long sampleID = Long.parseLong("" + sampleIDs.next());
				    PersistentStatus clonedStatus = status.createClone();
				    clonedStatus.setSampleID(sampleID);
				    clonedStatus.update(true);
				}
			    pageContext.forward("load?id=" + id + "&action=20");
			    break;

			case 2:
			    // class
			    sampleIDs = new SXQuery().findSampleIDsByClazzID((int)(id)).iterator();
				while(sampleIDs.hasNext()){
				    long sampleID = Long.parseLong("" + sampleIDs.next());
				    PersistentStatus clonedStatus = status.createClone();
				    clonedStatus.setSampleID(sampleID);
				    clonedStatus.update(true);
				}
			    id = new SXQuery().findPromtIDbyClazz((int)id);
			    pageContext.forward("load?id=" + id + "&action=20");
			    break;

			case 3:
			    // sample
				status.setSampleID(id);
			    status.update(true);
				pageContext.forward("sample_detail.jsp?id=" + id);
			    break;

			default:
			    break;
			}
		}
	} catch (Exception ex){
	    ex.printStackTrace();
	}
%>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<table align="center" width="60%" cellpadding="5">	
<tr>
	<th>overview
	<td colspan="2" valign="top" style="background-color:white; " align="center">
		<a href="sample_status_overview.jsp">to get an overview of the whole lab and the progess of the samples & experiments.</a>

	<tr>
		<td style="background-color:#6682B6" colspan="21">&nbsp;&nbsp;&nbsp;</td>
	
<tr>

<%
%>

<form action="sample_status_create.jsp">


	<th>ID</td>
	<td><input type="text" name="id" value="<%=id%>" onblur="document.forms[1].submit()"> </td>
	<td width = "200" rowspan="10" valign="top" style="background-color:white; " align="center">
	
	
		<% if (selectedEventType == 0){ %>
			<img src="pics/package.jpg"><br>
			<b>Note:</b>:<br>
			Sample was shipped by
			<%=user.getDisplay() %>
			at <%=DateFormat.getTimeInstance(DateFormat.SHORT, Locale.US).format(new Date())%>.
			<%} %>
		<% if (selectedEventType == 1){ %><img src="pics/freezer.jpg" width="200">
			<b>Note:</b>:<br>
			Sample was received and stored in a freezer by
			<%=user.getDisplay() %>
			at <%=Util.getTime() %>.
			<%} %>
		<% if (selectedEventType == 2){ %><img src="pics/BiocratesNL.jpg">
			<b>Note:</b>:<br>
			Sample was prepared by 
			<%=user.getDisplay() %>
			at <%=Util.getTime() %>.
		<%} %>
		
		


<tr>
	<th>type</td>
	<td>
<%
	String readonly = "";
	String disabled = "";


	switch(type){
	case 1:
	    out.println("<input type=\"text\" name=\"type\" readonly=\"readonly\" value= Experiment>");
	    out.println("<br>");	
	    out.println("<font class=\"small\">Info:    " + new SXQuery().findSampleIDsByPromtID(id).size() + " samples in this experiment.</font>");
	    out.println("<br>");	
	    out.println("<font class=\"small\">By selecting the whole experiment you will set the status for ALL samples in this experiment.</font>");
	    break;

	case 2:
	    out.println("<input type=\"text\" name=\"type\" readonly=\"readonly\" value=\"Class\">");
	    out.println("<br>");	
	    out.println("<font class=\"small\">Info:    " + new SXQuery().findSampleIDsByClazzID((int)id).size() + " samples in this class.</font>");
	    out.println("<br>");	
	    out.println("<font class=\"small\">By selecting the whole class you will set the status for ALL samples in this class.</font>");
	    break;

	case 3:
	    out.println("<input type=\"text\" name=\"type\" readonly=\"readonly\" value= Sample>");
	    break;

	default:
	    out.println("<input type=\"text\" name=\"type\" readonly=\"readonly\" value=-unknown- >");
		readonly = "readonly=\"readonly\"";
		disabled = "disabled=\"disabled\"";
	    break;
	}
%>

<tr>
<th>event
<td>
<%



Status[] statusEvents= new PersistentStatus[]{
        new SampleArrivedStatus(),
        new SampleShippedStatus(),
        new LabStatus()
        };
%>

		<% 
		//<% if(access.getExperimentID() == promtID){ selected } >
		for(int i = 0; i < statusEvents.length; i++){ %>
			<input <%=disabled %>
				type="radio" 
				name="eventtype" 
				value="<%=i%>"
				onChange="document.forms[1].submit()" 
				<%if (selectedEventType == i) {%>checked="checked"<%} %>
				>
			<%=statusEvents[i].createDisplayString()%> 
			<br>
		<%} %>
		<% if(user.isMasterUser(user.getUOID()) && type == 1){ %>
			<a href="sample_status_result_received.jsp?promtID=<%=id %>">result files were not recognized on arrival.</a>
		 	
		<%} %>
		


<!--  customized fields -->
<% 
switch(selectedEventType){ 
case 0: // shipped
    %>
	<tr>
		<th>message
		<td>
			<textarea <%=readonly%> <%=disabled %> onblur="document.forms[1].submit()" name="ta1" rows="5" cols="60"><%=read(request,"ta1", "Sample was shipped.")%></textarea>
			<br>
			<font class="small">Feel free to add any comments regarding this shipping.<br></font>			
	<%
    break;

case 1: // arrived
    %>
	<tr>
		<th>message
		<td>
			<textarea <%=readonly %> <%=disabled %>  onblur="document.forms[1].submit()" name="ta2" rows="5" cols="60"><%=read(request,"ta2", "Sample has arrived at the lab.")%></textarea>
			<br>
			<font class="small">Feel free to add any comments regarding this shipping.<br></font>			
	<tr>
		<th>location
		<td>
		
					<font class="small">Spot where the samples have been stored.<br></font>			
		
			<%
				// value
				String loc1 = request.getParameter("loc1");
				if (loc1 == null){
				    loc1 = "other";
				}
			
			 	String selction = "select location from status where discriminator = \"" + org.setupx.repository.core.communication.status.SampleArrivedStatus.class.getName() + "\" group by location";

				org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();
				try {
				    Iterator locationIter = hqlSession.createSQLQuery(selction)
				    .addScalar("location", org.hibernate.Hibernate.STRING)
				    .list().iterator();

				    hqlSession.close();

				    while(locationIter.hasNext()){
				        String location = "" + locationIter.next();
				        %>	
				        	<input <%=disabled %> type="radio" onClick="document.forms[1].submit()" name="loc1" value="<%=location %>" <%if (loc1.compareTo(location) == 0){  %> checked="checked"<%} %>> 
				        	<b><%=location %></b>
				        	<br><%
					}
				} catch (Exception e){
				    e.printStackTrace();
				}
			%> 	
			<input type="radio" <%=readonly%> onClick="document.forms[1].submit()" name="loc1" value="other" <%if (loc1.compareTo("other") == 0){  %> checked="checked"<%} %>> 
			<input type="text" <%=disabled %> onblur="document.forms[1].submit()" name="loc2" 
				value="<%=read(request,"loc2", " --- ")%>"
				
				<%if (loc1.compareTo("other") != 0){  %> disabled="disabled" <%} %>
			 />
			<br>

	<%
    break;

case 2: // lab status
%>
	<tr>
	<th>message
	<td>
		<textarea <%=readonly%> <%=disabled %> name="ta3" rows="5" cols="60" onblur="document.forms[1].submit()" ><%=read(request,"ta3", "Enter what was done in the lab.")%></textarea>
		<br>
		<font class="small">Feel free to add any comments regarding this shipping.<br></font>			
	
	<%
	break;

default:
    break;
}
%>


<tr>
	<th>user
	<td>
		<%=user.getDisplay()%>	


	<tr>
		<th>time
		<td>
			<%=DateFormat.getTimeInstance(DateFormat.SHORT, Locale.US).format(new Date())%>
			<br>
			<font class="small">Each event has a timestamp attached.</font>			
			


	<tr>
		<th>
		<td>
			<input src="pics/go.gif" type="image" name="go" value="ok" onClick="document.forms[1].submit()" <%=disabled %> />
<br>		<font class="small"><%if(disabled.length() > 1){%>Please enter a valid ID first.<%} %></font>



		

</table>
</form>

</html> 




<%! 
      public static String read(HttpServletRequest request, final String parametername, String alternativeValue) {
		  String value = request.getParameter(parametername);
		  if (value == null || value.compareTo("null") == 0) return alternativeValue;
	      return value;
     }
%>


<%@ include file="footer.jsp"%>
