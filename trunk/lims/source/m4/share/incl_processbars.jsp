<%

org.hibernate.Session _hqlSessionTMP = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

List ids = _hqlSessionTMP.createCriteria(SXQueryCacheObject.class).add(Expression.gt("numberofScannedSamples", new Integer(0))).addOrder(Order.desc("numberofScannedSamples")).list();

//List ids = _hqlSessionTMP.createSQLQuery("select c.* as ids from cache as c ").addEntity("ids", SXQueryCacheObject.class).list();
Iterator iteratorX = ids.iterator();

//Set set = new HashSet();
Hashtable ht = new Hashtable();


// four days ago
GregorianCalendar calendar1 = new GregorianCalendar();
calendar1.add(Calendar.DATE, -4);

//14 days ago
GregorianCalendar calendar2 = new GregorianCalendar();
calendar2.add(Calendar.DATE, -131);

// yesterday
GregorianCalendar calendar3 = new GregorianCalendar();
calendar3.add(Calendar.DATE, -1);



while (iteratorX.hasNext()){
	// check if the experiment is done
	SXQueryCacheObject queryCacheObject = (SXQueryCacheObject)iteratorX.next();
	long promtID = queryCacheObject.getExperimentID();

	// 
	int finishedSamples = new SXQuery().findSampleIDsByPromtID(promtID).size();

	// if cache updated in the last two days
	if (queryCacheObject.getDate().after(calendar1.getTime())) {
	    //set.add(queryCacheObject);
	    ht.put("" + queryCacheObject.getExperimentID(), queryCacheObject);
	} 
	// if unfinished and the cahched object is less then two weeks old
	else if (finishedSamples != queryCacheObject.getNumberofScannedSamples()  &&  queryCacheObject.getDate().after(calendar2.getTime())) {
	    //set.add(queryCacheObject);
	    ht.put("" + queryCacheObject.getExperimentID(), queryCacheObject);
	} 
}


// added 
// checking for recently processed data
File[] files = new java.io.File(Config.DIRECTORY_BB_RESULTS).listFiles();
List __list = new SXQuery().findPromtIDs();

for (int iX = 0; iX < files.length; iX++){
  java.io.File file = files[iX];
  boolean ok = true;
  
  if (file.isDirectory()) ok = false;

  
  if (ok) {
      // cut the id out
      String id = file.getName().substring(0, file.getName().lastIndexOf('.'));

      long promtID = 0;
      try {
	      promtID = Long.parseLong(id);
      }catch(NumberFormatException exception){
	  }

      boolean isExperiment = false;
      Iterator iterator_ = __list.iterator();
      while (iterator_.hasNext()) {
          if ((id).compareTo("" + iterator_.next()) == 0  && new Date(file.lastModified()).after(calendar1.getTime()) ){
              Logger.log(this,new Date(file.lastModified()).toLocaleString());
              isExperiment = true;
              try {
                  SXQueryCacheObject cacheObject = (SXQueryCacheObject)_hqlSessionTMP.createCriteria(SXQueryCacheObject.class).add(Expression.eq("experimentID", new Long(promtID))).list().get(0);
                  //SXQueryCacheObject cacheObject = (SXQueryCacheObject)SXQuery.persistence_loadByID(SXQueryCacheObject.class, _hqlSessionTMP, Long.parseLong(id));
          	    ht.put("" + cacheObject.getExperimentID(), cacheObject);
              } catch (Exception e){
                  e.printStackTrace();
              }
          }
      }
  }
}



// sorting 
if (ht.size() > 0){
%>

<%@page import="org.setupx.repository.server.persistence.SXQueryCacheObject"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="org.hibernate.criterion.Expression"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.Set"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="java.util.Vector"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.core.communication.binbase.BBConnector"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Enumeration"%>
<tr>
<td></td>
<td>Experiment in Progress<br><font class="small">experiments that are <b>running</b> right now, were <b>finished within the last four days</b> or have been processed by the annotation software within the last <b>four days</b>.</font></td>
<td colspan="3">
<table width="100%" align="center">
	<% Enumeration iterator2 = ht.elements();
	   while (iterator2.hasMoreElements()){
	    SXQueryCacheObject queryCacheObject = (SXQueryCacheObject)iterator2.nextElement();
	    %>
		<tr>
		<td colspan="2" style="background-color:#FFFFFF" align="center">
			<table width="100%">
				<tr>
					<td style="background-color:#FFFFFF" align="center" width="100%">
						<%=new SXQuery().findPromtTitleByPromtID(queryCacheObject.getExperimentID()) %>
						<font class="small"><%=queryCacheObject.getExperimentID() %> </font>

						<% boolean fileExist = BBConnector.determineBBResultsArrived(queryCacheObject.getExperimentID()); %>


						<% if(fileExist){%> 
							<!-- file exists -->					
						<td style="background-color:#32CD32;color:black" align="center" >
							ready
						<td style="background-color:#FFFFFF" align="center" width="16">
							<a href="load?id=<%=queryCacheObject.getExperimentID()%>&action=<%=PromtUserAccessRight.DOWNLOAD_RESULT%>"><img  border='0' src="pics/circle-download.gif" title="Download result data from <%=DateFormat.getDateInstance(DateFormat.MEDIUM, java.util.Locale.US).format(new Date(BBConnector.getBBResultFile(queryCacheObject.getExperimentID()).lastModified())) %> for this experiment."></a>
						<% } else if(queryCacheObject.getNumberofScannedSamples() == new SXQuery().findSampleIDsByPromtID(queryCacheObject.getExperimentID()).size()){ %>
						<td style="background-color:#FFFF33 " align="center" >
							export
						<td style="background-color:#FFFFFF" align="center" width="16">
							<!-- finished -->					
							<a href="export_selection.jsp?id=<%=queryCacheObject.getExperimentID()%>"><img  border='0' src="pics/select-grille.gif" title="Export experiment."></a>
						<% } else {%>
						<td style="background-color:#FF4500" align="center">
							running
						<td style="background-color:#FFFFFF" align="center" width="16">
							<!-- not finished -->					
							<img  border='0' src="pics/newperiodic.gif"/>
						<% } %>
					<td style="background-color:#FFFFFF" align="center" width="2">
						<a href="load?id=<%=queryCacheObject.getExperimentID()%>&action=20"><img  border='0' src="pics/details.gif"></a>
				</tr>
			</table>
				


			</td>
	</tr>
	    <%@ include file="incl_processbar.jsp"%>
	    
		<%} %>    
</table>
<td></td>
</tr>
<%} %>


