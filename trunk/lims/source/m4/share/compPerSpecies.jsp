

<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBI_Tree"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@ include file="checknonlogin.jsp"%>

<%
	int ncbiID = Integer.parseInt(request.getParameter("ncbiID"));
	NCBIEntry entry = NCBIConnector.determineNCBI_Information(ncbiID);
	String encrypted = "************************************************************************************************************************************************************************************************************************************************************************************";
%>


<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>



<table align='center' border='0' cellpadding='0' cellspacing="0" width="60%">
	<tr>
		<td colspan="21" style="background-color:#6682B6;" align="right"><font class="small"><%=new Date().toLocaleString() %>&nbsp;</font>

	<tr>
		<td width="1" style="background-color:#6682B6;" align="left" valign="top">
			<img  border='0' src="pics/corner_white2.gif" >
		<td colspan="21" style="background-color:white;" align="center">
			<font style="text-align: center; font-size:20">
				<b>Compounds found in Species <i><%=entry.getName()%></i></b>
			</font>
	<tr>
			<td style="background-color: white" align="center" colspan="21">
			<font class="small">The following information was found in the repository.<br>&nbsp;
			</font>

	<tr>
		<td colspan="7">
		<table align="center" width="100%" border=0 style="background-color:white; ">
			<tr>
							<form action="advancedquery_dev.jsp">
								<td style="background-color:white;" align="right" valign="top"><a href="q_main_ajax.jsp?type=0"><img  border='0' src="pics/back.gif"></td>
								<td style="background-color:white;" align="right" valign="top" width="400"></td>
								<td align="right" style="background-color: white"><input type="text" type="text" id="testinput" name="term" size="20" style="text-align: center; font-size:20" value="<%=entry.getName()	%>"> 
								<td style="background-color:white;" align="left"><input type="image" src="pics/go_search.gif" />
								<input type="hidden" name="type" value="0"> 
							</form>
		</table>
	</tr>


  	<tr>
	 	<th rowspan="821" style="background-color:white;">&nbsp;</th>
	 	<th rowspan="821" ">&nbsp;&nbsp;</th>
	    <th>&nbsp;</th>
	    <th>&nbsp;</th>
	    <th>&nbsp;</th>
	 	<th rowspan="821" ">&nbsp;&nbsp;</th>
	 	<th rowspan="821" style="background-color:white;">&nbsp;</th>
	</tr>
	
	<tr>
		<td colspan="7">
		<table >
			<tr>
				<td colspan="2" align="center">
					<font class="small">The following compounds have been found in samples that were taken from this species.</font>
			<tr>
				<td align="center" valign="top">
				<td colspan="1">
					<% int speciesID = entry.getTaxIdInt(); %>
				 	<%@ include file="incl_compound_by_species.jsp"%>
		</table>
	<tr>
		<th colspan="21">&nbsp;&nbsp;&nbsp;
	<tr>
		<td colspan="21" style="background-color:white;" align="center">
</table>

					
<%@ include file="footer.jsp"%>

