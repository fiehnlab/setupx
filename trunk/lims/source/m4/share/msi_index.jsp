<%@ include file="checknonlogin.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%> & Metabolomics Standards Initiative (MSI)</title>
</head>
<body>



<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>	
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

<%@ include file="incl_header_msi.jsp"%>
<br>

<table align="center" width="60%" cellpadding="5">
	<tr>
		<th rowspan="31" width="10%">
		<th colspan="2"><h2><%=org.setupx.repository.Config.SYSTEM_NAME%> & Metabolomics Standards Initiative (MSI)</h2>
						WebSite: <a href="http://metabolomicssociety.org/register/index.php?option=com_content&task=view&id=17&Itemid=35">MSI</a><br>
						Sponsor: <a href="http://www.metabolomicssociety.org/">Metabolomics Society</a>
		<th rowspan="31" width="10%">

	<tr>
		<td colspan="2">
		The Metabolomics Society has appointed an Oversight Committee to monitor, coordinate and review the efforts of 
		working groups (WG) in specialist areas that will examine standardization and make recommendations. 
		The overall chair of this committee is <a href="mailto:ofiehn@ucdavis.edu">Oliver Fiehn</a>.

	<tr>
		<td><h3>Standard reporting requirements for samples in metabolomics experiments</h3>
		List of MSI Attributes according to the publications in <a href="http://www.springerlink.com/content/j85g85483103/?p=e86ed60f7e524323bb3bbd3abd9953bd&pi=0">metabolomics</a>.
		<p>
		<td style="background-color: white" valign="middle"><a href="http://www.springerlink.com/content/j85g85483103/?p=e86ed60f7e524323bb3bbd3abd9953bd&pi=0"><img align="middle" src="pics/cover-metabolomics.jpg"></a> 
	<tr>
		<td colspan="2"><h4>List of MSI Attributes</h4>
		<p>
			The following list contains the current number of MSI Attributes.
			In order to be able to exchange data in between labs a machine readable format of 
			the attributes that were defined by the relevant committees has to be stored. Therefore we
			defined the attributes in a database. The list of Attributes is dynamic is updated whenever 
			a newer standard of attributes has been defined. 
		<p> The list of attributes is currently available as <a href="msi_attributes.jsp">HTML for human interaction</a> and as XML <a href="msi_attributes_rss.jsp"><img src="pics/rss3.gif" border="0"> </a>feed to be 
			embedded in 3rd party applications or news readers.

	<tr>
		<td colspan="2"><h4>XML Schema for Metabolomic Experiments (beta)</h4>
		<p> The current XML Export of Samples and entire Experiments is based on the XML Schema. The current version is 001 and can be 
			downloaded <a href="MSI-SetupX-001.xsd"><img src="pics/xml.gif" border="0"></a> and used for validation. Documentation is <a href="msi/MSI-SetupX-001.xsd.html">available</a>.  
	</table>


<br>	
<%@ include file="incl_footer_msi.jsp"%>

	