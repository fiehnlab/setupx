<%@page contentType="text/html" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<h2 class="tableheader1" align="center"><%=org.setupx.repository.Config.SYSTEM_NAME%></h2></h2>

<table width="80%" align="center" cellpadding='8'>

<th>Here you will find some basic instructions, developed on a very simple experimental design: </th>


<tr>
	<td>
		<p>	You will find the description of your experimental design in a set of different pages. All pages are diplayed by a header on top of the screen <font style="BACKGROUND-COLOR: #ff0000">(2)</font>. Required information is marked by (symbol). Only if at least all the required information is entered, you will be able to submit your experiment (at any time, however, you can save and come back later).</p>
		<p> Each page asks you for specific information, with a short page description on the left upper side <font style="BACKGROUND-COLOR: #ff0000">(1)</font>.</p>
	</td>
</tr>

<tr>
	<td>
		<p>	We define experimental designs by a matrix of �BioSource� versus �Treatment�. 
		</p>
		<ul type="square">
				<li>The BioSource pages ask for the species you work on, the genotype(s), the organs, tissue or cell types. </li>
				<li>The Treatment pages ask for the growth conditions that all your samples have experienced, plus any kind of treatment or array of treatments you have performed, plus any kind of dosages of your treatment or time dependencies. </li>
		</ul>
	</td>
</tr>

<tr>	
	<td>
		<p>	In case you want to test more than one species (or more than one organ, treatment etc), you need to add additional input fields. Ultimately, this will lead to a large matrix, of which you may not have originated samples for some classes: you may later deselect these classes from the sample submission number. 
		</p>
		<p> Lets try out the entry for a hypothetical experiment in �mad cow disease�. In the BioSource pages you would put in the species Bos Taurus (cattle), and male and female cattle would define two separate classes. Your experiment may contain diseased cattle and healthy controls, which you would generate in the Treatment pages. In total, this generates four classes. In order to define how many samples you are going to send to the metabolomics core laboratory, please have a look to the page Samples / Classes. Your matrix of classes is displayed in a table, and each class is represented by  . By pressing the magnify icon  inside the class-table or just by scrolling down you will find each class and finally the samples (replica) for each class. By default there is a number of 6 replica for each class to ensure some level of statistical significance.
		</p>
		<p> The minimum number of samples per experimental design group (we call this a �class�) is six. If you have less than six samples per class, please ask the core manager for exemption. 
		</p>
	</td>
</tr>

<tr>
	<td align="center">
		<img  border='0' src="pics/form.jpg"/>
	</td>
</tr>
</body>

</html>
