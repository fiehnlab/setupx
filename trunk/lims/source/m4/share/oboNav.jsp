<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 org.setupx.repository.core.communication.binbase.BBConnector,
                 java.util.List,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*"%>



<%@page import="org.setupx.repository.core.communication.ontology.obo.OBOConnector"%>
<%@page import="org.setupx.repository.core.communication.ontology.obo.OBOTerm"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.forms.FormObject"%>
<%@page import="org.setupx.repository.web.forms.inputfield.InputField"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>


<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<body>
    

<%
	Promt promt = (Promt)request.getSession().getAttribute(WebConstants.SESS_FORM_PROMT);
	String fieldKey = request.getParameter("field");
	InputField inputField = (InputField)promt.getField(fieldKey);

	// checking if a term was givven 
	try {
	    String term = request.getParameter("term");
	    inputField.setValue(term);
	}catch (Exception e){
	    // no - there is no valid term connected
	}
	

	
%>

formobject: <%=inputField.toString() %>
promt: <%=promt.getUOID() %>

<table align="center" width="60%" cellpadding="7">
	<th colspan="21"></th>
<%
		java.io.File file = new java.io.File("/lims/data/obo/" + request.getParameter("obo"));
		OBOConnector connector = OBOConnector.newInstance(file);

		try {
		    OBOTerm termOriginal = null;
		    try {
		        termOriginal = connector.determineTerm(inputField.getValue()+"");
		    } catch (Exception e){
		    }
	
	%>
	

	<tr>
		<td>
		<td>
		<td>
		<td colspan="21">
			<table>   		
    		<%
    		java.util.ArrayList arrayList = termOriginal.getIsAParents();
    		for (int w = 0  ; w < arrayList.size(); w++){        	
    		    try {
    		        // reformat
    		        // cell.obo - cut out "... !"
    		        String termString  = "" + arrayList.get(w);
    		        if (termString.indexOf("!") > 0){
    		            termString = termString.substring(termString.indexOf("!") + 2, termString.length());
    		        }
    			    OBOTerm term2 = null;
    		        try {
    			        term2 = connector.determineTerm(termString);
    			    } catch (Exception e){
    			        e.printStackTrace();
    			    }
    			    
    			    if (term2 != null){
    			        %>
				    	<tr>	
							<td><strong>Relation:</strong> Parent</td>
							<td align="center" style="background-color:#FFFFFF">
								<b><a href="?obo=<%=request.getParameter("obo")%>&term=<%=term2.getTerm()%>&field=<%=fieldKey%>"><%=term2.getTerm()%></a></b>
								<br><font class="small"><%=term2.getId()%></font>    		
							<td><font class="small"><%=term2.getDefinition()%></font>
				    	</tr>
    			        <% 
    			    }
    		    } catch (Exception e){
    		        e.printStackTrace();
            		%>
		<tr>
			<td>
				<b>err <%=arrayList.get(w) %></b>
			</td>
		</tr>	
            		<% 
    		    }
    		}
    		%>
	</table>



	<table>
	<tr>
		<td>
		<td>
		<td>
		<td >
	<% 
    String[] relationTypes = OBOConnector.getRelationTypes();
    for(int a = 0; a < relationTypes.length; a++){
      java.util.Set related_terms = connector.determineRelatedTermsByRelation(termOriginal, relationTypes[a]);
      java.util.Iterator iterator = related_terms.iterator();

      while (iterator.hasNext()) {
        OBOTerm termRelated = (OBOTerm) iterator.next();
    	%>
    	<tr>	
			<td><strong>Relation: </strong>
				<br><%=relationTypes[a].replace('_', ' ') %>
			</td>
			<td align="center" style="background-color:#FFFFFF">
				<b>
					<a href="?obo=<%=request.getParameter("obo")%>&term=<%=termRelated.getTerm()%>&field=<%=fieldKey%>"><%=termRelated.getTerm()%></a></b>
				<br><font class="small"><%=termRelated.getId()%></font>    		
			<td><font class="small"><%=termRelated.getDefinition()%></font>
    	</tr>
    	<% 
      }
    }
    
	%>
		</table>
	</tr>
	
	<tr>
		<td>
		<td style="background-color:#FFFFFF"><%=termOriginal.getId()%>
		<td style="background-color:#FFFFFF"><h4><%=termOriginal.getTerm()%></h4>
		<td style="background-color:#FFFFFF"><font class="small"><%=termOriginal.getDefinition()%></font></td>
		<td><a href="form"> THATS IT !</a>
	<tr>
		<td>
		<td>
		<td>
		<td colspan="21">


		<table>
<%	
    
    
    Iterator iterator = connector.determineReverseRelatedTermsByRelation(termOriginal, OBOConnector.getRelationTypes()).iterator();
    while(iterator.hasNext()){
        OBOTerm relatedTerm = (OBOTerm)iterator.next();
    %>
    <tr>	
    	<td><strong>Reverse Relation: </strong>
    	</td>
    	<td align="center" style="background-color:#FFFFFF">
			<a href="?obo=<%=request.getParameter("obo")%>&term=<%=relatedTerm.getTerm()%>&field=<%=fieldKey%>"><%=relatedTerm.getTerm()%></a></b>
    		<br><font class="small"><%=relatedTerm.getId()%></font>    		
    	<td><font class="small"><%=relatedTerm.getDefinition()%></font>
    </tr>
    <% 
    }
    
    
} catch (Exception e) {
    e.printStackTrace();
  }





%>
	</table>
	
	<td>
	
</table>
	
<table align="center" width="60%" cellpadding="7">	<form>
	<tr>
		<td>
		<td colspan="21"><input type=hidden name="ontology" value="<%=request.getParameter("ontology")%>"> 
			<input type="text" name="term" value="<%=request.getParameter("term") %>"> 
		</td>
	</tr>
	<tr>
		<td>
		<td colspan="21"><input type="submit" >
	</tr>
	</form>
	<tr>
		<td>
		<td colspan="21"><a href="?ontology=po_anatomy.obo&term=endosperm">endosperm</a>
	</tr>
</table>
<table align="center" width="60%" cellpadding="7">	
<th colspan="21"> change ontology</th>

<% 
java.io.File[] files = new java.io.File("/lims/data/obo").listFiles();
for( int i = 0 ; i < files.length ; i++) {
    %>
	<tr>
		<td><%=files[i].getName()%></td>
		<td><a href="obo_test.jsp?ontology=<%=files[i].getName()%>&term=<%=request.getParameter("term") %>"><img  border='0' src="pics/go.gif"></a></td>
	</tr>
<%} %>
</table>    
</td>
</table>

</body>
</html>