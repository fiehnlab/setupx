<%@page import="org.setupx.repository.web.forms.inputfield.InputField"%>
<%@page import="org.setupx.repository.web.forms.FormObject"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>
<%@page import="java.util.StringTokenizer"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%
    String row = request.getParameter("row").toString();
    Promt promt = (Promt) session.getAttribute(WebConstants.SESS_FORM_PROMT);
    String data = request.getParameter("data").toString();

    Sample[] samples = promt.getSamples();

    StringTokenizer stringTokenizer = new StringTokenizer(data, "\n");

    for (int i = 0; i < samples.length
            && stringTokenizer.hasMoreElements(); i++) {
        FormObject[] formObjects = samples[i].getFields();
        for (int j = 0; j < formObjects.length; j++) {
            InputField inputField = (InputField) formObjects[j];

            if (inputField.getQuestion().compareTo(row) == 0) {
                inputField.setValue(stringTokenizer.nextToken());
            }
        }
    }

%>
<jsp:forward page="sample_sutom_meta.jsp" />
