
    
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ include file="incl_searchtypes.jsp"%>
<%@ include file="checknonlogin.jsp"%>


<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>

<%
// -----------------------------
//      general search jsp	
// -----------------------------

String searchTerm = request.getParameter("term");    

// numberOfResultsPerPage
final int numberOfResultsPerPage = 20;

// limit it to certain number of elements.
int limitMax = numberOfResultsPerPage;
int limitMin = 0;
String publicOnly = "";

try {
	limitMax = (Integer.parseInt(request.getParameter("limitmax").toString()));
	limitMin = (Integer.parseInt(request.getParameter("limitmin").toString()));
} catch (Exception e){
    Logger.log(this, "illegal limit.");
}


try {
	publicOnly = request.getParameter("public").toString();
} catch (Exception e){
    Logger.log(this, "no public parameter.");
}
boolean pubOnly = (publicOnly.compareTo("true") == 0);


int selectedSearchTypeInt = 99;
try {
	selectedSearchTypeInt = (Integer.parseInt(request.getParameter("type") + ""));
} catch (Exception e){
	e.printStackTrace();	    
}

String selectedSearchTypeString = "";
if (selectedSearchTypeInt != 99){
    selectedSearchTypeString = searchTypes[selectedSearchTypeInt];
} else {
    selectedSearchTypeString = "";
}


Session s = CoreObject.createSession();
%>

<table align='center' border='0' cellpadding='0' cellspacing="0" width="60%">
	<tr>
		<td colspan="21" style="background-color:#6682B6;" align="right"><font class="small"><%=new Date().toLocaleString() %>&nbsp;</font>

	<tr>
		<td width="1" style="background-color:#6682B6;" align="left" valign="top">
			<img  border='0' src="pics/corner_white2.gif" >
		<td colspan="21" style="background-color:white;" align="center">
			<font style="text-align: center; font-size:20">
				Experiments containing <i><%=searchTerm%></i>
			</font>
	<tr>
			<td style="background-color: white" align="center" colspan="21">
			<font class="small">The following information was found in the repository.<br>&nbsp;
			</font>

	<tr>
		<td colspan="7">
		<table align="center" width="100%" border=0 style="background-color:white; ">
			<tr>
							<form action="advancedquery_dev.jsp">
								<td style="background-color:white;" align="right" valign="top"><a href="q_main_ajax.jsp?type=0"><img  border='0' src="pics/back.gif"></td>
								<td style="background-color:white;" align="right" valign="top" width="400"></td>
								<td align="right" style="background-color: white"><input type="text" type="text" id="testinput" name="term" size="20" style="text-align: center; font-size:20" value="<%=searchTerm	%>"> 
								<td style="background-color:white;" align="left"><input type="image" src="pics/go_search.gif" />
								<input type="hidden" name="type" value="0"> 
							</form>
		</table>
	</tr>
		

<%
List expList = new Vector();





switch (selectedSearchTypeInt) {
case 1:
    /* samples */

   	String query = "select relatedExperimentID from pubattribute, pubsample, pubdata " 
				+ " where pubattribute.label = \"" + searchTerm + "\" and pubsample.parent = pubdata.uoid "  
				+ " and pubsample.uoid = pubattribute.parent group by relatedExperimentID " ;
	

	/*    
	    String queryCompoundNames = "select samples.experiment as expID from samples as samples, pubattribute as att, pubsample as sample where sample.uoid = att.parent " +
		" and att.label like  \"%" + searchTerm + "%\"  and samples.sampleID = sample.setupXsampleID group by samples.experiment order by samples.experiment desc";
		expList.addAll(s.createSQLQuery(queryCompoundNames).addScalar("expID", Hibernate.STRING).list());
	*/
	expList.addAll(s.createSQLQuery(query).addScalar("relatedExperimentID", Hibernate.STRING).list());     
    break;


default:
    break;
}
int counter = 0;
Iterator expListIDIterator = expList.iterator();
%>

	<tr>
		<td style="background-color: white">
		<td colspan="5" style="background-color: white">
		<table style="background-color: white" width="100%">
			<tr>
			<td style="background-color: white"> 
				<a href="?term=<%=searchTerm%>&type=<%=selectedSearchTypeInt%>&limitmax=<%=numberOfResultsPerPage%>&limitmin=<%=0%>">
					<img  border='0' src="pics/page-prev-o.gif" title="first entry">
				</a>
			<td style="background-color: white"> 
				<a href="?term=<%=searchTerm%>&type=<%=selectedSearchTypeInt%>&limitmax=<%=limitMax-numberOfResultsPerPage%>&limitmin=<%=limitMin-numberOfResultsPerPage%>">
					<img  border='0' src="pics/page-prec.gif" title="prev page">
				</a>
			<td style="background-color: white"> 
				<a href="?term=<%=searchTerm%>&type=<%=selectedSearchTypeInt%>&limitmax=<%=limitMax+numberOfResultsPerPage%>&limitmin=<%=limitMin+numberOfResultsPerPage%>">
					<img  border='0' src="pics/page-suiv.gif" title="next page">
				</a>
			<td style="background-color: white"> 
				<a href="?term=<%=searchTerm%>&type=<%=selectedSearchTypeInt%>&limitmax=<%=expList.size()%>&limitmin=<%=expList.size()-numberOfResultsPerPage%>">
					<img  border='0' src="pics/page-next-o.gif" title="last page">
				</a>
			<td style="background-color: white"> 
				<a href="?term=<%=searchTerm%>&type=<%=selectedSearchTypeInt%>&limitmax=1000000&limitmin=0&public=<%=!pubOnly%>">
					<img  border='0' src="pics/paste.gif" title="public">
				</a>
			<td style="background-color: white" colspan="21" align="right" width="90%" > <font class="small"> [<%=limitMin %> to <%=limitMax %> out of <%=expList.size() %>]</font>
		</table>
		<td style="background-color:white;" align="right" valign="top"><a href="pubsample.jsp?<%=searchTerm%>"><img  border='0' src="pics/advanced.gif"></td>

		
    </tr>
    <tr>
    	<th>
    	<th colspan="3">
    	<th colspan="2">
    	<th>
    </th>
    <tr>
    	<td colspan="2" align="center"><font class="small">experiment
    	<td align="center"><font class="small">detail
    	<td align="center"><font class="small">title
    	<td align="center"><font class="small">abstract
    	<td align="center"><font class="small">details
    	<td align="center"> <font class="small">species
    </th>
    <tr>
    	<th>
    	<th colspan="3">
    	<th colspan="2">
    	<th>
    </th>
    
    
			
<%
while(expListIDIterator.hasNext() && counter < limitMax ){
    counter++;
    if (counter >= limitMin){
        	
        
    	long promtID = Long.parseLong(expListIDIterator .next().toString());
    	String promtTitle = new SXQuery().findPromtTitleByPromtID(promtID);
    	String promtAbstract = new SXQuery().findPromtAbstractByPromtID(promtID);
    	if (promtAbstract.length() > 200) promtAbstract = promtAbstract.substring(0,199).concat("...");

    	boolean locked = true;
    	try {
	    	locked = (PromtUserAccessRight.READ > new SXQuery().findPromtUserAccessRightForUserID(user.getUOID(), promtID).getAccessCode());
		} catch (Exception e){
		    // no access
		}
    	String species = new SXQuery().findSpeciesbyPromtID(promtID).get(0).toString();
    	
    	// in case only public samples should be shown
    	String _color = "style=\"background-color: white\"";
    	if (counter%2 >=1) _color = "";
    	if ((pubOnly && !locked) || !pubOnly){
    	    %>
		    <tr>
		    	<td align="right" <%=_color%>><font class="small"><%=counter %> &nbsp;&nbsp;</font> 
		    	<td align="center"  <%=_color%>><%=promtID%> 
		    	<%if(locked){%>    
		    		<td colspan="2" align="center" style="background-image: url('pics/locked.jpg'); background-repeat: no-repeat; background-position: center center; background-color: white;"><font class="x-small" ><i><b>not public</b></i></font>
		    	<%}else{ %>
			    	<td width="2" align="center" style="background-color: white"><a href="load?id=<%=promtID %>&action=20"><img  border='0' src="pics/details.gif"></a>
			    	<td align="center"  <%=_color%>><%=promtTitle%>
		    	<%} %>
		    	
		    	<%if(locked){%>    	
		    		<td colspan="2" align="center" style="background-image: url('pics/go_locked.gif'); background-repeat: no-repeat; background-position: center center; background-color: white;"><font class="x-small" ><i><b>not public</b></i></font>
		    	<%}else{ %>
			    	<td align="left"  <%=_color%>>
			    		<font class="small"><%=promtAbstract%>
			    	</font>
			    	<td width="2" align="center" style="background-color: white"><a href="load?id=<%=promtID %>&action=20"><img  border='0' src="pics/details.gif"></a>	
		    	<%} %>


		    	<td align="center"  <%=_color%>>
		    		<a href="advancedquery_dev.jsp?term=<%=species %>&type=0"><%=species %></a>
		    </tr>
    	    <%
    	}
    	%>
    	<%
    } else {
        expListIDIterator.next();
    }
} %>
   <tr>
    	<th>
    	<th colspan="3">
    	<th colspan="2">
    	<th>
    </th>
   <tr>
    	<th style="background-color: white">&nbsp;
    	<th colspan="3" style="background-color: white">
    	<th colspan="2" style="background-color: white">&nbsp;
    	<th style="background-color: white">&nbsp;
    </th>
   <tr>
    	<th>
    	<th colspan="3">
    	<th colspan="2">
    	<th>
    </th>
 </table>

<%@ include file="footer.jsp"%>




