<%@ include file="checklogin.jsp"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/	>



<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>
<%@page import="org.setupx.repository.web.forms.FormObject"%>
<%@page import="org.setupx.repository.web.forms.inputfield.InputField"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIProvider"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIQuery"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customize your sample information in <%=Config.SYSTEM_NAME %>.</title>
</head>
<body>

<%

	// find the experiemnt ID
	Promt promt = (Promt)session.getAttribute(WebConstants.SESS_FORM_PROMT);

	// list current attributes and option to remove them in case they are "custom"
	Sample[] samples = promt.getSamples();

%>

<!--  head containing logo and description -->
<table align='center' border='0' cellpadding='2' width="70%">
<tr>
		<td align="center" width="2">
			<a href="form.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h3> <%=promt.getName()%>
			<% if (promt.getUOID() != 0){ %> [<%=promt.getUOID() %>]
			<%} %>
			</h3>
			Add your custom information for each sample.
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			


		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
</tr>
</table>

<%		String tableBackgroundColor = "#1B5289"; //"#E7f3ff"; // "white";// "#6682B6" ; %>
<table border="0" width="70%" align='center'>
	<tr>
		<th>&nbsp;
		<th>How to ...?
		<th>
	<tr>
		<th>
		<td>	Below you see all the samples that are part of your experiment. <br>
				In order to add additional attributes to each sample please select from the MSI Attributes or define your own custom label.<br>
				Attributes can also be removed by using the <img src="pics/remove-all.gif" border="0"> icon.
			<p>
				In order to add information for multiple samples at once select them check the <img src="pics/form_history.gif" border="0"> icon next to the row you want to fill.
		<th>
		
	<tr>
		<th>
		<td align="left" width="100%">
<table border="0" align='center' cellpadding="0"  cellspacing="0" background="<%=tableBackgroundColor %>">
<%
	
	
	int maxNrAttributes = 0;
	// determine information about all samples
	for (int i = 0; i < samples.length; i++){
	    Sample sample = samples[i];
	    FormObject[] formObjects = sample.getFields();
        int nrAttr = 0;
	    for (int j = 0; j < formObjects.length; j++){
	        InputField inputField = (InputField)formObjects[j];
	        if (inputField.getValue().toString().length() > 0){
	            nrAttr++;
	            Logger.log(this, nrAttr +  ": " + inputField.getValue().toString().length());
	            Logger.log(this, nrAttr +  ": " + inputField.getValue().toString());
	        }
	    }
		if (nrAttr > maxNrAttributes) maxNrAttributes = nrAttr;
	}
	%>

	<tr>
		<th>ID
		<th>class
		<th>
		<th colspan="<%=maxNrAttributes%>">
		<th rowspan="2">
		<font class="small">
		add additional fields:<br>
		MSI Attributes<br>
		<%
		// 1. MSI attribtues
		Sample sample = promt.getSamples()[0];

		
		Session s = CoreObject.createSession();
		List attributes = CoreObject.persistence_loadAll(MSIAttribute.class, s);
		
				
	    %>
			<form name="1" action="sample_custom_attatch.jsp">
				<select name="attribute" onChange="document.forms[0].submit()">
					<option value="--" disabled="disabled" selected="selected">-MSI Attributes-</option>
					<%
					Iterator iterator2 = attributes.iterator();
						while (iterator2.hasNext()){
						    MSIAttribute attribute =(MSIAttribute )iterator2.next();
						    
						    FormObject[] formObjects = sample.getFields();
						    boolean found = false;
						    for (int k = 0; k < formObjects.length; k++){
						        if (((InputField) formObjects[k]).getQuestion().compareTo(attribute.getMsiLabel()) == 0){
						            found = true;
						        }
						    }
						    
						    // take out the ones that exist
						    if (!found){
								%>
								<option value="<%=attribute.getUOID()%>"><%=attribute.getMsiLabel()%></option>
								<%
						    }
						}
					%>
				</select>
			</form>
						<br>
			or you custom field<br>
			<form action="sample_custom_attatch.jsp">
				<input type="text" name="attribute" value="... your label">
				<INPUT TYPE="image" NAME="Submit" SRC="pics/link.gif" ALT="Submit">
			</form>
	<tr>
		<th>
		<th>
		<th>
		<%
		for (int j = 0; j < sample.getFields().length; j++){
		    try {
				InputField field = (InputField)sample.getField(j); 
				if (field.getValue().toString().length() > 0){
				    Logger.debug(this, "field:"  + field.getQuestion() );
				    %>
					<th><%=field.getQuestion() %>
					<% if(field.getDescription().length() == 0 || field.getDescription().compareTo("custom field") == 0){
					    // remove button
					    %>
						<a href="sample_custom_del.jsp?label=<%=field.getQuestion() %>&pos=<%=field.getInternalPosition() %>"><img src="pics/remove-all.gif" border="0"></a>
					<%} %>
					<a href="sample_custom_autofill.jsp?row=<%=field.getQuestion() %>"><img src="pics/form_history.gif" border="0"></a>
					<%
				}
			} catch (Exception e){
			    e.printStackTrace();
			}
		} %>
		

	<%
	long tmpClassID = 0;
	for (int i = 0; i < samples.length; i++){
	    Sample _sample = samples[i];
	    
	    // get number of samples for this class
	    int classID = (int)_sample.getParent().getUOID();
	    int numberOfSamplesPerClass = new SXQuery().findSampleIDsByClazzID(classID).size();
	    
	    
	    if (_sample.getUOID() == 0) promt.update(true);
	    
	    %>
	    <tr>
	    	<td>
	    		&nbsp;<%=_sample.getUOID() %>&nbsp;
	    	<td style="background-color:<%=Util.getColor((int)_sample.getParent().getUOID()) %>">
	    		<font style="font-size: xx-small;"> &nbsp;<%=_sample.getParent().getUOID() %>&nbsp;</font>
	    	<%
	    		// in case it is the first sample of this class / show the classdesciption
	    		
	    		if (tmpClassID != classID){
	    		    tmpClassID = classID;
	    		    %>
			    	<td style="background-color:<%=Util.getColor((int)_sample.getParent().getUOID()) %>" rowspan="<%=numberOfSamplesPerClass%>">
			    		<font class="small">
			    			<%= Util.replace(new SXQuery().findClassInformationString(classID), "\n", "<BR>") %>
			    		</font>
			    	</td>
	    		    <% 
	    		}
	    	%>	
	    		

	    <%
		// list preexisting attributes
		  	    
	    FormObject[] formObjects = _sample.getFields();
	    for (int j = 0; j < formObjects.length; j++){
	        try {
		        InputField inputField = (InputField)formObjects[j];
		        
		        if (inputField.getValue().toString().length() < 1){
		            throw new Exception("no value");
		        }
		        %>
		        <td align="center">
		        	<input type="text" readonly="readonly" value="<%=inputField.getValue() %>">
		        	
		        	<%
		        		if (inputField.getMSIAttribute() != null){
		        		    %>
		        		    <%=inputField.getMSIAttribute().getExtension() %>
		        		    <%
		        		}
		        	%>
		        </td>
		        <%
	        } catch (Exception e){
	            e.printStackTrace();
	        }
	    }
	    %>
	<td>
	<%	    
	}
	
	// 2. other frequent once
%>
	<tr>
	<td colspan="200" align="center">
		When finished return to the main form. <a href="form.jsp" ><img src="pics/go.gif" border="0"></a>
</table>

	<th>
</table>


