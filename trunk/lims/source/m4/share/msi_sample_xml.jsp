<%@page import="javax.xml.XMLConstants"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.util.xml.XMLFile"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.setupx.repository.core.util.xml.XMLUtil"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.setupx.repository.core.communication.msi.AttributeValuePair"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIQuery"%>
<%
	long id = Long.parseLong(request.getParameter("id"));

	Document document = XMLUtil.createNewDocument();

    document.appendChild(document.createElement("export"));
    Element root = document.getDocumentElement();

    AttributeValuePair[] values_promt = null;
    AttributeValuePair[] values_classes = null;
    AttributeValuePair[] values_sample = null;

    Element promt_attrib = null;
    Element class_attrib = null;
    Element sample_attrib = null;


	
	
	
	values_promt = MSIQuery.searchAttributesPerExperimentBySampleID(id);
	Logger.debug(this, "done.");
	
	try {
	    promt_attrib = document.createElement("study");
	    promt_attrib.setAttribute("SXUOID", "" + new SXQuery().findPromtIDbySample(id));
	    root.appendChild(promt_attrib);
	    MSIQuery.toXML(values_promt, document, promt_attrib);
	} catch (Exception e) {
	    Logger.log(this, "problems with study");
	    e.printStackTrace();
	}
	
	values_classes = MSIQuery.searchAttributesPerClassBySampleID(id);
	Logger.debug(this, "done.");
	try {
	    class_attrib = document.createElement("class");
	    class_attrib.setAttribute("SXUOID", "" + new SXQuery().findClazzIDBySampleID((int)id));
	    promt_attrib.appendChild(class_attrib);
	    MSIQuery.toXML(values_classes, document,
	            class_attrib);
	} catch (Exception e) {
	    Logger.log(this, "problems with classes");
	    e.printStackTrace();
	}

    values_sample = MSIQuery.searchAttributesPerSampleBySampleID(id);
    try {
        sample_attrib = document.createElement("sample");
        sample_attrib.setAttribute("SXUOID", "" + id);
        MSIQuery.toXML(values_sample, document, sample_attrib);
        class_attrib.appendChild(sample_attrib);
    } catch (Exception e) {
        Logger.log(this, "problems with samples");
        e.printStackTrace();
    }
	out.clear();
	//root.setAttribute("xmlns","http://setupx.fiehnlab.ucdavis.edu");
	root.setAttribute("xmlns:xsi", XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI);
	root.setAttribute("xsi:noNamespaceSchemaLocation", "MSI-SetupX-001.xsd");
	response.setContentType("text/xml");
	response.setCharacterEncoding("UTF-8");
	out.write(new XMLFile(document).content());
	%>

            	
    	