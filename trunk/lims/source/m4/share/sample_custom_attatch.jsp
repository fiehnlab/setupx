<%@page import="org.setupx.repository.web.forms.inputfield.StringInputfield"%>
<%@page import="org.setupx.repository.web.forms.inputfield.InputField"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>

<%

    // find the experiemnt ID
    Promt promt = (Promt) session.getAttribute(WebConstants.SESS_FORM_PROMT);

    // list current attributes and option to remove them in case they are "custom"
    Sample[] samples = promt.getSamples();

    String msiID = request.getParameter("attribute").toString();
    // get the attribute name

    // attatch this to each sample
    %><%=msiID%><%
	Logger.log(this, "adding new field " + msiID);

    String question = "";
    
    MSIAttribute msiAttribute = null; 
    try {
    	Session s = CoreObject.createSession();
        msiAttribute = (MSIAttribute)CoreObject.persistence_loadByID(MSIAttribute.class, s, Long.parseLong(msiID));
        question = msiAttribute.getMsiLabel();
    } catch (Exception e){
        Logger.debug(this, "not msi ");
        question = msiID;
    }
    boolean exists = false;    

    // loop over every sample and attatch it to the sample
    for (int i = 0; i < samples.length  && !exists; i++){

        
        // checking if the samples has a field like that already 
		FormObject[] formObjects = samples[i].getFields();
        
        for (int s = 0 ; s < formObjects.length; s++){
            if (((InputField)formObjects[s]).getQuestion().compareTo(question) == 0){
                exists = true;
            }
        }
        
        if ( !exists){
            StringInputfield field = new StringInputfield(question , "custom field", "unknown" , true);
            if (msiAttribute != null) field.setMSIAttribute(msiAttribute);
            field.setSize_x(20);
            samples[i].addField(field);
        }
    }  
%>

<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.web.forms.FormObject"%>
<jsp:forward page="sample_sutom_meta.jsp"/>
