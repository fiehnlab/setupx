<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.communication.importing.logfile.*,
				 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.communication.leco.*,
                 org.setupx.repository.core.util.*,
                 org.setupx.repository.server.persistence.*"%>




<%@page import="org.setupx.repository.core.communication.export.reporting.Report"%>
<%@page import="org.setupx.repository.web.forms.PromtCreator"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%
Report report = (Report)session.getAttribute(org.setupx.repository.web.WebConstants.SESS_REPORT);
long promtID = report.getPromtID();
%>
<body>



<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width='60%'>
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Export</h2>
			<br/>
			<font size="-2">Below you find all samples including every available run for each sample related to the selected experiment.<br>Choose all samples that you want to include in the result.</font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>

	<table align="center" width="60%" cellpadding="2">	

	<tr>
		<th>
		<th colspan="21">Different ways of exporting data.
		
	
	
	<tr>
		<% 
		// -----------------------------------------------------------------------------------------------------------------			
		//							Export Button (depending on the accesslevel)
		// -----------------------------------------------------------------------------------------------------------------				
		%>
			<% try{
			    PromtUserAccessRight.checkAccess(user.getUOID(), report.getPromtID(), PromtUserAccessRight.EXPORT);
			    %>
			<tr>
				<td style="background-color:#6682B6" width="20"></td>
				<td width="0">Export</td>
				<td align="center" style="background-color:#FFFFFF">
				<a href="exp2_standard.jsp?id=<%=report.getPromtID()%>">
					<img  border='0' src="pics/go_export.gif"></a></td>
				<td colspan="6" style="background-color:#FFFFFF" >
						Export this experiment the <b>standard way</b> including all samples or just a subset of the samples to the connected annotaion system (BinBase).
						</td>
				
			</tr>

			<% 
			}catch (Exception e)			    {%> 
			<tr>
				<td style="background-color:#6682B6" width="20"></td>
				<td width="0">Export</td>
				<td align="center" style="background-color:#FFFFFF"><a href="load?id=<%=report.getPromtID()%>&action=40"><img  border='0' src="pics/go_locked.gif"></a></td>
				</td>
				<td colspan="6" style="background-color:#FFFFFF" >
						Export this experiment including all samples or just a subset of the samples to the connected annotaion system (BinBase).
						</td>
				
			</tr>
			<%}%>


			
			<% if (user.isMasterUser(user.getUOID())){ %>
			<tr>
				<td style="background-color:#6682B6" width="20"></td>
				<td width="0">Export (special)</td>
				<td align="center" style="background-color:#FFFFFF">

					<img  border='0' src="pics/go_blank.gif">
				</td>
				<td colspan="6" style="background-color:#FFFFFF" >
						<b><i>deprecated</i></b> 
						Export this experiment and <b>overwrite</b> systems default settings. 
						</td>
				
			</tr>
			<% } %>


			<% if (user.isLabTechnician()){ %>
			<tr>
				<td style="background-color:#6682B6" width="20"></td>
				<td width="0">Export (special)</td>
				<td align="center" style="background-color:#FFFFFF">
					<a href="exp2_regroup.jsp?id=<%=report.getPromtID()%>">
					<img  border='0' src="pics/go_export.gif"></a></td>
				</td>
				<td colspan="6" style="background-color:#FFFFFF" >
						<b>Regroup</b> the experiment before sending it to binbase.
						</td>
				
			</tr>
			<% } %>
			<tr>
				<td style="background-color:#6682B6" width="20"></td>
				<td width="0">Export (special)</td>
				<td align="center" style="background-color:#FFFFFF">
					<a href="export_C_promtselect.jsp?add=<%=report.getPromtID()%>">
					<img  border='0' src="pics/go_export.gif"></a></td>
				</td>
				<td colspan="6" style="background-color:#FFFFFF" >
						<b>Combine</b> multiple classes from different experiments in one export before exporting them.  
						</td>
				
			</tr>
	</table>
			
			


<%@ include file="footer.jsp"%>
