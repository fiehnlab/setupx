<%

/**
requires HQL connection
promtID
*/
try {
	// Logger.debug(this, promtID +  " checking access");
	PromtUserAccessRight accessRight = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID(), promtID);
	
	HashSet otherUserHashset = new HashSet();
	
	Iterator useriterator  = new SXQuery().findPromtUserAccessRightForPromtID(accessRight.getExperimentID()).iterator();
	
	while(useriterator.hasNext()){
		PromtUserAccessRight accessRight2 = (PromtUserAccessRight)useriterator.next();
		// Logger.debug(this, promtID +  " checking users 1");
		if (accessRight2.getUserID() != user.getUOID()){
		    try {
		        UserDO _u = (UserDO) hqlSession.createCriteria(UserDO.class).add(Expression.eq("id", new Long(accessRight2.getUserID()))).list().get(0);
		    	// Logger.debug(this, promtID +  " checking user 2");
				//UserDO _u = (UserDO)UserLocal.persistence_loadByID(UserLocal.class, hqlSession, accessRight2.getUserID());
				// filter system users out
				if ( (	!_u.isLabTechnician() 
				        || (_u.isLabTechnician() && _u.isSuperUser()) )
						&& _u.getUOID() != 12 // masteraccount
						&& _u.getUOID() != 29 // gert
						&& _u.getUOID() != 4 // gert
						) {
				    otherUserHashset.add(_u);
				}
		    } catch (Exception e){
		        Logger.warning(this, e.toString());
		    }
		}    
	}
	
	// Logger.debug(this, promtID +  " checking file exist");
	boolean resultFileExists = BBConnector.determineBBResultsArrived(promtID);
	
	// number of samples finished 
	// Logger.debug(this, promtID +  " counting samples");
	int samplesTotal = new SXQuery().findSampleIDsByPromtID(promtID).size();
	
	// number of all samples already that already ran.
	int numberOfScannedSamples = 0;
	
	//new: deactivate unnessary queries
	if (resultFileExists){
	    numberOfScannedSamples = samplesTotal;
	} else {
	    numberOfScannedSamples = new SXQuery().determineSamplesFinishedByPromtID(promtID);
	}
	boolean scheduled = (numberOfScannedSamples != 0);
	
	int samplesFinished = numberOfScannedSamples;//new SXQuery().determineSamplesFinishedByPromtID(promtID);
	
	boolean running = (samplesFinished > 0); // new SXQuery().determinePromtRunStarted(promtID);
	
	int classes = new SXQuery().findClazzIDsByPromtID(promtID).size();
	boolean finished  = (samplesFinished == samplesTotal);//new SXQuery().determinePromtFinished(promtID);
	
	
/*	
	// checking if additional metadata has been uploaded
	String metadataQuery = "select count(formobject.uoid) as s from formobject where formobject.PARENT = " + sampleID;
	int max_numberOfMetadataFields = Integer.parseInt(hqlSession.createSQLQuery(metadataQuery).addScalar("s", Hibernate.STRING).list().get(0).toString());
*/

	boolean found = false;
	int max_numberOfMetadataFields = 0;
	Iterator sampleIDs = new SXQuery().findSampleIDsByPromtID(promtID).iterator();
	/*
	while(!found && sampleIDs.hasNext()){
		String metadataQuery = "select count(formobject.uoid) as s from formobject where formobject.PARENT = " + sampleIDs.next();
		max_numberOfMetadataFields = Integer.parseInt(hqlSession.createSQLQuery(metadataQuery).addScalar("s", Hibernate.STRING).list().get(0).toString());
		if (max_numberOfMetadataFields > 4) found = true;
	}
	*/
	
	
	
	
	%>
	
	
	<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
	<%@page import="org.setupx.repository.core.user.UserDO"%>
	<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
	<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
	<%@page import="org.setupx.repository.core.communication.binbase.BBConnector"%>
	<%@page import="java.util.HashSet"%>
	<%@page import="org.hibernate.criterion.Expression"%>
	<%@page import="org.setupx.repository.core.util.logging.Logger"%>
	<%@page import="org.setupx.repository.server.persistence.PersistenceActionFindException"%>
<tr>
			<td colspan="1" align="center">
			    	<%
			    	int publicDataCount = 0;
			    	String countPubs = "";
			    	try {
				    	countPubs = "select data.uoid as s from pubdata as data, formobject as f where data.relatedExperimentID = f.uoid and f.uoid = " + promtID;
				    	
						publicDataCount = hqlSession.createSQLQuery(countPubs).addScalar("s", Hibernate.STRING).list().size();
			    	} catch (Exception e){
			    	}
	
			    	
			    	if (publicDataCount > 0) {%>
			    		<img  border='0' src="pics/shared.gif"><br><font class="small"><%=publicDataCount %> public dataset<%if(publicDataCount > 1){%>s<%}%>
			    	<%} else {%>
	
			    	<%} %>
			</td>
	
			<!-- lable and ID -->
			<th align="justify">
			    <table>
			    	<th><a href="load?id=<%=promtID %>&action=<%=PromtUserAccessRight.READ%>"><img  border='0' src="pics/details_small.gif" title="view experiment"></a>
				<% 
					 // icons to remove the whole experiment
					 if (user.isMasterUser(user.getUOID()) && user.isSuperUser()){
				    %>
			    	<th><a href="remove_share_confirm_single.jsp?id=<%=promtID%>"><img  border='0' src="pics/remove-share.gif" title="remove experiment from the list of 'my experiments'"></a>
			    	<th><a href="remove_share_confirm.jsp?id=<%=promtID%>"><img  border='0' src="pics/remove-share-o.gif" title="remove experiment from everybodys list of my experiments"></a>
			    	<th><a href="remove_confirm.jsp?id=<%=promtID%>"><img  border='0' src="pics/remove-all-o.gif" src="pics/remove-share-o.gif" title="DELETE Experiment"></a>
				<% } %>
			    	<th><a href="promtxmlexport.jsp?id=<%=promtID%>"><img src="pics/xml3.gif" border="0"></a>
			    	<%if (found){ %><th><img src="pics/meta3.gif"  border="0"><%} %>
			    	<th><b><font style="color:#6682B6"> ID:<%=promtID%></font></b>
				</table>

				<%=Promt.createShortAbstractHTML(promtID, 100)%>
							 
				<br>
				<font size="-2" style="color:#6682B6">structure:</font><font size="-2"><br>
					<%@ include file="incl_experiment_structure.jsp"%></font> 
				</font>			 
	
				 
				 
				 <%
					 Iterator iterator235 = otherUserHashset.iterator();
					 if (iterator235.hasNext()){
				 %>
				<font style="color:#6682B6" size="-2"> <br>collaborator:</font>
							 
				 <%}
				 while (iterator235.hasNext()){
				     UserDO ___userDO = (UserDO)iterator235.next();
				     %>
							<font size="-2"> 
								<!--  in case the email can not be determined - case for user created before Apr2-2007 -->
								<br><a href="mailto:<%try{%><%=___userDO.getMailAddress().getEmailAddress() %><%}catch (Exception e){ } %>"><img  border='0' src="pics/mail.gif"></a>  <%=___userDO.getDisplayName() %>
							</font>
				     <%
				 }	
				 
				 
				 try {
					 String _myComment = new SXQuery().findPromtComment(promtID);
					 if (_myComment.length() > 2){%>
						<font style="color:#6682B6" size="-2"> <br>comment:</font>
									<font size="-2"> 
								<br><%=_myComment%>
								</font>
					     
					     <%
					 }
				 } catch (Exception asdaa){
				     asdaa.printStackTrace();
				 }
				 %>
				 
				</th>


			<!-- depending if it is a public experiment or a private -->
			<% if (user.isPublic()){ %>
				<td align="justify" colspan="7" style="background-color:#FFFFFF">
					<nobr><%=new SXQuery().findSpeciesbyPromtID(promtID).get(0) %></nobr>
					<br>
					<%=new SXQuery().findQuestionValueString("organ name", promtID) %>
					<font class="small">
					<br>
					<%=new SXQuery().findQuestionValueString("Genotype", promtID) %>
					<br>
					<%=samplesTotal %> samples
					<br>
					<!-- number of classes -->
					<%=classes %> classes
					</font>

					<!-- report / summary button -->
					<td align="center" style="background-color:#FFFFFF">
					Details:<br>
					<% if((accessRight.getAccessCode() >= PromtUserAccessRight.READ)){%> 
						<a href="load?id=<%=promtID %>&action=<%=PromtUserAccessRight.READ%>"><img  border='0' src="pics/go.gif"></a>
					<%} else {%>
						<img  border='0' src="pics/go_locked.gif">
					<%} %>
					</td>
					<td/>

			<% } else { %>
			
			<!-- number of samples -->
			<td align="center" style="background-color:#FFFFFF"><%=samplesTotal %></td>
	
			<!-- number of classes -->
			<td align="center" style="background-color:#FFFFFF"><%=classes %></td>
		
			<!-- scheduled ? -->
			<td align="center" style="background-color:#FFFFFF">
			<% if(scheduled){%> 
				<img  border='0' src="pics/checked.jpg">
			<%} else {%>
			
				<!-- in case it is a lab tec  - add functions for creating an ACQ Task -->
				<% if (user.isLabTechnician() || user.isMasterUser(user.getUOID())) { %>
					<form action="aq/query" method="POST">
						<input type="hidden" name="<%=org.setupx.repository.web.WebConstants.QUERYSERVLET_QUERYTYPE%>" value="<%=org.setupx.repository.web.QueryServlet.SAMPLE_BY_EXPERIMENT%>"/>
						<input type="hidden" name="<%=org.setupx.repository.web.WebConstants.QUERYSERVLET_EXPERIMENT_ID%>" value="<%=promtID%>"/>
						<input type="image" src='pics/export.gif' alt="Create Acquistion Task">
					</form>
				<% } else { %>
					<img  border='0' src="pics/blank.jpg">
				<% }  %>
			<%} %>
			</td>
		
			<!-- is running ? -->
				<td align="center" style="background-color:#FFFFFF">
			<% if(running){%> 
				<a><img  border='0' src="pics/checked.jpg"></a>
			<%} else {%>
				<img  border='0' src="pics/blank.jpg">
			<%} %>
			</td>
			
			<!-- is finished ? -->
			<td align="center" style="background-color:#FFFFFF;padding: 0" >
			<% if(finished){%> 
				<a><img  border='0' src="pics/checked.jpg"></a>
			<%} else if (samplesFinished < 0){%>
				<!-- when the number is not determined  -->
				<img  border='0' src="pics/unknown.gif">
			<%} else {%>
				<!-- <img  border='0' src="pics/blank.jpg"> -->
			<%}%>
	
			<% if(samplesFinished > 0 &&  samplesFinished !=samplesTotal){%>		
				<!--  number of samples running -->
				<br>				
				<% try{ %>
					<img src="http://chart.apis.google.com/chart?cht=p3&chd=t:<%=(samplesFinished*100/samplesTotal)%>,<%=100-(samplesFinished*100/samplesTotal)%>&chs=120x50&chco=00ff00,ff0000">
					<font class="small"><%=(samplesFinished*100/samplesTotal)%>%</font>
				<% }catch(Exception e){ %>
				--%
				<%}%>
				<!--  TODO REPLACE BY IMAGE -->
			<%}%>
			</td>
		
			<!-- edit button -->
			<td align="center" style="background-color:#FFFFFF">
			<% if (user.isAdmin()){ %>
				<a href="accessright_experiment.jsp?pid=<%=promtID%>"><img  border='0' src="pics/go_share.gif"></a>
			<%} else if(running || scheduled || finished ){%> 
				<img  border='0' src="pics/go_blank.gif">
			<%} else if((accessRight.getAccessCode() < PromtUserAccessRight.WRITE)){%> 
				<img  border='0' src="pics/go_locked.gif" title="You have insufficient user rights to edit this Experiment.">
			<%} else {%>
				<a href="load?id=<%=promtID %>&action=<%=PromtUserAccessRight.WRITE%>"><img  border='0' src="pics/go.gif"></a>
			<%} %>
			</td>
			
			
			
			
			<!-- report / summary button -->
			<td align="center" style="background-color:#FFFFFF">
			<% if((accessRight.getAccessCode() >= PromtUserAccessRight.READ)){%> 
				<a href="load?id=<%=promtID %>&action=<%=PromtUserAccessRight.READ%>"><img  border='0' src="pics/go.gif"></a>
			<%} else {%>
				<img  border='0' src="pics/go_locked.gif">
			<%} %>

			<%if (user.isSuperUser()){ %>
				<br>
				<a href="pubexperiment.jsp?id=<%=promtID%>"><img  border='0' src="pics/go_msi.gif"></a>
				<br>
				<a href="report_raw.jsp?id=<%=promtID %>"><img  border='0' src="pics/go_report.gif"></a>
			<%} %>

			</td>
			
			<!-- clone / redoit button -->
			<td align="center" style="background-color:#FFFFFF">
				<% if(user.isLabTechnician()){ // (accessRight.getAccessCode() >= PromtUserAccessRight.CLONE)){%> 
					<a href="sample_status_create.jsp?id=<%=promtID%>"><img  border='0' src="pics/go_track.gif"></a>	
				<%} else {%>
					<img  border='0' src="pics/go_locked.gif" title="You have insufficient user rights to clone this Experiment.">
				<%} %>
			</td>
			
			
			<td align="center" style="background-color:#FFFFFF">
				<% if(finished && accessRight.getAccessCode() >= PromtUserAccessRight.EXPORT && !(org.setupx.repository.core.communication.binbase.BBConnector.determineBBResultsArrived(promtID))){%> 
					<a href="export_selection.jsp?id=<%=promtID %>"><img  border='0' src="pics/go_export.gif"></a>
				<%} else if(finished && accessRight.getAccessCode() >= PromtUserAccessRight.DOWNLOAD_RESULT && resultFileExists){%> 
					<a href="download_related_data_pre.jsp?id=<%=promtID %>"><img  border='0' src="pics/go_download.gif"></a>
				<%} else if(finished && accessRight.getAccessCode() < PromtUserAccessRight.EXPORT){%> 
					<a href="load?id=<%=promtID %>&action=<%=PromtUserAccessRight.EXPORT%>"><img  border='0' src="pics/go_locked.gif"></a>
				<%} else {%>
					<img  border='0' src="pics/go_blank.gif">
				<%}%>
			</td>


			<%} // closing from user is public  %>
			
			</tr>
		<%} catch (PersistenceActionFindException persistenceActionFindException){
			 // no access    
			 %>
			<tr>
				<td>
				<th align="left">
				<table>				 
			    	<th><b><font style="color:#6682B6"> ID:<%=promtID%></font></b>
				</table>
				<%=Promt.createShortAbstractHTML(promtID, 100)%>
				<br>
				<font size="-2" style="color:#6682B6">structure:</font><font size="-2"><br>
				<%@ include file="incl_experiment_structure.jsp"%></font> 
				 </font>	
				 <td colspan="21" style="background-color: white;" align="center" valign="middle">- coming soon -		 
				<%}
		%>