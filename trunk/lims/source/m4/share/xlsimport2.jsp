<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFCell"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="java.util.Date"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFDataFormat"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFCellStyle"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
</head>
<body>


<%@include file="incl_xls_header.jsp"%>


<table align='center' border='0' cellpadding='4' width="60%" border = 1 >
<tr>	
	<th colspan="21">Select the area the contains the data in your file.
<tr>	
	<td colspan="21">
		Please select the area containing the relevant data from the Matrix below. 
		If you want to delete a row press the icon to the left of it and it will be removed. 
		Same for the columns: Just press the column on top and the column will disappear. 
		<p>
		<b>Note:</b> If your XLS sheet contains <b>labels </b>for the data inside this matrix <b>do not 
		remove</b> it. You can use this data in the next upcoming step.


<%


	Vector rows = new Vector();
	try {
		rows = (Vector)session.getAttribute("data"); 
		if (rows == null){
		    throw new NullPointerException("no data available yet");
		}
	}catch (Exception e){
	    e.printStackTrace();
		rows = new Vector();
	}
	String resetString = "";
	try{ 
	    resetString = request.getParameter("reset"); 
	    if (resetString == null || resetString.compareTo("null") == 0) throw new Exception("no reset requested.");
	}catch (Exception e){
	    resetString = "";	    
	}
	
	try {
	    // delete data from the array
		String removeDimension = request.getParameter("dim"); 
		int intRemDim = Integer.parseInt(request.getParameter("remove")); 

		if (removeDimension.compareTo("y") == 0){
		    rows.remove(intRemDim);
		}
		if (removeDimension.compareTo("x") == 0){
			// loop through all rows and remove element nr intRemDim
			for(int i = 0; i < rows.size(); i++){
			    try {
				    ((Vector)rows.get(i)).remove(intRemDim);
			    } catch (Exception e){
			        // some dont have that many elements in a row
			    }
			}
		}
	}catch (Exception e){
	    e.printStackTrace();
	}

	if (resetString.compareTo("yes") == 0 || rows.size() == 0){
	    rows = new Vector();
	    // read data

	    File file = (File)session.getAttribute("sourcefile"); 
		FileInputStream fileInputStream = new FileInputStream(file);
		HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
	    int numberOfSheets = workbook.getNumberOfSheets();
		int numberOfClasses = 5;
	    
		// reading the data
	    HSSFSheet sheet = workbook.getSheetAt(0);

	    for (int rowCount  = sheet.getFirstRowNum(); rowCount <= sheet.getLastRowNum(); rowCount++){
	        HSSFRow row = sheet.getRow(rowCount);
	        Vector rowVector = new Vector();
	        if (row != null){
	            for (short cellCount = row.getFirstCellNum(); cellCount <= row.getLastCellNum(); cellCount++){
	                try {
		            	String value = "";
		            	try {
			                HSSFCell cell = row.getCell(cellCount);
			            	Logger.log(this, "celltype: "  + cellCount + ": " + cell.getCellType());
			            	Logger.log(this, "celltype: (Util): " + Util.getCellValue(cell));
		            	
			                switch (cell.getCellType()) {
				                case HSSFCell.CELL_TYPE_STRING:
				                    value = cell.getStringCellValue();
				                    break;
				
				                case HSSFCell.CELL_TYPE_FORMULA:
				                    value = "" + cell.getNumericCellValue();
				                    break;
				                    
				                case HSSFCell.CELL_TYPE_NUMERIC:
				                    HSSFCellStyle style = workbook.createCellStyle();
				                    style.setDataFormat(HSSFDataFormat.getBuiltinFormat("d-mmm-yy"));
				                    cell.setCellStyle(style);
				                    
				                    value = "" + cell.getNumericCellValue();

				                    // checking if it is a Date
				                    try {
					                    //value = Util.getCellValue(cell);
				                        int valueD = (int)cell.getNumericCellValue();
				                        if (valueD > 10000){
					                        System.out.println("trying to parse: " + valueD + " " + new Date(110, 1, 1).getTime() + " " + new Date(80, 1, 1).getTime());
						                    if ((valueD < new Date(110, 1, 1).getTime()) && (valueD > new Date(80, 1, 1).getTime())) {
						                        value = new Date((long)valueD).toLocaleString(); 
						                    }
				                        }
				                    } catch (Exception e){
					                    value = "" + cell.getNumericCellValue();
				                    }
				                    break;
				
				                default:
				                    
				                    break;
			                }
		                } catch (Exception e){
		                    value = "";
		                }

		                // rowVector.add(cellCount,value);
		                rowVector.add(value);
//		                Logger.debug(this, "got: " + value);
	                } catch(Exception e){
	                    Logger.warning(this, "" + e);
	                }
	            }
	        } else {
				// nada
	        }
	        rows.add(rowCount, rowVector);
	        //Logger.debug(this, "got: " + rowVector);
	    }
	    session.setAttribute("data", rows); 
	}
	
	
	// find the max length of the vectors
	int maxRow = 0;
	for(int i = 0; i < rows.size(); i++){
	    try {
		    if (((Vector)rows.get(i)).size() > maxRow) maxRow = ((Vector)rows.get(i)).size() ;
	    } catch (Exception e){
	        // some dont have that many elements in a row
	    }
	}

	
	
	%>
        <tr>
	        <td>
    	    </td>
		<%for(int i = 0; i < maxRow; i++){%>
	        <th align="center">
				<a href="?dim=x&remove=<%=i%>">
					<img src='pics/remove-all.gif' border="0" valign="middle" align="center">
				</a>
	   	    </th>
		<%}%>

    	</tr>
        <%
			Iterator rowIterator = rows.iterator();
        	int rowCounter = -1;
        	while(rowIterator.hasNext()){
        	    rowCounter++;
        	    Vector row = (Vector)rowIterator.next();
                %>
		<tr>
            <th>
				<a href="?dim=y&remove=<%=rowCounter%>">
					<img src='pics/remove-all.gif' border="0" valign="middle" align="center">
				</a>
            </th>
	        </td>
	        	<% for (int i = 0; i < row.size(); i++){
	        	   %> 
			        <td align="right">
	        		<%=row.get(i)%>
		    	    </td>
	        	   <%  
	        	}%>
	       	</td>
		</tr>
		<% } // row iterator %>
</table>



<%@include file="incl_xls_footer.jsp"%>



</body>
</html>