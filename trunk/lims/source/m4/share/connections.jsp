<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<%@ include file="checklogin.jsp"%>
<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<body>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center">
			<a href="main.jsp">	
			<img src='pics/logo.gif' border="0" valign="middle" align="left">
			</a>
		</td>
		<th>
			<h2>Connections</h2><br>Connections to services that are connected to the system.
		</th>
	
		<td align="center" width="2" colspan="2">
			<a href="www.ucdavis.edu">
			<img src='pics/wires.gif' border="0" valign="right" align="right">
			</a>
		</td>
	</table>




<%@ include file="navi_admin.jsp"%>

<table align='center' border='0' cellpadding='4' width="60%">
<tr>
<th></th>
<th colspan="4">Connection Status WebServices</th>
<th></th>
</tr>



<tr>
<td></td>
<td>SampleLog Scanner</td>
<td align="center">
<img  border='0' src="pics/help_small.gif">
<td><a href="http://128.120.136.206:8080/m1/scan?active=true"><img  border='0' src="pics/url.gif"></a></td>
<td width="2"></td>
<td></td>
</tr>



<tr>
<td></td>
<td>LDAP</td>
<td align="center">
<% try {
	new org.setupx.repository.core.communication.ldap.LDAPConnector().checkAvailability();
    %>
<img  border='0' src="pics/check.gif">
    <% 
}catch (Exception e){
    %>
    <img  border='0' src="pics/x.gif">
    <% 
} %>
<td></td>
<td width="2"></td>
<td></td>
</tr>


<tr>
<td></td>
<td>DatabaseCache</td>
<td align="center">
<img  border='0' src="pics/check.gif">
<td>
    <a href="cache.jsp"><img  border='0' src="pics/url.gif"></a>
<td width="2"></td>
<td></td>
</tr>



<tr>
<td></td>
<td>NCBI</td>
<td align="center">
<% try {
	new org.setupx.repository.core.communication.ncbi.NCICB_connector().checkAvailability();
    %>
<img  border='0' src="pics/check.gif">
    <% 
}catch (Exception e){
    %>
    <img  border='0' src="pics/x.gif">
    <% 
} %>
<td></td>
<td width="2"></td>
<td></td>
</tr>

<tr>
<td></td>
<td>NCBI-Abbreviations</td>
<td align="center">
<img  border='0' src="pics/check.gif">
<td> 
    <a href="abbreviation.jsp"><img  border='0' src="pics/url.gif"></a>
</td>
<td width="2"></td>
<td></td>
</tr>

<tr>
<td></td>
<td>NCBI-cache</td>
<td align="center">
<img  border='0' src="pics/check.gif">
<td> 
    <a href="status_info.jsp"><img  border='0' src="pics/url.gif"></a>
</td>
<td width="2"></td>
<td></td>
</tr>


<tr>
<td></td>
<td><%=org.setupx.repository.Config.SYSTEM_NAME%> WebService</td>
<td align="center">
<% 
String url = "services/m1?wsdl";
try {
		org.setupx.repository.core.util.Util.checkURL(new java.net.URL(url));
	    %>
	<img  border='0' src="pics/check.gif">
	    <% 
	}catch (Exception e){
	    %>
	    <img  border='0' src="pics/x.gif">
	    <% 
	} %>
<td> 
    <a href="<%=url%>"><img  border='0' src="pics/url.gif"></a>
</td>
<td width="2"></td>
<td></td>
</tr>



<tr>
<td></td>
<td>BinBase WebService</td>
<td align="center">
<% 
url = "http://128.120.136.169:8080/BCI/BinBaseService?wsdl";
try {
	org.setupx.repository.core.util.Util.checkURL(new java.net.URL(url));
    %>
<img  border='0' src="pics/check.gif">
    <% 
}catch (Exception e){
    %>
    <img  border='0' src="pics/x.gif">
    <% 
} %>
<td> 
    <a href="<%=url%>"><img  border='0' src="pics/url.gif"></a>
</td>
<td width="2"></td>
<td></td>
</tr>




<tr>
<td></td>
<td>VariationsCache <font class="small">refresh / init cache for Multifield4Clazzes</font></td>
<td align="center">
<img  border='0' src="pics/check.gif">
<td> 
    <a href="init_cache.jsp"><img  border='0' src="pics/url.gif"></a>
</td>
<td width="2"></td>
<td></td>
</tr>






<tr>
<th></th>
<th colspan="4">WebServer</th>
<th></th>
</tr>

<tr>
<td></td>
<td>Metabolomics Core Web</td>
<td align="center">
<% 
url = "http://metacore.ucdavis.edu/";
try {
	org.setupx.repository.core.util.Util.checkURL(new java.net.URL(url));
    %>
<img  border='0' src="pics/check.gif">
    <% 
}catch (Exception e){
    %>
    <img  border='0' src="pics/x.gif">
    <% 
} %>
<td> 
    <a href="<%=url%>"><img  border='0' src="pics/url.gif"></a>
</td>
<td width="2"></td>
<td></td>
</tr>



<tr>
<td></td>
<td>Fiehnlab Web</td>
<td align="center">
<% 
url = "http://fiehnlab.ucdavis.edu/";
try {
	org.setupx.repository.core.util.Util.checkURL(new java.net.URL(url));
    %>
<img  border='0' src="pics/check.gif">
    <% 
}catch (Exception e){
    %>
    <img  border='0' src="pics/x.gif">
    <% 
} %>
<td> 
    <a href="<%=url%>"><img  border='0' src="pics/url.gif"></a>
</td>
<td width="2"></td>
<td></td>
</tr>




</table>


<%@ include file="footer.jsp"%>