<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBI_Classifier"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>


<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.Iterator"%>

<%

// query is used for the chart in repository_species
String __query = "select label.value as value, count(promt.uoid) as c, sum(cache.numberofScannedSamples) as samples "
        + " from formobject as promt "
        + " inner join formobject as page "
        + " on page.parent = promt.uoid "
        + " inner join formobject as species "
        + " on species.parent = page.uoid  "
        + " inner join formobject as ncbispecies "
        + " on ncbispecies.parent = species.uoid  "
        + " inner join formobject as label  "
        + " on label.parent = ncbispecies.uoid  "
        + " inner join cache as cache "
        + " on cache.experimentID = promt.uoid "
        + " inner join formobject as f "
        + " on f.uoid = cache.experimentID "
        + " where ncbispecies.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\"   "
        + " and label.value != \"\"   "
        + " and cache.numberofScannedSamples > 0 "
        + " group by label.value " + " order by c desc ";
        
// Logger.log(this, "query: " + __query);

List _list = CoreObject.createSession().createSQLQuery(__query)
        .addScalar("c", Hibernate.INTEGER).addScalar("value",
                Hibernate.STRING).addScalar("samples",
                Hibernate.INTEGER).list();


// ids of each kingdom
int[] speciesType = new int[] { 
        	NCBIEntry.PLANT,
        	NCBIEntry.MIRCOORGANISM, 
        	NCBIEntry.ANIMAL, 
        	NCBIEntry.HUMAN
        	};	

int[] countSamples = new int[speciesType.length];
int[] countExp = new int[speciesType.length];


%>



<%if (show){ %>
<table width="80%" border="1" align="center">
	<tr>
		<th colspan="5">
			<h3>Top Species</h3>
			<font class="small">List of the Top 5 Species per Kingdom</font>
		</th>
	</tr>


	<tr>
		<td colspan="2">Kingdom / species
		<td>Samples
		<td>Experiments
<%}%>
		
		
	
	<%
		// loop over the kingdoms
		for (int sp = 0; sp < speciesType.length; sp++) {
		    int kingdomspecifier = speciesType[sp];
		    
			Iterator listIter = _list.iterator();
			
			boolean isFirst = true;
			int limit = 6;

			while (listIter.hasNext()){
			    Object[] objects = (Object[])listIter.next();
				
			    String label = objects[1] + "";
			    String value = objects[0] + ""; 
			    String nr_samples = objects[2] + ""; 
			    
			    int ncbiID = NCBIConnector.determineNCBI_Id(label);
			    NCBIEntry ncbiEntry = NCBIConnector.determineNCBI_Information(ncbiID);
			    
			    if (ncbiEntry.getSpeciesType() == kingdomspecifier){
			        int tmp = countSamples[sp];
			        tmp = tmp + Integer.parseInt(nr_samples);
			        countSamples[sp] = tmp;
			            
			        tmp = countExp[sp];
			        tmp = tmp + Integer.parseInt(value);
			        countExp[sp] = tmp;
			        
				    limit--;
				    
				    
				    
				    if (isFirst && show) {
				        isFirst = false;
				        %>

				 	<tr>
						<th colspan="2">
							<%=ncbiEntry.getSpeciesTypeName() %>    
						<th>
						<th>

				        
				        <% 
				    }
				    
				    if (limit > 0 && show){
					    %>
						<tr>
							<td>&nbsp;&nbsp;&nbsp;
							<td><a href="advancedquery_dev.jsp?term=<%=label %>&&type=0"><%=label %>    
							<td><%=nr_samples %>    
							<td><%=value %>    
						<%
				    }
			    }
			}
			    
	    %>
		   	<%
		}
	%>		
	
<%if (show){ %>
</table>




<br>
<br>
<br>
<table width="80%" border="1" align="center">
	<tr>
		<th colspan="5">
			<h3>Kingdoms</h3>
			<font class="small">Number of Samples and Experiments per Kingdoms</font>
		</th>
	</tr>
	<tr>
		
		<th>Kingdom
		<th colspan="2">Samples
		<th colspan="2">Experiments
<%}
// count to a total number
int totalSamples = 0;
int totalExp = 0;

for (int t1 = 0; t1 < countSamples.length; t1++) {
    totalSamples = totalSamples + countSamples[t1];
    totalExp = totalExp + countExp[t1];
} 

// in case it is an empty DB 
if (totalExp == 0) totalExp = 1;
if (totalSamples == 0) totalSamples = 1;



for (int sp = 0; show && sp < speciesType.length; sp++) {
    %>
	<tr>
		<td><%=NCBIEntry.getSpeciesTypeName(speciesType[sp])%> 
    	<td><%=countSamples[sp]%> 
    	<td><%=countSamples[sp] * 100 / totalSamples %> % 
    	<td><%=countExp[sp]%> 
    	<td><%=countExp[sp] * 100 / totalExp%> % 
<%} %>


<%if (show){
    


%>
</table>
<a name="kingdom">
<table width="80%" border="1" align="center">
	<tr>
		<td style="background-color: white">
<%} %>
<img border="0" src="http://chart.apis.google.com/chart?cht=p3&chco=0000ff&chd=t:<%
for (int sp = 0; sp < speciesType.length; sp++) {
	if(sp != 0) out.print(",");
	%><%=countExp[sp]%><%} 
	%>&chl=<%
for (int sp = 0; sp < speciesType.length; sp++) {
	if(sp != 0) out.print("|");
	%><%=NCBIEntry.getSpeciesTypeName(speciesType[sp]) + " (" + countSamples[sp] + ")"%><%} 
	%>&chco=008000,FF0011&chs=500x130">

	<%if (show){%>
</table>
<%} %>
	