

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.web.forms.FormObject"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<html>
<body>

<%@ include file="checklogin.jsp"%>
<%@ include file="incl_header_msi.jsp"%>


<table align="center" width="60%" cellpadding="5">

<tr>
	<th rowspan="301">&nbsp;
	<th>Field
	<th>
	<th>
	<th rowspan="301">&nbsp;
	
<% 
String query = "select question, description, extension from formobject where msiattribute is null or msiattribute not in (select uoid from msiattribute) group by question"; 
 
Session s = CoreObject.createSession();

List l = s.createSQLQuery(query)
			.addScalar("question", Hibernate.STRING)
			.addScalar("description", Hibernate.STRING)
			.addScalar("extension", Hibernate.STRING)
			.list();

Iterator iterator = l.iterator();

while (iterator.hasNext()){
    Object[] objects = (Object[])iterator.next();
    %>
    
    <tr>
    	<td><%=objects[0]%>
    		<font style="font-size: x-small;"><br><%=objects[1]%></font>
    		<font style="font-size: xx-small;"><br><%=objects[2]%></font>    	
    	<td>
    		    <table width="100%">
    		<%
    		if (objects[1].toString().compareTo("custom field") == 0){
    		    // it is a custom field !!
    		} else {
    		    %>
   		    	<tr>
    		    	<td>
		    		    <a href="msi_attributes.jsp?question=<%=objects[0]%>&description=<%=objects[1]%>&extension=<%=objects[2]%>"><img src="pics/go_new.gif" border="0"></a>
		    		<td><font style="font-size: xx-small;">create MSI attribute based on this</font>
    		    <%
    		} 	%>
    			<tr>
    				<td>
			    		<form action="msi_assign_fields.jsp">
				    		<input type="hidden" name="question" value="<%=objects[0]%>">
				    		<input type="image" name="search" src="pics/go.gif" border="0">
			    		</form>
    				<td><font style="font-size: xx-small;">assign to msi attribute <br>(select it first)</font>



				<% // former assignments 
					String formerQuery = "select m.* from formobject as f, msiattribute as m where m.uoid = f.msiattribute and f.msiattribute is not null and f.msiattribute in (select uoid from msiattribute) and f.question like \"%" + objects[0] + "%\" group by m.uoid";
					List formerAssignments = s.createSQLQuery(formerQuery).addEntity(MSIAttribute.class).list();
					Iterator fIterator = formerAssignments.iterator();
					while(fIterator.hasNext()){
					    MSIAttribute attribute = (MSIAttribute) fIterator.next() ;
					    %>
					    <tr>
							<td><a href="msi_assign_commit.jsp?msi=<%=attribute.getUOID()%>&question=<%=objects[0]%>"><img src="pics/go.gif" border="0"></a>
		    				<td style="background-color: gray	;"><font style="font-size: xx-small;">assign to msi attribute <br>(<%=attribute.getMsiLabel()%></b>)</font>
<%
					}
				%>

			    </table>
			    	
<%
	} 
%>
</body>
</html>



