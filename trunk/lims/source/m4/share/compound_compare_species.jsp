<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBI_Classifier"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Hashtable"%>




<table align="center" width="60%" cellpadding="7">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th colspan="2" >
			<h2>Public Experiments</h2>
				<br>The following experiments are shared with the public. <br>
				Feel free to download look at the experimental design and download all the releated data.
		</th>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<%@ include file="checknonlogin.jsp"%>



<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} else { %>
<%@ include file="navi_public.jsp"%>
<%} %>



<% 


int g1 = 32644,  g2 = 32644, g3 = 32644; 

Hashtable datasets = new Hashtable();
try {
	g1 = Integer.parseInt(request.getParameter("g1"));
} catch (Exception e){
    g1 = 32644;
}

try {
	g2 = Integer.parseInt(request.getParameter("g2"));
} catch (Exception e){
    g1 = 32644;
}

try {
 	g3 = Integer.parseInt(request.getParameter("g3"));
} catch (Exception e){
    g1 = 32644;
}



int[] speciesIDs = new int[]{g1,g2,g3};

for (int spC = 0; spC < speciesIDs.length; spC++){
    List data1 = new SXQuery().findComoundNamesBySpecies(speciesIDs[spC]);
    datasets.put(NCBIConnector.determineNCBI_Information(speciesIDs[spC]).getName(), data1);
}

%>


	
<%@ include file="venn.jsp"%>