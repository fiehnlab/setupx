<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.List,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 	org.setupx.repository.core.communication.leco.*,
                 org.setupx.repository.server.persistence.*"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<body>
	<table align="center" width="60%" cellpadding="7">	<tr>
		<th  colspan="4">Export Experiment</th>
	</tr>

<%
    // dermine promtID 
    String column = request.getParameter("column");
	ColumnInformation columnInformation = ColumnDetector.getColumnInformationByID(column);
	
	int promtID = Integer.parseInt(request.getParameter("id"));

	try {
	    Promt.export(promtID, columnInformation);
	    %>


	<tr>
		<td colspan="1" align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/download.gif"></td>
		<td colspan="2" width = 90%><h3>The Export of your Experiment has been scheduled.</h3><p>The Export is quiet time consuming - because of that the data is not available right now.
			<br>As soon as the Export is finished, we will inform you and right then you will be able to download the data from <%=org.setupx.repository.Config.SYSTEM_NAME%>.
			<p>
		</td>
		<td colspan="1"></td>
	</tr>
	<tr>
		<td colspan="1"></td>
		<td colspan="2"></td>
		<td colspan="1"></td>
	</tr>
	<tr>
		<td colspan="1" align="right" style="background-color:#FFFFFF"><a href="admin.jsp"><img  border='0' src="pics/back.gif"></td>
		<td colspan="2"></td>
		<td colspan="1"></td>
	</tr>
	<% 
	} catch (org.setupx.repository.core.communication.CommunicationException e){
	%>
	<tr>
		<td colspan="1" align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/error.jpg"></td>
		<td colspan="2" width = 90%><h3>The Export of your Experiment failed.</h3>
		<p>The System was not able to start the Export with the given setting.
			<p><%=e.getMessage()%></td>
		<td colspan="1"></td>
	</tr>
	<tr>
		<td colspan="1"></td>
		<td colspan="2"></td>
		<td colspan="1"></td>
	</tr>
	<tr>
		<td colspan="1" align="right" style="background-color:#FFFFFF"><a href="admin.jsp"><img  border='0' src="pics/back.gif"></td>
		<td colspan="2"></td>
		<td colspan="1"></td>
	</tr>
	<%
	}
	%>
</table>
</body>

<%@ include file="footer.jsp"%>
