<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="javax.xml.XMLConstants"%>
<%@page import="org.setupx.repository.core.util.xml.XMLFile"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIQuery"%>
<%@page	import="org.setupx.repository.core.communication.msi.AttributeValuePair"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.setupx.repository.core.util.xml.XMLUtil"%>
<%@page import="org.w3c.dom.Element"%>

<%
            AttributeValuePair[] values_promt = null;
            AttributeValuePair[] values_classes = null;
            AttributeValuePair[] values_sample = null;

            Element promt_attrib = null;
            Element class_attrib = null;
            Element sample_attrib = null;


            long promtID = Long.parseLong(request.getParameter("id"));
            boolean promtKnown = false;
            List classIDs = new SXQuery().findClazzIDsByPromtID(promtID);

            Document document = XMLUtil.createNewDocument();

            document.appendChild(document.createElement("export"));
            Element root = document.getDocumentElement();

			// additon - for data -->
			String queryAttributeNames = "select att.label as l from pubattribute as att, pubsample as samp, pubdata where pubdata.relatedExperimentID = " + promtID + " and att.parent = samp.uoid and samp.parent = pubdata.uoid group by att.label "; 
			Session hibernateSession = CoreObject.createSession();
			List attributeNames = hibernateSession.createSQLQuery(queryAttributeNames).addScalar("l", Hibernate.STRING).list();
			// <---

            
            // looping over all classes
            for (int iClassPos = 0; iClassPos < classIDs.size(); iClassPos++) {

                long clazzID = Long.parseLong("" + classIDs.get(iClassPos));
                boolean classKnown = false;
                List sampleIDs = new SXQuery().findSampleIDsByClazzID((int) clazzID);

                // looping over all sampleIDs 
                for (int iSamplesPos = 0; iSamplesPos < sampleIDs.size(); iSamplesPos++) {

                    long sampleID = Long.parseLong("" + sampleIDs.get(iSamplesPos));
                    // Logger.debug(this, "sample " + sampleID + " " + iSamplesPos + 1 + "/" + sampleIDs.size());
                    if (promtKnown) {

                    } else {
                        // Logger.debug(this, "creating report for promt "  + promtID + " " + 1 + "/" + 1);
                        values_promt = MSIQuery
                                .searchAttributesPerExperimentBySampleID(sampleID);
                        // Logger.debug(this, "done creating report for promt "  + promtID + " " + 1 + "/" + 1);
                        promtKnown = true;

                        try {
                            promt_attrib = document.createElement("study");
                            promt_attrib.setAttribute("SXUOID", "" + promtID);
                            root.appendChild(promt_attrib);
                            MSIQuery.toXML(values_promt, document, promt_attrib);
                        } catch (Exception e) {
                            // Logger.log(this, "problems with study");
                            e.printStackTrace();
                        }
                    }

                    if (classKnown) {

                    } else {
                        // Logger.debug(this, "creating report for class "                                 + clazzID + " " + iClassPos + "/"                                  + classIDs.size());
                        values_classes = MSIQuery
                                .searchAttributesPerClassBySampleID(sampleID);
                        // Logger.debug(this, "done.");
                        classKnown = true;
                        try {
                            class_attrib = document.createElement("class");
                            class_attrib.setAttribute("SXUOID", "" + clazzID);
                            promt_attrib.appendChild(class_attrib);
                            MSIQuery.toXML(values_classes, document,
                                    class_attrib);
                        } catch (Exception e) {
                            // Logger.log(this, "problems with classes");
                            e.printStackTrace();
                        }

                    }

                    // Logger.debug(this, "creating report for sample " + sampleID  + " " + iSamplesPos + "/" + sampleIDs.size());
                    values_sample = MSIQuery
                            .searchAttributesPerSampleBySampleID(sampleID);
                    try {
                        sample_attrib = document.createElement("sample");
                        sample_attrib.setAttribute("SXUOID", "" + sampleID);
                        MSIQuery.toXML(values_sample, document, sample_attrib);
                        class_attrib.appendChild(sample_attrib);
                    } catch (Exception e) {
                        // Logger.log(this, "problems with samples");
                        e.printStackTrace();
                    }
                    
                    // check if with ir without
                    String inclData = "";
                    try {
                        inclData = request.getParameter("data");
                        if (inclData == null) throw new Exception("can not be null"); 
                    }catch (Exception e){
                        inclData = "";
                    }
                    if (inclData.compareTo("true") == 0){
                        
                        // NEW -  adding annotation information into the xml file
                        
                        // Logger.debug(this, "getting datapoints for sampleID: " + sampleID);
                           String valueQuery = "select att.value as v, att.label as l from pubattribute as att , pubsample as s where s.setupXsampleID = " + sampleID + " and s.uoid = att.parent";
                           List values = hibernateSession.createSQLQuery(valueQuery).addScalar("v", Hibernate.STRING).addScalar("l", Hibernate.STRING).list();
    
                        Element anno = document.createElement("annotation");
                        Element binbase = document.createElement ("binbase");
                        sample_attrib.appendChild(anno);
                        anno.appendChild(binbase);
    
                           // sort the values
                           Iterator xaf = attributeNames.iterator();
                           while(xaf.hasNext()){
                               String attName = xaf.next().toString();
                               
                               // loop and get the correct value
                               Iterator valueIterator = values.iterator();
                               while(valueIterator.hasNext()){
                                   Object[] objects = (Object[])valueIterator.next();
                                   if (objects[1].toString().compareTo(attName) == 0){
                                       // Logger.log(this,objects[0] + "");
                                       
                                       Element attribute = document.createElement("dataprocessingpoint");
                                       Element name = document.createElement("name");
                                        name.appendChild(document.createTextNode("" + objects[0]));
               
                                       Element value = document.createElement("value");
                                     value.appendChild(document.createTextNode("" + objects[1]));
    
                                    binbase.appendChild(attribute);
                                    attribute.appendChild(name);
                                    attribute.appendChild(value);
                                   }
                               }
                           }
                    }
                    
                    // Logger.debug(this, "done.");
                }
            }

			
	out.clear();
	root.setAttribute("xmlns:xsi", XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI);
	root.setAttribute("xsi:noNamespaceSchemaLocation", "MSI-SetupX-001.xsd");
	response.setContentType("text/xml");
	response.setCharacterEncoding("UTF-8");
	out.write(new XMLFile(document).content());
	%>

            	
