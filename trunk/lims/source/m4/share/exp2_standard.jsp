<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.communication.importing.logfile.*,
				 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.communication.leco.*,
                 org.setupx.repository.core.util.*,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="org.setupx.repository.core.communication.file.SampleFile"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%
int promtID = Integer.parseInt(request.getParameter("id"));
%>
<body>



<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width='60%'>
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Export</h2>
			Samples & Column
			<br/>
			<font size="-2">Below you find all samples including every available run for each sample related to the selected experiment.<br>Choose all samples that you want to include in the result.</font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>


<form method="post" action="exp2_summary.jsp">

<!--  column information -->
	<table align="center" width="60%" cellpadding="2">






	<table align="center" width="60%" cellpadding="2">	<tr>
		<th colspan="10">Select Samples for Export <%=promtID %></th>
	</tr>

<%


List classList = new SXQuery().findClazzIDsByPromtID(promtID);
Iterator iterator = classList.iterator();

// each class
int staticCount = -1;
while (iterator.hasNext()) {
    staticCount++;
    int classID = Integer.parseInt("" + iterator.next());

    // class labels
    java.util.Iterator iterator1 = new SXQuery().findLabelsforClazz(classID).iterator();
    %>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<%
		int group = staticCount;
		String linkOtherClasses = "";
		try {
		    try {
			    group = Integer.parseInt(request.getParameter("group" + classID));    
		    } catch (NumberFormatException e){
		        //group stays staticcount
		    }
		    
		    // create a string containing ALL OTHER classes with there grouping
		    java.util.Iterator allClasses = new SXQuery().findClazzIDsByPromtID(promtID).iterator();
		    int initialID = -1;
		    while (allClasses.hasNext()){
		        initialID++;
		        long _id = Long.parseLong("" + allClasses.next());
		        if (_id != classID){

		            int _tmpGroup = initialID;
					try {
					    // in case the ids are not initaliesed.
			            _tmpGroup = Integer.parseInt(request.getParameter("group" + _id) );
					} catch (Exception e){
					    org.setupx.repository.core.util.logging.Logger.debug(this, e.getMessage());
					}
		            linkOtherClasses = linkOtherClasses + "&group" + _id + "=" + _tmpGroup;    
					
		            org.setupx.repository.core.util.logging.Logger.debug(this, "LINK OTHERS: " + linkOtherClasses);
		        }
		    }
		    
		} catch (Exception e){
			e.printStackTrace();
		}
		
		%>
		<th></th>
		<th colspan="3" align="center" style="background-color:<%=Util.getColor(classID*2)%>">Class <b><%=classID %></b></th>

		<th colspan="6">
			
			<table align='center' border='0' cellpadding='4'>
			  <tr>
			  
				<% 
				//deactivated
				for (int cl = 0; cl < 0; cl++){ //classList.size(); cl++){ %>
				    <!--  missing is the ids of all other classes in the href -->
					<th>
						<!--  deactivated  -->
						<!--  <a href="export_selection.jsp?id=<%=promtID%>&group<%=classID%>=<%=cl%><%=linkOtherClasses %>"> -->
							<img src='<%if (cl==group){ %>pics/checked.jpg<%} else {%>pics/blank.jpg<%}%>' border="0" valign="middle" align="center">
						<!-- </a> -->
						<%if (cl==group){ %><input type="hidden" name="group<%=classID%>" value="<%=group%>"><%} %>


					</th>
				    <th valign="middle" align="center" style="background-color:<%=Util.getColor(cl*2)%>" >
				    	<b><%=cl%></b>
				    </th>
			    <%} %>
			  </tr>
			</table>
		</th>
		<th></th>
	</tr>
	<tr>
	<td colspan="1"></td>
	<td style="background-color:<%=org.setupx.repository.core.util.Util.getColor(classID*2)%>"  colspan="1"></td>
	<td colspan="2">
	<td colspan="6">
		<table>
    <% 
    while (iterator1.hasNext()) {
    Object[] element = (Object[]) iterator1.next();
      %>
			<tr>
					<td><%=element[0].toString()%>:</td>
					<td><b><%=element[1].toString()%></b></td>
			</tr>
      <%
    }
 	%>
			</table>
	    </td>
	  <td></td>
	</tr>
	
	
	<%
    // samples in that class
    Iterator sampleIDs = new SXQuery().findSampleIDsByClazzID(classID).iterator();
    while (sampleIDs.hasNext()){
        int sampleID = Integer.parseInt("" +  sampleIDs.next());
        //String label = new SXQuery().findSampleLabelBySampleID(sampleID);
        
		List list = new SXQuery().findScannedPair(sampleID);
        Iterator scannedPairs = list.iterator();
        boolean checked = true;
        
        %>

		<% if(list.size() == 0){%>
        <tr>
        <td ></td>        
        <td style="background-color:<%=org.setupx.repository.core.util.Util.getColor(classID*2)%>" align="center"></td>        
        <td align="center" style="background-color:#FFFFFF"></td>        
        <td align="center">Sample <b><%=sampleID%></b></td>        
        <td align="center"><%=new SXQuery().findSampleLabelBySampleID(sampleID) %></td>        
        <td align="center" colspan="3"><b>Sample has not been run yet.</b></td>        
		<td align="center"><img src='pics/hprio_tsk.gif' border="0" valign="middle" align="center"></td>        
        <td align="center" style="background-color:#FFFFFF"><input type="checkbox" disabled="disabled"></td>        
        <td></td>        
        </tr>
		<%} %>        
        
        
        <%
        HashSet hash = new HashSet();
        boolean first= true;
        while (scannedPairs.hasNext()) {
			String acqname = (String) scannedPairs.next();
			if (!hash.contains(acqname)){
	            hash.add(acqname);
				boolean exists = SampleFile.exists(acqname, SampleFile.TXT);  
	            %>
	        <tr>
	        <td></td>        
			<td style="background-color:<%=Util.getColor(classID*2)%>" align="center"></td>
			<td width="2" align="center" style="background-color:#FFFFFF"><% if (first){ %><a href="sample_detail.jsp?id=<%=sampleID%>"><img src='pics/details.gif' border="0" valign="middle" align="center"></a><%}%></td>        
	        <td align="center"><% if (first){ %>Sample <b><%=sampleID%> <%}%></b></td>        
	        <td align="center"><% if (first){ %><%=new SXQuery().findSampleLabelBySampleID(sampleID) %> <%}%></td>        

	        <!--  hotfix -->
	        <td align="center" <%if (!exists){%> style="background-color:#FF545A" <%} %> colspan="3"><%=acqname%></td>

			<%if (exists){ %>
	        <td align="center" valign="middle" width="2">
				<a href="run_detail_acq.jsp?acq=<%=acqname%>">	
		    	    <img src='pics/details.gif' border="0" valign="middle" align="center">
		        </a>
	        </td>        
	        <td align="center" style="background-color:#FFFFFF"><input type="checkbox" name="<%=acqname%>" value="<%=sampleID%>" <%if (checked){ %>checked="checked" <%} %>></td>        
	        <%} else { %>
			<td align="center" valign="middle">
				<img src='pics/warning.gif' border="0" valign="middle" align="center">
				<font class="error">Sample ran successfully but file was not converted.</font>
			</td>
	        <td align="center" style="background-color:#FFFFFF"><input type="checkbox" disabled="true" name="<%=acqname%>" value="<%=sampleID%>" <%if (checked){ %>checked="checked" <%} %>></td>        
	        <%}%>
	        <td></td>        
	        </tr>
			<%
            if (checked){   
                checked = false;
            }
        	}
			first = false;
        }
    }
}
%>
	  		<tr>
  					<td><input type="hidden" name="promt" value="<%=promtID%>"></td>
  					<td colspan="6" align="right">Send selection to Export.  </td>
  					<td colspan="2" align="left"><input src='pics/go_export.gif' type="image" value="connect"/></td>
  					<td></td>
	  		</tr>
	<tr>
		<td colspan="1"></td>
		<td colspan="8">
		<td colspan="1"></td>
	</tr>
	<%
	%>
</table>
</form>
</body>

<%@ include file="footer.jsp"%>
