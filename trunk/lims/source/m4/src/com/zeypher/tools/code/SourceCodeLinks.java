package com.zeypher.tools.code;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector ;

public class SourceCodeLinks {

    private File sourcefolder;
    private Vector files = new Vector();
    private Vector links = new Vector();

    public SourceCodeLinks(File folder) {
        this.sourcefolder = folder;
        
        // receive the name of all jsps
        readNames(this.sourcefolder);
        System.out.println("numnber of files:" + this.files.size() );
        
        // receive the names of links to jsps in jsps and in java
        findReferences(this.sourcefolder);
        
        // find non linked jsps
        findDeadJSPs();
    }


    private void findDeadJSPs() {
        StringBuffer pageWriteBuffer = new StringBuffer();
        
        File targetFolder = new File(this.sourcefolder.getAbsolutePath() + File.separator + "deprecated" + File.separator);
        System.out.println("" + targetFolder.getAbsolutePath()); 
        if (!targetFolder.exists()){
            targetFolder.mkdirs();
        }
        

        
        // take all filenames 
        Iterator iterator = files.iterator();
        
        while (iterator.hasNext()) {
            File tmpFile = ((File)iterator.next());
            String filename = tmpFile.getName();
            boolean found = false;
            
            // and look for those that are not linked
            for (int i = 0; !found && i < links.size(); i++){
                Link l = (Link)links.get(i);
                if (l.getTarget().getName().compareTo(filename) == 0){
                    found = true;
                }
            } 

            
            if (!found){
                System.out.println("not linked: " + filename + "   " + tmpFile.getAbsolutePath() );
                
                // moving this file into a "deprecated" folder
                
                try {
                    if (!targetFolder.exists()){
                        System.out.println("targetfolder does not exist.");
                    }
                    org.setupx.repository.core.util.File.move(targetFolder, tmpFile);
                    System.out.println("moved " + tmpFile.getName() + " to " + targetFolder.getAbsolutePath());
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                
                pageWriteBuffer.append("\n<br><a href='http://fiehnlab.ucdavis.edu:8080/xx1/" + tmpFile.getName() + "'>" + tmpFile.getName() );
            }
        }

        // write file
        try {
            org.setupx.repository.core.util.File.storeData2File(new File("/Users/scholz/Desktop/deadjsp.html"), pageWriteBuffer.toString());
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void findReferences(File folder) {
        // open every file and look for jsp 
        File[] files = folder.listFiles();
        
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            if (file.isDirectory() ) findReferences(file);
            else if (file.getName().endsWith("jsp") || file.getName().endsWith("java") ){
                receiveLinks(file);
            }
        }
        
    }


    private void receiveLinks(File file) {
        try {
            String content = org.setupx.repository.core.util.File.getFileContent(file);
            int offset = 0;
            boolean loop = true;
            
            while (loop){
                // check for jsp tag
                int pos = content.indexOf("jsp", offset);

                // find jsp n
                if (pos > 0) {
                    determineJSPName(file);
                    offset = pos + 1;
                } else {
                    loop = false;
                }
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }

    
    

    private void determineJSPName(File i_sourceFile) throws IOException {
        String content = org.setupx.repository.core.util.File.getFileContent(i_sourceFile);

        for (int i = 0; i != this.files.size(); i++){
            if (content.indexOf("" + ((File)this.files.get(i)).getName()) > 0){
                // System.out.println("found: " + filenames.get(i) + " in " + i_sourceFile.getName());
                this.links .add(new Link((File)this.files.get(i), i_sourceFile));
            }
        }
    }

    /*
    private void determineJSPName(String content, int pos) {
        int start = content.substring(0, pos).lastIndexOf("\"");
        int end = content.substring(pos).indexOf("\"");
        System.out.println(content.substring(start, start + end));
    }
    */


    /**
     * read all filenames recursive
     */
    private void readNames(File i_sourceFolder) {
        File[] files = i_sourceFolder.listFiles();
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            if (file.isDirectory()){
                readNames(file);
            } else if (file.getName().endsWith("jsp") ){
                this.files.add(file);
            }
        }
    }

    public static void main(String[] args) {
        new SourceCodeLinks(new File("/Users/scholz/Documents/2008/dev/workspace/setupx_gcode/lims/source/m4/"));
    }
}
