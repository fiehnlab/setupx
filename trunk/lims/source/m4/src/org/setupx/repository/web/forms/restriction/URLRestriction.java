/**
 * ============================================================================ File:    URLRestriction.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.core.util.ConnectException;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.ValidationAnswer;
import org.setupx.repository.web.forms.validation.ValidationException;

import java.net.URL;


/**
 * @hibernate.subclass
 */
public class URLRestriction extends Restriction {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new URLRestriction object.
   */
  public URLRestriction() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#cloneRestriction()
   */
  public Restriction cloneRestriction() {
    return new URLRestriction();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#validateObject()
   */
  public ValidationAnswer validateObject(InputField field)
    throws ValidationException {
    String string = (String) field.getValue();
    URL url = null;

    try {
      url = new URL(string);
    } catch (Exception e) {
      return new ValidationAnswer(field, "Please check the URL - it is not a valid URL.");
    }

    try {
      Util.checkURL(url);
    } catch (ConnectException e) {
      return new ValidationAnswer(field, "The URL seems to be incorrect or site is not available.");
    }

    return null;
  }
}
