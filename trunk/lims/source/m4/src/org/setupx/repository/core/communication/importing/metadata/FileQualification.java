/**
 * ============================================================================ File:    FileQualification.java Package: org.setupx.repository.core.communication.importing.metadata cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.importing.metadata;

import java.io.File;


class FileQualification extends MDCore {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private FileQualification() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param file TODO
   *
   * @return TODO
   */
  public static boolean check(File file) {
    log(FileQualification.class, "missing check");

    if (org.setupx.repository.core.util.File.getExtension(file).toLowerCase().compareTo("xls") == 0) {
      return true;
    }

    return false;
  }
}
