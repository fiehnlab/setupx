/**
 * ============================================================================ File:    Notification.java Package: org.setupx.repository.core.communication.notification cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.notification;

/**
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @deprecated replaced by message  
 */
public abstract class Notification {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String message = "";

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param message TODO
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getMessage() {
    return message;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public abstract String[] getRecipients();
}
