/**
 * ============================================================================ File:    NCBIVocRestriction.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.hibernate.Session;

import org.setupx.repository.core.communication.ncbi.NCBIConnector;
import org.setupx.repository.core.communication.ncbi.NCBIException;
import org.setupx.repository.core.communication.ncbi.local.NCBIEntry;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield;
import org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfieldException;
import org.setupx.repository.web.forms.validation.ValidationAnswer;
import org.setupx.repository.web.forms.validation.ValidationException;


/**
 * Restriction implementation for the NCBI Connector
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.17 $
 *
 * @hibernate.subclass
 */
public class NCBIVocRestriction extends Restriction {
    //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /** the super field of the StringInputfield which is beeing validated */

    //private NBCISpeciesInputfield spec = null;
    private NCBIConnector connector = new NCBIConnector();

    //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public static final int UNKNOWN_NCBI_ORGANISM_ID = 32644;
    public static final int ROOT = 1;

    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new NCBIVocRestriction object.
     */
    public NCBIVocRestriction() {
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* (non-Javadoc)
     * @see Restriction#cloneRestriction()
     */
    public Restriction cloneRestriction() {
        throw new UnsupportedClassVersionError();
    }

    /* (non-Javadoc)
     * @see org.setupx.repository.core.CoreObject#persistenceChilds(org.hibernate.Session, boolean)
     */
    public void persistenceChilds(Session session, boolean createIt)
    throws PersistenceActionException {
        /*
       if (spec() != null) {
         try {
           spec().update(session, createIt);
         } catch (Exception e) {
           // can be ignored, cause the spec field is already saved by the owning Field
           warning(this, "unable to update a " + Util.getClassName(spec()) + "  " + e.getLocalizedMessage());
           e.printStackTrace();
         }

       }*/
        super.persistenceChilds(session, createIt);
    }

    /**
     * get the NCBI id and add it to the related SpeciesInputfield.
     * 
     * <p>
     * The information hashtable is beeing resetted and new initalisied.
     * </p>
     * 
     * <p>
     * works only with <b><code>SpeciesInputfield</code></b>
     * </p>
     *
     * @throws ValidationException the related field to this restriction is not an StringInputfield
     *
     * @see SpeciesInputfield
     * @see org.setupx.repository.web.forms.restriction.Restriction#validateObject()
     */
    public ValidationAnswer validateObject(InputField field)
    throws ValidationException {
        //debug("start validation: " + field);
        //debug("start validation value: " + field.getValue());

        if (field.getValue() == null || field.getValue().toString().length() < 1) {
            warning(this,"going to ignore this value cause it is to short.");
            return null; 
        }

        NBCISpeciesInputfield speciesInputfield = null;

        if (!(field instanceof StringInputfield)) {
            throw new RuntimeException("the inputfiled is of the wrong type " + Util.getClassName(field));
        }

        try {
            speciesInputfield = (NBCISpeciesInputfield) field.getParent();
            //debug("found parent " + speciesInputfield);
        } catch (ClassCastException e) {
            err(this, e);
            throw new RuntimeException("the parent of the inputfiled is of the wrong type " + Util.getClassName(field.getParent()));
        } catch (NullPointerException e) {
            err(this, e);

            return null;
        }

        String entry = field.getValue() + "";
        //debug("validateObject(): \"" + entry + "\"");

        // if it is an ID - just get the real name
        int id = 9999999;

        try {
            id = Integer.parseInt(entry);
            //debug("the new value is an id: " + entry);
        } catch (Exception e) {
            //debug(entry + " doesn t seem to be an NCBI code");
            // uoops - seems to be something else then an ncbi id
        }

        /*
       if (!(speciesInputfield instanceof StringInputfield)) {
         ValidationException exception = new ValidationException("related Input field is not an StringInputfield");
         warning(exception);
         throw exception;
       }

         */
        if (connector == null) {
            connector = new NCBIConnector();
        }

        ValidationAnswer neagative_answer = new ValidationAnswer(speciesInputfield, "was not found in NCBI - check NCBI for real name"); //, new Link("http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?CMD=search&DB=taxonomy"));

        try {
            // find the id
            // in case that the id exist already do not look for it
            if (id == 9999999) {
                id = connector.determineNCBI_Id(entry);
            }

            debug("NCBI code delivered from " + Util.getClassName(connector) + " : " + id);

            // in case it is a ROOT - the id is root
            if (id == ROOT) {
                try {
                    // resetting the hashtable
                    (speciesInputfield).resetInformation();
                } catch (NBCISpeciesInputfieldException e1) {
                    e1.printStackTrace();
                }

                return new ValidationAnswer(speciesInputfield, "Element is root element in NCBI Taxonomy.");
            }

            // in case it is a 32644 - the id organism is unknown
            if (id == UNKNOWN_NCBI_ORGANISM_ID) {
                // resetting the hashtable
                try {
                    speciesInputfield.resetInformation();
                } catch (Exception e) {
                    // has to be caught, cause the persistence layer might add the spec later...
                    warning(this, "there was a vaiolation but the spec can not be resettet, cause " + e);
                    e.printStackTrace();
                }

                return neagative_answer;
            }

            // find all available information for this id
            debug("checking " + Util.getClassName(connector) + " for NCBI Entry-Object");

            NCBIEntry ent = connector.determineNCBI_Information(id);
            debug("found on " + Util.getClassName(connector) + " :" + ent.toString());

            debug("number of infos: " + ent.toHashtable().size());

            // add this information as noneditable to the related field
            // --  check if this() has been completly initalized
            try {
                debug("adding information hashtable to " + speciesInputfield);
                speciesInputfield.addInformation(id, ent);
                speciesInputfield.updateRelations(ent);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            // everything went well
            // check if the element is entry is 
            if (!ent.isSpecies()) {
                return new ValidationAnswer(speciesInputfield, "the typed word is not of the type of a species - it is a " + ent.getRank() + ". Please select a species from the list below.");
            }

            return null;
        } catch (NCBIException e) {
            e.printStackTrace();

            return neagative_answer;
        }
    }
}
