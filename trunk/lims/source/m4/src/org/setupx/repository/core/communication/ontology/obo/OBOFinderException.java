package org.setupx.repository.core.communication.ontology.obo;

public class OBOFinderException extends Exception {

    public OBOFinderException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public OBOFinderException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public OBOFinderException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public OBOFinderException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
