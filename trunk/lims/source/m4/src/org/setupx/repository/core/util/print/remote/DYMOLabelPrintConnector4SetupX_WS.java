package org.setupx.repository.core.util.print.remote;

import java.text.DateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.setupx.repository.core.util.print.DYMOLabelPrintConnector;
import org.setupx.repository.core.ws.M1_BindingStub;
import org.setupx.repository.core.ws.M1_ServiceLocator;
import org.setupx.repository.server.persistence.SXQuery;

/**
 * change sxqueryies to remote ws calls.
 * @author scholz
 *
 */

public class DYMOLabelPrintConnector4SetupX_WS extends DYMOLabelPrintConnector{
    private static final int lablesPerPage = 4;
    private long[] sampleIDs;
    boolean finished = false;
    private String[] sampleLables;

    
    public DYMOLabelPrintConnector4SetupX_WS(long id){
        long promtID = id  ;

        // read sampleIDs
        this.sampleIDs = toArray(readIDs(promtID));
        this.sampleLables = readLabels(this.sampleIDs);
    }
    
    private static long[] toArray(List list) {
        long[] ls = new long[list.size()];
        Iterator iterator = list.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            ls[i] = Long.parseLong("" + iterator.next());
            i++;
        }
        return ls;
    }

    private List readIDs(long promtID) {
        return new SXQuery().findSampleIDsByPromtID(promtID);
    }

    private static String[] readLabels(long[] sampleIDs) {
        String[] result = new String[sampleIDs.length];
        for (int i = 0; i < result.length; i++) {
            try {
                result[i] = new SXQuery().findSampleLabelBySampleID(sampleIDs[i]);
                System.out.println(i + "  " + result[i] + "  " + sampleIDs[i]);
            } catch (Exception e) {
                e.printStackTrace();
                result[i] = "-unknown-";
            }
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * @see org.setupx.repository.core.util.print.DYMOLabelPrintConnector#getValue(int, int)
     */
    public String getValue(final int elementOnLabel, final int labelCounter) {
        if (labelCounter == this.sampleIDs.length) finished = true;
        String value = "";
        
        switch (elementOnLabel) {
        case 0:
          // what ever you want to have in this line
          value = "SetupX";

          break;

        case 1:
            // what ever you want to have in this line
          value = "fiehnlab.ucd";

          break;

        case 2:
            // what ever you want to have in this line
          //value = "id: " + labelCounter;
            try {
            value = "" + this.sampleIDs[labelCounter];
            } catch (Exception e) {
                value = "---";
            }

          break;

        case 3:
            // what ever you want to have in this line
          // TODO - add DB connection
            try{
                value = this.sampleLables[labelCounter];
            } catch (Exception e) {
                value = "---";
            }

          break;

        case 4:
            // what ever you want to have in this line
          value = DateFormat.getDateInstance(DateFormat.SHORT, Locale.US).format(new Date());

          break;

        default:
          break;
        }

        return value;
    }


/*
 * (non-Javadoc)
 * @see org.setupx.repository.core.util.print.DYMOLabelPrintConnector#getPageNumbers()
 */  
    public int getPageNumbers() {
      int number = this.sampleIDs.length / lablesPerPage;
      if (this.sampleIDs.length % 4 > 0) number++;
      return number;
  }
    
    
    private M1_BindingStub createBinding() throws java.rmi.RemoteException{
        M1_BindingStub binding;
        try {
            binding = (M1_BindingStub) new M1_ServiceLocator().getm1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        binding.createMessage("Printing Labels", "requested binding to webservice in order to print labels.");
        return binding;
    }
}
