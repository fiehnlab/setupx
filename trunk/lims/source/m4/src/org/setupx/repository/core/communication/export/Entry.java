/**
 * ============================================================================ File:    Entry.java Package: org.setupx.repository.core.communication.export cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.export;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;

/**
 * Entry inside a an Export Part
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class Entry extends CoreObject{
    //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public String key;
    public String value;

    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new Entry object.
     * <b>All tags </b>(anything starting with "<" and ending with ">") <b>are cut out of the value.</b>
     *
     * @param key key
     * @param value value
     */
    public Entry(String key, String value) {
        Logger.log(this, "creating new instance: [" + key + ":" + value+ "]");
        this.key = key;

        //replace all html tags in there
        this.value = Util.removeHTMLtags(value);
    }



    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return this.key + ":" + this.value;
    }

    /** 
     * convert this to an XML node.
     * @param doc used to create the node incl the included CDATA section
     * @return
     */
    public Node createXMLNode(Document doc) {
        debug("creating XML representaion. " + this.toString());
        Element node = doc.createElement("atom");
        node.setAttribute("key", this.key);
        Node valueElement = doc.createCDATASection(this.value);
        node.appendChild(valueElement);
        return node;
    }
}
