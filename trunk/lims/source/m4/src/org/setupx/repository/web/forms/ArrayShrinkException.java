package org.setupx.repository.web.forms;

/**
 * @author ibm user
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ArrayShrinkException extends Exception {

    /**
     * 
     */
    public ArrayShrinkException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public ArrayShrinkException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public ArrayShrinkException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     */
    public ArrayShrinkException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
