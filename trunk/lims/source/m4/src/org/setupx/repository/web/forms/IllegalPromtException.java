package org.setupx.repository.web.forms;

import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.relation.AbstractRelation;
import org.setupx.repository.web.forms.relation.Relation;

public class IllegalPromtException extends Exception {

    public IllegalPromtException(String string) {
        super(string);
    }

    public IllegalPromtException(String string, Promt promt) {
        super(string);
        AbstractRelation[] relations = promt.findNBCISpeciesInputfield().getRelations();
        for (int i = 0; i < relations.length; i++) {
            AbstractRelation relation = relations[i];
            Logger.debug(this,"---target for relation from NBCISpeciesInputfield -- " + relation.getConditions().toString() );
            FormObject[] formObjects = ((Relation) relation).getRelationTargets();
            for (int j = 0; j < formObjects.length; j++) {
                FormObject object = formObjects[j];
                Logger.debug(this,"----- " + object.getUOID() + "  " + object.isActive() + "  " + object.getDescription() + " ");
            }
        }
    }
}
