package org.setupx.repository.core.query.answer;

public class FormobjectAnswerCreationException extends Exception {

    public FormobjectAnswerCreationException(String string) {
        super(string);
    }

}
