package org.setupx.repository.web.pub.data;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.Session;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.SXQuery;

public class PublicDataHandler {
    
    public static void main(String[] args) throws Exception{
        test();
    }

    public static void test() throws Exception{
        long userID = 4;

        long experimentID = 338050;
        
        HashSet pubSamples = createDummySamples(experimentID);

        PubDataSet dataSet = new PubDataSet("my TestPublication", experimentID , userID, new Date());
        dataSet.setPubSamples(pubSamples);
        
        dataSet.update(true);
        
        long id = dataSet.getUOID();
        
        Session s = CoreObject.createSession();
        PubDataSet loaded = (PubDataSet)PubDataSet.persistence_loadByID(PubDataSet.class , s, id);
        
        Logger.log(null, dataSet.toString());
        Logger.log(null, loaded.toString());
        
        
    }

    /**
     * create a dummy set of samples
     * @param experimentID 
     * @return
     */
    private static HashSet createDummySamples(long experimentID) {
        HashSet set = new HashSet();
        
        Iterator iterator = new SXQuery().findSampleIDsByPromtID(experimentID).iterator();
        while (iterator.hasNext()) {
             long sampleID = Long.parseLong("" + iterator.next());
             
             Set attributes = createDummyAttributes();
             PubSample pubSample = new PubSample(sampleID, "unknown", attributes);
             set.add(pubSample);
        }
        return set;
    }

    private static Set createDummyAttributes() {
        HashSet set = new HashSet();
        for (int i = 0; i < 55; i++) {
            set.add(new PubAttribute("key" + Math.round(Math.random() * i) , Math.round(Math.random() * 1000)));
        }
        return set;
    }
    
}
