package org.setupx.repository.core.query;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.setupx.repository.core.CoreObject;


/**
 * Container for Queries that have been submitted in the same way before - in order not to query the same stuff again and by this save a lot of time.
 */
public class QueryMasterCache extends CoreObject{
        
    private static QueryMasterCache thiZ = new QueryMasterCache();
    
    /** {@link Vector} containing all precached Results
     * <li>vector contains hashtables.
     * <li>hashtables are references by the queryType and thereby the position in the vector 
     *         (results for the type Querytype {@link QueryMaster#FACTOR} are found at the matching position in the vector)
     * <li>Hashtables themself contain a {@link List} of {@link QueryMasterAnswer}s. <b>key</b> for the Hashtable is the searchterm.
     */
    public static  Hashtable[] typeResults = new Hashtable[100];

    /**
     * check if a certain term and queryType combination is stored in the cache
     * @param term
     * @param queryType
     * @return true if exists
     */
    public static boolean containsTerm(String term, int queryType) {
        try {
            Object object =  typeResults[queryType].get(term);
            //debug(thiZ, object);
            return (object != null);
        } catch (NullPointerException e) {
            return false;
        }
    }

    /**
     * Get the precached results for a certain term and querytype combination. 
     * @param term the searchterm
     * @param queryType the type of query
     * @return a {@link List} of {@link QueryMasterAnswer}s.
     */
    public static List get(String term, int queryType) { 
       return (List)typeResults[queryType].get(term);
    }

    /** 
     * adding values to the cache
     * @param searchTerm
     * @param querytype
     * @param result
     */
    public static void add(String searchTerm, int querytype, Collection results) {
        debug(thiZ, "caching list of " + results.size() + " results for searchterm \"" + searchTerm + "\" and querytype \"" + querytype + "\"");
        Hashtable queryTypeResults = null;
        try {
            // checking if the {@link #typeResults} vector contains a {@link Hashtable} for searchTerm.
            //queryTypeResults = (Hashtable)typeResults.get(querytype);
            queryTypeResults = typeResults[querytype];
            debug(thiZ, "current number of cached results for querytype " + querytype + " is: " + queryTypeResults.size() );
        } catch (Exception e) { 
            warning(thiZ, "not able to find an existing hashtable for querytype: " + querytype);
            queryTypeResults =  new Hashtable();
        }  
        
        debug(thiZ, "adding results to hashtable");
        queryTypeResults.put(searchTerm, results);
        // setting/resetting with new values
        typeResults[querytype] = queryTypeResults;
    }

    public static String showContent() {
        StringBuffer buffer = new StringBuffer();
        
        for (int i = 0; i < typeResults.length; i++) {
            Hashtable searchResults = typeResults[i];
            

            try {
                if (searchResults == null || searchResults.size() == 0) throw new Exception("empty");
                
                buffer.append("\n-------------------------------------------------\n");
                buffer.append("querytype: " + i + " \n");

                Enumeration keys = searchResults.keys();
                while (keys.hasMoreElements()) {
                    String key = (String) keys.nextElement();
                    buffer.append("     \"" + key + "\": " + ((List)searchResults.get(key)).size() + " results. \n");                
                }
            } catch (Exception e) {
            }
        }
        debug(thiZ, buffer);
        return buffer.toString();
    }
}
