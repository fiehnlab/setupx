package org.setupx.repository.core.communication;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class CommunicationException extends Exception {
  /**
   * Creates a new CommunicationException object.
   *
   * @param e 
   */
    public CommunicationException(Throwable e) {
        super(e);
      }
    public CommunicationException(String msg , Throwable e) {
        super(msg ,e);
      }
    /**
     * @param string
     */
    public CommunicationException(String string) {
        super(string);
    }
}
