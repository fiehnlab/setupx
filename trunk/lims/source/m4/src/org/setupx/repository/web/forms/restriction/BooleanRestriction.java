/**
 * ============================================================================ File:    BooleanRestriction.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.ValidationAnswer;
import org.setupx.repository.web.forms.validation.ValidationException;


/**
 * restriction for boolean objects. doesnt do anything - because booleanobjects are always valid
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.17 $
 *
 * @hibernate.subclass
 */
public class BooleanRestriction extends Restriction {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new BooleanRestriction object.
   */
  public BooleanRestriction() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#cloneRestriction()
   */
  public Restriction cloneRestriction() {
    return new BooleanRestriction();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#validateObject(org.setupx.repository.web.forms.inputfield.InputField)
   */
  public ValidationAnswer validateObject(InputField field2)
    throws ValidationException {
    return null;
  }
}
