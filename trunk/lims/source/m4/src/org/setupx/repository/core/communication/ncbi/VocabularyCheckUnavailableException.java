
package org.setupx.repository.core.communication.ncbi;
public class VocabularyCheckUnavailableException extends Exception {

	public VocabularyCheckUnavailableException(String string) {
		super(string);	}

}
