package org.setupx.repository.core.communication.leco;

public class AcquisitionFileQueryException extends Exception {

    public AcquisitionFileQueryException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public AcquisitionFileQueryException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public AcquisitionFileQueryException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public AcquisitionFileQueryException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
