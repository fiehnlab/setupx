/**
 * ============================================================================ File:    JSPComponent.java Package: org.setupx.repository.util.code.creator cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.util.code.creator;

class JSPComponent {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Class returntype;
  private String name;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new JSPComponent object.
   *
   * @param name 
   * @param returntype 
   */
  public JSPComponent(String name, Class returntype) {
    this.name = name;
    this.returntype = returntype;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public static char createFooter() {
    // TODO Auto-generated method stub
    return 0;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public static char createHeader() {
    // TODO Auto-generated method stub
    return 0;
  }

  /**
   * creates one line in the jsp containing the label and the inputfield
   *
   * @return
   */
  public String toJSP() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("<tr><td>");
    buffer.append(this.createLabel());
    buffer.append("</td><td>");
    buffer.append(this.createField());
    buffer.append("</td></tr>");

    return buffer.toString();
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  private String createField() {
    if (this.returntype == Boolean.class) {
    } else {
    }

    return "";
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  private String createLabel() {
    return this.name.substring(3);
  }
}
