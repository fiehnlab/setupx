/**
 * ============================================================================ File:    MachinePool.java Package: org.setupx.repository.core.communication.exporting cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.exporting;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.communication.ldap.AcquisitionFileException;
import org.setupx.repository.core.communication.leco.AcquisitionFileQueryException;
import org.setupx.repository.core.communication.leco.GCTOF;
import org.setupx.repository.core.communication.leco.LecoACQFile;
import org.setupx.repository.core.communication.leco.Method;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;

import org.hibernate.Session;

import java.io.File;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;


/**
 * Pool of all the machines available.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class MachinePool extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static MachinePool thiz = new MachinePool();

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws PersistenceActionFindException TODO
   */
  public static List getAvailableMachines() throws PersistenceActionFindException {
    //throw new UnsupportedOperationException();
    Session s = PersistenceConfiguration.createSessionFactory().openSession();

    return Machine.persistence_loadAll(Machine.class, s);
  }

  /**
   * TODO: 
   *
   * @param machineID TODO
   *
   * @return TODO
   *
   * @throws MachineUnknownException TODO
   * @throws UnsupportedOperationException TODO
   */
  public static Machine getMachine(int machineID) throws MachineUnknownException {
    Session s = PersistenceConfiguration.createSessionFactory().openSession();
    Machine machine;

    try {
      machine = (Machine) persistence_loadByID(Machine.class, s, machineID);
    } catch (PersistenceActionFindException e) {
      throw new MachineUnknownException("machineID " + machineID + " is unknown. " + e.toString());
    }

    s.close();

    return machine;
  }

  /**
   * TODO: 
   *
   * @param machineLetter TODO
   *
   * @return TODO
   *
   * @throws MachineUnknownException TODO
   * @throws UnsupportedOperationException TODO
   */
  public static Machine getMachine(char machineLetter)
    throws MachineUnknownException {
    throw new UnsupportedOperationException();
  }

  /**
   * determine the AcquisitionParameters that were used in former Experiments in any of the existing  seq files.
   *
   * @return array of these parameters
   *
   * @throws AcquisitionFileException
   * @throws AcquisitionFileQueryException
   * @throws ParameterException
   *
   * @see AcquisitionParameter#DP_METHOD
   * @see AcquisitionParameter#GC_METHOD
   */
  public static AcquisitionParameter[] determineAcquisitionParameterUsed(String acquisitionParameterName)
    throws AcquisitionFileQueryException, AcquisitionFileException, ParameterException {
    File[] files = LecoACQFile.getSeqFiles();
    HashSet results = new HashSet();

    for (int i = 0; i < files.length; i++) {
      File file = files[i];
      results.addAll(new LecoACQFile(file).getValuesForParameterType(acquisitionParameterName));
    }

    return AcquisitionParameter.map(acquisitionParameterName, results);
  }

  /**
   * scannes for new Mehtods defined in the seq files. When found the new methods are being added to the database but <b>not</b> assigned to a machine. This is done later by the technican.<br>
   *
   * @see Method
   */
  public static void scan4NewMachineMethods() {
    // scan for all Methods
    // --- constants that are looked for
    String[] strings = new String[] { AcquisitionParameter.AS_METHOD, AcquisitionParameter.DP_METHOD, AcquisitionParameter.GC_METHOD, AcquisitionParameter.MS_METHOD, AcquisitionParameter.QC_METHOD };

    HashSet methods = new HashSet();

    for (int i = 0; i < strings.length; i++) {
      AcquisitionParameter[] acquisitionParameters;

      try {
        acquisitionParameters = MachinePool.determineAcquisitionParameterUsed(strings[i]);

        for (int j = 0; j < acquisitionParameters.length; j++) {
          Method method = new Method(acquisitionParameters[j]);
          methods.add(method);
        }
      } catch (Exception e) {
        e.printStackTrace();
        warning(thiz, e.toString());
      }
    }

    // Filter - to check for duclicates
    Filter filter = new Filter() {
        // checking if the method has been defined already
        public boolean accept(Method method) {
          //search for identical methodvalue 
          org.hibernate.Session s = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

          // expression missing to remove methods without an machineID
          try {
            String basic = "select {method.*} from machinemethod method where method.label = \"" + method.getLabel() + "\" ";
            int lenght = s.createSQLQuery(basic).addEntity("method", Method.class).list().size();
            s.close();
            debug(this, basic + "   :  " + lenght);

            return (lenght == 0);
          } catch (Exception e) {
            e.printStackTrace();
            s.close();

            return false;
          }
        }
      };

    // filtered ones
    HashSet filteredMethods = new HashSet();

    // check for duclicates
    Iterator iterator = methods.iterator();

    while (iterator.hasNext()) {
      Method method = (Method) iterator.next();

      if (filter.accept(method)) {
        try {
          method.update(true);
        } catch (PersistenceActionUpdateException e) {
          warning(thiz, e.toString());
        }
      }
    }
  }

  /**
   * TODO: 
   *
   * @throws PersistenceActionException TODO
   * @throws AcquisitionFileQueryException TODO
   * @throws AcquisitionFileException TODO
   * @throws ParameterException TODO
   */
  public static void test() throws PersistenceActionException, AcquisitionFileQueryException, AcquisitionFileException, ParameterException {
    // create machines 
    GCTOF machine = new GCTOF("Martins Test GCTOF", 'z', "only for testpuposes!");

    // save them
    //machine.setMethods(methods);
    machine.update(true);

    List list = MachinePool.getAvailableMachines();

    Iterator iterator = list.iterator();

    while (iterator.hasNext()) {
      Machine element = (Machine) iterator.next();
      debug(null, machine.toString());
    }
  }
}
