package org.setupx.repository.server.db;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class ConnectionPoolException extends Exception {
  /**
   * Creates a new ConnectionPoolException object.
   *
   * @param msg 
   */
  public ConnectionPoolException(String msg) {
    super(msg);
  }

  /**
   * Creates a new ConnectionPoolException object.
   *
   * @param ex 
   * @param msg 
   */
  public ConnectionPoolException(Exception ex, String msg) {
    super(msg, ex);
  }
}
