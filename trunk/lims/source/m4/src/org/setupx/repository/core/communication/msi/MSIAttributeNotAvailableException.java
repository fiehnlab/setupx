package org.setupx.repository.core.communication.msi;

import org.setupx.repository.server.persistence.PersistenceActionFindException;

public class MSIAttributeNotAvailableException extends Exception{

    public MSIAttributeNotAvailableException(String string) {
        super(string);
    }

    public MSIAttributeNotAvailableException(PersistenceActionFindException e) {
        super(e);
    }
}
