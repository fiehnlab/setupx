/**
 * ============================================================================ File:    XPath.java Package: org.setupx.repository.core.util.xml cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.xml;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.logging.Logger;

import org.apache.xpath.XPathAPI;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;


/**
 * utility to query a document
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.7 $
 */
public class XPath extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static DocumentBuilderFactory builderFactory;
  private static DocumentBuilder builder;
  private static XPath thiZ = new XPath();
  public static final String SEPERATOR = "/";

  static {
    DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

    try {
      builder = builderFactory.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      throw new RuntimeException("probs creating a new document builder");
    }
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private XPath() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param document TODO
   * @param node TODO
   * @param key TODO
   * @param value TODO
   */
  public static void addAttribute(Document document, Node node, String key, String value) {
    // Logger.debug(thiZ, "adding value (" + value + ") at Element: #" + node + "#  using key: " + key);
    Element element = (Element) node;
    element.setAttribute(key, value);
  }

  /**
   * TODO: 
   *
   * @param document TODO
   * @param node TODO
   * @param key TODO
   * @param value TODO
   *
   * @return TODO
   */
  public static Element addElement(Document document, Node node, String key, String value) {
    debug(thiZ, "adding value (" + value + ") at " + node.getLocalName() + " using key: " + key);

    Element newElement = document.createElement(key);
    Text text = document.createTextNode(value);

    //newElement.setNodeValue(value);
    newElement.appendChild(text);
    node.appendChild(newElement);

    return newElement;
  }

  /**
   * use it just for a nodelist, for single nodes use pathNode();
   *
   * @param document TODO
   * @param xpath_expression TODO
   *
   * @return TODO
   *
   * @throws XPathException TODO
   */
  public static NodeList path(Document document, String xpath_expression)
    throws XPathException {
    NodeList nodeList = null;

    try {
      //Logger.log(thiZ, "xpath " + xpath_expression);
      nodeList = XPathAPI.selectNodeList(document.getDocumentElement(), xpath_expression);

      //Logger.log(thiZ, "found for " + xpath_expression + " " + nodeList.getLength() + " elements.");
    } catch (TransformerException e) {
      throw new XPathException(e);
    } catch (NullPointerException e) {
      throw new XPathException(e);
    }

    return nodeList;
  }

  /**
   * TODO: 
   *
   * @param file TODO
   * @param xpath_expression TODO
   *
   * @return TODO
   *
   * @throws XPathException TODO
   */
  public static NodeList path(File file, String xpath_expression)
    throws XPathException {
    Document doc;

    try {
      // parsing the document
      doc = builder.parse(file);
    } catch (Exception e) {
      throw new XPathException(e);
    }

    return path(doc, xpath_expression);
  }

  /**
   * TODO: 
   *
   * @param file TODO
   * @param xpath_expression TODO
   *
   * @return TODO
   *
   * @throws XPathException TODO
   */
  public static NodeList path(XMLFile file, String xpath_expression)
    throws XPathException {
    return path(file.getDocument(), xpath_expression);
  }

  /**
   * TODO: 
   *
   * @param document TODO
   * @param xpath_expression TODO
   *
   * @return TODO
   *
   * @throws XPathException TODO
   */
  public static Node pathNode(Document document, String xpath_expression)
    throws XPathException {
    NodeList nodeList = null;

    try {
      //Logger.log(thiZ, "xpath " + xpath_expression);
      nodeList = XPathAPI.selectNodeList(document.getDocumentElement(), xpath_expression);

      //Logger.log(thiZ, "found for " + xpath_expression + " " + nodeList.getLength() + " elements.");
    } catch (TransformerException e) {
      throw new XPathException(e);
    } catch (NullPointerException e) {
      throw new XPathException(e);
    }

    try {
      return nodeList.item(0);
    } catch (NullPointerException e) {
      throw new XPathException("unable to find a node at " + xpath_expression);
    }
  }

  /**
   * example: <b><code>/workflow/id</code></b>
   *
   * @param document
   * @param string
   *
   * @return
   *
   * @throws XPathException
   * @throws DOMException
   */
  public static String pathValue(Document document, String string)
    throws DOMException, XPathException {
    return path(document, string).item(0).getFirstChild().getNodeValue();
  }

  /**
   * TODO: 
   *
   * @param document TODO
   * @param xpath_expression TODO
   * @param value TODO
   *
   * @throws XPathException TODO
   */
  public static void update(Document document, String xpath_expression, String value)
    throws XPathException {
    /*
       String oldValue = "";
       // only for debugging
       if (XClient.DEBUG_MODE) {
         try {
           oldValue = XPathAPI.selectNodeList(document.getDocumentElement(), xpath_expression).item(0)
                              .getNodeValue();
         } catch (NullPointerException e1) {
           throw new XPathException("no object found.");
         } catch (Exception e1) {
           Logger.log(thiZ, e1);
         }
       }
     */
    try {
      //Logger.log(thiZ, "changing value (" + xpath_expression + ") to " + value);
      XPathAPI.selectNodeList(document.getDocumentElement(), xpath_expression).item(0).setNodeValue(value);
    } catch (TransformerException e) {
      throw new XPathException(e);
    } catch (NullPointerException e) {
      Logger.err(thiZ, "there is no item at this point  " + xpath_expression);
    }

    // only for debugging

    /*if (XClient.DEBUG_MODE) {
       try {
         // comparing the old and new value
         // Logger.debug(null, "must be value - " + value);
         String newValue = XPathAPI.selectNodeList(document.getDocumentElement(), xpath_expression)
                                   .item(0).getNodeValue();
         Logger.debug(null, "     is value - " + newValue);
         if (XClient.DEBUG_MODE) {
           if (value.compareTo(newValue) != 0) {
             throw new Exception("changeing the values did not have any effects ...!");
           } else {
             Logger.debug(null, "changed successfully to " + newValue);
           }
           /*if (oldValue.compareTo(newValue) == 0) {
              throw new Exception("oldvalue and the new one are the same: " +  newValue + "  " + oldValue);
              }*/
    /*
       }
       } catch (Exception e1) {
         Logger.log(thiZ, e1);
       }
       }*/
  }
}
