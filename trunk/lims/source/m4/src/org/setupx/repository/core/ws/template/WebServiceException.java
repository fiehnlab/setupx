package org.setupx.repository.core.ws.template;

/**
 * TEMPLATE FOR GENERATION the webservice - look at the real implementaions
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class WebServiceException extends Exception {
  /**
   * @param arg0
   * @param arg1
   */
  public WebServiceException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }

  /**
   * Creates a new ServiceException object.
   *
   * @param msg 
   */
  public WebServiceException(String msg) {
    super(msg);
  }

  /**
   * Creates a new ServiceException object.
   *
   * @param arg1 
   */
  public WebServiceException(Throwable arg1) {
    super(arg1);
  }
}
