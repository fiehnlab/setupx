package org.setupx.repository.core.communication.export.reporting;

public class ReportInitException extends ReportException {

    public ReportInitException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public ReportInitException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ReportInitException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public ReportInitException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
