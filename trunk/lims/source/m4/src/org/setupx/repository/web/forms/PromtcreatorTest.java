package org.setupx.repository.web.forms;

import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import junit.framework.TestCase;

public class PromtcreatorTest extends TestCase {

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCreate() throws MappingError, PromtCreateException {
        // create a Promt
        Promt promt = PromtCreator.create(PromtCreator.DEFAULTVERSION);

        assertTrue("the initial ID of the promt is 0", promt.getUOID() != 0);
    }


    public void testLoad() throws Exception{
        // create a Promt
        Promt promt = PromtCreator.create(PromtCreator.DEFAULTVERSION);

        assertTrue("the initial ID of the promt is 0", promt.getUOID() != 0);
        
        long id = promt.getUOID();
        
        promt = null;
        
        promt = Promt.load(id);
        
        assertTrue("promt with wrong ID has been loaded." , promt.getUOID() == id);
    }
}
