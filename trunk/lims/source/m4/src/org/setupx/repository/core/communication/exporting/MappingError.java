/**
 * ============================================================================ File:    MappingError.java Package: org.setupx.repository.core.communication.exporting cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.exporting;

import java.util.HashSet;
import java.util.Iterator;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class MappingError extends ExportException {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public HashSet exceptions = new HashSet();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   *
   */
  public MappingError() {
    super();

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public MappingError(String arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public MappingError(Throwable arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   */
  public MappingError(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }

  /**
   * Creates a new MappingError object.
   *
   * @param string 
   * @param exceptions 
   */
  public MappingError(String string, HashSet exceptions) {
    this.exceptions = exceptions;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   */
  public void show() {
    this.printStackTrace();

    System.err.print("related exception:");

    Iterator iter = this.exceptions.iterator();

    while (iter.hasNext()) {
      Exception ex = (Exception) iter.next();
      System.err.print(ex.getMessage());
    }
  }
}
