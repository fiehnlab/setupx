/**
 * ============================================================================ File:    SamplePage.java Package: org.setupx.repository.web.forms.page cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.page;

/**
 * Page dedicated to contain only information about the samples themself.
 * 
 * <p>
 * Including different elements like the classes that contain the samples as well as the classtable.
 * </p>
 * 
 * <p>
 * The strcuture is the following:
 * </p>
 * 
 * <p>
 * <code> page <br> .|---ClassTable<br> . |---MultiFieldClazzes<br> .  |-Class<br> .  | |-Sample<br> .  | |-Sample<br> .  |-Class<br> .  .  |-Sample<br> .  .  |-Sample<br></code>
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @hibernate.subclass
 *
 * @see org.setupx.repository.web.forms.clazzes.ClassTable
 * @see org.setupx.repository.web.forms.inputfield.multi.MultiFieldClazzes
 * @see org.setupx.repository.web.forms.inputfield.multi.Clazz
 * @see org.setupx.repository.web.forms.inputfield.multi.Sample
 */
public class SamplePage extends Page {
    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @param name
     * @param description
     * @param longDescription
     */
    public SamplePage(String name, String description, String longDescription) {
        super(name, description, longDescription);
    }

    /**
     * Creates a new SamplePage object.
     */
    public SamplePage() {
        this("", "", "");
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* (non-Javadoc)
     * @see org.setupx.repository.web.forms.FormObject#validate()
     */
    public String validate() {
        return null;
    }
}
