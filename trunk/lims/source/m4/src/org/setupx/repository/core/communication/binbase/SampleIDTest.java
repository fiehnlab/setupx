package org.setupx.repository.core.communication.binbase;

import org.setupx.repository.core.util.logging.Logger;
import junit.framework.TestCase;
public class SampleIDTest extends TestCase {

    private SampleID sampleid;

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
        this.sampleid = new SampleID("123");
    }

    public void testSampleID() {
        assertTrue(this.sampleid.getID() != null);
    }

    public void testGetID() {
        assertTrue(this.sampleid.getID().compareTo("123") == 0);
    }

    public void testNuller() {
        this.sampleid = new SampleID(null);
        Logger.log(this, this.sampleid.getID());
        assertTrue(this.sampleid.getID().compareTo("undefined") == 0);
    }

    public void testAsStringArray() {
        SampleID[] sampleIDs = new SampleID[100];
        for (int i = 0; i < sampleIDs.length; i++) {
            sampleIDs[i] = new SampleID("" + i);
        }

        for (int i = 0; i < sampleIDs.length; i++) {
            SampleID sampleID = sampleIDs[i];
            assertTrue(sampleID.getID().compareTo("" + i) == 0);
            Logger.log(this, "" + sampleIDs[i]);
        }
    }

    public void testToSampleIDArray() {
        SampleID[] sampleIDs = new SampleID[100];
        for (int i = 0; i < sampleIDs.length; i++) {
            sampleIDs[i] = new SampleID("" + i);
        }

        for (int i = 0; i < sampleIDs.length; i++) {
            SampleID sampleID = sampleIDs[i];
            assertTrue(sampleID.getID().compareTo("" + i) == 0);
            Logger.log(this, "" + sampleIDs[i]);
        }
    }

}
