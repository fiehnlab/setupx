/**
 * ============================================================================ File:    ExportSample.java Package: org.setupx.repository.core.communication.export cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.export;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.setupx.repository.core.communication.leco.LecoACQFile;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.multi.Sample;


class ExportSample {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** the sample that will exported */
  private Sample sample;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected ExportSample(Sample sample) {
    this.sample = sample;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @return
   */
  public int getIDposition() {
    return 0;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  protected Vector toLine() {
    Vector results = new Vector();

    // if id moved - change id positon in method getIDposion below.
    results.add(new Entry("Sample ID", "" + this.sample.getUOID()));

    /*
    try {
      results.add(new Entry("Comment", "" + this.sample.getComment()));
    } catch (Exception e1) {
      results.add(new Entry("Comment", "-unknown-"));
    }
    */

    try {
      results.add(new Entry("Class ID", "" + new SXQuery().findParent(this.sample.uoid)));
    } catch (PersistenceActionFindException e1) {
      results.add(new Entry("Class ID", "-unknown-"));
    }

    try {
      results.add(new Entry("Experiment ID", "" + new SXQuery().findPromtIDbySample(this.sample.uoid)));
    } catch (PersistenceActionFindException e) {
      results.add(new Entry("Experiment ID", "-unknown-"));
    }

    // file label
    try {
        results.add(new Entry("filename", "" + new SXQuery().findAcquisitionNameBySampleID(this.sample.uoid)));
    } catch (Exception e) {
        e.printStackTrace();
    }
    
    
    /*
    // check if class & multifields are available
    try {
      Clazz clazz = (Clazz) sample.getParent();

      MultiField4Clazz[] field4Clazzs = clazz.getMultiField4Clazzs();

      Arrays.sort(field4Clazzs, FormObjectComparator.byID());

      for (int j = 0; j < field4Clazzs.length; j++) {
        MultiField4Clazz multiField4Clazz = field4Clazzs[j];

        if (multiField4Clazz.isActive()) {
          //results.add(new Entry("factor ID",		"" + multiField4Clazz.getUOID()));
          results.add(new Entry("factor label", "" + multiField4Clazz.getQuestion()));
          //results.add(new Entry("factor value",	"" + multiField4Clazz.getValue()));
          results.add(new Entry("factor short label", multiField4Clazz.createShortLabel()));
        }
      }
    } catch (Exception e) {
    }
    */

    
    String label_value = "";
    String label_key = "";
    
    for (int mySampleCounter = 0; mySampleCounter < sample.getFields().length ; mySampleCounter++){
        if (sample.getField(mySampleCounter) instanceof StringInputfield){
            try {
                label_key = "" +  ((StringInputfield)sample.getField(mySampleCounter)).getQuestion();
                label_value = "" + ((StringInputfield)sample.getField(mySampleCounter)).getValue();
                if (label_value.length() > 0){
                    results.add(new Entry(label_key, label_value));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    try {
        long classID = new SXQuery().findClazzIDBySampleID(Integer.parseInt("" + sample.getUOID()));
        Iterator labels = new SXQuery().findLabelsforClazz(classID).iterator();
        while (labels.hasNext()) {
            Object[] element = (Object[]) labels.next();
            results.add(new Entry(("classInfo:" + element[0].toString()),("" + element[1].toString()) ));
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    
    

    // add run details
    try {
        List allRuns = new SXQuery().findScannedPair(this.sample.uoid);
        if (allRuns.size() == 0){
            Logger.log(this, "no runs detected.");
        } else {
            String acqname = "" + allRuns.get(0);
            acqname = acqname.substring(0, acqname.indexOf(":"));
            //LecoACQFile lecofile = LecoACQFile.determineFile(acqname);
            Hashtable ht = LecoACQFile.getValuesPerACQname(acqname);
            Enumeration enumeration = ht.keys();
            while(enumeration.hasMoreElements()) { 
                String key = (String)enumeration.nextElement();
                String value = (String)ht.get(key);     
                value = value.replace('\\',' ');
                if (key.length() > 0 && value.length()>0){
                    results.add(new Entry(("runInfo:" + key),("" + value) ));
                } 
            }
        }
    }catch (Exception e){
        e.printStackTrace();
    }
    
    // add status infos
    // maybe later...
    
    return results;
    
    

    // todo - more infos about the class
    // find information about the class
  }
}
