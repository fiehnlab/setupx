/**
 * ============================================================================ File:    MailContentBox.java Package: org.setupx.repository.core.communication.mail cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.mail;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.server.logging.Message;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.mail.MessagingException;


/**
 * contains all non send mailcontent objects. The Box is designed to avoid that lots of mails are send to one user in short time  so they are collected and send after a while  FIXME add persistence to this object - if the system is shut down - all non send mails will be lost.
 */
class MailContentBox extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static Object thiz = new MailContentBox();
  private static HashSet contentObjects = new HashSet();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private MailContentBox() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @return
   */
  protected final static boolean isEmpty() {
    return contentObjects.isEmpty();
  }

  /**
   * @param content
   */
  protected synchronized final static void add(MailContent content) {
    try {
      new Message(content.content, content.content).update(true);
      debug(thiz, "creating a new message to " + content.user.getMailAddress().toString());
    } catch (Exception e) {
    }

    contentObjects.add(content);
  }

  /**
   * create an array of MailDelivery-Objects. These are generated from the single MailContentobjects, which are combined for each user.
   *
   * @return
   *
   * @throws MessagingException
   *
   * @see MailContent
   */
  protected synchronized final static void createAndSend()
    throws MessagingException {
    // hashtable - key: username - object: vector containing all MailContentObjects
    Hashtable usersVectors = new Hashtable();

    // loop over all mailcontents
    for (Iterator iterator = contentObjects.iterator(); iterator.hasNext();) {
      MailContent element = (MailContent) iterator.next();

      // NEW: creating messages in the system
      Message message = new Message();
      message.setLabel(element.content);
      message.setMessage(element.content);

      try {
        message.update(true);
      } catch (PersistenceActionUpdateException e) {
        e.printStackTrace();
      }

      //Logger.debug(null, "Got a msg for " + element.user.username);
      // if user is known add the mailcontentobject to his vector 
      if (usersVectors.containsKey(element.user.username)) {
        Vector v = (Vector) usersVectors.get(element.user.username);
        v.add(element);
      }
      // otherwise add a new vector with his name and the mailcontentobject inside 
      else {
        Vector v = new Vector();
        v.add(element);
        usersVectors.put(element.user.username, v);
      }
    }

    // take each user form the hashtable and create the mail for him.
    for (Enumeration enumeration = usersVectors.elements(); enumeration.hasMoreElements();) {
      Vector vectorOfMailContents = (Vector) enumeration.nextElement();
      UserDO userDO = null;
      StringBuffer msg = new StringBuffer();

      for (int i = 0; i < vectorOfMailContents.size(); i++) {
        MailContent content = (MailContent) vectorOfMailContents.get(i);
        userDO = content.user;
        msg.append("----------------\n " + content.user + " \n" + content.content + "\n\n");
        vectorOfMailContents = new Vector();
      }

      String subject = "[" + Config.SYSTEM_NAME + "] " + userDO.username + ". You got new messages.";
      MailDelivery mailDelivery = new MailDelivery(userDO.getMailAddress(), subject, msg.toString());
      mailDelivery.notifyUser();
    }

    contentObjects = new HashSet();
  }

  /**
   * @return
   */
  protected final static int size() {
    return contentObjects.size();
  }
}
