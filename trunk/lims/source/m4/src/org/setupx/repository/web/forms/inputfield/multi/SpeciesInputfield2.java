/**
 * ============================================================================ File:    SpeciesInputfield2.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.core.communication.ncbi.local.NCBIEntry;
import org.setupx.repository.web.forms.ArrayExpandException;
import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.clazzes.AlignX;
import org.setupx.repository.web.forms.inputfield.BigStringInputField;
import org.setupx.repository.web.forms.inputfield.DateInputfield;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.relation.RelationKingdom;
import org.setupx.repository.web.forms.restriction.Restriction4Int;
import org.setupx.repository.web.forms.restriction.Restriction4Numbers;
import org.setupx.repository.web.forms.restriction.Restriction4StringLength;


/**
 * New Version.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.5 $
 *
 * @hibernate.subclass
 * @deprecated cause it was a MultiField4Clazz - the new one is not anymore
 */
public class SpeciesInputfield2 extends MultiField4Clazz implements AlignX {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @deprecated just for peristencelayer
   */
  public SpeciesInputfield2() {
    super();
  }

  /**
   * Creates a new SpeciesInputfield2 object.
   *
   * @param string
   */
  public SpeciesInputfield2(String speciesName) {
    super("Biological Source ", "define the sample and the organ as detailed as possible.");

    NBCISpeciesInputfield species = null;

    //this.setQuestion(this.getName());
    try {
      // micro
      try {
        //debug(NBCISpeciesInputfield.cameraIMG);
        species = new NBCISpeciesInputfield(speciesName);
        species.getSpeciesInputfield().setValue(speciesName);
      } catch (ArrayExpandException e) {
        throw e;
      } catch (Exception e) {
        e.printStackTrace();
      }

      this.addField(species);

      // organs HUMAN
      InputField inOrganHUMAN = new StringInputfield("organ", "define the organ (human)");
      warning(this, "NCICBOrganRestriction deactivated.");
      //inOrganHUMAN.setRestriction(new NCICBOrganRestriction(inOrganHUMAN));
      inOrganHUMAN.setRestriction(new Restriction4StringLength(3));
      addField(inOrganHUMAN);
      species.addRelation(new RelationKingdom(NCBIEntry.HUMAN, inOrganHUMAN));

      // organs other
      InputField inOrganANIMAL = new StringInputfield("organ", "define the organ (animal)");
      inOrganANIMAL.setRestriction(new Restriction4StringLength(3));
      this.addField(inOrganANIMAL);
      species.addRelation(new RelationKingdom(new int[] { NCBIEntry.ANIMAL, NCBIEntry.PLANT, NCBIEntry.MIRCOORGANISM }, inOrganANIMAL));

      // tissue
      InputField inTissue = new StringInputfield("tissue", "define the tissue");
      inTissue.setValue("-total-");
      inTissue.setHelptext("Biological tissue is a group of cells that perform a similar function.The study of tissues is known as histology, or, in connection with disease, histopathology.");
      inTissue.setRequiered(false);
      this.addField(inTissue);

      // cell type
      StringInputfield inCellType = new StringInputfield("cell type", "please define the cell type.");
      inCellType.setHelptext("The smallest structural unit of living organisms that is able to grow and reproduce independently.");
      inCellType.setRestriction(null);
      inCellType.setRequiered(false);
      this.addField(inCellType);

      // sub cell 
      StringInputfield inSubcellular = new StringInputfield("subcellular celltype", "define the subcellular celltype.");
      inSubcellular.setRestriction(null);
      inSubcellular.setRequiered(false);
      this.addField(inSubcellular);
      species.addRelation(new RelationKingdom(new int[] { NCBIEntry.PLANT, NCBIEntry.ANIMAL, NCBIEntry.HUMAN }, new InputField[] { inTissue, inCellType, inSubcellular }));

      // sex 
      StringInputfield inSex = new StringInputfield("sex", "please define the gender");
      inSex.setValue("male");
      inSex.setRestriction(new Restriction4StringLength(4));
      inSex.setSize_x(7);
      this.addField(inSex);
      species.addRelation(new RelationKingdom(new int[] { NCBIEntry.ANIMAL, NCBIEntry.HUMAN }, new InputField[] { inSex }));

      // birth - human
      DateInputfield inHdob = new DateInputfield();
      inHdob.setQuestion("DOB");
      inHdob.setRequiered(false);
      inHdob.setDescription("date of birth");
      addField(inHdob);

      // age - only animals
      StringInputfield inAAge = new StringInputfield("age", "the animals age");
      inAAge.setSize_x(4);
      inAAge.setRequiered(false);
      inAAge.setRestriction(new Restriction4Int(0, 98));
      inAAge.setExtension("years");
      this.addField(inAAge);
      species.addRelation(new RelationKingdom(new int[] { NCBIEntry.ANIMAL }, new InputField[] { inAAge }));

      // dev stage
      StringInputfield inPDevStage = new StringInputfield("Development Stage", "");
      inPDevStage.setHelptext("TODO");
      this.addField(inPDevStage);
      species.addRelation(new RelationKingdom(new int[] { NCBIEntry.PLANT }, new InputField[] { inPDevStage }));

      // BMI
      StringInputfield inHBMI = new StringInputfield("BMI", "body mass index");
      inHBMI.setHelptext("A popular method used to gauge whether or not a person is overweight. <br>BMI is calculated by dividing a person's weight (in kilograms) by his or her height (in meters, squared).");
      inHBMI.setRestriction(new Restriction4Int());
      inHBMI.setSize_x(4);
      inHBMI.setRequiered(false);
      this.addField(inHBMI);

      StringInputfield inDiagn = new BigStringInputField("Diagnosis", "diagnosis");
      inDiagn.setRestriction(null);
      this.addField(inDiagn);
      species.addRelation(new RelationKingdom(NCBIEntry.HUMAN, new InputField[] { inHBMI, inHdob, inDiagn }));

      // line
      StringInputfield inPLine = new StringInputfield("Line", "line, strain, or genotype home");
      inPLine.setHelptext("TODO");
      inPLine.setRequiered(false);
      this.addField(inPLine);

      // backgorund line
      StringInputfield inPBackgroudLine = new StringInputfield("Parental Line", "the background line of the plant");
      inPBackgroudLine.setRequiered(false);
      this.addField(inPBackgroudLine);
      species.addRelation(new RelationKingdom(new int[] { NCBIEntry.PLANT }, new InputField[] { inPLine, inPBackgroudLine, inPDevStage }));
    } catch (ArrayExpandException e) {
      e.printStackTrace();
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws UnsupportedClazzMappingException TODO
   */
  public String getPath() throws UnsupportedClazzMappingException {
    throw new UnsupportedClazzMappingException();
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws UnsupportedClazzMappingException TODO
   */
  public FormObject getSingleValueField() throws UnsupportedClazzMappingException {
    throw new UnsupportedClazzMappingException();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.MultiField4Clazz#cloneField()
   */
  public InputField cloneField() throws CloneException {
    //      SpeciesInputfield2 instance = new SpeciesInputfield2();
    //      instance.create("--unknown--");
    //      return instance;
    SpeciesInputfield2 spClone = new SpeciesInputfield2("" + this.species().getSpeciesName());

    // take each value and take the value
    if (spClone.getFields().length != this.getFields().length) {
      throw new CloneException("unable to clone " + org.setupx.repository.core.util.Util.getClassName(this) + " cause the number of Fields in the clone is not identical.");
    }

    Object o;

    for (int i = 0; i < this.getFields().length; i++) {
      o = ((InputField) this.getField(i)).getValue();
      ((InputField) spClone.getField(i)).setValue(o);
    }

    spClone.species().setValue(species().getValue());

    return spClone;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.clazzes.ClazzInformation#createShortLabel()
   */
  public String createShortLabel() {
    return this.toStringHTML();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.MultiField4Clazz#determineMainValue()
   */
  public String determineMainValue() {
    try {
      NBCISpeciesInputfield speciesInputfield = this.findNBCISpeciesInputfield();

      return "Species: " + speciesInputfield.entry.getName();
    } catch (Exception e) {
      return "Species";
    }
  }

  /**
   * @return
   */
  public NBCISpeciesInputfield species() {
    return this.findNBCISpeciesInputfield();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.MultiField4Clazz#toStringHTML()
   */
  public String toStringHTML() {
    return determineMainValue();
  }
}
