/**
 * ============================================================================ File:    Mapping.java Package: org.setupx.repository.core.util.hotfix.oldsamples.mapping cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.hotfix.oldsamples.mapping;

import org.setupx.repository.core.CoreObject;

import java.util.Hashtable;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @deprecated
 */
public class Mapping extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final long serialVersionUID = -307538427998859645L;
  private static Object thiz = new Mapping();
  private static Hashtable mappings;

  static {
    mappings = new Hashtable();

    mappings.put("/sample/trayname_generic/class", "class");
    mappings.put("/sample/trayname_generic/line", "line");
    mappings.put("/sample/trayname_generic/Operator_comments", "sampleComment");
    mappings.put("/sample/trayname_generic/Species", "species");
    mappings.put("/sample/trayname_generic/organ", "organ");
    mappings.put("/sample/trayname_generic/organ_specification", "organ_detail");
    mappings.put("/sample/trayname_generic/Growth_location", "gr_location");
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Mapping() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * find the related question inside a promt for a specified path
   *
   * @param string
   *
   * @return
   *
   * @throws MappingException
   */
  public static String getQuestion(String path) throws MappingException {
    debug(thiz, "determine mapping for " + path);

    if (mappings.containsKey(path)) {
      return (String) mappings.get(path);
    } else {
      throw new MappingException("no question defined for " + path + " pathes: " + mappings.size());
    }
  }
}
