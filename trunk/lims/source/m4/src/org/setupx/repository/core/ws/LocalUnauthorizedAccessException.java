package org.setupx.repository.core.ws;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class LocalUnauthorizedAccessException extends Exception {
  /**
   * Creates a new LocalUnauthorizedAccessException object.
   */
  public LocalUnauthorizedAccessException() {
    super();
  }

  /**
   * Creates a new LocalUnauthorizedAccessException object.
   */
  public LocalUnauthorizedAccessException(String arg0) {
    super(arg0);
  }

  /**
   * Creates a new LocalUnauthorizedAccessException object.
   */
  public LocalUnauthorizedAccessException(Throwable arg0) {
    super(arg0);
  }

  /**
   * Creates a new LocalUnauthorizedAccessException object.
   */
  public LocalUnauthorizedAccessException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }
}
