/**
 * ============================================================================ File:    MachineClazzFinishedNotification.java Package: org.setupx.repository.core.communication.notification cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.notification;

import org.setupx.repository.core.communication.notification.Notification;


/**
@deprecated replaced by message   */
public class MachineClazzFinishedNotification extends Notification {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new MachineClazzFinishedNotification object.
   *
   * @param promtID 
   */
  public MachineClazzFinishedNotification(long promtID) {
    super();
    setMessage("Class is finished: " + promtID);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String[] getRecipients() {
    return new String[] { "mscholz@ucdavis.edu" };
  }
}
