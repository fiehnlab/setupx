/**
 * ============================================================================ File:    Price.java Package: org.setupx.repository.core.communication.billing cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.billing;

import org.setupx.repository.core.CoreObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.Currency;
import java.util.Locale;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.4 $
 */
public class Price extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Locale locale = Locale.US;

  // 
  private NumberFormat format;
  private int key;
  private int sum;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Price object.
   *
   * @param sum 
   * @param key 
   */
  public Price(int sum, int key) {
    this(sum, key, Locale.US);
  }

  /**
   * Creates a new Price object.
   *
   * @param sum 
   * @param key 
   * @param locale 
   */
  public Price(int sum, int key, Locale locale) {
    this.locale = locale;
    format = DecimalFormat.getInstance(locale);
    format.setCurrency(Currency.getInstance(locale));
    this.sum = sum;
    this.key = key;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param locale TODO
   */
  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public Locale getLocale() {
    return locale;
  }

  /**
   * @return calculated value
   */
  public String calculate() {
    double value = 0;

    switch (key) {
    case Billing.SAMPLE:
      value = (Billing.SAMPLE_PRICE_USD * sum * getExchangeRatio(this.locale));

      break;

    default:
      err(this, new BillingException("item code: " + key + " unknown"));
    }

    return format.getCurrency().getSymbol() + " " + format.format(value);
  }

  /**
   * returns the exchangeratio
   * 
   * <p>
   * <i>usage:</i><br><code>value multiplied by ratio = value in givven locale</code>
   * </p>
   *
   * @param i_locale locale for which the ratio will be calculated
   *
   * @return exchange ratio for a given locale
   */
  private static double getExchangeRatio(Locale i_locale) {
    // TODO
    warning(null, "Price.getExchangeRatio(Locale i_locale) not implemented.");

    return 1.0;
  }
}
