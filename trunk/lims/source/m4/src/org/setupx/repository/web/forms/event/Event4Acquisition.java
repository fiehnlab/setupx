/**
 * ============================================================================ File:    Event4Acquisition.java Package: org.setupx.repository.web.forms.event cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.event;

import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.communication.exporting.AcquisitionParameter;
import org.setupx.repository.core.communication.exporting.JobInformation;
import org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample;
import org.setupx.repository.server.persistence.PersistenceActionException;

import org.hibernate.Session;


/**
 * @hibernate.subclass
 */
public class Event4Acquisition extends Event {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private AcquisitionSample acquisitionSample;
  private JobInformation jobInformation;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Event4Acquisition object.
   */
  public Event4Acquisition() {
  }

  /**
   * Creates a new Event4Acquisition object.
   *
   * @param result 
   * @param jobInformation2 
   */
  public Event4Acquisition(AcquisitionSample result, JobInformation jobInformation2) {
    super("sheduled for Acquisition ");
    setJobInformation(jobInformation2);
    setAcquisitionSample(result);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @hibernate.many-to-one column="ACQSAMPLE" class = "org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample"
   */
  public AcquisitionSample getAcquisitionSample() {
    return acquisitionSample;
  }

  /**
   * TODO: 
   *
   * @param jobInformation TODO
   */
  public void setJobInformation(JobInformation jobInformation) {
    this.jobInformation = jobInformation;
  }

  /**
   * hibernate.many-to-one column="JOB" class = "org.setupx.repository.core.communication.exporting.JobInformation"
   */
  public JobInformation getJobInformation() {
    return jobInformation;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.CoreObject#persistenceChilds(org.hibernate.Session, boolean)
   */
  public void persistenceChilds(Session session, boolean createIt)
    throws PersistenceActionException {
    if (this.jobInformation != null) {
      this.jobInformation.update(session, createIt);
    }

    if (this.acquisitionSample != null) {
      this.acquisitionSample.update(session, createIt);
    }

    super.persistenceChilds(session, createIt);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.event.Event#toString()
   */
  public String toString() {
    try {
      return super.toString() + this.getAcquisitionSample().getParamter(AcquisitionParameter.NAME).getValue() + " " + this.getAcquisitionSample().getParamter(AcquisitionParameter.VIAL).getValue();
    } catch (ParameterException e) {
      warning(e);

      return super.toString();
    } catch (NullPointerException e) {
      warning(e);

      return super.toString();
    }
  }

  /**
   * @param result
   */
  private void setAcquisitionSample(AcquisitionSample result) {
    this.acquisitionSample = result;
  }
}
