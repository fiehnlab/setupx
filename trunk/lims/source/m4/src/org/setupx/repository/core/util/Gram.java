/**
 * ============================================================================ File:    Gram.java Package: org.setupx.repository.core.util cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util;

class Gram {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  String gram;
  String str;
  int occurences;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  Gram(String inGram, String inString) {
    gram = inGram;
    str = inString;
    occurences = 0;

    int ng = gram.length();
    int len = str.length();

    for (int i = 0; i <= (len - ng); i++) {
      String gr = str.substring(i, i + ng);

      if (gram.equalsIgnoreCase(gr)) {
        occurences++;
      }
    }
  }
}
