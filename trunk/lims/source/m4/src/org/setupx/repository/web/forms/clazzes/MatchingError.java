/**
 * ============================================================================ File:    MatchingError.java Package: org.setupx.repository.web.forms.clazzes cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.clazzes;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class MatchingError extends Exception {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new MatchingError object.
   *
   * @param string 
   */
  public MatchingError(String string) {
    super(string);
  }

  /**
   * Creates a new MatchingError object.
   *
   * @param string 
   * @param e 
   */
  public MatchingError(String string, Exception e) {
    super(string, e);
  }
}
