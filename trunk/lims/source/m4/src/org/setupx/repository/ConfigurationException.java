package org.setupx.repository;

import java.net.URL;

/**
 * 
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class ConfigurationException extends Exception {
    /**
     *
     */
    public ConfigurationException() {
        super();

        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public ConfigurationException(String message) {
        super(message);

        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public ConfigurationException(Throwable cause) {
        super(cause);

        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     */
    public ConfigurationException(String message, Throwable cause) {
        super(message, cause);

        // TODO Auto-generated constructor stub
    }

    /**
     * @param url
     * @param e1
     */
    public ConfigurationException(URL url, Exception e1) {
        // super("unable to connect to " + url.toString() + " - " + e1.getCause().getMessage(), e1);
        super("unable to connect to " + url.toString(), e1);
    }
}
