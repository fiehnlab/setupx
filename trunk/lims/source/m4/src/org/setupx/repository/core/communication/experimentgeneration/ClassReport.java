/**
 * ============================================================================ File:    ClassReport.java Package: org.setupx.repository.core.communication.experimentgeneration cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.experimentgeneration;

import java.io.File;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.xml.XMLFile;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class ClassReport extends CoreObject {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ClassReport object.
   *
   * @param file 
   *
   * @throws Exception 
   */
  public ClassReport(File file) throws Exception {
    StringBuffer buffer = new StringBuffer();
    String[] pathes = new String[] { "/sample/trayname_generic/Species", "/sample/trayname_generic/organ", "/sample/trayname_generic/organ_specification", "/sample/trayname_generic/line", "/sample/trayname_generic/Operator_comments", "/sample/trayname_generic/class" };

    for (int i = 0; file.isDirectory() && (i < file.listFiles().length); i++) {
      File clazz_directory = file.listFiles()[i];
      debug("reading " + clazz_directory.getName());

      for (int j = 0; clazz_directory.isDirectory() && (j < clazz_directory.listFiles().length); j++) {
        File sampleFile = clazz_directory.listFiles()[j];
        buffer.append(sampleFile.getName() + "\t");

        for (int k = 0; k < pathes.length; k++) {
          String path = pathes[k];

          try {
            XMLFile xmlFile = new XMLFile(sampleFile);
            String value = xmlFile.pathValue(path);

            //debug(sampleFile.getName() + "\t" + value + "\t" + path);
            buffer.append(value + "\t");
          } catch (Exception e) {
            buffer.append("-ERR-" + "\t");
          }
        }

        buffer.append("\n");
      }
    }

    org.setupx.repository.core.util.File.storeData2File(new File(file, "classesReport.txt"), buffer.toString());
  }
}
