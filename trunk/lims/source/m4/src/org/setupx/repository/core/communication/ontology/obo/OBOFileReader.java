/**
 * ============================================================================ File:    OBOFileReader.java Package: org.setupx.repository.core.communication.ontology.obo cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ontology.obo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.setupx.repository.core.InitException;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;


/**
 * Title:   This class will read an OBO file for further analysis 
 * Copyright:    Copyright (c) 2005 Company: University of Washington
 *
 * @author: Adam Silberfein, Li-Kuan Chen (with some modifications)
 * @author Martin Scholz
 * @version 1.1
 */
public class OBOFileReader {
    //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public File sourceFile;

    /** 
     * key: term.getID  value: term
     */
    protected OBOHashtable oboHashtable = new OBOHashtable("GO1"); // 

    /**
     * MartinScholz: key: term   value: term.getID  // THAT IS SOOOO DIRTY - who wrote this stuff!
     */   
    protected OBOHashtable termIdHashtable = new OBOHashtable("GO2");   

    /** This will hold the collection of relationships that are discovered */
    private ArrayList relationships = new ArrayList();
    
    private BufferedReader inputReader;

    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new OBOFileReader object.
     */
    public OBOFileReader(File inputFile) throws InitException {
        try {
            Logger.debug(this, "starting obo filereader for oboFile: " + inputFile.getAbsolutePath());
            this.inputReader = new BufferedReader(new FileReader(inputFile));
            this.sourceFile = inputFile;
            this.readTerms();
        } catch (Exception e) {
            throw new InitException("unable to init " + Util.getClassName(this));
        }
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * TODO: 
     *
     * @return TODO
     */
    public ArrayList getRelationships() {
        return this.relationships;
    }

    //Print out the final collections
    public OBOHashtable getoboHashtable() {
        return this.oboHashtable;
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public OBOHashtable gettermIdHashtable() {
        System.out.println("Term Table Size: " + this.termIdHashtable.size());

        return this.termIdHashtable;
    }

    // This function will read through a text (OBO) file and create OBOTerm objects, to be stored in an
    // ArrayList. All attributes of the OBOTerm will be stored along the way.
    public void readTerms() {
        String line;
        OBOTerm currentTerm = null;
        String currentRelName = "";

        int flag = 0; //0: default, 1: reading [terms], 2: reading [relationships]

        try {
            while ((line = this.inputReader.readLine()) != null) {
                //Discard header texts
                if (line.startsWith("[Term]")) {
                    //Store and close the preceding term and start a new one
                    //Disregard obsolete terms
                    if (flag == 1) {
                        if (!currentTerm.isObsolete) {
                            oboHashtable.put(currentTerm.getId(), currentTerm); //(K,V)
                            termIdHashtable.put(currentTerm.getTerm(), currentTerm.getId());
                        }

                        currentTerm = new OBOTerm();
                    } else if (flag == 0) {
                        flag = 1;
                        currentTerm = new OBOTerm();
                    }
                } else if (!line.startsWith("[Term]") & !line.startsWith("[Typedef]") & (flag == 1)) {
                    if (line.startsWith("id:")) {
                        ArrayList a = parseIdLine(line);
                        currentTerm.setPrefix((String) a.get(0));
                        currentTerm.setId((String) a.get(1));
                    } else if (line.startsWith("name:")) {
                        currentTerm.setTerm(parseTermLine(line));
                    } else if (line.startsWith("def:")) {
                        ArrayList def = parseDefLine(line);
                        currentTerm.setDefinition((String) def.get(0));
                        currentTerm.setDefReference((String) def.get(1));
                    } else if (line.startsWith("comment:")) {
                        currentTerm.setComment(parseCommentLine(line));
                    } else if (line.startsWith("subset:")) {
                        currentTerm.setSubset(parseSubsetLine(line));
                    } else if (line.startsWith("is_a:")) {
                        currentTerm.addParent(parseParentLine(line));
                    } else if (line.startsWith("synonym:") || line.startsWith("exact_synonym:") || line.startsWith("related_synonym")) {
                        currentTerm.addSynonym(parseSynonymLine(line));
                    } else if (line.startsWith("is_obsolete:")) {
                        currentTerm.isObsolete = true;
                    } else if (line.startsWith("xref_analog:")) {
                        currentTerm.addXref(parseXrefLine(line));
                    } else if (line.startsWith("relationship:")) {
                        parseRelationshipLine(currentTerm, line);
                    }
                } else if (line.startsWith("[Typedef]")) {
                    //Assume [Typedef] always appear at the very end of the file 
                    //Store the preceding term. This case is very rare. 
                    if ((flag == 1) & !currentTerm.isObsolete) {
                        oboHashtable.put(currentTerm.getId(), currentTerm);
                        termIdHashtable.put(currentTerm.getTerm(), currentTerm.getId());
                        flag = 2;
                    }
                    //Store and close the preceding relationship
                    else if (flag == 2) {
                        relationships.add(currentRelName);
                    }
                } else if ((flag == 2) & line.startsWith("id:")) {
                    currentRelName = parseRelIdLine(line);
                }
            }

            // Take care of the last term/relationship
            if (flag == 1) {
                if (!currentTerm.isObsolete) {
                    oboHashtable.put(currentTerm.getId(), currentTerm);
                }

                termIdHashtable.put(currentTerm.getTerm(), currentTerm.getId());
            } else if (flag == 2) {
                relationships.add(currentRelName);
            }

            this.inputReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * TODO: 
     *
     * @param line TODO
     *
     * @return TODO
     */
    private String parseCommentLine(String line) {
        return line.substring(9);
    }

    // This method returns an ArrayList containing both the definition[0] and the reference[1]
    private ArrayList parseDefLine(String line) {
        ArrayList retVal = new ArrayList();

        int secondQuoteIndex = line.indexOf("\"", 6); // This will be the closing quote for the definition

        StringTokenizer tok = new StringTokenizer(line, "\"");
        tok.nextToken(); // Gets rid of "def: ")

        String def = tok.nextToken();
        String reference = "";

        if (tok.hasMoreTokens()) // There is a definition reference
        {
            reference = line.substring(secondQuoteIndex + 1);
        }

        retVal.add(removeEscapes(def.trim()));
        retVal.add(removeEscapes(reference.trim()));

        return retVal;
    }

    // Returns an ArrayList with the prefix[0] and id[1]
    private ArrayList parseIdLine(String line) {
        ArrayList retVal = new ArrayList(2);

        // ID will be everything after the second colon
        int colonIndex = line.indexOf(':', 4);
        String prefix = line.substring(4, colonIndex);
        String id = line.substring(colonIndex + 1);
        retVal.add(prefix);
        retVal.add(id);

        return retVal;
    }

    /**
     * TODO: 
     *
     * @param line TODO
     *
     * @return TODO
     */
    private String parseParentLine(String line) {
        // This will be everything after the second colon
        int colonIndex = line.indexOf(':', 6);

        return line.substring(colonIndex + 1);
    }

    /**
     * TODO: 
     *
     * @param line TODO
     *
     * @return TODO
     */
    private String parseRelIdLine(String line) {
        return line.substring(4);
    }

    /**
     * TODO: 
     *
     * @param term TODO
     * @param line TODO
     */
    private void parseRelationshipLine(OBOTerm term, String line) {
        StringTokenizer tok = new StringTokenizer(line);
        tok.nextToken();

        String relName = tok.nextToken();
        String id = tok.nextToken();

        // This ID will be everything after the colon
        String val = id.substring(id.indexOf(':') + 1);
        //Logger.debug(this, term.getId() + " " + term.getTerm() + "adding relationship: " + relName + " value: " + val);
        term.addRelationship(relName, val);
    }

    /**
     * TODO: 
     *
     * @param line TODO
     *
     * @return TODO
     */
    private String parseSubsetLine(String line) {
        return line.substring(8);
    }

    /**
     * TODO: 
     *
     * @param line TODO
     *
     * @return TODO
     */
    private String parseSynonymLine(String line) {
        StringTokenizer tok = new StringTokenizer(line, "\"");
        tok.nextToken(); // Gets rid of "synonym: ")

        return removeEscapes(tok.nextToken());
    }

    /**
     * TODO: 
     *
     * @param line TODO
     *
     * @return TODO
     */
    private String parseTermLine(String line) {
        return line.substring(6);
    }

    /**
     * TODO: 
     *
     * @param line TODO
     *
     * @return TODO
     */
    private String parseXrefLine(String line) {
        return line.substring(13);
    }

    // This method removes the espcape characters form the input String
    private String removeEscapes(String input) {
        String retVal = input;

        while (retVal.indexOf('\\') >= 0) {
            int index = retVal.indexOf('\\');
            retVal = retVal.substring(0, index) + retVal.substring(index + 1);
        }

        return retVal;
    }
}
