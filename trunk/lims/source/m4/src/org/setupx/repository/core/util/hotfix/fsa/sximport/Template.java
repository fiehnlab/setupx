/**
 * ============================================================================ File:    Template.java Package: org.setupx.repository.core.util.hotfix.fsa.sximport cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.hotfix.fsa.sximport;

import java.util.HashSet;


abstract class Template {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private HashSet childs = new HashSet();
  private int id;

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  final HashSet getChilds() {
    return this.childs;
  }

  /**
   * TODO: 
   *
   * @param id TODO
   */
  final void setID(int id) {
    this.id = id;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  final int getID() {
    return this.id;
  }

  /**
   * TODO: 
   *
   * @param object TODO
   */
  final void addChild(Template object) {
    System.out.println("adding " + object.toString());
    this.childs.add(object);
  }

  /**
   * TODO: 
   *
   * @param sxID TODO
   *
   * @return TODO
   */
  final static int parse(String sxID) {
    sxID = org.setupx.repository.core.util.Util.replace(sxID, "\n", "");

    return Integer.parseInt(sxID);
  }
}
