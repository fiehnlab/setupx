/**
 * ============================================================================ File:    BooleanInputField.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import org.setupx.repository.core.util.Util;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 *
 * @hibernate.subclass
 */
public class BooleanInputField extends InputField {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String submitOnChange = "";

  {
    this.setValue("" + false);
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @deprecated just for peristencelayer
   */
  public BooleanInputField() {
    this("", "");
  }

  /**
   * Creates a new BooleanInputField object.
   *
   * @param question 
   * @param desc 
   */
  public BooleanInputField(String question, String desc) {
    super(question, desc);
  }

  /**
   * Creates a new BooleanInputField object.
   *
   * @param question 
   * @param desc 
   * @param submitIfChanged 
   */
  public BooleanInputField(String question, String desc, boolean submitIfChanged) {
    super(question, desc);

    if (submitIfChanged) {
      this.setSubmitonchangeBOOL("onClick=\"document.forms[0].submit()\"");
    }

    // add a link that the user find the old position again.
    // TODO
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


  /**
   * TODO: 
   *
   * @param string TODO
   */
  public void setSubmitonchangeBOOL(String string) {
    this.submitOnChange = string;
  }

  /**
   * @return
   *
   * @hibernate.property
   */
  public String getSubmitonchangeBOOL() {
    return this.submitOnChange;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() {
    return new BooleanInputField(this.getQuestion(), this.getDescription());
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTML()
   */
  public String createHTML(int col) {
    String noSelect = "";
    String yesSelect = "";

    // check if the value is selected - otherwise do not select it
    boolean selected = false;

    if (((String) this.getValue()).compareTo("true") == 0) {
      debug(this, "SELECETED value: " + this.getValue());
      selected = true;
    } else {
      debug(this, "NOT SELECETED value: " + this.getValue());
    }

    if (selected) {
      yesSelect = "checked=\"true\"";
    } else {
      noSelect = "checked=\"false\"";
    }

    return "<!-- " + Util.getClassName(this) + " -->\n\n<tr><td  colspan=" + (col - 1) + ">" + this.getQuestion() + this.createRequieredNote() + "<br/><font size=\"-3\">&nbsp &nbsp &nbsp " + this.getDescription() + "</font></td><td>" + "<input type=\"radio\" name=\"" + this.getName() + "\" " +
    this.submitOnChange + " value=\"true\" " + yesSelect + " /> YES  " + "<input type=\"radio\" name=\"" + this.getName() + "\"  " + this.submitOnChange + " value=\"false\" " + noSelect + " /> NO " + this.createHelpButton() + "</td></tr>";
  }
}
