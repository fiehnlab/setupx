/*
 * Created on 17.11.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.setupx.repository.web.forms;

/**
 * @author ibm user TODO To change the template for this generated type comment go to Window -
 *         Preferences - Java - Code Style - Code Templates
 */
public class ArrayExpandException extends Exception {
  /**
   *
   */
  public ArrayExpandException() {
    super();

    // TODO Auto-generated constructor stub
  }

  /**
   * @param message
   */
  public ArrayExpandException(String message) {
    super(message);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param cause
   */
  public ArrayExpandException(Throwable cause) {
    super(cause);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param message
   * @param cause
   */
  public ArrayExpandException(String message, Throwable cause) {
    super(message, cause);

    // TODO Auto-generated constructor stub
  }
}
