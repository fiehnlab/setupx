/*
 * Created on 23.01.2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.setupx.repository.web.forms.inputfield.multi;

/**
 * @author scholz
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class NBCISpeciesInputfieldException extends Exception {

    /**
     * 
     */
    public NBCISpeciesInputfieldException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public NBCISpeciesInputfieldException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public NBCISpeciesInputfieldException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     */
    public NBCISpeciesInputfieldException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
