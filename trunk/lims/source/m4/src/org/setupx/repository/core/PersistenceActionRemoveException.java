package org.setupx.repository.core;

import org.setupx.repository.server.persistence.PersistenceActionException;

public class PersistenceActionRemoveException extends PersistenceActionException {

    public PersistenceActionRemoveException(String string, Exception e) {
        super(string, e);
    }

}
    