/**
 * ============================================================================ File:    QueryServlet.java Package: org.setupx.repository.web cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web;

import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.core.util.web.UiHttpServlet;
import org.setupx.repository.core.util.web.WebForwardException;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.Sample;

import org.hibernate.Session;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * queries for id - mainly constructed for communication with systems (binbase,...)
 * 
 * <p>
 * the returned result is an xml document
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class QueryServlet extends UiHttpServlet {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // defining the type of query that will be run 
  int queryType = 0;
  private String host;
  private long clazzID = 0;
  private long promtID = 0;
  private long sampleID = 0;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final int EXPERIMENT_BY_CLASS = 1;
  public static final int EXPERIMENT_BY_SAMPLE = 2;
  public static final int SAMPLE_BY_EXPERIMENT = 4;
  public static final int SAMPLE_BY_CLASS = 5;
  public static final int SAMPLE_BY_SAMPLE = 6;
  public static final int SAMPLE_BY_DATE = 7;
  public static final int SAMPLE_BY_OWNER = 8;
  public static final int SAMPLE_ALL = 10;
  public static final int CLASS_BY_SAMPLE = 11;

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.core.util.web.UiHttpServlet#process(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  public void process(HttpServletRequest request, HttpServletResponse response)
    throws WebForwardException {
    // List of elements that will be send back 
    HashSet samples = new HashSet();

    // check ip
    host = request.getRemoteAddr();
    //this.validateIP(host, request);
    new WebAccess(request);

    // set values by reading them from the request
    readParameters(request);

    Exception ex;

    try {
      switch (queryType) {
      case EXPERIMENT_BY_CLASS:
        break;

      case EXPERIMENT_BY_SAMPLE:
        break;

      case SAMPLE_BY_CLASS:
        samples = ((Clazz) FormObject.persistence_loadByID(Clazz.class, clazzID)).getSamples();

        break;

      case CLASS_BY_SAMPLE:
        break;

      case SAMPLE_BY_EXPERIMENT:

        List list = new SXQuery().findSampleIDsByPromtID(promtID);
        Iterator iter = list.iterator();

        while (iter.hasNext()) {
          samples.add((Sample) Sample.persistence_loadByID(Sample.class, Long.parseLong("" + iter.next())));
        }

        //samples = ((Clazz) FormObject.persistence_loadByID(Clazz.class, clazzID)).getSamples();
        break;

      /*
      
                                   try {
                                     samples = toHashSet(Promt.load(promtID,true).getSamples());
                                   } catch (SampleScheduledException e) {
                                     e.printStackTrace();
                                   }
                                   break;
       */
      case SAMPLE_BY_SAMPLE:
        samples = toHashSet(new Sample[] { Sample.loadByID(sampleID) });

        break;

      case SAMPLE_BY_DATE:
        break;

      case SAMPLE_BY_OWNER:
        break;

      case SAMPLE_ALL:

        Session session = PersistenceConfiguration.createSessionFactory().openSession();
        List listofAll = Sample.persistence_loadAll(Sample.class, session);
        samples.addAll(listofAll);

        break;

      default:
        break;
      }

      /*} catch (PromtCreateException e) {
         ex = e;
       */
    } catch (PersistenceActionException e) {
      ex = e;
    }

    if (samples.size() == 0) {
      //ex.printStackTrace();
      forward("/aq/query.jsp", request, response);

      return;
    }

    // attatch the samples to the session
    request.getSession().putValue(WebConstants.SESS_SAMPLES, samples);
    Logger.log(this, "added " + samples.size() + " elements to the samples.");
    forward("/aq/sample_list.jsp", request, response);
  }

  /**
   * TODO: 
   */
  public static void testingQueries() {
    Logger.log(null, "start looking for ids");

    SampleID sampleID = new SampleID("1234");
    ClazzID clazzID = new ClazzID("123");
    Clazz clazz;

    try {
      Logger.log(null, "looking for clazz id");
      clazz = findClazz(sampleID);
      Logger.log(null, "found clazz: " + clazz.getUOID());
    } catch (PersistenceActionFindException e) {
      e.printStackTrace();
    }

    /*
       try {
         Logger.log(null, "looking for samples");
         Sample[] samples;
         //samples = findSample(clazzID);
         Logger.log(null, "found " + samples.length + " samples for " + clazzID.uoid);
       } catch (PersistenceActionException e1) {
         e1.printStackTrace();
       } catch (PromtCreateException e1) {
         e1.printStackTrace();
       }*/
  }

  /**
   * TODO: 
   *
   * @param sampleID2 TODO
   *
   * @return TODO
   *
   * @throws PersistenceActionFindException TODO
   */
  private static Clazz findClazz(SampleID sampleID2) throws PersistenceActionFindException {
    Session session = PersistenceConfiguration.createSessionFactory().openSession();
    Sample sample = (Sample) Promt.persistence_loadByID(Sample.class, session, sampleID2.uoid);

    if (sample == null) {
      return null;
    } else {
      return (Clazz) (sample.getParent());
    }
  }

  /**
   * TODO: 
   *
   * @param samples TODO
   *
   * @return TODO
   */
  private static HashSet toHashSet(Sample[] samples) {
    HashSet hashSet = new HashSet();

    for (int i = 0; i < samples.length; i++) {
      hashSet.add(samples[i]);
    }

    return hashSet;
  }

  /**
   * TODO: 
   *
   * @param request TODO
   */
  private void readParameters(HttpServletRequest request) {
    this.promtID = 0;
    this.clazzID = 0;
    this.sampleID = 0;

    this.queryType = 0;

    this.promtID = UiHttpServlet.getRequestParameterLong(request, WebConstants.QUERYSERVLET_EXPERIMENT_ID);
    this.clazzID = UiHttpServlet.getRequestParameterLong(request, WebConstants.QUERYSERVLET_CLAZZ_ID);
    this.sampleID = UiHttpServlet.getRequestParameterLong(request, WebConstants.QUERYSERVLET_SAMPLE_ID);

    this.queryType = UiHttpServlet.getRequestParameterInt(request, WebConstants.QUERYSERVLET_QUERYTYPE);
  }

  /**
   * validates if the server has the correct ip.
   *
   * @param host the servers ip address
   */
  private static void validateIP(String host, HttpServletRequest request)
    throws InvalidIpException {
    Logger.debug(null, "access from: " + host);

    // localhost
    // 1.5  String localAddress = request.getLocalAddr();
    String localAddress = request.getServerName();

    if (localAddress.compareTo(host) == 0) {
      return;
    }

    System.out.println("IP CHECK DEAVTIVATED!");
  }
}
