/*
 * Created on 18.08.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.setupx.repository.server.workflow;

/**
 * @author ibm user TODO To change the template for this generated type comment go to Window -
 *         Preferences - Java - Code Style - Code Templates
 */
public class MemoryObjectNotFoundException extends Exception {
  /**
   *
   */
  public MemoryObjectNotFoundException() {
    super();

    // TODO Auto-generated constructor stub
  }

  /**
   * @param message
   */
  public MemoryObjectNotFoundException(String message) {
    super(message);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param cause
   */
  public MemoryObjectNotFoundException(Throwable cause) {
    super(cause);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param message
   * @param cause
   */
  public MemoryObjectNotFoundException(String message, Throwable cause) {
    super(message, cause);

    // TODO Auto-generated constructor stub
  }
}
