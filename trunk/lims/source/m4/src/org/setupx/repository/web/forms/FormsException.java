/*
 * Created on 09.11.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.setupx.repository.web.forms;

/**
 * @author ibm user TODO To change the template for this generated type comment go to Window -
 *         Preferences - Java - Code Style - Code Templates
 */
public class FormsException extends Exception {
  /**
   *
   */
  public FormsException() {
    super();

    // TODO Auto-generated constructor stub
  }

  /**
   * @param message
   */
  public FormsException(String message) {
    super(message);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param cause
   */
  public FormsException(Throwable cause) {
    super(cause);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param message
   * @param cause
   */
  public FormsException(String message, Throwable cause) {
    super(message, cause);

    // TODO Auto-generated constructor stub
  }
}
