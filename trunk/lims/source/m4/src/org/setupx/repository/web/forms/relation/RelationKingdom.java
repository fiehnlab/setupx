/**
 * ============================================================================ File:    RelationKingdom.java Package: org.setupx.repository.web.forms.relation cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.relation;

import org.setupx.repository.core.communication.ncbi.NCBIConnector;
import org.setupx.repository.core.communication.ncbi.NCBIException;
import org.setupx.repository.core.communication.ncbi.local.NCBIEntry;
import org.setupx.repository.web.forms.ComparisonsException;
import org.setupx.repository.web.forms.FormObject;


/**
 * Relation, that activates an Inputfield by the selected Kingdom
 * 
 * <p>
 * <strong><font color="RED">FIXME:</font></strong>
 * </p>
 * 
 * <p>
 * The persistence for the type array does not work.!!
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 *
 * @hibernate.subclass
 */
public class RelationKingdom extends Relation {
    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * related inputfields
     */

    //private FormObject[] objects;

    /**
     * defines the type of related species
     */

    //  private int[] types;

    /**
     * Creates a new RelationKingdom object.
     */
    public RelationKingdom() {
        this(new int[] {  }, new FormObject[] {  });
    }

    /**
     * @param type type of organ
     * @param organ related organinputfield
     */
    public RelationKingdom(int type, FormObject object) {
        this(new int[] { type }, new FormObject[] { object });
    }

    /**
     * @param type type of organ
     * @param organ related organinputfield
     */
    public RelationKingdom(int type, FormObject[] objects) {
        this(new int[] { type }, objects);
    }

    /**
     * Creates a new RelationKingdom object.
     *
     * @param ids 
     * @param object 
     */
    public RelationKingdom(int[] ids, FormObject object) {
        this(ids, new FormObject[] { object });
    }

    /**
     * Relation
     *
     * @param strings
     * @param fields
     */
    public RelationKingdom(int[] ids, FormObject[] fields) {
        super(mapToStringArray(ids), fields);

        /*    this.types = ids;
       this.objects = fields;
       debug("creating new instance.");
       debug("required type :" + this.types + " ");
       for (int i = 0; i < this.types.length; i++) {
         debug("type: " + this.types[i]);
       }
       debug("related objects: " + this.objects.length + " ");
       for (int i = 0; i < this.objects.length; i++) {
         debug("object: " + this.objects[i]);
       }
         */
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * the types that are requiered to be actived
     *
     * @param types
     *
     * @see NCBIEntry#ANIMAL
     * @see NCBIEntry#HUMAN
     * @see NCBIEntry#PLANT
     * @see org.setupx.repository.core.communication.ncbi.local.NCBI_Classifier#classify(NCBIEntry)
     */
    public void setTypes(int[] types) {
        this.setConditions(mapToStringArray(types));
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public int[] getTypes() {
        return mapToIntArray(this.getConditions());
    }

    /*
     *  (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return (org.setupx.repository.core.util.Util.getClassName(this) + ":  Different types: " + this.getConditions().length + "   related to " + this.getRelationTargets().length + " objects.");
    }

    /* (non-Javadoc)
     * @see org.setupx.repository.web.forms.relation.RelationInterface#update(java.lang.String)
     */
    public void update(String string) {
        if (string.length() < 1)  {
            //warning(this, ".update(String string): ignoring update(String) cause value length < 1");
            return;
        }
        // dont do anything - 
        warning(this, ".update(String string): trying to update this relation: But it is not made for String updates - USE update(NCBIEntry) !");


        /* 
         * I guess this is the source that experiments loose their structure.
         * - take the string 
         * - convert it back to an ncbi entry 
         * - set the entry
         * - check if the entry.getName is identical with the "string"
         */

        debug("update(String) with " + string);

        int id;
        try {
            id = NCBIConnector.determineNCBI_Id(string);
        } catch (NCBIException e) {
            e.printStackTrace();
            warning(e);
            return;
        }

        debug("update(String) - found ncbi id: " + id);

        NCBIEntry entry;
        try {
            entry = NCBIConnector.determineNCBI_Information(id);
        } catch (NCBIException e) {
            e.printStackTrace();
            warning(e);
            return;
        }

        debug("update(String) - found ncbi entry : " + entry.getName());

        this.update(entry);

        debug("update(String) - updated via update(NCBIEntry) using " + entry.getName());
    }

    /**
     * updates the related objects.
     *
     * @param entry the ncbientry that is beeing set.
     */
    public void update(NCBIEntry entry) {
        if (entry == null){
            // dont do anything
            warning(this, "trying to set an entry to null");
            return;
        }

        /*
        debug(".update(NCBIEntry entry) - start ------");
        debug(".update(NCBIEntry entry):" + entry.getName() + " " + entry.getSpeciesTypeName());
        debug(".update(NCBIEntry entry):" + this.getRelationTargets().length + " related objects.");
        debug(".update(NCBIEntry entry):" + this.getTypes().length + " required types.");
        */


        //MailController.inform(Config.OPERATOR_ADMIN, "NCBIcode:" + entry.getTaxID(), ".update(NCBIEntry entry) " + entry.getName() + " species type: " + entry.getSpeciesType() + "\n\n " + entry);
        boolean _active = false;

        // figuring out if any of the related objects (speciestypes) matches this.
        for (int i = 0; i < this.getTypes().length; i++) {
            int tempID = this.getTypes()[i];

            if (entry.getSpeciesType() == tempID) {
                _active = true;
            }
        }

        // changing each activation for related fields.
        for (int i = 0; i < this.getRelationTargets().length; i++) {
            this.getRelationTargets()[i].setActive(_active);
        }
    }

    /**
     * TODO: 
     *
     * @param strings TODO
     *
     * @return TODO
     */
    private int[] mapToIntArray(String[] strings) {
        int[] result = new int[strings.length];

        for (int i = 0; i < strings.length; i++) {
            result[i] = Integer.parseInt(strings[i]);
        }

        return result;
    }

    /**
     * TODO: 
     *
     * @param types2 TODO
     *
     * @return TODO
     */
    private static String[] mapToStringArray(int[] types2) {
        String[] result = new String[types2.length];

        for (int i = 0; i < types2.length; i++) {
            result[i] = "" + types2[i];
        }

        return result;
    }

    public void compare(AbstractRelation relation2) throws ComparisonsException{
        super.compare(relation2);
        if (this.getTypes().length != ((RelationKingdom)relation2).getTypes().length )throw new ComparisonsException(this, relation2, "types length");
        if (this.getRelationTargets().length != ((RelationKingdom)relation2).getRelationTargets().length )throw new ComparisonsException(this, relation2, "relation targets length");
    }

}
