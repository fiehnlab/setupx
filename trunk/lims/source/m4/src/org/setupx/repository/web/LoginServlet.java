/**
 * ============================================================================ File:    LoginServlet.java Package: org.setupx.repository.web cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web;

import org.setupx.repository.Config;
import org.setupx.repository.Settings;
import org.setupx.repository.core.ServiceMediator;
import org.setupx.repository.core.communication.mail.EMailAddress;
import org.setupx.repository.core.communication.mail.MailController;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.UserFactoryException;
import org.setupx.repository.core.user.UserNotFoundException;
import org.setupx.repository.core.user.WrongPasswordException;
import org.setupx.repository.core.util.File;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.core.util.web.UiHttpServlet;
import org.setupx.repository.core.util.web.WebForwardException;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.12 $
 */
public class LoginServlet extends UiHttpServlet {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.util.web.UiHttpServlet#process(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  public void process(HttpServletRequest request, HttpServletResponse response)
    throws WebForwardException {
    // determine id and password from session
    String id = getRequestParameter(request, WebConstants.PARAM_ID);
    String password = getRequestParameter(request, WebConstants.PARAM_PASSWORD);

    Logger.log(this, "logging in: " + id);

    // find the user
    UserDO userDO = null;

    try {
      userDO = ServiceMediator.findUser(id, password);
    } catch (UserNotFoundException e) {
      forwardToLogin(request, response);

      return;
    } catch (WrongPasswordException e) {
      forwardToLogin(request, response);

      return;
    } catch (UserFactoryException e) {
      forwardToLogin(request, response);

      return;
    }

    // add userObject to session
    request.getSession().setAttribute(WebConstants.SESS_USER, userDO);

    // NEW:
    // adding every session to the hashtable of existing sessions.
    HttpSessionPool.add(request.getSession());

    MailController.inform(new EMailAddress("mscholz@ucdavis.edu"), userDO.getDisplay() + "[" + userDO.getMailAddress().getEmailAddress() + "] logged in.", "msg");

    // decide where to go 
    if (userDO.isLabTechnician()) {
      forward("/admin.jsp", request, response);

      return;
    } else {
      if (isMaintaining(request)) {
        forward("/maintain.jsp", request, response);
      } else {
        forward("/main.jsp", request, response);
      }
    }

    // forward to main page
    //forward("/form?form_target=0", request, response);
  }

  /**
   * TODO: 
   *
   * @param request TODO
   *
   * @return TODO
   */
  private boolean isMaintaining(HttpServletRequest request) {
    Logger.log(this, "checking MAINTAIN mode");

    String content = "";

    try {
      content = File.getFileContent(new java.io.File(Settings.getValue("system.maintain.messagefile", Config.DIRECTORY_DATA + java.io.File.separator + "maintain.txt" )));
    } catch (IOException e) {
      e.printStackTrace();
    }

    if (content.length() > 2) {
      request.getSession().setAttribute("maintain", convertToHTML(content));

      return true;
    } else {
      return false;
    }
  }

  /**
   * TODO: 
   *
   * @param content TODO
   *
   * @return TODO
   */
  private String convertToHTML(String content) {
    return org.setupx.repository.core.util.Util.replace(content, "\n", "<br>");
  }

  /**
   * TODO: 
   *
   * @param request TODO
   * @param response TODO
   *
   * @throws WebForwardException TODO
   */
  private void forwardToLogin(HttpServletRequest request, HttpServletResponse response)
    throws WebForwardException {
    forward("/login_err.jsp", request, response);
  }
}
