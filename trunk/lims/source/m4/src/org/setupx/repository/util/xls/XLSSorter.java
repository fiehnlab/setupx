package org.setupx.repository.util.xls;

import org.setupx.repository.core.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;


/**
 * 
 *
 * @author $author$
 * @version $Revision$
 */
public class XLSSorter {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param args TODO
   *
   * @throws IOException TODO
   */
  public static void main(String[] args) throws IOException {
    InputStream input = new FileInputStream(new File("/mnt/metabolomics/Scholz/test.xls"));

    HSSFWorkbook workbook = new HSSFWorkbook(input);
    HSSFSheet sheet = workbook.getSheetAt(0);

    short cellPos = 0;

    for (; cellPos < 200; cellPos++) {
      SortedSet set = new TreeSet();

      int rowPos = 0;

      for (; rowPos < 200; rowPos++) {
        try {
          HSSFRow row = sheet.getRow(rowPos);

          if (row == null) {
            throw new Exception();
          }

          //Logger.debug(null,"------ row:" + rowPos + " cell:" + cellPos + " ------");
          HSSFCell cell = row.getCell(cellPos);
          Double double1 = new Double(cell.getNumericCellValue());

          //Logger.debug(null,"" + value);
          set.add(double1);
        } catch (Exception e) {
        }
      }

      Logger.debug(null, " - - - - -  - -");

      Iterator iterator = set.iterator();
      short pos = 0;

      while (iterator.hasNext()) {
        HSSFRow row = sheet.getRow(pos);

        //short pos = new Short("" + (set.size() + 5)).shortValue();
        HSSFCell cell = row.getCell(cellPos);
        cell.setCellValue(new Double("" + iterator.next()).doubleValue());
        pos++;
      }
    }

    FileOutputStream out = new FileOutputStream(new File("/mnt/metabolomics/Scholz/test_result.xls"));
    workbook.write(out);
  }
}
