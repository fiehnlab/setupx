/**
 * ============================================================================ File:    Navigation.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.core.util.Util;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.forms.FormContainer;
import org.setupx.repository.web.forms.inputfield.InputField;

import java.util.Vector;


/**
 * navigation within a page. can be included like every other formobject and finds then all elements of the  defined type out of this formcontainer, which is usually a Page.
 *
 * @see org.setupx.repository.web.forms.Page
 */
public class Navigation extends MultiField {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Class reqieredClass;
  private FormContainer formcontainer;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Navigation object. Mainly created to have a navigation in the page with all the different classes and samples
   *
   * @param page the container which contains all the objects that will be shown in the link
   */
  public Navigation(FormContainer field, Class class1) {
    // the label and desc. are never used.
    super("Navigation", "");
    this.formcontainer = field;
    this.reqieredClass = class1;

    this.setLongDescription("Please klick on the different icons to jump to the related item.");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#getHTML()
   */
  public String createHTML(int col) {
    StringBuffer buffer = new StringBuffer();

    // a Vector filled with all classes, that were defined in the Consturctir as the requered fields.
    Vector formObjects = findFieldsByClass();

    buffer.append("\n<!-- " + Util.getClassName(this) + " -->\n" + "<tr>\n<td colspan=\"" + (col) + "\">");

    //buffer.append("<table border=\"1\"><tr><td colspan=\"" + (col - 1) + "\"rowspan=\"" + (formObjects.size() + 1) + "\"></td><td></td></tr>");
    buffer.append("<table width=\"100%\" " + WebConstants.BORDER + ">");

    buffer.append(htmlCreateHeader(col));

    // create a back link to each field
    for (int i = 0; i < formObjects.size(); i++) {
      InputField field = (InputField) formObjects.get(i);

      if (field.isActive()) {
        // leave the field small - will be resized 
        buffer.append("<tr><td width=\"3\">");

        buffer.append(field.createLinkedIMG());

        buffer.append("</td><td>");

        buffer.append("\n<!-- " + Util.getClassName(this) + " -->\n");
        buffer.append(field.toStringHTML());

        buffer.append("</td></tr>");
      }
    }

    buffer.append("</table>");
    buffer.append("</td></tr>");

    return buffer.toString();

    /*
       buffer.append("\n<tr><td><a name=\"" + multiField.getName() + "\"/>");
       buffer.append("<!-- number of " + reqieredClass.getName() + ": " + formObjects.size() + " -->");
       for (int i = 0; i < formObjects.size(); i++) {
         try {
           InputField object = (InputField) formObjects.get(i);
           buffer.append(object.createLinkedIMG() + "         " + object.getQuestion() + "  [" + object.toStringHTML() + "]");
         } catch (ClassCastException e) {
           e.printStackTrace();
         }
       }
       buffer.append("</td></tr>\n");
           StringBuffer buffer = new StringBuffer();
           buffer.append("<table><tr><td rowspan=\"" + (multiField.getflength+1) + "\">");
           buffer.append("<b>" + this.getQuestion() + "</b>");
           buffer.append("</td><td></td></tr>");
    
                          // create a back link to each multifield
                          for (int i = 0; i < this.multiField4Clazzs.length; i++) {
                              buffer.append("<tr><td>");
                              MultiField4Clazz multiField4Clazz = this.multiField4Clazzs[i];
                              buffer.append(multiField4Clazz.createLinkedIMG());
                              buffer.append(multiField4Clazz.toStringHTML());
                              buffer.append("</tr></td>");
                        }
                          return buffer.toString();
    
    
                      return buffer.toString();
     */
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#toStringHTML()
   */
  public String toStringHTML() {
    // not needed
    return "";
  }

  /**
   * finds all userinputfields
   *
   * @return
   */
  private Vector findFieldsByClass() {
    Vector vector = new Vector();
    formcontainer.getField(this.reqieredClass, vector);

    return vector;
  }
  
  
  
  

}
