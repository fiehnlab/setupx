/**
 * ============================================================================ File:    LogFileScanner2.java Package: org.setupx.repository.core.communication.importing.logfile cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.importing.logfile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.MappingException;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.hibernate.StaleStateException;
import org.hibernate.criterion.Expression;

import org.setupx.repository.Config;
import org.setupx.repository.core.PersistenceActionUpdateObjectUnknownException;
import org.setupx.repository.core.communication.CommunicationException;
import org.setupx.repository.core.communication.exporting.Machine;
import org.setupx.repository.core.communication.importing.ScanEventHandler;
import org.setupx.repository.core.communication.ldap.AcquisitionFileException;
import org.setupx.repository.core.communication.leco.GCTOF;
import org.setupx.repository.core.communication.notification.NotificationCentral;
import org.setupx.repository.core.communication.notification.SystemNotification;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.logging.Message;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.server.persistence.SXQueryCacheObject;
import org.setupx.repository.server.persistence.SXQueryNonCachedException;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;
import org.setupx.repository.web.forms.inputfield.multi.Sample;


/**
 * Scanner for the logfiles.
 * 
 * <p>
 * create a new instance and that is it. s
 * </p>
 * 
 * <p>
 * It is a thread that pauses itself.
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @todo find a solution for the persistence of the hashsets that contain the ids of scanned and imported samples
 *
 * @hibernate.class table = "scanner"
 * @hibernate.discriminator column = "discriminator"
 */
public class LogFileScanner2 extends Thread {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public Date lastScanDate = new Date();

  /** the directory that is scanned for samplelogs */
  protected File directory;
  protected HashSet cache = new HashSet();

  /** all samplenames that have been scanned already */
  protected Hashtable scannedSamples = new Hashtable();

  /** the instrumenttype which is creating the logfile */
  protected Machine instrument;

  /** handler for events like clazz finished, ... */
  protected ScanEventHandler scanEventHandler = new ScanEventHandler();
  protected Set scannedSamplespersistentSet = new HashSet();

  /** number of samples scanned the last time */
  protected int numberOfScannedSamplesLastTime;

  /** number of miliseconds between each scan. */
  protected int pause;
  private long uoid;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final String DIRECTORY_CLASS_LOG = Config.DIRECTORY_EXPORT + File.separator + "class_status" + File.separator;
  private static final String DIRECTORY_BINBASE_HOTFIX = File.separator + "mnt" + File.separator + "gctof" + File.separator + "binbase" + File.separator + "class" + File.separator;

  /** there is only one instance of the scanner */
  private static LogFileScanner2 singleInstance = null;

  {
    // TODO: add persistence for scanned files
    Util.initXMLParser();
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * instance for testing reasons
   */
  public LogFileScanner2() {
    super();
    this.instrument = new GCTOF();
  }

  /**
   * Creates a new LogFileScanner2.
   * 
   * <p>
   * The Scanner is a thread, which is created once and then stays alive. The Scanner <b>pauses</b> itself after every scan for a defined number of miliseconds.
   * </p>
   *
   * @param pause number of miliseconds the scanner will pause between each of the scans
   * @param directory the directory that will be scanned for files containing sampleIDs
   * @param instrument the type of instrument that the samples are imported from
   *
   * @throws ScannerConfigurationException the extension is not valid
   */
  public LogFileScanner2(int pause, File directory, Machine instrument)
    throws ScannerConfigurationException {
    Logger.log(this, "init ");

    if ((pause == 0) || (directory == null) || (instrument == null)) {
      throw new ScannerConfigurationException("Illegal configuartion.");
    }

    if (singleInstance != null) {
      throw new ScannerConfigurationException("An instance of " + Util.getClassName(this) + " is already working.");
    } else {
      singleInstance = this;
    }

    this.pause = pause;
    this.directory = directory;
    this.instrument = instrument;
    checkExtension();
    this.setName("LogFileScanner");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see java.lang.Thread#run();
   */
  public final void run() {
    NotificationCentral.notify(new SystemNotification("starting " + Util.getClassName(this)));

    for (; true;) {
      Logger.log(this, "scanning NOW - " + Util.getDateString());

      try {
        scan();
      } catch (ScannerException e1) {
        e1.printStackTrace();
      }

      try {
        Logger.log(this, "number of cached entries: " + cache.size());
        Logger.log(this, "sleeping for " + pause + ".");
        this.lastScanDate = new Date();
        this.sleep(pause);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * TODO: 
   *
   * @param args TODO
   *
   * @throws ScannerConfigurationException TODO
   * @throws AcquisitionFileException TODO
   */
  public static void main(String[] args) throws ScannerConfigurationException, AcquisitionFileException {
    new LogFileScanner2(180000, new File(Config.DIRECTORY_LOGFILES_MACHINE), new GCTOF()).run();
  }
  
  

  /**
   * TODO: 
   *
   * @param scannedSamplespersistentSet TODO
   */
  public void setScannedSamplespersistentSet(Set scannedSamplespersistentSet) {
    this.scannedSamplespersistentSet = scannedSamplespersistentSet;
  }

  /**
   * set of samples that are run - which is persistent.
   *
   * @hibernate.set lazy="true" role="scannedlables"
   * @hibernate.collection-key column="scannerlableIDs"
   * @hibernate.collection-one-to-many class = "org.setupx.repository.core.communication.importing.logfile.ScannedPair"
   */
  public Set getScannedSamplespersistentSet() {
    return scannedSamplespersistentSet;
  }

  /**
   * The setter method for Unique Object identifier.
   */
  public void setUOID(long lId) {
    uoid = lId;
  }

  /**
   * The getter method for Unique Object identifier.
   * 
   * <p>
   * The id is generated by the persistence layer.
   * </p>
   *
   * @return unique id for this object
   *
   * @hibernate.id column = "uoid" generator-class="native"
   */
  public long getUOID() {
    return (uoid);
  }

  /**
   * @param sampleName
   * @param sampleID
   */
  public void createScannedPair(String sampleName, int sampleID) {
    ScannedPair pair = new ScannedPair(sampleName, "" + sampleID);

    try {
      pair.update(true);

      Message message = new Message();
      message.setClassName(Sample.class.getName());
      message.setRelatedObjectID(sampleID);
      message.setMessage("finished: " + sampleID + " - " + sampleName + " " + sampleID);
      message.update(true);

      Logger.log(this, " done: " + sampleName);

      // I guess this is redundant - cause below a new object is created which replaces the deprecated one!
      try {
        SXQueryCacheObject.findBySampleID(Integer.parseInt(pair.getSampleID())).remove();
        Logger.log(this, " cleaned cache ");
      } catch (SXQueryNonCachedException e) {
        Logger.log(this, " not in cache ");
      }

      try {
        long promtID = new SXQuery().findPromtIDbySample(Long.parseLong("" + pair.getSampleID()));
        int numberOfSamples = new SXQuery().determineSamplesFinishedByPromtID(promtID);
        new SXQueryCacheObject(promtID, numberOfSamples);
        Logger.log(this, " cache updated");
      } catch (Exception e) {
        e.printStackTrace();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
/*
 * (non-Javadoc)
 * @see java.lang.Thread#toString()
 */
  public String toString() {
    return ("" + this.cache.size());
  }

  /**
   * check the extension of the logfile.
   *
   * @throws ScannerConfigurationException the fileextension is illeagal
   */
  private final void checkExtension() throws ScannerConfigurationException {
    if (instrument.getFileExtension() == null) {
      throw new ScannerConfigurationException(instrument.getFileExtension());
    }

    if (instrument.getFileExtension().length() < 3) {
      throw new ScannerConfigurationException(instrument.getFileExtension());
    }

    if (instrument.getFileExtension().indexOf(".") != -1) {
      throw new ScannerConfigurationException(instrument.getFileExtension());
    }
  }

  /**
   * creates a report of the current status of the scanner.
   *
   * @return the report in ascii text
   */
  private String createReport() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("\n   Number of Samples meassuered:   " + this.scannedSamples.size());

    StringBuffer filebuffer = new StringBuffer();
    filebuffer.append(buffer);

    Enumeration enumeration = this.scannedSamples.keys();
    buffer.append("\n   samples : \n");

    while (enumeration.hasMoreElements()) {
      String sampleName = (String) enumeration.nextElement();
      filebuffer.append("\n " + sampleName);
    }

    try {
      org.setupx.repository.core.util.File.storeData2File(new File(Config.DIRECTORY_MAIN + "scanner_report.log"), filebuffer.toString());
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return buffer.toString();
  }

  /**
   * Scan a directory for files containing sampleIDs.
   * 
   * <p>
   * File will be checked for samples that match the fileextension in the instrument
   * </p>
   *
   * @throws ScannerException unable to scan
   */
  private final void scan() throws ScannerException {
    try {
      this.update();
    } catch (Exception e1) {
      throw new ScannerException(e1);
    }

    // get all files from scan folder
    File[] files = this.directory.listFiles();
    
    if (files == null) throw new ScannerException("unable to scan - no files in sourcedirectory " + this.directory.getName() + "(" + this.directory.getAbsolutePath() + ")");
    Logger.log(this, "scanning files:" + files.length);

    for (int i = 0; files != null && i < files.length; i++) {
      // check every file
      if (!files[i].isDirectory()) {
        // check extension
        String ext = org.setupx.repository.core.util.File.getExtension(files[i]);

        if (ext.compareTo(instrument.getFileExtension()) == 0) {
          // scan that file 
          Iterator iterator = scan(files[i]).samples();
          org.hibernate.Session hqlSession = PersistenceConfiguration.createSessionFactory().openSession();

          while (iterator.hasNext()) {
            String element = (String) iterator.next();

            // check if it exists in the database already
            // if (element.startsWith("0611")){
            if (true && !cache.contains(element)) {
              cache.add(element);

              //List list = hqlSession.createCriteria(ScannedPair.class).add(Expression.eq("label", element)).add(Expression.ne("sampleID", "")).list();
              List list = hqlSession.createCriteria(ScannedPair.class).add(Expression.eq("label", element)).list();

              // the sample is found for the first time.
              if (list.size() < 1) {
                Logger.log(this, "---------------------------------------------------");
                Logger.log(this, " finished: " + element);

                int sampleID = 0;
                String label = "";

                if (element.indexOf(':') != -1) {
                  label = element.substring(0, element.indexOf(':'));
                }

                try {
                  sampleID = new SXQuery().findSampleIDByAcquisitionName(label);
                } catch (PersistenceActionFindException e) {
                  // checking for lecologfiles
                  try {
                    sampleID = new SXQuery().findSampleIDByAcquisitionNameFILEBASED(label);
                  } catch (AcquisitionFileException e1) {
                    Logger.warning(this, "no sampleID for " + label);

                    ScannedPair pair = new ScannedPair(element, "");

                    try {
                      pair.update(true);
                    } catch (PersistenceActionUpdateException e2) {
                      e2.printStackTrace();
                    }

                    Message message = new Message();
                    message.setClassName(Sample.class.getName());
                    message.setRelatedObjectID(0);
                    message.setMessage("unknown sample: " + sampleID + " - " + element + " " + sampleID);

                    try {
                      message.update(true);
                    } catch (PersistenceActionUpdateException e2) {
                    }
                  }
                } catch (org.hibernate.QueryException e1) {
                  Logger.warning(this, e1.getMessage());
                  e1.printStackTrace();
                  NotificationCentral.notify(new SystemNotification("sample was finished but can not be identified as setupx sample: " + element));
                }

                if (sampleID > 10) {
                  createScannedPair(element, sampleID);
                }
              }
            } else {
            }
          }
        }
      }
    }

    if (this.scannedSamples.size() != numberOfScannedSamplesLastTime) {
      Logger.mail(this, this.scannedSamples.size() - this.numberOfScannedSamplesLastTime + " new samples found.");
    }

    this.numberOfScannedSamplesLastTime = this.scannedSamples.size();
  }

  /**
   * creates a new instance of a logfile.
   *
   * @param file file to be scanned
   *
   * @return instance containing all samples in that specific logfile
   *
   * @throws ScannerException unable to scan
   */
  private final LogFile scan(File file) throws ScannerException {
    try {
      return new LogFile(file, this.instrument);
    } catch (IOException e) {
      throw new ScannerException(e);
    } catch (ParserException e) {
      throw new ScannerException(e);
    }
  }

  /**
   * Take a single sample, check if its whole clazz is done.
   * 
   * <p>
   * <B>Import of samples on BB are only activated when the whole class is complete</B>
   * </p>
   *
   * @param label ACQ label of the sample
   *
   * @return if the sample has been imported or not
   */
  private boolean mergeSample(final String reallabel) {
    // get the sample ID
    int sampleID;
    String label = "";

    if (reallabel.indexOf(':') != -1) {
      label = reallabel.substring(0, reallabel.indexOf(':'));
    }

    try {
      sampleID = new SXQuery().findSampleIDByAcquisitionName(label);
    } catch (PersistenceActionFindException e) {
      // checking for lecologfiles
      try {
        sampleID = new SXQuery().findSampleIDByAcquisitionNameFILEBASED(label);
      } catch (AcquisitionFileException e1) {
        //Logger.mailWarning(this, this.instrument.toString(), "sample not known in setupX and the samplelog : " + label);
        return false;
      }
    }

    Logger.debug(this, "mergeSample - sampleID: " + sampleID);
    this.scannedSamples.put(reallabel, "" + sampleID);
    this.scannedSamplespersistentSet.add(new ScannedPair(reallabel, "" + sampleID));

    Message message = new Message();
    message.setClassName(Sample.class.getName());
    message.setRelatedObjectID(sampleID);
    message.setMessage("finished: " + sampleID + " - " + reallabel);

    try {
      message.update(true);
    } catch (PersistenceActionUpdateException e2) {
      e2.printStackTrace();
    }

    int clazzID;

    try {
      clazzID = new SXQuery().findClazzIDBySampleID(sampleID);
    } catch (PersistenceActionFindException e1) {
      Logger.log(this, e1);
      e1.printStackTrace();

      return false;
    }

    Logger.debug(this, "mergeSample - clazzID: " + clazzID);

    // list of sampleIDs that do belong to the same class
    List sampleID4Clazzs = new SXQuery().findSampleIDsByClazzID(clazzID);
    Logger.debug(this, "mergeSample - number of samples in clazz " + clazzID + " : " + sampleID4Clazzs.size());

    // data going to status file
    StringBuffer stringFile = new StringBuffer();

    // file being created for

    /**
     * @deprecated only for the old BinBase Version
     */
    StringBuffer stringBBFile = new StringBuffer();
    stringFile.append("\nnumber of samples in clazz " + clazzID + " : " + sampleID4Clazzs.size() + "\n");

    // get the other samples for that clazz
    String[] sampleACQNames = new String[sampleID4Clazzs.size()];

    for (int i = 0; i < sampleID4Clazzs.size(); i++) {
      String sID = sampleID4Clazzs.get(i) + "";
      Logger.debug(this, "looking for acqname of sample(" + i + "):" + sID + "");

      if (this.scannedSamples.containsValue("" + sID)) {
        Enumeration acqnames = scannedSamples.keys();
        boolean loop = true;

        while (loop && acqnames.hasMoreElements()) {
          String acqName = (String) acqnames.nextElement();
          String temp_sID = (String) scannedSamples.get(acqName);

          /*
             if (temp_sID.length() != 0) {
               Logger.debug(this, "SEARCH: acqname: " + acqName + " sampleID: \"" + temp_sID + "\" vs. \"" + sID + "\" " + temp_sID.compareTo(sID));
             }
           */
          if (temp_sID.compareTo(sID) == 0) {
            sampleACQNames[i] = acqName;
            loop = false;
            Logger.debug(this, "FOUND: acqname of sample(" + i + "):" + sID + " is: " + acqName);
          }
        }
      } else {
        stringFile.append("\nfile not run yet: " + sID + "\n");
      }
    }

    // check if clazz is complete
    Logger.log(this, "checking consistence of clazz: " + clazzID);

    boolean complete = true;

    for (int i = 0; i < sampleACQNames.length; i++) {
      Logger.log(this, "checking sample " + sampleACQNames[i] + " in clazz: " + clazzID);

      // check if this sample has been scanned
      if ((sampleACQNames[i] != null) && (sampleACQNames[i].compareTo("null") != 0) && (this.scannedSamples.containsKey(sampleACQNames[i]))) {
        // ok
        //Logger.log(this, "Clazz: " + clazzID + " is complete. ");
      } else {
        // clazz is incomplete
        Logger.log(this, "Clazz: " + clazzID + " is incomplete. ");
        complete = false;
      }
    }

    //for
    String extension = "";

    if (complete) {
      extension = "_done";
    }

    for (int j = 0; j < sampleACQNames.length; j++) {
      stringFile.append("\nSample " + j + "/" + sampleACQNames.length + " in Clazz(" + clazzID + "): " + sampleACQNames[j]);

      if (sampleACQNames[j] != null) {
        stringBBFile.append(sampleACQNames[j] + "\t" + clazzID + "\n");
      }
    }

    /*
       try {
         new File(DIRECTORY_CLASS_LOG + clazzID + ".txt").delete();
         org.setupx.repository.core.util.File.storeData2File(new File(DIRECTORY_BINBASE_HOTFIX + clazzID + ".txt"), stringBBFile.toString());
         org.setupx.repository.core.util.File.storeData2File(new File(DIRECTORY_CLASS_LOG + clazzID + extension + ".txt"), stringFile.toString());
       } catch (Exception e) {
         e.printStackTrace();
       }
     */
    if (complete) {
      Logger.log(this, "Clazz: " + clazzID + " is complete. ");

      // ok - all samples are present
      // import clazz
      try {
        scanEventHandler.clazzCompleted(clazzID, sampleACQNames);
      } catch (CommunicationException e) {
        Logger.warning(this, "error binbase", e);
        // TODO: handle exception
      }
    }

    return complete;
  }

  /**
   * TODO: 
   *
   * @throws PersistenceActionUpdateObjectUnknownException TODO
   * @throws PersistenceActionUpdateException TODO
   * @throws RuntimeException TODO
   */
  private synchronized void update() throws PersistenceActionUpdateObjectUnknownException, PersistenceActionUpdateException {
    Session session = PersistenceConfiguration.createSessionFactory().openSession();
    this.update(session);
    session.close();

    if (this.uoid == 0) {
      throw new RuntimeException("uoid is not valid.");
    }
  }

  /**
   * update an object in the database
   */
  private synchronized final void update(Session s) throws PersistenceActionUpdateObjectUnknownException, PersistenceActionUpdateException {
    org.hibernate.Transaction transaction = s.beginTransaction();

    try {
      if (this.uoid == 0) {
        s.save(this);
      } else {
        s.update(this);
      }

      Set set = this.getScannedSamplespersistentSet();
      Iterator iterator = set.iterator();

      while (iterator.hasNext()) {
        ScannedPair pair = (ScannedPair) iterator.next();

        if (pair.getUOID() != 0) {
          s.update(pair);
        } else {
          s.save(pair);
        }
      }

      transaction.commit();
      Logger.debug(this, "this.update(session) .. done.");
    } catch (MappingException e) {
      throw new RuntimeException("Configuration problem: Object is unknow for the mapping - check if hibernate-mapping-file for " + org.setupx.repository.core.util.Util.getClassName(this) + " exists.", e);
    } catch (NonUniqueObjectException e) {
      throw new PersistenceActionUpdateObjectUnknownException(e);
    } catch (StaleStateException e) {
      throw new PersistenceActionUpdateObjectUnknownException(e);
    } catch (Exception e) {
      throw new PersistenceActionUpdateException(this, e);
    }
  }
}
