package org.setupx.repository.core.communication.export;

public class XMLConvertException extends Exception {

    public XMLConvertException(String string, Exception e) {
        super(string, e);
    }

}
