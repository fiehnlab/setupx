/**
 * ============================================================================ File:    FilescannerAbstractThread.java Package: org.setupx.repository.core.communication.importing.filescanner cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.importing.filescanner;

import org.setupx.repository.core.InitException;
import org.setupx.repository.core.communication.importing.FileInfo;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;

import java.io.File;

import java.util.Hashtable;
import java.util.Vector;


/**
 * Scanning one directory in an interval and is looking for new files.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 */
public abstract class FilescannerAbstractThread extends Thread {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private File directoryToScan = null;
  private Hashtable lastScanedFiles = new Hashtable();
  private boolean lookingForNewFiles = true;
  private boolean lookingForUpdatedFiles = true;
  private long timeBetweenScan = 999;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new FilescannerAbstractThread object.
   *
   * @param i_directoryToScan 
   * @param timeBetweenScanning 
   *
   * @throws InitException 
   */
  public FilescannerAbstractThread(File i_directoryToScan, long timeBetweenScanning)
    throws InitException {
    Logger.log(this, "init");
    this.directoryToScan = i_directoryToScan;
    this.timeBetweenScan = timeBetweenScanning;
  }

  private FilescannerAbstractThread() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * abstract method beeing executed when a new or modifiued file is found
   *
   * @param modifiedFiles all the files that were added or modified in the specific directory
   *
   * @throws DocumentUpdateException
   */
  public abstract void action(Vector modifiedFiles) throws FileScannerException;

  /**
   * TODO: 
   *
   * @throws
   */
  public void run() {
    Logger.log(this, "run");

    for (; true;) {
      Logger.log(this, "scanning NOW - " + Util.getDateString());

      try {
        scan();
      } catch (FileScannerException e1) {
        e1.printStackTrace();
      }

      try {
        Thread.sleep(timeBetweenScan);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * checking the file-array for new or mod ones.
   *
   * @param i_files all files to check
   *
   * @return all files that are new or modi
   */
  private Vector check4Diff(File[] i_files) {
    Vector modifiedFiles = new Vector();

    for (int i = 0; i < i_files.length; i++) {
      File file = i_files[i];
      boolean aNewOne = false;

      // the file did already exist -- so looking for differences if activated
      if (lastScanedFiles.containsKey(file.getName()) && lookingForUpdatedFiles) {
        // checking for differences
        FileInfo oldVersion = (FileInfo) lastScanedFiles.get(file.getName());

        long modifiedOld = oldVersion.lastModified();
        long modifiedNew = file.lastModified();

        long sizeOld = oldVersion.length();
        long sizeNew = file.length();

        // only if all the parameters are the same the file is still the old one - otherwise add it to the table
        if ((sizeNew == sizeOld) && (modifiedNew == modifiedOld)) {
          //
        } else {
          Logger.log(this, "it has been modified");
          aNewOne = true;
        }
      } else {
        // it is an unknown file
        aNewOne = true;
      }

      // when it is a new one add it to the hashtable
      if (aNewOne) {
        lastScanedFiles.put(file.getName(), new FileInfo(file));
        modifiedFiles.add(file);
      }
    }

    return modifiedFiles;
  }

  /**
   * TODO: 
   *
   * @throws DocumentUpdateException
   * @throws InitException TODO
   */
  private void scan() throws FileScannerException {
    // checking
    if ((directoryToScan == null) || !directoryToScan.isDirectory()) {
      new FileScannerException("unable to start scanning cause directory (" + directoryToScan + ") is invalid").printStackTrace();
    }

    // looking for all files in the directory
    File[] files = directoryToScan.listFiles();

    // compare this to the last scan
    Vector modFiles = check4Diff(files);

    // if difference was found - start action
    if (modFiles.size() != 0) {
      action(modFiles);
    }
  }
}
