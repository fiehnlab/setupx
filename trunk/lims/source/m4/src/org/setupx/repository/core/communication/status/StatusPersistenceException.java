package org.setupx.repository.core.communication.status;


public class StatusPersistenceException extends StatusException {

    public StatusPersistenceException(Exception e) {
        super(e);
    }

}
