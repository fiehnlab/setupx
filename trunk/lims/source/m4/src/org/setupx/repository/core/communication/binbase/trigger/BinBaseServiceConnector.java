/**
 * ============================================================================ 
 * File:    BinBaseServiceConnector.java 
 * Package: org.setupx.repository.core.communication.binbase.trigger 
 * cvs:     $Revision: 1.4 $ 
 *          $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ 
 * Martin Scholz  
 * Copyright (C) 2005 Martin Scholz. 
 * All rights reserved. 
 * For more information please 
 * see http://fiehnlab.ucdavis.edu/ 
 * ============================================================================
 */
package org.setupx.repository.core.communication.binbase.trigger;

import java.net.URL;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.setupx.repository.Config;
import org.setupx.repository.ConfigurationException;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.CommunicationException;
import org.setupx.repository.core.communication.ServiceNotAvailableException;
import org.setupx.repository.core.communication.leco.ColumnInformation;
import org.setupx.repository.core.communication.status.ImportedEventStatus;
import org.setupx.repository.core.communication.status.PersistentStatus;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.logging.Message;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.web.forms.inputfield.multi.Sample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.webservice.BinBaseService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.webservice.BinBaseServicePortSoapBindingStub;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.webservice.BinBaseServiceServiceLocator;
import edu.ucdavis.genomics.metabolomics.exception.CommunicationExcpetion;

import javax.xml.rpc.ServiceException;
import org.setupx.repository.core.communication.status.*;

/**
 * Connection to BinBase.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class BinBaseServiceConnector extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private BinBaseService stub;
  private URL url;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** active or deactivated the whole connection to the binbasesystem */
  public static final boolean ACTIVE = true;
  public static URL URL_DEVELOPMENT1;
  public static URL URL_DEVELOPMENT2;
  public static URL URL_PRODUCTION1;
  public static URL URL_PRODUCTION2;

  static {
    try {
      URL_DEVELOPMENT1 = Util.createURL("http://128.120.136.19:8080/BCI/BinBaseService");
      URL_DEVELOPMENT2 = Util.createURL("http://128.120.136.154:8080/BCI/BinBaseService");
      URL_PRODUCTION1 = Util.createURL("http://128.120.136.203:8080/BCI/BinBaseService");
      URL_PRODUCTION2 = Util.createURL("http://128.120.136.17:8080/BCI/BinBaseService");
    } catch (ConfigurationException e) {
      err(BinBaseServiceConnector.class, e);
    }
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new BinBaseTrigger object.
   */
  public BinBaseServiceConnector(final URL url) {
    super();

    this.url = url;

    try {
      this.stub = this.createStub();
    } catch (ServiceException e) {
      err("unable to init connection to BinBase.");
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param classSamples a hashset filled with Strings for the samples
   *
   * @return an array of samples
   */
  public static final ExperimentSample[] createSamples(HashSet classSamples) {
    ExperimentSample[] experimentSamples = new ExperimentSample[classSamples.size()];
    int i = 0;

    for (Iterator iter = classSamples.iterator(); iter.hasNext();) {
      String name = (String) iter.next();
      Logger.debug(null, "creating experimentsample " + name);
      experimentSamples[i] = new ExperimentSample(name, name);
      i++;
    }

    return experimentSamples;
  }

  /**
   * create experiment samples.
   *
   * @param sampleACQNames
   *
   * @return TODO
   */
  public static final ExperimentSample[] createSamples(String[] sampleACQNames) {
    ExperimentSample[] experimentSamples = new ExperimentSample[sampleACQNames.length];

    for (int i = 0; i < sampleACQNames.length; i++) {
      String name = sampleACQNames[i];
      experimentSamples[i] = new ExperimentSample(name, name);
    }

    return experimentSamples;
  }

  /**
   * combined export and <strong>import</strong> for a classset.
   *
   * @param classSet
   *
   * @throws CommunicationException
   */
  public final void launchExport(ClassSet classSet) throws CommunicationException {
      warning(this, "TODO: add a funciton to determine if a class has been imported.");
      this.launchExport(classSet, false);  // <------------------
  }

  /**
   * start the export of an whole experiment in the binbaseside
   * 
   * <p>
   * <b>includes import of ALL classes</b>
   * </p>
   *
   * @param experiment TODO
   *
   * @throws CommunicationException TODO
   */
  public final void launchExport(ClassSet classSet, boolean includeImport)
    throws CommunicationException {
      Logger.debug(this, "launchExport(ClassSet classSet " + classSet.getClasses().length + " , boolean includeImport " + includeImport + "  )");
    try {
      ExperimentClass[] classes = classSet.getClasses();

      for (int i = 0; i < classes.length; i++) {
        ExperimentClass class1 = classes[i];
        class1.setColumn(classSet.getColumn());
        class1.setIncrease(0);
        checkClassImported(class1.getId());

        // starting import
        if (includeImport) {
          this.launchImport(class1);
          Logger.debug(this, "-->>  binbase connection : start import class : " + classSet.getId() + "   " + classSet.getClasses().length + " clazzes. <<-- <<--");
        }
      }

      Logger.debug(this, "-->>  binbase connection : start exporting : " + classSet.getId() + "   " + classSet.getClasses().length + " clazzes. <<-- <<--");

      this.stub.triggerExport(classSet);
      
      for (int k = 0; k < classes.length; k++){
          ExperimentClass clazZ = classes[k];
          edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample[] samples = clazZ.getSamples();
          for (int i = 0; i < samples.length; i++) {
              edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample sample = samples[i];
              PersistentStatus status= new ExportedEventStatus(Long.parseLong(sample.getId()), null, "sample " + sample.getId() + "(" + sample.getName() + " has been exported. Column: " + classSet.getColumn());
              status.update(true);
          }
      }

      new Message(this, "Classset has been exported.");
    } catch (CommunicationExcpetion e) {
      throw new CommunicationException("Export can not be launched. BinBase is offline.", e);
    } catch (RemoteException e) {
      throw new CommunicationException("Export can not be launched. BinBase is offline.", e);
    } catch (Exception e) {
      throw new CommunicationException(e);
    }
  }

  /**
   * checks if a specific class has been imported
   *
   * @param id the id of the class
   *
   * @return true if all samples of this class have been imported - false if it was not completely imported.
   *
   * @throws CommunicationException
   */
  public boolean checkClassImported(String id) throws CommunicationException {
    warning(this, "BinBase Error: unsupported method on binbase - check if the class has been completely imported. ");

    // this.stub.checkImport(id);
    return false;
  }

  /**
   * notifies binbase that the class can be imported.
   *
   * @param experimentClass
   *
   * @throws CommunicationException
   */
  public final void launchImport(ExperimentClass experimentClass)
    throws CommunicationException {
    try {
      Logger.debug(this, "-->>  binbase connection : start importing class: " + experimentClass.getId() + "   " + experimentClass.getSamples().length + " <<-- <<--");
      Logger.debug(this, "connection: " + stub.toString());

      experimentClass.setIncrease(0);

      if (experimentClass != null && experimentClass.getSamples() != null && experimentClass.getSamples().length > 0) {
          
          this.stub.triggerImportClass(experimentClass);
          
          ExperimentSample[] samples = experimentClass.getSamples();
          for (int i = 0; i < samples.length; i++) {
              edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample sample = samples[i];
              PersistentStatus status = new ImportedEventStatus(Long.parseLong(sample.getId()), null, "sample " + sample.getId() + "(" + sample.getName() + " has been exported");
              status.update(true);
          }
      }
      new Message(this, "Class " + experimentClass.getId() + " launched for Import. Number of samples in Class: " + experimentClass.getSamples().length).update(true);
    } catch (Exception e) {
      throw new CommunicationException(e);
    }
  }

  /**
   * mapping
   *
   * @param classes TODO
   *
   * @return TODO
   */
  public static final ExperimentClass[] map(List classes) {
    ExperimentClass[] experimentClasses = new ExperimentClass[classes.size()];
    int i = 0;

    for (Iterator iter = classes.iterator(); iter.hasNext();) {
      ExperimentClass experimentClass = (ExperimentClass) iter.next();
      experimentClasses[i] = experimentClass;
      i++;
    }

    return experimentClasses;
  }

  /**
   * TODO: 
   *
   * @param sampleIDs TODO
   *
   * @return TODO
   *
   * @throws PersistenceActionFindException TODO
   */
  public static ExperimentSample[] createSamplesBySampleIDs(final List sampleIDs)
    throws PersistenceActionFindException {
    ExperimentSample[] experimentSamples = new ExperimentSample[sampleIDs.size()];
    Iterator iterator = sampleIDs.iterator();
    int i = 0;

    while (iterator.hasNext()) {
      long sampleID = Long.parseLong("" + iterator.next());
      String sampleName = new SXQuery().findAcquisitionNameBySampleID(sampleID);
      experimentSamples[i] = new ExperimentSample("" + sampleID, sampleName);
      i++;
    }

    return experimentSamples;
  }

  /**
   * TODO: 
   *
   * @param classIDs TODO
   *
   * @return TODO
   *
   * @throws PersistenceActionFindException TODO
   */
  public static ExperimentClass createSuperClass(List classIDs)
    throws PersistenceActionFindException {
    ExperimentClass experimentClass = new ExperimentClass();
    experimentClass.setIncrease(-1);
    experimentClass.setId("MERGE_" + classIDs.toString());

    Iterator iterator = classIDs.iterator();
    HashSet acqnames = new HashSet();
    int _counter = 0;

    while (iterator.hasNext()) {
      int classID = Integer.parseInt("" + iterator.next());
      Iterator samplesIDIter = new SXQuery().findSampleIDsByClazzID(classID).iterator();

      while (samplesIDIter.hasNext()) {
        long sampleID = Long.parseLong("" + samplesIDIter.next());
        String sampleACQNAME = new SXQuery().findAcquisitionNameBySampleID(sampleID);
        Logger.debug(null, _counter + " " + classID + " " + sampleACQNAME);
        acqnames.add(sampleACQNAME);
      }
    }

    experimentClass.setSamples(createSamples(acqnames));

    return experimentClass;
  }

  /**
   * TODO: 
   *
   * @param exportLabel TODO
   * @param promtLs TODO
   * @param $paramType$ TODO
   *
   * @throws ServiceNotAvailableException TODO
   */
  public final void exportClassesCombined(String exportLabel, long[] promtLs, ColumnInformation columnInformation)
    throws ServiceNotAvailableException {
    HashSet classes = new HashSet();

    for (int i = 0; i < promtLs.length; i++) {
      long l = promtLs[i];
      classes.addAll(new SXQuery().findClazzIDsByPromtID(l));
    }

    Object[] classLs = classes.toArray();

    this.launchExport(exportLabel, classLs, columnInformation);
  }

  /**
   * import a whole clazz of samples
   *
   * @param clazzID the id of the clazz
   * @param sampleACQNames names used in the acquisitiontask and by the label
   *
   * @throws CommunicationException
   */
  public void importClazz(int clazzID, String[] sampleACQNames)
    throws CommunicationException {
    Logger.debug(this, "importing clazzID: " + clazzID);
    Logger.mail(this, "clazzID: " + clazzID + " complete.");

    ExperimentSample[] experimentSamples = BinBaseServiceConnector.createSamples(sampleACQNames);
    ExperimentClass experimentClass = new ExperimentClass();
    experimentClass.setId("" + clazzID);
    experimentClass.setIncrease(0);
    experimentClass.setSamples(experimentSamples);

    try {
      this.createBBConnection().launchImport(experimentClass);
    } catch (CommunicationException e) {
      throw new CommunicationException("unable to start import on BinBase:", e);
    }
  }

  /**
   * exports a set of classes in one Export. There is <strong>no import</strong> included!
   *
   * @param classLs
   *
   * @throws ServiceNotAvailableException
   * @throws ConnectExceptionException
   * @throws CommunicationException
   * @throws
   */
  public void launchExport(String exportLabel, Object[] classLs, ColumnInformation columnInformation)
    throws ServiceNotAvailableException {
    // create a new classset 
    ClassSet classSet = new ClassSet(columnInformation);

    ExperimentClass[] classes = new ExperimentClass[classLs.length];

    for (int i = 0; i < classLs.length; i++) {
      long classID = Long.parseLong("" + classLs[i]);
      List sampleIDs = new SXQuery().findSampleIDsByClazzID((int) classID);

      ExperimentSample[] samples;

      try {
        samples = createSamplesBySampleIDs(sampleIDs);
      } catch (PersistenceActionFindException e) {
        throw new ServiceNotAvailableException("unable to launch export - sample: " + sampleIDs + " can not be found in setupx.", e);
      }

      classes[i] = new ExperimentClass(columnInformation.getBbid(), "" + classID, -1, -1, samples);
    }

    // assign the real classes to the classset
    classSet.setClasses(classes);
    classSet.setId(exportLabel);

    // export it
    try {
      launchExport(classSet, true);
    } catch (CommunicationException e) {
      throw new ServiceNotAvailableException(e);
    }
  }

  /**
   * create a stubs
   *
   * @throws ServiceException TODO
   */
  protected BinBaseService createStub() throws ServiceException {
    if (ACTIVE) {
      BinBaseServiceServiceLocator baseServiceServiceLocator = new BinBaseServiceServiceLocator();

      return (BinBaseServicePortSoapBindingStub) baseServiceServiceLocator.getBinBaseServicePort(determineTarget());
    }

    throw new ServiceException("BinBase Connection is deactivated.");
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  private final URL determineTarget() {
    return this.url;
  }

  /**
   * creates a BinBaseConnection
   *
   * @return the binbaseconnection
   */
  private BinBaseServiceConnector createBBConnection() {
    return new BinBaseServiceConnector(Config.BINBASE_SERVER);
  }
}
