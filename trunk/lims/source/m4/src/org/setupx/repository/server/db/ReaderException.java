package org.setupx.repository.server.db;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class ReaderException extends Exception {
  public static final String CVS_REVISION = "$Revision: 1.1 $";

  /**
   * Creates a new ReaderException object.
   */
  public ReaderException() {
    super();
  }

  /**
   * Creates a new ReaderException object.
   *
   * @param arg0 
   */
  public ReaderException(String arg0) {
    super(arg0);
  }

  /**
   * Creates a new ReaderException object.
   *
   * @param arg0 
   */
  public ReaderException(Throwable arg0) {
    super(arg0);
  }

  /**
   * Creates a new ReaderException object.
   *
   * @param arg0 
   * @param arg1 
   */
  public ReaderException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }
}
