package org.setupx.repository.core.communication.status;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import org.setupx.repository.server.logging.Message;
import org.setupx.repository.server.persistence.SXQuery;


/**
 * Status declaring that a Sample has been run.
 * 
 * @author scholz
 *
 */
public class SampleRunStatus extends DynamicStatus implements Timestamped{

    private String label;
    private Date date;


    public SampleRunStatus(long sampleUOID, String label, Date date) {
        super(sampleUOID);
        this.label = label;
        this.date = date;
        
    }

    public String getMessage() {
        return "Sample " + this.sampleID + " ran successful using the label " + this.label;
    }

    public String createDisplayString() {
        return "successful run"; 
    }
    
    
    public static Collection find(final long sampleUOID, Session hqlSession) {
        Vector result = new Vector();
        
        // get the actual runs
        Iterator lableIDIterator = new SXQuery().findScannedPair(sampleUOID).iterator();

        while (lableIDIterator.hasNext()) {

            // label used for this run
            String label = "" + lableIDIterator.next();
            debug(null, sampleUOID + " ran as " + label);
             
            // time when the sample ran
            List list = hqlSession.createCriteria(Message.class).add(Expression.eq("relatedObjectID", new Long(sampleUOID))).addOrder(Order.asc("date")).list();
            Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                Message message = (Message) iterator.next();
                if (message.getMessage().indexOf(label) > 0)
                result.add(new SampleRunStatus(sampleUOID, label, message.getDate()));
            }
        }
        return result;
    }

    
    public Date getDate() {
        return this.date;
    }
}
