package org.setupx.repository.core.communication.msi;

import java.util.List;

import org.hibernate.Hibernate;

import org.setupx.repository.core.CoreObject;

public class MetaDataProvider extends CoreObject {
    
    /**
     * 
     * NOT TESTED
     * find custom metadata 
     */
    public static List findCustomMetadata(){
        String pubQuery = "select question, experiment from formobject,  samples where description = \"custom field\"  and question != \"\" and samples.sampleID = formobject.PARENT group by question order by question";

        List pubList = CoreObject.createSession()
                        .createSQLQuery(pubQuery)
                        .addScalar("question", Hibernate.STRING)
                        .addScalar("experiment", Hibernate.LONG)
                        .list();
        
        return pubList;
    }
    
    
}
