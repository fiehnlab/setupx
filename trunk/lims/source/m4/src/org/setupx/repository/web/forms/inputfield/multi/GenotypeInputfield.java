/**
 * ============================================================================ File:    GenotypeInputfield.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.core.util.hotfix.oldsamples.OldClassTemplate;
import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;


/**
 * @hibernate.subclass
 */
public class GenotypeInputfield extends MultiField4ClazzSimple {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new GenotypeInputfield object.
   *
   * @param question 
   * @param description 
   */
  public GenotypeInputfield(String question, String description) {
    super(question, description);

    StringInputfield inputfield = new StringInputfield("Genotype", "type in the name of the genotype");
    this.addField(inputfield);
  }

  /**
   * Creates a new GenotypeInputfield object.
   */
  public GenotypeInputfield() {
      super("","");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getPath()
   */
  public String getPath() {
    return OldClassTemplate.LINE;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getSingleValueField()
   */
  public FormObject getSingleValueField() {
    return this.getField(0);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#cloneField()
   */
  public InputField cloneField() throws CloneException {
    return new GenotypeInputfield(this.getQuestion(), this.getDescription());
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.clazzes.ClazzInformation#createShortLabel()
   */
  public String createShortLabel() {
    return this.toStringHTML();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#toStringHTML()
   */
  public String toStringHTML() {
    return this.getQuestion() + ": " + ((InputField) this.getField(0)).getValue();
  }
  
  /*
  public void setActive(boolean _active){
        if (_active != super.isActive()) {
            try {
                throw new Exception("changing ACTIVE to : " + _active + "!!!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //if (!_active) throw new RuntimeException("NO!!");
        
        super.setActive(_active);
        try {
            if (this.isActive() != _active)
                throw new Exception("status was not changed!!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    */
}
