package org.setupx.repository.core.util.print;

import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.print.PrintService;
import javax.swing.JTextArea;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class DYMOLabelPrintConnector4XLS extends DYMOLabelPrintConnector {
    private static final int lablesPerPage = 4;
    private String[] sampleIDs;
    boolean finished = false;
    private String[] sampleLables;
    private String header;
    private String labname;
    private File sourceFile;
    private static JTextArea myLog = null;


    public DYMOLabelPrintConnector4XLS(File file, String labname, String operator, boolean printit){
        this(file, labname, operator, printit, false, null);
    }

    public DYMOLabelPrintConnector4XLS(File file, String labname, String operator, boolean printit, boolean showprintmenu, JTextArea _log) {
        myLog = _log;
        PRINTMENU = showprintmenu;
        
        log("using printmenu: " + showprintmenu);
        this.sourceFile = file;
        
        try {
            read();
        } catch (IOException e) {
            throw new RuntimeException("unable to start case file \"" + this.sourceFile + "\" can not be found.");
        }
        this.labname = labname;
        this.header = operator;
        
        if (printit) {
            printIt(this.sourceFile, this.labname, this.header);
        }
    }

    private static void log(String string) {
        if (myLog != null){
            myLog.append(string + "\n");
        } else {
            System.out.println(string);
        }
    }

    private void read() throws IOException {
        FileInputStream fileInputStream = new FileInputStream(this.sourceFile);
        HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);

        log("read  xls workbook");

        HSSFSheet sheet = workbook.getSheetAt(0);
        log("got sheet");
        this.sampleIDs = new String[sheet.getLastRowNum() +1];
        this.sampleLables = new String[sheet.getLastRowNum() +1];

        log("got " + sheet.getLastRowNum() + " entries");
        
        for (int rowCount  = sheet.getFirstRowNum(); rowCount <= sheet.getLastRowNum(); rowCount++){
            HSSFRow row = sheet.getRow(rowCount);
            if (row != null){
                try {
                    short pos = 0;
                    log("pos: " + pos +"-" + rowCount);
                    
                    this.sampleIDs[rowCount] = getCellValue(row.getCell(pos));
                    this.sampleLables[rowCount] = getCellValue(row.getCell((short)(pos+1)));

                    log("got values: " + this.sampleIDs[rowCount] + ": " + this.sampleLables[rowCount] );
                } catch (Exception e) {
                    log("err: " + e.getMessage());
                }
            } else {
                
            }
        }
    }


    private String getCellValue(HSSFCell cell) {
        String returnString = "";    
        //returnString = cell.getDateCellValue().toLocaleString();
        try {
            returnString = "" + cell.getNumericCellValue();
            returnString = returnString.substring(0, returnString.lastIndexOf('.'));
        } catch (Exception e) {
            try {
                returnString = cell.getStringCellValue();
            } catch (Exception e2) {
                returnString = "--";
            }
        }
        
        return returnString;
    }

    /*
     * (non-Javadoc)
     * @see org.setupx.repository.core.util.print.DYMOLabelPrintConnector#getValue(int, int)
     */
    public String getValue(final int elementOnLabel, final int labelCounter) {
        if (labelCounter == this.sampleIDs.length) finished = true;
        String value = "";

        switch (elementOnLabel) {
        case 0:
            // what ever you want to have in this line
            value = this.header;

            break;

        case 1:
            // what ever you want to have in this line
            value = this.labname;

            break;

        case 2:
            // what ever you want to have in this line
            try {
                value = "" + this.sampleIDs[labelCounter];
            } catch (Exception e) {
                value = "---";
            }
            break;

        case 3:
            // what ever you want to have in this line
            // TODO - add DB connection
            try{
                value = this.sampleLables[labelCounter];
            } catch (Exception e) {
                value = "---";
            }

            break;

        case 4:
            // what ever you want to have in this line
            value = DateFormat.getDateInstance(DateFormat.SHORT, Locale.US).format(new Date());

            break;

        default:
            break;
        }
        return value;
    }


    /*
     * (non-Javadoc)
     * @see org.setupx.repository.core.util.print.DYMOLabelPrintConnector#getPageNumbers()
     */  
    public int getPageNumbers() {
        int number = this.sampleIDs.length / lablesPerPage;
        if (this.sampleIDs.length % 4 > 0) number++;
        return number;
    }
    
    
    private static void printIt(File _sourceFile, String _labname, String _header) {
          log("starting");

          PrinterJob printerJob = PrinterJob.getPrinterJob();
          PageFormat pageFormat = printerJob.defaultPage();
          Paper paper = new Paper();

          final double widthPaper = (1.2 * 72);
          final double heightPaper = (1.5 * 72);

          paper.setSize(widthPaper, heightPaper);
          paper.setImageableArea(0, 0, widthPaper, heightPaper);

          pageFormat.setPaper(paper);

          pageFormat.setOrientation(PageFormat.LANDSCAPE);

          log("using a printmenu: " + PRINTMENU);
          if (PRINTMENU) {
            if (printerJob.printDialog()) {
              printerJob.setPrintable(new DYMOLabelPrintConnector4XLS(_sourceFile, _labname, _header, false), pageFormat); // The 2nd param is necessary for printing into a label width a right landscape format.
              
              try {
                printerJob.print();
              } catch (PrinterException e) {
                e.printStackTrace();
              }
            }
          } else {
            PrintService[] printService = PrinterJob.lookupPrintServices();

            for (int i = 0; i < printService.length; i++) {
              System.out.println(printService[i].getName());

              if (printService[i].getName().compareTo(PRINTERNAME) == 0) {
                try {
                  printerJob.setPrintService(printService[i]);
                  printerJob.setPrintable(new DYMOLabelPrintConnector4XLS(_sourceFile, _labname, _header, false), pageFormat); // The 2nd param is necessary for printing into a label width a right landscape format.
                  printerJob.print();
                } catch (PrinterException e) {
                  e.printStackTrace();
                }
              }
            }
          }
        }

}
