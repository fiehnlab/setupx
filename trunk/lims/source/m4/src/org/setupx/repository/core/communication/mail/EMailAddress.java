/**
 * ============================================================================ File:    EMailAddress.java Package: org.setupx.repository.core.communication.mail cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.mail;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.user.UnvalidEmailAddressException;


/**
 * A single EmailAddress.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.3 $
 * 
 * @deprecated - is not used in the new version anymore
 */
public class EMailAddress extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private static final long serialVersionUID = 9097053213477367930L;

    private String emailAddress;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final Object thiz = new EMailAddress();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new EMailAddress object.
   *
   * @param email 
   */
  public EMailAddress(String email) {
    try {
      setEmailAddress(email);
    } catch (UnvalidEmailAddressException e) {
      e.printStackTrace();
    }
  }

  private EMailAddress() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * validates a mailaddress and if it is ok it set a new email
   *
   * @param emailAddress the new emailaddress
   *
   * @throws UnvalidEmailAddressException the mailaddress is not valid
   */
  public void setEmailAddress(String emailAddress) throws UnvalidEmailAddressException {
    validateEmail(emailAddress);
    this.emailAddress = emailAddress;
  }

  /**
   * @return emailaddress as string
   */
  public String getEmailAddress() {
    return emailAddress;
  }

  /**
   * @return
   */
  public String toHTMLLink() {
    return ("<a href=\"mailto:" + this.getEmailAddress() + "\">" + this.getEmailAddress() + "</a>");
  }

  /*
   *  (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return emailAddress;
  }

  /**
   * validates the email
   *
   * @param emailAddress2 the new email that will be checked
   *
   * @throws UnvalidEmailAddressException the mailaddress is not valid
   */
  private static void validateEmail(String emailAddress2)
    throws UnvalidEmailAddressException {
    org.setupx.repository.core.util.Util.validateEmail(emailAddress2);
  }
}
