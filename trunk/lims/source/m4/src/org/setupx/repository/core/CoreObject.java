package org.setupx.repository.core;

import java.io.Serializable;
import java.util.List;

import org.hibernate.MappingException;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.hibernate.StaleStateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionStoreException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.multi.Promt;

/**
 * Superclass of all objects in this system. The logging methods <b>must </b>be used - and not the logging methods from
 * Logger.
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.18 $
 */
public abstract class CoreObject implements Serializable {
    // ~ Instance fields
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /** Unique Object ID - check the hibernate classes for more information */
    public long uoid;
    private int wasChecked4Persistence = 0;

    // ~ Static fields/initializers
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private static Class thiz = CoreObject.class;
    public static final boolean log = true;
    public static final boolean debug = true;
    public static final boolean SHOW_PERSISTENCE_LOG = false;

    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new CoreObject object.
     */
    public CoreObject() {
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * logging
     */
    public final static void debug(Object thiZ2, Object string) {
        if (debug) {
            Logger.debug(thiZ2, "" + string);
        }
    }

    /**
     * logging.
     * 
     * @see #debug
     */
    public final static void debug(Class thiZ2, Object string) {
        if (debug) {
            Logger.debug(thiZ2, "" + string);
        }
    }

    /**
     * logging - adds the uoid in front.
     */
    public void debug(String string) {
        debug(this, "[" + this.uoid + "]" + string);
    }

    /**
     * logging
     */
    public final static void err(Object object, Throwable error) {
        StackTraceElement[] elements = error.getStackTrace();
        StringBuffer buffer = new StringBuffer();

        buffer.append(error.getMessage() + " \n\n");

        for (int i = 0; i < elements.length; i++) {
            StackTraceElement element = elements[i];
            buffer.append(element.toString() + "\n");
        }

        Logger.err(object, "" + buffer.toString());
    }

    /**
     * logging
     */
    public final static void log(Object thiZ2, String[] strings) {
        if (log) {
            Logger.log(thiZ2, strings);
        }
    }

    /**
     * logging
     */
    public final static void log(Object thiZ2, String string) {
        if (log) {
            Logger.log(thiZ2, string);
        }
    }

    /**
     * logging
     */
    public final void debug(Object o) {
        debug(this, "" + o);
    }

    /**
     * logging
     */
    public final void err(String string) {
        Logger.err(this, string);
    }

    /**
     * logging
     */
    public final void log(Object o) {
        log(this, "" + o);
    }

    /**
     * logging
     */
    public final static void warning(Object thiZ2, String string) {
        Logger.warning(thiZ2, string);
    }

    /**
     * logging
     */
    public static boolean isDebug() {
        return debug;
    }

    /**
     * logging
     */
    public static boolean isLog() {
        return log;
    }

    /**
     * log an exception
     */
    public final void err(Throwable throwable) {
        this.err(throwable.getMessage());
    }

    /**
     * removes an object. The uoid of this object has to be != 0 otherwise it can not be peristent.
     */
    public void remove() {
        if (this.uoid == 0) {
            warning(this, "trying to remove myself, even so I am not even persistent.");

            return;
        }

        Session s = PersistenceConfiguration.createSessionFactory().openSession();
        Transaction transaction = s.beginTransaction();

        // store this object
        try {
            this.remove(s);
            transaction.commit();

            if (SHOW_PERSISTENCE_LOG) {
                log(this, "transaction commited - successfully.");
            }
        } catch (PersistenceActionException e) {
            err(this, e);
            warning(this, "rolling transaction back !! - cause: " + e.getLocalizedMessage());
            transaction.rollback();
            s.close();

            return;
        }

        // if no err - finish transaction
        s.close();
    }

    /**
     * @see #remove()
     */
    public void remove(Session s) throws PersistenceActionRemoveException {
        try {
            if (SHOW_PERSISTENCE_LOG) {
                log(this, "removing myself ");
            }

            s.delete(this);
        } catch (Exception e) {
            throw new PersistenceActionRemoveException("unable to remove ", e);
        }
    }

    /**
     * update an object
     * 
     * @param session
     *                owning session
     * @param createIt
     *                in case the object has not been stored yet - decide if it should be created or not.
     * @throws PersistenceActionUpdateException
     */
    public void update(Session session, boolean createIt) throws PersistenceActionUpdateException {
        // checking if this object is already sheduled for update in the session
        if (this.wasChecked4Persistence == session.hashCode()) {
            if (SHOW_PERSISTENCE_LOG) {
                debug(this, "-- object is already sheduled for update in the session");
            }

            return;
        } else {
            if (SHOW_PERSISTENCE_LOG) {
                debug(this, "update [create it: " + createIt + "  " + this + "]");
            }

            this.wasChecked4Persistence = session.hashCode();

            try {
                // store child objects
                if (SHOW_PERSISTENCE_LOG) {
                    debug(this, "persistence childs step 1/2");
                }

                this.persistenceChilds(session, createIt);

                // check if object has been saved before
                boolean doesExist = persistence_checkIfExists(this.getClass(), this.uoid);

                // in case the parent exists and the parent is a multifield - then this was added to the
                // multifield and will be save !!
                if (!createIt && this instanceof InputField) {
                    FormObject parent = ((FormObject) this).getParent();

                    if (parent.getUOID() != 0) {
                        createIt = true;
                    }
                }

                if (doesExist) {

                    // this exists, so only update
                    if (this instanceof FormObject) {
                        if (CoreObject.SHOW_PERSISTENCE_LOG) {
                            this.debug("CORE update: [START]" + ((FormObject) this).isActive() + "  "
                                    + ((FormObject) this).toString());
                        }
                    }
                    this.update(session);

                    if (this instanceof FormObject) {
                        if (CoreObject.SHOW_PERSISTENCE_LOG) {
                            this.debug("CORE update [DONE]: " + ((FormObject) this).isActive() + "  "
                                    + ((FormObject) this).toString());
                        }
                    }

                } else if (createIt) {
                    if (SHOW_PERSISTENCE_LOG) {
                        log(this, "does not exist - now saving me. ");
                    }

                    if (this instanceof FormObject) {
                        if (CoreObject.SHOW_PERSISTENCE_LOG) {
                            this.debug("CORE saving: [START]" + ((FormObject) this).isActive() + "  "
                                    + ((FormObject) this).toString());
                        }
                    }
                    this.save(session);

                    if (this instanceof FormObject) {
                        if (CoreObject.SHOW_PERSISTENCE_LOG) {
                            this.debug("CORE saving [DONE]: " + ((FormObject) this).isActive() + "  "
                                    + ((FormObject) this).toString());
                        }
                    }

                    // debug(this, "ok - I saved myself - now going back and updating myself.");
                    // warning(this, "DEACTIVATED : this.update(session, createIt);"); // this.update(session,
                    // createIt);
                } else {
                    throw new PersistenceActionInconsistentException(this,
                            "object does not exist but it is not permitted to create it in the database - A C H T U N G   !!! ");
                }
            } catch (PersistenceActionUpdateException e) {
                throw e;
            } catch (PersistenceActionException e) {
                throw new PersistenceActionUpdateException(this, e);
            }
        }
    }

    public void persistenceChilds(Session session, boolean createIt) throws PersistenceActionException {
        if (SHOW_PERSISTENCE_LOG) {
            debug(this, "persistence childs step 2/2");
        }
    }

    /**
     * checks if an instance of class1 with the uniqueObjectID does exist in the database does not work _ cause it can
     * be called within a session - and then the object is not submittet yet
     * 
     * <pre>
     * HOTFIXED by checking the id
     * </pre>
     * 
     * @param class1
     * @param uniqueObjectID
     *                the objects id
     * @return true if exists
     */
    public static boolean persistence_checkIfExists(Class class1, long uniqueObjectID) {
        // HOTFIX
        if (uniqueObjectID > 0) {
            return true;
        }

        return false;

        /*
         * Session session = PersistenceConfiguration.createSessionFactory().openSession(); try { boolean a =
         * (persistence_loadByID(class1, session, uniqueObjectID) != null); session.close(); debug("element found: " +
         * a); return a; } catch (PersistenceActionFindException e) { debug("while counting objects: " +
         * e.getLocalizedMessage()); session.close(); return false; }
         */
    }

    /**
     * TODO:
     * 
     * @throws PersistenceActionStoreException
     *                 TODO
     */
    public final void save() throws PersistenceActionStoreException {
        if (SHOW_PERSISTENCE_LOG) {
            debug(this, "----------------------------------------------");
        }

        if (SHOW_PERSISTENCE_LOG) {
            debug(this, "--               SAVING     save()          --");
        }

        if (SHOW_PERSISTENCE_LOG) {
            debug(this, "----------------------------------------------");
        }

        // create a session
        Session s = PersistenceConfiguration.createSessionFactory().openSession();
        Transaction transaction = s.beginTransaction();

        try {
            this.update(s, true);

            // if no err - finish transaction
            if (SHOW_PERSISTENCE_LOG) {
                this.debug("committing transaction");
            }

            try {
                transaction.commit();

                if (SHOW_PERSISTENCE_LOG) {
                    debug(this, "closing session");
                }
            } catch (StaleStateException e) {
                throw new PersistenceActionStoreException(e);
            }
        } catch (PersistenceActionException e) {
            if (SHOW_PERSISTENCE_LOG) {
                log(this, "rolling transaction back         !! - cause: " + e.getLocalizedMessage());
            }

            transaction.rollback();
            err(this, e);
            s.close();
            throw new PersistenceActionStoreException("unable to store - rolled back transaction already.", e);
        }

        s.close();
        debug(this, "----------------------------------------------");
    }

    /**
     * @see #update(Session, boolean)
     * @deprecated use method with session
     */
    public final void update(boolean createIt) throws PersistenceActionUpdateException {
        long tmpID = this.uoid;

        if (SHOW_PERSISTENCE_LOG) {
            debug(this, "updating[create if not exists: " + createIt + "] : " + this);
        }

        // create a session
        Session s = PersistenceConfiguration.createSessionFactory().openSession();
        Transaction transaction = s.beginTransaction();

        try {
            this.update(s, createIt);
            // if no err - finish transaction
            transaction.commit();

            s.close();
            // ServiceMediator.notifyUser(this);
        } catch (PersistenceActionException e) {
            transaction.rollback();
            s.close();

            if (e instanceof PersistenceActionUpdateException) {
                throw (PersistenceActionUpdateException) e;
            } else {
                throw new PersistenceActionUpdateException(this, e);
            }
        } catch (StaleStateException e) {
            e.printStackTrace();
            transaction.rollback();
            s.close();
            throw new PersistenceActionUpdateException(this, e);
        }

        if (this instanceof Promt) {
            ((Promt) this).updateRelations();

        }
        if (tmpID != this.uoid && tmpID != 0) {
            this.warning(new PersistenceActionUpdateException(
                    "serious problem - two different ids assigned to the same object."));
        }
    }

    /**
     * @return
     */
    public static int persistence_count(Class c, Session s) throws PersistenceActionFindException {
        return s.createCriteria(c).list().size();
    }

    /**
     * TODO:
     * 
     * @param s
     *                TODO
     * @return TODO
     * @throws PersistenceFindException
     *                 TODO
     */
    public static List persistence_loadAll(Class c, Session s) throws PersistenceActionFindException {
        List list = s.createCriteria(c).addOrder(Order.asc("id")).list();

        if (SHOW_PERSISTENCE_LOG) {
            debug(thiz, "number of objects found: " + list.size());
        }

        return list;
    }

    /**
     * TODO:
     * 
     * @param s
     *                TODO
     * @param id
     *                TODO
     * @return TODO
     * @throws PersistenceFindException
     *                 TODO
     */
    public static CoreObject persistence_loadByID(Class clazz, Session s, long id)
            throws PersistenceActionFindException {
        List list = s.createCriteria(clazz).add(Restrictions.eq("id", new Long(id))).list();

        if (SHOW_PERSISTENCE_LOG) {
            debug(thiz, "found " + list.size() + " elements of " + Util.classString(clazz.getName()));
        }

        if (list.size() > 1) {
            throw new PersistenceActionFindException("found more then one element for the id " + id,
                    new ParameterException("wrong number of elements"));
        }

        if (list.size() == 0) {
            throw new PersistenceActionFindException("unable to find element for the id " + id, new ParameterException(
                    "number of elements in result is 0"));
        } else {
            return (CoreObject) list.get(0);
        }
    }

    /**
     * @deprecated this id is not supported - use the unique object id uoid
     */
    public final void setID(int i) {
        throw new UnsupportedOperationException("this id is not supported - use the unique object id uoid");
    }

    /**
     * @deprecated this id is not supported - use the unique object id uoid
     */
    public final void setID(long i) {
        throw new UnsupportedOperationException("this id is not supported - use the unique object id uoid");
    }

    /**
     * @deprecated this id is not supported - use the unique object id uoid
     */
    public final void setId(int i) {
        throw new UnsupportedOperationException("this id is not supported - use the unique object id uoid");
    }

    /**
     * TODO:
     * 
     * @param objects
     *                TODO
     */
    public final void show(Object[] objects) {
        for (int i = 0; i < objects.length; i++) {
            Object object = objects[i];
            debug(this, "[pos:" + i + "]: " + object);

            if (object instanceof FormObject) {
                FormObject[] fields = ((FormObject) object).getFields();
                this.show(fields);
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return Util.getClassName(this) + " uoid: " + this.uoid;
    }

    /**
     * logging
     * 
     * @param object
     */
    public void warning(Throwable throwable) {
        warning(this, throwable.toString());
        throwable.printStackTrace();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#finalize()
     */
    protected void finalize() throws Throwable {
        // debug(this, "bye.");
        // MailController.sendNonsendMails();
        super.finalize();
    }

    /**
     * TODO:
     * 
     * @param s
     *                TODO
     * @throws PersistenceActionStoreException
     *                 TODO
     */
    private final void save(Session s) throws PersistenceActionStoreException {
        try {
            if (SHOW_PERSISTENCE_LOG) {
                log(this, "saving me.");
            }

            s.save(this);

            if (SHOW_PERSISTENCE_LOG) {
                log(this, "saving done. - my new ID is " + this.uoid);
            }
        } catch (Exception e) {
            throw new PersistenceActionStoreException(e);
        }
    }

    /**
     * @see #update(Session, boolean)
     */
    private final void update(Session s) throws PersistenceActionUpdateObjectUnknownException,
            PersistenceActionUpdateException {
        try {
            s.update(this);
            debug(this, "this.update(session) .. done.");
        } catch (MappingException e) {
            throw new RuntimeException(
                    "Configuration problem: Object is unknow for the mapping - check if hibernate-mapping-file for "
                            + org.setupx.repository.core.util.Util.getClassName(this) + " exists.", e);
        } catch (NonUniqueObjectException e) {
            throw new PersistenceActionUpdateObjectUnknownException(e);
        } catch (StaleStateException e) {
            throw new PersistenceActionUpdateObjectUnknownException(e);
        } catch (Exception e) {
            throw new PersistenceActionUpdateException(this, e);
        }
    }

    public static Session createSession() {
        return PersistenceConfiguration.createSessionFactory().openSession();
    }

    /**
     * checking if a term is not null and if it is not "null"
     * 
     * @param term
     * @throws PersistenceActionInconsistentException
     */
    public static void checkString(String term) throws PersistenceActionInconsistentException {
        if ((term == null) || (term.compareTo("null") == 0)) {
            throw new PersistenceActionInconsistentException("term: " + term + " is invalid.");
        }
    }
}
