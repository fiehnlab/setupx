/**
 * ============================================================================ File:    NCBI_ID_Extension.java Package: org.setupx.repository.core.communication.ncbi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ncbi;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.xml.XPath;
import org.setupx.repository.web.forms.restriction.NCBIVocRestriction;

import org.w3c.dom.Document;

import java.io.InputStream;

import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * Find the <b>NCBI </b>id for an organisms in the Taxonomy of NCBI.
 * 
 * <p>
 * The NCBI taxonomy database contains the names of all organisms that are represented in the genetic databases with at least one nucleotide or protein sequence. Click on the tree if you want to browse the taxonomic structure or retrieve sequence data for a particular group of organisms.
 * </p>
 * 
 * <p>
 * like: http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=taxonomy&term=arabidopsis
 * </p>
 *
 * @see http://www.ncbi.nlm.nih.gov/
 * @see org.setupx.repository.core.communication.ncbi.NCBIConnector
 */
class NCBI_ID_Extension extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected DocumentBuilder builder;
  protected DocumentBuilderFactory factory;
  protected URL baseUrl = null;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public final static String NCBI_URL_STRING = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi";
  public final static String URL_SEPERATOR = "?";
  public final static String URL_ARGUMENT_SEPERATOR = "&";
  public final static String URL_ARGUMENT_EQUAL = "=";

  {
    try {
      baseUrl = new URL(NCBI_URL_STRING);
      Util.checkURL(baseUrl);

      factory = DocumentBuilderFactory.newInstance();
      builder = factory.newDocumentBuilder();
    } catch (Exception e) {
      warning(this, e.toString());
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * find the ncbi id for an organism
   *
   * @param search_string the organism
   *
   * @return the id
   *
   * @throws NCBIException any problem while requesting the id from the remote service
   */
  public int determineNCBI_Id(String search_string) throws NCBIException {
    try {
      int i = NCBIAbbreviationMapping.determineNCBI_Id(search_string);
      debug(search_string + " was replaced by " + Util.getClassName(NCBIAbbreviationMapping.class) + " as " + i);

      return i;
    } catch (NCBIAbbreviationException e) {
      // there is no Abbreviation for this name
    }

    try {
      // create the whole url
      URL url = new URL(NCBI_URL_STRING + URL_SEPERATOR + "db" + URL_ARGUMENT_EQUAL + "taxonomy" + URL_ARGUMENT_SEPERATOR + "term" + URL_ARGUMENT_EQUAL + search_string + URL_ARGUMENT_SEPERATOR + "email" + URL_ARGUMENT_EQUAL + NCBIConnector.EMAIL); //"tool" + URL_ARGUMENT_EQUAL + NCBIConnector.TOOL + URL_ARGUMENT_SEPERATOR + 

      debug(url);

      // connect
      URLConnection connection = url.openConnection();
      connection.connect();

      String contentType = connection.getContentType();

      if (contentType.compareTo("XML") != 0) {
        //warning(this, "the service is responding with the wrong type of content intstead of XML : " + contentType);

        /*
           warning(new NCBIException("the service is responding with the wrong type of content intstead of XML : " + contentType));
           warning(new NCBIException("will treat it like an XML doc"));
         */
      }

      // create a doc from it
      InputStream in = connection.getInputStream();

      Document doc = builder.parse(in);

      // parse it for the answer
      String value;

      try {
        value = XPath.pathValue(doc, "/eSearchResult/IdList/Id");
      } catch (NullPointerException e) {
        value = "";
      }

      debug("value: " + value + "      \told one:" + search_string);

      // create the answer
      return Integer.parseInt(value);
    } catch (NumberFormatException e) {
      warning(new NCBIException("unable to get a valid ID from the NCBI server - check the ncbi server"));

      return NCBIVocRestriction.UNKNOWN_NCBI_ORGANISM_ID;
    } catch (Exception e) {
      throw new NCBIException(e);
    }
  }
}
