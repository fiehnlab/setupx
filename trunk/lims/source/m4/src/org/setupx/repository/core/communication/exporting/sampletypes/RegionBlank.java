/**
 * ============================================================================ File: RegionBlank.java Package:
 * org.setupx.repository.core.communication.exporting.sampletypes cvs: $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06
 * 21:54:21 scholz Exp $ ============================================================================ Martin Scholz
 * Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/
 * ============================================================================
 */
package org.setupx.repository.core.communication.exporting.sampletypes;

import org.setupx.repository.core.communication.exporting.JobInformation;
import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;

/**
 * 
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 * @hibernate.subclass
 */
public class RegionBlank extends AcquisitionSample {
    // ~ Static fields/initializers
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private static final String NAME = "Reagent Blank";

    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new RegionBlank object.
     */
    public RegionBlank() {
        super();
    }

    /**
     * Creates a new RegionBlank object.
     * 
     * @param information
     *                
     * @throws MappingError
     *                 
     */
    public RegionBlank(JobInformation information) throws MappingError {
        super(information, NAME);
        this.positionImportance = 1;
        this.sequenceImportance = 1;
        this.typeShortcut = "rs";
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /*
     * (non-Javadoc)
     * 
     * @see org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample#connectToSample()
     */
    public void connectToSample() throws PersistenceActionFindException, PersistenceActionUpdateException {
        debug(org.setupx.repository.core.util.Util.getClassName(this)
                + " are not connected to any samples. - so will be ignored.");
    }
}
