/**
 * ============================================================================ File:    DynamicLabel.java Package: org.setupx.repository.web.forms.label cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.label;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.setupx.repository.web.forms.InActiveException;

/**
 * @hibernate.subclass
 */
abstract class DynamicLabel extends Label {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String UNDEFINED_VALUE = "-undefined-";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @see Label#Label(String, String, String)
   */
  public DynamicLabel(String question, String description) {
    super(question, description);
  }

  /**
   * @deprecated only for persistencelayer
   */
  public DynamicLabel() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public abstract Object getDynamicValue();
  /*
   * (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createXMLNode(org.w3c.dom.Document)
   */
  public Node createXMLNode(Document doc) throws InActiveException {
    warning(this, "not xml exported.");
      return null;
}
}
