/**
 * ============================================================================ File:    Logger.java Package: org.setupx.repository.core.util.logging cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.logging;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Calendar;

import javax.mail.MessagingException;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.notification.NotificationCentral;
import org.setupx.repository.core.communication.notification.SystemNotification;
import org.setupx.repository.core.util.Mail;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;


/**
 * Logger sending all messages to screen, file, ...
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class Logger {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final String SPACER = "                       ";
  private static String SEPERATOR = ": ";
  private static int maxlength = 17;
  public static PersistenceConfiguration xs = null;
  /**
   * first message that is shown in the init
   */
  static {
    log(new Logger(), "_____________________________________________________________________");
    log(new Logger(), "                                                                      ");
    log(new Logger(), SPACER + Util.getDateString() + "                  ");
    log(new Logger(), "                                                                      ");
    log(new Logger(), "creating new instance");
    //xs = PersistenceConfiguration.getInstance();
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Logger() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws IOException TODO
   */
  public static String getLog() throws IOException {
    return readFile(new File(Config.IMPORT_FILENAME_LOG));
  }

  /**
   * logging messages for debugmode
   */
  public static void debug(Object object, String string) {
    if (CoreObject.debug) {
      log(object, "    DEBUG: " + string);
    }
  }

  /**
   * @param experiment
   * @param string
   */
  public static void err(Object i_object, String msg) {
    try {
      if (Config.LOGGING_ACITVE) {
        if (i_object == null) {
          i_object = Object.class;
        }

        System.err.println(Util.getClassName(i_object).concat(SEPERATOR).concat("" + msg));

        // Mail.postError(i_object, Util.getClassName(i_object).concat(SEPERATOR).concat(msg));
        if (Config.LOGGING2FILE_ACITVE) {
          try {
            log2File(new File(Config.IMPORT_FILENAME_LOG), i_object.getClass().getName() + SEPERATOR + msg);
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * TODO: 
   *
   * @param i_object TODO
   * @param msg TODO
   */
  public static void log(Object i_object, String msg) {
    if (Config.LOGGING_ACITVE) {
      String _classname = null;

      if (i_object == null) {
        _classname = Config.UNKNOW_CLASS;
      } else {
        _classname = Util.getClassName(i_object);

        if (_classname.compareTo("Class") == 0) {
          _classname = Util.classString(i_object.toString()); //Config.UNKNOW_CLASS;
          //_classname = Config.UNKNOW_CLASS;
        }
      }

      _classname = _classname + "______________________________";

      if (_classname.length() > maxlength) {
        _classname = _classname.substring(0, maxlength);
      }

      String _mem = "";

      if (Config.LOGGING_SYSTEM_ACITVE) {
        Runtime runtime = Runtime.getRuntime();
        _mem = "processors: " + runtime.availableProcessors() + " free mem:" + ((100 * runtime.freeMemory()) / runtime.totalMemory()) + "% ";
      }

      //System.out.println(i_object.getClass().toString().concat(SEPERATOR).concat(msg));
      System.out.println(_mem.concat(_classname.concat(SEPERATOR).concat(msg)));

      /*
         if (Config.LOGGING2FILE_ACITVE) {
           try {
             log2File(new File(Config.IMPORT_FILENAME_LOG), i_object.getClass().getName() + SEPERATOR + msg);
           } catch (Exception e) {
             e.printStackTrace();
           }
         }
       */
    }
  }

  /**
   * TODO: 
   *
   * @param obj TODO
   * @param ex TODO
   */
  public static void log(Object obj, Throwable ex) {
    NotificationCentral.notify(new SystemNotification(Util.getClassName(obj), ex));

    String msg = ex.getClass().toString() + "  " + ex.getLocalizedMessage() + " - " + ex.getMessage();

    /*
       ex.printStackTrace();
       try {
         log2File(new File(Config.IMPORT_FILENAME_ERROR), obj.getClass().getName() + SEPERATOR + ex.toString() + "   " + ex.getLocalizedMessage());
       } catch (FileNotFoundException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
       } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
       }
     */
    log("---err---err---err---err---err---err---err---err---err---err---err---err---err---err---err---");
    err(obj, "ERROR:  " + msg);
    log("---err---err---err---err---err---err---err---err---err---err---err---err---err---err---err---");

    /*
       System.err.println(obj.getClass() + SEPERATOR + msg);
     */
  }

  /**
   * @param thiZ
   * @param strings
   *
   * @deprecated only used by core object
   */
  public static void log(Object thiZ, String[] strings) {
    log(thiZ, "logging array of Strings - size: " + strings.length);

    for (int i = 0; i < strings.length; i++) {
      log(thiZ, i + 1 + "/" + strings.length + ": " + strings[i]);
    }
  }

  /**
   * adds information at the end of a file.
   *
   * @param i_File the logfile
   * @param i_content the information that is going to be added to the file.
   *
   * @throws java.io.FileNotFoundException TODO
   * @throws java.io.IOException TODO
   */
  public static void log2File(File i_File, String i_content)
    throws java.io.FileNotFoundException, java.io.IOException {
    if (Config.LOGGING2FILE_ACITVE) {
      String _content = readFile(i_File);

      Calendar cal = Calendar.getInstance();
      String date = Util.getDateString();
      _content = _content.concat(date.concat("    " + i_content + "\n"));

      writeFile(i_File, _content);
    }
  }

  /**
   * @param config
   */
  public static void logAnatomize(Object config) {
    Field[] fields = config.getClass().getDeclaredFields();
    Logger.debug(config, "-- fields of " + Util.getClassName(config));

    for (int i = 0; i < fields.length; i++) {
      Field field = fields[i];

      try {
        Logger.debug(config, "anatomize: " + shortIt(field.getName()) + " " + field.get(config) + "    [" + Util.classString(field.getType().toString()) + "]");
      } catch (IllegalArgumentException e) {
        // IGNORE
      } catch (IllegalAccessException e) {
        // IGNORE
      }
    }
  }

  /**
   * TODO: 
   *
   * @param object TODO
   * @param string TODO
   *
   * @deprecated use NotificationCentral
   */
  public static void mail(Object object, String string) {
    try {
      Mail.postMail(new String[] { Config.OPERATOR_ADMIN.toString() }, Config.SYSTEM_NAME + ":" + Util.getClassName(object), string, Config.OPERATOR_ADMIN.toString());
    } catch (MessagingException e) {
      e.printStackTrace();
    }
  }

  /**
   * TODO: 
   *
   * @param object TODO
   * @param string TODO
   * @param string2 TODO
   */
  public static void mailWarning(Object object, String string, String string2) {
    warning(object, string + ": " + string2);
    NotificationCentral.notify(new SystemNotification(Util.getClassName(object).concat(SEPERATOR).concat(string).concat(": " + string2)));
  }

  /**
   * TODO: 
   *
   * @param object TODO
   * @param string TODO
   */
  public static void warning(Object object, String string) {
    /*      try {
       new Message(object, "   WARNING: " + string).update(true);
       } catch (PersistenceActionUpdateException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       }
     */
    log(object, "   WARNING: " + string);
    // NotificationCentral.notify(new SystemNotification(Util.getClassName(object).concat(SEPERATOR).concat(string)));
  }

  /**
   * TODO: 
   *
   * @param thiZ TODO
   * @param string TODO
   * @param e TODO
   */
  public static void warning(Object thiZ, String string, Throwable e) {
    Logger.warning(thiZ, string + ":  " + Util.getClassName(e) + ":  " + e.getMessage());
  }

  /**
   * reads the content of a file
   *
   * @param i_file the file that will be read
   *
   * @return the content of the file
   *
   * @throws java.io.IOException there were problems accessing the file
   */
  protected static String readFile(File i_file) throws java.io.IOException {
    int _sizeFile = (int) i_file.length();
    int _char_read = 0;
    
    // checking if file exists and in case it does not / create file
    if (!i_file.exists()){
        i_file.createNewFile();
    }
    
    
    java.io.FileReader inFile = new java.io.FileReader(i_file);
    char[] readData = new char[_sizeFile];

    while (inFile.ready()) {
      _char_read += inFile.read(readData, _char_read, _sizeFile - _char_read);
    }

    inFile.close();

    String _text_read = new String(readData, 0, _char_read);

    return _text_read;
  }

  /**
   * TODO: 
   *
   * @param msg TODO
   */
  private static void log(String msg) {
    if (Config.LOGGING_ACITVE) {
      log(new Object(), msg);
    }
  }

  /**
   * TODO: 
   *
   * @param string TODO
   *
   * @return TODO
   */
  private static String shortIt(String string) {
    string = string.concat(SPACER);

    return string.substring(0, 20);
  }

  /**
   * TODO: 
   *
   * @param i_File TODO
   * @param i_content TODO
   *
   * @throws IOException TODO
   */
  private static void writeFile(File i_File, String i_content)
    throws IOException {
    FileWriter _fileWriter = new FileWriter(i_File);
    BufferedWriter _buffWriter = new BufferedWriter(_fileWriter);
    _buffWriter.write(i_content);
    _buffWriter.close();
    _fileWriter.close();
  }
}
