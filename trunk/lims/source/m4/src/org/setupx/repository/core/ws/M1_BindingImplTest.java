/**
 * 
 */
package org.setupx.repository.core.ws;

import java.rmi.RemoteException;

import junit.framework.TestCase;

/**
 * @author scholz
 *
 */
public class M1_BindingImplTest extends TestCase {
    long idExperiment =338050;
    long idClazz = 338156 ;
    long idSample = 338135;  
    
    
    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#getClassIDsByExperiment(java.lang.String)}.
     */
    public void testGetClassIDsByExperiment() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#getClassInfos(java.lang.String)}.
     * @throws RemoteException 
     * @throws WebServiceException 
     */
    public void testGetClassInfos() throws WebServiceException, RemoteException {
        if (new M1_BindingImpl().getClassInfos("" + idClazz).length() < 4) fail("message to short");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#getClazzIDbySampleID(java.lang.String)}.
     * @throws RemoteException 
     * @throws WebServiceException 
     */
    public void testGetClazzIDbySampleID() throws WebServiceException, RemoteException {
        if (new M1_BindingImpl().getClazzIDbySampleID("" + idSample).compareTo("" + idClazz) != 0 ) {
            fail("wrong id");
        }
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#getExperimentIDbyClazz(java.lang.String)}.
     * @throws RemoteException 
     * @throws WebServiceException 
     */
    public void testGetExperimentIDbyClazz() throws WebServiceException, RemoteException {
        if (new M1_BindingImpl().getExperimentIDbyClazz("" + idClazz).compareTo("" + idExperiment) != 0 ) {
            fail("wrong id");
        }
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#getExperimentIDbySample(java.lang.String)}.
     * @throws WebServiceException 
     */
    public void testGetExperimentIDbySample() throws WebServiceException {
        if (new M1_BindingImpl().getExperimentIDbyClazz("" + idSample).compareTo("" + idExperiment) != 0 ) {
            fail("wrong id");
        }
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#getExperimentsSheduled()}.
     */
    public void testGetExperimentsSheduled() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#getSampleIDbyACQLabel(java.lang.String)}.
     */
    public void testGetSampleIDbyACQLabel() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#getSamplesByClassID(java.lang.String)}.
     */
    public void testGetSamplesByClassID() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#getSamplesForUser(long)}.
     */
    public void testGetSamplesForUser() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#activateService(java.lang.String)}.
     */
    public void testActivateService() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#addEvent(java.lang.String, java.lang.String)}.
     */
    public void testAddEvent() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#authenticateUser(java.lang.String, java.lang.String, java.lang.String)}.
     * @throws WebServiceException 
     */
    public void testAuthenticateUser() throws WebServiceException {
        new M1_BindingImpl().authenticateUser("sx", "sx", " " );
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#availableColumns()}.
     */
    public void testAvailableColumns() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#createMessage(java.lang.String, java.lang.String)}.
     * @throws RemoteException 
     * @throws WebServiceException 
     */
    public void testCreateMessage() throws WebServiceException, RemoteException {
        new M1_BindingImpl().createMessage("testmessage from JUnit", "testmessage");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#determineColumnIDbyACQname(java.lang.String)}.
     */
    public void testDetermineColumnIDbyACQname() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#determineSpeciesType(java.lang.String)}.
     */
    public void testDetermineSpeciesType() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#determineSpeciesTypeName(java.lang.String)}.
     */
    public void testDetermineSpeciesTypeName() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#exportByExperimentID(java.lang.String)}.
     */
    public void testExportByExperimentID() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#uploadResultFile(java.lang.String, java.lang.String)}.
     */
    public void testUploadResultFile() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#determineMetaDataByClass(java.lang.String, java.lang.String)}.
     */
    public void testDetermineMetaDataByClass() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#determineMetaDataByExperiment(java.lang.String, java.lang.String)}.
     */
    public void testDetermineMetaDataByExperiment() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#determineMetaDataBySample(java.lang.String, java.lang.String)}.
     */
    public void testDetermineMetaDataBySample() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#determineMetaDataBySampleMax(java.lang.String, java.lang.String)}.
     */
    public void testDetermineMetaDataBySampleMax() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#determineNCBIBySampleID(java.lang.String)}.
     */
    public void testDetermineNCBIBySampleID() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#determineSpeciesNameByNCBI(java.lang.String)}.
     */
    public void testDetermineSpeciesNameByNCBI() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#isMasterUser(java.lang.String, java.lang.String)}.
     */
    public void testIsMasterUser() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link org.setupx.repository.core.ws.M1_BindingImpl#isUser(java.lang.String, java.lang.String)}.
     */
    public void testIsUser() {
        fail("Not yet implemented");
    }

}
