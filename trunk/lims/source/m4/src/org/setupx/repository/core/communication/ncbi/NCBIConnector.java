/**
 * ============================================================================ File:    NCBIConnector.java Package: org.setupx.repository.core.communication.ncbi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ncbi;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import org.setupx.repository.Config;
import org.setupx.repository.core.Connectable;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.ServiceNotAvailableException;
import org.setupx.repository.core.communication.ncbi.local.LocalDatabaseInstance;
import org.setupx.repository.core.communication.ncbi.local.NCBIEntry;
import org.setupx.repository.core.communication.tissueDB.TissueDBReader;
import org.setupx.repository.core.communication.vocabulary.VocabularyCheckException;
import org.setupx.repository.web.forms.restriction.NCBIVocRestriction;


/**
 * Connector to the remote services of the NCBI database.
 * 
 * <p>
 * <a href="http://www.ncbi.nlm.nih.gov/entrez/query/static/esoap_help.html">www.ncbi.nlm.nih.gov</a>
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.32 $
 */
public class NCBIConnector extends CoreObject implements Connectable {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static Object thiz = NCBIConnector.class;
  private static Hashtable cache_ids = new Hashtable(100);
  private static Hashtable cache_entries = new Hashtable(50);

  //private EUtilsServiceSoapStub binding;
  static {
    debug(NCBIConnector.class, "new instance");

    try {
      init();
    } catch (Exception e) {
      throw new RuntimeException("unable to init " + org.setupx.repository.core.util.Util.getClassName(thiz) + ": " + e.toString());
    }
  }

  /** soap connection */

  //private static EUtilsServiceSoap utils = null;

  /** is send with the request to offer the remote system admins to validate the request and in case of problems to contact the admin */
  public static final String EMAIL = "mscholz@ucdavis.edu";

  /** is send with the request to offer the remote system admins to validate the request and in case of problems to contact the admin */
  public static final String TOOL = Config.SYSTEM_NAME + " NCBI Connector";

  /** name of the remote database */
  private static final String TAXONOMY_DATABASE = "taxonomy";

  /** vocabulary extension */
  private static NCBI_VocExtension ncbi_VocExtension = new NCBI_VocExtension();

  /** id extension */
  private static NCBI_ID_Extension ncbi_id_extension = new NCBI_ID_Extension();

  /** info extension */
  private static NCBI_Info_Extension ncbi_info_extension = new NCBI_Info_Extension();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new NCBIConnector object.
   */
  public NCBIConnector() {
    try {
      init();
    } catch (NCBIException e) {
      e.printStackTrace();
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public static Hashtable getCache_entries() {
    return cache_entries;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public static Hashtable getCache_ids() {
    return cache_ids;
  }

  /**
   * main - just for testing
   *
   * @param args TODO
   *
   * @throws NCBIException
   * @throws Exception TODO
   */
  public static void main(String[] args) throws NCBIException {
    System.out.println("--->" + TissueDBReader.data.size());

    NCBIConnector connector = new NCBIConnector();

    String[] names = new String[] { "cow", "arabidopsis", "frog", "human", "tiger", "nut", "onion", "chicken", "apple", "homo sapiens", "Chlamydomonas" };
    internalTest(names, connector);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.Connectable#isAvailable()
   */
  public void checkAvailability() throws ServiceNotAvailableException {
    try {
      this.determineNCBI_Id("Arabidopsis");
    } catch (NCBIException e) {
      super.warning(e);
      throw new ServiceNotAvailableException(e);
    }
  }

  /**
   * finds the ncbi id for a species, kingdom, ...
   * 
   * <p>
   * The method s beeing replaced by a non web based implementation because of compatibilty and performance reasons.
   * </p>
   * the flag NCBIConnector#WEBSERVICE_ACTIVE defines if the webservice implementaion is used or a direct xml implementaion
   *
   * @param search_string the species, kingdom, ...
   *
   * @return the NCBI unique id
   *
   * @throws NCBIException TODO
   *
   * @see NCBIConnector#WEBSERVICE_ACTIVE
   */
  public static int determineNCBI_Id(String search_string)
    throws NCBIException {
    search_string = search_string.toLowerCase();

    int id = NCBIVocRestriction.UNKNOWN_NCBI_ORGANISM_ID;

    if (search_string.length() < 2) {
      return id;
    }

    // use direct connection
    // >>   http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=taxonomy&term=arabidopsis
    debug(thiz, "looking for ncbi id for: \"" + search_string + "\" IDcache size: " + cache_ids.size());

    // cached objects are stored in a hashtable to speed it up. 
    if (cache_ids.containsKey(search_string)) {
      debug(thiz, "found a cached object of " + search_string);

      return Integer.parseInt((String) cache_ids.get(search_string));
    }

    try {
      id = Integer.parseInt(LocalDatabaseInstance.getNCBI_Entry(search_string).getTaxID());

      if (id != NCBIVocRestriction.UNKNOWN_NCBI_ORGANISM_ID) {
        //debug(thiz, "found a localdatabase object for " + search_string);
      }
    } catch (Exception e) {
      id = NCBIVocRestriction.UNKNOWN_NCBI_ORGANISM_ID;
    }

    if (id == NCBIVocRestriction.UNKNOWN_NCBI_ORGANISM_ID) {
      debug(thiz, "looking remote for NCBI code");
      id = ncbi_id_extension.determineNCBI_Id(search_string);
      debug(thiz, "I am glad found it remote: " + id);
    }

    // caching it...
    cache_ids.put(search_string, "" + id);

    return id;

    //if (!NCBIConnector.WEBSERVICE_ACTIVE) {
    //    return ncbi_id_extension.determineNCBI_Id(search_string);
    //return Integer.parseInt(LocalDatabaseInstance.getNCBI_Entry(search_string).getTaxID());
    //}

    /*}
       debug("creating remote request");
       init();
       if (utils == null) {
         throw new NCBIException("NCBI is not available");
       }
       ESearchRequest request = new ESearchRequest();
       request.setDb(TAXONOMY_DATABASE);
       request.setTerm(search_string);
       request.setEmail(EMAIL);
       request.setTool(TOOL);
       ESearchResult response;
       try {
         response = utils.run_eSearch(request);
       } catch (RemoteException e) {
         throw new NCBIException("unable to determine id ", e);
       }
       String[] strings = response.getIdList().getId();
       if (strings == null) {
         throw new NCBIFindException("no ncbi id found for " + search_string);
       }
       if (strings.length > 1) {
         throw new NCBIException("there was more then one result");
       }
       if (strings.length == 0) {
         throw new NCBIFindException("no ncbi id found");
       }
       for (int i = 0; i < strings.length; i++) {
         debug(search_string + " : " + strings[i]);
       }
       return Integer.parseInt(strings[0]);*/
  }

  /**
   * determine information from the ncbi database to a specific NCBI id.
   * 
   * <p>
   * the flag NCBIConnector#WEBSERVICE_ACTIVE defines if the webservice implementaion is used or a direct xml implementaion
   * </p>
   *
   * @param id the unique ncbi id
   *
   * @return Hashtable containing all attributes for this specific NCBI code - now an NCBIEntryobject
   *
   * @throws NCBIException TODO
   *
   * @see NCBIConnector#WEBSERVICE_ACTIVE
   */
  public static NCBIEntry determineNCBI_Information(int id)
    throws NCBIException {
    //debug(thiz, "determineNCBI_Information(): looking for ncbi for: " + id + " ENTRYcache size: " + cache_entries.size());
    if (cache_entries.containsKey("" + id)) {
      //debug(thiz, "returning a cached object");
      return (NCBIEntry) cache_entries.get("" + id);
    }

    NCBIEntry entry = LocalDatabaseInstance.getNCBI_Entry_byID(id + "");
    cache_entries.put("" + id, entry);

    return entry;
    // replacing webservice by direct xml communication
    // use direct connection
    // http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=taxonomy&id=9913,30521&retmode=xml
    //debug("looking for ncbi information by id: " + id);
    //return LocalDatabaseInstance.getNCBI_Entry_byID(id + "").toHashtable();

    /*} else {
       log("using the webservice implementation");
       //  old webservie implementation
       ESummaryRequest summaryRequest = new ESummaryRequest();
       summaryRequest.setDb(TAXONOMY_DATABASE);
       summaryRequest.setEmail(EMAIL);
       summaryRequest.setTool(TOOL);
       summaryRequest.setId("" + id);
       Hashtable result = new Hashtable();
       ESummaryResult r;
       try {
         utils = new EUtilsServiceLocator().geteUtilsServiceSoap();
         r = utils.run_eSummary(summaryRequest);
       } catch (RemoteException e) {
         throw new NCBIException("unable to search for " + id, e);
       } catch (ServiceException e) {
         throw new NCBIException("unable to search for " + id, e);
       }
       DocSumType[] doc = r.getDocSum();
       String key = "";
       String value = "";
       for (int j = 0; j < doc.length; j++) {
         DocSumType type = doc[j];
         ItemType[] itemTypes = type.getItem();
         for (int k = 0; k < itemTypes.length; k++) {
           ItemType type2 = itemTypes[k];
           key = itemTypes[k].getName();
           MessageElement[] elemente = type2.get_any();
           if (elemente == null) {
             warning(this, "unable to find " + id + "/" + key);
             value = "";
           } else if (elemente.length > 1) {
             warning(this, "there is more then one value for a key");
           } else {
             for (int index = 0; index < elemente.length; index++) {
               MessageElement element = elemente[index];
               value = element.getValue();
             }
           }
           debug(id + " adding " + key + ": " + value);
           result.put(key, value);
         }
       }
       return result;
       }*/
  }

  /**
   * @param word
   *
   * @return
   *
   * @throws VocabularyCheckException
   */
  public boolean vocabularyCheck(String word) throws VocabularyCheckException {
    return ncbi_VocExtension.vocabularyCheck(word);
  }

  /**
   * @param word
   *
   * @return
   *
   * @throws VocabularyCheckException
   */
  public String vocabularySuggestion(String word) throws VocabularyCheckException {
    return ncbi_VocExtension.vocabularySuggestion(word);
  }

  /**
   * init the connector.
   *
   * @throws NCBIException problems creating a connection, ...
   */
  private static void init() throws NCBIException {
    if (NCBI_Config.NCBI_precaching_acitve) {
      debug(thiz, "preloading the cache!");

      String[] queryStrings = new String[] { "homo sapiens" };

      try {
        for (int i = 0; i < queryStrings.length; i++) {
          determineNCBI_Id(queryStrings[i]);
        }

        Iterator iterator = cache_ids.values().iterator();
        String element;

        while (iterator.hasNext()) {
          element = (String) iterator.next();

          int myID = Integer.parseInt(element);
          NCBIEntry entry = determineNCBI_Information(myID);
          log(thiz, entry.getSpeciesTypeName());
        }
      } catch (Exception e) {
        warning(thiz, "unable to precache NCBI objects: " + e);
      }
    }
  }

  /**
   * @param names
   * @param connector
   */
  private static void internalTest(String[] names, NCBIConnector connector) {
    for (int i = 0; i < names.length; i++) {
      int id;
      NCBIEntry entry = null;

      try {
        log(thiz, " ----------------------------------------------------------------------------------------");
        log(thiz, " ----> start for " + names[i] + "  ");
        id = connector.determineNCBI_Id(names[i]);
        entry = connector.determineNCBI_Information(id);

        log(thiz, " ----> found for " + names[i] + "  the id: " + id + " speciesType " + entry.getSpeciesType() + "  and " + entry.toHashtable().size() + " attributes.");

        Enumeration enumeration = entry.toHashtable().keys();

        while (enumeration.hasMoreElements()) {
          String element = (String) enumeration.nextElement();

          log(thiz, names[i] + " - " + element + " : " + entry.toHashtable().get(element));
        }
      } catch (NCBIException e) {
        log(thiz, names[i] + " " + e.getLocalizedMessage());
      } finally {
        log(thiz, " ----------------------------------------------------------------------------------------");
      }
    }
  }
}
