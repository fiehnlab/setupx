package org.setupx.repository.web.forms;

public class CloneException extends Exception {

    public CloneException(Throwable e) {
        super(e);
    }

    /**
     * @param string
     */
    public CloneException(String string) {
        super(string);
    }

}
