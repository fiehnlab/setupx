package org.setupx.repository.server.workflow;

import java.io.File;
import java.io.UTFDataFormatException;

import org.xml.sax.SAXException;

import junit.framework.TestCase;
import org.setupx.repository.core.InitException;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.workflow.elements.ProcessingException;

public class WorkFlowManagerInitTest extends TestCase {

    public static void main(String[] args) throws InitException, UTFDataFormatException, SAXException, WorkFlowException, ProcessingException {
        WFConfigFile file = new WFConfigFile(new File(File.separator + "dev"+ File.separator + "source"+ File.separator + "m3"+ File.separator + "share"+ File.separator + "firstWorkflow.xml"));

        WorkFlowManager flowManager =  WorkFlowManagerFactory.getInstance();
        Logger.debug(null, " ------------------------------------------------------------------------------------");
        Logger.debug(null, " ------------------------------------------------------------------------------------");
        Logger.debug(null, " ------------------------------------------------------------------------------------");
        Logger.debug(null, " ------------------------------------------------------------------------------------");
        Logger.debug(null, " ------------------------------------------------------------------------------------");

        Workflow workflow =  flowManager.newWorkFlow(file);

        workflow.start();
    }



    public void testInit() throws UTFDataFormatException, InitException, SAXException, WorkFlowException, ProcessingException {
        main(null);
    }

}
