/**
 * ============================================================================ File:    Relation.java Package: org.setupx.repository.web.forms.relation cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.relation;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.Session;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.web.forms.ComparisonsException;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.inputfield.multi.MultiField;


/**
 * Relation between an Inputfield and a number of related FormObjects.
 * 
 * <p>
 * a single Relation can contain one condition which changes the status of a set of FormObjects.
 * </p>
 * 
 * <p>
 * If the value is the same as the <code>condition</code>, the InputFileds will be activated. <code>setActive(true)</code>
 * </p>
 * 
 * <p>
 * If <b>not</b> the related Inputfileds will be deactivated. <code>setActive(false)</code>
 * </p>
 * Example:<br><code> Relation relation_plant = new <b>Relation(new String[] { "eins" }, field_To_Activate);</b><br>  fieldContainingEins.<b>addRelation</b>(relation_plant); </code>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.3 $
 *
 * @hibernate.subclass
 */
public class Relation extends AbstractRelation {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String[] conditions = { "" };

  /**
   * @label the form objects what will be modified
   */
  private FormObject[] relationTargets;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Relation object.
   *
   * @param conditions 
   * @param relatedTo 
   */
  public Relation(String[] conditions, FormObject[] relatedTo) {
    setRelationTargets(relatedTo);
    this.conditions = conditions;
  }

  /**
   * @param condition condition to change the active state of all related formobjects
   * @param relatedTo
   */
  public Relation(String[] conditions, FormObject relatedTo) {
    this(conditions, new FormObject[] { relatedTo });
  }

  /**
   * Creates a new Relation object.
   */
  public Relation() {
    this("", new FormObject[0]);
  }

  /**
   * @param string
   * @param page_disease
   */
  public Relation(String string, FormObject formObject) {
    this(new String[] { string }, formObject);
  }

  /**
   * @param string
   * @param page_disease
   */
  public Relation(String string, FormObject[] relatedTo) {
    this(new String[] { string }, relatedTo);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param conditions TODO
   */
  public void setConditions(String[] conditions) {
    this.conditions = conditions;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String[] getConditions() {
    return conditions;
  }

  /**
   * @param relatedTo
   */
  public void setRelationTargets(FormObject[] relatedTo) {
    //deactivate all related fields
    for (int i = 0; i < relatedTo.length; i++) {
      relatedTo[i].setActive(false);
    }

    this.relationTargets = relatedTo;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public FormObject[] getRelationTargets() {
    return relationTargets;
  }

  /**
   * TODO: 
   *
   * @param set TODO
   */
  public void setRelationTargetsHibernate(Set set) {
    setRelationTargets(map(set));
  }

  /**
   * @hibernate.set
   * @hibernate.collection-key column="TARGET"
   * @hibernate.collection-one-to-many  class="org.setupx.repository.web.forms.FormObject"
   */
  public Set getRelationTargetsHibernate() {
    return map(getRelationTargets());
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.CoreObject#persistenceChilds(org.hibernate.Session, boolean)
   */
  public final void persistenceChilds(Session session, boolean createIt)
    throws PersistenceActionException {
    Iterator iterator = this.getRelationTargetsHibernate().iterator();

    while (iterator.hasNext()) {
      ((CoreObject) iterator.next()).update(session, createIt);
    }

    // contitions
    // an hashset of string  is done automaticly
    super.persistenceChilds(session, createIt);
  }

  /*
   *  (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    StringBuffer buffer = new StringBuffer("Relation - ");

    for (int i = 0; i < this.getConditions().length; i++) {
      buffer.append(this.getConditions()[i] + "   \n");
    }

    buffer.append(" related to:  \n");

    for (int i = 0; i < this.getRelationTargets().length; i++) {
      buffer.append(this.getRelationTargets()[i].toString() + "   \n");
    }

    return buffer.toString();
  }

  /**
   * checks if this is the condition to activate the related objects - if not all related Inputfields will be deactived
   *
   * @param updateValue the new value which will be compared to the condition
   */
  public void update(String updateValue) {
    //debug("checking dependencies: " + this.conditions + "  vs. " + updateValue);
    boolean activate = false;

    // checking every possible condition
    for (int i = 0; i < conditions.length; i++) {
      if (this.conditions[i].compareTo(updateValue) == 0) {
        activate = true;
      }
    }

    for (int i = 0; i < relationTargets.length; i++) {
      relationTargets[i].setActive(activate);

      /*
      // HOTFIX
      // in case it is a mulifield - activate the childs
      //log(Util.getClassName(relationTargets[i]) + "    \tTreatment: " + (relationTargets[i] instanceof Treatment));
      try {
        ((MultiField) relationTargets[i]).activateTreatmentsChilds(activate);
      } catch (ClassCastException e) {
        // oops - was not a multifield
      }
      */
    }
  }

  /**
   * TODO: 
   *
   * @param set TODO
   *
   * @return TODO
   */
  private final static FormObject[] map(Set set) {
    FormObject[] results = new FormObject[set.size()];
    Iterator iterator = set.iterator();

    int pos = 0;

    while (iterator.hasNext()) {
      results[pos] = (FormObject) iterator.next();
      pos++;
    }

    return results;
  }

  /**
   * TODO: 
   *
   * @param relationTargets2 TODO
   *
   * @return TODO
   */
  private static Set map(FormObject[] relationTargets2) {
    HashSet hashSet = new HashSet();

    for (int i = 0; i < relationTargets2.length; i++) {
      hashSet.add(relationTargets2[i]);
    }

    return hashSet;
  }
  
  public void compare(AbstractRelation relation2) throws ComparisonsException{
      super.compare(relation2);
      if (this.getRelationTargets().length != ((Relation)relation2).getRelationTargets().length )throw new ComparisonsException(this, relation2, "relation targets length");
  }

}
