/**
 * ============================================================================ File:    PromtTeschter.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import org.setupx.repository.core.CoreObject;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.36 $
 */
public class PromtTeschter extends CoreObject {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new PromtTeschter object.
   *
   * @throws ArrayShrinkException 
   * @throws ArrayExpandException
   */
  public PromtTeschter() throws ArrayShrinkException, ArrayExpandException {
    super();

    /*
       InputField fieldA = new StringInputfield("principle investigator", "name of the principle investigator", "yourFirstName yourLastname", new Restriction4Names(), false);
       log("---->   " + fieldA.validate());
       InputField fieldB = new StringInputfield("email", "mail", "ASD.asas423df.ASD@ASDFADSF.PERC.de", new Restriction4Email(), false);
       log("---->   " + fieldB.validate());
       Relation relation = new Relation(new String[] { "Martin Scholz" }, fieldB);
       Relation[] relations = new Relation[1];
       relations[0] = relation;
       log("fieldA: " + fieldA.isActive());
       log("fieldB: " + fieldB.isActive());
       fieldA.setRelations(relations);
       String[] values = { "Martin Scholz", "Martin Scholzo", "Martin Scholza", "Martin Scholz" };
       for (int i = 0; i < values.length; i++) {
         fieldA.setValue(values[i]);
         log("value : " + fieldA.getValue());
         log("fieldA: " + fieldA.isActive() + "   fieldB: " + fieldB.isActive());
       }
       /*
          field = new StringInputfield("phone", "phone", "+49 (531) 1234 - 124",
              new Restriction4Telephone(), false);
          log("---->   " + field.validate());
    
    
                                                                                                                                                          Promt promt = new Promt("name", "description", "long description");
                                                                                                                                     Page pageA = new Page("name", "description", "long description");
                                                                                                                                     StringInputfield field1 = new StringInputfield("a1", "b1", "BLA", new LexiRestriction(), false);
                                                                                                                                     InputField field2 = new StringInputfield("a2", "b2", "Blaunschweig", new LexiRestriction(),
                                                                                                                                         false);
                                                                                                                                     ExpandingField metaField = new MultiFieldExpanding("question", "descriotion", "long description", field1);
                                                                                                                                     pageA.addField((InputField) metaField);
                                                                                                                                     pageA.addField(field2);
                                                                                                                                     pageA.addField(new StringInputfield("bla","bla","120",new Restriction4Numbers(1,100),false));
                                                                                                                                     promt.addField(pageA);
                                                                                                                                     try {
                                                                                                                                       log("" + promt.remove(field1.getName()));
                                                                                                                                       throw new RuntimeException("was able to remove the last object form a formcontainer");
                                                                                                                                     } catch (Exception e) {
                                                                                                                                       InputField field3 = new StringInputfield("a3", "b3");
                                                                                                                                       metaField.addField(field3);
                                                                                                                                     }
                                                                                                                                     log("\n" + promt.showIt(0) + "");
                                                                                                                                     log("" + promt.remove(field1.getName()));
                                                                                                                                     log("\n" + promt.showIt(0) + "");
                                                                                                                                     metaField.expand(4);
                                                                                                                                     log(promt.validate());
     */
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param args TODO
   *
   * @throws ArrayShrinkException TODO
   */
  public static void main(String[] args) throws Exception {
    new PromtTeschter();
  }
}
