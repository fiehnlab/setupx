/**
 * ============================================================================ File:    Report.java Package: org.setupx.repository.core.communication.export.reporting cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.export.reporting;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.export.FileExport;
import org.setupx.repository.core.communication.export.FileExportException;
import org.setupx.repository.core.communication.export.xls.XLSFileExport;
import org.setupx.repository.web.forms.inputfield.multi.Promt;


/**
 * Represent an Experiment, that is not for modification anymore.
 */
public class Report extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Promt promt;
  private long promtID;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final long serialVersionUID = 1L;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Report object.
   *
   * @param experiment_ID 
   *
   * @throws ReportInitException 
   */
  public Report(long experiment_ID) throws ReportInitException {
    this.promtID = experiment_ID;

    /*
       // load the promt
       try {
         promt = Promt.load(experiment_ID, true);
       } catch (PersistenceActionFindException e) {
         throw new ReportInitException("experiment " + experiment_ID + " is unknow.", e);
       } catch (PersistenceActionStoreException e) {
         throw new ReportInitException("experiment " + experiment_ID + " is unknow.", e);
       } catch (PromtCreateException e) {
         throw new ReportInitException("experiment " + experiment_ID + " is unknow.", e);
       } catch (SampleScheduledException e) {
         throw new ReportInitException("experiment can not be loaded -- haha !!", e);
       }
     */
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param promtID TODO
   */
  public void setPromtID(long promtID) {
    this.promtID = promtID;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public long getPromtID() {
    return promtID;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String createHTML() {
    return "this is my report";
  }

  /**
   * create an xls export file containing all information
   *
   * @return link to download this file
   *
   * @throws ReportException
   */
  public String export() throws ReportException {
    FileExport fileExport;

    try {
      fileExport = new XLSFileExport(FileExport.createFileName(this.promtID, "xls"));
      fileExport.export(promt);
      fileExport.createExportFile();

      return fileExport.createDownloadLink();
    } catch (FileExportException e) {
      throw new ReportException(e);
    }
  }
}
