package org.setupx.repository.web.forms.inputfield.multi;

import java.io.File;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Vector;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.CommunicationException;
import org.setupx.repository.core.communication.billing.Billing;
import org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector;
import org.setupx.repository.core.communication.binbase.trigger.ClassSet;
import org.setupx.repository.core.communication.experimentgeneration.MetadataSource;
import org.setupx.repository.core.communication.experimentgeneration.template.ClazzTemplate;
import org.setupx.repository.core.communication.export.reporting.Report;
import org.setupx.repository.core.communication.export.reporting.ReportInitException;
import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.core.communication.importing.logfile.ScannedPair;
import org.setupx.repository.core.communication.leco.ColumnInformation;
import org.setupx.repository.core.communication.leco.MachineRunInfo;
import org.setupx.repository.core.communication.leco.MissingMachineInformationException;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.access.PromtUserAccessRight;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.core.util.xml.DocumentConverter;
import org.setupx.repository.core.util.xml.XMLFile;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionStoreException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;
import org.setupx.repository.web.forms.ArrayShrinkException;
import org.setupx.repository.web.forms.ComparisonsException;
import org.setupx.repository.web.forms.FormContainer;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.FormRootNotAvailableException;
import org.setupx.repository.web.forms.PromtCreateException;
import org.setupx.repository.web.forms.PromtCreator;
import org.setupx.repository.web.forms.clazzes.ClazzDimension;
import org.setupx.repository.web.forms.clazzes.ClazzDimensionDetector;
import org.setupx.repository.web.forms.clazzes.ClazzTemplateMerger;
import org.setupx.repository.web.forms.clazzes.PromtSummary;
import org.setupx.repository.web.forms.inputfield.FormObjectFilter;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.page.Page;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;

/**
 * represents a seriens of pages for storing information inside.
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.24 $
 * @hibernate.subclass clazzdimension detector is beeing moved to multifiledclasses
 */
public class Promt extends FormObject implements FormContainer {
    // ~ Instance fields
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private Billing billing;

    // ~ Static fields/initializers
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private static Promt thiz = new Promt("", "", "");

    static {
        debug(thiz, "static init");
    }

    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new Promt object.
     */
    public Promt() {
        this("", "", "");
    }

    /**
     * Creates a new Promt object.
     */
    public Promt(String name, String description, String longDescription, Page[] pages) {
        this(name, description, longDescription);
        this.setFields(pages);
    }

    /**
     * @param string
     * @param string2
     */
    public Promt(String name, String description, String longDescription) {
        super(name, description, longDescription);

        this.billing = new Billing(this);
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /*
     * (non-Javadoc)
     * 
     * @see org.setupx.repository.web.forms.FormObject#getAttributes()
     */
    public Hashtable getAttributes() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @return
     * @throws FormRootNotAvailableException
     * @deprecated - the clazzdimdetector moved to multifieldclasses
     */
    public ClazzDimensionDetector getClazzDimensionDetector() {
        MultiFieldClazzes multiFieldClazzes = this.findMultifieldClasses();

        try {
            if (multiFieldClazzes == null) {
                throw new FormRootNotAvailableException(
                        "there are NO multifiledclasses in this promt yet. you might want to call it after init of the whole promt is done.");
            }

            return multiFieldClazzes.getClazzDimensionDetector();
        } catch (FormRootNotAvailableException e) {
            e.printStackTrace();

            warning(this,
                    "unable to find a valid ClazzDimensionDetector - so creating a blank one without existing clazzes");

            return new ClazzDimensionDetector(this, null);
        }
    }

    /**
     * @return
     */
    public Clazz[] getClazzes() {
        // checking if these samples are active
        Clazz[] clazzs = this.getClazzDimensionDetector().getClazzes();
        FormObjectFilter filter = new FormObjectFilter(FormObjectFilter.FILTERTYPE_ACTIVE);

        return Clazz.toArray(filter.filter(clazzs));
    }

    /**
     * @return
     */
    public String[] getOwners() {
        Vector fields = new Vector();
        this.findUserInputfields(fields);

        this.log("number of userinputfields found : " + fields.size());

        String[] userDOs = new String[fields.size()];

        try {
            for (int i = 0; i < fields.size(); i++) {
                userDOs[i] = (String) ((UserInputfield) fields.get(i)).email.getValue();
            }
        } catch (Exception e) {
            return new String[0];
        }

        return userDOs;
    }

    /**
     * return just the samples
     */
    public Sample[] getSamples() {
        /** buggy !! */
        Clazz[] clazzes = this.getClazzes();
        Sample[] samples = new Sample[this.numberSamples()];
        int count = 0;
        Sample sample;
        Clazz clazz;
        FormObject[] formObjects;

        for (int i = 0; i < clazzes.length; i++) {
            clazz = clazzes[i];

            formObjects = clazz.getFields();

            for (int j = 0; j < formObjects.length; j++) {
                sample = (Sample) formObjects[j];
                // log(sample);
                samples[count] = sample;
                count++;
            }
        }

        return samples;
    }

    /**
     * called from the JSP
     * 
     * @return summary for the jsps
     */
    public String getSummaryHTML() {
        try {
            PromtSummary summary = this.createSummary();

            return summary.createHTMLTable();
        } catch (Exception e) {
            Logger.warning(this, "unable to create summary");
            e.printStackTrace();

            return "";
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.setupx.repository.web.forms.FormContainer#addField(org.setupx.repository.web.forms.FormObject)
     */
    public void addField(FormObject formObject) {
        this.addInternalField(formObject);
    }

    /**
     * TODO: 
     * 
     * @param promtID
     *                TODO
     * @return TODO
     */
    public static Promt createClone(long promtID) {
        // load the old promt
        // final Promt orignialPromt = Promt.load(promtID);

        // should not return the object - it should be just the id
        return null;

        // erster ansatz:

        /*
         * // take each page and clone it FormObject[] formObjects = orignialPromt.getFields(); FormObject[] clonePages =
         * new FormObject[formObjects.length]; for (int i = 0; i < formObjects.length; i++) { clonePages[i] =
         * ((Page)formObjects[i]).clonePage(); } Promt clonePromt = new Promt(); clonePromt.setFields(clonePages); //
         * add a comment on last page that this is a clone debug("TODO - add comment"); // save it recursiv // check id -
         * can not be the same // return the clone
         */
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.setupx.repository.web.forms.FormObject#getHTML()
     */
    public String createHTML(int col) {
        /*
         * StringBuffer buffer = new StringBuffer(); buffer.append("\n<html><head><title>" + Config.SYSTEM_NAME + " " +
         * this.getName() + "</title></head>"); buffer.append("\n<body><p><h3>" + this.getName() + "</h3></p>");
         * buffer.append("\n<p>" + this.getDescription() + "</p>"); buffer.append("<table width=\"100%\"
         * boder=\"1\">"); buffer.append(this.createHTMLChilds()); buffer.append("</table>"); buffer.append("\n</body></html>");
         * return buffer.toString();
         */
        return null;
    }

    /**
     * loades a promt <br/> in case the id is <code>0</code> the returned Promt will be a new blank promt.
     * 
     * @param id
     *                the promts id
     * @return the promt from the database
     * @throws PromtCreateException
     * @throws PersistenceActionFindException
     * @throws PersistenceActionFindException
     * @throws PersistenceActionStoreException
     */
    public static Promt load(long id) throws PromtCreateException, PersistenceActionFindException,
            PersistenceActionStoreException, SampleScheduledException {
        return load(id, false);
    }

    /**
     * loades a promt <br/> in case the id is <code>0</code> the returned Promt will be a new blank promt.
     * 
     * @param id
     *                the promts id
     * @param ignoreIfScheduled
     *                if true - the promt will be loaded even if the promt has been scheduled
     * @return the promt from the database
     * @throws PromtCreateException
     * @throws PersistenceActionFindException
     * @throws PersistenceActionFindException
     * @throws PersistenceActionStoreException
     */
    public static Promt load(long id, boolean ignoreIfScheduled) throws PromtCreateException,
            PersistenceActionFindException, PersistenceActionStoreException, SampleScheduledException {
        debug(thiz, "trying to load promt with the id: " + id);

        // checking if one of the samples, related to this experiment has been scheduled for a run yet.
        final boolean sampleScheduled = checkIfScheduled(id);
        final boolean sampleRun = checkIfRun(id);

        if (!ignoreIfScheduled && sampleScheduled) {
            throw new SampleScheduledException("This experiment " + id
                    + " has been scheduled for a run on the machine.");
        }

        if (!ignoreIfScheduled && sampleRun) {
            throw new SampleScheduledException("This experiment " + id
                    + " has been run or is in the process of beeing run.");
        }

        Promt promt = null;

        Throwable throwable = null;
        Session session = PersistenceConfiguration.createSessionFactory().openSession();

        if (id != 0L) {
            try {
                session.beginTransaction();
                promt = (Promt) CoreObject.persistence_loadByID(Promt.class, session, id);

                // going to reset all relations
                promt.updateRelations();

                session.close();
            } catch (HibernateException e) {
                session.close();
                throw new PersistenceActionFindException(e);
            } catch (PersistenceActionFindException e) {
                throwable = e;
                session.close();
            }
        } else {
            warning(thiz, "did not start db-connection - cause id is " + id);
        }

        if (promt == null) {
            // in case there was an id that was looked for and it was not found, throw a new exception
            if (id != 0L) {
                throw new PersistenceActionFindException("was not able to load Promt " + id + "  ", throwable);
            }

            // if experiment does not exist, send a blank one
            log(thiz, "did not find a Promt in DB.");
            log(thiz, "creating a new Promt and store it.");

            try {
                promt = PromtCreator.create(PromtCreator.DEFAULTVERSION);
            } catch (MappingError e) {
                throw new PromtCreateException("unable to create new Experiment, because of Mapping problems", e);
            }

            log(thiz, "new promt has the id: " + promt.getUOID());
        }

        return promt;
    }

    /**
     * creates a summary for the jsps
     * 
     * @return summary
     */
    public PromtSummary createSummary() {
        return new PromtSummary(this);
    }

    /**
     * determine <b>ALL</b> dimensions for the creation of classes.
     * 
     * @return dimensions for the creation of classes.
     */
    public ClazzDimension[] determineClazzDimensions() {
        // debug ("----------");
        int numbers = this.getClazzDimensionDetector().getClazzDimensions().length;

        ClazzDimension[] dimensions = this.getClazzDimensionDetector().getClazzDimensions();

        // log(createTable(creater.getClazzDimensions()));
        // debug ("----------");
        this.debug("number of ClazzDimensions: " + dimensions.length + "  ");
        this.debug("number of Clazzes: " + this.getClazzDimensionDetector().getClazzes().length);

        return this.getClazzDimensionDetector().getClazzDimensions();
    }

    /**
     * looked for an inputfiled inside one of the pages and returns the number of the page
     * 
     * <pre>
     * only the &lt;b&gt;first inputfield&lt;/b&gt;s will be used for the determination
     * </pre>
     * 
     * @param values
     *                a hashtable containing different values ( key: name of the inputfiled - value: value of the
     *                inputfiled) - the hashtable must be already filtered
     * @return the number of the page containing the <b>first inputfield</b>
     * @see Promt#findInputFiled(String)
     */
    public int findPageContaingKey(Hashtable values) {
        // name of the last inputfield
        this.debug("--> looking for pageinformation");

        try {
            String firstName = (String) values.keys().nextElement();
            this.debug("<-- looking for pageinformation");

            return this.findPageContaingKey(firstName);
        } catch (NoSuchElementException e) {
            return 9999;
        }
    }

    /**
     * looked for an fieldobject inside the array and returns the position inside the array
     * 
     * @param name
     *                of the field
     * @return the number of the page containing the <b>first inputfield</b>
     */
    public int findPageContaingKey(String key) {
        this.debug("looking for a page containg a key: " + key);

        // name of the last inputfield
        String firstName = key;
        FormObject[] pages = this.getFields();

        for (int i = 0; i < pages.length; i++) {
            Page page = (Page) pages[i];
            Object pageName = page.getField(firstName);
            int pos;

            if (pageName != null) {
                // got the page
                // now find the number of the page
                pos = this.getFieldPos(page.getName());
                debug(this, "found inputfield on page " + pos);

                return pos;
            }
        }

        warning(this, "didnt find the page containing field with the name " + key);

        return 9999;
    }

    /**
     * calculate the number of classes
     * 
     * @return number of classes
     */
    public final int numberClasses() {
        return this.getClazzDimensionDetector().getClazzes().length;
    }

    /**
     * calculate the total number of samples in this promt
     * 
     * @return number of samples
     */
    public final int numberSamples() {
        /*
         * int size = new SXQuery().findSampleIDsByPromtID(this.getUOID()).size(); debug(this, "number of samples: " +
         * size); return size;
         */
        int numOfSamples = 0;
        Clazz[] clazzs = this.getClazzes();

        for (int i = 0; i < clazzs.length; i++) {
            // only for active classes
            if (clazzs[i].isActive()) {
                numOfSamples = numOfSamples + clazzs[i].size();
            }
        }

        return numOfSamples;
    }

    /**
     * TODO: 
     * 
     * @param promtID
     *                TODO
     * @return TODO
     * @throws ReportInitException
     *                 TODO
     */
    public static Report loadReport(long promtID) throws ReportInitException {
        return new Report(promtID);
    }

    /**
     * count number of Promt Objects
     * 
     * @throws PersistenceActionFindException
     */
    public static int persistence_count(Session session) throws PersistenceActionFindException {
        return persistence_count(Promt.class, session);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.setupx.repository.web.forms.FormContainer#remove(org.setupx.repository.web.forms.FormObject)
     */
    public boolean remove(FormObject formObject) throws ArrayShrinkException {
        return this.removeInternalField(formObject.getName());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.setupx.repository.web.forms.FormContainer#remove(java.lang.String)
     */
    public boolean remove(String key) throws ArrayShrinkException {
        FormObject formObject = this.getField(key);

        return this.remove(formObject);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.setupx.repository.web.forms.FormContainer#size()
     */
    public final int size() {
        return this.getFields().length;
    }

    /**
     * TODO: 
     * 
     * @param userDO
     *                TODO
     * @throws PersistenceActionUpdateException
     *                 TODO
     */
    public void save(UserDO userDO) throws PersistenceActionUpdateException {
        Session session = PersistenceConfiguration.createSessionFactory().openSession();

        // saving user and promt
        userDO.setLastExperimentID(this.getUOID());

        try {
            this.update(true);

            userDO.setLastExperimentID(this.getUOID());
            userDO.update(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        PromtUserAccessRight.createFullAccess(this.uoid, userDO.getUOID());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.setupx.repository.web.forms.FormObject#toStringHTML()
     */
    public String toStringHTML() {
        return this.getName() + " " + this.getDescription();
    }

    /**
     * total price for this promt
     * 
     * @return total price
     * @see org.setupx.repository.core.communication.billing.Price#calculate()
     */
    public final String totalPrice() {
        return this.billing.getTotalPrice().calculate();
    }

    /**
     * creates a short description for this experiment.
     * <p>
     * Includes the Title and the Abstract
     * </p>
     * <p>
     * <b> the abstract is beeing shortend to the given size</b>
     * </p>
     */
    public static String createShortAbstractHTML(long promtID, int maxAbstractLength) {
        String promtAbstract;
        String promtTitle;

        try {
            promtTitle = new SXQuery().findPromtTitleByPromtID(promtID);
            promtAbstract = new SXQuery().findPromtAbstractByPromtID(promtID);
        } catch (PersistenceActionFindException e) {
            err(Promt.class, e);
            promtAbstract = "--";
            promtTitle = "--";
        }

        if (promtAbstract.length() > maxAbstractLength) {
            // shorten it
            promtAbstract = promtAbstract.substring(0, maxAbstractLength) + "...";
        }

        return (promtTitle + "<br><font class='small'>" + promtAbstract + "</font>");
    }

    /**
     * TODO: 
     * 
     * @param userDO
     *                TODO
     * @return TODO
     * @throws PersistenceActionFindException
     *                 TODO
     * @throws PromtCreateException
     *                 TODO
     * @throws SampleScheduledException
     */
    public static Promt load(UserDO userDO) throws PersistenceActionException, PromtCreateException,
            SampleScheduledException {
        // this is an illegal id
        long experimentID = 0L;

        try {
            experimentID = userDO.getLastExperimentID();
        } catch (NullPointerException e) {
            // user seems to be null - get a clean promt
        }

        Promt promt = null;

        try {
            promt = Promt.load(experimentID);

            // checking if it the correct experiment
            if ((experimentID != 0) && (experimentID != promt.getUOID())) {
                throw new RuntimeException("trying to return the wrong experiment ! " + experimentID + " "
                        + promt.getUOID());
            }
        } catch (PersistenceActionFindException e) {
            e.printStackTrace();
            warning(thiz, "unable to load the experiment with the ID : " + experimentID + "  so creating a new one.");
            promt = Promt.load(0L);
        } catch (SampleScheduledException e) {
            // the sample has been scheduled already - so no modifications anymore
            promt = Promt.load(0L);
        }

        return promt;
    }

    /**
     * @return if the whole promt is finally accepted by the user for submitting
     */
    public boolean isComplete() {
        boolean result = true;

        // check every page
        for (int i = 0; i < this.getFields().length; i++) {
            if (this.getFields()[i].isActive()) {
                result = result && this.getFields()[i].isValid();
            }
        }

        // if every page is valid, return true
        return result;
    }

    /**
     * Load a promt containing a sampleID
     * 
     * @param sampleID
     * @return
     * @throws PersistenceActionFindException
     */
    public static Promt loadBySampleID(Session session, long sampleID) throws PersistenceActionFindException {
        /***************************************************************************************************************
         * select from formobject as promt, formobject as child4, formobject as child3, formobject as child2, formobject
         * as child1 where promt.discriminator = "org.setupx.repository.web.forms.inputfield.multi.Promt" and
         * child1.parent = promt.uoid and child2.parent = child1.uoid and child3.parent = child2.uoid and child4.parent =
         * child3.uoid and child4.discriminator = "org.setupx.repository.web.forms.inputfield.multi.Sample"
         */
        String basic = "select promt from " + Promt.class.getName() + " as promt, " + FormObject.class.getName()
                + " as child4, " + FormObject.class.getName() + " as child3, " + FormObject.class.getName()
                + " as child2, " + FormObject.class.getName() + " as child1  " + "where child1.parent = promt "
                + "and child2.parent = child1 " + "and child3.parent = child2 " + "and child4.parent = child3 "
                + "and child4 = '" + sampleID + "'";

        Query query = session.createQuery(basic);

        List list = query.list();

        if (list.size() != 1) {
            Iterator iterator = list.iterator();

            while (iterator.hasNext()) {
                log(thiz, "" + iterator.next());
            }

            throw new PersistenceActionFindException("unable to find promt for sampleID: " + sampleID
                    + " (if promt structure has changed - check query) - results found: " + list.size());
        } else {
            return (Promt) list.get(0);
        }
    }

    /**
     * TODO: 
     * 
     * @return TODO
     */
    public long[] getSampleIDs() {
        /*
         * List list = new SXQuery().findSampleIDsByPromtID(this.getUOID()); Iterator iterator = list.iterator(); long[]
         * result = new long[list.size()]; int i = 0; while (iterator.hasNext()) { result[i] = Long.parseLong("" +
         * iterator.next()); i++; } return result;
         */

        Sample[] samples = this.getSamples();
        long[] ids = new long[this.getSamples().length];
        for (int i = 0; i < samples.length; i++) {
            Sample sample = samples[i];
            ids[i] = sample.getUOID();
        }
        return ids;
    }

    /**
     * TODO: 
     * 
     * @param question
     *                TODO
     * @return TODO
     */
    public InputField[] determineInputfields(String question) {
        warning(this, "TODO");

        return new InputField[] {};
    }

    /**
     * TODO: 
     * 
     * @param promtID
     *                TODO
     * @throws CommunicationException
     *                 TODO
     * @throws PersistenceActionFindException
     *                 TODO
     */
    public static void export(final long promtID, final ColumnInformation colInfo) throws CommunicationException,
            PersistenceActionFindException {
        export(promtID, colInfo, true);
    }

    public static void export(final long promtID, final ColumnInformation colInfo, boolean includeImport)
            throws CommunicationException, PersistenceActionFindException {
        debug(thiz, "starting export for " + promtID + " on column: " + colInfo.getLable() + " ");

        List clazzIDList = new SXQuery().findClazzIDsByPromtID(promtID);

        // warning(thiz, "EXPORT deactivated.");
        export("" + promtID, colInfo, clazzIDList, includeImport);
    }

    /**
     * TODO: 
     * 
     * @param promtIDs
     *                TODO
     * @param colInfo
     *                TODO
     * @throws PersistenceActionFindException
     *                 TODO
     * @throws CommunicationException
     *                 TODO
     */
    public static void export(final long[] promtIDs, final ColumnInformation colInfo)
            throws PersistenceActionFindException, CommunicationException {
        debug(thiz, "starting export for " + promtIDs.length + " experiments on column: " + colInfo.getLable() + " ");

        Vector totalClasses = new Vector();

        for (int i = 0; i < promtIDs.length; i++) {
            long promtID = promtIDs[i];
            List clazzIDList = new SXQuery().findClazzIDsByPromtID(promtID);
            totalClasses.addAll(clazzIDList);
        }

        export("c" + promtIDs.length, colInfo, totalClasses);
    }

    /**
     * @return
     * @see Promt#determineRelatedFields(long)
     */
    public final File[] determineRelatedFields() {
        return determineRelatedFields(this.uoid);
    }

    /**
     * checking if there are existing files that are related to this experiment
     * 
     * @return all documents in that experiment folder
     */
    public final static File[] determineRelatedFields(long promtID) {
        File directory = new File(Config.DIRECTORY_RELATED + File.separator + promtID);

        if (!directory.exists()) {
            // create the directory
            directory.mkdir();
        }

        return directory.listFiles();
    }

    /**
     * assign the templates of metadatasource to the clazzes in this promt
     */
    public void assignClazzTemplates(final MetadataSource metadataSource) throws MappingError {
        debug(this, "assigning clazztemplates to the real promt");

        final ClazzTemplate[] clazzTemplates = metadataSource.getClazzTemplates();

        // take each clazztemplate - and find the matching one in the new promt
        Clazz[] clazzs = this.getClazzes();
        ClazzTemplateMerger.merge(clazzs, clazzTemplates);
    }

    /**
     *
     */
    public void updateClazzDimDetector() {
        this.getClazzDimensionDetector().update();
    }

    /**
     * TODO: 
     * 
     * @throws PersistenceActionFindException
     *                 TODO
     */
    void bl() throws PersistenceActionFindException {
        int promtID = 0;

        List classList = new SXQuery().findClazzIDsByPromtID(promtID);
        Iterator iterator = classList.iterator();

        // each class
        while (iterator.hasNext()) {
            int classID = Integer.parseInt("" + iterator.next());

            // class labels
            java.util.Iterator iterator1 = new SXQuery().findLabelsforClazz(classID).iterator();

            while (iterator1.hasNext()) {
                Object[] element = (Object[]) iterator1.next();
                log(null, element[0].toString());
                log(null, element[1].toString());
            }

            // samples in that class
            Iterator sampleIDs = new SXQuery().findSampleIDsByClazzID(classID).iterator();

            while (sampleIDs.hasNext()) {
                int sampleID = Integer.parseInt("" + sampleIDs.next());
                String label = new SXQuery().findSampleLabelBySampleID(sampleID);

                Iterator scannedPairs = new SXQuery().findScannedPair(sampleID).iterator();
                boolean checked = true;

                while (scannedPairs.hasNext()) {
                    ScannedPair pair = (ScannedPair) scannedPairs.next();
                    String acqname = pair.getLabel();
                    if (checked) {
                        checked = false;
                    }
                }
            }
        }
    }

    /**
     * checks if samples in this experiment have been run yet
     * 
     * @param id
     * @return
     */
    private static boolean checkIfRun(long promt_id) {
        return new SXQuery().determinePromtRunStarted(promt_id);
    }

    /**
     * checking if one of the samples, related to this experiment has been scheduled for a run yet.
     * 
     * @param id
     * @return
     */
    private static boolean checkIfScheduled(long id) {
        warning(thiz, "checkIfScheduled(long id)  not implemented yet.");

        // query: select * from where sample not containing machinescheduled informaiton
        return false;
    }

    private static void export(String label, final ColumnInformation colInfo, List clazzIDList)
            throws PersistenceActionFindException, CommunicationException {
        export(label, colInfo, clazzIDList, true);
    }

    public static void export(String label, final ColumnInformation colInfo, List clazzIDList, boolean includeImport)
            throws PersistenceActionFindException, CommunicationException {
        Iterator iterator = clazzIDList.iterator();

        ClassSet classSet = new ClassSet(colInfo);
        classSet.setColInfo(colInfo);
        classSet.setId(label);

        ExperimentClass[] experimentClasses = new ExperimentClass[clazzIDList.size()];
        int j = 0;

        while (iterator.hasNext()) {
            int clazzID = ((Long) iterator.next()).intValue();

            ExperimentClass experimentClass = new ExperimentClass();
            experimentClass.setId("" + clazzID);
            experimentClass.setIncrease(0);
            experimentClass.setSamples(new ExperimentSample[0]);

            List sampleList = new SXQuery().findSampleIDsByClazzID(clazzID);

            ExperimentSample[] experimentSamples = new ExperimentSample[sampleList.size()];
            Iterator sampleIter = sampleList.iterator();
            int i = 0;

            while (sampleIter.hasNext()) {
                int sampleID = ((Long) sampleIter.next()).intValue();
                String acqName = new SXQuery().findAcquisitionNameBySampleID(sampleID);
                ExperimentSample experimentSample = new ExperimentSample();
                experimentSample.setId(sampleID + "");
                experimentSample.setName(acqName);
                experimentSamples[i] = experimentSample;
                i++;
            }

            experimentClass.setSamples(experimentSamples);
            experimentClasses[j] = experimentClass;
            j++;
        }

        // addition
        // loop over classes and check for empty and null classes
        Logger.debug(thiz, "checking experimentClasses for export:  current number of classes: "
                + experimentClasses.length);
        HashSet set = new HashSet();
        for (int i = 0; i < experimentClasses.length; i++) {
            if (experimentClasses[i] != null && experimentClasses[i].getSamples() != null
                    && experimentClasses[i].getSamples().length > 0) {
                set.add(experimentClasses[i]);
            } else {
                warning(null, "did contain an empty class - now removed.");
            }
        }

        ExperimentClass[] result = new ExperimentClass[set.size()];
        Iterator e = set.iterator();
        for (int i = 0; e.hasNext(); i++) {
            result[i] = (ExperimentClass) e.next();
        }
        Logger.debug(thiz, "checking experimentClasses for export:  updated number of classes: " + result.length);

        classSet.setClasses(result);

        // ExperimentClass[] experimentClasses = Clazz.toBBExperimentClasses(this.getClazzes());
        // experiment.setClasses(experimentClasses);
        // creating a binbase service
        BinBaseServiceConnector binBaseSerivce = new BinBaseServiceConnector(Config.BINBASE_SERVER);

        // start export
        binBaseSerivce.launchExport(classSet, includeImport);
    }

    public static void export(long promtID) throws MissingMachineInformationException, PersistenceActionFindException,
            CommunicationException {
        // check if the experiment is done.
        if (new SXQuery().determinePromtFinished(promtID)) {
            Logger.log(thiz, "Experiment ready for export: " + promtID);
            ColumnInformation colInfo = MachineRunInfo.determineMachineRunInfoByPromt(promtID).getColumnInformation();
            export(promtID, colInfo);
        } else {
            Logger.log(thiz, "Experiment is not finished yet: " + promtID);
        }
    }

    /**
     * compare two promts.
     * 
     * @param promt
     * @param oldPromt
     * @throws ComparisonsException
     */
    public static void compare(Promt promt1, Promt promt2) throws ComparisonsException {
        promt1.compare(promt2);
    }

    /*
     * added an update of the ClazzdimensionDetector (non-Javadoc)
     * 
     * @see org.setupx.repository.core.CoreObject#update(org.hibernate.Session, boolean)
     */
    public void update(Session session, boolean createIt) throws PersistenceActionUpdateException {
        this.determineClazzDimensions();
        super.update(session, createIt);
    }

    public String getXMLChildname() {
        return "experiment";
    }

    public XMLFile createXML() throws Exception {
        Document doc = DocumentConverter.createNewDocument();
        doc.appendChild(doc.createElement("root"));
        XMLFile xmlfile = null;

        Element element = doc.getDocumentElement();
        // create a root object
        // Element element = doc.createElement("root");

        // load a promt
        Node promtNode = this.createXMLNode(doc);

        // attach all childs
        element.appendChild(promtNode);

        // create xml file
        xmlfile = new XMLFile(doc);
        xmlfile.storeDocument(java.io.File.createTempFile("sx", "sx"));

        return xmlfile;
    }

    public static void testClone() {

        // clone it
        // Promt promt2 = Promt.createClone(id);

        // check if they are identical

    }
}
