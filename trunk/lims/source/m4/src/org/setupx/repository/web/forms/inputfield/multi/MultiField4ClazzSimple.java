/**
 * ============================================================================ File:    MultiField4ClazzSimple.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.Config;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.forms.FormObject;


/**
 * @hibernate.subclass
 */
public abstract class MultiField4ClazzSimple extends MultiField4Clazz {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new MultiField4ClazzSimple object.
   */
  public MultiField4ClazzSimple() {
    super();
  }

  /**
   * Creates a new MultiField4ClazzSimple object.
   *
   * @param question 
   * @param description 
   */
  public MultiField4ClazzSimple(String question, String description) {
    super(question, description);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTML()
   */
  public String createHTML(int col) {
    if (Config.NEW_DESIGN) {
      return super.createHTML(col);
    }

    StringBuffer response = new StringBuffer();

    // create a table containing 
    response.append("<!-- " + this.getName() + " (" + this.getDescription() + ") -->");
    response.append("<tr><td colspan=\"" + (col) + "\" align=\"center\">" + "<table " + WebConstants.BORDER + " width=\"100%\">\n");

    // cols within this table 
    int cols = 4;

    // header
    //response.append(htmlCreateHeader(cols));
    // anchor
    response.append(this.createAnchor());

    // put every inputfield in there
    response.append(this.createHTMLChilds(cols));

    // add a help button
    response.append(this.createHelpButton());

    // close the table
    response.append("</table></td></tr>");

    return response.toString();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTMLChilds(int)
   */
  public String createHTMLChilds(int numberOfColums) {
    if (Config.NEW_DESIGN) {
      return super.createHTMLChilds(numberOfColums);
    }

    StringBuffer buffer = new StringBuffer();

    // every field
    for (int i = 0; i < this.getFields().length; i++) {
      FormObject formObject = this.getFields()[i];

      // only active objects will be shown
      if (formObject.isActive()) {
        buffer.append("\n<!-- START -- " + formObject.toString() + " -->\n");
        buffer.append(formObject.createHTML(numberOfColums));
        buffer.append("\n<!-- END -- " + formObject.toString() + " -->\n");
      }
    }

    return buffer.toString();
  }
}
