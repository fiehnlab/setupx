package org.setupx.repository.core.communication.file;

import java.io.File;

import org.setupx.repository.Config;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;


/**
 * different files for a certain sample
 */
public class SampleFile{
    
    public static final short PEG = 0;
    public static final short TXT = 1;
    public static final short SMP = 2;
    public static final short CDF = 3;

    public static final short[] TYPES = new short[]{PEG,TXT,SMP,CDF};

    private String fileBasename;

    public SampleFile(String i_acqname){
        this.fileBasename = i_acqname;
        try {
            this.fileBasename = this.fileBasename.replace(':','_');
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public SampleFile(long sampleID) throws PersistenceActionFindException{
        this(new SXQuery().findAcquisitionNameBySampleID(sampleID));        
    }

    public boolean exists(short fileType){
        return getFile(fileType).exists();
    }

    public File getFile(short fileType) {
        return new java.io.File(this.getFilenameFull(fileType));
    }


    /**
     * it ll give you a pre checked version of the path - depending if one of the extensions exists ...
     */
    private String getFilenameFull(short fileType) {
        String path = this.getPath(fileType);
        String extension = this.getExtension(fileType);
        String _tmppath = path.concat(File.separator).concat(this.getACQName()).concat(".");
        
        String absolutPath = _tmppath.concat(extension);
        
        // tries different spellings
        if (new File(absolutPath).exists()){
            return absolutPath;
        } else {
            return _tmppath.concat(extension.toUpperCase());
        }
    }

    public static String getExtension(short fileType) {
        String extension = "";
        switch (fileType) {
        case PEG:
            extension = Config.SAMPLEFILE_EXTENSION_PEG;
            break;
        case TXT:
            extension = Config.SAMPLEFILE_EXTENSION_TXT;
            break;
        case SMP:
            extension = Config.SAMPLEFILE_EXTENSION_SMP;
            break;
        case CDF:
            extension = Config.SAMPLEFILE_EXTENSION_CDF;
            break;
        default:
            break;
        }

        return extension;
    }


    public String getPath(short fileType) {
        String path = "";
        switch (fileType) {
        case PEG:
            path = Config.DIRECTORY_SAMPLEFILE_PEG;
            break;
        case TXT:
            path = Config.DIRECTORY_SAMPLEFILE_TXT;
            break;
        case SMP:
            path = Config.DIRECTORY_SAMPLEFILE_SMP;
            break;
        case CDF:
            path = Config.DIRECTORY_SAMPLEFILE_CDF;
            break;

        default:
            break;
        }
        return path;
    }


    public boolean existsAll(){
        return (
                exists(PEG)
                && exists(TXT)
                && exists(SMP)
                && exists(CDF)
        );
    }


    public static boolean exists(String acqname, short type){
        SampleFile fileCheck = new SampleFile(acqname);
        return fileCheck.exists(type);
    }

    public static boolean exists(long sampleID, short type){
        SampleFile fileCheck;
        try {
            fileCheck = new SampleFile(sampleID);
            return fileCheck.exists(type);
        } catch (PersistenceActionFindException e) {
            Logger.warning(null, "proplems in SampleFile: " + e);
            return false;
        }
    }

    public static boolean exists(int sampleID, short type){
        return exists(Long.parseLong("" + sampleID), type);
    }
    
    public String getACQName(){
        return this.fileBasename;
    }
}