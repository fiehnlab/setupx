/**
 * ============================================================================ File:    Clazz.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.LazyInitializationException;

import org.setupx.repository.Config;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.FormRootNotAvailableException;
import org.setupx.repository.web.forms.ParentNotAvailableException;
import org.setupx.repository.web.forms.clazzes.ClazzDimension;
import org.setupx.repository.web.forms.inputfield.InputField;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;


/**
 * A Clazz defines a specific set of treatment, biological source and other attributes that have influence on a sample.
 * 
 * <p>
 * The number of classes is determined by the CalculationExtension of the ClazzDimensionDetector.
 * </p>
 * 
 * <p>
 * the number of MultiField4Clazzes is similar to the number of Clazzdimensions.
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 *
 * @hibernate.subclass
 *
 * @see org.setupx.repository.web.forms.MultiField4Clazz
 * @see org.setupx.repository.web.forms.CalculationExtension#createClazz(ClazzDimension[])
 * @see org.setupx.repository.web.forms.ClazzDimensionDetector
 */
public class Clazz extends MultiFieldExpanding {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** fields that contain the information for this clazz. - these are <B>NOT</B> the fields contained inside this  object. */
  private MultiField4Clazz[] multiField4Clazzs = new MultiField4Clazz[0];

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final int DEFAULT_NUMBER_SAMPLES = 6;
  private static final int MINUMUM_NUMBER_SAMPLES = 6;
  private static int lastid = 0;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Clazz object.
   */
  public Clazz() {
    this(new MultiField4Clazz[0], 0);
  }

  /**
   * create a new Clazz object
   *
   * @param fields all fields that the clazz is related to
   * @param numberOfSamples the number of samples that will be generated for this clazz
   */
  public Clazz(MultiField4Clazz[] fields, int numberOfSamples) {
    super("Class", "Class", "", null);

    this.keyLabel = "add sample to this Class";

    this.setMultiField4Clazzs(fields);
    debug("number of related fields (ARRAY): " + this.multiField4Clazzs().length);

    if (this.multiField4Clazzs().length < 1) {
      return;
    }

    // modify the number of samples only of it is not correct
    if (this.getFields().length != numberOfSamples) {
      this.setFields(this.createChilds(numberOfSamples));
    }

    /*this.setLongDescription("This class contains a set of samples. By default there are " + Clazz.DEFAULT_NUMBER_SAMPLES +
       " samples per clas. If you want to add additional samples just type in the number of samples that you want to add and press the button next to it.<br>In case that there are more samples listed in this class than you actually have for your experiment, you might remove them by pressing the " +
       FormObject.removeIMG + "-Button");
     */
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param set TODO
   */
  public void setMultiField4ClazzesHibernate(Set set) {
    //debug("setMultiField4ClazzesHibernate(Set set) " + set.size());
    multiField4Clazzs = mapToMultiField4Clazz(set);
  }

  /**
   * @hibernate.set
   * @hibernate.collection-key column = "multiField4Clazzs"
   * @hibernate.collection-many-to-many  class="org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz"
   */
  public Set getMultiField4ClazzesHibernate() {
    //debug("getMultiField4ClazzesHibernate()" + multiField4Clazzs.length);
    return mapFromMultiField4Clazzes(this.multiField4Clazzs);
  }

  /**
   * @param fields
   */
  public void setMultiField4Clazzs(MultiField4Clazz[] fields) {
    this.multiField4Clazzs = fields;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public MultiField4Clazz[] getMultiField4Clazzs() {
    return multiField4Clazzs;
  }

  /**
   * mapping
   */
  public final static Clazz[] convertToClazz(FormObject[] fields) {
    Clazz[] clazzs = new Clazz[fields.length];

    for (int i = 0; i < fields.length; i++) {
      clazzs[i] = (Clazz) fields[i];
    }

    return clazzs;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.MultiFieldExpanding#createExpandButtonADD()
   */
  public String createExpandButtonADD() {
    String msg = "add an other item";
    StringBuffer buffer = new StringBuffer();
    buffer.append("form?");
    buffer.append("number_" + this.getName());
    buffer.append("=");
    buffer.append("1");
    buffer.append("&");
    buffer.append(WebConstants.PARAM_FORM_TARGET);
    buffer.append("=");

    // HOTFIX
    // buffer.append("" + this.parentPage().getPosThis());
    try {
      if (this.parentPage().getPosThis() != 4) {
        throw new RuntimeException("the clazzed seem not to be on 4");
      }

      buffer.append("" + this.parentPage().getPosThis());
    } catch (ParentNotAvailableException e) {
      e.printStackTrace();
      err(this, e);
    }

    // Clazz does not have an parent
    // buffer.append(this.parentPage().getPosThis());
    return "<a href=\"" + buffer.toString() + "\"	title=\"" + msg + "\">" + IMG_PLUS + "</a>  ";
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String createExpandButtonREDUCE() {
    //int target_page = this.parentPage().getPosThis();
    return ((Sample) lastItem()).createRemoveButton("remove", IMG_MINUS, 4);
  }

  /*
     public String createSummary() {
     StringBuffer buffer = new StringBuffer();
     buffer.append("Class:<br>");
     if (comment.length() > 1) buffer.append("(" + comment + ")");
     for (int i = 0; i < fields.length; i++) {
         MultiField field= fields[i];
         buffer.append(field.toStringHTML() + " <br>");
     }
     buffer.append("Samples<br>");
     for (int i = 0; i < samples.length; i++) {
         Sample sample = samples[i];
         buffer.append(sample.toStringHTML() + " <br>");
     }
     return buffer.toString();
     }*/
  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTML()
   */
  public String createHTML(int col) {
   
    StringBuffer response = new StringBuffer();

    int numberOfColumns = Sample.numberOfAttributes + 1;

    // create a table containing 
    response.append("<!-- " + this.getName() + " (" + this.getDescription() + ") -->");
    response.append("<tr><td colspan=\"" + col + "\" align=\"center\">");

    response.append("<table  " + WebConstants.BORDER + " width=\"100%\">\n");

    // header
    response.append("<th colspan=\"" + numberOfColumns + "\">" + this.getDescription() + " (id:" + this.getUOID() + ")</th>\n");

    // class desctiption
    response.append(Util.replace(new SXQuery().findClassInformationString(this.getUOID()), "\n", "<BR>")); 
    
    // long description
    response.append("<tr><td colspan=\"" + numberOfColumns + "\"><font class=\"small\">" + this.getLongDescription() + "</font></td></tr>");

    /*
       // deactivation for the whole class
       response.append("<tr><td colspan=\"" + numberOfColumns + "\"><font class=\"small\">In case you have no samples for this class (combination of these " + this.multiField4Clazzs.length +
         " factors) you can deactivate remove the whole class by pressing the button.</font>" + this.createActivateButton() + "<br>" +
         "If you want to add samples for this class after you deactivated this class, you can activate the class again by pressing the corresponding icon in the table on top of this page.</td></tr>");
     */

    // in case the number of samples is less than the minimum - error message
    int numOfClasses = calculateNumberOfSamples();

    if (numOfClasses < MINUMUM_NUMBER_SAMPLES) {
      response.append("<tr><td colspan=\"" + numberOfColumns + "\"><span class=\"error\">The number of samples for this Class is less than the minimum of " + DEFAULT_NUMBER_SAMPLES + ". <p>" + "Please get in contact with the Core Lab Manager. " + Config.OPERATOR_MANAGER.toHTMLLink() +
        ".</span></td></tr>");
    }

    
    /*
    //response.append(toStringHTML());
    // kein unterschied zu response.append(toStringHTML()); ODER ? FIXME
    // link all Multifield4clazzes this Clazz was created from
    for (int i = 0; i < this.getMultiField4Clazzs().length; i++) {
      InputField field = (InputField) this.getMultiField4Clazzs()[i];

      // HOTFIX
      try {
        if (field.isActive()) {
          // leave the field small - will be resized 
          response.append("<tr><td colspan=\"" + numberOfColumns + "\">");

          response.append(field.createLinkedIMG());

          response.append("\n<!-- " + Util.getClassName(this) + " -->\n");
          response.append(field.toStringHTML());

          response.append("</td></tr>");
        }
      } catch (LazyInitializationException e) {
        warning(e);
      }
    }
    */

    try {
        if (this.getUOID() == 0) this.root().update(true);
    }catch (Exception e){
        
    }
    //response.append(new SXQuery().findLabelsforClazz(this.uoid));
    
    
    /*
       response.append("<tr><td colspan=\"" + numberOfColumns + "\"><font class=\"small\">" + this.getLongDescription() + "</font></td></tr>");
       response.append(navi.createHTML(numberOfColumns));
       response.append("<tr><td colspan=\"" + numberOfColumns + "\"><font class=\"small\">detailed view of this Clazz: " + this.createDetailButton() + "</font></td></tr>");
     */
    // detail button
    //response.append("<tr><td colspan=\"" + numberOfColumns + "\"><font class=\"small\">detailed view of this Clazz: " + this.createDetailButton() + "</font></td></tr>");

    // anchor
    response.append(this.createAnchor());

    // expand button
    response.append("<th>" + createExpandButton() + createAutofillButton() + "</th>");

    // NEW reinit the header
    ((Sample)this.getField(0)).resetHTMLheader();
    for (int s = 0; s < this.getSamples().size(); s++){
        ((Sample)this.getField(s)).initTableLabels();

    }
    
    // header of samples
    response.append(Sample.createHTMLheader());

    // every sample
    for (int i = 0; i < this.getFields().length; i++) {
      //log(this.getFields()[i]);
      Sample sample = (Sample) this.getFields()[i];

      // only active objects will be shown
      if (sample.isActive()) {
        response.append("\n<!-- START -- " + sample.toString() + " -->\n");
        response.append(sample.createHTML(8));
        response.append("\n<!-- END -- " + sample.toString() + " -->\n");
      }
    }

    // add a autofill button
    //response.append(this.createAutofillButton());
    // add a help button
    response.append(this.createHelpButton());

    // close the table
    response.append("</table></td></tr>");

    return response.toString();
  }

  /**
   * mapping
   */
  public final static MultiField4Clazz[] mapToMultiField4Clazz(Set set) {
    MultiField4Clazz[] results = new MultiField4Clazz[set.size()];
    Iterator iterator = set.iterator();

    MultiField4Clazz f = null;

    int i = 0;

    while (iterator.hasNext()) {
      f = (MultiField4Clazz) iterator.next();
      results[i] = f;
      i++;
    }

    return results;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public HashSet getSamples() {
    HashSet result = new HashSet();
    FormObject[] formObjects = this.getFields();

    for (int i = 0; i < formObjects.length; i++) {
      if (formObjects[i] instanceof Sample) {
        Sample sample = ((Sample) formObjects[i]);

        if (sample.isActive()) {
          result.add(sample);
        }
      }
    }

    return result;
  }

  /**
   * creates an Array of Samples related to this Clazz.
   *
   * @param numberOfSamples the number of Samples that will be generated
   *
   * @return an Array of Samples
   */
  public Sample[] createChilds(int numberOfSamples) {
    debug("createSamples(int numberOfSamples):" + numberOfSamples + "  - number of existing samples: " + this.getFields().length);
    warning(this, "creating new instance - TODO : replace it with checking if instance of combination of these two already exists !!!   ! !                  !");

    Sample[] samples = new Sample[numberOfSamples];

    for (int i = 0; i < samples.length; i++) {
      samples[i] = new Sample();

      try {
        samples[i].cacheRoot(this.root());
      } catch (org.setupx.repository.web.forms.FormRootNotAvailableException e) {
        debug("unable to precache root " + e.getLocalizedMessage());
      }

      samples[i].setRemovable(true);
    }

    return samples;
  }

  /**
   * @return multiField4Clazzs related to this clazz
   */
  public MultiField[] multiField4Clazzs() {
    return this.multiField4Clazzs;
  }

  /**
   * determines if all the MultiField4Clazzes are related to this Clazz
   *
   * @return true if this clazz is related to all of the the givven MultiField4Clazz
   */
  public boolean relatedTo(MultiField4Clazz[] i_fields) {
    for (int i = 0; i < i_fields.length; i++) {      
      if (this.relatedTo(i_fields[i])) {
          //debug("related to " + i_fields[i].getUOID() );
      } else if (!i_fields[i].active){
          //debug("ignoring " + i_fields[i].getUOID() + " because inactive." );
      } else {
          //debug("not related " + + i_fields[i].getUOID());
        return false;
      }
    }
    //debug("match for " + i_fields.length + " on this:" + this.getUOID());
    return true;
  }

  /**
   * determines if the MultiField4Clazz is related to this Clazz
   *
   * @return true if this clazz is related to the givven MultiField4Clazz
   */
  public boolean relatedTo(MultiField4Clazz field) {
    //debug(this, "looking for: " + field);
    for (int i = 0; i < this.multiField4Clazzs.length; i++) {
      //debug(this, " clazzHash:" + this.hashCode() + " " + i + "    looking for: " + field.hashCode() + "   field contained:" + multiField4Clazzs[i].hashCode());
        try {
            if (this.multiField4Clazzs[i].getName() == field.getName()) {
                //debug("comparing " + this.multiField4Clazzs[i].getUOID() + " vs. " + field.getUOID());
                return true;
            }
        } catch (org.hibernate.LazyInitializationException e) {
            warning(this, "unable to perform comparisson: this.multiField4Clazzs[" + i + "]  no accessible.");
            warning(e);
        }
    }
    return false;
  }

  /**
   * TODO: 
   *
   * @param objects TODO
   *
   * @return TODO
   */
  public static Clazz[] toArray(FormObject[] objects) {
    Clazz[] clazzs = new Clazz[objects.length];

    for (int i = 0; i < objects.length; i++) {
      clazzs[i] = (Clazz) objects[i];
    }

    return clazzs;
  }

  /**
   * TODO: 
   *
   * @param clazzes TODO
   *
   * @return TODO
   */
  public static ExperimentClass[] toBBExperimentClasses(Clazz[] clazzes) {
    ExperimentClass[] experimentClasses = new ExperimentClass[clazzes.length];

    for (int i = 0; i < clazzes.length; i++) {
      Clazz clazz = clazzes[i];
      experimentClasses[i] = clazz.toBBExperimentClass();
    }

    return experimentClasses;
  }

  /**
   * mapps a clazz onto an ExperimentClazz
   *
   * @return
   */
  public ExperimentClass toBBExperimentClass() {
    ExperimentClass experimentClass = new ExperimentClass();
    experimentClass.setIncrease(0);
    experimentClass.setId("" + this.uoid);

    Iterator iterator = this.getSamples().iterator();
    int i = 0;
    ExperimentSample[] experimentSamples = new ExperimentSample[this.getSamples().size()];

    while (iterator.hasNext()) {
      Sample sample = (Sample) iterator.next();
      experimentSamples[i] = sample.toExperimentSample();
      i++;
    }

    experimentClass.setSamples(experimentSamples);

    return experimentClass;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#toStringHTML()
   */
  public String toStringHTML() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("<table " + WebConstants.BORDER + "><tr><td rowspan=\"" + (multiField4Clazzs.length + 1) + "\">");
    buffer.append("<b>" + this.getQuestion() + "ID: " + this.getUOID() + "</b>");
    buffer.append("</td><td></td></tr>");

    // create a back link to each multifield
    for (int i = 0; i < this.multiField4Clazzs.length; i++) {
      MultiField4Clazz multiField4Clazz = this.multiField4Clazzs[i];

      if (multiField4Clazz.isActive() && multiField4Clazz.getParent().getFields().length > 1) {
        buffer.append("<tr><td>");
        buffer.append(multiField4Clazz.createLinkedIMG());
        buffer.append(multiField4Clazz.toStringHTML());
        buffer.append("</td></tr>");
      }
    }

    buffer.append("</table>");

    return buffer.toString();
  }

  /**
   * @return a calculated number of samples (checks if samples are active, ...) and makes a sum out of them
   */
  private int calculateNumberOfSamples() {
    int num = 0;

    for (int i = 0; i < this.getFields().length; i++) {
      //log(this.getFields()[i]);
      Sample sample = (Sample) this.getFields()[i];

      if (sample.isActive()) {
        num++;
      }
    }

    return num;
  }

  /**
   * @return
   *
   * @deprecated use UOID - generated by the database.
   */
  private static int createID() {
    lastid++;

    return lastid - 1;
  }

  /**
   * mapping
   */
  private Set mapFromMultiField4Clazzes(MultiField4Clazz[] multiField4Clazzs) {
    Set result = new HashSet();

    for (int i = 0; i < multiField4Clazzs.length; i++) {
      result.add(multiField4Clazzs[i]);
    }

    return result;
  }
  
  /*
   * (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#getXMLChildname()
   */
  public final String getXMLChildname() {
      return "class";
  }
  
  public Hashtable getExportElements() {
      Hashtable hashtable = super.getExportElements();
      try {
            this.root().getClazzDimensionDetector().update();
        } catch (FormRootNotAvailableException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        log("this.getMultiField4Clazzs().size", "" + this.getMultiField4Clazzs().length);
        for (int i = 0; i < this.getMultiField4Clazzs().length; i++) {          
          hashtable.put("originatorID_" + i, "" + this.getMultiField4Clazzs()[i].getUOID());
      }
      return hashtable; 
  }


  public void setActive(boolean activation){

/*      debug("changing activation form : " + this.isActive() + " to: " + activation );
      // just to figure out who called the activation
      try {
          throw new NullPointerException("DEV EXCEPTION...");
      } catch (NullPointerException e) {
          e.printStackTrace();
      }
*/      super.setActive(activation);

//      debug("new activation is : " + this.isActive() + " and was supposed to be: " + activation);
  }
}
