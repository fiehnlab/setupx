/**
 * ============================================================================ File:    Restriction4Int.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

/**
 * limitation for Ints
 *
 * @hibernate.subclass
 *
 * @see java.lang.Integer#parseInt(java.lang.String)
 */
public class Restriction4Int extends Restriction4Numbers {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final long serialVersionUID = 4669457899301683196L;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Restriction4Int object.
   */
  public Restriction4Int() {
    super();
  }

  /**
   * Creates a new Restriction4Int object.
   *
   * @param startRange 
   * @param endRange 
   */
  public Restriction4Int(int startRange, int endRange) {
    super(startRange, endRange);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction4Numbers#getErrorMessage()
   */
  public String getErrorMessage() {
    return "The value should be whole number. Fractions are not allowed.";
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction4Numbers#parse(java.lang.String)
   */
  public double parse(String s) throws NumberFormatException {
    return Integer.parseInt(s);
  }
}
