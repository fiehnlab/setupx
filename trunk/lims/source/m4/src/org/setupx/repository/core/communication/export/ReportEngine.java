
package org.setupx.repository.core.communication.export;

import org.setupx.repository.core.communication.export.xls.XLSFileExport;
import org.setupx.repository.core.communication.exporting.ExportException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionStoreException;
import org.setupx.repository.web.forms.PromtCreateException;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.SampleScheduledException;

public class ReportEngine {

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
       return super.toString(); 
    }
    
    public static Export determineMetaDataBySampleMax(String id) throws ExportException {
        return XMLExport.export(id);
    }

    public static Export determineMetaData(String id) throws ExportException {
        return XMLExport.export(id);
    }
    
    public static void testingExport(long promtID){
        Promt promt;
        try {
            promt = Promt.load(promtID,true);

            FileExport fileExport;

            fileExport = new XLSFileExport(FileExport.createFileName(promtID, "xls"));
            fileExport.export(promt);
            fileExport.createExportFile();
        } catch (PersistenceActionFindException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (PersistenceActionStoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (PromtCreateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SampleScheduledException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileExportException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
