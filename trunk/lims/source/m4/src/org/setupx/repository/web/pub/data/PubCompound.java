package org.setupx.repository.web.pub.data;


public class PubCompound extends PublicationCoreObject {

    public static final String CAS = "cas";
    public static final String KEGG = "kegg";
    public static final String NAME = "name";
    public static final String BEILSTEIN = "beilstein";
    public static final String INCHI = "inchi";
    public static final String SMILES = "smiles";
    public static final String MF = "mf";
    public static final String MW = "mw";
    // synonyms    
    
    
}
