/**
 * ============================================================================ File:    FormObjectComparator.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import java.util.Comparator;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class FormObjectComparator implements Comparator {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private int type = 0;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final int ID = 1;
  private static final int INTERNAL_POSITION = 2;
  private static final int NUMER_OF_FIELDS = 3;
  private static final int PARENT_ID = 4;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new FormObjectComparator object.
   *
   * @param type 
   */
  public FormObjectComparator(int type) {
    this.type = type;
  }

  private FormObjectComparator() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public static Comparator byID() {
    return new FormObjectComparator(ID);
  }

  /* (non-Javadoc)
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   *
   * a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
   */
  public int compare(Object o1, Object o2) {
    FormObject formObject1 = (FormObject) o1;
    FormObject formObject2 = (FormObject) o2;

    switch (type) {
    case ID:
      return (int) (formObject1.getUOID() - formObject2.getUOID());

    case INTERNAL_POSITION:
      return (int) (formObject1.getInternalPosition() - formObject2.getInternalPosition());

    case NUMER_OF_FIELDS:
      return (int) (formObject1.getFields().length - formObject2.getFields().length);

    case PARENT_ID:
      return (int) (formObject1.getParent().getUOID() - formObject2.getParent().getUOID());

    default:
      return (int) (formObject1.getUOID() - formObject2.getUOID());
    }
  }
}
