package org.setupx.repository.server.workflow.elements;

import org.setupx.repository.server.workflow.elements.Configuration;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class StandardConfiguration implements Configuration {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private int id;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new StandardConfiguration object.
   *
   * @param i 
   */
  public StandardConfiguration(int i) {
    this.setId(i);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.elements.Configuration#setDescription(java.lang.String)
   */
  public void setDescription(String description) {
    // TODO Auto-generated method stub
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.elements.Configuration#getDescription()
   */
  public String getDescription() {
    return "Default Configuration for Standard-Settings.";
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.elements.Configuration#setId(int)
   */
  public void setId(int id) {
    this.id = id;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.elements.Configuration#getId()
   */
  public int getId() {
    return this.id;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.elements.Configuration#setLabel(java.lang.String)
   */
  public void setLabel(String label) {
    throw new UnsupportedOperationException("method not implemented in " + this.getClass().getName());
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.elements.Configuration#getLabel()
   */
  public String getLabel() {
    return "Instance with Standard-Configuration";
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public final String toString() {
    return this.getLabel().concat(" [id:").concat(this.getId() + "").concat("] ").concat(this.getDescription());
  }
}
