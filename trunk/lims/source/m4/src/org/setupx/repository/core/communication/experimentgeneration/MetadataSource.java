/**
 * ============================================================================ File:    MetadataSource.java Package: org.setupx.repository.core.communication.experimentgeneration cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.experimentgeneration;

import org.setupx.repository.core.communication.experimentgeneration.template.ClazzTemplate;
import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.core.util.hotfix.oldsamples.mapping.MappingException;
import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.PromtCreateException;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.UnsupportedClazzMappingException;


/**
 * DataSource containing metadata, that will be converted and or imported, ...
 * 
 * <ul>
 * <li>
 * The metadatasources are created by MetadataSourceFinder
 * </li>
 * <li>
 * each of the sources is taken and used as a template for a whole new promt
 * </li>
 * <li>
 * the mapping is done inside the creating process of the the new promt
 * </li>
 * </ul>
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @see org.setupx.repository.web.forms.inputfield.multi.Promt#assignClazzTemplates(MetadataSource)
 * @see org.setupx.repository.web.forms.PromtCreator#createInternal(MetadataSource)
 */
public interface MetadataSource {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  //public PromtTemplate getPromtTemplates();
  public ClazzTemplate[] getClazzTemplates();

  /**
   * defines if all samples in this source have been finished already.
   * 
   * <p>
   * used for mapping - if true scanned pairs will be created
   * </p>
   *
   * @return true if finished
   */
  public boolean isFinishedRun();

  /**
   * create a promt out of the this metadatasource
   *
   * @return the new promt
   *
   * @throws PromtCreateException unable to create a promt
   * @throws MappingException
   * @throws MappingError
   */
  public Promt createPromt() throws PromtCreateException, MappingError;

  /**
   * TODO: 
   */
  public void createScannedPairs();

  /**
   * maps a value (or more) found at a specific path onto formobjects value.
   * 
   * <p>
   * use this if it is a regular field - if the field is a MultiField4Clazz use the other one.
   * </p>
   *
   * @throws MappingException unable to map existing informaton onto this field - can be that the formobject can not be expanded to the number of information in the path.
   *
   * @see #map(MultiField4Clazz)
   * @see InputField#setValue(Object)
   */
  public void map(InputField formObject, String path) throws MappingError;

  /**
   * maps the information from this source onto the multifield4clazz
   *
   * @param multiField4Clazz
   *
   * @throws CloneException
   * @throws UnsupportedClazzMappingException
   * @throws MappingError
   */
  public void map(MultiField4Clazz multiField4Clazz) throws MappingError;
}
