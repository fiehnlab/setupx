package org.setupx.repository.core.util.hotfix.oldsamples.mapping;


/**
 * @deprecated
 * @author scholz
 *
 */
public class MappingException extends Exception {

    public MappingException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public MappingException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public MappingException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public MappingException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
