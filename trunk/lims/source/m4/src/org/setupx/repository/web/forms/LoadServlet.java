/**
 * ============================================================================ File:    LoadServlet.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.setupx.repository.Config;
import org.setupx.repository.Settings;
import org.setupx.repository.core.communication.CommunicationException;
import org.setupx.repository.core.communication.binbase.BBConnector;
import org.setupx.repository.core.communication.export.reporting.Report;
import org.setupx.repository.core.communication.export.reporting.ReportInitException;
import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.core.communication.leco.MissingMachineInformationException;
import org.setupx.repository.core.user.UserConnector;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.UserFactoryException;
import org.setupx.repository.core.user.UserNotFoundException;
import org.setupx.repository.core.user.WrongPasswordException;
import org.setupx.repository.core.user.access.PromtUserAccessRight;
import org.setupx.repository.core.util.hotfix.oldsamples.mapping.MappingException;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.core.util.web.UiHttpServlet;
import org.setupx.repository.core.util.web.WebForwardException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionStoreException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.web.DownloadServlet;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.SampleScheduledException;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class LoadServlet extends UiHttpServlet {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final Object thiZ = new LoadServlet();

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.core.util.web.UiHttpServlet#process(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  public void process(HttpServletRequest request, HttpServletResponse response)
    throws Throwable {
    /* the id of the experiment that is going to be loaded. */
    long promtID = Long.parseLong(getRequestParameter(request, "id"));

    /* the action id - determinating what action will be done */
    int action = PromtUserAccessRight.READ;

    /* find the user in the session */
    UserDO userDO = determineUser(request);

    if (userDO == null) {
      throw new UserNotFoundException("User is not know - or not logged in.");
    }

    try {
      action = getRequestParameterInt(request, WebConstants.PARAM_LOAD_ACTION);

      // in case it is ileagel, reduce the action to read status
      if (action > 100) {
        throw new InsufficientUserRightException("illegal user acces rights requested.");
      }
    } catch (Exception e) {
      action = PromtUserAccessRight.READ;
    }

    /* SPECIAL CASE - in case someone requests a blank form - load(0) - set actionid to CREATE */
    if (promtID == 0) {
      action = PromtUserAccessRight.CREATE;
    }

    Logger.debug(this, "loading promt with id: " + promtID);
    Logger.debug(this, "user id : " + userDO.getUOID());
    Logger.debug(this, "action id : " + action);

    // check weather the user is allowed to edit this experiment    
    PromtUserAccessRight.checkAccess(userDO.getUOID(), promtID, action);

    // no exception - so keep going and have fun.
    switch (action) {
    case PromtUserAccessRight.WRITE:

      int accessright = new SXQuery().findPromtUserAccessRightForUserID(userDO.getUOID(), promtID).getAccessCode();
      loadForEdit(request, response, promtID, accessright);

      break;

    case PromtUserAccessRight.READ:
      loadForReport(request, response, promtID);

      break;

    case PromtUserAccessRight.CLONE:
      createClone(request, response, promtID);

      break;

    case PromtUserAccessRight.CREATE:
      createBlank(request, response);

      break;

    case PromtUserAccessRight.EXPORT:
      export(promtID, request, response);

      break;

    case PromtUserAccessRight.DOWNLOAD_RESULT:

      OutputStream out = response.getOutputStream();
      response.setContentType(DownloadServlet.CONTENT_TYPE_ZIP);

      if (!BBConnector.determineBBResultsArrived(promtID)) {
        throw new ContentNotAvailableException("The requested content is not available.");
      }

      File file = BBConnector.getBBResultFile(promtID);
      
      /*
      if (userDO.isPublic()){
          File file = BBConnector.getBBResultFile(promtID);
      }
      */
      DownloadServlet.returnFile(file, out);

      break;

    default:
      break;
    }
    
    
  }

  /**
   * TODO: 
   *
   * @param request TODO
   * @param response TODO
   *
   * @throws PersistenceActionFindException TODO
   * @throws PersistenceActionStoreException TODO
   * @throws PromtCreateException TODO
   * @throws SampleScheduledException TODO
   * @throws WebForwardException TODO
   * @throws MappingException
   * @throws MappingError
   */
  private void createBlank(HttpServletRequest request, HttpServletResponse response)
    throws PersistenceActionFindException, PersistenceActionStoreException, PromtCreateException, SampleScheduledException, WebForwardException, MappingError {
    Promt promt = null;

    promt = PromtCreator.create(PromtCreator.DEFAULTVERSION);    
    Logger.debug(this, "created new Promt: uoid: " + promt.getUOID());
    long tmp_ID = promt.getUOID();
    
    
    try {
        if (promt.getUOID() == 0 ){
            promt.update(true);
            
            Logger.debug(this, "promt stored: uoid: " + promt.getUOID());
            if (tmp_ID != 0 && tmp_ID != promt.getUOID()) throw new PersistenceActionUpdateException("two ids assigned to one object.");
            request.getSession().setAttribute("TIME", "" + new Date().getTime());
        }
    }catch (org.setupx.repository.server.persistence.PersistenceActionUpdateException e) {
        e.printStackTrace();
    }

    debug("adding promt " + promt.getUOID() + " to session");
    request.getSession().putValue(WebConstants.SESS_FORM_PROMT, promt);
    request.getSession().putValue(WebConstants.SESS_FORM_POS, new Integer(0));

    forward("/newPromt.jsp", request, response);
  }

  /**
   * TODO: 
   *
   * @param request TODO
   * @param response TODO
   * @param promtID TODO
   *
   * @throws WebForwardException TODO
   */
  private void createClone(HttpServletRequest request, HttpServletResponse response, long promtID)
    throws WebForwardException {
    Promt promt = Promt.createClone(promtID);
    request.getSession().putValue(WebConstants.SESS_FORM_PROMT, promt);
    request.getSession().putValue(WebConstants.SESS_FORM_POS, new Integer(1));

    forward("/form.jsp", request, response);
  }

  /**
   * find the user in the session
   *
   * @param request the request where the session is taken from in which the user has to be.
   *
   * @return the user if the user is found the object of the user - otherwise null
   */
  private static final UserDO determineUser(HttpServletRequest request) {
    Logger.debug(thiZ, "determineUser()");

    Object object = request.getSession().getAttribute(WebConstants.SESS_USER);

    if (object == null) {
      return null;
    }

    if (!(object instanceof UserDO)) {
      Logger.err(thiZ, "the object found in the session was not a userDO");

      return null;
    }

    return (UserDO) object;
  }

  /**
   * TODO: 
   *
   * @param promtID TODO
   * @param request TODO
   * @param response TODO
   *
   * @throws CommunicationException TODO
   * @throws PersistenceActionFindException TODO
   * @throws PersistenceActionStoreException TODO
   * @throws PromtCreateException TODO
   * @throws SampleScheduledException TODO
   * @throws WebForwardException TODO
   * @throws MissingMachineInformationException
   */
  private void export(long promtID, HttpServletRequest request, HttpServletResponse response)
    throws CommunicationException, PersistenceActionFindException, PersistenceActionStoreException, PromtCreateException, SampleScheduledException, WebForwardException, MissingMachineInformationException {
    forward("export_selection.jsp?id=" + promtID, request, response);
  }

  /**
   * load a promt into the session - and then forwards to the sxfrontend
   *
   * @param accessright
   */
  private void loadForEdit(HttpServletRequest request, HttpServletResponse response, long id, int accessright)
    throws PersistenceActionFindException, PersistenceActionStoreException, PromtCreateException, SampleScheduledException, WebForwardException {
    Promt promt = null;

    // old experiments can NOT be edited anymore
    if (id < Config.PROMT_OUTDATED_ID) throw new PromtCreateException("This experiment was created with an older version of SetupX and is declared as non modifiable.", new Exception("deprecated:  experiment " + id + " is older than 318159"));
    

    // ignore write protection
    // when: accessright > 90
    promt = Promt.load(id, (accessright > 90));

    request.getSession().putValue(WebConstants.SESS_FORM_PROMT, promt);
    request.getSession().putValue(WebConstants.SESS_FORM_POS, new Integer(0));

    // last time it was saved
    request.getSession().setAttribute("TIME", "" + new Date().getTime());


    forward("/form.jsp", request, response);
  }

  /**
   * TODO: 
   *
   * @param request TODO
   * @param response TODO
   * @param promtID TODO
   *
   * @throws WebForwardException TODO
   * @throws ReportInitException TODO
   */
  private void loadForReport(HttpServletRequest request, HttpServletResponse response, long promtID)
    throws WebForwardException, ReportInitException {
    Report report = Promt.loadReport(promtID);

    request.getSession().putValue(WebConstants.SESS_REPORT, report);

    /*
       if (new SXQuery().findSampleIDsByPromtID(promtID).size() > 40) {
         forward("/report_small.jsp", request, response);
       } else {
     */
    forward("/report.jsp", request, response);
    //}
  }

  /**
   * @deprecated Persistence is not based on Files anymore
   */
  private final void loadFromFile(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ClassNotFoundException, WebForwardException, UserNotFoundException, WrongPasswordException, UserFactoryException {
    String filename = Config.DIRECTORY_PERSISTENCE_SX + getRequestParameter(request, "id");
    Logger.debug(this, "loading promt: " + filename);

    File file = new File(filename);
    Promt promt = Persistence.loadPromt(file);

    request.getSession().putValue(WebConstants.SESS_FORM_PROMT, promt);
    request.getSession().putValue(WebConstants.SESS_USER, UserConnector.findUser("local", "local"));
    request.getSession().putValue(WebConstants.SESS_FORM_POS, new Integer(1));

    forward("/form.jsp", request, response);
  }
}
