/**
 * ============================================================================ File:    MailContent.java Package: org.setupx.repository.core.communication.mail cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.mail;

import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.UserLocal;


/**
 * Part of a mail. The Mailcontentobjects are collected in the Mailcontroller. A single Mail can contain several MailContent-Objects
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.14 $
 *
 * @see org.setupx.repository.core.communication.mail.MailController#sendNonsendMails()
 */
class MailContent {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected String content;
  protected UserDO user;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param address
   * @param subject
   * @param msg
   */
  protected MailContent(EMailAddress address, String subject, String msg) {
    this.content = subject + "\n " + msg;
    this.user = new UserLocal(address.getEmailAddress(), "", address);
  }

  protected MailContent(UserDO userDO, String content) {
    this.user = userDO;
    this.content = content;
  }

  private MailContent() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


}
