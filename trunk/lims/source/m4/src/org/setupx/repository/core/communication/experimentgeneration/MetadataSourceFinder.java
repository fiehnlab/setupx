/**
 * ============================================================================ File:    MetadataSourceFinder.java Package: org.setupx.repository.core.communication.experimentgeneration cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.experimentgeneration;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;

import org.setupx.repository.core.util.hotfix.oldsamples.PromtConverter;
import org.setupx.repository.core.util.hotfix.oldsamples.PromtConverterException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;


/**
 * creator for different metadatasources.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public final class MetadataSourceFinder {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private MetadataSourceFinder() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * creates metadatasources based on the type of directory.
   *
   * @param directory TODO
   *
   * @return TODO
   *
   * @throws FileNotFoundException TODO
   * @throws PersistenceActionFindException TODO
   * @throws PromtConverterException TODO
   */
  public static MetadataSource[] newInstances(File directory)
    throws FileNotFoundException, PersistenceActionFindException, PromtConverterException {
    if (!directory.isDirectory() && !directory.exists()) {
      throw new FileNotFoundException("The directory " + directory + " does not exist or is not a directory.");
    }

    // internal filter
    File[] subDirectories = directory.listFiles(new FileFilter() {
          public boolean accept(File pathname) {
            return pathname.isDirectory();
          }
        });

    // each directory represents a source - a promt
    MetadataSource[] metadataSources = new MetadataSource[subDirectories.length];

    for (int i = 0; i < subDirectories.length; i++) {
      // open each sub directory 
      metadataSources[i] = new PromtConverter(subDirectories[i]);
    }

    return metadataSources;
  }
}
