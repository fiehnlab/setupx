/**
 * ============================================================================ File: ServiceMediator.java Package:
 * org.setupx.repository.core cvs: $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz Copyright (C) 2005 Martin
 * Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/
 * ============================================================================
 */
package org.setupx.repository.core;

import org.setupx.repository.core.communication.mail.EMailAddress;
import org.setupx.repository.core.communication.mail.MailController;
import org.setupx.repository.core.communication.vocabulary.VocabularyChecker;
import org.setupx.repository.core.user.UserConnector;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.UserFactoryException;
import org.setupx.repository.core.user.UserNotFoundException;
import org.setupx.repository.core.user.WrongPasswordException;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.web.forms.inputfield.multi.Promt;

/**
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 * @deprecated
 */
public class ServiceMediator extends CoreObject {
    // ~ Static fields/initializers
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    static Connectable connBinbase = null;
    private static final ServiceMediator thiZ = new ServiceMediator();
    public static String version = "$Revision: 1.12 $";

    static {
        log(thiZ, "init  Mediator Version: " + version);

        log(thiZ, "init  start ---------------------------------------------------------");

        try {
            throw new Exception("TAMINO NOT CONNECTED");
            // sourceExperiments = DataSourceFactory.createDS(Config.TAMINO_DEFAULT_DATABASENAME_M2_EXP.toString(),
            // Config.TAMINO_EXPERIMENT_COLLECTION, XMLabstractDocument.EXPERIMENT);
            // sourceSamples = DataSourceFactory.createDS(Config.TAMINO_DEFAULT_DATABASENAME_M2_SAMPLES.toString(),
            // Config.TAMINO_SAMPLES_COLLECTION, XMLabstractDocument.SAMPLE);
            // sourceSchemas = DataSourceFactory.createDS(Config.TAMINO_DEFAULT_DATABASENAME_M2_SCHEMA.toString(),
            // Config.TAMINO_SCHEMA_COLLECTION, XMLabstractDocument.SCHEMA);
            // connBinbase = new BBConnector();
        } catch (Exception ex) {
            // throw new RuntimeException("problems initalizing the databases and services !!! ", ex);
            warning(thiZ, "unable to init the databases");
            // err(thiZ, ex);
        }

        log(thiZ, "init end ---------------------------------------------------------");
        Logger.logAnatomize(thiZ);
    }

    public static final int ACTION_FIND = 4;
    public static final int ACTION_REMOVE = 1;
    public static final int ACTION_STORE = 2;
    public static final int ACTION_UPDATE = 3;
    public static final boolean DEBUGGING_MAIL_ACTIVATED = true;

    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new M1ServiceMediator object.
     */
    private ServiceMediator() {
        Logger.debug(this, "new instance");
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @param entry
     * @return
     */
    public static String[] checkVocabulary(String entry) {
        return VocabularyChecker.check4suggestion(entry);
    }

    /**
     * @param id
     *                the ID of a User in the System - if the user is an user from the local LDAP-SYstem the ID must be
     *                the id from the LDAP system
     * @param password
     *                the users password - if the user is an user from the local LDAP-SYstem the password must be the
     *                password from the LDAP system
     * @return a dataobject containing information about the user
     * @throws UserNotFoundException
     * @throws WrongPasswordException
     * @throws UserFactoryException
     */
    public static UserDO findUser(String id, String password) throws UserNotFoundException, WrongPasswordException,
            UserFactoryException {
        return UserConnector.findUser(id, password);
    }

    /**
     * checks if a user can be logged in
     * 
     * @param userID
     *                the user id
     * @param password
     *                password
     * @return true if the user was found - false if it was not possible to find the user -
     */
    public static boolean login(String userID, String password) {
        try {
            UserConnector.findUser(userID, password);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * TODO:
     * 
     * @param email
     *                TODO
     * @param subject
     *                TODO
     * @param content
     *                TODO
     */
    public static void notifyUser(EMailAddress email, String subject, String content) {
        MailController.inform(email, subject, content);
    }

    /**
     * TODO:
     * 
     * @param promt
     *                TODO
     * @param notify_ID
     *                TODO
     * @deprecated use NotificationCentral
     */
    public static void notifyUser(Promt promt, int notify_ID) {
        warning(thiZ, "add seperation of different mail types.");

        // inform the admin
        // find all admins needed for this notify status
        UserDO[] user = UserConnector.getUser(notify_ID);

        for (int i = 0; i < user.length; i++) {
            UserDO userDO = user[i];
            MailController.inform(userDO, promt, notify_ID);
        }

        // send an email to the owner
        String[] owners = promt.getOwners();

        for (int i = 0; i < owners.length; i++) {
            MailController.inform(owners[i], promt, notify_ID);
        }
    }

    /**
     * @param sample
     */
    public static void notifyUser(Object object) {
        warning(thiZ, "MISSING: notifyUser(Sample sample)");
    }
}
