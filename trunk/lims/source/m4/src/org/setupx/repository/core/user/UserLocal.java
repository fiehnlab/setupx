/**
 * ============================================================================ File:    UserLocal.java Package: org.setupx.repository.core.user cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.user;

import org.setupx.repository.core.communication.mail.EMailAddress;


/**
 * Localuser
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 *
 * @hibernate.subclass
 */
public class UserLocal extends UserDO {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new UserLocal object.
   *
   * @param username2 
   * @param password2 
   * @param email 
   */
  public UserLocal(String username2, String password2, EMailAddress email) {
    super(username2, password2, email);
  }

  /**
   * Creates a new UserLocal object.
   */
  public UserLocal() {
    super();
  }
}
