/**
 * ============================================================================ File:    MultiFieldExpandingSimple.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.Config;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.Removable;


/**
 * sampe as MultiFieldExpanding - just a simpler createHTML
 *
 * @hibernate.subclass
 */
public class MultiFieldExpandingSimple extends MultiFieldExpanding {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   *
   */
  public MultiFieldExpandingSimple() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @param question
   * @param description
   * @param longDescription
   * @param firstInputfield
   */
  public MultiFieldExpandingSimple(String question, String description, String longDescription, Removable firstInputfield) {
    super(question, description, longDescription, firstInputfield);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTML()
   */
  public String createHTML(int col) {
    StringBuffer response = new StringBuffer();
    response.append(createAnchor());

    // create a table contaitaining 
    response.append("<!-- " + this.getName() + " (" + this.getDescription() + ") -->");

    response.append("<th colspan=\"" + col + "\">" + this.getQuestion() + "\n" + "<br><font class=\"small\">" + this.getDescription() + "</font>" + "<br><font class=\"small\">" + this.getLongDescription() + "</font></td></th>");

    response.append("<tr><td colspan=\"" + col + "\"><span class=\"small\"> Currently there are/is " + this.size() + " " + ((InputField) this.getField(0)).getQuestion() + "-Fields. If you need more inputfields, just type in the number of needed fields.</span>" + this.createExpandButton() +
      this.createAutofillButton() + "</td>  </tr>");

    response.append("<tr><td colspan=\"" + col + "\" align=\"center\">");
    response.append("\n<table " + WebConstants.BORDER + " width=\"90%\">\n");

    // setting my own number of cols
    col = 4;

    // put every inputfield in there
    response.append(this.createHTMLChilds(col));

    // add an expand button
    // - moved up - 
    // response.append(this.createExpandButton());
    // add a help button
    response.append(this.createHelpButton());

    // close the table
    response.append("</table></td></tr>");

    return response.toString();
  }

  /**
   * TODO: 
   *
   * @param numberOfColums TODO
   *
   * @return TODO
   */
  public String createHTMLChilds(int numberOfColums) {
    if (Config.NEW_DESIGN) {
      return super.createHTMLChilds(numberOfColums);
    }

    StringBuffer buffer = new StringBuffer();

    for (int i = 0; i < this.getFields().length; i++) {
      FormObject object = this.getFields()[i];

      // only active objects will be shown
      if (object.isActive()) {
        buffer.append("\n<!-- START -- " + object.toString() + " -->\n");

        if (this.dividerHR) {
          buffer.append("<tr><td colspan=\"" + numberOfColums + "\"><hr></td></tr>");
        }

        buffer.append("<tr><td valign=\"top\" align=\"right\">" + (i + 1) + "/" + this.getFields().length + "  " + ((Removable) object).createRemoveButton() + "</td>");
        buffer.append("<td><table>");
        buffer.append(object.createHTML(numberOfColums - 1));
        buffer.append("</table></td>");

        buffer.append("\n<!-- END -- " + object.toString() + " -->\n");
      } else {
      }
    }

    return buffer.toString();
  }
  
  
  
  
  
  /*
   * (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#setFields(org.setupx.repository.web.forms.FormObject[])
   *//*
  public void setFields(FormObject[] formObjects){
      debug("DEBUGGING: ");
      debug("DEBUGGING: ----------------- START -----------------");
      debug("DEBUGGING:  trying to set " + formObjects.length + " formObjects. " );
      debug("DEBUGGING:  current status of this " + Util.getClassName(this) + " is " + this.isActive() + ". ");
      debug("DEBUGGING:  now checking if some of them are active. ");

      for (int i = 0; i < formObjects.length; i++) {
            FormObject object = formObjects[i];
            debug("DEBUGGING:       formobject to be set: " + object.getUOID() + "  active:" + object.isActive() + " " + object.getName());
      }
      
      debug("DEBUGGING:  now setting the fields in super.setFields(...) . ");
      super.setFields(formObjects);
      debug("DEBUGGING:  status after setting the forms of this " + Util.getClassName(this) + " is " + this.isActive() + ". ");
      debug("DEBUGGING: ----------------- END -----------------");
      debug("DEBUGGING: ");
  }
  
  /*
   * (non-Javadoc)
   * @see org.setupx.repository.core.CoreObject#update(org.hibernate.Session, boolean)
   *//*
  public final void update(Session session, boolean createIt) throws PersistenceActionUpdateException{
      boolean _1 = this.isActive();
      boolean _2 = this.getRemovable();
      boolean _3 = this.isValid();
      debug("UPDATE(...) : ----------------- START -----------------");
      debug("UPDATE(...) : " + _1 + " " + _2 + " " + _3);
      debug("UPDATE(...) : ----------------- START -----------------");
      super.update(session, createIt);
      debug("UPDATE(...) : ----------------- END -----------------");
      debug("UPDATE(...) : " + _1 + " " + _2 + " " + _3);
      debug("UPDATE(...) : ----------------- END -----------------");
      if (_1 != this.isActive()) throw new RuntimeException("_1");
      if (_2 != this.getRemovable()) throw new RuntimeException("_1");
      if (_3 != this.isValid()) throw new RuntimeException("_1");
      
  }*/
}
