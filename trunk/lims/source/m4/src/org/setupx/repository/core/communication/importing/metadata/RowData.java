/**
 * ============================================================================ File:    RowData.java Package: org.setupx.repository.core.communication.importing.metadata cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.importing.metadata;

import org.setupx.repository.core.util.Util;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class RowData extends MDCore {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private HSSFRow row;
  private HSSFSheet sheet;
  private Hashtable values = new Hashtable();
  private String sheetname = "";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * create an RowData object by taking the whole line out of the sheet. A label is beeing found by determineHeader()
   *
   * @see RowData#determineHeader(short)
   */
  RowData(HSSFSheet sheet, int validRow) {
    this.sheet = sheet;

    this.row = this.sheet.getRow(validRow);

    short lastCell = this.row.getLastCellNum();

    for (short i = 0; i < lastCell; i++) {
      HSSFCell cell = this.row.getCell(i);

      // find all value
      String value = "";
      value = Util.getCellValue(cell);

      if (value.length() > 0) {
        // find the labels for each value
        String label = determineHeader(i);

        // add each pair   
        this.values.put(label, value);
      }
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @return the sheetname where the values are from
   */
  public String getSheetName() {
    return this.sheetname;
  }

  /**
   * Hashtable: <br> key: label <br> value: the entry
   *
   * @return a hashtable
   */
  public Hashtable getValues() {
    return this.values;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String toString() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("sheet: " + this.sheetname);
    buffer.append("\n");

    Enumeration en = this.values.keys();

    while (en.hasMoreElements()) {
      String key = (String) en.nextElement();
      String rowData = (String) this.values.get(key);
      buffer.append(key + ":\t" + rowData + "\n");
    }

    return buffer.toString();
  }

  /**
   * TODO: 
   *
   * @param sheetlabel TODO
   */
  void setSheetName(String sheetlabel) {
    this.sheetname = sheetlabel;
  }

  /**
   * find the header of a column. Take the first cell in this column and navigate down until there is a value found.
   *
   * @return cell value or -<b><code>i</code></b>- if nothing found.
   *
   * @see Util#getCellValue(HSSFCell)
   */
  private String determineHeader(final short i) {
    Iterator iterator = this.sheet.rowIterator();

    while (iterator.hasNext()) {
      HSSFCell cell = ((HSSFRow) iterator.next()).getCell(i);

      String value = Util.getCellValue(cell);

      if (value.length() > 0) {
        return value;
      }
    }

    return "-" + i + "-";
  }
}
