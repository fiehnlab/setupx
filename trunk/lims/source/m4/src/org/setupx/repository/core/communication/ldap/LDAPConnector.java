/**
 * ============================================================================ File:    LDAPConnector.java Package: org.setupx.repository.core.communication.ldap cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ldap;

import org.setupx.repository.Settings;
import org.setupx.repository.core.Connectable;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.ServiceNotAvailableException;
import org.setupx.repository.core.communication.mail.EMailAddress;
import org.setupx.repository.core.user.UserNotFoundException;
import org.setupx.repository.core.user.UserRemote;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchResult;


/**
 * connection to the remote LDAP server   TODO - remove contants (servername, ...)
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class LDAPConnector extends CoreObject implements Connectable {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String MAIL = "mail";
  public static final String USERNAME = "username";
  private static Object thiz = new LDAPConnector();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Connector object.
   */
  public LDAPConnector() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * debugging
   */
  public static void main(String[] args) throws UserNotFoundException, NamingException {
    LDAPConnector connector = new LDAPConnector();
    log(connector, "" + LDAPConnector.receive("mscholz"));
  }

  /**
   * receives information for a specific user.  <code> ldap server="ldap.ucdavis.edu" action="query" name="directoryLookup" start="ou=People,dc=ucdavis,dc=edu" attributes="eduPersonAffiliation" filter="uid=#Remote_User#" </code>   using <code>ldap://ldap.ucdavis.edu:389</code> as server
   *
   * @param username2 username - which is the login for at ucdavis
   *
   * @return a userremote object containing all information
   *
   * @throws UserNotFoundException unable to find user in the system - TODO check mapping - seems to throw usernotfound too - just becuase of mapping probs
   * @throws NamingException problems accessing a few attributes
   */
  public static UserRemote receive(String username2) throws UserNotFoundException, NamingException {
    debug(thiz, "looking for " + username2);

    Hashtable userInformation = new Hashtable();

    Hashtable env = new Hashtable();
    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
    env.put(Context.PROVIDER_URL, Settings.getValue("system.ldpa.server", "ldap://ldap.ucdavis.edu:389"));

    // Create the initial context
    DirContext ctx;

    try {
      ctx = new InitialDirContext(env);
    } catch (NamingException e) {
      throw new UserNotFoundException("unable to find the user - cause there is no connection to the LDAP-Server");
    }

    Attributes matchAttrs = new BasicAttributes(true); // ignore case
    matchAttrs.put(new BasicAttribute("uid", username2));

    //matchAttrs.put(new BasicAttribute("mail"));
    // Search for objects that have those matching attributes
    NamingEnumeration answer;

    try {
      answer = ctx.search(Settings.getValue("system.ldpa.query", "ou=People, dc=ucdavis,dc=edu"), matchAttrs);

      if (answer == null) {
        throw new UserNotFoundException("LADP answer is null");
      }
    } catch (NamingException e1) {
      throw new UserNotFoundException("unable to find the user - cause there problems with the LDAP-communication");
    }

    if (answer == null) {
      throw new UserNotFoundException("");
    }

    // create a hashtable with all the attributes
    SearchResult answerElement = (SearchResult) answer.nextElement();
    Attributes attributes = answerElement.getAttributes();

    NamingEnumeration enumeration = attributes.getAll();

    while (enumeration.hasMoreElements()) {
      Attribute o = (Attribute) enumeration.nextElement();

      //debug(thiz, "found attribute: " + o.getID() + " " + o.get());
      //debug(thiz, "" + o.getID() + " " + o.get(0));
      userInformation.put(o.getID(), o.get(0));
    }

    try {
      userInformation.put(USERNAME, attributes.get("uid").get(0).toString());
      userInformation.put(MAIL, attributes.get("mail").get(0).toString());
    } catch (NullPointerException e) {
      warning(thiz, "unable to receive all requiered attributes.");
      userInformation.put(USERNAME, username2);
    }

    // mapping user
    return mapping(userInformation);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.Connectable#isAvailable()
   */
  public void checkAvailability() throws ServiceNotAvailableException {
    try {
      main(null);
    } catch (UserNotFoundException e) {
      warning(e);
      throw new ServiceNotAvailableException(e);
    } catch (NamingException e) {
      warning(e);
      throw new ServiceNotAvailableException(e);
    }
  }

  /**
   * @param attrs
   *
   * @deprecated not used 14apr2005
   */
  static void printAttrs(Attributes attrs) {
    //debug(thiz, "" + attrs.size());
    if (attrs == null) {
      System.out.println("No attributes");
    } else {
      /* Print each attribute */
      try {
        for (NamingEnumeration ae = attrs.getAll(); ae.hasMore();) {
          Attribute attr = (Attribute) ae.next();

          //System.out.print("attribute: " + attr.getID());
          String element = "";

          /* print each value */
          for (NamingEnumeration e = attr.getAll(); e.hasMore(); element = "" + e.next()) {
          }

          if (element.equals("Martin Scholz")) {
            System.out.println("");
            System.out.print(" " + element + ", ");
            System.exit(0);
          }
        }
      } catch (NamingException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * mapps ladp information to a real user object
   *
   * @param userInformation
   *
   * @return user remote object
   *
   * @throws UserNotFoundException unable to map - cause of nullpointer while mapping
   */
  private static UserRemote mapping(Hashtable userInformation)
    throws UserNotFoundException {
    UserRemote userRemote = new UserRemote();
    debug(thiz, "mapping the userinformation onto an user");

    try {
      userRemote.phone = (String) userInformation.get("telephoneNumber");
      userRemote.displayName = (String) userInformation.get("displayName");
      userRemote.organisation = (String) userInformation.get("ou");
      userRemote.setMailAddress(new EMailAddress((String) userInformation.get(LDAPConnector.MAIL)));
      userRemote.username = (String) userInformation.get(LDAPConnector.USERNAME);
      userRemote.userinformation = userInformation;
    } catch (NullPointerException e) {
      throw new UserNotFoundException("unable to find userattributes");
    }

    return userRemote;
  }
}
