/**
 * ============================================================================ File:    BBConnector.java Package: org.setupx.repository.core.communication.binbase cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.binbase;

import java.io.File;
import java.rmi.RemoteException;
import java.util.Hashtable;

import javax.naming.NamingException;

import org.setupx.repository.Config;
import org.setupx.repository.core.Connectable;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.InitException;
import org.setupx.repository.core.communication.CommunicationException;
import org.setupx.repository.core.communication.ServiceNotAvailableException;
import org.setupx.repository.core.util.Mail;



/**
 * connector to the binbaseSystem. Connector itself is deprecated but still used for file references.
 * 
 * <p>
 * Redirects the Methodcalls to the BBsystem and fetches the results from the bb
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 * @deprecated was replaced by a soap connection
 * 
 */
public class BBConnector extends CoreObject implements Connectable {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//  private BBJMSReceiver receiver = null;
//  private QuantificationService quantificationService;
//  private ResultService resultService;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static boolean error = false;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new BBConnector object.
   *
   * @throws InitException 
   */
  public BBConnector() throws ServiceNotAvailableException {
    if (error == true) {
      throw new ServiceNotAvailableException(this);
    }

    log("new instance");


    log("init finished");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @return
   */
  public static Connectable getInstace() {
    try {
      return new BBConnector();
    } catch (ServiceNotAvailableException e) {
      return null;
    }
  }

  /**
   * starts the Quantification of a set of samples
   *
   * @param ids an array of ids that will be processed on bbase
   *
   * @return position of each Sample in the processing queue
   *
   * @throws CommunicationException - problems on the remote side
  public int[] calculate(SampleID[] ids) throws CommunicationException {
    int[] i;

    try {
      return quantificationService.quantify(SampleID.asStringArray(ids));
    } catch (RemoteException e) {
      Mail.postError(e);
      throw new CommunicationException(e);
    }
  }
  */
 
  /* (non-Javadoc)
   * @see org.setupx.repository.core.Connectable#isAvailable()
   */
  public void checkAvailability() throws ServiceNotAvailableException {
    // just in case the service has been tested already
    if (this.error) {
      throw new ServiceNotAvailableException(this);
    }
  }

  /**
   * checks if results from binbase have arrived yet
   *
   * @return true if data set exists
   */
  public static final boolean determineBBResultsArrived(long resultSetID) {
    // check directory for file containing data with that specific id
    java.io.File target = getBBResultFile(resultSetID);
    return target.exists();
  }

  /**
   * TODO: 
   *
   * @param resultSetID TODO
   *
   * @return TODO
   */
  public static File getBBResultFile(long resultSetID) {
    return new java.io.File(Config.DIRECTORY_BB_RESULTS + resultSetID + ".zip");
  }

  /**
   * TODO: 
   *
   * @param resultSetID TODO
   *
   * @return TODO
   */
  public static File getBBResultFile(String resultSetID) {
    return new java.io.File(Config.DIRECTORY_BB_RESULTS + resultSetID + ".zip");
  }





}
