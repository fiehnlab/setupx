package org.setupx.repository.server;

import org.setupx.repository.Config;

import org.apache.axis.transport.http.AxisServlet;


/**
 * Servlet doing the registration of the whole LIMS.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class RegistrationServlet extends AxisServlet {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new RegistrationServlet object.
   */
  public RegistrationServlet() {
    super();

    // init the LIMs-System
    Config.callTheStaticConstructor();
  }
}
