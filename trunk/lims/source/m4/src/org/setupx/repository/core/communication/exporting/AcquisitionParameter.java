package org.setupx.repository.core.communication.exporting;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.util.xml.XPath;
import org.setupx.repository.core.util.xml.XPathException;
import org.setupx.repository.server.persistence.hibernate.SXSerializable;


/**
 * Parameters of an acquisitionsample
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 *
 * @hibernate.class table = "acquisitionparameter"
 */
public class AcquisitionParameter extends CoreObject implements SXSerializable {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String label;
  private String value;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static Hashtable limitations = new Hashtable();
  public static final String QC_METHOD = "QC Method";
  public static final String TRAY = "tray";
  public static final String TYPE = "Type";
  public static final String FOLDER = "Folder";
  public static final String NAME = "Name";
  public static final String STATUS = "Status";
  public static final String VIAL = "Vial";
  public static final String REPETITIONS = "Repetitions";
  public static final String AS_METHOD = "ASMethod";
  public static final String GC_METHOD = "GCMethod";
  public static final String MS_METHOD = "MSMethod";
  public static final String DP_METHOD = "DPMethod";
  public static Iterator labelIterator = fillLabelIterator();
  public static Hashtable positionsInLine = new Hashtable();
  public static final String[] GC_METHODS = new String[] {
      "\\Gas Chromatograph (GC) Methods\\Tobias\\quick-split-1:5", "\\Gas Chromatograph (GC) Methods\\Tobias\\quick-split-1:10", "\\Gas Chromatograph (GC) Methods\\Tobias\\quick-split-1:20", "\\Gas Chromatograph (GC) Methods\\Tobias\\quick-split-1:50",
      "\\Gas Chromatograph (GC) Methods\\Tobias\\quick-split-1:100", "\\Gas Chromatograph (GC) Methods\\Tobias\\quick-split-1:5_liner deactivate", "\\Gas Chromatograph (GC) Methods\\Tobias\\quick-split-1:5_Agilent Liner"
    };

  static {
    // just add those, that are limited in there values
    limitations.put(TYPE, new String[] { "Sample", "Mass Calibration", "Ion Optic Focus", "Filament Focus", "Acquisation System Adjust" });
    limitations.put(FOLDER, new String[] { "\\Acquired Samples\\SetupX" });
    limitations.put(REPETITIONS, new String[] { "1" });
    limitations.put(MS_METHOD, "\\Mass Spectrometer (MS) Methods\\Tobias\\quick");
    addLimitations(GC_METHOD, GC_METHODS);

    // define the positotions for each value in the file
    positionsInLine.put(QC_METHOD, 4 + "");
    positionsInLine.put(TRAY, 9 + "");
    positionsInLine.put(TYPE, 2 + "");
    positionsInLine.put(FOLDER, 3 + "");
    positionsInLine.put(NAME, 1 + "");
    //positionsInLine.put(STATUS, 1+"");
    positionsInLine.put(VIAL, 10 + "");
    positionsInLine.put(REPETITIONS, 11 + "");
    positionsInLine.put(AS_METHOD, 5 + "");
    positionsInLine.put(GC_METHOD, 6 + "");
    positionsInLine.put(MS_METHOD, 7 + "");
    positionsInLine.put(DP_METHOD, 8 + "");
  }

  private static Class thiZ = AcquisitionParameter.class;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new AcquisitionParameter object.
   */
  public AcquisitionParameter() {
    super();
  }

  /**
   * Creates a new Parameter object.
   *
   * @param label 
   * @param value 
   *
   * @throws ParameterException 
   */
  public AcquisitionParameter(String label, String value)
    throws ParameterException {
    setValue(label, value);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param parameter TODO
   *
   * @return TODO
   *
   * @throws PositionUndefindedException TODO
   */
  public static int getPostitionForParameter(final String parameter)
    throws PositionUndefindedException {
    String pos;

    try {
      pos = (String) positionsInLine.get(parameter);

      return Integer.parseInt(pos);
    } catch (Exception e) {
      throw new PositionUndefindedException("Position not available");
    }
  }

  /**
   * TODO: 
   *
   * @param label TODO
   */
  public void setLabel(String label) {
    this.label = label;
  }

  /**
   * returns the label of this parameter
   *
   * @return the label of this parameter
   *
   * @hibernate.property
   */
  public String getLabel() {
    return this.label;
  }

  /**
   * The setter method for this Customer's identifier.
   */
  public void setUOID(long lId) {
    uoid = lId;
  }

  /**
   * @hibernate.id column = "uoid" generator-class="native"
   */
  public long getUOID() {
    return (uoid);
  }

  /**
   * TODO: 
   *
   * @param value TODO
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * @hibernate.property
   */
  public String getValue() {
    return this.value;
  }

  /**
   * returns the value for a label from an document.
   *
   * @param doc the document containing the values ...
   * @param label the label of the parameter to look for in the document
   *
   * @return the value found in the document
   *
   * @throws DOMException TODO
   * @throws XPathException TODO
   */
  public static final String map(Document doc, String label)
    throws DOMException, XPathException {
    String result_value = "";

    try {
      if (label.equals(QC_METHOD)) {
        result_value = XPath.path(doc, "/sample/meta/gc-method").item(0).getFirstChild().getNodeValue();
      } /*else if (label.equals(TRAY)) {
         value = XPath.path(doc, "/sample/meta/as-method").item(0).getFirstChild().getNodeValue();
         } */
      else if (label.equals(TYPE)) {
        result_value = XPath.path(doc, "/sample/meta/type").item(0).getFirstChild().getNodeValue();
      } else if (label.equals(NAME)) {
        result_value = XPath.path(doc, "/sample/meta/name").item(0).getFirstChild().getNodeValue();
      } else if (label.equals(STATUS)) {
        result_value = XPath.path(doc, "/sample/meta/status").item(0).getFirstChild().getNodeValue();
      } else if (label.equals(VIAL)) {
        result_value = XPath.path(doc, "/sample/meta/vial").item(0).getFirstChild().getNodeValue();
      } /*else if (label.equals(REPETITIONS)) {
         value = XPath.path(doc, "/sample/meta/").item(0).getFirstChild().getNodeValue();
         } */
      else if (label.equals(AS_METHOD)) {
        result_value = XPath.path(doc, "/sample/meta/as-method").item(0).getFirstChild().getNodeValue();
      } else if (label.equals(GC_METHOD)) {
        result_value = XPath.path(doc, "/sample/meta/gc-method").item(0).getFirstChild().getNodeValue();
      } else if (label.equals(MS_METHOD)) {
        result_value = XPath.path(doc, "/sample/meta/ms-method").item(0).getFirstChild().getNodeValue();
      } else if (label.equals(DP_METHOD)) {
        result_value = XPath.path(doc, "/sample/meta/dp-method").item(0).getFirstChild().getNodeValue();
      } else {
        result_value = "";
      }
    } catch (NullPointerException e) {
      throw new XPathException("unable to find a mapped value for " + label);
    }

    /*    log(thiZ, "_______________________________________________________________________");
       log(thiZ, "looked for : #" + label + "#  found in document: #" + result_value + "#");
       log(thiZ, "_______________________________________________________________________");*/
    return result_value;
  }

  /*
     protected static Iterator createMappingIterator(Sample sample) throws MappingError {
     Vector v = new Vector();
     Iterator labels = getLabelIterator();
     String _tempLabel = "";
     String _tempValue = "";
     AcquisitionParameter _acquisitionParameter = null;
     //    debug(thiz, " number of possible labels for a acquisitionparameter: " + labels.)
     while (labels.hasNext()) {
       _tempLabel = (String) labels.next();
       try {
         _tempValue = map(information, _tempLabel);
         _acquisitionParameter = new AcquisitionParameter(_tempLabel, _tempValue);
       } catch (ParameterException e) {
         e.printStackTrace();
       } catch (XPathException e) {
         throw new MappingError("unable to create a mapping iterator, cause some of the parameters for the exporting are not set. Maybe the meta inf from the machine are not importet yet.");
       }
       v.add(_acquisitionParameter);
     }
     return v.iterator();
     }
   */

  /**
   * @return
   */
  public static Iterator getLabelIterator() {
    //if (labelIterator == null) {
    labelIterator = fillLabelIterator();

    //}
    if ((labelIterator == null) || !labelIterator.hasNext()) {
      err(null, new RuntimeException("ERROR   - the iterator containing the labels is null or empty " + labelIterator));
    }

    return labelIterator;
  }

  /**
   * TODO: 
   *
   * @param acquisitionParameterName TODO
   * @param results TODO
   *
   * @return TODO
   *
   * @throws ParameterException TODO
   */
  public static final AcquisitionParameter[] map(String acquisitionParameterName, HashSet results)
    throws ParameterException {
    AcquisitionParameter[] result = new AcquisitionParameter[results.size()];

    Iterator iterator = results.iterator();

    int i = 0;

    while (iterator.hasNext()) {
      result[i] = new AcquisitionParameter(acquisitionParameterName, (String) iterator.next());
      i++;
    }

    return result;
  }

  /*
   *  (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    //return org.setupx.repository.core.util.Util.getClassName(this) + ": " + this.getLabel() + " = " + this.getValue();
    return this.getLabel() + " = " + this.getValue();
  }

  /**
   * TODO: 
   *
   * @param document TODO
   *
   * @return TODO
   *
   * @throws MappingError
   */
  protected static Iterator createMappingIterator(Document document)
    throws MappingError {
    Vector v = new Vector();

    Iterator labels = getLabelIterator();
    String _tempLabel = "";
    String _tempValue = "";
    AcquisitionParameter _acquisitionParameter = null;

    //    debug(thiz, " number of possible labels for a acquisitionparameter: " + labels.)
    while (labels.hasNext()) {
      _tempLabel = (String) labels.next();

      try {
        _tempValue = map(document, _tempLabel);
        _acquisitionParameter = new AcquisitionParameter(_tempLabel, _tempValue);
      } catch (ParameterException e) {
        e.printStackTrace();
      } catch (XPathException e) {
        throw new MappingError("unable to create a mapping iterator, cause some of the parameters for the exporting are not set. Maybe the meta inf from the machine are not importet yet.");
      }

      v.add(_acquisitionParameter);
    }

    return v.iterator();
  }

  /**
   * TODO: 
   *
   * @param gc_method2 TODO
   * @param gc_methods2 TODO
   */
  private static void addLimitations(String gc_method2, String[] gc_methods2) {
    for (int i = 0; i < gc_methods2.length; i++) {
      limitations.put(gc_method2, gc_methods2[i]);
    }
  }

  /**
   * return an iterator containing all labels for all possible values
   *
   * @return an iterator containing all labels for all possible values
   */
  private static Iterator fillLabelIterator() {
    // init the labels
    Vector v = new Vector();

    //v.add(QC_METHOD);
    v.add(TRAY);
    //v.add(TYPE);
    v.add(FOLDER);
    v.add(NAME);
    v.add(STATUS);
    v.add(VIAL);
    v.add(REPETITIONS);
    v.add(AS_METHOD);
    v.add(GC_METHOD);
    v.add(MS_METHOD);
    v.add(DP_METHOD);

    // debug(thiZ, "filling the labeliterator with \"" + v.size() + "\" values");
    Iterator iter = v.iterator();

    // debug(thiZ, "iterator does habe next: " + iter.hasNext());
    return iter;
  }

  /**
   * checks if there are any limitations for this parameter
   *
   * @param label the label of the parameter to check
   *
   * @return true if the parameter is limited in any kind
   */
  private boolean isLimited(String label) {
    return (limitations.containsKey(label));
  }

  /**
   * TODO: 
   *
   * @param label2 TODO
   * @param value2 TODO
   *
   * @throws ParameterException TODO
   */
  private void setValue(String label2, String value2) throws ParameterException {
    if ((value2 == null) || ((label2 == null) || (label2.length() < 2))) {
      err("illegal label \": " + label2);
      throw new ParameterException(label2, value2);
    }

    if (isLimited(label2)) {
      checkValue(label2, value2);
    }

    //debug("new label: " + label2 + " value: " + value2 + "");
    this.label = label2;
    this.value = value2;
  }

  /**
   * checks if there are any limitations for a value ...
   *
   * @param label the label of the parameter to check
   * @param value the value that has to be checked
   *
   * @throws ParameterException if the value is not valid for this parameter
   */
  private void checkValue(String i_label, String i_value)
    throws ParameterException {
    String[] values = null;

    try {
      values = (String[]) limitations.get(i_label);
    } catch (ClassCastException e) {
      warning(thiZ, "unable to check value: " + i_value + " with label: " + i_label);

      return;
    }

    if (i_value == null) {
      err("illegal value for \"" + i_label + "\": it is null.");

      return;
    } else {
      //debug(this, "number of limitations: " + values.length);
      for (int i = 0; i < values.length; i++) {
        //debug(this, "limitation " + i + " " + values[i]);
      }
    }

    boolean valid = false;

    for (int i = 0; i < values.length; i++) {
      if (values[i].equals(i_value)) {
        valid = true;
      }
    }

    if (!valid) {
      throw new ParameterException(i_label, i_value);
    }
  }
}
