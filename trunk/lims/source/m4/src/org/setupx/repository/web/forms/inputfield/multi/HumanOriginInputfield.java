/**
 * ============================================================================ File:    HumanOriginInputfield.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.clazzes.AlignY;
import org.setupx.repository.web.forms.inputfield.BigStringInputField;
import org.setupx.repository.web.forms.inputfield.InputField;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.3 $
 *
 * @hibernate.subclass
 */
public class HumanOriginInputfield extends MultiField4Clazz implements AlignY {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new HumanOriginInputfield object.
   */
  public HumanOriginInputfield() {
    super("Origin (human)", "the origin of the human samples");

    InputField field = new BigStringInputField("information", "any information related to the origin");
    this.addField(field);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getPath()
   */
  public String getPath() throws UnsupportedClazzMappingException {
    throw new UnsupportedClazzMappingException();
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getSingleValueField()
   */
  public FormObject getSingleValueField() throws UnsupportedClazzMappingException {
    throw new UnsupportedClazzMappingException();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.clazzes.ClazzInformation#createShortLabel()
   */
  public String createShortLabel() {
    return this.toStringHTML();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.MultiField4Clazz#toStringHTML()
   */
  public String toStringHTML() {
    return ((InputField) this.getField(0)).getValue() + "";
  }
}
