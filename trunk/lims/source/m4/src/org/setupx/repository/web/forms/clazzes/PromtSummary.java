/**
 * ============================================================================ File:    PromtSummary.java Package: org.setupx.repository.web.forms.clazzes cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.clazzes;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.web.forms.ClazzNotFoundException;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;
import org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz;
import org.setupx.repository.web.forms.inputfield.multi.Promt;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 */
public class PromtSummary extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private ClazzDimension x;
  private Promt promt;
  private ClazzDimension[] y = new ClazzDimension[0];

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new PromtSummary object.
   *
   * @param promt 
   */
  public PromtSummary(Promt promt) {
    this.promt = promt;

    //log(this, promt.showIt(0));
    ClazzDimension[] dimensions = promt.determineClazzDimensions();

    // use longest dimension for X-Axis
    // deprecated: ClazzDimension longestDimension = ClazzDimension.determineLargestDimension(dimensions);
    // use X Values
    ClazzDimension longestDimension = ClazzDimension.determineXDimension(dimensions);
    this.setX(longestDimension);

    // add all other dimensions to the summary
    for (int i = 0; i < dimensions.length; i++) {
      ClazzDimension dimension = dimensions[i];

      if (  
              // in case it is not the longestDimension, cause that one is on the X axis
              (dimension != longestDimension 
              // and has more than one elemen
              && dimension.size() > 1)
              // or if is just the only one (y is still empty and it is the last one. 
              || ( this.y.length == 0 && i+1 == dimension.size() )) {
        this.addY(dimension);
      }
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param longestDimension
   */
  public void setX(ClazzDimension longestDimension) {
    this.x = longestDimension;
  }

  /**
   * @param dimension
   */
  public void addY(ClazzDimension dimension) {
    //debug("expanding from " + this.y.length + " to " + (this.y.length + 1));
    ClazzDimension[] clazzDimensions = new ClazzDimension[this.y.length + 1];

    for (int i = 0; i < this.y.length; i++) {
      clazzDimensions[i] = this.y[i];
    }

    clazzDimensions[clazzDimensions.length - 1] = dimension;
    this.y = clazzDimensions;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String createHTMLTable() {
    //int numLines = promt.calculateNumClasses();
    StringBuffer buffer = new StringBuffer();
    buffer.append("<!--  Classes     -->");
    buffer.append("<table border = \"2\">\n");

    // X-AXIS
    buffer.append("<tr>");

    // space for the classdescribtions
    buffer.append("<td colspan =\"" + this.y.length + "\" ></td>");

    // TODO - change from one dimension on x axis to n dimensions for x axis
    // each part of the longest dimension
    for (int i = 0; i < x.size(); i++) {
      buffer.append(x.getDescription(i, true, true));
      //buffer.append("<td><font size=\"-1\" >" + x.getField(i).toStringHTML() + " " + x.getField(i).createLinkedIMG() + "</font></td>");
    }

    buffer.append("</tr>");

    // array with the pos of each dimension
    int[] positions = new int[y.length];

    boolean loop = true;

    while (loop) {
      buffer.append("<tr>");

      MultiField4Clazz[] multiField4Clazzs = new MultiField4Clazz[y.length + 1]; // one additional place for the x-axis-Multifiled4Clazz

      // spalte 0 wert 0 | spalte 1 wert 0 | ... bis anzahl der spalten
      for (int i = 0; i < y.length; i++) {
        //debug(i + "pos: " + positions[i] + "   " + y[i].getFormObject(positions[i]).getName());
        buffer.append(y[i].getDescription(positions[i], true, true));
        multiField4Clazzs[i] = y[i].getField(positions[i]);
      }

      // the classes inside the table
      for (int i = 0; i < x.size(); i++) {
        // get the clazz and display number of samples and button for edit
        //find all MultiField4Clazzes
        multiField4Clazzs[multiField4Clazzs.length - 1] = x.getField(i);

        try {
          Clazz clazz = promt.getClazzDimensionDetector().getClazz(multiField4Clazzs);
          buffer.append("<td align=\"center\"" + selectBackground(clazz.isActive()) + ">");

          buffer.append("<span class=\"small\">ClassID: <b>" + clazz.getUOID() + "</b></span>");

          if (clazz.isActive()) {
            buffer.append("<span class=\"small\">   [" + clazz.size() + " Samples]</span>");
          }

          buffer.append("<br>");

          buffer.append(clazz.createActivateButton());

          if (clazz.isActive()) {
            //debug("number of related multifield4class in clazz: " + clazz.multiField4Clazzs().length);
            //debug("number of samples in clazz: " + clazz.size());
            // first sample used for link
            buffer.append(clazz.createLinkedIMG());
            //if (clazz.getField(0) != null) {
            //  buffer.append(((Sample) clazz.getField(0)).createLinkedIMG() + "  " + clazz.size() + " samples");
            //} else {
            buffer.append("     ");
            buffer.append(clazz.createExpandButtonADD());
            buffer.append(clazz.createExpandButtonREDUCE());
            //}
            //buffer.append("<td>number of related multifield4class in clazz: " + clazz.multiField4Clazzs().length  + "<br>number of samples in clazz: " + clazz.size() + "</td>");
          }
        } catch (ClazzNotFoundException e1) {
          e1.printStackTrace();
        }

        buffer.append("</td>");
      }

      // add next step in last dimension

      /*for (int i = 0; i < positions.length; i++) {
         debug("pos " + (i) + ": " + (positions[i])); // + "  number of elements in dim:" + y[positions[i]].size());
         }*/

      // if step is at max set it to 0 and increase position of dimension last-1
      int posInArray = y.length - 1;

      // ----> increasing the positions of the dimensions
      boolean looping = true;
      
      while (looping) {
        if (positions[posInArray] == (y[posInArray].size() - 1)) {
          debug(this, "max(" + y[posInArray].size() + ") for dimenson nr." + (posInArray + 1) + "/" + y.length + " reached.");
          positions[posInArray] = 0;
          posInArray--;

          if (posInArray == -1) {
            looping = false;
            loop = false;
            debug(this, "final end reached " + posInArray);
          }
        } else {
          // increase position of pos in the dimension
          positions[posInArray]++;
          looping = false;
          //debug("increaesed pos of dimension " + posInArray + " to " + positions[posInArray]);
        }
      }

      // <---  increasing the positions of the dimensions
      buffer.append("</tr>\n");
    }

    buffer.append("</table>");
    buffer.append("<!--  Classes     -->");

    return buffer.toString();
  }

  /**
   * generated summary for the small columns
   *
   * @return summary
   */
  public String createHTMLsummaryShort() {
    StringBuffer buffer = new StringBuffer(30);

    // --- number of dimension
    int numberOfDimensions = promt.getClazzDimensionDetector().getClazzDimensions().length;

    // --- number of classes
    int numOfClasses = promt.numberClasses();

    // --- number of samples
    int numOfSamples = promt.numberSamples();

    buffer.append("<span class=\"portlet_header\">current status:</span>");
    buffer.append("<span class=\"small\">");
    //buffer.append("<br><b>number of dimension:</b>");
    //buffer.append("<br>" + numberOfDimensions);
    buffer.append("<br><b>number of classes:</b>");
    buffer.append("<br>" + numOfClasses);
    buffer.append("<br><b>number of samples:</b>");
    buffer.append("<br>" + numOfSamples);

    /*
       try {
         buffer.append(((Sample) promt.getClazzDimensionDetector().getClazzDimensions()[0].get(0)).createLinkedIMG());
       } catch (Exception e) {
    
         warning(e);
       }
     */
    buffer.append("</span>");

    /*
     * Deactivated
           buffer.append("<br><span class=\"portlet_header\">current price:</span>");
           buffer.append("<span class=\"small\">");
           buffer.append("<br>" + "</span><b> " + promt.totalPrice() + "<span class=\"small\"></b>");
           buffer.append("<br>" + "This is the <br>anticipated price for <br>the whole experiment.");
           buffer.append("</span>");
     */
    return buffer.toString();
  }

  /**
   * TODO: 
   *
   * @param dimension TODO
   *
   * @return TODO
   */
  private String getHTMLrow(ClazzDimension dimension) {
    StringBuffer buffer = new StringBuffer();

    for (int j = 0; j < dimension.size(); j++) {
      MultiField4Clazz object = dimension.getField(j);
      buffer.append("<td>");
      buffer.append(object.getName());
      buffer.append("</td>");
    }

    return buffer.toString();
  }

  /**
   * @param b
   *
   * @return
   */
  private String selectBackground(boolean b) {
    String color;

    // if true - white 
    if (b) {
      color = "#FFFFFF";
    }
    // else #C0C0C0
    else {
      color = "#C0C0C0";
    }

    return "style=background-color:" + color;
  }
}
