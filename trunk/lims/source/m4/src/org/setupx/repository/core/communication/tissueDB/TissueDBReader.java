/**
 * ============================================================================ File:    TissueDBReader.java Package: org.setupx.repository.core.communication.tissueDB cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.tissueDB;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.File;

import java.io.IOException;

import java.util.Hashtable;
import java.util.StringTokenizer;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.8 $
 * @deprecated
 */
public class TissueDBReader extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static boolean TISSUE_ACTIVATED = false;
  public static Hashtable data = read();
  private static Object thiz = TissueDBReader.class;

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   */
  public static void init() {
    try {
      data = read();
    } catch (Throwable e) {
      e.printStackTrace();
    }
  }

  /**
   * TODO: 
   *
   * @param args TODO
   */
  public static void main(String[] args) {
    log(null, "entries: " + data.size());
  }

  /**
   * @param filename
   *
   * @throws IOException
   */
  public static Hashtable parseHTML_file(String filename)
    throws IOException {
    log(thiz, "start parsing " + filename);

    String searchString = "tissueId=";

    // open file and seperate
    log(thiz, "file length: " + File.getFileContent(new java.io.File(filename)).length());

    StringTokenizer stringTokenizer = new StringTokenizer(File.getFileContent(new java.io.File(filename)), "<");

    Hashtable hashtable = new Hashtable();

    while (stringTokenizer.hasMoreElements()) {
      String element = (String) stringTokenizer.nextElement();

      // a href="tissue.xml?tissueId=THS002552">serosa (large intestine)
      if ((element.charAt(element.length() - 1) != '>') && (element.lastIndexOf("middle") < 1) && (element.lastIndexOf("&nbsp;") < 1)) {
        //log(thiz, element);
        // cut the id out
        int posStart = element.indexOf(searchString);

        if (posStart > 1) {
          int posEnd = element.indexOf('"', posStart);

          //log(thiz,"" + posStart);
          //log(thiz,"" + posEnd);
          if ((posEnd > 1) && (posStart > 1)) {
            String tissueDBid = element.substring(posStart + searchString.length(), posEnd);

            // cut the nameout
            String name = element.substring(posEnd + 2);

            log(thiz, "" + name + ": " + tissueDBid);

            hashtable.put(name, tissueDBid);
          } else {
            log(thiz, "  " + posStart + "   " + posEnd + "   ");
          }
        }
      }
    }

    return hashtable;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  private static Hashtable read() {
    if (!TISSUE_ACTIVATED) {
      return new Hashtable();
    }

    String filename = Config.FILE_TISSUE_DB;

    // debuggin 
    filename = "/lims/data/tissuedb/tissue.html";

    java.io.File file = new java.io.File(filename);

    log(thiz, "age of the file: " + (File.howOld(file) / 1000 / 60) + " minutes.");

    /*
       // (100*60*24*24*31) is a month
       if ((!file.exists()) || File.howOld(file) > (100*60*24*24*31)){
           log(thiz, "downloading a new Version of the TissueDB cause the file is to old than a month or does not exist.");
           try {
               File.download(new URL("http://tissuedb.ontology.ims.u-tokyo.ac.jp:8082/tissuedb/hierarchy.xml?query=*"),file);
           }
    
                                   catch (Exception e) {
                                       err(thiz, new CommunicationException("unable to connect do tissue db server.", e));
                                   }
                               }
     */
    try {
      return parseHTML_file(filename);
    } catch (IOException e) {
      e.printStackTrace();

      return new Hashtable();
    }
  }
}
