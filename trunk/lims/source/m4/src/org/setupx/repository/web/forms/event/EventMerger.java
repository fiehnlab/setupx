/**
 * ============================================================================ File:    EventMerger.java Package: org.setupx.repository.web.forms.event cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.event;

import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.web.forms.inputfield.multi.Sample;

import java.util.Iterator;
import java.util.Set;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class EventMerger {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new EventMerger object.
   *
   * @param events 
   *
   * @throws EventMergerException
   */
  public EventMerger(Set events) throws EventMergerException {
    Iterator iterator = events.iterator();

    // each sample
    while (iterator.hasNext()) {
      MachineRunEvent machineRunEvent = (MachineRunEvent) iterator.next();

      // take name and look in all events if they have been sheduled with this name
      Sample sample;

      try {
        sample = Sample.findByAcquisitionName(machineRunEvent.getName());
        sample.addEvent(machineRunEvent);
        sample.update(true);
      } catch (PersistenceActionException e) {
        throw new EventMergerException("unable to store " + org.setupx.repository.core.util.Util.getClassName(this), e);
      }
    }
  }
}
