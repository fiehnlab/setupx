/**
 * ============================================================================ File:    SampleSelector.java Package: org.setupx.repository.web cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web;

import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.core.util.web.UiHttpServlet;
import org.setupx.repository.web.forms.inputfield.multi.Sample;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * selection of samples
 */
public class SampleSelector extends UiHttpServlet {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String PARAMETER_PRE_LABEL = "sample";

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.core.util.web.UiHttpServlet#process(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  public void process(HttpServletRequest request, HttpServletResponse response)
    throws Throwable {
    // DEBUGGING
    this.printRequestParameters(request);
    this.printSessionAttributes(request);

    // take the list of selected samples from the session
    HashSet selectedSamples = (HashSet) request.getSession().getValue(WebConstants.SESS_SAMPLES_SELECTED);

    // id that will be taken from the hashset 
    String removeID = (String) request.getSession().getValue(WebConstants.SESS_SAMPLES_REMOVEID);

    if (selectedSamples == null) {
      Logger.warning(this, "there are NO samples in the session");
      selectedSamples = new HashSet();
    }

    Iterator iter = selectedSamples.iterator();

    /** while (iter.hasNext()) { Sample element = (Sample) iter.next(); if (element.getID().compareTo(removeID) == 0){ selectedSamples.remove(element); }} */

    // take all fields named sampleXX 
    Enumeration parameterNames = request.getParameterNames();

    while (parameterNames.hasMoreElements()) {
      String parametername = (String) parameterNames.nextElement();

      // if it is a sample 
      if (parametername.startsWith(PARAMETER_PRE_LABEL)) {
        // check of they are active or not 
        String value = request.getParameter(parametername);
        Logger.log(this, "found " + parametername + " : " + value);

        // we have a hit - get the real sample
        // get the id
        long id = Long.parseLong(value);

        Logger.log(this, "going to add " + id + " (" + value + ")");

        // get those samples and add them to the acquistionList
        Sample sample = (Sample) Sample.persistence_loadByID(Sample.class, id);
        selectedSamples.add(sample);
      }
    }

    debug("currently selected samples: " + selectedSamples.size());
    request.getSession().putValue(WebConstants.SESS_SAMPLES_SELECTED, selectedSamples);
    forward("/aq/sample_list.jsp", request, response);
  }
}
