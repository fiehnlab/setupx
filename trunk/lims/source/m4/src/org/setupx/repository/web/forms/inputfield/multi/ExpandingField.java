/**
 * ============================================================================ File:    ExpandingField.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.web.forms.ArrayExpandException;
import org.setupx.repository.web.forms.Controller;
import org.setupx.repository.web.forms.FormContainer;


/**
 * Object containing other objects that can <b>grow</b> by creating a clone of the last object. While the objects that  implement the formcontainer just have the ability to contain other objects and add new ones (of the  type FormObject), this object is able to grow by <code>cloning </code>the last
 * object.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 */
public interface ExpandingField extends FormContainer {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a inputfield and submit button. The value in the inputfiled will be read by the controller-servlet, which is expanding the filedarray by the number defefined in the inputfiled.
   * <pre>TODO - replace into jsp</pre>
   *
   * @return a String representing a inputfields and a submitbutton to expand this array.
   *
   * @see Controller
   */
  public String createExpandButton();

  /**
   * adds a numner of clones to the array of fields.  <br> The cloned object is taken from the array at the last position. <br> The number of elements that can be added is <strong>limited to 10.</strong> to get rid of extreme big expansions in the frontend.
   *
   * @param i the number of samples that will be <strong>added</strong> to the existing set of fields.
   *
   * @throws ArrayExpandException if the array does not contain an element that can be used to act as a <strong>clone</strong> for the new elements that will be added.
   */
  public void expand(int i) throws ArrayExpandException;
}
