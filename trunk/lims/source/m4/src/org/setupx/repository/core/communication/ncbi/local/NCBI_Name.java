package org.setupx.repository.core.communication.ncbi.local;

import java.util.StringTokenizer;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
class NCBI_Name extends NCBI_dump_Information {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static StringTokenizer stringTokenizer = null;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected NCBI_Name() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public boolean isCommonName() {
    String value = "common name";

    return check(value);
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public boolean isEquivalentName() {
    String value = "equivalent name";

    return check(value);
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public boolean isMisspelling() {
    String value = "misspelling";

    return check(value);
  }

  /**
   * @return
   */
  public String getName() {
    return ((String) this.vec.get(1));
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public boolean isScientificName() {
    String value = "scientific name";

    return check(value);
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public boolean isSynonym() {
    String value = "synonym";

    return check(value);
  }
}
