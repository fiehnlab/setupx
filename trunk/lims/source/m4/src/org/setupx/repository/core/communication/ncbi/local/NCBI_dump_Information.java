/**
 * ============================================================================ File:    NCBI_dump_Information.java Package: org.setupx.repository.core.communication.ncbi.local cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ncbi.local;

import org.setupx.repository.core.CoreObject;

import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;


/**
 * abstract class for every object created from the NCBI information files
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
abstract class NCBI_dump_Information extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected Vector vec = new Vector();

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static StringTokenizer stringTokenizer = null;

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * fills a clean instance with the different values cutted out of the long string - seperated by <code>tab </code>and <code>|</code>
   *
   * @param object the object that will be filled
   * @param instance that will be filled with valued from the string
   *
   * @return the filled instance
   */
  public final static NCBI_dump_Information setUpInstance(String string, NCBI_dump_Information instance) {
    //debug (null, "cutting: " + object);
    stringTokenizer = new StringTokenizer(string, "\t|");

    //debug(NCBI_Name.class, "found " + stringTokenizer.countTokens() + " attributes.");
    String element = null;

    while (stringTokenizer.hasMoreElements()) {
      element = (String) stringTokenizer.nextElement();
      //debug(null, " ---> " + element);
      instance.add(element);
    }

    return instance;
  }

  /**
   * return the tax id of this entry
   *
   * @return the tax id of this entry
   */
  public String getTAX_id() {
    return ((String) this.vec.get(0));
  }

  /*
   *  (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    StringBuffer buffer = new StringBuffer();
    Iterator iterator = vec.iterator();
    int i = 0;

    while (iterator.hasNext()) {
      String element = (String) iterator.next();
      buffer.append("   " + i + ": [" + element + "]");
      i++;
    }

    return buffer.toString();
  }

  /**
   * checks if a value is stored in pos 2
   * 
   * <p></p>
   *
   * @param value the value will be compared with this one
   *
   * @return true if the value is identical with value at pos 2 otherwise false
   */
  protected boolean check(String value) {
    try {
      return (((String) this.vec.get(2)).compareTo(value) == 0);
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * adds a value to the internal hashtable
   *
   * @param element the element to be added
   */
  private void add(String element) {
    int pos = element.length();
    boolean loop = true;

    while (loop) {
      if (element.charAt(pos - 1) == ' ') {
        pos--;
      } else {
        loop = false;
        element = element.substring(0, pos);
      }
    }

    //debug(this, "\"" + element + "\"");
    this.vec.add(element);
  }
}
