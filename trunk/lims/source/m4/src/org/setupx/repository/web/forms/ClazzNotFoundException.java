/*
 * Created on 06.05.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.setupx.repository.web.forms;

import org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz;

public class ClazzNotFoundException extends Exception {

    public  MultiField4Clazz[] multiField4Clazzs;

    /**
     * 
     */
    public ClazzNotFoundException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public ClazzNotFoundException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public ClazzNotFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public ClazzNotFoundException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param multiField4Clazzs
     */
    public ClazzNotFoundException(MultiField4Clazz[] multiField4Clazzs) {
        super();
        this.multiField4Clazzs = multiField4Clazzs;
    }

}
