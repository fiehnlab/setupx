package org.setupx.repository.web.forms;

public class InActiveException extends XMLException {

    public InActiveException(String string) {
        super(string);
    }
}
