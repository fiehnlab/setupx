/**
 * ============================================================================ File:    NCBI_Config.java Package: org.setupx.repository.core.communication.ncbi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ncbi;

/**
 * Configuration for the NCBI Validation
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.8 $
 */
public class NCBI_Config {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** activate or deactivate the LOCAL NCBI validation */
  public static final boolean NCBI_local_database_acitve = true;
  public static final boolean NCBI_precaching_acitve = false;
}
