/**
 * ============================================================================ File:    OldClassTemplate.java Package: org.setupx.repository.core.util.hotfix.oldsamples cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.hotfix.oldsamples;

import org.setupx.repository.core.communication.experimentgeneration.template.ClazzTemplate;
import org.setupx.repository.core.communication.experimentgeneration.template.SampleTemplate;

import java.util.Hashtable;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class OldClassTemplate implements ClazzTemplate {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Hashtable values;
  private SampleTemplate[] samples;

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public SampleTemplate[] getSamples() {
    return this.samples;
  }

  /**
   * creating a clazztemplate out of a set of values
   *
   * @param set
   *
   * @return
   */
  public static final ClazzTemplate createInstance(Hashtable set) {
    OldClassTemplate clazzTemplate = new OldClassTemplate();
    clazzTemplate.values = set;

    return clazzTemplate;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.communication.experimentgeneration.template.ClazzTemplate#getPathes()
   */
  public final String[] getPathes() {
    return OldSampleMapping.getPath();
  }

  /**
   * TODO: 
   *
   * @param sampleTemplates TODO
   */
  public void setSamples(SampleTemplate[] sampleTemplates) {
    this.samples = sampleTemplates;
  }

  /**
   * Hashtable with all the values for that path
   */
  public String getValue(String path) {
    return "" + this.values.get(path);
  }

  /*
   *  (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    StringBuffer buffer = new StringBuffer();
    String[] pathes = OldSampleMapping.getPath();

    for (int i = 0; i < pathes.length; i++) {
      buffer.append(pathes[i] + " " + this.getValue(pathes[i]));
      buffer.append("\n");
    }

    return buffer.toString();
  }
}
