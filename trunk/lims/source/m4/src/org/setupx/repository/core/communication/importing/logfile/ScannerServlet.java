package org.setupx.repository.core.communication.importing.logfile;

import org.setupx.repository.Config;
import org.setupx.repository.core.communication.leco.GCTOF;
import org.setupx.repository.core.util.web.UiHttpServlet;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Hashtable;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * 
 *
 * @author $author$
 * @version $Revision$
 */
public class ScannerServlet extends HttpServlet {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  LogFileScanner2 thread; // background search thread

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   */
  public void destroy() {
    thread.stop();
  }

  /**
   * TODO: 
   *
   * @param req TODO
   * @param res TODO
   *
   * @throws ServletException TODO
   * @throws IOException TODO
   */
  public void doGet(HttpServletRequest req, HttpServletResponse res)
    throws ServletException, IOException {
    res.setContentType("text/plain");

    PrintWriter out = res.getWriter();
    String string = UiHttpServlet.getRequestParameter(req, "active");
    boolean active = (string.compareTo("true") == 0);
    out.println("active:" + active);

    if (active && !thread.isAlive()) {
      try {
        thread.start();
        //thread.run();
      } catch (Exception e) {
        out.print(e.toString());

        try {
          thread = new LogFileScanner2(18000, new java.io.File(Config.DIRECTORY_LOGFILES_MACHINE), new GCTOF());
        } catch (Exception e2) {
          out.print(e.toString());
          out.print(e2.toString());
        }
      }
    } else if (!active && thread.isAlive()) {
      thread.stop();
    }

    out.println("alive:" + thread.isAlive());
    out.println("scanning: " + thread.directory);
    out.println("pause: " + thread.pause);
    out.println("cached: " + thread.cache.size());
    out.println("last scan completed:" + thread.lastScanDate.toLocaleString());
    out.println("now                :" + new Date().toLocaleString());

    GregorianCalendar t = new GregorianCalendar();
    t.setTime(thread.lastScanDate);
    t.add(Calendar.MILLISECOND, thread.pause);
    out.println("next               :" + t.getTime().toLocaleString());
  }

  /**
   * TODO: 
   *
   * @param config TODO
   *
   * @throws ServletException TODO
   */
  public void init(ServletConfig config) throws ServletException {
    super.init(config); // always!

    try {
      thread = new LogFileScanner2(18000, new java.io.File(Config.DIRECTORY_LOGFILES_MACHINE), new GCTOF());

      /*        boolean trueB = true;
         int kI = 0;
         while (trueB) {
             kI++;
             Logger.log(null,"--> " + thread.getName() + "  "  + thread.toString());
         }
       */
    } catch (ScannerConfigurationException e) {
      throw new ServletException(e);
    }

    thread.setPriority(Thread.MIN_PRIORITY); // be a good citizen
    thread.start();
  }
}
