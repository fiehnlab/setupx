/**
 * ============================================================================ File:    MailController.java Package: org.setupx.repository.core.communication.mail cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.mail;

import javax.mail.MessagingException;

import org.setupx.repository.core.Connectable;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.ServiceNotAvailableException;
import org.setupx.repository.core.user.UserConnector;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.UserLocal;
import org.setupx.repository.web.forms.inputfield.multi.Promt;


/**
 * Handling all the mailtraffic in the system. All different kinds of messages. This includes the decision which user is going to  get which mail. The MailController collects the mails for each user and combine the mails in a single mail, to avoid that the users are getting tons of mails.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.14 $
 */
public class MailController extends CoreObject implements Connectable {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static Object thiZ = new MailController();

  /**
   * @label XMLabstractDocument notifications
   */
  private static MailDelivery mailDelivery;
  private static MailContentBox box;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * hashtable containing all different nonsend mailcontent parts, that will be added to a single mail and send then.
   */
  public MailController() {
    debug(this, "new instance");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


  /**
   * takes all non send mailscontents and sends them to the users. In case that there are more then one mailcontents for a single user, they will be combined to a singe mail and then then.
   *
   * @throws MessagingException
   */
  public static void sendNonsendMails() throws MessagingException {
    // check number of mails in the Hashtable
    if (MailContentBox.isEmpty()) {
      return;
    }

    // get all maildeliveries from the box
    debug(thiZ, "number of mailcontentobjects that will be send :" + MailContentBox.size());
    MailContentBox.createAndSend();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.Connectable#isAvailable()
   */
  public void checkAvailability() throws ServiceNotAvailableException {
    MailContentBox.add(new MailContent(new UserLocal("-connectiontest-", "-connectiontest-", new EMailAddress("mscholz@ucdavis.edu")), "-connectiontest-"));

    try {
      sendNonsendMails();
    } catch (MessagingException e) {
      warning(e);
      throw new ServiceNotAvailableException(e);
    }
  }

  /**
   * TODO: 
   *
   * @param address TODO
   * @param subject TODO
   * @param msg TODO
   */
  public static void inform(EMailAddress address, String subject, String msg) {
    MailContentBox.add(new MailContent(address, subject, msg));
  }

  /**
   * TODO: 
   *
   * @param userDO TODO
   * @param promt TODO
   * @param notifyID TODO
   */
  public static void inform(UserDO userDO, Promt promt, int notifyID) {
    MailContent newMail = MailCreator.create(userDO, promt, notifyID);
    MailContentBox.add(newMail);
  }

  /**
   * @param string
   * @param promt
   * @param notify_ID
   */
  public static void inform(String string, Promt promt, int notify_ID) {
    // inform the user
    inform(new UserLocal(string, "--", new EMailAddress(string)), promt, notify_ID);
  }
}
