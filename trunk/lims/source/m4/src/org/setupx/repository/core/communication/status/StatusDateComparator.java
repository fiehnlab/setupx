package org.setupx.repository.core.communication.status;

import java.util.Comparator;

import org.setupx.repository.core.util.logging.Logger;

class StatusDateComparator implements Comparator {

    /*
     * (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    public int compare(Object o1, Object o2) {
        Logger.debug(null, "compare " + o1 + "   " + o2);
        
        Timestamped timestamped1 ;
        Timestamped timestamped2 ;
        
        if (o1 instanceof Timestamped && o2 instanceof Timestamped){
            timestamped1 = (Timestamped)o1;
            timestamped2 = (Timestamped)o2;
        } else {
            Logger.debug(this, "unable to compare cause not both timestamped");
            return 0;
        }
        
        // identical
        if (timestamped1.getDate().getTime() == timestamped2.getDate().getTime()){
            return 0;
        }
        
        if (timestamped1.getDate().getTime() > timestamped2.getDate().getTime()){
            return 1;
        } else {
            return -1;
        }
    }
}
