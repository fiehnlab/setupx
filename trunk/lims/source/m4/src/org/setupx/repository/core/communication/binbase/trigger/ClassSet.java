/**
 * ============================================================================ File:    ClassSet.java Package: org.setupx.repository.core.communication.binbase.trigger cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.binbase.trigger;

import org.setupx.repository.core.communication.leco.ColumnInformation;
import org.setupx.repository.core.communication.status.ImportedEventStatus;
import org.setupx.repository.core.communication.status.PersistentStatus;
import org.setupx.repository.web.forms.inputfield.multi.Sample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;


/**
 * Local Type for the representation on BinBase
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class ClassSet extends Experiment {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private ColumnInformation colInfo = null;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ClassSet object.
   */
  public ClassSet() {
    this(ColumnInformation.RTX5);
  }

  /**
   * Creates a new ClassSet object.
   *
   * @param colInfo 
   */
  public ClassSet(ColumnInformation colInfo) {
    setColInfo(colInfo);
    setIncrease(0);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param colInfo TODO
   */
  public void setColInfo(ColumnInformation colInfo) {
    this.colInfo = colInfo;
    this.setColumn(colInfo.getBbid());
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public ColumnInformation getColInfo() {
    return colInfo;
  }
}
