/**
 * ============================================================================ File:    OBOConnector.java Package: org.setupx.repository.core.communication.ontology.obo cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ontology.obo;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.setupx.repository.Config;
import org.setupx.repository.core.Connectable;
import org.setupx.repository.core.InitException;
import org.setupx.repository.core.communication.ServiceNotAvailableException;
import org.setupx.repository.core.util.logging.Logger;


/**
 * Connector for reading ontologies.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @see OBOConnector#internalTest() shows how it works.
 */
public class OBOConnector implements Connectable {
    //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private OBOFileReader obofilereader;

    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new OBOConnector object.
     *
     * @param fileReader 
     */
    public OBOConnector(OBOFileReader fileReader) {
        this.obofilereader = fileReader;
    }

    /**
     * Creates a new OBOConnector object.
     *
     * @throws InitException 
     */
    public OBOConnector() throws InitException {
        this.obofilereader = new OBOFileReader(new File(Config.FILE_OBO_ANATOMY));
    }

    private OBOConnector(File file) throws InitException {
        Logger.warning(this, "creating a new " + org.setupx.repository.core.util.Util.getClassName(this) + " based on: " + file.toString());
        this.obofilereader = new OBOFileReader(file);
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * TODO: 
     *
     * @return TODO
     */
    public OBOFileReader getFileReader() {
        return obofilereader;
    }

    /**
     * TODO: 
     *
     * @throws ServiceNotAvailableException TODO
     */
    public void checkAvailability() throws ServiceNotAvailableException {
    }

    /**
     * TODO: 
     *
     * @param term TODO
     * @param relationType TODO
     *
     * @return the list of related objects and an empty set if the term does not have any relations
     *
     * @throws OBOFinderException an object is in any of the relations that can not be found
     */
    public Set determineRelatedTermsByRelation(OBOTerm term, String relationType)
    throws OBOFinderException {
        HashSet result = new HashSet();
        ArrayList list = term.getRelationship(relationType);

        if ((list == null) || (list.size() == 0)) {
            // no related terms found
            return result;
        }

        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            String relation_id = (String) iterator.next();
            OBOTerm temp_term = (OBOTerm) this.determineTerm(relation_id);
            result.add(temp_term);
        }

        return result;
    }

    /**
     * TODO: 
     *
     * @param element TODO
     *
     * @return TODO
     * @throws OBOFinderException 
     *
     * @throws OBOFinderException TODO
     */
    public OBOTerm determineTerm(String element) throws OBOFinderException  {
        OBOTerm term = null;
        try {
            // check if it is a label 
            term = determineTermByLabel(element);
        } catch (Exception e) {
            try {
                // or a synonym
                term = determineTermBySynonym(element);
            } catch (Exception e2) {
                // or at least an ID
                try {
                    term = determineTermByID(element);
                } catch (OBOFinderException e1) {
                }
            }
        }
        if (term == null){
            throw new OBOFinderException("unable to find element for : " + element);
        }
        return term;
    }

    private OBOTerm determineTermByID(String element) throws OBOFinderException {
        try {
            return (OBOTerm) this.obofilereader.oboHashtable.get(element);
        } catch (Exception e) {
            throw new OBOFinderException("unable to find Term for :\"" + element + "\"", e);
        }
    }


    private OBOTerm determineTermByLabel(String id) throws OBOFinderException {
        try {
            return (OBOTerm) this.obofilereader.oboHashtable.get(this.obofilereader.termIdHashtable.returnId(id));
        } catch (Exception e) {
            throw new OBOFinderException("unable to find Term for :\"" + id + "\"", e);
        }
    }

    private OBOTerm determineTermBySynonym(String synonym)
    throws OBOFinderException {
        Hashtable oboHash = this.obofilereader.oboHashtable;

        Enumeration elements = oboHash.elements();

        while (elements.hasMoreElements()) {
            OBOTerm term = (OBOTerm) elements.nextElement();
            List list = term.getSynonyms();

            //Logger.debug(this, "checking: " + term.getTerm());
            if (list.contains(synonym)) {
                return term;
            }
        }

        throw new OBOFinderException("unable to find Term for :\"" + synonym + "\"");
    }

    /**
     * internal test for reading relations of seed.
     *
     * @throws OBOFinderException unable to find the term
     */
    public void internalTest(String testString) {
        Logger.log(null, ">>>>>>>>>>>>>>> INTERNAL TEST FOR " + testString + ">>>>>>>>>>>>>>>>");


        try {
            OBOTerm term = this.determineTerm(testString);
            Logger.log(null, "--- original term ---");
            Logger.log(null, term.toString());

            for (int i = 0; i < getRelationTypes().length; i++){
                Logger.log(null, "--- relation: " + getRelationTypes()[i] + " ---");
                Iterator related_terms = this.determineRelatedTermsByRelation(term, getRelationTypes()[i]).iterator();
                while (related_terms.hasNext()) {
                    Logger.log(null, "--- related term ---");
                    term = (OBOTerm) related_terms.next();
                    Logger.log(null, term.toString());
                }
                related_terms = this.determineReverseRelatedTermsByRelation(term, getRelationTypes()).iterator();
                while (related_terms.hasNext()) {
                    Logger.log(null, "--- reverse related term ---");
                    term = (OBOTerm) related_terms.next();
                    Logger.log(null, term.toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Logger.log(null, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    }


    /**
     * @return a hashset filled with the related termsobjects
     */
    public HashSet determineReverseRelatedTermsByRelation(final OBOTerm term, final String[] relationNames){        
        if (term.reverseRelated != null){
            return term.reverseRelated;
        }
        HashSet relatedObjects = new HashSet();

        // get all terms
        Enumeration allTerms = this.getFileReader().getoboHashtable().elements();

        // loop over all termn
        int countElements = 0;
        while (allTerms.hasMoreElements()) {
            countElements ++;

            OBOTerm element = (OBOTerm) allTerms.nextElement();
            // Logger.debug(this, " checking: " + countElements + "/" + this.getFileReader().getoboHashtable().size() +  " " + element.getTerm() + " " + element.getId());

            // loop over relations
            for (int i = 0; i < relationNames.length; i++) {
                String relationName = relationNames[i];
                ArrayList list = element.getRelationship(relationName);
                Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    String termString = "" + iterator.next();
                    // check if any of these relations has a relation to this
                    if((termString.compareTo(term.getId()) == 0) ||
                            (termString.compareTo(term.getTerm()) == 0)){
                        // add it to the list of related objects
                        try {
                            // add it to the list
                            relatedObjects.add(element);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        term.reverseRelated = relatedObjects;
        return relatedObjects;
    }

    /**
     * create an instance of the oboconnector  - in case the file has been requested before the oboconnector will be reused.
     */
    public static OBOConnector newInstance(File sourceFile) throws InitException {
        OBOConnector connector = null;

        //checking if the pool contains an instance of this already
        if (OBOConnectorPool.contains(sourceFile)) {
            OBOConnectorPool.get(sourceFile);
        } else {
            connector = new OBOConnector(sourceFile);
            OBOConnectorPool.add(connector);
        }

        return connector;
    }


    public static String[] getRelationTypes(){
        return new String[]{"part_of", "is_a", "develops_from"};
    }

    public static void internalTest(){
        try {
            OBOConnector connector = OBOConnector.newInstance(new File ( Config.DIRECTORY_OBO + File.separator + "human-dev-anat-abstract.obo"));
            connector.internalTest("1040");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * find the element that can be used for starting to browse through the ontology
     * 
     * @return
     */
    public String findStartingElement(){
        Enumeration enumeration = this.obofilereader.oboHashtable.keys();
        return ((OBOTerm)this.obofilereader.oboHashtable.get("" + enumeration.nextElement())).getTerm(); 
    }
}
