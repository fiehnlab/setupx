package org.setupx.repository.core.communication.msi;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.ibm.xml.parser.Util;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.core.util.xml.XMLFile;
import org.setupx.repository.core.util.xml.XMLUtil;
import org.setupx.repository.server.db.SQLStatementPool;
import org.setupx.repository.server.db.SQLStatementPoolInit;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;

public class MSIQuery extends CoreObject {
    static Session s = null;
    static Object thiz = new MSIQuery();

    static{
        s = CoreObject.createSession();
    }

    public static AttributeValuePair[] searchAttributesPerSampleBySampleID(long sampleID){
        String query3 = 
            " select sample.question as question1, child.question  as question2, child.value, child.msiattribute as msi1, sample.msiattribute as msi2  " + 
            " from formobject as sample, formobject as child where sample.uoid =  " + sampleID + "  " + 
            " and sample.discriminator like \"%Sample\" and child.parent = sample.uoid";

        return query(query3);
    }

    public static AttributeValuePair[] searchAttributesPerClassBySampleID(long sampleID){
        String query2 = 
            " select mf_container.question  as question1, mf_label.question  as question2, mf_label.value, mf_label.msiattribute as msi1,  mf_container.msiattribute as msi2 from "
            + " formobject as sample, formobject as multiField4Clazzes, formobject as mf_label, formobject as clazz, formobject as mf_container, multiField4ClazzesHibernate as x "
            + " where clazz.uoid = x.multiField4Clazzs and x.elt = multiField4Clazzes.uoid and multiField4Clazzes.active = true and (mf_label.msiattribute is not null or mf_container.msiattribute is not null) "
            + " and mf_label.PARENT = multiField4Clazzes.uoid and sample.uoid = " + sampleID + " and clazz.uoid = sample.parent and clazz.discriminator like \"%Clazz\" and mf_container.uoid = mf_label.PARENT "
            + " and (   select count(*) from formobject where parent = multiField4Clazzes.parent ) > 1  and mf_label.value != \"\" order by clazz.uoid, mf_container.uoid, mf_label.question, mf_label.msiattribute ";
        
        return query(query2);
    }
    
    
    public static AttributeValuePair[] searchAttributesPerExperimentByPromtID(long promtID){
        // get the first sample
        long sampleID = Long.parseLong("" + new SXQuery().findSampleIDsByPromtID(promtID).get(0));
        return searchAttributesPerExperimentBySampleID(sampleID);
    }
    


    /**
     * 
     * 
     * 
     *     
     *     <h3>usage:</h3>
<br>Enumeration keys = cache.keys();
<br>while (keys.hasMoreElements()) {
<br>&nbsp;&nbsp;String key = keys.nextElement().toString();
<br>&nbsp;&nbsp;long msiLong = Long.parseLong(key);
<br>&nbsp;&nbsp;try {
<br>&nbsp;&nbsp;&nbsp;&nbsp;{@link MSIAttribute} _attribute = ({@link MSIAttribute})CoreObject.persistence_loadByID({@link MSIAttribute}.class, hibernateSession, msiLong);
<br>&nbsp;&nbsp;&nbsp;&nbsp;Vector values = (Vector)cache.get(key);
<br>&nbsp;&nbsp;&nbsp;&nbsp;<b>_attribute .getQuestion()</b>
<br>&nbsp;&nbsp;&nbsp;&nbsp;<b>_attribute.getMsiLabel() </b>
<br>&nbsp;&nbsp;&nbsp;&nbsp;<b>_attribute.getDescribtion() </b>
<br>&nbsp;&nbsp;&nbsp;&nbsp;for (int a = 0; a < values.size(); a++) 
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>values.get(a)</b> 
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>_attribute.getExtension()</b> 
<br>&nbsp;&nbsp;&nbsp;&nbsp}
<br>&nbsp;&nbsp;} catch (Exception e){
<br>&nbsp;&nbsp;}
<br>}
     * 
     * 
     * @param id
     * @return
     */
    public static Hashtable searchAttributesPerExperiment(long id){
        String query = "select value, msiattribute from formobject where msiattribute is not null and root(uoid) = " + id;

        Session s = CoreObject.createSession();
        List list = s.createSQLQuery(query)
        .addScalar("value", Hibernate.STRING)
        .addScalar("msiattribute", Hibernate.STRING)
        .list();


        Logger.debug(thiz, "result size: " +list.size());
        Hashtable cache = new Hashtable();
        Iterator iterator = list.iterator();
        while(iterator.hasNext()){
            Object[] objects = (Object[])iterator.next();  
            try {
                String value = objects[0].toString();
                if (value != null && value.compareTo("null") != 0 && value.length() > 0){
                    try {
                        Logger.debug(thiz, value + " cached? " + ((Vector)cache.get(objects[1].toString())).contains(value));
                    } catch (Exception e){
                        //Logger.debug(this, "creating new Vector for " + objects[1].toString());
                        cache.put(objects[1].toString(), new Vector());
                        ((Vector)cache.get(objects[1].toString())).add(value);
                        Logger.debug(thiz, "added value (2) " + value);
                    }

                    if (((Vector)cache.get(objects[1].toString())).contains(value)){

                    } else {
                        if (value.length() > 1){
                            long msiLong = Long.parseLong(objects[1].toString());
                            try {
                                ((Vector)cache.get(objects[1].toString())).add(value);
                                Logger.debug(thiz, "added value (1) \"" + value + "\"");
                            } catch (Exception e){
                                Logger.debug(thiz, "creating new Vector for " + objects[1].toString());
                                cache.put(objects[1].toString(), new Vector());
                                ((Vector)cache.get(objects[1].toString())).add(value);
                                Logger.debug(thiz, "added value (2) \"" + value + "\"");
                            }
                        }    
                    } // end else
                }// end if
            }catch (Exception e){

            }       
        }

        return cache;
        /*
    Enumeration keys = cache.keys();
    while (keys.hasMoreElements()) {
        String key = keys.nextElement().toString();
        long msiLong = Long.parseLong(key);
        try {
            MSIAttribute _attribute = (MSIAttribute)CoreObject.persistence_loadByID(MSIAttribute.class, hibernateSession, msiLong);
            Vector values = (Vector)cache.get(key);
            //if (values.size() != 0) throw new Exception("is too small");
            %>
            <tr>
                <td><%=_attribute .getQuestion() %>
                    <font style="font-size: xx-small;">
                        <br>
                        <b><%=_attribute.getMsiLabel() %></b>
                        <br>
                        <%=_attribute.getDescribtion() %>
                    </font>
                <td>
                    <%for (int a = 0; a < values.size(); a++){ %>
                        <nobr><%=values.get(a) %> <%=_attribute.getExtension() %> </nobr><br>
                    <%} %>
            <%
        } catch (Exception e){

        }
    }
         */
    }

    // NOT TESTED _ the result contains data for more than one class!!!
    public static AttributeValuePair[] searchAttributesPerClassByClassID(int classID){
        // get the first sample
        long sampleID = Long.parseLong("" + new SXQuery().findSampleIDsByClazzID(classID).get(0));
        return searchAttributesPerClassBySampleID(sampleID);
    }

    public static AttributeValuePair[] searchAttributesPerExperimentBySampleID(long sampleID){
        String query1 = 
            "select " + 
            "mf_container.question as question1, " + 
            "mf_label.question  as question2, " + 
            "mf_label.value, " + 
            "mf_container.uoid, " + 
            "clazz.uoid, " + 
            "mf_label.msiattribute as msi1, " +
            "mf_container.msiattribute as msi2 " +
            "from      " + 
            "formobject as sample, " + 
            "formobject as multiField4Clazzes,         " + 
            "formobject as mf_label,          " + 
            "formobject as clazz,          " + 
            "formobject as mf_container,          " + 
            "multiField4ClazzesHibernate as x       " + 
            "where clazz.uoid = x.multiField4Clazzs " + 
            "and x.elt = multiField4Clazzes.uoid          " + 
            "and multiField4Clazzes.active = true     " + 
            "and (mf_label.msiattribute is not null or mf_container.msiattribute is not null) " + 
            "and mf_label.PARENT = multiField4Clazzes.uoid          " + 
            "and sample.uoid =  " + sampleID + "   " + 
            "and clazz.uoid = sample.parent " + 
            "and clazz.discriminator like \"%Clazz\" " + 
            "and mf_container.uoid = mf_label.PARENT         " + 
            "and (   select count(*) " + 
            "           from formobject " + 
            "           where parent = multiField4Clazzes.parent " + 
            "   ) = 1        and mf_label.value != \"\" " + 
            "order by clazz.uoid, mf_container.uoid, mf_label.question";

        return query(query1);
    }




    /** 
     * internal Query method - using a query in a String 
     */
    private static AttributeValuePair[] query(String query) {
        AttributeValuePair[] returnResult = null;

        List result = s.createSQLQuery(query)
        .addScalar("question1", Hibernate.STRING)
        .addScalar("question2", Hibernate.STRING)
        .addScalar("value", Hibernate.STRING)
        .addScalar("msi1", Hibernate.LONG)
        .addScalar("msi2", Hibernate.LONG)
        .list();

        Logger.log(thiz, "Query: " + query);
        Logger.debug(thiz, "Number of results: " + result.size());

        returnResult = new AttributeValuePair[result.size()];

        Iterator resultIterator = result.iterator();
        int count = -1;
        while (resultIterator.hasNext()) {
            count++;
            Object[] objects = (Object[]) resultIterator.next();
            if (objects[2] != null
                    && objects[2].toString().compareTo("null") != 0
                    && objects[2].toString().compareTo("--other--") != 0
                    && objects[2].toString().compareTo("") != 0) {
                
                
                // create a new object and add it to the result
                Logger.debug(null, "creating new AttributeValuePair");
                AttributeValuePair attributeValuePair = new AttributeValuePair();
                attributeValuePair.setLabel1(objects[0].toString());
                attributeValuePair.setLabel2(objects[1].toString());
                attributeValuePair.setValue(objects[2].toString());
                long msiID = 0;
                try {
                    msiID = Long.parseLong(objects[3].toString());
                } catch (Exception e) {
                    try {
                        msiID = Long.parseLong(objects[4].toString());
                    } catch (Exception e2) {
                        // no msiid
                    }
                }
                attributeValuePair.setMsiID(msiID);
                // add it to the array
                Logger.debug(null, "finished new AttributeValuePair on pos : " + count);
                returnResult[count] = attributeValuePair;

            } else {
                warning(MSIQuery.class," problems with " + objects.toString() + ".");
            }
        }
        return returnResult;
    }




    /**
     * adds the attributes to an existing document at the specific node
     */
    public static final void toXML(AttributeValuePair[] attributeValuePairs, Document document, Node nodeToAttatchTo){
        // loop over the attributes and attatch an Node on
        Logger.debug(null, "attaching " + attributeValuePairs.length + " attributes to " + nodeToAttatchTo.toString());
        
        for (int i = 0; i < attributeValuePairs.length; i++) {
            AttributeValuePair attributeValuePair = attributeValuePairs[i];

            try {
                Logger.debug(null, "attaching " + attributeValuePair);
                nodeToAttatchTo.appendChild(attributeValuePair.toXML(document));
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    
    public static void testIt() throws ParserConfigurationException, TransformerConfigurationException, FileNotFoundException, TransformerFactoryConfigurationError, TransformerException, IOException{
        long[] ids = new long[]{111566, 111586, 111587, 111667};


        for(int i = 0; i <  ids.length; i++){
            // loading each and then show the xml doc
            Document document = XMLUtil.createNewDocument();
            document.appendChild(document.createElement("root"));
            Element root = document.getDocumentElement();

            AttributeValuePair[] attributeValuePairs = MSIQuery.searchAttributesPerSampleBySampleID(ids[i]);
            
            toXML(attributeValuePairs, document, root);
            Logger.log(null, new XMLFile(document).content());
        }
    }
    
    

    /**
     * filters a certain attribute out of the array
     * @param searchAttributesPerExperimentByPromtID
     * @param msiAttributeID
     * @return the filtered array
     */
    public static AttributeValuePair[] filter(
            AttributeValuePair[] searchAttributesPerExperimentByPromtID,
            long msiAttributeID) {
        
        Vector attributeVector = new Vector();
        
        for (int i = 0; i < searchAttributesPerExperimentByPromtID.length; i++) {
            AttributeValuePair attributeValuePair = searchAttributesPerExperimentByPromtID[i];
            try {
                if (attributeValuePair.getMsiID() == msiAttributeID){
                    attributeVector.add(attributeValuePair);
                }
            } catch (Exception e) {
                // MSI not available
            }
        }
        return AttributeValuePair.toArray(attributeVector);
    }
}
