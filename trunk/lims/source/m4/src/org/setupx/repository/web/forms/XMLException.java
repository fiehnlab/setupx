package org.setupx.repository.web.forms;


public class XMLException extends Exception {

    public XMLException(String string) {
        super(string);
    }
}
