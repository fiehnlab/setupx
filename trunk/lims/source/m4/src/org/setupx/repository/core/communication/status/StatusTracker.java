package org.setupx.repository.core.communication.status;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.importing.logfile.ScannedPair;
import org.setupx.repository.core.user.UserFactoryException;
import org.setupx.repository.core.user.UserNotFoundException;
import org.setupx.repository.core.user.WrongPasswordException;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;


public class StatusTracker extends CoreObject{
    
    
    static {
        //starting a background threat that auto caches the status of the lab
       
        StatusTrackerCache.init();
        
    }
    

    private static final int SHIPPED = 1;
    private static final int ARRIVED = 2;
    private static final int PREPARED = 3;
    private static final int SCHEDULED = 4;
    private static final int RUN_FINISHED = 5;
    private static final int POSTPROCESSING = 6;
    private static final int RESULTS_RECEIVED = 7;

    /**
     * Find all status for a certain sample. The list contains persistent statuses as well as dynamic generated statuses.
     * 
     * @param sampleUOID the sample uoid
     * @return list of all samples related to that sample.
     * @throws NoStatusAvailableException
     */
    public final static List getStatus(final long sampleUOID) throws NoStatusAvailableException{
        List list = new Vector();
        
        list.addAll(getPersistentStatus(sampleUOID));
        list.addAll(getNonPersistentStatus(sampleUOID));

        debug(StatusTracker.class,"number of Statuses found : " + list.size());

        sortByDate(list);
        
        return list;
    }

    
    private static void sortByDate(List list) {
        Collections.sort(list, new Comparator(){
            public int compare(Object o1, Object o2) {
                //Logger.debug(null, "compare " + o1 + "   " + o2);
                
                Timestamped timestamped1 ;
                Timestamped timestamped2 ;
                
                if (o1 instanceof Timestamped && o2 instanceof Timestamped){
                    timestamped1 = (Timestamped)o1;
                    timestamped2 = (Timestamped)o2;
                } else {
                    Logger.debug(this, "unable to compare cause not both timestamped");
                    if (o1 instanceof Timestamped) return +1;
                    else return -1;
                }

                Logger.debug(null, "compare " + timestamped1.getDate().getTime() + "   " + timestamped2.getDate().getTime());

                // identical
                if (timestamped1.getDate().getTime() == timestamped2.getDate().getTime()){
                    return 0;
                }
                
                if (timestamped1.getDate().getTime() > timestamped2.getDate().getTime()){
                    return -1;
                } else {
                    return +1;
                }
            }
        });
    }


    /**
     * @param sampleUOID
     * @return list of non persistent statuses - these are generated on the fly.
     */
    private static Collection getNonPersistentStatus(long sampleUOID) {
        Vector list = new Vector();
        Session hqlSession = DynamicStatus.createSession();
        list.addAll(SampleRunStatus.find(sampleUOID, hqlSession));
        list.addAll(SampleScheduledStatus.find(sampleUOID, hqlSession));
        // very slow - this needs to be accelerated.
        list.addAll(FilecreatedStatus.find(sampleUOID, hqlSession));
        list.addAll(EventStatus.find(sampleUOID, hqlSession));
        return list;
    }

    /**
     * 
     * @param sampleUOID
     * @return list of all persistent statuses
     * @throws NoStatusAvailableException
     */
    private static Collection getPersistentStatus(long sampleUOID) throws NoStatusAvailableException {
        // check for different statuses and add them then.
        Session hqlSession = PersistenceConfiguration.createSessionFactory().openSession();

        try {
            return hqlSession.createCriteria(PersistentStatus.class).add(Expression.eq("sampleID", new Long(sampleUOID))).addOrder( Order.desc("date") ).list();
        } catch (Exception e) {
            throw new NoStatusAvailableException("unable to search for status.", e);
        }
    }

    
    /**
     * gets the latest status of all statuses related to the sample. <b>TODO:</b> add sorting and filer.
     * @param sampleUOID
     * @return
     * @throws NoStatusAvailableException
     */
    public final static Status getLatestStatus(final long sampleUOID) throws NoStatusAvailableException{
        // load all persistent samples
        Collection list = getPersistentStatus(sampleUOID);

        Session hqlSession = CoreObject.createSession();
        
        // if the highest one is resultreceived, postprocessing then return it -
        try {
            try {
                Status firstElement = (Status)list.iterator().next();
                if (firstElement instanceof ResultReceivedStatus || firstElement instanceof PostprocessEventStatus){
                    return firstElement;
                } 
            }catch (Exception e) {
                // e.printStackTrace();
            }
        
            // otherwise start quering non persistent ones.
            // check if it ran
            list = SampleRunStatus.find(sampleUOID, hqlSession);
            if (list.size() > 0) {
                return (Status)list.iterator().next();
            }
        } catch (Exception exception){
            exception.printStackTrace();
            throw new NoStatusAvailableException("no status for " + sampleUOID, new Exception() );
        }
        throw new NoStatusAvailableException("no status for " + sampleUOID, new Exception() );
/*
        
        try {
            return hqlSession.createCriteria(PersistentStatus.class).add(Expression.eq("sampleID", new Long(sampleUOID))).list();
        } catch (Exception e) {
            throw new NoStatusAvailableException("unable to search for status.", e);
        }
        
        return (Status)getStatus(sampleUOID).get(0);
        
        // filter out the most recent one.
         */
    }

        
    
    
    
    // test ID
    public static long testSampleID = 205829;
    private static Iterator iterator;
    private static Vector newList;
    private static Hashtable cache = new Hashtable();
    
    public static void testIt() throws UserNotFoundException, WrongPasswordException, UserFactoryException, StatusPersistenceException, PersistenceActionUpdateException, NoStatusAvailableException{
        
        // 
        StatusCurator.createMissingStatuses();        
        
        Logger.log(null, "number of shipped: " + findShipped().size());
        Logger.log(null, "number of stored: " + findStored().size());
        Logger.log(null, "number of prepared: " + findPrepared().size());
        Logger.log(null, "number of scheduled: " + findScheduled().size());
        Logger.log(null, "number of run: " + findRun().size());
        Logger.log(null, "number of postprocessing: " + findPostProcessing().size());
        

        List sampleList = new SXQuery().findSampleIDsByPromtID(101184);
        Logger.log(null, "number of samples to get status for: " + sampleList.size());
        Iterator sampleIDs = sampleList.iterator();
        while (sampleIDs.hasNext()) {
            long id = (Long.parseLong(sampleIDs.next()  + ""));
            try {
                Logger.log(null, "status: " + getLatestStatus(id));    
            }catch (Exception e) {
                e.printStackTrace();
            }
            
        }
        
        /*
        
        // create my user
        UserDO userDO = UserConnector.findUser("sx","sx");

        // create some statuses
        new SampleShippedStatus(testSampleID, userDO, "shipped by FedEx").update(true);
        new SampleArrivedStatus(testSampleID, userDO, "Received from Costa Cordalis.", "freezer 132a").update(true);
        
        // TODO
        new LabStatus(testSampleID, userDO, "sample derivatized step3.").update(true);
        new LabStatus(testSampleID, userDO, "sample derivatized step1.").update(true);
        new LabStatus(testSampleID, userDO, "sample derivatized step2.").update(true);
        new LabStatus(testSampleID, userDO, "sample is broken.").update(true);

        
        new ExportedEventStatus(testSampleID, userDO, "").update(true);
        new ImportedEventStatus(testSampleID, userDO, "").update(true);
        new ResultReceivedStatus(testSampleID, userDO, "").update(true);
        

        try {
            Iterator iterator = StatusTracker.getStatus(318686).iterator();
            while (iterator.hasNext()) {
                Status element = (Status) iterator.next();
                Logger.log(null, element.toString());
                
                
            }
            
        } catch (NoStatusAvailableException e1) {
            e1.printStackTrace();
        }
        
        
        List 
        samples = StatusTracker.findShipped();
        samples = StatusTracker.findStored();
        samples = StatusTracker.findPrepared();
        samples = StatusTracker.findScheduled();
        samples = StatusTracker.findRun();
        samples = StatusTracker.findPostProcessing();

        
        

        if (true) return;
        
        
        // search for all statuses
        List statusList = new Vector();
        try {
            statusList = StatusTracker.getStatus(testSampleID);
        } catch (NoStatusAvailableException e) {
            e.printStackTrace();
        }
        
        Iterator statusIter = statusList.iterator();
        Logger.log(null, "number of messages: " + statusList.size());
        while (statusIter.hasNext()) {
            Status status = (Status) statusIter.next();
            Logger.log(status, status.toString());
        }
        */
    }


    public static List findPostProcessing() {
        Session session = createSession();
        List result = find(session , POSTPROCESSING);
        result.removeAll(find(session, ARRIVED ));
        result.removeAll(find(session, PREPARED ));
        result.removeAll(find(session, SCHEDULED ));
        result.removeAll(find(session, RUN_FINISHED ));
        result.removeAll(find(session, RESULTS_RECEIVED ));
        session.close();
        return result;
    }
    
    
    public static List findRun() {
        Session session = createSession();
        
        List result = null;

        result = find(session, RUN_FINISHED);
        result.removeAll(find(session, ARRIVED ));
        result.removeAll(find(session, PREPARED ));
        result.removeAll(find(session, SCHEDULED ));
        result.removeAll(find(session, POSTPROCESSING ));
        result.removeAll(find(session, RESULTS_RECEIVED ));

        
        session.close();
       
        return result;
    }


    public static List findScheduled() {
        Session session = createSession();
        
        List result = null;

        result = find(session, SCHEDULED);
        result.removeAll(find(session, ARRIVED ));
        result.removeAll(find(session, PREPARED ));
        result.removeAll(find(session, RUN_FINISHED ));
        result.removeAll(find(session, POSTPROCESSING ));
        result.removeAll(find(session, RESULTS_RECEIVED ));

        
        session.close();
       
        return result;
    }


    public static List findPrepared() {
        Session session = createSession();
        
        List result = null;

        result = find(session, PREPARED);
        result.removeAll(find(session, ARRIVED ));
        result.removeAll(find(session, SCHEDULED ));
        result.removeAll(find(session, RUN_FINISHED ));
        result.removeAll(find(session, POSTPROCESSING ));
        result.removeAll(find(session, RESULTS_RECEIVED ));

        
        session.close();
       
        return result;
    }


    public static List findStored() {
        
        Session session = createSession();
        
        List result = null;

        result = find(session, ARRIVED);
        result.removeAll(find(session, PREPARED ));
        result.removeAll(find(session, SCHEDULED ));
        result.removeAll(find(session, RUN_FINISHED ));
        result.removeAll(find(session, POSTPROCESSING ));
        result.removeAll(find(session, RESULTS_RECEIVED ));

        
        session.close();
       
        return result;
    }




    /**
     * shipped & NOT arrived
     * @return
     */
    public static List findShipped() {
        
        Session session = createSession();
        
        List result = null;

        result = find(session, SHIPPED);
        result.removeAll(find(session, ARRIVED ));
        result.removeAll(find(session, PREPARED ));
        result.removeAll(find(session, SCHEDULED ));
        result.removeAll(find(session, RUN_FINISHED ));
        result.removeAll(find(session, POSTPROCESSING ));
        result.removeAll(find(session, RESULTS_RECEIVED ));
        
        session.close();
       
        return result;
    }
    

    /**
     * find  all relevant sampleIDs for a specific type of Status.
     * <p>the list does contains each sample ID only <b>once</b> even if there are more instances of that specifid 
     * type existing.
     * <p><b>Example:</b> <code>find(Session hqlSession, {@link PostprocessEventStatus}.class){</code>...
     * <br> will only return the sampleID once
     * even if it has been there is <code>{@link ResultReceivedStatus}</code> and a <code>{@link ExportedEventStatus}</code>.
     * @param hqlSession a valid usable session
     * @param clazz the type of status you are looking for
     * @return a list containing the sampleIDs
     */
    public static List find(Session hqlSession, Class clazz){
        String basic = 
            "select c.sampleID " +
            " from " + clazz.getName() + " as c " +
            " group by c.sampleID ";

        Query query = hqlSession.createQuery(basic);
        return query.list();
    }
    
    
    /**
     * unfiltered finder - 
     * @param hqlSession
     * @param type
     */
    private static List find(Session session, int type){
        List result = new Vector();

        if (cache.containsKey("" + type)){
            CachedStatusCollection statColl = (CachedStatusCollection)cache.get("" + type);
            
            // check caches age - max is 20 sec
            if(statColl.getAge() < Config.STATUS_REFRESH){
                return statColl.getList();
            }
        }
        
        switch (type) {
        case SHIPPED:
            result = find(session, SampleShippedStatus.class);            
            break;

        case ARRIVED:
            result = find(session, SampleArrivedStatus.class);            
            break;

        case PREPARED:
            result = find(session, LabStatus.class);            
            break;

        case SCHEDULED:
            Logger.warning(null, "TODO:          case SCHEDULED:");
            break;
            
        case RUN_FINISHED:
            String basic = "select pair.sampleID from " + ScannedPair.class.getName() + " as pair group by pair.sampleID order by pair.sampleID desc ";
            Query query = session.createQuery(basic);
            // result comes as string - needs to be a long
            result = toBigInt(query.list());
            break;
            
        case POSTPROCESSING:
            result = find(session, PostprocessEventStatus.class);
            break;

        case RESULTS_RECEIVED:
            result = find(session, ResultReceivedStatus.class);
            break;
            
        default:
            break;
        }
        
        
        
        int max = 10;
        int i= 0;
        for (Iterator iter = result.iterator(); iter.hasNext() && i < max;) {
            //Logger.debug(StatusTracker.class, "class: " + iter.next().getClass());
            i++;
        }
        

        //Logger.log(StatusTracker.class, "number of results for: " + type + " :  " + result.size());
        cache.put("" + type, new CachedStatusCollection(result));
        return result;
    }


    /**
     * converts a list of X.toString() to ints
     * @param list
     * @return
     */
    private static List toBigInt(List list) {
        iterator = list.iterator();
        newList = new Vector();
        while (iterator.hasNext()) {
            try {
                newList.add(new Long(iterator.next().toString()));
            } catch (java.lang.NumberFormatException e) {
                //e.printStackTrace();
            }
        }
        return newList;
    }
}