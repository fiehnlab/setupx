/**
 * ============================================================================ File:    VocabularyCheckerProcess.java Package: org.setupx.repository.server.workflow.elements cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.server.workflow.elements;

import org.setupx.repository.core.InitException;
import org.setupx.repository.core.communication.vocabulary.VocabularyCheck;
import org.setupx.repository.core.communication.vocabulary.VocabularyCheckException;
import org.setupx.repository.server.workflow.MemoryObject;
import org.setupx.repository.server.workflow.MemoryObjectNotFoundException;
import org.setupx.repository.server.workflow.WorkFlowException;


/**
 * process checking a single word - checks if it is (might be) a correct word - based on libary-checks, or other methods.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class VocabularyCheckerProcess extends Process {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private VocabularyCheck googleChecker = null; // TODO new GoogleCheck();

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final String VOCABULARY_WORD = "vocabularycheck_word";
  private static final String RESULT = MailProcess.MAILADDRESS; //"vocabularycheck_result";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new VocabularyCheckerProcess object.
   *
   * @throws InitException 
   * @throws WorkFlowException 
   */
  public VocabularyCheckerProcess() throws InitException, WorkFlowException {
    super(new ProcessConfiguration("vocabularychecker", 22));
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.server.workflow.WFElement#action()
   */
  public void action() throws ProcessingException {
    MemoryObject memoryObject = null;

    try {
      memoryObject = this.getWorkflow().workflowMemory.pull(VOCABULARY_WORD);
    } catch (MemoryObjectNotFoundException e) {
      //throw new ProcessingException(e);
    }

    String word = "Braunschweig";

    //String word = memoryObject.objectAsString();
    boolean valid;

    try {
      valid = this.googleChecker.check(word);
    } catch (VocabularyCheckException e1) {
      throw new ProcessingException(e1);
    }

    this.getWorkflow().workflowMemory.push(new MemoryObject(VocabularyCheckerProcess.RESULT, new Boolean(valid)));
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.WFElement#getDataInKeys()
   */
  protected String[] getDataInKeys() {
    return none();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.WFElement#getDataOutKeys()
   */
  protected String[] getDataOutKeys() {
    return new String[] { RESULT };
  }
}
