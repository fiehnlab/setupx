/**
 * ============================================================================ File:    TimelineInputfield.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.web.forms.ArrayExpandException;
import org.setupx.repository.web.forms.clazzes.Treatment;
import org.setupx.repository.web.forms.inputfield.Removable;


/**
 * @hibernate.subclass
 */
public class TimelineInputfield extends MultiField implements Treatment {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new TimelineInputfield object.
   *
   * @param question 
   * @param description 
   */
  public TimelineInputfield(String question, String description) {
    super(question, description);

    Removable removable = new TimelineDateTimeInputfield("Sample Taken", "define when the sample was taken");
    MultiFieldExpanding fieldExpanding = new MultiFieldExpanding("Dates", "define every point in time when samples where taken", "", removable);

    try {
      fieldExpanding.expand(3);
    } catch (ArrayExpandException e) {
      e.printStackTrace();
    }

    this.addField(fieldExpanding);
  }

  /**
   * Creates a new TimelineInputfield object.
   */
  public TimelineInputfield() {
    super();
  }
}
