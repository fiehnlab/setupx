package org.setupx.repository.web.forms;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.Util;

public class ComparisonsException extends Exception {

    public ComparisonsException(CoreObject formObject1, CoreObject formObject2, String string) {
        super("ERR:" + formObject1.uoid + "  " + Util.getClassName(formObject1) + " vs. " + formObject2.uoid + "  " + Util.getClassName(formObject2) + " cause:" + string);
    }
    
    public ComparisonsException(String msg) {
        super(msg);
    }
}
