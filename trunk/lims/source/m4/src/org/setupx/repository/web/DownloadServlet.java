/**
 * ============================================================================ File:    DownloadServlet.java Package: org.setupx.repository.web cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web;

import org.setupx.repository.Config;
import org.setupx.repository.ConfigurationException;
import org.setupx.repository.core.util.web.UiHttpServlet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * download a file from the directory on the server
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class DownloadServlet extends UiHttpServlet {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String CONTENT_TYPE_ZIP = "application/x-zip-compressed";
  public static final String CONTENT_TYPE_XLS = "application/vnd.ms-excel";
  public static final String CONTENT_TYPE_DOC = "application/vnd.ms-word";
  public static final String CONTENT_TYPE_TXT = "text/plain";
  public static final String CONTENT_TYPE_PDF = "application/pdf";
  private static final String URL = "download";

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param file
   *
   * @return
   */
  public static final String createDownloadLink(String fileName, String promtID) {
    return (DownloadServlet.URL + "?" + WebConstants.PARAM_FILENAME + "=" + fileName + "&" + WebConstants.PARAM_PROMTID + "=" + promtID);
  }

  /**
   * TODO: 
   *
   * @param file TODO
   * @param out TODO
   *
   * @throws FileNotFoundException TODO
   * @throws IOException TODO
   */
  public static void returnFile(File file, OutputStream out)
    throws FileNotFoundException, IOException {
    InputStream in = null;

    try {
      in = new BufferedInputStream(new FileInputStream(file));

      byte[] buf = new byte[4 * 1024]; // 4K buffer
      int bytesRead;

      while ((bytesRead = in.read(buf)) != -1) {
        out.write(buf, 0, bytesRead);
      }
    } finally {
      if (in != null) {
        in.close();
      }
    }
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.util.web.UiHttpServlet#process(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  public void process(HttpServletRequest request, HttpServletResponse response)
    throws WebException {
    // get filename from session
    String filename = getRequestParameter(request, WebConstants.PARAM_FILENAME);

    // check if access allowed
    String id = getRequestParameter(request, WebConstants.PARAM_PROMTID);

    //((Promt) request.getSession().getAttribute(WebConstants.SESS_FORM_PROMT)).getUOID();
    filename = Config.DIRECTORY_RELATED + File.separator + id + File.separator + filename; 

    try {
      org.setupx.repository.core.util.Util.checkFile(filename);
    } catch (ConfigurationException e) {
      throw new WebException("file is not available");
    }

    File file = new File(filename);

    // determine content type 
    String contentType = org.setupx.repository.core.util.File.determineContentType(file);

    // Set the headers.
    response.setContentType(contentType);
    response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());

    // Send the file.
    OutputStream out;

    try {
      out = response.getOutputStream();
      returnFile(file, out);
    } catch (IOException e1) {
      throw new WebException("unable to open file", e1);
    }
  }
}
