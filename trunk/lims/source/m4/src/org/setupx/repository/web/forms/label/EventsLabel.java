/**
 * ============================================================================ File:    EventsLabel.java Package: org.setupx.repository.web.forms.label cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.label;

import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.event.Event;
import org.setupx.repository.web.forms.inputfield.multi.Sample;

import java.util.Iterator;


/**
 * @hibernate.subclass
 */
public class EventsLabel extends DynamicLabel {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new EventsLabel object.
   */
  public EventsLabel() {
    super("Events", "number of events");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.label.DynamicLabel#getDynamicValue()
   */
  public Object getDynamicValue() {
    FormObject formObject = this.getParent();

    if (!(formObject instanceof Sample)) {
      warning(this, "THE EVENT LABEL CAN ONLY BE ADDED TO A SAMPLE");

      return UNDEFINED_VALUE;
    } else {
      // create an image - with rollover that contains the text
      Iterator events = ((Sample) formObject).getEvents().iterator();

      String msg = "";

      while (events.hasNext()) {
        Event event = (Event) events.next();
        msg = msg + " " + event.toString() + " \n";
      }

      return "<a href=\".\" title=\"" + msg + "\">" + IMG_INFO + "</a> [" + ((Sample) formObject).getEvents().size() + "]";
      //return "<a href=\"events.jsp\" title=\"" + msg + "\">" + IMG_INFO + "</a> [" + ((Sample) formObject).getEvents().size() + "]";
    }
  }
}
