/**
 * ============================================================================ File:    AbtractLibrary.java Package: org.setupx.repository.core.communication.vocabulary cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.vocabulary;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.ServiceNotAvailableException;
import org.setupx.repository.core.util.Util;

import java.io.Serializable;

import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;


/**
 * library containg all methods for the library
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.12 $
 */
public abstract class AbtractLibrary extends CoreObject implements Library, Serializable {
    //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private long startTime;
    private long stopTime;

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * compares a word against a whole Hashset and returns the mist simialar word.
     *
     * @param word the word to compare
     * @param set the hashset containing all words that will be compared with the single word
     *
     * @return the most similar word from that hashset - if the none of the word is similar in any kind - null will be returned
     */
    public static final String compare(final String word, final HashSet set) {
        Iterator enumeration_x1= set.iterator();

        String returnValue = null;
        int returnValueSimilarity = 10;
        String value;
        int similarity;

        for (int i = 0; enumeration_x1.hasNext(); i++) {
            value = (String) enumeration_x1.next();

            similarity = Util.getSimilarity(value.toLowerCase(), word.toLowerCase());

            // similarity is ok
            if (similarity < returnValueSimilarity) {
                debug(null, "similarity: " + similarity + " " + value);
                returnValueSimilarity = similarity;
                returnValue = value;
            }
        }

        return returnValue;
    }

    /* (non-Javadoc)
     * @see org.setupx.repository.core.Connectable#checkAvailability()
     */
    public final void checkAvailability() throws ServiceNotAvailableException {
        try {
            HashSet hashSet = this.receiveValues();

            if (hashSet.size() < 1) {
                throw new ServiceNotAvailableException(this, "no values in the Lib");
            }
        } catch (ServiceNotAvailableException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceNotAvailableException(this, e);
        }
    }

    /* (non-Javadoc)
     * @see org.setupx.repository.core.communication.vocabulary.Library#contains(java.lang.String)
     */
    public final boolean contains(final String word) {
        start();
        log(this, " looking for " + word + " in a set of " + receiveValues().size() + " words ");

        boolean exists = this.receiveValues().contains(word);
        stop();

        return this.receiveValues().contains(word);
    }

    /*
     *  (non-Javadoc)
     * @see org.setupx.repository.core.communication.vocabulary.Library#createSuggestion(java.lang.String)
     */
    public final String createSuggestion(final String word) {
        start();

        String value = compare(word, this.receiveValues());
        stop();

        return value;
    }

    /**
     * <i>do not rename it to getter cause getters are used by the casotr persistence layer</i>
     *
     * @return a hashset of values inside the library
     */
    protected abstract HashSet receiveValues();

    /**
     * sets the start time
     */
    private void start() {
        this.startTime = GregorianCalendar.getInstance().getTimeInMillis();
    }

    /**
     * sets the stoptime and gives a log message containing the information how long it took since calling the start method
     *
     * @return the time since calling the start method the last time in <b>MILLISECONDS</b>.
     *
     * @see AbtractLibrary#start()
     */
    private long stop() {
        this.stopTime = GregorianCalendar.getInstance().getTimeInMillis();

        long duration = this.stopTime - this.startTime;
        debug(this, "operation took " + duration + " mSec");

        return duration;
    }
}
