package org.setupx.repository.server.workflow;

import junit.framework.TestCase;
import org.setupx.repository.core.InitException;
import org.setupx.repository.server.workflow.elements.ProcessingException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.1 $
 */
public class WorkFlowManagerTest extends TestCase {
  private WorkFlowManager flowManager;

  /**
   * Creates a new WorkFlowManagerTest object.
   *
   * @param arg0 
   */
  public WorkFlowManagerTest(String arg0) {
    super(arg0);
  }

  /**
   * TODO: 
   *
   * @param args TODO
   */
  public static void main(String[] args) {
  }

  /**
   * TODO: 
   *
   * @throws Exception TODO
   */
  protected void setUp() throws Exception {
    super.setUp();
    this.flowManager = WorkFlowManagerFactory.getInstance();
  }

  /**
   * TODO: 
   *
   * @throws Exception TODO
   */
  protected void tearDown() throws Exception {
    super.tearDown();
  }

  public void testStartAllProcesses() throws WorkFlowException, ProcessingException, InitException {
        Workflow[] workflows = flowManager.getWorkflows();

        for (int i = 0; i < workflows.length; i++) {
          Workflow workflow = workflows[i];
          workflow.start();
        }
  }

  /**
   * public void testProcessValidation() { WorkflowElementCollection elementCollection =
   * flowManager .availableProcesses(); Enumeration enumeration = elementCollection.keys(); while
   * (enumeration.hasMoreElements()) { String id = (String) enumeration.nextElement();
   * assertTrue("a valid process is not found ", flowManager
   * .isValidProcess(elementCollection.get(id))); }}
   */
  /**
   * TODO: 
 * @throws InitException
   */
  public void testInvalidProcess() throws InitException {
      Workflow[] workflows = flowManager.getWorkflows();
      
      for (int i = 0; i < workflows.length; i++) {
        Workflow workflow = workflows[i];
        assertFalse("an invalid process is declared as valid ", workflow.isValidProcess(null));
    }      
  }

  public void testInit() throws InitException, WorkFlowException {
    WorkFlowManager flowManager = WorkFlowManagerFactory.getInstance();
  }

  public void testStarting() throws InitException, WorkFlowException, ProcessingException {
        WorkFlowManager flowManager = WorkFlowManagerFactory.getInstance();

        Workflow[] workflows  = flowManager.getWorkflows();
        
        assertTrue("no workflows registrated " , workflows.length != 0);
        
        for (int i = 0; i < workflows.length; i++) {
            Workflow workflow = workflows[i];

            workflow.start();
            
            WFElement[] elements =  workflow.registratedProcessables;

            assertTrue("no workflowelements registrated " + elements.length ,  elements.length != 0);
            
            
            for (int j = 0; j <  workflow.getWorkflowElementIDs().length; j++) {
                //Logger.log(this, "workflowelements: " + workflow.getWorkflowElementIDs().length);
                //Logger.log(this, "counter         : " + j);
                
                int id = workflow.getWorkflowElementIDs()[j];

                //Logger.log(this, "id              : " + id);
                
                WFElement element = elements[id];
                
                assertNotNull("the element is null " + j + "/" + workflows.length  , element);
                assertNotNull("the config is null ", element.getConfiguration());
                assertTrue("the config_id is null ", element.getConfiguration().getId() != 0);
                assertNotNull("the config_id is null ", element.getConfiguration().getDescription());
                assertTrue("number of found elements is wrong", elements.length > 1);

                //Logger.log(this, element.getConfiguration().toString());
            }
            
            workflow.showRegistratedElements();
            
        }
    }







}
