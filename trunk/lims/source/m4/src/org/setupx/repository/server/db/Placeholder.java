package org.setupx.repository.server.db;

public class Placeholder {

    private String variableName = "";
    private String placeholderToken = "";
 
    /**
     * @param variableName
     * @param placeholderToken
     */
    public Placeholder(String variableName, String placeholderToken) {
        super();
        this.variableName = variableName;
        this.placeholderToken = placeholderToken;
        //System.out.println("created new Placeholder: " + variableName + " " + placeholderToken);
    }

    public String getVariableName() {
        return variableName;
    }

    public String getPlaceholderToken() {
        return placeholderToken;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public void setPlaceholderToken(String placeholderToken) {
        this.placeholderToken = placeholderToken;
    }
    
}
