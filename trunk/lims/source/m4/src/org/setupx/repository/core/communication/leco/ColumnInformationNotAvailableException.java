package org.setupx.repository.core.communication.leco;

public class ColumnInformationNotAvailableException extends Exception {

    public ColumnInformationNotAvailableException(String string) {
        super(string);
    }

    public ColumnInformationNotAvailableException(String string, Exception e) {
        super(string, e);
    }
}
