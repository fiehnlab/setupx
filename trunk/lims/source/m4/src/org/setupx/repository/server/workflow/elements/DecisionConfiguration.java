package org.setupx.repository.server.workflow.elements;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class DecisionConfiguration extends DefaultConfiguration {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new DecisionConfiguration object.
   *
   * @param label 
   * @param id 
   */
  public DecisionConfiguration(String label, int id) {
    super(label, id, "NA");
  }

  /**
   * Creates a new DecisionConfiguration object.
   *
   * @param label 
   * @param id 
   * @param description 
   */
  public DecisionConfiguration(String label, int id, String description) {
    super(label, id, description);
  }
}
