package org.setupx.repository.server.workflow.elements;

import org.setupx.repository.core.InitException;
import org.setupx.repository.server.workflow.MemoryObjectNotFoundException;
import org.setupx.repository.server.workflow.WorkFlowException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class MailProcess extends Process {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String MAILADDRESS = "mailaddress";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new MailProcess object.
   *
   * @throws InitException 
   * @throws WorkFlowException 
   */
  public MailProcess() throws InitException, WorkFlowException {
    super(new ProcessConfiguration("Mail sending Process", 3));
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @throws ProcessingException TODO
   */
  public void action() throws ProcessingException {
    try {
      this.getWorkflow().workflowMemory.pull(MailProcess.MAILADDRESS);
    } catch (MemoryObjectNotFoundException e) {
      throw new ProcessingException(e);
    }
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.WFElement#getDataInKeys()
   */
  protected String[] getDataInKeys() {
    return new String[] { MAILADDRESS };
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.WFElement#getDataOutKeys()
   */
  protected String[] getDataOutKeys() {
    return new String[0];
  }
}
