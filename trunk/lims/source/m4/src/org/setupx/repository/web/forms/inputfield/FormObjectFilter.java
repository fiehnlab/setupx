/**
 * ============================================================================ File:    FormObjectFilter.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.web.forms.FormObject;

import java.util.HashSet;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class FormObjectFilter extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private int type;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final int FILTERTYPE_ACTIVE = 10;
  private static final int FILTERTYPE_VALID = 20;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new FormObjectFilter object.
   *
   * @param type 
   */
  public FormObjectFilter(int type) {
    this.type = type;
  }

  private FormObjectFilter() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param formObjects TODO
   *
   * @return TODO
   */
  public FormObject[] filter(FormObject[] formObjects) {
    HashSet set = new HashSet();

    for (int i = 0; i < formObjects.length; i++) {
      FormObject object = formObjects[i];

      if (check(object)) {
        // leave it in here
        set.add(object);
      } else {
        // kick it out
        // bye
      }
    }

    formObjects = FormObject.mapIgnoreInternalPos(set);

    return formObjects;
  }

  /**
   * TODO: 
   *
   * @param object TODO
   *
   * @return TODO
   */
  private boolean check(FormObject object) {
    switch (type) {
    case FILTERTYPE_ACTIVE:
      return object.isActive();

    case FILTERTYPE_VALID:
      return object.isValid();

    default:
      break;
    }

    return false;
  }
}
