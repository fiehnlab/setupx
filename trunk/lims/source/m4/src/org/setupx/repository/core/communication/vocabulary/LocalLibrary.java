/**
 * ============================================================================ File:    LocalLibrary.java Package: org.setupx.repository.core.communication.vocabulary cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.vocabulary;

import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.web.forms.Persistence;
import org.setupx.repository.web.forms.PersistenceException;

import java.util.HashSet;
import java.util.Vector;


/**
 * Library containing words. The Library is growing with the number of words added to it.
 * 
 * <p>
 * The library exists just once in the whole system. It is initalized by startup.
 * </p>
 * 
 * <p>
 * Everytime a new word is added by add(String) the library is stored, so that no words will be lost when the system crashes.
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.12 $
 */
public class LocalLibrary extends AbtractLibrary {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static Object thiz = new LocalLibrary();
  private static Vector values = new Vector();

  /**
   * @label single existing instance
   */
  private static LocalLibrary singleInstance = null;
  private static final int STORE = 1111;
  private static final int LOAD = 2222;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new LocalLibrary object.
   *
   * @deprecated used by castor !!
   */
  public LocalLibrary() {
    debug(this, "new instance");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * called by the castor persistenze layer
   *
   * @param values the words that will be contained in this library
   */
  public void setValues(final String[] values1) {
    values = Util.convert2vec(values1);
  }

  /**
   * called by the castor persistenze layer
   *
   * @return all values stored in this library
   */
  public final String[] getValues() {
    return Util.convert2StringArr(values);
  }

  /**
   * adds a single word to the local library - right after the word is added, the library is made persistent
   *
   * @param value
   */
  public final void add(final String value) {
    log(this, "comparing " + values.contains(value));

    if (!values.contains(value)) {
      values.add(value);

      try {
        // store the new version after expansion
        store();
      } catch (PersistenceException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

  /*
   *  (non-Javadoc)
   * @see java.lang.Object#finalize()
   */
  public void finalize() {
    try {
      store();
    } catch (PersistenceException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /**
   * @return a new instance
   */
  public static LocalLibrary instance() {
    if (singleInstance == null) {
      try {
        //create a new instance
        singleInstance = (LocalLibrary) persistenze(LOAD);
        debug(thiz, "size of library is " + singleInstance.getValues().length);

        if (singleInstance.getValues().length == 0) {
          Logger.warning(thiz, "the library is not ready - there are NO entries.");
        }
      } catch (PersistenceException e) {
        Logger.err(thiz, "unable to init the libary - so a blank library is created" + e);
        singleInstance = new LocalLibrary();
      }
    }

    return singleInstance;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.communication.vocabulary.AbtractLibrary#receiveValues()
   */
  public HashSet receiveValues() {
    return Util.convert2Hashset(values);
  }

  /**
   * makes the library persistent and stores a backup version of it
   *
   * @throws PersistenceException
   */
  protected void store() throws PersistenceException {
    // create a copy of this object as backup using the timestamp
    Persistence.marshallBackup(this);
    persistenze(STORE);
  }

  /**
   * TODO: 
   *
   * @param code TODO
   *
   * @return TODO
   *
   * @throws PersistenceException TODO
   */
  private static synchronized Object persistenze(int code)
    throws PersistenceException {
    switch (code) {
    case LOAD:
      return Persistence.unmarshall(LocalLibrary.class);

    case STORE:
      Persistence.marshall(thiz);

      return null;

    default:
      throw new PersistenceException("code is invalid for persitenze");
    }
  }
}
