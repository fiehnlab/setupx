package org.setupx.repository;

public class SystemConstants {
    // ~ Static fields/initializers
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public static final String XML_ELEMENT_SURNAME = "surname";
    public static final String XML_ELEMENT_USER = "user";
    public static final String XML_ELEMENT_LASTNAME = "lastname";
    public static final String XML_ELEMENT_PHONE = "phone";
    public static final String XML_ELEMENT_DESCRIBTION = "describtion";
    public static final String XML_ELEMENT_LOCATION = "location";
    public static final String XML_ELEMENT_WORKGROUP = "workgroup";
    public static final String XML_ELEMENT_PASSWORD = "password";
    public static final String XML_ELEMENT_ADMIN = "admin";
    public static final String XML_ELEMENT_EMAIL = "email";
    public static final String XML_ELEMENT_TAGNAME = "tagname";
    public static final String XML_ROOTELEMENT_TRAYSET = "trayset";
    public static final String XML_ELEMENT_TRAYNAME = "trayname";
    public static final String XML_ELEMENT_USERID = "id";
}
