/**
 * ============================================================================ File:    BinBaseServicePortSoapBindingStubSimulation.java Package: org.setupx.repository.core.communication.binbase.trigger cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.binbase.trigger;

import java.rmi.RemoteException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.SampleInfo;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.CommunicationExcpetion;
import org.setupx.repository.core.ws.M1_BindingImpl;


/**
 * 
 *
 * @author $author$
 * @version $Revision$
 */
public abstract class BinBaseServiceSimulation extends org.apache.axis.client.Stub implements edu.ucdavis.genomics.metabolomics.binbase.bci.server.webservice.BinBaseService {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param in0 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public String[] getAllExperimentIds(String in0) throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   */
  public String[] getAvailableDatabases() throws RemoteException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public Experiment getExperiment(String in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public String[] getOutDatedSampleIds(String in0) throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public String[] getSampleComments(int in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public SampleInfo getSampleInfo(int in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public SampleInfo[] getSampleInfoByClass(String in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public SampleInfo[] getSampleInfoBySampleId(String in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public SampleInfo[] getSampleInfoBySampleName(String in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public String[] getSamplesForUser(String in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public String[] getSamplesForUserAsUser(User in0) throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public String[] getSamplesForUserAsUserId(long in0) throws RemoteException, BinBaseException {
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public ExperimentClass getStoredClass(String in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public ExperimentClass[] getStoredClasses(String in0)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public ExperimentSample[] getStoredSample(String in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public boolean containsSample(String in0, String in1)
    throws RemoteException, BinBaseException {
    return false;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public boolean containsSampleObject(ExperimentSample in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return false;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   * @param in2 TODO
   * @param in3 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public String createCustomizedExperiment(String in0, String[] in1, String in2, String in3)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   * @param in2 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void generateSchema(String in0, String in1, String in2)
    throws RemoteException, BinBaseException {
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void generateSchemaByUser(User in0, String in1)
    throws RemoteException, BinBaseException {
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   * @param in2 TODO
   *
   * @throws RemoteException TODO
   * @throws AuthentificationException TODO
   * @throws BinBaseException TODO
   */
  public void grantExperimentForUser(User in0, String in1, String in2)
    throws RemoteException, AuthentificationException, BinBaseException {
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public User login(String in0, String in1) throws RemoteException, BinBaseException {
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   *
   * @throws RemoteException TODO
   * @throws AuthentificationException TODO
   * @throws BinBaseException TODO
   */
  public void revokeSamplesFromUser(User in0) throws RemoteException, AuthentificationException, BinBaseException {
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void storeNetCDFFile(String in0, String in1) throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public String triggerExperimentURL(String in0) throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   *
   * @throws RemoteException TODO
   * @throws CommunicationExcpetion TODO
   */
  public void triggerExport(Experiment in0) throws RemoteException, CommunicationExcpetion {
    org.setupx.repository.core.util.logging.Logger.debug(this, "simulation export");
    new M1_BindingImpl().uploadResultFile(in0.getId(), "http://fiehnlab.ucdavis.edu/");
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerExportById(String in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerExportClass(ExperimentClass in0) throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerExportClassById(String in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerExportClasses(ExperimentClass[] in0)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerExportClassesById(String in0, String[] in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerExportDatabase(String in0) throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerImportClass(ExperimentClass in0) throws RemoteException, BinBaseException {
    org.setupx.repository.core.util.logging.Logger.debug(this, "simulation import class");
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerImportFile(String in0, byte[] in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerImportSample(ExperimentSample in0)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public String triggerProcessedExperimentURL(String in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerReImportClassById(String in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerReImportClassesById(String in0, String[] in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerReImportExperimentById(String in0, String in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param in0 TODO
   * @param in1 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerStoreFile(String in0, byte[] in1)
    throws RemoteException, BinBaseException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *    
   * @param in0 TODO
   *
   * @throws RemoteException TODO
   * @throws BinBaseException TODO
   */
  public void triggerUpdateDatabase(String in0) throws RemoteException, BinBaseException {
  }

  public void registerConnectionParameter(edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.ConnectionParameter in0) throws java.rmi.RemoteException{

  }
  public edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.ConnectionParameter[] getConnectionParameter() throws java.rmi.RemoteException, edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException, edu.ucdavis.genomics.metabolomics.exception.BinBaseException{
      return null;
  }

public Object createEmail(String in0, String in1, String in2) throws RemoteException, BinBaseException {
    return null;
}

public void sendMessage(String in0, String in1, String in2) throws RemoteException, BinBaseException {
}

public void sendMessageWithAttachment(String in0, String in1, String in2, String in3, String in4) throws RemoteException, BinBaseException {
}

public byte[] getNetCdfFile(java.lang.String in0) throws java.rmi.RemoteException, edu.ucdavis.genomics.metabolomics.exception.BinBaseException{return null;}
}
