/*
 * Created on 09.02.2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.setupx.repository.core.util.hotfix;

/**
 * @author scholz
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class HotFixException extends Exception {
    /**
     * 
     */
    public HotFixException() {
        super();
        // TODO Auto-generated constructor stub
    }
    /**
     * @param message
     */
    public HotFixException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }
    /**
     * @param message
     * @param cause
     */
    public HotFixException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }
    /**
     * @param cause
     */
    public HotFixException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }
}
