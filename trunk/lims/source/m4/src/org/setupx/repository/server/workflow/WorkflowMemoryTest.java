package org.setupx.repository.server.workflow;

import junit.framework.AssertionFailedError;
import junit.framework.TestCase;

public class WorkflowMemoryTest extends TestCase {

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Constructor for WorkflowMemoryTest.
     * @param arg0
     */
    public WorkflowMemoryTest(String arg0) {
        super(arg0);
    }

    public void testWorkflowMemory() {
        WorkflowMemory memory = new WorkflowMemory();
        assertNotNull("the memory key are null", memory.keys());

        assertTrue("mem contains something without filling it !! num of objects in mem = " + memory.keys().length, memory.keys().length == 0);
    }

    public void testPush() throws MemoryObjectNotFoundException {
        WorkflowMemory memory = new WorkflowMemory();

        MemoryObject memoryObject = new MemoryObject("ABC", 5);
        
        memory.push(memoryObject);
        
        assertTrue(memory.keys().length == 1);
        
        assertTrue(memory.pull("ABC").key.compareTo("ABC") == 0);
        

        try {
            memory.pull("abc");
            throw new AssertionFailedError("missing exception");
        } catch (MemoryObjectNotFoundException e) {
        }
        
    }

    public void testPull() {
        WorkflowMemory memory = new WorkflowMemory();

        for (int i = 0; i < 200; i++) {
            MemoryObject memoryObject = new MemoryObject("ABC" + i, i);
            memory.push(memoryObject);
        }
        
        assertTrue(memory.size() == 200);
    }

    public void testRemove() throws MemoryObjectNotFoundException {
        WorkflowMemory memory = new WorkflowMemory();
        
        MemoryObject memoryObject = new MemoryObject("testcase", new java.util.Vector());
        memory.push(memoryObject);
        
        
        assertTrue(memory.size() == 1);
        
        memory.remove(memoryObject.key);
        
        assertTrue(memory.size() == 0);

        try {
            memory.remove("BLABLA");
            throw new AssertionFailedError("missing exception");
        } catch (MemoryObjectNotFoundException e) {
        }
    }
    
    
    /*
     * Class under test for String toString()
     */
    public void testToString() {
    }

}
