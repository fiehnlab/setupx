/**
 * ============================================================================ File:    NullMetaDatasource.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import org.setupx.repository.core.communication.experimentgeneration.MetadataSource;
import org.setupx.repository.core.communication.experimentgeneration.template.ClazzTemplate;
import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.core.util.hotfix.oldsamples.mapping.MappingException;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz;
import org.setupx.repository.web.forms.inputfield.multi.Promt;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class NullMetaDatasource implements MetadataSource {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public ClazzTemplate[] getClazzTemplates() {
    // TODO Auto-generated method stub
    return new ClazzTemplate[0];
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.communication.experimentgeneration.MetadataSource#isFinishedRun()
   */
  public boolean isFinishedRun() {
    return false;
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws PromtCreateException TODO
   */
  public Promt createPromt() throws PromtCreateException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   */
  public void createScannedPairs() {
  }

  /**
   * TODO: 
   *
   * @param formObject TODO
   */
  public void map(InputField formObject) {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param multiField4Clazz TODO
   */
  public void map(MultiField4Clazz multiField4Clazz) {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param formObject TODO
   * @param path TODO
   *
   * @throws MappingException TODO
   */
  public void map(InputField formObject, String path) throws MappingError {
    // TODO Auto-generated method stub
  }
}
