/**
 * ============================================================================ File:    LevenshteinDistanceMetric.java Package: org.setupx.repository.core.util cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util;

/**
 * Calculates the distance between Strings x and y using the <b>Dynamic Programming</b> algorithm.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class LevenshteinDistanceMetric {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static int m;
  private static int n;
  private static int[][] T;

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Calculates the distance between Strings x and y using the <b>Dynamic Programming</b> algorithm.
   */
  public static final int distance(String x, String y) {
    m = x.length();
    n = y.length();

    T = new int[m + 1][n + 1];

    T[0][0] = 0;

    for (int j = 0; j < n; j++) {
      T[0][j + 1] = T[0][j] + 1;
    }

    for (int i = 0; i < m; i++) {
      T[i + 1][0] = T[i][0] + 1;

      for (int j = 0; j < n; j++) {
        T[i + 1][j + 1] = min(T[i][j] + sub(x, i, y, j), T[i][j + 1] + 1, T[i + 1][j] + 1);
      }
    }

    return T[m][n];
  }

  /**
   * TODO: 
   *
   * @param a TODO
   * @param b TODO
   * @param c TODO
   *
   * @return TODO
   */
  private static int min(int a, int b, int c) {
    return Math.min(Math.min(a, b), c);
  }

  /**
   * TODO: 
   *
   * @param x TODO
   * @param xi TODO
   * @param y TODO
   * @param yi TODO
   *
   * @return TODO
   */
  private static int sub(String x, int xi, String y, int yi) {
    return (x.charAt(xi) == y.charAt(yi)) ? 0 : 1;
  }
}
