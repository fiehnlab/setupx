/**
 * ============================================================================ File: MethodBlank.java Package:
 * org.setupx.repository.core.communication.exporting.sampletypes cvs: $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06
 * 21:54:21 scholz Exp $ ============================================================================ Martin Scholz
 * Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/
 * ============================================================================
 */
package org.setupx.repository.core.communication.exporting.sampletypes;

import org.setupx.repository.core.communication.exporting.JobInformation;
import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;

/**
 * 
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 * @hibernate.subclass
 */
public class MethodBlank extends AcquisitionSample {
    // ~ Static fields/initializers
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private static final String NAME = "Method Blank";

    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new MethodBlank object.
     */
    public MethodBlank() {
        super();
    }

    /**
     * Creates a new MethodBlank object.
     * 
     * @param information
     *                
     * @throws MappingError
     *                 
     */
    public MethodBlank(JobInformation information) throws MappingError {
        super(information, NAME);
        this.positionImportance = 0;
        this.sequenceImportance = 0;
        this.typeShortcut = "mb";
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /*
     * (non-Javadoc)
     * 
     * @see org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample#connectToSample()
     */
    public void connectToSample() throws PersistenceActionFindException, PersistenceActionUpdateException {
        debug(org.setupx.repository.core.util.Util.getClassName(this)
                + " are not connected to any samples. - so will be ignored.");
    }
}
