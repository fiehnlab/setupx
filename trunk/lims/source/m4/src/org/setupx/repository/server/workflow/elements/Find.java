package org.setupx.repository.server.workflow.elements;

import org.setupx.repository.core.InitException;
import org.setupx.repository.server.workflow.WorkFlowException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class Find extends Process {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Find object.
   *
   * @param configuration 
   *
   * @throws InitException 
   * @throws WorkFlowException 
   */
  public Find(ProcessConfiguration configuration) throws InitException, WorkFlowException {
    super(configuration);
  }

  /**
   * Creates a new Find object.
   *
   * @throws InitException 
   * @throws WorkFlowException 
   */
  public Find() throws InitException, WorkFlowException {
    super(new ProcessConfiguration("Finder - TODO", 20));
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @throws ProcessingException TODO
   */
  public void action() throws ProcessingException {
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.WFElement#getDataInKeys()
   */
  protected String[] getDataInKeys() {
    // TODO Auto-generated method stub
    return null;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.WFElement#getDataOutKeys()
   */
  protected String[] getDataOutKeys() {
    // TODO Auto-generated method stub
    return null;
  }
}
