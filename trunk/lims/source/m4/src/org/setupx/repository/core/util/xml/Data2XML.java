/**
 * ============================================================================ File:    Data2XML.java Package: org.setupx.repository.core.util.xml cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.xml;

import org.setupx.repository.core.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;

import java.io.File;
import java.io.UTFDataFormatException;

import java.util.HashMap;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


/**
 * Converts Data to an XML_Document.
 *
 * @author <a href="mailto:scholz@zeypher.com?subject=m1" >&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.7 $
 */
public abstract class Data2XML {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected Document document = null;
  protected HashMap trayNamesHashTable = new HashMap();
  private String XMLchildName = "child";
  private String XMLrootElementName = "root";
  private String lineSeperator = "\n";
  private String valueSeperator = ";";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Data2XML object.
   *
   * @param i_document 
   */
  public Data2XML(Document i_document) {
    this.document = i_document;

    if (i_document == null) {
      throw new NullPointerException("document is null");
    }

    Logger.log(this, "creating a new object.");

    try {
      Logger.log(this, "importing config files.");
      importConfigFiles();
    } catch (Exception e) {
      Logger.log(this, e);
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * reads the content of a file
   *
   * @param i_file the file that will be read
   *
   * @return the content of the file
   *
   * @throws java.io.IOException there were problems accessing the file
   */
  public static String getFileContent(File i_file) throws java.io.IOException {
    // file size
    int _sizeFile = (int) i_file.length();

    // number of character
    int _char_read = 0;

    java.io.FileReader inFile = new java.io.FileReader(i_file);
    char[] readData = new char[_sizeFile];

    while (inFile.ready()) {
      _char_read += inFile.read(readData, _char_read, _sizeFile - _char_read);
    }

    inFile.close();

    String _text_read = new String(readData, 0, _char_read);

    return _text_read;
  }

  /**
   * TODO: 
   *
   * @param string TODO
   */
  public void setLineSeperator(String string) {
    lineSeperator = string;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getLineSeperator() {
    return lineSeperator;
  }

  /**
   * TODO: 
   *
   * @param string TODO
   */
  public void setValueSeperator(String string) {
    valueSeperator = string;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getValueSeperator() {
    return valueSeperator;
  }

  /**
   * TODO: 
   *
   * @param string TODO
   */
  public void setXMLchildName(String string) {
    XMLchildName = string;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getXMLchildName() {
    return XMLchildName;
  }

  /**
   * TODO: 
   *
   * @param string TODO
   */
  public void setXMLrootElementName(String string) {
    XMLrootElementName = string;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getXMLrootElementName() {
    return XMLrootElementName;
  }

  /**
   * Creates an XML-Document out of the given data. If the document has not been initialised, a new document will be created.
   *
   * @return TODO
   *
   * @throws ParserConfigurationException TODO
   * @throws ArrayIndexOutOfBoundsException TODO
   */
  public String convert(String i_sourceData) throws ParserConfigurationException {
    // if the document has not been initialised, a new one will be created
    if (document == null) {
      document = createNewDocument();
    }

    Element root = document.createElement(XMLrootElementName);
    document.appendChild(root);

    // cuts the file into tokens representing a single line in the file
    StringTokenizer stringTokenizerAll = new StringTokenizer(i_sourceData, lineSeperator);

    // the number of lines in the sourceData
    int _numLines = -1;
    StringBuffer o_resultSB = new StringBuffer();

    while (stringTokenizerAll.hasMoreTokens()) {
      _numLines++;

      Element lineObject = document.createElement(XMLchildName);
      root.appendChild(lineObject);

      String _line = stringTokenizerAll.nextToken();

      try {
        string2dom(lineObject, _line);
      } catch (Data2XMLExcetption e) {
        Logger.log(this, e);

        // did not covert correctly - so remove whole line in document
        Node parentNode = lineObject.getParentNode();
        parentNode.removeChild(lineObject);

        // add unconverted line to StringBuffer
        Logger.log(this, "unable to convert " + lineObject);
        o_resultSB.append(_line).append("\n");
      }
    }

    // return all unconverted Lines
    return o_resultSB.toString();
  }

  /**
   * reads tagnames from a XML File. Opens a XML file  and reads all names for the specified element and returns these as a Sting Array
   *
   * @param i_SourceFile TODO
   *
   * @return TODO
   *
   * @throws ImporterException TODO
   */
  protected String[] getTagNamesFormFile(File i_SourceFile, String i_tagName)
    throws Data2XMLExcetption {
    try {
      XMLFile _xmlFile = new XMLFile(i_SourceFile);
    } catch (SAXException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (UTFDataFormatException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    NodeList _list = document.getElementsByTagName(i_tagName); // TODO

    String[] tagNamesTRAY_NAME = new String[_list.getLength()];
    Node _tmpNode = null;
    Element _element = null;

    for (int i = 0; i < _list.getLength(); i++) {
      _tmpNode = _list.item(i);

      // get all tagnames from element in configfile
      if (_tmpNode.getNodeType() == Document.ELEMENT_NODE) {
        _element = (Element) _tmpNode;
        tagNamesTRAY_NAME[i] = _element.getFirstChild().getNodeValue();
      } else {
        throw new Data2XMLExcetption("unable to find " + i_tagName + " in XML_File");
      }
    }

    return tagNamesTRAY_NAME;
  }

  /**
   * TODO: 
   *
   * @param i_configFile TODO
   *
   * @throws ImporterException TODO
   */
  protected String[] addTabConfigToHash(File i_configFile)
    throws Data2XMLExcetption {
    String[] columsNames = getTagNamesFormFile(i_configFile, "tagname");
    Logger.log(this, "adding " + i_configFile.getAbsolutePath() + " to ConfigHash with  " + columsNames.length + " attributes");

    for (int i = 0; i < columsNames.length; i++) {
      //strings[i] = strings[i].replaceAll("-","");   // TODO ersetzen durch einen echten replacer
      columsNames[i] = columsNames[i].replaceAll(",", "");
      columsNames[i] = columsNames[i].replaceAll(" ", "-");

      //strings[i] = strings[i].replaceAll(".","_");
    }

    int numberOfcolums = columsNames.length;

    if (trayNamesHashTable.containsKey(numberOfcolums + "")) {
      throw new Data2XMLExcetption("error - a config file with this number of colums has allready been stored in the hashtable");
    }

    trayNamesHashTable.put(numberOfcolums + "", columsNames);

    return columsNames;
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws ParserConfigurationException TODO
   */
  protected Document createNewDocument() throws ParserConfigurationException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document _document = builder.newDocument();

    return _document;
  }

  /**
   * TODO: 
   *
   * @throws Exception TODO
   */
  protected abstract void importConfigFiles() throws Exception;

  /**
   * finds all elements in a String an attaches these elements as xml-nodes to the given element. The Datafileds in the String have to be seperated by a specific character, witch can be set via setValueSeperator()
   *
   * @param i_Element
   * @param i_String
   *
   * @return null if the conversion has been succesfull - otherwise the original String that had to be converted will be returned.
   */
  protected void string2dom(Element i_Element, String i_StringToConvert)
    throws Data2XMLExcetption {
    if ((i_Element == null) || (i_StringToConvert == null)) {
      throw new Data2XMLExcetption("illegal Paramter:  Element:" + i_Element + "  StringToConvert: " + i_StringToConvert);
    }

    /*
       Logger.log(this, i_StringToConvert);
       Logger.log(this, "adding SPACES between the tokens ");
     */
    i_StringToConvert = i_StringToConvert.replaceAll(this.getValueSeperator(), " " + this.getValueSeperator());

    StringTokenizer stringTokenizerLine = new StringTokenizer(i_StringToConvert, valueSeperator);

    // the number of datafields
    int tmpNumDataFields = -1;
    int totalNumDataFields = stringTokenizerLine.countTokens();

    while (stringTokenizerLine.hasMoreTokens()) {
      tmpNumDataFields++;

      String value = stringTokenizerLine.nextToken();

      String[] XMLTagNames = getTabNames(totalNumDataFields);

      //logger.log(this,"ColumNames " + XMLTagNames);
      if (XMLTagNames == null) {
        throw new Data2XMLExcetption("unable to find the configFile for a dataset of " + totalNumDataFields + " colums.  OriginalData: " + i_StringToConvert, totalNumDataFields);
      } else if (tmpNumDataFields > (XMLTagNames.length - 1)) {
        throw new Data2XMLExcetption(new ArrayIndexOutOfBoundsException("not enough names in the fieldnames-Vector - required " + tmpNumDataFields + " but just " + (XMLTagNames.length - 1) + " TAGs"));
      } else {
        String fieldname = XMLTagNames[tmpNumDataFields];
        Element element = null;
        Text newTextNode = null;

        try {
          //Logger.log(this, fieldname);
          if (fieldname.matches("name")) {
            value = value.replace(':', '_');
          }

          element = document.createElement(fieldname);
          newTextNode = document.createTextNode(value);

          // attach textnode and element to the given Element
          element.appendChild(newTextNode);
          i_Element.appendChild(element);
        } catch (org.w3c.dom.DOMException domEx) {
          Logger.log(this, domEx);
          throw new Data2XMLExcetption("unable to create element: fieldname:'" + fieldname + "'  value   '" + value + "' ", domEx);
        }
      }
    }
  }

  /**
   * TODO: 
   *
   * @param i_numberOfColums TODO
   *
   * @return TODO
   */
  private String[] getTabNames(int i_numberOfColums) {
    /*          log ("current number of configfiles: " + trayNamesHashTable.size());
       for (int i = 0 ; i < 50; i++){
               logger.log(this,"size: " + i + "   -->  " + trayNamesHashTable.get(i+"") + " trying to get num:" + i_numberOfColums);
       }
     */
    return (String[]) this.trayNamesHashTable.get(i_numberOfColums + "");

    //TODO
  }
}
