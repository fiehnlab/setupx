/**
 * ============================================================================ File:    Restriction4StringLength.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.ValidationAnswer;
import org.setupx.repository.web.forms.validation.ValidationException;


/**
 * Restriction to set the minimum requiered lenght of a field.
 * 
 * <p>
 * The value that is checked must be a String.
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.23 $
 *
 * @hibernate.subclass
 */
public class Restriction4StringLength extends Restriction {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private int minLength = 0;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Restriction4StringLength object.
   */
  public Restriction4StringLength() {
    super();
  }

  /**
   * Creates a new Restriction4StringLength object.
   *
   * @param minLength 
   */
  public Restriction4StringLength(int minLength) {
    super();
    this.minLength = minLength;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * set the requiered length for the value
   *
   * @param minLength the requiered length for the value
   */
  public void setMinLength(int minLength) {
    this.minLength = minLength;
  }

  /**
   * requiered length that the value of the related object in the formobject has to have.
   *
   * @hibernate.property
   */
  public int getMinLength() {
    return minLength;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#cloneRestriction()
   */
  public Restriction cloneRestriction() {
    return new Restriction4StringLength(this.minLength);
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#validateObject()
   */
  public ValidationAnswer validateObject(InputField field)
    throws ValidationException {
    if ((field.getValue()).toString().length() < minLength) {
      return new ValidationAnswer(field, "please describe it detailed. Type at least " + this.minLength + " characters.");
    } else {
      return null;
    }
  }
}
