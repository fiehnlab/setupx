package org.setupx.repository.core.util;

import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.util.zip.ZipException;

import junit.framework.TestCase;

public class ZipTest extends TestCase {
    public static final File file   = new File("C:\\dev\\data\\testdata\\file4testcase.zip");
    public static final File target = new File(
					    "C:\\dev\\data\\testdata\\testcasedata\\a\\b\\a\\a\\b\\a\\a\\b\\a\\a\\b\\a\\a\\b\\a\\a\\b\\a\\a\\b\\a\\a\\b\\a\\a\\b\\a\\");

    public void testUnzipFile() throws IllegalArgumentException, IOException {
	Zip.unzipFile(file, target);
    }

    public void testGetFilenamesFromZip() throws ZipException, IOException {
	Vector vector = Zip.getFilenamesFromZip(file);
	assertTrue("wrong number of files found", vector.size() == 3);
    }

}
