package org.setupx.repository.core.util.print.remote;

import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.text.DateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.print.PrintService;

import org.setupx.repository.core.util.print.DYMOLabelPrintConnector;
import org.setupx.repository.server.persistence.SXQuery;

public class DYMOLabelPrintConnector4SetupX extends DYMOLabelPrintConnector{
    private static final int lablesPerPage = 4;
    private long[] sampleIDs;
    boolean finished = false;
    private String[] sampleLables;

    
    public DYMOLabelPrintConnector4SetupX(long id){
        long promtID = id  ;

        // read sampleIDs
        this.sampleIDs = toArray(readIDs(promtID));
        this.sampleLables = readLabels(this.sampleIDs);
    }
    
    private static long[] toArray(List list) {
        long[] ls = new long[list.size()];
        Iterator iterator = list.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            ls[i] = Long.parseLong("" + iterator.next());
            i++;
        }
        return ls;
    }

    private List readIDs(long promtID) {
        return new SXQuery().findSampleIDsByPromtID(promtID);
    }

    private static String[] readLabels(long[] sampleIDs) {
        String[] result = new String[sampleIDs.length];
        for (int i = 0; i < result.length; i++) {
            try {
                result[i] = new SXQuery().findSampleLabelBySampleID(sampleIDs[i]);
                System.out.println(i + "  " + result[i] + "  " + sampleIDs[i]);
            } catch (Exception e) {
                e.printStackTrace();
                result[i] = "-unknown-";
            }
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * @see org.setupx.repository.core.util.print.DYMOLabelPrintConnector#getValue(int, int)
     */
    public String getValue(final int elementOnLabel, final int labelCounter) {
        if (labelCounter == this.sampleIDs.length) finished = true;
        String value = "";
        
        switch (elementOnLabel) {
        case 0:
          // what ever you want to have in this line
          value = "SetupX";

          break;

        case 1:
            // what ever you want to have in this line
          value = "fiehnlab.ucd";

          break;

        case 2:
            // what ever you want to have in this line
          //value = "id: " + labelCounter;
            try {
            value = "" + this.sampleIDs[labelCounter];
            } catch (Exception e) {
                value = "---";
            }

          break;

        case 3:
            // what ever you want to have in this line
          // TODO - add DB connection
            try{
                value = this.sampleLables[labelCounter];
            } catch (Exception e) {
                value = "---";
            }

          break;

        case 4:
            // what ever you want to have in this line
          value = DateFormat.getDateInstance(DateFormat.SHORT, Locale.US).format(new Date());

          break;

        default:
          break;
        }

        return value;
    }


/*
 * (non-Javadoc)
 * @see org.setupx.repository.core.util.print.DYMOLabelPrintConnector#getPageNumbers()
 */  
    public int getPageNumbers() {
      int number = this.sampleIDs.length / lablesPerPage;
      if (this.sampleIDs.length % 4 > 0) number++;
      return number;
  }
    
    public static void print(long promtID) {
        System.out.println("starting");
        System.out.println("id "+ promtID);

        PrinterJob printerJob = PrinterJob.getPrinterJob();
        PageFormat pageFormat = printerJob.defaultPage();
        Paper paper = new Paper();

        final double widthPaper = (1.2 * 72);
        final double heightPaper = (1.5 * 72);

        paper.setSize(widthPaper, heightPaper);
        paper.setImageableArea(0, 0, widthPaper, heightPaper);

        pageFormat.setPaper(paper);

        pageFormat.setOrientation(PageFormat.LANDSCAPE);

        System.out.println("using a printmenu: " + PRINTMENU);
        if (PRINTMENU) {
          if (printerJob.printDialog()) {
            printerJob.setPrintable(new DYMOLabelPrintConnector4SetupX(promtID), pageFormat); // The 2nd param is necessary for printing into a label width a right landscape format.

            try {
              printerJob.print();
            } catch (PrinterException e) {
              e.printStackTrace();
            }
          }
        } else {
          PrintService[] printService = PrinterJob.lookupPrintServices();

          for (int i = 0; i < printService.length; i++) {
            System.out.println(printService[i].getName());

            if (printService[i].getName().compareTo(PRINTERNAME) == 0) {
              try {
                printerJob.setPrintService(printService[i]);
                printerJob.setPrintable(new DYMOLabelPrintConnector4SetupX(promtID), pageFormat); // The 2nd param is necessary for printing into a label width a right landscape format.
                printerJob.print();
              } catch (PrinterException e) {
                e.printStackTrace();
              }
            }
          }
        }
      }
}
