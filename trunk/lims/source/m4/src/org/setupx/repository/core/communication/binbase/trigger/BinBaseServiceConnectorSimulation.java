
package org.setupx.repository.core.communication.binbase.trigger;

import org.setupx.repository.core.ws.M1_BindingImpl;
import org.setupx.repository.core.ws.M1_PortType;
import org.setupx.repository.core.ws.M1_ServiceLocator;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.rpc.ServiceException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class BinBaseServiceConnectorSimulation extends BinBaseServiceConnector {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new BinBaseServiceConnectorSimulation object.
   */
  public BinBaseServiceConnectorSimulation() {
    super(null);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   */
  public void testM1() {
    M1_PortType service;

    service = new M1_BindingImpl();
    testM1(service);

    try {
      service = new M1_ServiceLocator().getm1(new URL("http://128.120.136.206:8080/m1/services/m1"));
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (ServiceException e) {
      e.printStackTrace();
    }

    testM1(service);
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws ServiceException TODO
   */
  protected edu.ucdavis.genomics.metabolomics.binbase.bci.server.webservice.BinBaseService createStub()
    throws ServiceException {
    return null; //new BinBaseServiceSimulation();
  }

  /**
   * TODO: 
   *
   * @param m1 TODO
   */
  private static void testM1(M1_PortType m1) {
    /*m1.getClazzID(null);
       getExperimentIDbyClazz("3319");
       getExperimentIDbySample("");
       getExperimentsSheduled();
       getSampleIDbyACQLabel(java.lang.String acquistitionName);
       activateService(int serviceCode);
       uploadResultFile(java.lang.String clazzSetID, java.lang.String resultfile_url);
     */
  }
}
