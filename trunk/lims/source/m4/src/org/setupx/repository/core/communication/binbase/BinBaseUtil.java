package org.setupx.repository.core.communication.binbase;

import java.util.Iterator;

import org.setupx.repository.core.communication.CommunicationException;
import org.setupx.repository.core.communication.leco.MissingMachineInformationException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.web.forms.inputfield.multi.Promt;

public class BinBaseUtil {

    /**
     * checks for the most up to date experiments and runs them through binbase.
     * @throws CommunicationException 
     * @throws MissingMachineInformationException 
     * @throws PersistenceActionFindException 
     */
    public static void updateBinBaseExperiments(){
        Iterator promtIDsIterator = new SXQuery().findPromtIDs().iterator();
        
        while (promtIDsIterator.hasNext()) {
            long promtID = Long.parseLong(""+promtIDsIterator.next());
            
            if(checkPromt(promtID)){
                try {
                    Promt.export(promtID);
                } catch (PersistenceActionFindException e) {
                    e.printStackTrace();
                } catch (MissingMachineInformationException e) {
                    e.printStackTrace();
                } catch (CommunicationException e) {
                    e.printStackTrace();
                }
            }
            
        }
        
        
    }


    private static boolean checkPromt(long promtID) {
        return true;
    }
}
