/*
 * Created on 24.01.2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.setupx.repository.web.forms;

/**
 * @author scholz
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ParentNotAvailableException extends Exception {

    /**
     * 
     */
    public ParentNotAvailableException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public ParentNotAvailableException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public ParentNotAvailableException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     */
    public ParentNotAvailableException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
