/**
 * ============================================================================ File:    StringInputfield.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import java.util.Hashtable;

import org.hibernate.Session;

import org.setupx.repository.core.util.Util;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.web.forms.restriction.Restriction4StringLength;


/**
 * Input field containing a simple string
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.25 $
 *
 * @hibernate.subclass
 */
public class StringInputfield extends InputField implements Removable {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String extension = "";
  private boolean autosubmit = false;
  private boolean removable = false;
  private int size_x = 40;

  {
    this.setRestriction(new Restriction4StringLength(3));
  }
      
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @deprecated just for peristencelayer
   */
  public StringInputfield() {
    this("...", "...");
  }

  /**
   * Creates a new StringInputfield object.
   */
  public StringInputfield(String question, String description, boolean i_removable) {
    super(question, description);
    setRemovable(i_removable);
  }

  /**
   * Creates a new StringInputfield object.
   *
   * @param question 
   * @param description 
   * @param answer 
   * @param i_removable 
   */
  public StringInputfield(String question, String description, String answer, boolean i_removable) {
    super(question, description);
    setQuestion(question);
    setValue(answer);
    setRemovable(i_removable);
  }

  /**
   * Creates a new StringInputfield object.
   *
   * @param question 
   * @param description 
   */
  public StringInputfield(String question, String description) {
    super(question, description);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param autosubmit TODO
   */
  public void setAutosubmit(boolean autosubmit) {
    this.autosubmit = autosubmit;
  }

  /**
   * TODO: 
   *
   * @hibernate.property
   */
  public boolean getAutosubmit() {
    return autosubmit;
  }

  /**
   * @return
   *
   * @hibernate.property
   */
  public String getExtension() {
    return this.extension;
  }

  /**
   * TODO: 
   *
   * @param removable TODO
   */
  public void setRemovable(boolean i_removable) {
    this.removable = i_removable;
  }

  /**
   * @return ture if the object can be removed<p>
   *
   * @hibernate.property
   */
  public boolean getRemovable() {
    return removable;
  }

  /**
   * x row
   *
   * @param size_x TODO
   */
  public void setSize_x(int size_x) {
    this.size_x = size_x;
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @hibernate.property
   */
  public int getSize_x() {
    return size_x;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() {
    InputField instance = new StringInputfield(this.getQuestion(), this.getDescription(), (String) this.getValue(), this.getRemovable());

    // copy the restriction
    if (this.getRestriction() != null) {
      instance.setRestriction(this.getRestriction().cloneRestriction());
    }

    return instance;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#getHTML()
   */
  public String createHTML(int col) {
    StringBuffer buffer = new StringBuffer();
    buffer.append("\n<!-- " + Util.getClassName(this) + " -->\n<tr>\n");
    buffer.append("<td " + createBackgroudcolor() + ">" + this.getQuestion() + createRequieredNote());
    buffer.append("<br><font size=\"-3\">&nbsp &nbsp &nbsp " + this.getDescription() + "</font></span></td>");
    buffer.append("\n<td " + createBackgroudcolor() + " colspan=\"" + (col - 1) + "\">");

    // button to remove this
    if (this.getRemovable()) {
      buffer.append(this.createRemoveButton());
    }

    // new addition - button for history completion
    buffer.append(this.createHistoryButton());

    buffer.append(this.createField());

    buffer.append(this.createHelpButton());

    buffer.append("</td></tr>\n");

    return buffer.toString();
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public final String toStringHTML() {
    if (this.isActive()) {
      return super.toStringHTML() + this.getExtension();
    } else {
      return "";
    }
  }

  /**
   * TODO: 
   *
   * @param extension TODO
   */
  public void setExtension(String extension) {
    this.extension = extension;
  }

  /**
   * creates just the inputfield - excludung any table formatting
   */
  public String createField() {
    StringBuffer buffer = new StringBuffer();
    String submit = "";

    if (autosubmit) {
      submit = "onBlur=\"document.forms[0].submit()\"";
    }

    buffer.append("<a name=\"" + this.getName() + "\"/><input type=\"text\" name=\"" + this.getName() + "\"  size=\"" + this.getSize_x() + "\"  " + submit + " value=\"" + (String) this.getValue() + "\"/> " + this.getExtension());
    buffer.append(this.createErrorMSG());

    return buffer.toString();
  }
 
  
  public void update(Session session, boolean createIt) throws PersistenceActionUpdateException {
      super.update(session, createIt);
  }
  
  public String getXMLChildname() {
      return "element";
   }
  
  public Hashtable getExportElements(){
      Hashtable hashtable = super.getExportElements();
      return hashtable;
  }  
  
  public Hashtable getExportAttributes() {
    Hashtable hashtable = super.getExportAttributes();
    hashtable.put("extension", this.getExtension());
    return hashtable; 
  }
}
