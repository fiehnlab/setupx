/**
 * ============================================================================ File:    DictWebServiceCheck.java Package: org.setupx.repository.core.communication.vocabulary cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.vocabulary;

import org.setupx.repository.core.CoreObject;


/**
 * a checker for a remote dictionary - <code>webservice </code>based.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.10 $
 *
 * @deprecated not used
 */
class DictWebServiceCheck extends CoreObject implements VocabularyCheck {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param word TODO
   *
   * @return TODO
   *
   * @throws VocabularyCheckException TODO
   */
  public String getSuggestion(String word) throws VocabularyCheckException {
    // TODO Auto-generated method stub
    return "";
  }

  /**
   * TODO: 
   *
   * @param word TODO
   *
   * @return TODO
   *
   * @throws VocabularyCheckException TODO
   */
  public boolean check(String word) throws VocabularyCheckException {
    // TODO Auto-generated method stub
    return false;
  }

  /*  public DictWebServiceCheck() throws InstantiationException {
     super();
     if (!ACTIVE) {
       return;
     }
     try {
       service = (DictServiceSoapStub) new DictServiceLocator().getDictServiceSoap();
       service.setTimeout(Config.WEB_SERVICE_TIMEOUT_STANDARD);
       String value = null;
       value = service.serverInfo();
       debug(this, "new instance");
     } catch (ServiceException e) {
       throw new InstantiationException(e.toString());
     } catch (RemoteException e) {
       throw new InstantiationException(e.toString());
     } catch (NoClassDefFoundError e) {
       throw new InstantiationException(e.toString());
     }
     }
       public String getSuggestion(final String word) throws VocabularyCheckException {
       Logger.warning(this, "getSuggestion method is not supported by this checker.");
       throw new VocabularyCheckException("getSuggestion method is not supported by this checker.");
     }
     public boolean check(final String word) throws VocabularyCheckException {
       if (!ACTIVE) {
         return false;
       }
       Definition[] definitions;
       try {
         definitions = this.service.define(word).getDefinitions().getDefinition();
         debug(this, "found " + definitions.length + " definitions for " + word);
         return (definitions.length > 0);
       } catch (RemoteException e) {
         throw new VocabularyCheckException(e);
       } catch (NullPointerException e) {
         Logger.warning(this, "nullpointer: " + e);
         return false;
       }
     }
   */
}
