/**
 * ============================================================================ File:    AdminPage.java Package: org.setupx.repository.web.forms.page cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.page;

/**
 * special page for the display of user - they have a summary at the top
 *
 * @hibernate.subclass
 */
public class AdminPage extends Page {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new AdminPage object.
   *
   * @param name 
   * @param description 
   * @param longDescription 
   */
  public AdminPage(String name, String description, String longDescription) {
    super(name, description, longDescription);
    //this.addField(new Navigation(this, UserInputfield.class));
  }

  /**
   * Creates a new AdminPage object.
   */
  public AdminPage() {
  }
}
