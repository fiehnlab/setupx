/**
 * ============================================================================ File:    AcquisitionServlet.java Package: org.setupx.repository.core.communication.exporting cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.exporting;

import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.core.util.web.UiHttpServlet;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.forms.inputfield.multi.Sample;


/**
 * Servlet for the Generation of Acquisition Tasks  modified: 20. june 2006
 */
public class AcquisitionServlet extends UiHttpServlet {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.core.util.web.UiHttpServlet#process(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  public void process(HttpServletRequest request, HttpServletResponse response)
    throws Throwable {
    // take the list of samples from the session 
    HashSet samples = (HashSet) request.getSession().getValue(WebConstants.SESS_SAMPLES_SELECTED);
    long[] sampleIDs = Sample.mapToIDs(samples);

    if (samples == null) {
      Logger.warning(this, "no samples selected - going to be a pretty empty acq task");
      samples = new HashSet();
    }

    debug(samples.size() + " samples.");

    // get the seperated parts of the jobinformation 
    UserDO operator = (UserDO) request.getSession().getValue(WebConstants.SESS_USER);
    debug("operator: " + operator);

    int machineID = getRequestParameterInt(request, WebConstants.PARAM_MACHINE_ID);
    debug("machine: " + machineID);

    Machine machine = MachinePool.getMachine(machineID);
    debug("machine: " + machine);

    boolean surroundingBlanks = Boolean.valueOf(getRequestParameter(request, WebConstants.PARAM_BLANKS)).booleanValue();
    debug("surrounding Blanks: " + surroundingBlanks);

    int vailOffset = getRequestParameterInt(request, WebConstants.PARAM_VAILOFFSET);
    debug("offset for vails: " + vailOffset);

    boolean alex = Boolean.valueOf(getRequestParameter(request, WebConstants.PARAM_ALEX)).booleanValue();
    debug("using ALEX: " + alex);
    Logger.warning(this, "add alex funcitonality - WebConstants.PARAM_ALEX");

    // create a jobinformation object out of the seperated informations
    JobInformation jobInformation = new JobInformation(machine);

    String gc = getRequestParameter(request, "(GC)");
    String ms = getRequestParameter(request, "(MS)");
    String dp = getRequestParameter(request, "(DP)");

    jobInformation.getValues().put(AcquisitionParameter.GC_METHOD, gc);
    jobInformation.getValues().put(AcquisitionParameter.MS_METHOD, ms);
    jobInformation.getValues().put(AcquisitionParameter.DP_METHOD, dp);

    try {
      jobInformation.setOperator(operator.getAbbreviation());
    } catch (NullPointerException e) {
      jobInformation.setOperator("xx");
    }

    jobInformation.setSurroundingBlanks(surroundingBlanks);
    jobInformation.setVialPositionOffset(vailOffset);

    // create a acquisition 
    Acquisition acquisition = new XExporter().createAcquisition(sampleIDs, jobInformation);
    debug("acquisition created.");

    request.getSession().putValue(WebConstants.SESS_ACQUISTION, acquisition);
    debug("acquisition stored in promt.");

    forward("/aq/acquisition.jsp", request, response);
  }
}
