package org.setupx.repository.core.communication.status;

import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.server.persistence.PersistenceActionFindException;

/**
 * @hibernate.subclass
 */

public class ExportedEventStatus extends PostprocessEventStatus {

    public ExportedEventStatus(long sampleID, UserDO userDO, String message) throws StatusPersistenceException {
        super(sampleID, userDO, message);
    }
        
    public ExportedEventStatus(){
        super();
    }

    public String createDisplayString() {
        return "sample has been exported.";
    }

    public PersistentStatus createClone() {
        try {
            return new ExportedEventStatus(this.sampleID, this.getUser(getUserID()), this.getMessage());
        } catch (StatusPersistenceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (PersistenceActionFindException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
