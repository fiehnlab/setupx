package org.setupx.repository.core.communication.ncbi;


public class NCBIException extends Exception {

	public NCBIException(String string) {
		super(string);
	}

	/**
	 * @param string
	 * @param e
	 */
	public NCBIException(String string, Exception e) {
		super(string, e);
	}

	/**
	 * @param e
	 */
	public NCBIException(Exception e) {
		super(e);
	}
}
