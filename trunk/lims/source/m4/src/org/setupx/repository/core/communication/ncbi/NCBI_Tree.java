package org.setupx.repository.core.communication.ncbi;

import org.setupx.repository.core.communication.ncbi.local.NCBIEntry;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.SXQuery;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;


/**
 * 
 *
 * @author $author$
 * @version $Revision$
 */
public class NCBI_Tree {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** containing all IDs related - either a parent or a real ID itself */
  public static Hashtable taxIDs = new Hashtable();

  /** splitpoints with more then one child */
  public static HashSet splitpointIDs = new HashSet();
  public static Object thiz = new NCBI_Tree();

  static {
    try {
      init();
    } catch (NCBIException e) {
      e.printStackTrace();
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Check if it is a Splitpoint
   *
   * @param ncbiTaxonmyID 
   *
   * @return true if it is a known splitpoint
   */
  public static boolean isSplitpoint(int ncbiTaxonmyID) {
    return (splitpointIDs.contains(ncbiTaxonmyID + ""));
  }

  /**
   * TODO: 
   *
   * @param taxID TODO
   *
   * @return TODO
   *
   * @throws NCBIException TODO
   */
  public static int findExperiments(int taxID) throws NCBIException {
    if (taxIDs.containsKey("" + taxID)) {
      int numberOfExperiment = Integer.parseInt("" + taxIDs.get("" + taxID));

      if (numberOfExperiment > 0) {
        return numberOfExperiment;
      }
    }

    NCBIEntry entry = NCBIConnector.determineNCBI_Information(taxID);
    Logger.debug(thiz, "findExperiments for " + entry.getName());

    // HOTFIX 
    taxIDs.put(entry.getTaxID(), "" + 0);

    if (entry.getName().compareTo("unclassified sequences") == 0) {
      return 0;
    }

    try {
      Iterator iterator = entry.getChilds().iterator();
      int numberOfExpTotal = 0;

      while (iterator.hasNext()) {
        try {
          NCBIEntry element = (NCBIEntry) iterator.next();

          // check how many experiments exist for that one
          if (taxIDs.containsKey(element.getTaxID())) {
            int numberOfExperiment = Integer.parseInt("" + taxIDs.get(element.getTaxID()));

            if (numberOfExperiment > 0) {
              numberOfExpTotal = numberOfExpTotal + numberOfExperiment;
            } else {
              Logger.debug(thiz, "findExperiments for " + element.getName() + " as child of " + entry.getName());

              // HOTFIX
              // problem that root is child of root !!
              if (element.getTaxIdInt() == entry.getTaxIdInt()) {
                // dont to anything
                Logger.warning(thiz, "trying to access child where child.parent = child - element: " + element.getName());
              } else {
                numberOfExpTotal = numberOfExpTotal + findExperiments(element.getTaxIdInt());
              }
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }

      Logger.debug(thiz, "findExperiments for " + entry.getName() + " --> " + numberOfExpTotal);
      taxIDs.put(entry.getTaxID(), "" + numberOfExpTotal);

      return numberOfExpTotal;
    } catch (Exception e) {
      e.printStackTrace();

      return 0;
    }
  }

  /**
   * TODO: 
   *
   * @param speciesString TODO
   *
   * @return TODO
   *
   * @throws NCBIException TODO
   * @throws NCBIFindException TODO
   */
  public static NCBIEntry findGroup(final String speciesString)
    throws NCBIException {
    int ncbiID = NCBIConnector.determineNCBI_Id(speciesString);
    NCBIEntry entry = NCBIConnector.determineNCBI_Information(ncbiID);

    // keep going parent
    boolean loop = true;

    while (loop) {
      // in case it is a known ID
      if (splitpointIDs.contains(entry.getTaxID())) {
        // this is a splitpoint
        return entry;
      }

      try {
        entry = entry.getParent();
      } catch (NCBIFindException findException) {
        throw findException;
      }
    }

    throw new NCBIFindException("nothing found");
  }

  /**
   * TODO: 
   *
   * @throws NCBIException TODO
   */
  private static void init() throws NCBIException {
    Logger.debug(thiz, "----init-START---");

    // fill hashset with all IDs
    List speciesList = new SXQuery().findSpecies();

    /*
       List speciesList = new Vector();
       speciesList.add("Homo sapiens neanderthalensis");
       speciesList.add("Arabidopsis thaliana");
     */
    Iterator speciesIterator = speciesList.iterator();

    while (speciesIterator.hasNext()) {
      String speciesString = "" + speciesIterator.next();
      Logger.debug(thiz, "-----------------\n Species " + speciesString + " to taxIDs. number of taxID :" + taxIDs.size());

      int ncbiID = NCBIConnector.determineNCBI_Id(speciesString);
      NCBIEntry entry = NCBIConnector.determineNCBI_Information(ncbiID);
      int numberOfExperiment = new SXQuery().findPromtIDbySpecies(entry.getTaxIdInt()).size();

      boolean loop = true;

      while (loop) {
        // in case it is a known ID
        if (taxIDs.containsKey(entry.getTaxID())) {
          // this is a splitpoint
          splitpointIDs.add(entry.getTaxID());
          Logger.debug(thiz, "adding " + entry.getName() + " to splitpoints. number of splitpoints :" + splitpointIDs.size());
          loop = false;
        } else {
          // add experiements of all childs
          taxIDs.put(entry.getTaxID(), "" + numberOfExperiment);
          Logger.debug(thiz, "adding " + entry.getName() + " to taxIDs. number of taxID :" + taxIDs.size() + "  num of Exp: " + numberOfExperiment);

          try {
            entry = entry.getParent();
            numberOfExperiment = -1;
          } catch (NCBIFindException e) {
            // no parent anymore
            loop = false;
          }

          if (entry == null) {
            loop = false;
          }
        }
      }
    }

    Logger.debug(thiz, "----init-END---");

    speciesIterator = new SXQuery().findSpecies().iterator();

    while (speciesIterator.hasNext()) {
      String species = "" + speciesIterator.next();
      int id = NCBIConnector.determineNCBI_Id(species);
      Logger.log(null, "-----------------------" + species + "----------------------");
      Logger.log(null, NCBIConnector.determineNCBI_Information(id).getName() + " " + NCBI_Tree.findExperiments(id));
      Logger.log(null, "-----------------------------------------------------------");
    }
  }
}
