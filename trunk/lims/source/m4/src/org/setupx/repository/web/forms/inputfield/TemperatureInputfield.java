/**
 * ============================================================================ File:    TemperatureInputfield.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import org.setupx.repository.web.forms.inputfield.multi.Restriction4Double;
import org.setupx.repository.web.forms.restriction.Restriction4Numbers;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @hibernate.subclass
 */
public class TemperatureInputfield extends StringInputfield {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final int DEFAULT_TEMP = 15;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new TemperatureInputfield object.
   */
  public TemperatureInputfield() {
    this("...", "...");
  }

  /**
   * Creates a new TemperatureInputfield object.
   *
   * @param question 
   * @param description 
   */
  public TemperatureInputfield(String question, String description) {
    super(question, description);
    this.setRestriction(new Restriction4Double(-15, 50));
    this.setValue("" + DEFAULT_TEMP);
    this.setSize_x(5);
    this.setExtension("&#176C");
  }

  /**
   * Creates a new TemperatureInputfield object.
   *
   * @param string 
   * @param string2 
   * @param temp 
   */
  public TemperatureInputfield(String string, String string2, Object temp) {
    this(string, string2);
    this.setValue(temp);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() {
    return new TemperatureInputfield(this.getQuestion(), this.getDescription(), this.getValue());
  }
}
