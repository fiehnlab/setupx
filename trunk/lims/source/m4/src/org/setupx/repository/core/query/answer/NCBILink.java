package org.setupx.repository.core.query.answer;

import org.setupx.repository.util.Link;

public class NCBILink extends Link {

    public NCBILink(String label, int ncbiID) {
        super(label, null, null, "http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=" + ncbiID + "&lvl=3&lin=f&keep=1&srchmode=1&unlock");
        this.setImg("pics/ncbi.gif");
    }
}


