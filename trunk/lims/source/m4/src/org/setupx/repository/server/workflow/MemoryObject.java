package org.setupx.repository.server.workflow;

import org.setupx.repository.core.util.Util;

import java.io.Serializable;


/**
 * The Objects that can be stored in the WorkflowMemory.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class MemoryObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** the class of the Object beeing stored in this MemObject. */
  public Class clazz;

  /** the object that is kept inside this MemObject */
  public Serializable object;

  /** commments ... whar ever needed */
  public String comment;

  /** the key for identifying the object */
  public String key;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new MemoryObject object.
   *
   * @param key key of the object
   * @param object the object itself
   */
  public MemoryObject(String key, Serializable object) {
    this.key = key;
    this.clazz = object.getClass();
    this.object = object;
  }

  /**
   * Creates a new MemoryObject object.
   *
   * @param key key of the object
   * @param i the object itself
   */
  public MemoryObject(String key, int i) {
    this.key = key;
    this.clazz = Integer.class;
    this.object = new Integer(i);
  }

  /**
   * Creates a new MemoryObject object.
   *
   * @param key key of the object
   * @param i the object itself
   */
  public MemoryObject(String key, String i) {
    this.clazz = String.class;
    this.object = i;
    this.key = key;
  }

  /**
   * @param string
   * @param valid
   */
  public MemoryObject(String string, Boolean valid) {
    this.clazz = Boolean.class;
    this.object = valid;
    this.key = string;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public int objectAsInt() {
    return ((Integer) this.object).intValue();
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String objectAsString() {
    return (String) this.object;
  }

  /**
   * to String ...
   *
   * @see Object#toString()
   */
  public String toString() {
    return (Util.getClassName(this) + " " + this.key + " " + Util.getClassName(clazz) + " ( " + this.comment + " ) " + this.object);
  }
}
