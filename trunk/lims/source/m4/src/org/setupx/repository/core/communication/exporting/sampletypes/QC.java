/**
 * ============================================================================ File: QC.java Package:
 * org.setupx.repository.core.communication.exporting.sampletypes cvs: $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06
 * 21:54:21 scholz Exp $ ============================================================================ Martin Scholz
 * Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/
 * ============================================================================
 */
package org.setupx.repository.core.communication.exporting.sampletypes;

import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.communication.exporting.AcquisitionParameter;
import org.setupx.repository.core.communication.exporting.JobInformation;
import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;

/**
 * 
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 * @hibernate.subclass
 */
public class QC extends AcquisitionSample {
    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new QC object.
     */
    public QC() {
        super();
    }

    /**
     * Creates a new QC object.
     * 
     * @param label
     *                
     * @param jobInformation
     *                
     * @throws MappingError
     *                 
     */
    public QC(String label, JobInformation jobInformation) throws MappingError {
        super(jobInformation, label);
        this.positionImportance = 2;
        this.sequenceImportance = 2;
        this.typeShortcut = "qc";
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * map an extension of a QCname (qc10, qc50, ...) to the requiered label
     * 
     * @param qc_type
     * @return
     */
    public final static int map(int qc_type) {
        int returnValue = 0;

        switch (qc_type) {
        case 10:
            returnValue = 2;

            break;

        case 30:
            returnValue = 3;

            break;

        case 50:
            returnValue = 4;

            break;

        case 100:
            returnValue = 5;

            break;

        default:
            break;
        }

        return returnValue;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample#connectToSample()
     */
    public void connectToSample() throws PersistenceActionFindException, PersistenceActionUpdateException {
        debug(org.setupx.repository.core.util.Util.getClassName(this)
                + " are not connected to any samples. - so will be ignored.");
    }

    /**
     * QC have a different way of beeing labeled.
     */
    protected int generateCounterID() {
        /*
         * qc10 qc30 qc50 qc100
         */
        String _label = "";

        try {
            _label = this.getParamter(AcquisitionParameter.TRAY).getValue();
        } catch (ParameterException e) {
            err(null, e);
        }

        int qc_type = Integer.parseInt(org.setupx.repository.core.util.Util.replace(_label, "qc", ""));

        // going to be 10, 30, 50, 100
        return QC.map(qc_type);
    }
}
