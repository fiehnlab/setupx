package org.setupx.repository.web.pub.data.cache;

import java.util.List;

import org.hibernate.Session;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.web.forms.inputfield.multi.OrganInputfield;


/**
 * Find Variations from a precached table.
 * <p>
 * The {@link #VariationFinder()} is actually a cache that precaches the result of a single query.
 * @author scholz
 */
public class VariationFinder extends CoreObject {

    
    
    public static List findVariation(Class variationType, String searchTerm){
        Session session = createSession();
        String basic = "select multiField4Clazzes.uoid as dimension " + "     from  " + "      formobject as multiField4Clazzes, " + "      formobject as clazz, " + "      formobject as unknown, " + "      multiField4ClazzesHibernate as x " + "      where clazz.uoid = x.multiField4Clazzs " +
        "      and x.elt = multiField4Clazzes.uoid " + "      and multiField4Clazzes.active = true " + "      and unknown.uoid = clazz.parent " + "      and 1 < (select count(*) from formobject where parent = multiField4Clazzes.parent ) ";
        return session.createSQLQuery(basic).addScalar("dimension", org.hibernate.Hibernate.LONG).list();
    }
    
    public static void main(String[] args) {
        VariationFinder.findVariation(OrganInputfield.class, "blood");        
    }
}