package org.setupx.repository.core.communication.kerberos;

import org.setupx.repository.core.Connectable;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.CommunicationException;
import org.setupx.repository.core.communication.ServiceNotAvailableException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class KerberosConnector extends CoreObject implements Connectable {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.core.Connectable#isAvailable()
   */
  public void checkAvailability() throws ServiceNotAvailableException {
    try {
      new Login("mscholz", "bla");
    } catch (CommunicationException e) {
      throw new ServiceNotAvailableException(e);
    }
  }

  /**
   * TODO: 
   *
   * @param username TODO
   * @param password TODO
   *
   * @throws UnvalidUserException TODO
   * @throws CommunicationException TODO
   */
  public void validate(String username, String password)
    throws UnvalidUserException, CommunicationException {
    new Login(username, password);
  }
}
