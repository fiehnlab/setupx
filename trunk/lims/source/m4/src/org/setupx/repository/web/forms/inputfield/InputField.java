/**
 * ============================================================================ File:    InputField.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.Session;

import org.setupx.repository.core.communication.ncbi.local.NCBIEntry;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.Controller;
import org.setupx.repository.web.forms.FormContainer;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield;
import org.setupx.repository.web.forms.page.Page;
import org.setupx.repository.web.forms.relation.AbstractRelation;
import org.setupx.repository.web.forms.relation.Relation;
import org.setupx.repository.web.forms.relation.RelationKingdom;
import org.setupx.repository.web.forms.restriction.Restriction;
import org.setupx.repository.web.forms.validation.ValidationAnswer;
import org.setupx.repository.web.forms.validation.ValidationException;


/**
 * Abstract Inputfield.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @hibernate.subclass
 */
public abstract class InputField extends FormObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Object value = "";
  private Restriction restriction;
  private String label;
  private String questionString = DEFAULT_LABEL;

  /**
   * @label answer from the validation created when the validation was executed (in case of setValue)
   */
  private ValidationAnswer validationAnswer = null;

  /**
   * @label relations between this object and an other form object
   */
  private AbstractRelation[] relations = new AbstractRelation[0];
  private boolean requiered = true;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String IMG_PLUS = "<img  border='0' src=\"pics/plus.gif\">";
  public static final String IMG_MINUS = "<img  border='0' src=\"pics/minus.gif\">";
  public static final String iconDetailIMG = "<img  border='0' src=\"pics/details.gif\">";
  public static final String iconJumpIMG = iconDetailIMG; //"<img  border='0' src=\"pics/jump.gif\">";
  public static final String iconActiveIMG_ACTIVATE = "<img  border='0' src=\"pics/active_no.gif\">";
  public static final String iconActiveIMG_DEACTIVATE = "<img  border='0' src=\"pics/active_yes.gif\">";
  public static final String IMG_AUTOFILL = "<img  border='0' src=\"pics/txt.gif\">";
  private static int fieldUID = 0;

  {
    this.setValue(DEFAULT_LABEL);
    this.label = DEFAULT_LABEL;
    this.setName(createUName());
    this.validate();
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new InputField object.
   */
  public InputField() {
    this("...", "...");
  }

  /**
   * Creates a new InputField object.
   */
  public InputField(String question, String description) {
    super(null, description, "");
    // the name is auto set to a generated UID
    setQuestion(question);
    setDescription(description);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * set the question of this inputfiled
   *
   * @param question the question of this inputfiled
   */
  public void setQuestion(String question) {
    this.questionString = question;
  }

  /**
   * @hibernate.property   (non-Javadoc)
   *
   * @see org.setupx.repository.web.forms.inputfield.InputField#getQuestion()
   */
  public String getQuestion() {
    return questionString;
  }

  /**
   * @return the relations to other FormObject
   */
  public final AbstractRelation[] getRelations() {
    return relations;
  }

  /**
   * TODO: 
   *
   * @param set TODO
   */
  public void setRelationsHibernate(Set set) {
    AbstractRelation[] interfaces = new AbstractRelation[set.size()];

    Iterator iterator = set.iterator();
    int i = 0;

    while (iterator.hasNext()) {
      interfaces[i] = (AbstractRelation) iterator.next();
      //debug("updating relation after setRelationsHibernate(" + this.getValue() + ")");

      try {
        interfaces[i].update("" + this.getValue());
      } catch (Exception e) {
        e.printStackTrace();
      }

      i++;
    }

    this.setRelations(interfaces);
  }

  /**
   * @hibernate.set
   * @hibernate.collection-key column="INPUTFIELD"
   * @hibernate.collection-one-to-many  class="org.setupx.repository.web.forms.relation.AbstractRelation"
   */
  public Set getRelationsHibernate() {
    Set set = new HashSet();
    AbstractRelation[] relationInterfaces = this.getRelations();

    for (int i = 0; i < relationInterfaces.length; i++) {
      set.add(relationInterfaces[i]);
    }

    return set;
  }

  /**
   * @param requiered
   */
  public void setRequiered(boolean requiered) {
    this.requiered = requiered;
  }

  /**
   * by default all the fields are requiered - just in case you do not need the information you can set it as non requiered.
   *
   * @hibernate.property
   */
  public boolean isRequiered() {
    return requiered;
  }

  /**
   * sets the restriction used for the validation of this inputfiled
   * 
   * <p>
   * and sets the <b>reverse link inside the restriction</b> to this object!.
   * </p>
   * 
   * <p>
   * everytime a new restriction is set - the <i>validation is beeing executed</i>.
   * </p>
   *
   * @param restriction restriction used for the validation of this inputfiled
   */
  public void setRestriction(Restriction new_restriction) {
    // the new one is not null
    if (new_restriction != null) {
      // in case there is a restriction && the restriction is not a new one && restrictions are saved  
      if ((this.restriction != null) && (this.restriction.uoid == new_restriction.uoid) && (this.restriction.uoid != 0)) {
        return;
      }

      // setting the new one
      this.restriction = new_restriction;

      validationInternal();
    } else {
      // this is setting the current restriction to null
      this.restriction = new_restriction;
    }
  }

  /**
   * restriction used for the validation of this inputfiled
   *
   * @return restriction used for the validation of this inputfiled
   *
   * @hibernate.many-to-one column="RESTRICTION" class = "org.setupx.repository.web.forms.restriction.Restriction"
   */
  public Restriction getRestriction() {
    if (this.restriction == null) {
      // warning(this, "there is no restriction for this inputfield");
      return restriction;
    } else {
      return restriction;
    }
  }

  /**
   * Sets the value of this object.
   * 
   * <p>
   * the object can be of <b>any </b>type - meens that the object must be casted when get is called.
   * </p>
   * Before the value is changed, the <code>value</code> is <code>string</code> compared and the value will only be changed if it is not the old string. <br> In case that it is an other type of object, it is always set.
   * 
   * <p>
   * <b>the method might not be overwritten</b> Method is not <code>final </code> anymore just for SpeciesInputfield and in the multifield.
   * </p>
   * 
   * <p>
   * The multifiled itself can not contain any values itself - but in case the Multifield will get  a String as value it will adjust the number of elements inside itself to the matching number  and set the value from each line into each of the values.
   * </p>
   *
   * @param object the object will be stored in this object
   *
   * @see SpeciesInputfield
   */
  public void setValue(Object object) {
    if (object == null) {
      this.setValue("");

      return;
    }

    // in case it is a non modified string - nothing will be changed or checked 
    try {
      if (((String) this.getValue()).compareTo((String) object) == 0) {
        // value did not change - so leave it and stop any further processing
        return;
      }
    } catch (ClassCastException e) {
      // uuuops - it wasnt a string ...
      // so keep going
    }

    // the actual setting of the value
    this.value = object;

    this.updateRelations(object);
    this.validationInternal();
  }

  /**
   * the object stored in this object
   * 
   * <p>
   * <b>don t forget to <code>CAST</code> it to the desired type</b>
   * </p>
   * 
   * <p>
   * not <code>final</code> anymore - only for the PromtIDLabel
   * </p>
   *
   * @return the object stored in this object
   *
   * @hibernate.property type = "java.lang.String"
   *
   * @see DynamicLabel#getValue()
   */
  public Object getValue() {
    return this.value;
  }

  /**
   * creates a cloned <code>copy</code> of this object.
   * 
   * <p>
   * take care that the reverse reference back to the <code>parent</code> object is beeing replaced too.
   * </p>
   * 
   * <p>
   * <B>WARNING:</B> if there are relations to this object, they are <b>NOT</B> cloned.
   * </p>
   *
   * @return the new instance
   *
   * @throws CloneException unable to clone this object
   */
  public abstract InputField cloneField() throws CloneException;

  /**
   * the counter for the inputfields -
   * <pre>the last inputfields is maxFieldName()-1</pre>
   */
  public static int maxFieldName() {
    return fieldUID;
  }

  /**
   * set the relations of this object
   *
   * @param relations Relations to activate other objects
   */
  final public void setRelations(AbstractRelation[] relations) {
    //debug("setting relations: " + relations.length);
    this.relations = relations;
  }

  /**
   * add a relation
   *
   * @param addedRelation the new relation which will be added
   */
  public void addRelation(AbstractRelation addedRelation) {
    if (this.getRelations() == null) {
      this.setRelations(new AbstractRelation[0]);
    }

    // the new one
    AbstractRelation[] newRelations = new AbstractRelation[this.getRelations().length + 1];

    // fill new one 
    for (int i = 0; i < this.getRelations().length; i++) {
      newRelations[i] = this.getRelations()[i];
    }

    newRelations[newRelations.length - 1] = addedRelation;
    addedRelation.update((String) this.getValue());

    this.setRelations(newRelations);
    this.validationInternal();
  }

  /**
   * creates an HTML anchor - can be added everywhere
   *
   * @return HTML anchor
   */
  public final String createAnchor() {
    return "<a name=\"" + this.getName() + "\"/>";
  }

  /**
   * changes the backgroundcolor of this item.
   * 
   * <p>
   * depending on isValid, isRequiered, hasRestriction
   * </p>
   *
   * @return for example <code>style=background-color:#FF6C6C</code>
   */
  public final String createBackgroudcolor() {
    if (this instanceof SubmitButton) {
      return " style=\"background-color:#87FC96\"  ";
    }

    if ((!this.isValid()) && this.isRequiered() && hasRestriction()) {
      return " style=\"background-color:#FF6C6C\"  ";
    } else {
      return "";
    }
  }

  /**
   * values of this object packed in a hashtable
   *
   * @return values for this object
   */
  public Hashtable getAttributes() {
    Hashtable hashtable = new Hashtable();
    hashtable.put("label", this.label);
    hashtable.put("value", this.getValue());
    hashtable.put("questionString", this.getQuestion());
    hashtable.put("valid", "" + isValid());
    hashtable.putAll(super.getAttributes());

    return hashtable;
  }

  /**
   * creates a linked img to itself including an anchor
   *
   * @return
   */
  public String createLinkedIMG() {
    // <a href="bla#Zieldef">Ziel f�r Verweise definieren</a>
    return ValidationAnswer.toLink(this.getName(), (String) this.getValue(), iconJumpIMG, "Press this button to jump to inputfield: " + this.getQuestion());
  }

  /**
   * create a standard remove button
   *
   * @return html remove button
   */
  public final String createRemoveButton() {
    return createRemoveButton("This item can be removed. Press if you want to remove this item.");
  }

  /**
   * TODO: 
   *
   * @param msg TODO
   *
   * @return TODO
   */
  public final String createRemoveButton(String msg) {
    return createRemoveButton(msg, removeIMG, this.parentPage().getInternalPosition());
  }

  /**
   * TODO: 
   *
   * @param msg Message to be shown as ALT
   * @param img the image
   *
   * @return the button
   */
  public String createRemoveButton(String msg, String img) {
    return createRemoveButton(msg, img, this.parentPage().getInternalPosition());
  }

  /**
   * TODO: 
   *
   * @param msg Message to be shown as ALT
   * @param img the image
   * @param target_page the position of the Page
   *
   * @return the button
   *
   * @see FormObject#getPosThis()
   */
  public String createRemoveButton(String msg, String img, int target_page) {
      /*
      Logger.debug(this,"createRemoveButton:  removable:" 
              + (this instanceof Removable)  + "  " 
              + ((Removable) this).getRemovable() + "  " 
              + (this.getParent().getFields().length ));
      */
      
    if ((this instanceof Removable && ((Removable) this).getRemovable() && (this.getParent().getFields().length != 1))) {
      StringBuffer buffer = new StringBuffer();
      buffer.append("form?");
      buffer.append(this.getName());
      buffer.append("=");
      buffer.append("");
      buffer.append("&");
      buffer.append(WebConstants.PARAM_FORM_TARGET);
      buffer.append("=");
      buffer.append(target_page);
      buffer.append("&");
      buffer.append("remove");
      buffer.append("=");
      buffer.append(this.getName());

      // new - anchor on parent field
      buffer.append("#" + this.getParent().getName());

      return "<a href=\"" + buffer.toString() + "\"  title=\"" + msg + "\">" + img + "</a>  ";
    } else {
      return "";
    }
  }

  /**
   * @return
   *
   * @deprecated use createBackgroudColor();
   */
  public final String createRequieredNote() {
    /*if (isRequiered() && (this.getRestriction() != null)) {
       return "<span class=\"error\">" + IMG_REQUIERED + "</span>";
       }
     *
     */
    return "";
  }

  /**
   * @return
   */
  public String createErrorMSG() {
    StringBuffer buffer = new StringBuffer();

    // error message
    if ((this.validate() != null) && (this.validate().length() > 1)) {
      buffer.append("<!--  error message -->");
      buffer.append("\n<br>");
      //buffer.append(IMG_ERR);
      buffer.append("<span class=\"error\">" + this.validate() + "</span>");
    }

    return buffer.toString();
  }

  /**
   * return the page this item is located at - does not care about if there are more elements containing each other.
   *
   * @return the parent page
   */
  public Page parentPage() {
    FormObject formObject = this;

    do {
      formObject = formObject.getParent();
      //log("parentPage() - checking:  " + formObject);
    } while ((formObject != null) && !(formObject instanceof Page));

    //log("parentPage() - result:  " + formObject);
    return (Page) formObject;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#persistenceChilds(org.hibernate.Session, boolean)
   */
  public void persistenceChilds(Session session, boolean createIt)
    throws PersistenceActionException {
    if (this.getRestriction() != null) {
      this.getRestriction().update(session, createIt);
    }

    for (int i = 0; i < this.getRelations().length; i++) {
      this.getRelations()[i].update(session, createIt);

      // relations are beeing updted after they have been saves / updated
      // warning(this, "THERE WAS AN UPDATE OF THE RELATION HERE _ BUT I DONT KNOW WHY? TOOK IT OUT!");
      // debug("updating relation with value " + this.getValue() + " - after the relation has been saved.");
      this.getRelations()[i].update("" + this.getValue());

      // checking the relation
      if (this.getRelations()[i].getUOID() == 0) {
        throw new RuntimeException("relations was not correctly stored.");
      }

      // 
      if (this.getRelations()[i] instanceof Relation) {
        Relation relation = (Relation) this.getRelations()[i];
        FormObject[] formObjects = relation.getRelationTargets();

        for (int j = 0; j < formObjects.length; j++) {
          FormObject object = formObjects[j];

          if (object.getUOID() == 0) {
            throw new RuntimeException("object in relation was not correctly stored.");
          }
        }
      }
    }

    super.persistenceChilds(session, createIt);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#toStringHTML()
   */
  public String toStringHTML() {
    if (this.isActive()) {
      return ("<B>" + this.getQuestion() + "</B>: " + this.getValue());
    }

    return "";
  }

  /**
   * updateing all relations.
   *
   * @param object
   */
  public final void updateRelations(Object object) {
    for (int i = 0; i < relations.length; i++) {
      // HOTFIX - for NCBI Kingdom relations
      if (relations[i] instanceof RelationKingdom) {
        NCBIEntry ncbiEntry = ((NBCISpeciesInputfield) this).entry;

        // getting NCBI entry 
        ((RelationKingdom) relations[i]).update(ncbiEntry);
      } else {
        relations[i].update((String) object);
      }
    }
  }

  /**
   * TODO: 
   *
   * @param inputfields TODO
   *
   * @return TODO
   */
  public static long[] mapToIDs(Collection inputfields) {
    Iterator iterator = inputfields.iterator();
    long[] ids = new long[inputfields.size()];
    int i = 0;

    while (iterator.hasNext()) {
      InputField element = (InputField) iterator.next();
      ids[i] = element.getUOID();
      i++;
    }

    return ids;
  }

  /**
   * The method returns just a <code>generated</code> validation result, which is created when the setValue  method is called
   * 
   * <p>
   * Just in case that the value changes when calling the setValue-Method the validation is  processed. This avoids checks when the value is <i>not</i> changed.
   * </p>
   * 
   * <p>
   * In case that the controlling restriction is a <code>LexiRestriction</code> the suggestions will be formated as a link ???TODO
   * </p>
   * 
   * <p>
   * if this is an instance of the not of a InoutFiled the <code>super</code> validation will be used
   * </p>
   *
   * @see org.setupx.repository.web.forms.FormObject#validate()
   * @see FormContainer
   */
  public String validate() {
    if (this instanceof FormContainer) {
      return super.validate();
    }

    if (!(this instanceof InputField)) {
      return super.validate();
    }

    if ((this.getRestriction() == null) || (this.validationAnswer == null)) {
      return null;
    }

    // the validation is just used when the set method is called ... 
    return this.validationAnswer.getLink().toString();
  }

  /**
   * @return true if there is a restriction
   */
  protected final boolean hasRestriction() {
    return (this.getRestriction() != null);
  }

  /**
   * @return a <code>unique</code> name for the fileds
   */
  private static String createUName() {
    String name = Controller.FIELDSTART + fieldUID;
    fieldUID++;

    return name;
  }

  /**
   * validation - <b>it is only called by the setValue-Method </B> and in case the restriction is changed or set.  determines the restriction and validates the object with it.
   * 
   * <p>
   * the result of this validation is then stored in the <code>validationanswer</code>
   * </p>
   *
   * @see ValidationAnswer
   * @see Restriction#validateObject()
   */
  private final void validationInternal() {
    // in case that there are no restriction
    if (this.getRestriction() == null) {
      this.validationAnswer = null;

      return;
    }

    // response form validation
    ValidationAnswer result = null;

    try {
      result = getRestriction().validateObject(this);
    } catch (ValidationException e1) {
        log("validation not succesful.");
        //warning(e1);
    }

    this.validationAnswer = result;
  }
  

  
  public Hashtable getExportElements(){
      Hashtable hashtable = super.getExportElements();
      hashtable.put("value", this.getValue());
      return hashtable;
  }  
  
  public Hashtable getExportAttributes() {
    Hashtable hashtable = super.getExportAttributes();
    hashtable.put("question", this.getQuestion());
    return hashtable; 
}
}
