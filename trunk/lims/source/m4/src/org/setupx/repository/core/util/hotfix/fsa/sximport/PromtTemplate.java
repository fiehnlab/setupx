/**
 * ============================================================================ File:    PromtTemplate.java Package: org.setupx.repository.core.util.hotfix.fsa.sximport cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.hotfix.fsa.sximport;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import java.util.StringTokenizer;


class PromtTemplate extends Template {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected PromtTemplate(File directory) throws IOException {
    File file = new File(directory.toString() + File.separator + "info");
    String content = org.setupx.repository.core.util.File.getFileContent(file);

    StringTokenizer stringTokenizer = new StringTokenizer(content, "\n");

    // empty
    stringTokenizer.nextToken();
    stringTokenizer.nextToken();

    // clazzes
    boolean loop = true;

    while (loop && stringTokenizer.hasMoreElements()) {
      String line = stringTokenizer.nextToken();
      System.out.println("\"" + line + "\"");

      //StringTokenizer stringTokenizer2 = new StringTokenizer(line, "\t");
      String oldclassID = line.substring(0, line.indexOf('\t'));
      String sxID = line.substring(line.indexOf('\t') + 1, line.length() - 1);
      ClassTemplate classTemplate = new ClassTemplate(parse(sxID), oldclassID);
      File[] files = new File(directory.toString() + File.separator + oldclassID).listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
              return (name.endsWith("xml") || name.endsWith("m1s"));
            }
          });

      if (files != null) {
        System.out.println("number of samples in this class [" + oldclassID + "] : " + files.length);

        for (int i = 0; i < files.length; i++) {
          // samples
          classTemplate.addChild(new SampleTemplate(files[i]));
        }

        this.addChild(classTemplate);
      } else {
        System.out.println("illegal class " + oldclassID);
      }
    }
  }
}
