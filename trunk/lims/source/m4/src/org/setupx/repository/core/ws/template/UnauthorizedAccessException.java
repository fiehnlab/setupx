package org.setupx.repository.core.ws.template;

/**
 * TEMPLATE FOR GENERATION the webservice - look at the real implementaions
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class UnauthorizedAccessException extends Exception {
  /**
   * Creates a new UnauthorizedAccessException object.
   */
  public UnauthorizedAccessException() {
    super();
  }

  /**
   * Creates a new UnauthorizedAccessException object.
   *
   * @param arg0 
   */
  public UnauthorizedAccessException(String arg0) {
    super(arg0);
  }

  /**
   * Creates a new UnauthorizedAccessException object.
   *
   * @param arg0 
   */
  public UnauthorizedAccessException(Throwable arg0) {
    super(arg0);
  }

  /**
   * Creates a new UnauthorizedAccessException object.
   *
   * @param arg0 
   * @param arg1 
   */
  public UnauthorizedAccessException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }
}
