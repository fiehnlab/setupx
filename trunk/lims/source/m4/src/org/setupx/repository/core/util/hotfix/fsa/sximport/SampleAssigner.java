/**
 * ============================================================================ File:    SampleAssigner.java Package: org.setupx.repository.core.util.hotfix.fsa.sximport cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.hotfix.fsa.sximport;

import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionStoreException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.web.forms.PromtCreateException;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.Sample;
import org.setupx.repository.web.forms.inputfield.multi.SampleScheduledException;

import java.io.File;
import java.io.IOException;

import java.util.Iterator;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class SampleAssigner {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Promt promt;
  private PromtTemplate promtTemplate;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new SampleAssigner object.
   *
   * @param promtID 
   *
   * @throws PersistenceActionFindException 
   * @throws PersistenceActionStoreException 
   * @throws PromtCreateException 
   * @throws SampleScheduledException 
   */
  public SampleAssigner(int promtID) throws PersistenceActionFindException, PersistenceActionStoreException, PromtCreateException, SampleScheduledException {
    // load promt
    this.promt = Promt.load(promtID);

    // check id
    if (promt.getUOID() != promtID) {
      throw new PersistenceActionFindException("wrong promt was loaded.");
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public long getPromtID() {
    return this.promt.getUOID();
  }

  /**
   * assign each class template to the real class.
   * 
   * <p>
   * including the assignment of the samples to those classes then.
   * </p>
   * 
   * <p>
   * <code>Promt</code> is beeing saved after assignment was sucessful. <br>
   * </p>
   *
   * @throws PersistenceActionUpdateException
   */
  public void assign() throws PersistenceActionUpdateException {
    // take each class from mappingfile
    Iterator iterator = promtTemplate.getChilds().iterator();

    // take samples information from mappingfile 
    while (iterator.hasNext()) {
      try {
        // template
        ClassTemplate classtemplate = (ClassTemplate) iterator.next();

        // real class
        Clazz realClazz = this.getRealClazz(classtemplate.getID());

        Iterator iteratorTemplates = classtemplate.getChilds().iterator();

        /*
        
                                                                                                        // assign them to real class
                                                                                                        Logger.debug(this, "0. number of samples in templates: " + classtemplate.getChilds().size());
        
                                                                                                        // getting rid of the old stuff
                                                                                                        Iterator iteratorSamples = realClazz.getSamples().iterator();
                                                                                                        while (iteratorSamples.hasNext() && realClazz.size() > 2) {
                                                                                                            Sample element = (Sample) iteratorSamples.next();
                                                                                                            realClazz.remove(element);
                                                                                                        }
        
                                                                                                        // makes a lot of samples be unrelated - TODO
                                                                                                        //realClazz.setFields(formObjects);
        
                                                                                                        while (classtemplate.getChilds().size() < realClazz.size()){
                                                                                                            Logger.debug(this, "1. number of samples in classes: " + realClazz.size());
                                                                                                            realClazz.expand(classtemplate.getChilds().size() - realClazz.size());
                                                                                                            Logger.debug(this, "2. number of samples in realclasses after resize: " + realClazz.size());
                                                                                                        }
                                                                                                        Logger.debug(this, "3. done ");
        
                                                                                                        Iterator iterator3 = realClazz.getSamples().iterator();
         */
        Sample[] samples = new Sample[classtemplate.getChilds().size()];

        for (int i = 0; i < samples.length; i++) {
          SampleTemplate sampleTemplate = (SampleTemplate) iteratorTemplates.next();
          samples[i] = new Sample();
          samples[i].setComment(sampleTemplate.getName());
        }

        realClazz.setFields(samples);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    try {
      this.promt.update(true);
    } catch (PersistenceActionUpdateException e) {
      throw e;
    }
  }

  /**
   * TODO: 
   *
   * @param file TODO
   *
   * @throws IOException TODO
   */
  public void loadMapping(File file) throws IOException {
    this.promtTemplate = new PromtTemplate(file);
  }

  /**
   * TODO: 
   *
   * @param id TODO
   *
   * @return TODO
   *
   * @throws Exception TODO
   */
  private Clazz getRealClazz(int id) throws Exception {
    Clazz[] clazzs = this.promt.getClazzes();

    for (int i = 0; i < clazzs.length; i++) {
      Clazz clazz = clazzs[i];
      Logger.debug(this, "compare: " + clazz.getUOID() + "  " + id);

      if (clazz.getUOID() == id) {
        return clazz;
      }
    }

    throw new Exception("unable to find class " + id);
  }
}
