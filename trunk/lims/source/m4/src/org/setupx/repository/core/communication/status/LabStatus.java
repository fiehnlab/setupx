package org.setupx.repository.core.communication.status;


import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.server.persistence.PersistenceActionFindException;


/**
 * @hibernate.subclass
 */
public class LabStatus extends PersistentStatus {

    public LabStatus(){
        super();
    }
    
    public LabStatus(long sampleID, UserDO userDO, String message) throws StatusPersistenceException {
        super(sampleID, userDO, message);
    }

    public String createDisplayString() {
        return "lab work";
    }

    public PersistentStatus createClone() {
        try {
            return new LabStatus(this.sampleID, this.getUser(getUserID()), this.getMessage());
        } catch (StatusPersistenceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (PersistenceActionFindException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
