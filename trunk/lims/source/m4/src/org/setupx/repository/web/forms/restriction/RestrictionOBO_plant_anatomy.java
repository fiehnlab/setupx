/**
 * ============================================================================ File:    RestrictionOBO_plant_anatomy.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.Config;
import org.setupx.repository.Settings;
import org.setupx.repository.core.communication.ontology.obo.SourceDefinition;


/**
 * Restriction for Plant Anatomy.
 * 
 * <p>
 * Sourcefile: <a href="http://www.plantontology.org/download/download.html">http://www.plantontology.org/download/download.html</a>
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @hibernate.subclass
 *
 * @see org.setupx.repository.web.forms.restriction.OBORestriction
 */
public class RestrictionOBO_plant_anatomy extends OBORestriction {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** the source file containin the ontology in valid OBO Format. */
  String source = Settings.getValue("data.obo.plantanatomy", Config.DIRECTORY_OBO + "po_anatomy.obo");


  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#setSource()
   */
  public void setSource(String _source) {
    this.source = _source;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#getSource()
   */
  public String getSource() {
    return this.source;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#cloneRestriction()
   */
  public Restriction cloneRestriction() {
    return new RestrictionOBO_plant_anatomy();
  }

  
  public SourceDefinition getSourceDefinition() {
      return new SourceDefinition("Plant Ontology (Anatomy)", "http://www.plantontology.org/amigo/go.cgi?action=plus_node&depth=1&search_constraint=terms&query=PO:0009011");
  }
}
