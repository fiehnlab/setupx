package org.setupx.repository.server.workflow;

/**
 * Configuration for a single Workflow.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class WorkFlowConfiguration {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public String comment = null;
  public String id = null;
  public int[] wfElementIds = null;

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @return TODO
   *
   * @see Object#toString()
   */
  public String toString() {
    String idSting = "";

    for (int i = 0; i < wfElementIds.length; i++) {
      idSting = idSting.concat("" + wfElementIds[i]);

      if (i < (wfElementIds.length - 1)) {
        idSting = idSting.concat(", ");
      }
    }

    return this.id + "  ( " + comment + " ) containing ids: " + wfElementIds.length + "  (" + idSting + ")";
  }
}
