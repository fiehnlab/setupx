/**
 * ============================================================================ File:    UserExistsRestriction.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.core.communication.ldap.LDAPConnector;
import org.setupx.repository.core.user.UserRemote;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.validation.ValidationAnswer;
import org.setupx.repository.web.forms.validation.ValidationException;

import org.hibernate.Session;


/**
 * Restriction that a User exists.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.5 $
 *
 * @hibernate.subclass
 */
public class UserExistsRestriction extends Restriction {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // the fields are only filled with default valued ... 
  private InputField email = new StringInputfield();
  private InputField info = new StringInputfield();
  private InputField username = new StringInputfield();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new UserExistsRestriction object.
   *
   * @param name 
   * @param remote_username 
   * @param remote_info 
   * @param email 
   */
  public UserExistsRestriction(InputField name, InputField remote_username, InputField remote_info, InputField email) {
    super();
    this.username = remote_username;
    this.info = remote_info;
    this.email = email;
  }

  /**
   * Creates a new UserExistsRestriction object.
   */
  public UserExistsRestriction() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * The users email address.
   *
   * @param email an inputfiled containing the emailaddress
   */
  public void setEmail(InputField email) {
    this.email = email;
  }

  /**
   * @hibernate.many-to-one class = "org.setupx.repository.web.forms.inputfield.InputField"
   */
  public InputField getEmail() {
    return email;
  }

  /**
   * TODO: 
   *
   * @param info TODO
   */
  public void setInfo(InputField info) {
    this.info = info;
  }

  /**
   * @hibernate.many-to-one class = "org.setupx.repository.web.forms.inputfield.InputField"
   */
  public InputField getInfo() {
    return info;
  }

  /**
   * TODO: 
   *
   * @param username TODO
   */
  public void setUsername(InputField username) {
    this.username = username;
  }

  /**
   * @hibernate.many-to-one class = "org.setupx.repository.web.forms.inputfield.InputField"
   */
  public InputField getUsername() {
    return username;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#cloneRestriction()
   */
  public Restriction cloneRestriction() {
    err(this, new NotClonableException("TODO"));

    return null;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.CoreObject#persistenceChilds(org.hibernate.Session, boolean)
   */
  public void persistenceChilds(Session session, boolean createIt)
    throws PersistenceActionException {
    // not updating - cause that is gonna update me again !
    try {
      this.getEmail().update(session, createIt);
      this.getInfo().update(session, createIt);
      this.getUsername().update(session, createIt);
    } catch (Exception e) {
      err(this, e);
    }

    super.persistenceChilds(session, createIt);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#validateObject()
   */
  public ValidationAnswer validateObject(InputField field)
    throws ValidationException {
    String userid = (String) field.getValue();

    if (userid == null) {
      return new ValidationAnswer(field, "The user id " + userid + " does not exist in the system.");
    }

    if (userid.length() <= 1) {
      return new ValidationAnswer(field, "The user id " + userid + " does not exist in the system.");
    }

    UserRemote userRemote = null;

    try {
      // checking if the user exists
      userRemote = LDAPConnector.receive(userid);

      // fill the info fields with information
      this.username.setValue(userRemote.getDisplay());

      String information = (String) Util.convertString(userRemote.getUserinformation(), 15);
      log(this, "LDAP information set: " + information);
      log(this, "LDAP: before setting the value: " + this.info.getValue());
      this.info.setValue(information);
      log(this, "LDAP: after setting the value: " + this.info.getValue());

      //log(this, this.info.getParent().toStringTree());
      this.email.setValue(userRemote.getMailAddress().toString());

      // in case the user was found - the answer is null
      return null;
    } catch (Exception e) {
      warning(e);
      // resetting all information in the related field.
      email.setValue("");
      info.setValue("");
      username.setValue("");

      this.username.setValue("unknown");
      this.info.setValue("unknown");
      this.email.setValue("unknown");

      return new ValidationAnswer(field, "Unable to find user " + userid + " .");
    }
  }
}
