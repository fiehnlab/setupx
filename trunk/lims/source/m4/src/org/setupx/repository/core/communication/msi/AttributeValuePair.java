package org.setupx.repository.core.communication.msi;

import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.export.XMLConvertException;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;

public class AttributeValuePair {

    private String value = "";
    private String label1 = "";
    private String label2 = "";
    private long msiID = 0L;

    public MSIAttribute getMSIAttribute() throws MSIAttributeNotAvailableException{
        if (this.getMsiID() != 0){
            try {
                return (MSIAttribute)CoreObject.persistence_loadByID(MSIAttribute.class, CoreObject.createSession(), this.getMsiID());
            } catch (PersistenceActionFindException e) {
                throw new MSIAttributeNotAvailableException(e);
            }
        } else {
            throw new MSIAttributeNotAvailableException("not an msi attribute");
        }
    }

    public String getExtension(){
        try {
            String extension = this.getMSIAttribute().getExtension();
            if (extension == null || extension.compareTo("null") == 0) throw new MSIAttributeNotAvailableException("no extension available.");
            return extension;
        } catch (MSIAttributeNotAvailableException e) {
            return "";
        }
    }


    public String toString(){
        try {
            return this.getLabel1() + " " + this.getLabel2() + " " + this.getValue() + " " + this.getExtension() + " " ;
        } catch (Exception e){
            return "some/all values are null";
        }
    }


    public String getValue() {
        return value;
    }
    public String getLabel1() {
        return label1;
    }
    public String getLabel2() {
        return label2;
    }
    public long getMsiID() {
        return msiID;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public void setLabel1(String label1) {
        this.label1 = label1;
    }
    public void setLabel2(String label2) {
        this.label2 = label2;
    }
    public void setMsiID(long msiID) {
        this.msiID = msiID;
    }

    public Node toXML(Document document) throws XMLConvertException {
//      try {
        Logger.debug(this, "converting " + this.getMsiID() );

        Element element = null;
        try {
            element = document.createElement("attribute");
            element.setAttribute("name", this.getMSIAttribute().getMsiLabel());
            element.setAttribute("isMSI", "true");
        } catch (MSIAttributeNotAvailableException e) {
            element = document.createElement("attribute");
            element.setAttribute("isMSI", "false");
            element.setAttribute("name", this.getLabel1());
        }

        if (this.getExtension() != null && this.getExtension().compareTo("") !=0){
            element.setAttribute("extension", this.getExtension());
        }
        element.setAttribute("label1", this.getLabel1());
        element.setAttribute("label2", this.getLabel2());

        element.appendChild(document.createTextNode(this.getValue()));

        try {
            Element msi = document.createElement("msi");
            msi.setAttribute("comment",this.getMSIAttribute().getComment());
            msi.setAttribute("descr",this.getMSIAttribute().getDescribtion());
            msi.setAttribute("extension",this.getMSIAttribute().getExtension());
            msi.setAttribute("label",this.getMSIAttribute().getMsiLabel());
            msi.setAttribute("question",this.getMSIAttribute().getQuestion());
            msi.setAttribute("submitter","" + this.getMSIAttribute().getUserID());
            
            try {
                UserDO submitter = ((UserDO)UserDO.persistence_loadByID(UserDO.class, CoreObject.createSession(), this.getMSIAttribute().getUserID()));
                msi.setAttribute("submitted_by","" + submitter.getDisplay());
            } catch (PersistenceActionFindException e) {
                e.printStackTrace();
            }
            msi.setAttribute("submitterSXID","" + this.getMSIAttribute().getUserID());
            msi.setAttribute("version",MSIAttribute.toDateString(this.getMSIAttribute().getVersion()));
            element.appendChild(msi);
        } catch (MSIAttributeNotAvailableException e) {
            Logger.debug(this, "not an MSI attribute");
        }
        Logger.debug(this, "finished converting " + this.getMsiID() );
        return element;

        /*
        } catch (Exception e) {
            Logger.warning(this, "converting " + this.getMsiID() + " failed.");
            throw new XMLConvertException("unable to create node: ", e);
        }
         */
    }

    public static AttributeValuePair[] toArray(Vector attributeVector) {
        AttributeValuePair[] result = new AttributeValuePair[attributeVector.size()]; 
        for (int i = 0; i < result.length; i++){
            result[i] = (AttributeValuePair)attributeVector.get(i);
        }
        return result;
    }

    public static Vector toList(AttributeValuePair[] array) {
        Vector result = new Vector();

        for (int i = 0; i < array.length; i++){
            result.add(array[i]);
        }
        return result;
    }
}
