/**
 * ============================================================================ File:    SubmitButtonRestriction.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.SubmitButton;
import org.setupx.repository.web.forms.validation.ValidationAnswer;
import org.setupx.repository.web.forms.validation.ValidationException;


/**
 * REstriction specialy made for the submitbutton - to validate if the submit button has been pressed<br> checks if the button has been pressed or not
 *
 * @hibernate.subclass
 *
 * @see org.setupx.repository.web.forms.SubmitButton
 */
public class SubmitButtonRestriction extends Restriction {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   */
  public SubmitButtonRestriction() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#cloneRestriction(org.setupx.repository.web.forms.inputfield.InputField)
   */
  public Restriction cloneRestriction() {
    return new SubmitButtonRestriction();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#validateObject()
   */
  public ValidationAnswer validateObject(InputField field)
    throws ValidationException {
    //log("validate: " + this.getField().getValue() + "  converted to boolean: " + Boolean.valueOf(this.getField().getValue().toString()));
    if (Boolean.valueOf(field.getValue() + "").booleanValue()) {
      return null;
    } else {
      return new ValidationAnswer(field, "please check the box when you are done with the form.");
    }
  }
}
