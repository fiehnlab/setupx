/**
 * ============================================================================ File:    StringRestriction.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.core.communication.leco.LecoACQFile;
import org.setupx.repository.core.communication.vocabulary.VocabularyChecker;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.Link;
import org.setupx.repository.web.forms.validation.ValidationAnswer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Regular-Expression-Restriction for a String.
 * 
 * <p>
 * The String is validated against a Pattern in RegEx
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.17 $
 *
 * @hibernate.subclass
 *
 * @see java.util.regex.Pattern
 */
public class StringRestriction extends Restriction {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String patternString;
  private boolean checkSpelling = false;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * creates a new StringRestriction.
   *
   * @see java.util.regex.Pattern the regEx that must be matched by the String
   */
  public StringRestriction(String regEx, boolean checkVoc) {
    setPattern(regEx);
    setCheckSpelling(checkVoc);
  }

  /**
   * Creates a new StringRestriction object.
   */
  public StringRestriction() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * activates a test for the correct spelling
   *
   * @param checkSpelling true if test will be active spelling
   */
  public void setCheckSpelling(boolean checkVoc) {
    this.checkSpelling = checkVoc;
  }

  /**
   * activates a test for the correct spelling
   *
   * @return if spell check is activated
   *
   * @hibernate.property
   */
  public boolean getCheckSpelling() {
    return checkSpelling;
  }

  /**
   * @param regEx the pattern used by this restirction
   */
  public void setPattern(String regEx) {
    this.patternString = regEx;
  }

  /**
   * @return the current pattern, set for this test
   *
   * @hibernate.property
   */
  public String getPattern() {
    //debug(this, "pattern: " + this.patternString);
    return patternString;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#cloneRestriction()
   */
  public Restriction cloneRestriction() {
    return new StringRestriction(this.patternString, this.checkSpelling);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#validateObject(java.lang.Object)
   */
  public ValidationAnswer validateObject(InputField field) {
    Matcher m = null;
    String stringValue = (String) field.getValue();

    if (stringValue == null) {
      stringValue = "";
    }

    Pattern pattern = Pattern.compile(this.getPattern());
    m = pattern.matcher(stringValue);
    debug("validation result: " + m.matches());

    if (m.matches()) {
      // now checking if it is spelled correctly 
      debug(this, "pattern matches but now testing spelling" + this.checkSpelling);

      if (this.checkSpelling) {
        String[] response = VocabularyChecker.check4suggestion(stringValue);
        debug(this, "response from validationcheck inside a stringpattern restriction :");
        debug(this, stringValue);

        // if empty return null otherwise a generated msg
        if (response.length == 0) {
          return null;
        }

        ValidationAnswer answer = new ValidationAnswer(field);

        for (int i = 0; i < response.length; i++) {
          if (response[i] != null) {
            answer.add(response[i]);
          }
        }

        return answer;
      }

      return null;
    } else {
      return new ValidationAnswer(new Link(field.getValue() + " does not match " + this.patternString));
    }
  }
}
