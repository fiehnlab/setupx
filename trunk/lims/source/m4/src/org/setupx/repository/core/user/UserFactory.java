/**
 * ============================================================================ File:    UserFactory.java Package: org.setupx.repository.core.user cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.user;

import java.util.Hashtable;

import javax.naming.NamingException;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.CommunicationException;
import org.setupx.repository.core.communication.kerberos.KerberosConnector;
import org.setupx.repository.core.communication.kerberos.UnvalidUserException;
import org.setupx.repository.core.communication.ldap.LDAPConnector;
import org.setupx.repository.server.persistence.PersistenceActionFindException;


/**
 * Factory creating the users
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.13 $
 */
class UserFactory extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static LDAPConnector ldapConnector = new LDAPConnector();
  private static KerberosConnector kerberosConnector = new KerberosConnector();
  public static final int LDAP = 1;
  public static final int LOCAL = 2;
  public static final int EJB = 3;
  public static final int HIBERNATE = 4;
  private static UserDO user_dataobject = null;

  // hashtable containing all users that are NO ldapusers ---> all users from extern
  private static Hashtable userHashtable = new Hashtable();
  private static LocalUserIterator iterator;
  private static Object thiz = new UserFactory();

  //private static PersonHome home;
  static {
    log(thiz, " UserFactory init");

    //intUserTable();
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // stays private
  private UserFactory() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * checks if the user is registreated in any of the connected systems
   *
   * @param username usersname
   * @param password password
   *
   * @return true if exists otherwise false
   */
  public static boolean isExistingUser(String username, String password) {
    boolean exists = false;

    try {
      findUser(username, password);
      exists = true;
    } catch (Exception e) {
      err(thiz, e);
    }

    return exists;
  }

  /**
   * creates a user and stores the user
   *
   * @param user a userDO representing a user
   *
   * @throws UserFactoryException problems while creating the user
   *
   * @deprecated ejb no longer supported
   */
  public static void createUser(UserDO user) throws UserFactoryException {
    /*
       debug(thiz, "adding " + user);
       try {
         home.create(user.username, user.getPassword(), user.mailAddress.getEmailAddress().toString(), user.username, user.username);
       } catch (RemoteException e) {
         throw new UserFactoryException(e);
       } catch (javax.ejb.CreateException e) {
         throw new UserFactoryException(e);
       }
       debug(thiz, "created object person ");
     */
  }

  /**
   * find a user - the user can be come from different sources. This method is looking for the user in the local configuration file and in the LDAP-Server
   *
   * @param username the username
   * @param password the password
   *
   * @return the user that was found
   *
   * @throws UserNotFoundException
   * @throws UserFactoryException
   * @throws WrongPasswordException
   */
  public static UserDO findUser(String username, String password)
    throws UserNotFoundException, WrongPasswordException, UserFactoryException {
    //Config.initCoreUsers();
    user_dataobject = null;

    // special case - when the password is null - check if the user is registered in the users.xml - and then get the information from LDAP
    // ---> special case
    if ((password == null) || (password.compareTo("") == 0)) {
      log(thiz, "--- special case - the user has access to the system but - information will be taken from ldap");
      user_dataobject = (UserDO) userHashtable.get(username);
      log(thiz, "userDO:" + user_dataobject);

      if (user_dataobject != null) {
        try {
          // the user is in the users.xml
          user_dataobject = LDAPConnector.receive(username);

          return user_dataobject;
        } catch (NamingException e) {
          throw new UserFactoryException(e);
        }
      }
    }

    // <--- specail case
    int userSourceID = 0;

    // check if user exists in the ejb-system
    if (isExistingUser(username, password, HIBERNATE)) {
      userSourceID = HIBERNATE;
    }
    // check whether the user exists in the local-user-storagesystem (just for external user, that do not have an local ldap account

    /*else if (isExistingUser(username, password, LOCAL)) {
       userSourceID = LOCAL;
       }
     */

    // check whether the user exists in the ldap-system
    else if (isExistingUser(username, password, LDAP)) {
      userSourceID = LDAP;
    }
    // check if user exists in the ejb-system
    else if (isExistingUser(username, password, EJB)) {
      userSourceID = EJB;
    }
    // unknown user
    else {
      throw new UserNotFoundException("user " + username + " was not found in any of the user databases");
    }

    // find the user from the defined source
    user_dataobject = findUser(username, password, userSourceID);

    return user_dataobject;
  }

  /**
   * @param userDO
   *
   * @throws UserFactoryException
   */
  public static void remove(UserDO userDO) throws UserFactoryException {
    /* FIXME
       try {
           home.findByPrimaryKey(new PersonPK(userDO.username)).remove();
       } catch (RemoteException e) {
           throw new UserFactoryException(e);
       } catch (RemoveException e) {
           throw new UserFactoryException(e);
       } catch (FinderException e) {
           throw new UserFactoryException(e);
       }*/
  }

  /**
   * @param username
   * @param password
   * @param ejb2
   *
   * @return
   */
  private static boolean isExistingUser(String username, String password, int code) {
    try {
      debug(thiz, "checking if exists on: " + code);
      findUser(username, password, code);
      debug(thiz, "found on: " + code);

      return true;
    } catch (UserNotFoundException e) {
    } catch (WrongPasswordException e) {
    } catch (UserFactoryException e) {
    }

    debug(thiz, "not found on: " + code);

    return false;
  }

  /**
   * looking for the user in the different sources. Later the user is validated - the password is checked.
   * 
   * <p>
   * possible sources are defined as parameters in this class.
   * </p>
   *
   * @param username the username to look for - if LDAP connection - it must be the same as on the LDAP system
   * @param password the password
   * @param sourceID the id defining from which source the user will be caught
   *
   * @return user that was found
   *
   * @throws UserNotFoundException unable to find a user with the defined paramters
   * @throws WrongPasswordException the user was found, but the password is wrong
   * @throws UserFactoryException
   */
  private static UserDO findUser(String username, String password, int sourceID)
    throws UserNotFoundException, WrongPasswordException, UserFactoryException {
    debug(thiz, "looking at " + sourceID + " for username: " + username);

    switch (sourceID) {
    case HIBERNATE:
      debug(thiz, "checking HIBERNATE");

      try {
        user_dataobject = UserDO.load(username, password);
      } catch (PersistenceActionFindException e4) {
        throw new UserNotFoundException(username, e4);
      }

      break;

    case LDAP:
      debug(thiz, "checking on LDAP");

      try {
        debug(thiz, "checking on kerberos");
        kerberosConnector.validate(username, password);

        debug(thiz, "user is valid");

        debug(thiz, "receiving the userinformation from the ldap-system");
        user_dataobject = LDAPConnector.receive(username);

        // cause password is not stored in ldap add it
        user_dataobject.password = password;
      } catch (NamingException e3) {
        err(thiz, e3);
        throw new UserFactoryException(e3);
      } catch (UnvalidUserException e) {
        throw new UserNotFoundException(e);
      } catch (CommunicationException e) {
        err(thiz, e);
        throw new UserFactoryException(e);
      }

      break;

    case LOCAL:
      debug(thiz, "checking LOCAL");

      // looking for user on local-system
      if (userHashtable.containsKey(username)) {
        user_dataobject = (UserDO) userHashtable.get(username);
      } else {
        throw new UserNotFoundException(username);
      }

      break;

    case EJB:
    /*
       debug(thiz, "checking on EJB");
       // localcontext - with default parameters
       Context ctx;
       try {
         ctx = new InitialContext();
       } catch (NamingException e) {
         throw new UserFactoryException("unable to connect to JSEE server " + e);
       }
       PersonLocalHome localHome;
       try {
         localHome = (PersonLocalHome) ctx.lookup(PersonHome.COMP_NAME);
       } catch (NoInitialContextException e1) {
         throw new UserFactoryException("unable to connect to JSEE server ", e1);
       } catch (NamingException e1) {
         throw new UserFactoryException("unable to connect to JSEE server ", e1);
       }
       // find user by primary key
       Person eePerson;
       try {
         eePerson = localHome.findByPrimaryKey(new PersonPK(username));
         debug(thiz, "returned personobject from Applicationserver: " + eePerson);
       } catch (FinderException e2) {
         throw new UserNotFoundException("user was not found", e2);
       }
       user_dataobject = map(eePerson);
       throw new UnsupportedOperationException();
       // user_dataobject = new User(username, password, eePerson);
       // break;
     *
     */
    default:
      throw new UserNotFoundException("unknown type of userDatabase");
    }

    log(thiz, "found " + username + " on sourceID " + sourceID + " result: " + user_dataobject);

    // last check if the password is correct !!!!
    if (password.compareTo(user_dataobject.password) != 0) {
      //log(thiz, user_dataobject.password + "   " + password);
      throw new WrongPasswordException("The password did not fit to the username");
    }

    return user_dataobject;
  }

  /**
   *
   */
  private static void initAdminUser() {
  }

  /*
   * @deprecated
               private static void initPersonHome() throws UserFactoryException {
                 // localcontext - with default parameters
                 Context ctx;
                 try {
                   Hashtable props = new Hashtable();
                   ctx = new InitialContext(props);
                   debug(thiz, "EJB - created InitialContext");
                 } catch (NamingException e) {
                   throw new UserFactoryException("unable to connect to JSEE server " + e);
                 }
                 try {
                   home = (PersonHome) ctx.lookup(PersonHome.JNDI_NAME);
                   debug(thiz, "EJB - ctx.lookup done");
                   if (home == null) {
                     throw new NamingException("the localhome is null");
                   }
                 } catch (NamingException e1) {
                   throw new UserFactoryException("unable to connect to JSEE server " + PersonHome.JNDI_NAME, e1);
                 }
               }
   */
}
