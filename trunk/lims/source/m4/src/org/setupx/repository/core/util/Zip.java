/**
 * ============================================================================ File:    Zip.java Package: org.setupx.repository.core.util cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.Enumeration;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;


/**
 * Handels Zipped Files ...
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class Zip {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * reades all files from a zipfiles and returns them as a vector of files.
   *
   * @param i_File the zipfile
   *
   * @return vector of files
   */
  public static java.util.Vector getFilenamesFromZip(File i_File)
    throws ZipException, IOException {
    ZipFile zipFile = null;
    zipFile = new ZipFile(i_File);

    Vector v = new Vector();
    Enumeration enumeration = zipFile.entries();

    while (enumeration.hasMoreElements()) {
      ZipEntry tempFile = (ZipEntry) enumeration.nextElement();

      if (tempFile != null) {
        v.add(tempFile.getName());
      }
    }

    return v;
  }

  /**
   * unzippes a file into a directory.
   *
   * @param fin the zipfile
   * @param to the directory where the files will be extracted to.
   *
   * @throws IllegalArgumentException the directory or file does not exist
   */
  public static void unzipFile(File fin, File to) throws IllegalArgumentException, IOException {
    byte[] buffer = new byte[4096];
    int bytes_read;

    // check input file
    if (!fin.exists()) {
      throw new IllegalArgumentException(fin.getName() + " does not exist.");
    }

    if (!fin.canRead()) {
      throw new IllegalArgumentException(fin.getName() + " read protected.");
    }

    ZipInputStream in = new ZipInputStream(new FileInputStream(fin));
    ZipEntry entry;
    FileOutputStream out;

    while ((entry = in.getNextEntry()) != null) {
      String toName = to.getAbsolutePath() + File.separator + entry.getName();
      File fout = new File(toName);

      createParentDirectory(fout);

      if (fout.exists()) {
        // don't overwrite existing files and continue
        System.out.println(fout.getAbsolutePath());
        System.err.println("File '" + toName + "' already exisits.");

        continue;
      }

      if (entry.isDirectory()) {
        // create directory for directory entries
        if (!fout.mkdirs()) {
          System.err.println("Unable to create directory: " + toName);
          System.exit(-1);
        }
      } else {
        // write file
        out = new FileOutputStream(toName);

        while ((bytes_read = in.read(buffer)) != -1)
          out.write(buffer, 0, bytes_read);

        out.close();
      }
    }

    in.close();
  }

  /**
   * @param entry
   */
  private static void createParentDirectory(File target) {
    if (!target.getParentFile().exists()) {
      // parent folder does not exist ... - so create it
      createParentDirectory(target.getParentFile());
      target.getParentFile().mkdir();
    }
  }
}
