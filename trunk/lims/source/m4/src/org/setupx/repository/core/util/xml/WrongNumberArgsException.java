package org.setupx.repository.core.util.xml;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class WrongNumberArgsException extends Exception {
  /**
   * Creates a new WrongNumberArgsException object.
   */
  public WrongNumberArgsException() {
    super();
  }

  /**
   * Creates a new WrongNumberArgsException object.
   *
   * @param arg0 
   */
  public WrongNumberArgsException(String arg0) {
    super(arg0);
  }

  /**
   * Creates a new WrongNumberArgsException object.
   *
   * @param arg0 
   */
  public WrongNumberArgsException(Throwable arg0) {
    super(arg0);
  }

  /**
   * Creates a new WrongNumberArgsException object.
   *
   * @param arg0 
   * @param arg1 
   */
  public WrongNumberArgsException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }
}
