package org.setupx.repository.core.communication.leco;


public class MissingMachineInformationException extends Exception {

    public MissingMachineInformationException(String string, Exception e) {
        super(string,e);
    }
}
