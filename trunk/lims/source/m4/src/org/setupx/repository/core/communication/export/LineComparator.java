/**
 * ============================================================================ File:    LineComparator.java Package: org.setupx.repository.core.communication.export cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.export;

import org.setupx.repository.core.CoreObject;

import java.util.Comparator;
import java.util.Vector;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class LineComparator extends CoreObject implements Comparator {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private int positionInLine;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new LineComparator object.
   *
   * @param position 
   */
  public LineComparator(int position) {
    this.positionInLine = position;
  }

  private LineComparator() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  public int compare(Object o1, Object o2) {
    Vector v1 = (Vector) o1;
    Vector v2 = (Vector) o2;

    Entry entry1 = (Entry) v1.get(this.positionInLine);
    Entry entry2 = (Entry) v2.get(this.positionInLine);

    debug("comparing " + entry1 + " " + entry2);

    return (entry1.value).compareTo(entry2.value);
  }
}
