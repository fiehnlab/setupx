package org.setupx.repository.core;

import org.setupx.repository.core.communication.ServiceNotAvailableException;

public interface Connectable {

    /**
     * checks if the service is available or not
     * 
     * @throws ServiceNotAvailableException
     */
    void checkAvailability() throws ServiceNotAvailableException;
}
