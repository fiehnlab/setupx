package org.setupx.repository.core.communication.status;

import java.io.File;
import java.util.Date;
import java.util.Iterator;

import org.setupx.repository.Config;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.SXQuery;

public class StatusCurator {

    /**
     * creating missing status information for existing result files.
     * 
     * @throws PersistenceActionUpdateException
     * @throws StatusPersistenceException
     */
    public static void createMissingStatuses() throws PersistenceActionUpdateException, StatusPersistenceException {
            // create an instance 
                UserDO userDO = null;
                try {
                    userDO = UserDO.load("scholz", "bla");
                } catch (PersistenceActionFindException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
        
            SampleShippedStatus s1 = new SampleShippedStatus(0, userDO, "Status created by StatusCurator - therefore the date is incorrect. ");
            SampleArrivedStatus s2 = new SampleArrivedStatus(0, userDO, "Status created by StatusCurator - therefore the date is incorrect. ", "unknown");
            LabStatus s3 = new LabStatus(0, userDO, "Status created by StatusCurator - therefore the date is incorrect. ");
            ImportedEventStatus s4 = new ImportedEventStatus(0, userDO, "Status created by StatusCurator - therefore the date is incorrect. ");
            ExportedEventStatus s5 = new ExportedEventStatus(0, userDO, "Status created by StatusCurator - therefore the date is incorrect. ");
            ResultReceivedStatus s6 = new ResultReceivedStatus(0, userDO, "Status created by StatusCurator - therefore the date is incorrect. ");
            
            PersistentStatus[] persistentStatus = new PersistentStatus[]{/*s1,s2,s3,s4,s5,*/s6};
            
            // check for the each existing file
            //File[] resultFiles = new File(Config.DIRECTORY_BB_RESULTS).listFiles();
            File[] resultFiles = new File[]{
                    new File(Config.DIRECTORY_BB_RESULTS + File.separator + "340761.zip"),
                    new File(Config.DIRECTORY_BB_RESULTS + File.separator + "29518.zip"),
                    new File(Config.DIRECTORY_BB_RESULTS + File.separator + "331566.zip"),
                    new File(Config.DIRECTORY_BB_RESULTS + File.separator + "89524.zip"),
                    new File(Config.DIRECTORY_BB_RESULTS + File.separator + "327304.zip")
            };

            for (int i = 0; i < resultFiles.length; i++) {
                try {
                    
                    File file = resultFiles[i];
                    
                    Logger.log(StatusCurator.class, "filename " + file.getName());
                    
                    // get promt ID 
                    String promt_id_string = file.getName().substring(0, file.getName().indexOf('.'));
                    Logger.log(StatusCurator.class, "promt_id_string: " + promt_id_string + " from " + file.getName());

                    long promt_id = Long.parseLong(promt_id_string);
                    
                    Logger.log(StatusCurator.class, "promt_id: " + promt_id + " from " + file.getName());
    
                    
                    generateStatus(persistentStatus, file.lastModified(), promt_id);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
    }

    /**
     * @param persistentStatus
     * @param dateLastModified
     * @param promt_id
     * @throws NumberFormatException
     * @throws PersistenceActionUpdateException
     */
    public static void generateStatus(PersistentStatus[] persistentStatus, long dateLastModified, long promt_id) throws NumberFormatException, PersistenceActionUpdateException {
        // get all sampleIDs for this promt

        Iterator sampleIDs = new SXQuery().findSampleIDsByPromtID(promt_id).iterator();
        while (sampleIDs.hasNext()) {
            long sampleID = Long.parseLong("" + sampleIDs.next());


            for (int j = 0; j < persistentStatus.length; j++) {
                // assign a clone to each sample
                PersistentStatus status = persistentStatus[j].createClone();
                status.setSampleID(sampleID);
                status.setDate(new Date(dateLastModified + (j*1000*60)));
                Logger.log(StatusCurator.class, "set time: " + status.getDate());
                status.update(true);
                Logger.log(StatusCurator.class, "created status: " + status.toString());
            }
        }
    }
}
