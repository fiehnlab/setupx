/**
 * ============================================================================ File:    OBOConnectorPool.java Package: org.setupx.repository.core.communication.ontology.obo cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ontology.obo;

import java.io.File;

import java.util.Hashtable;


/**
 * Pool of connections available in the system. Prevents from having multiple instances of the same ontology.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class OBOConnectorPool {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static Hashtable connectors = new Hashtable();

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param connector TODO
   */
  public static void add(OBOConnector connector) {
    connectors.put(connector.getFileReader().sourceFile.getName(), connector);
  }

  /**
   * TODO: 
   *
   * @param sourceFile TODO
   *
   * @return TODO
   */
  public static boolean contains(File sourceFile) {
    return connectors.contains(sourceFile.getName());
  }

  /**
   * TODO: 
   *
   * @param sourceFile TODO
   *
   * @return TODO
   */
  public static OBOConnector get(File sourceFile) {
    return (OBOConnector) connectors.get(sourceFile.getName());
  }
}
