package org.setupx.repository.core.user;

/**
 * problems while user created
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class UserFactoryException extends Exception {
  /**
   * Creates a new UserFactoryException object.
   */
  public UserFactoryException() {
    super();
  }

  /**
   * Creates a new UserFactoryException object.
   *
   * @param arg0 
   */
  public UserFactoryException(String arg0) {
    super(arg0);
  }

  /**
   * Creates a new UserFactoryException object.
   *
   * @param arg0 
   */
  public UserFactoryException(Throwable arg0) {
    super(arg0);
  }

  /**
   * Creates a new UserFactoryException object.
   *
   * @param arg0 
   * @param arg1 
   */
  public UserFactoryException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }
}
