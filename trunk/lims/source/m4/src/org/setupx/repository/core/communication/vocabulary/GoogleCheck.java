/**
 * ============================================================================ File:    GoogleCheck.java Package: org.setupx.repository.core.communication.vocabulary cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.vocabulary;

import com.google.soap.search.GoogleSearch;
import com.google.soap.search.GoogleSearchFault;
import com.google.soap.search.GoogleSearchResult;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;


/**
 * word checker for a word based on the google system.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.10 $
 *
 * @see org.setupx.repository.Config#GOOGLE_LICENSEKEY
 * @see org.setupx.repository.core.communication.vocabulary.VocabularyCheck
 */
class GoogleCheck extends CoreObject implements VocabularyCheck {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new GoogleCheck object.
   */
  public GoogleCheck() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.communication.vocabulary.VocabularyCheck#getSuggestion(java.lang.String)
   */
  public String getSuggestion(final String word) throws VocabularyCheckException {
    if (!ACTIVE) {
      return "";
    }

    GoogleSearch search = new GoogleSearch();

    // Set mandatory attributes
    search.setKey(Config.GOOGLE_LICENSEKEY);

    try {
      String suggestion = search.doSpellingSuggestion(word);

      //debug(this, "Suggestion for " + word + " : " + suggestion);
      return suggestion;
    } catch (GoogleSearchFault e1) {
      throw new VocabularyCheckException(e1);
    }
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.communication.vocabulary.VocabularyCheck#check(java.lang.String)
   */
  public boolean check(final String word) throws VocabularyCheckException {
    if (!ACTIVE) {
      return false;
    }

    boolean result = false;

    GoogleSearch search = new GoogleSearch();

    // Set mandatory attributes
    search.setKey(Config.GOOGLE_LICENSEKEY);
    search.setQueryString(word);

    // Set optional attributes
    search.setSafeSearch(true);

    // Invoke the actual search
    GoogleSearchResult searchResult;

    // process the result
    try {
      searchResult = search.doSearch();
    } catch (GoogleSearchFault e) {
      throw new VocabularyCheckException(e);
    }

    debug(this, word + " count : " + searchResult.getEstimatedTotalResultsCount());

    if (searchResult.getEstimatedTotalResultsCount() > Config.GOOGLE_LIMIT) {
      result = true;
    }

    return result;
  }
}
