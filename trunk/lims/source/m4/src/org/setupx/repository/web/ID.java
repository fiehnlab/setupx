package org.setupx.repository.web;

import org.setupx.repository.core.CoreObject;

public abstract class ID extends CoreObject {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  public ID(String id) {
    this.uoid = Long.parseLong(id);
  }

  public ID(long id) {
    this.uoid = id;
  }
}
