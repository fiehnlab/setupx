package org.setupx.repository.core.util;

import org.setupx.repository.core.CoreObject;

import java.net.URL;

import java.util.Vector;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class SubClassIdentification extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final Object thiz = new SubClassIdentification();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new SubClassIdentification object.
   */
  public SubClassIdentification() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Findes all Derivates for a Class in a defined package.
   *
   * @param packagename The directory containing the Classes that will be checked
   *
   * @throws UtilException unable to create instances of the classes
   */
  public static Class[] findImplementations(Class clazzToLookFor, String packagename)
    throws UtilException {
    debug(thiz, "looking for instances of " + clazzToLookFor.getName() + " in package: " + packagename);

    // vector containing all classes that match
    Vector classes = new Vector();
    String packageName = packagename;

    if (!packagename.startsWith("/")) {
      packagename = "/" + packagename;
    }

    packagename = packagename.replace('.', '/');

    // Get a File object for the package
    URL url = File.class.getResource(packagename);

    if (url == null) {
      throw new UtilException("unable to create an url for Packagename: " + packagename);
    }

    java.io.File directory = new java.io.File(url.getFile());
    debug(null, directory.getAbsolutePath());

    if (directory.exists()) {
      // Get the list of the files contained in the package
      String[] files = directory.list();

      for (int i = 0; i < files.length; i++) {
        // we are only interested in .class files
        if (files[i].endsWith(".class")) {
          debug(thiz, "file in directory:  " + files[i]);

          // removes the .class extension
          String classname = files[i].substring(0, files[i].length() - 6);

          try {
            Class myClass = Class.forName(packageName + "." + classname);
            debug(null, "working on " + myClass.getName());

            // Try to create an instance of the object
            Object o = myClass.newInstance();

            log(thiz, myClass.toString());
            log(thiz, clazzToLookFor.toString());

            /*
               if (clazzToLookFor instanceof clazzToLookFor) {
                 classes.add(myClass);
               }*/
          } catch (ClassNotFoundException cnfex) {
            System.err.println(cnfex);
            throw new UtilException("problems creating an object for : " + packagename + "." + classname);
          } catch (InstantiationException iex) {
            // We try to instanciate an interface
            // or an object that does not have a
            // default constructor
            System.err.println(iex);
          } catch (IllegalAccessException iaex) {
            // The class is not public
            System.err.println(iaex);
          }
        }
      }
    }

    Class[] result = new Class[classes.size()];

    for (int i = 0; i < classes.size(); i++) {
      Class clazz = (Class) classes.get(i);
      result[i] = clazz;
    }

    return result;
  }
}
