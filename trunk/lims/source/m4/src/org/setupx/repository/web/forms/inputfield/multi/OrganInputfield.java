/**
 * ============================================================================ File:    OrganInputfield.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.core.communication.ncbi.local.NCBIEntry;
import org.setupx.repository.core.util.hotfix.oldsamples.OldClassTemplate;
import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.FormRootNotAvailableException;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.relation.RelationKingdom;
import org.setupx.repository.web.forms.restriction.RestrictionOBO_human_anatomy;
import org.setupx.repository.web.forms.restriction.RestrictionOBO_plant_anatomy;
import org.setupx.repository.web.forms.restriction.StringRestriction;


/**
 * @hibernate.subclass
 */
public class OrganInputfield extends MultiField4Clazz {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private NBCISpeciesInputfield species;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new OrganInputfield object.
   *
   * @param question 
   * @param description 
   */
  public OrganInputfield(String question, String description, final NBCISpeciesInputfield i_species) {
      super(question, description);
      this.species = i_species;
      
      debug("creaing new instance.");
      if (i_species == null) {
          warning(new NullPointerException("no species is defined! "));
      } else {
          debug("species: " + i_species);
          debug("species: " + i_species.entry);
          //debug("species: " + i_species.entry.getName());
      }



      // different inputfields for each species and the connected obo
      //StringInputfield inputfieldOrganAnimal = new OBOStringInputfield("organ name", "type in the name of the organ");
      StringInputfield inputfieldOrganAnimal = new StringInputfield("organ name", "type in the name of the organ");
      inputfieldOrganAnimal.setRequiered(true);
      // --- no obo yet !!!
      this.addField(inputfieldOrganAnimal);
      debug("added animal organ inputfield.");

      // StringInputfield inputfieldOrganHuman = new OBOStringInputfield("organ name", "type in the name of the organ (human)");
      StringInputfield inputfieldOrganHuman = new StringInputfield("organ name", "type in the name of the organ (human)");
      inputfieldOrganHuman.setRequiered(true);
      inputfieldOrganHuman.setRestriction(new RestrictionOBO_human_anatomy());
      this.addField(inputfieldOrganHuman);
      debug("added human organ inputfield.");

      
      // StringInputfield inputfieldOrganPlant = new OBOStringInputfield("organ name", "type in the name of the organ (plant)");
      StringInputfield inputfieldOrganPlant = new StringInputfield("organ name", "type in the name of the organ (plant)");
      inputfieldOrganPlant.setRequiered(true);
      inputfieldOrganPlant.setRestriction(new RestrictionOBO_plant_anatomy());
      this.addField(inputfieldOrganPlant);
      debug("added plant organ inputfield.");

      // additional information received from the obo
      
      

      // tissue
      InputField inTissue = new StringInputfield("tissue", "define the tissue");
      inTissue.setValue("-total-");
      inTissue.setHelptext("Biological tissue is a group of cells that perform a similar function.The study of tissues is known as histology, or, in connection with disease, histopathology.");
      inTissue.setRequiered(false);
      this.addField(inTissue);

      // cell type
      StringInputfield inCellType = new StringInputfield("cell type", "please define the cell type.");
      inCellType.setHelptext("The smallest structural unit of living organisms that is able to grow and reproduce independently.");
      inCellType.setRestriction(new StringRestriction("(.*)", true));
      inCellType.setRequiered(false);
      this.addField(inCellType);

      // sub cell 
      StringInputfield inSubcellular = new StringInputfield("subcellular celltype", "define the subcellular celltype.");
      inSubcellular.setRestriction(null);
      inSubcellular.setRequiered(false);
      this.addField(inSubcellular);

      // plant & microorganism
      this.species.addRelation(new RelationKingdom(new int[] { NCBIEntry.PLANT, NCBIEntry.MIRCOORGANISM }, new InputField[] { inputfieldOrganPlant }));
      // animal
      this.species.addRelation(new RelationKingdom(new int[] { NCBIEntry.ANIMAL }, new InputField[] { inputfieldOrganAnimal }));
      // human
      this.species.addRelation(new RelationKingdom(new int[] { NCBIEntry.HUMAN }, new InputField[] { inputfieldOrganHuman }));
      // all
      this.species.addRelation(new RelationKingdom(new int[] { NCBIEntry.PLANT, NCBIEntry.ANIMAL, NCBIEntry.HUMAN }, new InputField[] { inTissue, inCellType, inSubcellular }));
  }

  /**
   * Creates a new OrganInputfield object. <br>
   * Constructor is used by Hibernate.
   * <p>
   * <h2>Problem:</h2>
   * <code>this.species</code> has not been initalized by the time the <code>clone</code> is beeing called.
   */
  public OrganInputfield() {
    super();
    
    // FIX:
    try {
        this.species = this.root().findNBCISpeciesInputfield();
    } catch (Exception e) {
        e.printStackTrace();
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getPath()
   */
  public String getPath() {
    //return OldClassTemplate.ORGAN;
    return OldClassTemplate.OPERATOR_COMMENT;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getSingleValueField()
   */
  public FormObject getSingleValueField() {
    return this.getField(0);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#cloneField()
   */
  public InputField cloneField() throws CloneException {
      if (this.species == null) {
          warning(this, "the species is not initalized. Trying to init now.");
          try {
              this.species = this.root().findNBCISpeciesInputfield();
          } catch (FormRootNotAvailableException e) {
              throw new RuntimeException("Unknown species.");
          }

          if (this.species == null) {
              throw new RuntimeException("Unknown species.");
          } else {
              debug("the species has been recovered.");
          }
      }
      return new OrganInputfield(this.getQuestion(), this.getQuestion(), this.species);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.clazzes.ClazzInformation#createShortLabel()
   */
  public String createShortLabel() {
    return this.toStringHTML();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#toStringHTML()
   */
  public String toStringHTML() {
    return this.getQuestion() + ": " + ((InputField) this.getField(0)).getValue();
  }
}
