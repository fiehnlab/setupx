/**
 * ============================================================================ File:    Restriction4Double.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.web.forms.restriction.Restriction4Numbers;


/**
 * limitation for doubles
 *
 * @hibernate.subclass
 */
public class Restriction4Double extends Restriction4Numbers {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final long serialVersionUID = -1008136424106118695L;

  {
    this.includePatternTest = false;
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Restriction4Double object.
   */
  public Restriction4Double() {
    super();
  }

  /**
   * Creates a new Restriction4Double object.
   *
   * @param startRange 
   * @param endRange 
   */
  public Restriction4Double(int startRange, int endRange) {
    super(startRange, endRange);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction4Numbers#getErrorMessage()
   */
  public String getErrorMessage() {
    return "The value should be like 1.52";
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction4Numbers#parse(java.lang.String)
   */
  public double parse(String s) {
    return Double.parseDouble(s);
  }
}
