package org.setupx.repository.core.query;

import java.util.List;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.server.persistence.PersistenceActionFindException;

public class TestingTheQueries extends CoreObject {
    
    private static Class thiz = TestingTheQueries.class; 
    
    public static String[] searchTypes = new String[]{
        "species",
        "compound",
        "factor", 
        "title",
        "sample"
    };

    public static String[] searchTerms = new String[]{
        "Arabidopsis",
        "29518",
        "Gly", 
        "blood",
        "Warden",
        "Warden",
        "Warden",
        "Arabidopsis thaliana",
        "glyci",
        "glyc",
        "ara",
        "gl",
        "blood",
        "Bos taurus",
        "gly",
        "human",
        "cow",
        "Arab",
        "29518",
        "wild",
        "mus",
        "Beta vulgaris subsp. vulgaris"        
    };

    
    public static void testIt() throws PersistenceActionFindException{
        QueryMaster master = new QueryMaster();

        UserDO[] users = new UserDO[]{
                    UserDO.load("scholz"),
                    UserDO.load("public"),
                    UserDO.load("kliebenstein")
        };
        
        for (int i = 0; i < searchTerms.length; i++) {
            String searchTerm = searchTerms[i];
            
            for (int j = 0; j < searchTypes.length; j++) {
                String searchType = searchTypes[j];

                for (int k = 0; k < users.length; k++) {
                    UserDO userDO = users[k];
                    
                    List results = master.search(userDO, j, searchTerm);
                    log(thiz, "term: " + searchTerm + "  searchType: " + searchType + "  user: " + userDO.getUsername() + "  results: " + results.size());
                }
            }
        }
    }
}
