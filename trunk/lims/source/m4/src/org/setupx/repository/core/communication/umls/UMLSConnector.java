/**
 * ============================================================================ File:    UMLSConnector.java Package: org.setupx.repository.core.communication.umls cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.umls;

import org.setupx.repository.core.Connectable;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.ServiceNotAvailableException;
import org.setupx.repository.core.communication.ncbi.NCICBException;
import org.setupx.repository.core.communication.ncbi.NCICB_Result;
import org.setupx.repository.core.communication.ncbi.NCICB_connector;

import gov.nih.nlm.kss.api.KSSRetrieverV5_0;
import gov.nih.nlm.kss.util.DatabaseException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

import java.net.MalformedURLException;

import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import java.util.Vector;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class UMLSConnector extends CoreObject implements Connectable {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static PrintStream out;
  private static UMLSConnector instance;
  static Object thiz = new UMLSConnector();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new UMLSConnector object.
   */
  public UMLSConnector() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public static UMLSConnector getInstance() {
    if (instance == null) {
      instance = new UMLSConnector();
    }

    return instance;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.Connectable#checkAvailability()
   */
  public void checkAvailability() throws ServiceNotAvailableException {
  }

  /**
   * TODO: 
   *
   * @param args TODO
   *
   * @throws Exception TODO
   */
  public static void main(String[] args) throws Exception {
    String[] testWords = org.setupx.repository.core.util.Util.convert2StringArr(new java.io.File("c:\\organ_testlist.txt"));
    File file = new File("C:\\organs_test_result.txt");

    //String[] testWords = new String[]{"eye", "lung", "arm"};
    for (int i = 0; i < testWords.length; i++) {
      String testWord = testWords[i];
      testWord = testWord.replace('\n', ' ');

      try {
        NCICB_Result result = NCICB_connector.query(testWord);
        log(thiz, "NCICB_connector " + testWord + "   " + result.getName() + " (" + result.getId() + ")");
        org.setupx.repository.core.util.logging.Logger.log2File(file, testWord + "   " + result.getName() + " (" + result.getId() + ")");
      } catch (NCICBException e) {
        org.setupx.repository.core.util.logging.Logger.log2File(file, testWord + "   -- (  -- )");
        e.printStackTrace();
      }
    }

    // ORIGINaL CODE
    KSSRetrieverV5_0 retriever = null;

    //  Create the input stream to obtain user input
    BufferedReader br = null;

    try {
      br = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));

      out = new PrintStream(System.out, true, "UTF-8");
    } catch (Exception ex) {
      ex.printStackTrace();

      return;
    }

    try {
      String host = "umlsks.nlm.nih.gov";
      String name = "//" + host + "/" + "KSSRetriever";

      retriever = (KSSRetrieverV5_0) Naming.lookup(name);
    } catch (RemoteException rex) {
      System.err.println("RemoteException: " + rex.getMessage());

      return;
    } catch (NotBoundException nbex) {
      System.err.println("NotBoundException: " + nbex.getMessage());

      return;
    } catch (MalformedURLException mfurl) {
      System.err.println("MalformedURLException: " + mfurl.getMessage());

      return;
    }

    boolean done = false;

    while (!done) {
      int menuChoice = outputMenu(br);

      switch (menuChoice) {
      case 0: // exiting client ...
        done = true;

        break;

      case 1: // Requesting the string for an AUI
        getAUIDetails(br, retriever);

        break;

      case 2: // Requesting the concept attributes from the
              // new atomic model
        getConceptAttributes(br, retriever);

        break;

      case 3: // Requesting the relationship attributes from the
              // new atomic model
        getRelationshipAttributes(br, retriever);

        break;

      default:
        done = true;

        break;
      }
    }

    //	To help out with garbage collection, I set the
    //	retriever to null here
    retriever = null;
  }

  /**
   * TODO: 
   *
   * @param br TODO
   * @param retriever TODO
   */
  private static void getAUIDetails(BufferedReader br, KSSRetrieverV5_0 retriever) {
    System.out.print("Enter UMLS Release Year: ");

    String dbYear = getString(br);
    System.out.print("Enter AUI: ");

    String aui = getString(br);

    System.out.print("Output contents to 1) the screen or 2) a file: ");

    String output = getString(br);
    output = output.toLowerCase();

    boolean screenOutput = true;
    String outputFileName = "";

    if (output.equals("2")) {
      screenOutput = false;

      System.out.print("Enter output file name: ");
      outputFileName = getString(br);
    }

    String cmdName = "getAUIDetails";

    try {
      long start = System.currentTimeMillis();

      char[] result = null;
      result = retriever.getAUIDetails(dbYear, aui);

      long end = System.currentTimeMillis();
      long interval = end - start;

      System.out.println();
      System.out.println("\t" + cmdName + " for '" + aui + "' in XML took " + interval + " milliseconds");

      String xml = new String(result);

      if (screenOutput) {
        out.println(xml);
      } else {
        writeFile(outputFileName, xml);
      }
    } catch (DatabaseException ex) {
      System.err.println(cmdName + ": DatabaseException-> " + ex.getMessage());
    } catch (RemoteException ex) {
      System.err.println(cmdName + ": RemoteException-> " + ex.getMessage());
    }
  }

  /**
   * TODO: 
   *
   * @param br TODO
   * @param retriever TODO
   */
  private static void getConceptAttributes(BufferedReader br, KSSRetrieverV5_0 retriever) {
    System.out.print("Enter UMLS release: ");

    String release = getString(br);

    System.out.print("Enter CUI: ");

    String cui = "C0040300"; //getString(br);

    Vector sabs = new Vector();
    System.out.println("Enter SABs (one per line, blank to end list):");

    String sab = getString(br);

    while (!sab.equals("")) {
      sabs.add(sab);
      sab = getString(br);
    }

    System.out.print("Output contents to 1) the screen or 2) a file: ");

    String output = getString(br);
    output = output.toLowerCase();

    boolean screenOutput = true;
    String outputFileName = "";

    if (output.equals("2")) {
      screenOutput = false;

      System.out.print("Enter output file name: ");
      outputFileName = getString(br);
    }

    try {
      long start = System.currentTimeMillis();

      char[] result = retriever.getConceptAttributes(release, cui, sabs);

      long end = System.currentTimeMillis();
      long interval = end - start;

      System.out.println();
      System.out.println("\tgetConceptAttributes " + "' in XML took " + interval + " milliseconds");

      String xml = new String(result);

      if (screenOutput) {
        out.println(xml);
      } else {
        writeFile(outputFileName, xml);
      }
    } catch (DatabaseException ex) {
      System.err.println("getConceptAttributes: DatabaseException-> " + ex.getMessage());
    } catch (RemoteException ex) {
      System.err.println("getConceptAttributes: RemoteException-> " + ex.getMessage());
    }
  }

  /**
   * TODO: 
   *
   * @param br TODO
   * @param retriever TODO
   */
  private static void getRelationshipAttributes(BufferedReader br, KSSRetrieverV5_0 retriever) {
    System.out.print("Enter UMLS release: ");

    String release = getString(br);

    System.out.print("Enter CUI: ");

    String cui = getString(br);

    Vector sabs = new Vector();
    System.out.println("Enter SABs (one per line, blank to end list):");

    String sab = getString(br);

    while (!sab.equals("")) {
      sabs.add(sab);
      sab = getString(br);
    }

    System.out.print("Output contents to 1) the screen or 2) a file: ");

    String output = getString(br);
    output = output.toLowerCase();

    boolean screenOutput = true;
    String outputFileName = "";

    if (output.equals("2")) {
      screenOutput = false;

      System.out.print("Enter output file name: ");
      outputFileName = getString(br);
    }

    try {
      long start = System.currentTimeMillis();

      char[] result = retriever.getRelationshipAttributes(release, cui, sabs);

      long end = System.currentTimeMillis();
      long interval = end - start;

      System.out.println();
      System.out.println("\tgetRelationshipAttributes " + "' in XML took " + interval + " milliseconds");

      String xml = new String(result);

      if (screenOutput) {
        out.println(xml);
      } else {
        writeFile(outputFileName, xml);
      }
    } catch (DatabaseException ex) {
      System.err.println("getRelationshipAttributes: DatabaseException-> " + ex.getMessage());
    } catch (RemoteException ex) {
      System.err.println("getRelationshipAttributes: RemoteException-> " + ex.getMessage());
    }
  }

  /**
   * TODO: 
   *
   * @param br TODO
   *
   * @return TODO
   */
  private static String getString(BufferedReader br) {
    String str = null;

    try {
      str = br.readLine();
    } catch (IOException iex) {
      System.err.println("IOException: " + iex.getMessage());
    }

    return str;
  }

  //	Shows the client menu and returns the menu choice. 0 is returned
  //	if the user wants to quit the client.
  private static int outputMenu(BufferedReader br) {
    //	Show the client menu and obtain the selection
    //	from the user
    System.out.println("\nUMLSKS ClientV5_0 Menu:");

    String tab = "    ";
    System.out.println(tab + " 1) Get String Information for AUI");
    System.out.println(tab + " 2) Retrieve Concept Attributes");
    System.out.println(tab + " 3) Retrieve Relationship Attributes");
    System.out.println(tab + "'q' to Quit client");
    System.out.println();
    System.out.print("Selection: ");

    String select = getString(br);
    select = select.toLowerCase();

    if (select.equals("q")) {
      System.out.println();

      return 0;
    }

    Integer i = null;

    try {
      i = new Integer(select);
    } catch (NumberFormatException ex) {
      System.out.println("Either 'q' or a number must be entered");

      //	Make them try again
      return outputMenu(br);
    }

    int selection = i.intValue();

    if ((selection < 1) || (selection > 3)) {
      //	Invalid menu selection, try again
      System.out.println("Either 'q' or a number must be entered");

      return outputMenu(br);
    }

    return selection;
  }

  /**
   * TODO: 
   *
   * @param outputFileName TODO
   * @param xml TODO
   */
  private static void writeFile(String outputFileName, String xml) {
    try {
      //	Open the file and write an error message if unable to create it.
      PrintStream fos = new PrintStream(new FileOutputStream(outputFileName), true, "UTF-8");

      //  Write the xml to the file.
      fos.println(xml);

      //	Close the stream.
      fos.close();
    } catch (IOException ex) {
      System.out.println("IOException: " + ex.getMessage());
    }
  }
}
