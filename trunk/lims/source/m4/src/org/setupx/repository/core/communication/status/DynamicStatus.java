package org.setupx.repository.core.communication.status;

import java.util.Collection;
import java.util.Vector;

import org.hibernate.Session;

import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;


/**
 * Status which is based on events, values, .... It is not persistent and will be recreated everytime again
 * when look for one.
 * 
 * @author scholz
 *
 */
public abstract class DynamicStatus extends Status {


    public DynamicStatus(long sampleUOID) {
        super(sampleUOID);
    }

    public static Session createSession() {
        return PersistenceConfiguration.createSessionFactory().openSession();
    }

    public static Collection find(long sampleUOID, Session hqlSession) {
        return new Vector();
    }
}


