package org.setupx.repository.web.forms;

import org.setupx.repository.web.forms.clazzes.Treatment;
import org.setupx.repository.web.forms.inputfield.BigStringInputField;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.Removable;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.multi.MultiField;
import org.setupx.repository.web.forms.inputfield.multi.MultiFieldExpanding;
import org.setupx.repository.web.forms.restriction.Restriction4StringLength;

/**
 * @hibernate.subclass
 */
public class DietInputfield2 extends MultiField implements Treatment {
    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new DietInputfield object.
     */
    public DietInputfield2() {
      super();
    }

    /**
     * Creates a new DietInputfield object.
     *
     * @param question 
     * @param description 
     */
    public DietInputfield2(String question, String description) {
      super(question, description);

      InputField field1 = new StringInputfield("Label", "Please give the diet a meaningful label - for example name it \"Lithium diet\" if you plan to compare samples that were collected after treatment with lithium. ", "diet", false);
      field1.setValue("Lithium diet");
      field1.setRestriction(new Restriction4StringLength(8));

      BigStringInputField field2 = new BigStringInputField("comment", "feel free to describe the diet in detail", true);
      field2.setValue(".... please add a brief description of this Diet.");
      field2.setRestriction(null);

      this.addField(field1);
      this.addField(field2);

      Removable removable = new DietStringInputfield2("diet");
      MultiFieldExpanding fieldExpanding = new MultiFieldExpanding("Variation", "define every variation within this diet - so for example \"Lithium\", \" control\", ...", "", removable);

      try {
        fieldExpanding.expand(3);
      } catch (ArrayExpandException e) {
        e.printStackTrace();
      }

      this.addField(fieldExpanding);
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* (non-Javadoc)
     * @see org.setupx.repository.web.forms.inputfield.multi.MultiField#cloneField()
     */
    public InputField cloneField() throws CloneException {
      return new DietInputfield2(this.getQuestion(), this.getDescription());
    }
}
