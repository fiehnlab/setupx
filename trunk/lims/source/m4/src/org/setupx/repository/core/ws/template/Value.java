package org.setupx.repository.core.ws.template;

/**
 * TEMPLATE FOR GENERATION the webservice - look at the real implementaions
 *
 * @author <a href="mailto:scholz@zeypher.com?subject=m1" >&nbsp;Martin  Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class Value {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String title = null;
  private String value = null;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Value object.
   */
  public Value() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param title
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param value
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * @return
   */
  public String getValue() {
    return value;
  }
}
