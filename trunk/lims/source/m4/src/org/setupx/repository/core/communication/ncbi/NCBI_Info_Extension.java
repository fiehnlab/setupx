/**
 * ============================================================================ File:    NCBI_Info_Extension.java Package: org.setupx.repository.core.communication.ncbi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ncbi;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.xml.XPath;


/**
 * Requesting information from remote NCBI service
 * 
 * <p>
 * <code><a href="http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=taxonomy&id=3701&retmode=xml">http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=taxonomy&id=3701&retmode=xml</a></code>
 * </p>
 */
class NCBI_Info_Extension extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*            <eSummaryResult>
     <DocSum>
             <Id>3701</Id>
             <Item Name="Rank" Type="String">genus</Item>
             <Item Name="Division" Type="String">eudicots</Item>
             <Item Name="ScientificName" Type="String">Arabidopsis</Item>
             <Item Name="CommonName" Type="String"></Item>
             <Item Name="TaxId" Type="Integer">3701</Item>
             <Item Name="NucNumber" Type="Integer">1005572</Item>
             <Item Name="ProtNumber" Type="Integer">128337</Item>
             <Item Name="StructNumber" Type="Integer">158</Item>
             <Item Name="GenNumber" Type="Integer">7</Item>
             <Item Name="GeneNumber" Type="Integer">30872</Item>
     </DocSum>
     </eSummaryResult>
   */
  protected DocumentBuilder builder;
  protected DocumentBuilderFactory factory;
  protected URL baseUrl = null;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public final static String NCBI_URL_STRING = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi";
  public final static String URL_SEPERATOR = "?";
  public final static String URL_ARGUMENT_SEPERATOR = "&";
  public final static String URL_ARGUMENT_EQUAL = "=";

  {
    try {
      baseUrl = new URL(NCBI_URL_STRING);
      Util.checkURL(baseUrl);

      factory = DocumentBuilderFactory.newInstance();
      builder = factory.newDocumentBuilder();
    } catch (Exception e) {
      err(e.toString());
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param id TODO
   *
   * @return TODO
   *
   * @throws NCBIException TODO
   */
  public Hashtable determineNCBI_Information(int id) throws NCBIException {
    try {
      // create the whole url - must look like : http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=taxonomy&id=3701&retmode=xml
      URL url = new URL(NCBI_URL_STRING + URL_SEPERATOR + "db" + URL_ARGUMENT_EQUAL + "taxonomy" + URL_ARGUMENT_SEPERATOR + "id" + URL_ARGUMENT_EQUAL + id + URL_ARGUMENT_SEPERATOR + "retmode" + URL_ARGUMENT_EQUAL + "xml" + URL_ARGUMENT_SEPERATOR + "email" + URL_ARGUMENT_EQUAL + NCBIConnector.EMAIL); //"tool" + URL_ARGUMENT_EQUAL + NCBIConnector.TOOL + URL_ARGUMENT_SEPERATOR + 

      debug(url);

      // connect
      URLConnection connection = url.openConnection();
      connection.connect();

      String contentType = connection.getContentType();

      if (contentType.compareTo("XML") != 0) {
        warning(new NCBIException("the service is responding with the wrong type of content intstead of XML : " + contentType));
        warning(new NCBIException("will treat it like an XML doc"));
      }

      // create a doc from it
      InputStream in = connection.getInputStream();

      Document doc = builder.parse(in);

      // parse it for the answer
      String value;
      NodeList values = null;

      try {
        values = XPath.path(doc, "/eSummaryResult/DocSum/Item");
      } catch (NullPointerException e) {
        value = "";
      }

      Hashtable result = new Hashtable();

      // convert it into an Hashtable
      int size = values.getLength();

      for (int i = 0; i < size; i++) {
        Node node = values.item(i);
        NamedNodeMap namedNodeMap = node.getAttributes();
        String node_name = namedNodeMap.getNamedItem("Name").getNodeValue();
        String node_value = node.getFirstChild().getNodeValue();
        result.put(node_name, node_value);
      }

      // create the answer
      return result;
    } catch (Exception e) {
      throw new NCBIException(e);
    }
  }
}
