package org.setupx.repository.core.query;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.ncbi.NCBIConnector;
import org.setupx.repository.core.communication.ncbi.NCBIException;
import org.setupx.repository.core.query.answer.CompoundQueryMasterAnswer;
import org.setupx.repository.core.query.answer.FormobjectAnswer;
import org.setupx.repository.core.query.answer.FormobjectAnswerCreationException;
import org.setupx.repository.core.query.answer.FoundExperimentTwiceException;
import org.setupx.repository.core.query.answer.NCBIQueryMasterAnswer;
import org.setupx.repository.core.query.answer.VariationAnswer;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.access.PromtUserAccessRight;
import org.setupx.repository.server.db.SQLStatementPool;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.Sample;
import org.setupx.repository.web.pub.data.cache.VariationsCache;

/**
 * Main Query master.  
 */
public class QueryMaster extends CoreObject {

    public static QueryMasterCache cache = new QueryMasterCache();

    private static QueryMaster thiZ = new QueryMaster();

    public static final int SPECIES = 0;
    public static final int COMPOUND = 1;
    public static final int FACTOR= 2;
    public static final int TITLE= 3;
    public static final int SAMPLE= 4;
    public static final int ID = 8;
    public static final int ALL= 99;

    /** min length to start queryes based by looking for their ID */
    public static final int MIN_LENGTH_QUERYBYID = 3;

    /**
     * Userobject used for authentification. Depending on the users {@link PromtUserAccessRight} some of the information will be hidden.
     */
    private UserDO user;

    /** type of the query defined by the types above
     * @see #SPECIES
     */
    private int querytype;

    /** the searchterm */
    private String searchTerm;

    /** {@link Session} for the queries */ 
    private Session session;
    private long timer = 0;

    /**
     * General init.
     */
    static {
        log(null,"init QueryMaster");
    }


    /**
     * Search.
     * <h4>Search</h4>
     * Searching in different sources and for different Objects. The Answer is always a {@link List} of {@link QueryMasterAnswer}s. The {@link QueryMasterAnswer} contains
     * the actual result. 
     * <p>
     * <h4>Define a search</h4>
     * To define a search a {@link #querytype} is needed. The {@link #querytype} defines what kind of search is going to be processed. The different queries are currently
     * {@link #SPECIES}, {@link #COMPOUND}, {@link #FACTOR}, {@link #TITLE} and {@link #SAMPLE}. Also it is possible to perform a search over all of them by using {@link #ALL} 
     * as value for the {@link #querytype}. 
     * <p>
     * <h4>History</h4>
     * All queries are beeing stored as a {@link QueryTerm} including the userID, the actual term, the querytype and a timestamp.
     * 
     * <h3>todo:</h3>
     * <li>add cache function
     * <li>add user check - if user can actually access the related sample, data, ...
     * <p>
     * <br>
     * <br>
     * 
     * @param user Userobject used for authentification. Depending on the users {@link PromtUserAccessRight} some of the information will be hidden.
     * @param queryType {@link #SPECIES}, {@link #COMPOUND}, {@link #FACTOR}, {@link #TITLE} and {@link #SAMPLE}. Also it is possible to perform a search over all of them by using {@link #ALL} 
     * @param term the actual search term.
     * @param variation variation for the factors
     * @return a list of {@link QueryMasterAnswer}s
     * 
     */
    public List search(UserDO user, final int queryType, final String term, final String variation){
        if (user == null)
            try {
                user = UserDO.load("public");
            } catch (PersistenceActionFindException e2) {
                e2.printStackTrace();
            }
        
        this.user = user;
        this.searchTerm = term;

        if (this.searchTerm.length() < 3) {
            debug("query \"" + term + "\" too short - terminating query"); 
            return new Vector();
        }
        
        this.querytype = queryType;

        // add it to the list of submitted queries
        try {
            new QueryTerm(this.searchTerm, this.user, this.querytype).update(true);
        } catch (PersistenceActionUpdateException e1) {
            e1.printStackTrace();
        }

        // staring
        log("start search  querytype: " + queryType + " term: \"" + term + "\"");

        // the result containing the different answers
        List resultList = new Vector();
        long start = new Date().getTime();

        // checking if there is a matching result for #querytype
        try {
            return filter(this.getPrechachedResult(this.querytype));
        }catch (NonCachedObjectAvailable e) {
            //e.printStackTrace();
        }

        // creating an hql session
        this.session = createSession();

        log("user: " + this.user + "  queryType: " + this.querytype + "  searchTerm:" + this.searchTerm);

        switch (queryType) {
        case ALL:

        case SPECIES:
            resultList.addAll(searchSpecies());
            if (queryType != ALL) break;
        case COMPOUND:
            resultList.addAll(searchCompound());
            if (queryType != ALL) break;
        case FACTOR:
            resultList.addAll(searchFactor(variation, 50));
            if (queryType != ALL) break;
        case TITLE:
            resultList.addAll(searchExperiment());
            resultList.addAll(searchByID(Promt.class));
            if (queryType != ALL) break;
        case SAMPLE:
            resultList.addAll(searchByID(Sample.class));
            resultList.addAll(searchSampleLabel());
            if (queryType != ALL) break;
        default:
            break;
        }

        long end = new Date().getTime();
        debug("number of results: " + resultList.size());
        debug("query took " + (end-start) + "ms");
        // done return the result list.

        addPrecachedResult(queryType, resultList);

        resultList = filter(resultList);

        return resultList;
    }



    /**
     * search Samples.
     * @return
     */
    private Collection searchSampleLabel() {
        List sampleIDList = null;
        Collection answers = new Vector();

        // search by label or comment
        String query = "select sample.uoid as sampleID from formobject as sample, formobject as label "
            + " where sample.uoid = label.parent and (label.internalPosition between 1 and 2) and label.value like \"%" + this.searchTerm + "%\""
            + " order by sample.uoid desc limit 0, 200";



        // String queryCompoundNames = "select sample.setupXsampleID as sampleID from pubattribute as att, pubsample as sample where sample.uoid = att.parent and att.label like \"%" + searchTerm + "%\" group by sample.uoid order by sample.uoid desc";
        sampleIDList = (this.session.createSQLQuery(query).addScalar("sampleID", Hibernate.STRING).list()); 

        // creating answers out of the ids
        QueryMasterAnswer answer = null;
        long id = 0;
        for (Iterator iter = sampleIDList.iterator(); iter.hasNext();) {
            try {
                id = Long.parseLong(iter.next().toString());
                answer = new FormobjectAnswer(id);
                answers.add(answer);
            } catch (FormobjectAnswerCreationException e) {
                e.printStackTrace();
            }
        }
        return answers;
    }



    /**
     * filtering the result to information that is only visable to the user 
     * @param resultList
     */
    private List filter(List resultList) {

        QueryFilter filter = QueryFilter.newInstance(user);
        List filteredList = filter.filter(resultList);

        return filteredList;
    }



    private Collection searchFactor(String variation, int maxResults) {
        List result = new Vector();
        List variations = null;
        try {
            Criteria c = this.session.createCriteria(VariationsCache.class).add(org.hibernate.criterion.Expression.like("value", "%" + searchTerm + "%")).addOrder(Order.desc("classid")).setMaxResults(maxResults);
            try {
                if (variation != null && variation.length() > 0){
                    debug("adding Expression.eq(\"question\", " + variation + ")");
                    c.add(org.hibernate.criterion.Expression.eq("question", variation));
                }
            } catch (Exception e){
                e.printStackTrace();
            }
            debug("starting query.");
            
            variations = c.list();
            debug("number of results: " + variations.size()); 

        }catch (Exception exception){
            warning(exception);
            variations = new Vector();
        }

        if (variations.size() > 0){ 
            java.util.Iterator iter = variations.iterator();
            while (iter.hasNext()){
                try {
                    debug("creating answer");
                    result.add(new VariationAnswer((VariationsCache )iter.next(), searchTerm, variation));
                } catch (PersistenceActionFindException e) {
                    warning(e);
                } catch (FoundExperimentTwiceException e) {
                    warning(e);
                }
            }
        }
        // addPrecachedResult(FACTOR, result);
        return result;
    }    



    /**
     * looking for formobejects based on there ID. 
     * <pre>the ID has to be at least MIN_LENGTH_QUERYBYID long. </pre>
     * @see #MIN_LENGTH_QUERYBYID
     * @param classToLookFor
     * @return
     */
    private Collection searchByID(Class classToLookFor) {
        try {
            return this.getPrechachedResult(ID);
        } catch (Exception e) {
        }
        showTime("start - searchByID");
        Collection result = new Vector();

        // in case it is a very short string
        if (this.searchTerm.length() < MIN_LENGTH_QUERYBYID) {
            debug("skipping searchByID search: searchTerm to short " + this.searchTerm.length());
            return result;
        }

        try {
            Integer.parseInt(this.searchTerm);
            String basic =  "select * from formobject where discriminator = \"" + classToLookFor.getName() + "\" and uoid like \"%" + this.searchTerm + "%\" limit 0, 200";
            log(basic);
            Iterator iterator = this.session.createSQLQuery(basic).addScalar("uoid", org.hibernate.Hibernate.LONG).list().iterator();
            while (iterator.hasNext()) {
                long id = Long.parseLong(iterator.next().toString());
                QueryMasterAnswer answer = new FormobjectAnswer(id);
                result.add(answer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        showTime("end - searchByID");
        addPrecachedResult(ID, result);
        return result;
    }


    private Collection searchExperiment() {
        try {
            return this.getPrechachedResult(TITLE);
        } catch (Exception e) {
        }

        showTime("start - searchExperiment");

        // searching by Experiment ID by SampleID
        Vector result = new Vector();
        try {
            // in case it is a very short string
            if (this.searchTerm.length() >= MIN_LENGTH_QUERYBYID) {
                FormobjectAnswer answer;
                answer = new FormobjectAnswer(new SXQuery().findPromtIDbySample(Long.parseLong(this.searchTerm)));
                result.add(answer);
            } else {
                debug("skipping searchExperiment search: searchTerm to short " + this.searchTerm.length());
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (PersistenceActionFindException e) {
            e.printStackTrace();
        } catch (FormobjectAnswerCreationException e) {
            e.printStackTrace();
        }


        // searching by title and abstract
        String basic  = "select promt.uoid from formobject as promt, " + 
        "formobject as page, " + 
        "formobject as element1 , " + 
        "formobject as element2 " + 
        "where promt.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.Promt\" " + 
        "and page.parent = promt.uoid " + 
        "and element1.parent = page.uoid " + 
        "and element2.parent = page.uoid " + 
        "and element1.question = \"Title\" " + 
        "and element2.question = \"abstract\" " + 
        "and (element1.value LIKE \"%" + this.searchTerm + "%\" OR element2.value LIKE \"%" + this.searchTerm + "%\") " +
        "order by promt.uoid DESC " +
        "limit 0, 200";
        List promtIDs = null;
        try {
            promtIDs = session.createSQLQuery(basic).addScalar("promt.uoid", org.hibernate.Hibernate.STRING).list();
        } catch (Exception exception){
            //result = new Vector();
        }

        java.util.Iterator iter = promtIDs.iterator();
        while (iter.hasNext()){
            try {
                long promtID = Integer.parseInt(iter.next().toString());
                QueryMasterAnswer answer = new FormobjectAnswer(promtID);
                result.add(answer);
            } catch (Exception e4){
            }
        }

        showTime("end - searchExperiment");
        // by id
        addPrecachedResult(TITLE, result);
        return result;
    }


    /**
     * search for compounds. Anwers will also contain how often the compound was found in samples.
     * @return
     */
    private Collection searchCompound() {
        try {
            return this.getPrechachedResult(COMPOUND);
        } catch (Exception e) {
        }
        showTime("start - searchCompound");

        List result = new Vector();
        String queryCompoundNames = "select label from pubattribute where label like \"%" + searchTerm + "%\" group by label";
        List compoudNameslist = this.session.createSQLQuery(queryCompoundNames).addScalar("label", Hibernate.STRING).list(); 
        Iterator compoudNamesiterator = compoudNameslist.iterator();

        // creating an anser
        while (compoudNamesiterator.hasNext()){
            String label = compoudNamesiterator.next().toString();

            // finding how often it occured
            String query_count = "select count(*) as c from pubattribute where label = \"" + label + "\"";
            String occourance = this.session.createSQLQuery(query_count).addScalar("c", Hibernate.INTEGER).list().get(0).toString();

            QueryMasterAnswer answer = new CompoundQueryMasterAnswer(label, occourance);
            result.add(answer);
        }
        showTime("end - searchCompound");
        addPrecachedResult(COMPOUND, result);
        return result;
    }




    /**
     * adding a result to the chache
     * @param compound2
     * @param result
     */
    private void addPrecachedResult(int querytype, Collection result) {
        QueryMasterCache.add(this.searchTerm, querytype, result);
    }



    /**
     * searching for species.
     * not super nice - should be optimized that the ncbientries are not created twice
     */
    private Collection searchSpecies() {
        try {
            return this.getPrechachedResult(SPECIES);
        } catch (Exception e) {
        }
        showTime("start - searchSpecies");

        // result 
        List result = new Vector();

        showTime();
        // the query
        String querySpeciesNames = "select label.value as ncbilabel from formobject as ncbi, formobject as label where ncbi.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\" and label.PARENT = ncbi.uoid and label.value != \"\""
            + " and label.internalPosition = 0 "
            + " and label.value like \"%" + this.searchTerm + "%\" "
            + " group by label.value ORDER by label.value ";
        log(querySpeciesNames);

        // get all species existing in the system
        List speciesNamesList = this.session.createSQLQuery(querySpeciesNames).addScalar("ncbilabel", org.hibernate.Hibernate.STRING).list();

        // checking if it is an NCBI term that has not been used in here yet
        try {
            String _speciesTMPname = NCBIConnector.determineNCBI_Information(
                    NCBIConnector.determineNCBI_Id(searchTerm)
            ).getName();
            if (! speciesNamesList.contains(_speciesTMPname) && _speciesTMPname.compareTo("unidentified") != 0) speciesNamesList.add(_speciesTMPname);
        } catch (Exception e){

        }

        // filter it 
        Iterator speciesNamesiterator = speciesNamesList.iterator();        
        if (speciesNamesList.size() > 0 && speciesNamesList.get(0).toString().compareTo("unidentified") != 0){
            while (speciesNamesiterator.hasNext()){
                // creating the resultobject
                String speciesNameString = speciesNamesiterator.next().toString();

                try {
                    result.add(new NCBIQueryMasterAnswer(NCBIConnector.determineNCBI_Id(speciesNameString)));
                } catch (NCBIException e) {
                    e.printStackTrace();
                }
            }
        }

        // done
        addPrecachedResult(SPECIES, result);
        showTime("end - searchSpecies");
        return result;
    }


    /**
     * myQueryType is beeing used because the {@link #querytype} can also be 99 and would not find precached seperate results
     * 
     * @param myQueryType
     * @return
     * @throws NonCachedObjectAvailable
     */
    private List getPrechachedResult(final int myQueryType) throws NonCachedObjectAvailable {
        debug("requesting precached Result for \"" + this.searchTerm + "\"  \"" +  myQueryType + "\" from Cache");
        QueryMasterCache.showContent();

        // checking the cache 
        if (QueryMasterCache.containsTerm(this.searchTerm , myQueryType)){
            debug("there is a precached result for query \"" + this.searchTerm + "\" type: " + myQueryType);
            return QueryMasterCache.get(this.searchTerm , myQueryType);
        } else {
            warning(this, "there is a no precached result for that query \"" + this.searchTerm + "\" type: " + myQueryType);
            throw new NonCachedObjectAvailable();
        }
    }


    /** debugging: 
     * show a time and set it to current time.
     * @see #timer
     */
    private void showTime(String comment) {
        long tmpTime = new Date().getTime();
        if (this.timer != 0) log("timer: " + (tmpTime - this.timer) + "  "  + comment);
        this.timer = tmpTime;
    }

    private void showTime() {
        showTime("");
    }



    /**
     * testing...
     */
    public static void  testME(){
        QueryMaster queryMaster = new QueryMaster();

        UserDO user = null;

        int[] searchType = new int[]{FACTOR}; //, COMPOUND, TITLE, SAMPLE, SPECIES};
        String[] searchTerms = new String[]{"Arab", "human", "gly", "29518", "cow", "mus", "FSA", "blood"};


        for (int a = 0; a < 30; a++){
            for (int b = 0; b < searchTerms.length; b++){
                String term = searchTerms[b];

                for (int i = 0; i < searchType.length; i++) {
                    int type = searchType[i];

                    log(thiZ, "--------------------------------------------------------------");
                    log(thiZ, "--   starting query: searchtype: " + type + "  " + term + "--");
                    List response = queryMaster.search(user, type, term);
                    Iterator results = response.iterator();
                    while (results.hasNext()) {
                        QueryMasterAnswer answer = (QueryMasterAnswer) results.next();
                        log(answer, answer.toString());
                    }
                    log(thiZ, "--   end query: searchtype: " + type + "  " + term + "--");
                    log(thiZ, "--------------------------------------------------------------");
                }
            }
        }
        
        try {
            TestingTheQueries.testIt();
        } catch (PersistenceActionFindException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    /**
     * @see #search(UserDO, int, String)
     */
    public List search(UserDO user2, int type, String term) {
        return search(user2, type, term, "");
    }



    public static String getCacheMessage(){
        return QueryMasterCache.showContent();
    }


    /**
     * init the cache with the most recent values from the system.
     */
    public static void reinitCache() {
        // reset the cache
        QueryMaster.cache = new QueryMasterCache();
        
        // find the last used terms
        String accessUserQuery = "select a.date as date , a.querytype as querytype, a.searchTerm , (select count(b.searchTerm) from query as b where b.searchTerm = a.searchterm) as count, (select u.displayName from user as u where u.uoid = a.userID ) as username from query as a order by date desc LIMIT 100";
//        String accessUserQuery = new SQLStatementPool(thiZ).getStatement("access_user").toString();
        
        List accessUser = CoreObject.createSession().createSQLQuery(accessUserQuery)
            .addScalar("date", Hibernate.LONG)
            .addScalar("querytype", Hibernate.LONG)
            .addScalar("searchterm", Hibernate.STRING)
            .addScalar("count", Hibernate.LONG)
            .addScalar("username", Hibernate.STRING)
            .list(); 
        Iterator accessIter = accessUser.iterator();
        while (accessIter.hasNext()){
            Object[] o = (Object[])accessIter.next();

            String term = "" + o[2];
            int type = Integer.parseInt("" + o[1]);
            
            new QueryMaster().search(null, type, term);
        }

        
        // submit a search for each 
    }
}
