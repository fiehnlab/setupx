/**
 * ============================================================================ File:    Restriction4Percent.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.web.forms.inputfield.InputField;


/**
 * Restriction that the value of an inputfield must be in percent.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 *
 * @hibernate.subclass
 */
public class Restriction4Percent extends Restriction4Int {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Restriction4Percent object.
   */
  public Restriction4Percent(InputField field) {
    this.setStartRange(0);
    this.setEndRange(100);
  }

  /**
   * Creates a new Restriction4Percent object.
   */
  public Restriction4Percent() {
    super();
  }
}
