/**
 * ============================================================================ File:    Restriction4Date.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.ValidationAnswer;

import java.text.DateFormat;
import java.text.ParseException;

import java.util.Date;
import java.util.Locale;


/**
 * Restriction for Dates
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.5 $
 *
 * @hibernate.subclass
 */
public class Restriction4Date extends Restriction {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Date dateAfter;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static DateFormat[] dateFormats;
  private static Locale locale = Locale.US;
  private static int DEFAULT = 1;

  static {
    dateFormats = new DateFormat[5];
    dateFormats[0] = DateFormat.getDateInstance();
    dateFormats[1] = DateFormat.getDateInstance(DateFormat.SHORT, locale);
    dateFormats[2] = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);
    dateFormats[3] = DateFormat.getDateInstance(DateFormat.LONG, locale);
    dateFormats[4] = DateFormat.getDateInstance(DateFormat.FULL, locale);
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Restriction4Date object.
   *
   * @param inputfield related inputfield
   * @param _dateBevor date which must be after the checked date
   */
  public Restriction4Date(final Date _dateAfter) {
    this.dateAfter = (_dateAfter);
  }

  /**
   * Creates a new Restriction4Date object.
   */
  public Restriction4Date() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param dateAfter TODO
   */
  public void setDateAfter(Date dateAfter) {
    this.dateAfter = dateAfter;
  }

  /**
   * @return
   *
   * @hibernate.property type = "java.util.Date"
   */
  public Date getDateAfter() {
    return dateAfter;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#cloneRestriction()
   */
  public Restriction cloneRestriction() {
    return new Restriction4Date(this.dateAfter);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#validateObject()
   */
  public ValidationAnswer validateObject(InputField field) {
    String dateString = field.getValue().toString();

    Date date = null;

    boolean loop = true;

    for (int i = 0; loop && (i < dateFormats.length); i++) {
      try {
        date = dateFormats[i].parse(dateString);
        loop = false;
      } catch (ParseException e) {
      }
    }

    if (date == null) {
      return new ValidationAnswer(field, "The date " + dateString + " can not be recognised as a date. Format:(" + dateFormats[DEFAULT].format(new Date()) + ")");
    }

    setDateField(field, date);

    if (this.dateAfter.before(date)) {
      return new ValidationAnswer(field, "The date " + dateFormats[DEFAULT].format(date) + " must be before the " + dateFormats[DEFAULT].format(dateAfter));
    }

    return null;
  }

  /**
   * resets the value of the filed to an accepted format
   */
  private static void setDateField(InputField formobject, Date date) {
    formobject.setValue(dateFormats[DEFAULT].format(date));
  }
}
