package org.setupx.repository.server.db;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class UnknownDocumentTypeException extends Exception {
  /**
   *
   */
  public UnknownDocumentTypeException() {
    super();

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public UnknownDocumentTypeException(String arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public UnknownDocumentTypeException(Throwable arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   */
  public UnknownDocumentTypeException(String arg0, Throwable arg1) {
    super(arg0, arg1);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param docType
   */
  public UnknownDocumentTypeException(int docType) {
    super("the doctype is unknown: " + docType);
  }
}
