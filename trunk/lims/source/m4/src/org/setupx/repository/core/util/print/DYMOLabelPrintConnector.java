package org.setupx.repository.core.util.print;
/**
 * ============================================================================ 
 *  Martin Scholz  
 * 
 *  Copyright (C) 2007 Martin Scholz. 
 *  All rights reserved. 
 *  For more information please see http://fiehnlab.ucdavis.edu/ 
 * ============================================================================
 */


import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.print.PrintService;


/**
 * Print Labels for DYMO LabelWriter 400
 *
 * @author <a href=mailto:mscholz@ucdavis.edu>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class DYMOLabelPrintConnector implements Printable {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** name of the printer as known in the settings. 
   * If you dont know the exact name, leave it empty 
   * and set PRINTMENU to true - this will  activate the menu. */
  public static final String PRINTERNAME = "DYMO LabelWriter 400";

  /** true if you want a menu to select the printer */
  public static boolean PRINTMENU = false;

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * GO !
   */


  /**
   * overwrite this method in order to get other values. The current implementation will generate lables with 5 lines.
   * 
   * <ol>
   * <li>
   * "SetupX"
   * </li>
   * <li>
   * "fiehnlab.ucd"
   * </li>
   * <li>
   * "id" + the current counter number
   * </li>
   * <li>
   * "label" + the position
   * </li>
   * <li>
   * the current date
   * </li>
   * </ol>
   * 
   *
   * @param elementOnLabel position of the field ON the label.
   * @param labelCounter counter of ALL labels - first label is 0
   *
   * @return the value for the specific field
   */
  public String getValue(final int elementOnLabel, final int labelCounter) {
    String value = "";
 
    switch (elementOnLabel) {
    case 0:
      // what ever you want to have in this line
      value = "SetupX";

      break;

    case 1:
        // what ever you want to have in this line
      value = "fiehnlab.ucd";

      break;

    case 2:
        // what ever you want to have in this line
      value = "id: " + labelCounter;

      break;

    case 3:
        // what ever you want to have in this line
      // TODO - add DB connection
      value = "label:" + elementOnLabel;

      break;

    case 4:
        // what ever you want to have in this line
      value = DateFormat.getDateInstance(DateFormat.SHORT, Locale.US).format(new Date());

      break;

    default:
      break;
    }

    return value;
  }

  /*
   * (non-Javadoc)
   * @see java.awt.print.Printable#print(java.awt.Graphics, java.awt.print.PageFormat, int)
   */
  public int print(Graphics graphics, PageFormat pageFormat, int pageIndex)
    throws PrinterException {
    System.out.println("printing page: " + pageIndex + "/" + getPageNumbers());
    
    if (pageIndex <= getPageNumbers() ) {
      Graphics2D g = (Graphics2D) graphics;
      
//      g.getFont().getSize();
      
      // g.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
      g.translate(15, 8);
      //g.translate(10, 5);

      String value = "";
      pageIndex = pageIndex + 1;
      
      // specific for four circular labels per page
      for (int x = 0; x < 80; x = x + 50) {
        for (int y = 0; y < 80; y = y + 36) {
          int posOnPage = 4; // BottomRight, TopRight, BottomLeft, TopLeft  

          if (x > 0) {
            posOnPage = posOnPage - 2;
          }

          if (y > 0) {
            posOnPage = posOnPage - 1;
          }

          // current counter for the label.
          int id = (posOnPage - 1) + ((pageIndex - 1) * 4);

          // setupx
          g.setFont(new Font(g.getFont().getFontName(), g.getFont().getStyle(), 3));
          value = this.getValue(0, id);
          g.drawString("       " + value, x, y);

          // fiehnlab
          g.setFont(new Font(g.getFont().getFontName(), g.getFont().getStyle(), 3));
          value = this.getValue(1, id);
          g.drawString("     " + value, x, y + 4);
          
          int style = g.getFont().getStyle();

          // ID
          g.setFont(new Font(g.getFont().getFontName(), style, 6));
          value = this.getValue(2, id);
          g.drawString(" " + value, x, y + 10);

          // label
          //g.setFont(new Font(g.getFont().getFontName(), style , 3));
          g.setFont(new Font(g.getFont().getFontName(), Font.BOLD , 3));
          value = this.getValue(3, id);
          if (value == null) value = "---";
          if (value.length() > 11){
              g.drawString("  " + value.substring(0,11), x, y + 14);
              g.drawString("    " + value.substring(11, value.length()), x, y + 17);
          } else {
              g.drawString("  " + value, x, y + 16);
          }

          // date
          g.setFont(new Font(g.getFont().getFontName(), style , 3));
          value = this.getValue(4, id);
          g.drawString("      " + value, x, y + 21);
        }
      }

      return PAGE_EXISTS;
    } else {
      return NO_SUCH_PAGE;
    }
  }


/**
   * 
   * @return index of the last page.
   */
public int getPageNumbers() {
    return 5;
}
}
