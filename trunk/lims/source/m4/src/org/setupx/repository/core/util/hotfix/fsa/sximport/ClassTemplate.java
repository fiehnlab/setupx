/**
 * ============================================================================ File:    ClassTemplate.java Package: org.setupx.repository.core.util.hotfix.fsa.sximport cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.hotfix.fsa.sximport;

class ClassTemplate extends Template {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  String oldClassID = "";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ClassTemplate object.
   *
   * @param sxID 
   * @param oldclassID 
   */
  public ClassTemplate(int sxID, String oldclassID) {
    setID(sxID);
    setOldClassID(oldclassID);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param oldClassID TODO
   */
  public void setOldClassID(String oldClassID) {
    this.oldClassID = oldClassID;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getOldClassID() {
    return oldClassID;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String toString() {
    return this.getID() + " " + this.getOldClassID();
  }
}
