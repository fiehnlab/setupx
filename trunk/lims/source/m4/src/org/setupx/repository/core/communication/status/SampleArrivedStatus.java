package org.setupx.repository.core.communication.status;

import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.server.persistence.PersistenceActionFindException;

/**
 * @hibernate.subclass
 */

public class SampleArrivedStatus extends PersistentStatus {

    private String location;

    public SampleArrivedStatus() {
        super();
    }

    public SampleArrivedStatus(long sampleID, UserDO userDO, String message, String location) throws StatusPersistenceException {
        super(sampleID, userDO, message);
        this.setLocation(location);
    }

    /**
     * @hibernate.property
     * @return
     */
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    
    public String createDisplayString() {
        return "sample has arrived";
    }

    public PersistentStatus createClone() {
        try {
            return new SampleArrivedStatus(this.sampleID, this.getUser(getUserID()), this.getMessage(), this.getLocation());
        } catch (StatusPersistenceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (PersistenceActionFindException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }}
