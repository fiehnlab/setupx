package org.setupx.repository.server.workflow.elements;

import org.setupx.repository.core.InitException;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.workflow.WorkFlowException;


/**
 * Sample Process
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.2 $
 */
public class TeschtingProcess extends Process {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new TeschtingProcess object.
   *
   * @param configuration 
   *
   * @throws InitException 
   * @throws WorkFlowException 
   */
  public TeschtingProcess(ProcessConfiguration configuration)
    throws InitException, WorkFlowException {
    super(configuration);
  }

  /**
   * Creates a new TeschtingProcess object.
   *
   * @throws InitException 
   * @throws WorkFlowException 
   */
  public TeschtingProcess() throws InitException, WorkFlowException {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   * (non-Javadoc)
   *
   * @see org.setupx.repository.server.workflow.Process#action()
   */
  public void action() throws ProcessingException {
    Logger.log(this, "action is called for process : " + this.getConfiguration().getId() + "[" + this.getConfiguration().getLabel() + "]");
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.WFElement#getDataInKeys()
   */
  protected String[] getDataInKeys() {
    return none();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.WFElement#getDataOutKeys()
   */
  protected String[] getDataOutKeys() {
    return new String[] { MailProcess.MAILADDRESS };
  }
}
