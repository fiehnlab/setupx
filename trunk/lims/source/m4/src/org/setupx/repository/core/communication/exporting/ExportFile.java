/**
 * ============================================================================ File:    ExportFile.java Package: org.setupx.repository.core.communication.exporting cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.exporting;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.File;

import java.io.FileNotFoundException;
import java.io.IOException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
abstract class ExportFile extends CoreObject {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ExportFile object.
   */
  public ExportFile() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public abstract String getContent();

  /**
   * TODO: 
   *
   * @param file TODO
   *
   * @throws FileNotFoundException TODO
   * @throws IOException TODO
   */
  public void saveTo(java.io.File file) throws FileNotFoundException, IOException {
    if (!file.exists()) {
      file.createNewFile();
    }

    File.storeData2File(file, getContent());
  }

  /**
   * TODO: 
   *
   * @param fileName TODO
   *
   * @throws FileNotFoundException TODO
   * @throws IOException TODO
   */
  public void saveTo(String fileName) throws FileNotFoundException, IOException {
    debug("saving to " + fileName);
    saveTo(new java.io.File(fileName));
  }
}
