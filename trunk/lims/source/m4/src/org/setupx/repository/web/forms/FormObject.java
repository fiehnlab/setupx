/**
 * ============================================================================ File:    FormObject.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import org.hibernate.LazyInitializationException;
import org.hibernate.Session;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.msi.MSIAttribute;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;
import org.setupx.repository.server.persistence.hibernate.SXSerializable;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.forms.clazzes.ClazzDimensionDetector;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.Removable;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;
import org.setupx.repository.web.forms.inputfield.multi.ExpandingField;
import org.setupx.repository.web.forms.inputfield.multi.GenotypeInputfield;
import org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz;
import org.setupx.repository.web.forms.inputfield.multi.MultiFieldClazzes;
import org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.Sample;
import org.setupx.repository.web.forms.inputfield.multi.SpeciesInputfield2;
import org.setupx.repository.web.forms.inputfield.multi.TreatmentInputfield;
import org.setupx.repository.web.forms.inputfield.multi.UserInputfield;
import org.setupx.repository.web.forms.page.Page;
import org.setupx.repository.web.forms.relation.AbstractRelation;
import org.setupx.repository.web.forms.relation.Relation;
import org.setupx.repository.web.forms.restriction.Restriction;


/**
 * Super Object of all element in the Forms.
 * <pre><B>TODO</B> - extract the public array operations into the interface ... and make these internal like addInternalField</pre>
 * 
 * <p><h3>UPDATE:</h3>
 * <b>Oct 2007:</b> Adding MSI attribute to all Formobjects in order to allow mapping to MSI.
 * 
 *</p>
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.23 $
 *
 * @hibernate.class table = "formobject"
 * @hibernate.discriminator column = "discriminator"
 */
public abstract class FormObject extends CoreObject implements SXSerializable {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public String helptext = "";

  /** are there <code><strong>HR</strong></code>in the <code>HTML</code> representaion. */
  public boolean dividerHR = false;

  /** @label the parent object */
  private FormObject parent;

  /** a cached version of the rootobject */
  private FormObject root_cached = null;
  
  private String description = "";

  /** description of this object */
  private String longDescription = "";

  private String name = "";

  /**
   * link to an MSI attribute
   */
  private MSIAttribute msiAttribute;
  
  /**
   * the actual field contained inside this. The values are only beeing set by field that can contain other fields like the MultiFields.
   *
   * @see FormContainer
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField
   */
  private FormObject[] fields = new FormObject[0];

  /** if the object is active or not - depending on this:  <br>if shown in the gui <br>if used for validation  <br>... */
  public boolean active = true;

  /** position of the object inside the array of the parent object - <b>ONLY FOR DB internal use</b> */
  private int internalPosition = 0;


  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String resizeYminus = "pics/resize_y_minus.gif";
  public static final String resizeYplus = "pics/resize_y_plus.gif";
  public static final String resizeXminus = "pics/resize_x_minus.gif";
  public static final String resizeXplus = "pics/resize_x_plus.gif";
  public static final String removeIMG = "<img  border='0' src=\"pics/trash.gif\"/>";
  public static final String helpIMG = "<img  border='0' src=\"pics/help_small.gif\"/>";
  public static final String linkIMG = "<img  border='0' src=\"pics/link.gif\"/>";
  public static final String IMG_UNVALID = "<img  border='0' src=\"pics/x.gif\"/>";
  public static final String IMG_ERR = "<img  border='0' src=\"pics/error.gif\"/>";
  public static final String IMG_INFO = "<img  border='0' src=\"pics/info.gif\"/>";
  public static final String IMG_REQUIERED = "<img  border='0' src=\"pics/requiered.gif\"/>";
  public static final String IMG_VALID = "<img  border='0' src=\"pics/check.gif\"/>";
  public static final String SEPERATOR = " ";

  /** label being used for default init. */
  public static final String DEFAULT_LABEL = "";

  /** TODO:  */
  private static Set resultSetMapping;

  {
    //debug(this, "new Instance");
    setDescription("description(" + DEFAULT_LABEL + ")");
    setLongDescription("longdescription(" + DEFAULT_LABEL + ")");
    setName("name(" + DEFAULT_LABEL + ")");
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new FormObject object.
   *
   * @param name 
   * @param description 
   * @param longDescription 
   */
  public FormObject(String name, String description, String longDescription) {
    super();
    setDescription(description);
    setLongDescription(longDescription);
    setName(name);
  }

  /**
   * Creates a new FormObject object.
   *
   * @param name 
   * @param description 
   *
   * @deprecated use the longer constructors supporting the longer description
   */
  public FormObject(String name, String description) {
    this(name, description, "");
  }

  /**
   * @deprecated ONLY USED BY CASTOR !!!
   */
  public FormObject() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public Hashtable getAttributes() {
    Hashtable hashtable = new Hashtable();
    hashtable.put("active", "" + this.active);
    hashtable.put("describption", this.getDescription());
    hashtable.put("longdescription", this.getLongDescription());
    hashtable.put("name", getName());

    return hashtable;
  }

  /**
   * @param description the description of the object
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * returns the description of the object
   *
   * @return the description of the object
   *
   * @hibernate.property
   */
  public String getDescription() {
    return description;
  }

  /**
   * return formobject from a defined position in the array
   * <pre>only objects contained inside <B>this</b> will be returned!</pre>
   *
   * @param i the position in the array
   *
   * @return formobject from a defined position in the array
   */
  public FormObject getField(int i) {
    if (this.fields.length <= i) {
      if (Config.LOGGING_SHOW_MISSING_FIELDS) {
        throw new ArrayIndexOutOfBoundsException("the position " + i + " is not available. The size of the array is : " + this.fields.length);
      }
    }

    return this.fields[i];
  }

  /**
   * formobject with the <B>key</B> from the array
   * 
   * <p>
   * it can happen that inside an field there are other field - these objects  containing other do all implement FormContainer.
   * </p>
   *
   * @param key key of the object
   *
   * @return the object - null if it is <b>NOT</b> found
   *
   * @see ExpandingField
   * @see FormContainer
   */
  public final FormObject getField(String key) {
    for (int i = 0; i < this.getFields().length; i++) {
      FormObject object = this.getFields()[i];
      //debug(this, "comparing: " + object.getName() +  " vs. " + key);

      if (object.getName().compareTo(key) == 0) {
        //debug(this, "object: [" + object.getName() + "] found inside object: " + this.getName());

        return object;
      }

      // checking if the object itself contains fields - like the FormContainer
      if (object instanceof FormContainer) {
        // recursive call of this mehtod !!
        FormObject formObject = object.getField(key);

        if (formObject != null) {
          return formObject;
        }
      }
    }

    return null;
  }

  /**
   * TODO: 
   *
   * @param class1 TODO
   * @param vec TODO
   */
  public final void getField(Class class1, Vector vec) {
    for (int i = 0; i < this.getFields().length; i++) {
      FormObject object = this.getFields()[i];

      if (object == null) {
        warning(this, " contains an empty Formobject.");
      }

      try {
        if (object.getClass().getName().compareTo(class1.getName()) == 0) {
          //debug(this, "object: [" + object.getName() + "] found inside object: " + this.getName());
          vec.add(object);
        }

        // checking if the object itself contains fields - like the FormContainer
        if (object instanceof FormContainer) {
          // recursive call of this mehtod !!
          object.getField(class1, vec);
        }
      } catch (NullPointerException e) {
        warning(this, "unable to find field - " + e.getLocalizedMessage());
      }
    }
  }

  /**
   * return the position of that specific page in the fieldarray
   *
   * @param firstName
   *
   * @return <p><p>
   */
  public final int getFieldPos(String key) {
    for (int i = 0; i < this.getFields().length; i++) {
      FormObject object = this.getFields()[i];

      if (object.getName().compareTo(key) == 0) {
        //debug(this, "found object : " + object.getName() + " " + key + "  found in: " + this.getName());
        return i;
      }
    }

    return 9999;
  }

  /**
   * @see FormContainer#setFields(FormObject[])
   */
  public void setFields(FormObject[] formObjects) {
    // only in case it is a FormContainer
    if (!(this instanceof FormContainer)) {
      //debug("ignore setFields - cause not a Formcontainer");
      return;
    }

    this.fields = formObjects;

    // check if there are any active fields in it - otherwise - it ll be deactivated too
    boolean activationTOBE = false;

    if (this.fields.length > 0){
        //debug("setFields(FormObject[] formObjects):   determine if object should be active or not: START --->");
        for (int i = 0; (i < this.fields.length) && !activationTOBE; i++) {
          try {
              activationTOBE = this.fields[i].active;
              //debug("setFields(FormObject[] formObjects):   checking field: " + i + " result: " + activationTOBE);
          } catch (NullPointerException e) {
            warning(this, e.toString());
          }
        }
        //debug("setFields(FormObject[] formObjects):   result after checking my fields: " + activationTOBE + "  <---- END");
    } else {
        activationTOBE = false;
        //debug("setFields(FormObject[] formObjects):   activate will be "  + activationTOBE + " because there are " + this.fields.length + " objects inside.");
    }

    //debug("setFields(FormObject[] formObjects):   result after checking the number of fields: " + activationTOBE);

    setActive(activationTOBE);

    this.updateParents();
  }

  /**
   * return all FormObjects (childobjects) related to this object
   *
   * @return all FormObjects (childobjects) related to this object
   */
  public FormObject[] getFields() {
    if (this.fields == null) {
      warning(this, "the fields are not initalized");
      this.fields = new FormObject[0];
    }

    return this.fields;
  }

  /**
   * Hibernate setter.
   *
   * @param set TODO
   */
  public void setFieldsHibernate(Set set) {
      //if (set.size() > 0) debug("setFieldsHibernate(Set set) " + set.size());

    // checking size
    int tmpSize = set.size();

    // mapping the fields
    this.setFields(map(set));

    // checking size
    if (this.getFields().length != tmpSize) {
      throw new RuntimeException("error setting fields - wrong size");
    }
  }

  /**
   * @hibernate.set lazy="false" role="FieldsHibernate"
   * @hibernate.collection-key column="PARENT"
   * @hibernate.collection-one-to-many  class="org.setupx.repository.web.forms.FormObject"
   */
  public Set getFieldsHibernate() {
    // updateParents();
    return map(this.getFields());
  }

  /**
   * sets the help text
   *
   * @param helptext the helptxt
   */
  public void setHelptext(String helptext) {
    this.helptext = helptext;
  }

  /**
   * returns the helptext
   *
   * @return helptext
   *
   * @hibernate.property
   */
  public String getHelptext() {
    return helptext;
  }

  /**
   * position of the object inside the array of the parent object - <b>ONLY FOR DB internal use</b>
   */
  public void setInternalPosition(int in_internalPosition) {
    /*if (in_internalPosition != this.internalPosition) {
       debug("chaning internal position");
       }
     */
    this.internalPosition = in_internalPosition;
  }

  /**
   * position of the object inside the array of the parent object - <b>ONLY FOR DB internal use</b>
   *
   * @hibernate.property
   */
  public int getInternalPosition() {
    // checking parent 
    try {
      int i;
      i = this.getPosThis();
      this.internalPosition = i;
    } catch (ParentNotAvailableException e) {
      //debug(this, e);
      //debug(this, "leaving internalPosition untouched");
    }

    return internalPosition;
  }

  /**
   * @return long description
   *
   * @hibernate.property
   */
  public String getLongDescription() {
    return longDescription;
  }

  /**
   * set the name of this object
   *
   * @param name2 name of this object
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return name of this object
   *
   * @hibernate.property
   */
  public String getName() {
    return this.name;
  }

  /**
   * parent item
   *
   * @hibernate.many-to-one column="PARENT" class = "org.setupx.repository.web.forms.FormObject"
   */
  public FormObject getParent() {
    if (this.parent == null) {
      this.testParentField();
    }

    return this.parent;
  }

  /**
   * muss geaendert werden !!!     retrun thisobjects position
   *
   * @return position
   *
   * @throws ParentNotAvailableException
   *
   * @see FormObject#getFieldPos(String)
   */
  public int getPosThis() throws ParentNotAvailableException {
    try {
      int value = this.getParent().getFieldPos(this.getName());

      if (value == 9999) {
        throw new NullPointerException("can not be found");
      } else {
        return value;
      }
    } catch (Exception e) {
      throw new ParentNotAvailableException();
    }
  }

  /**
   * The setter method for Unique Object identifier.
   */
  public void setUOID(long lId) {
    uoid = lId;
  }

  /**
   * The getter method for Unique Object identifier.
   * 
   * <p>
   * The id is generated by the persistence layer.
   * </p>
   *
   * @return unique id for this object
   *
   * @hibernate.id column = "uoid" generator-class="native"
   */
  public long getUOID() {
    return (uoid);
  }

  /**
   * Creates an HTML representation of this object. The representation depends on the tpye of the object.
   * 
   * <p>
   * The result contains a whole line - including <code>TR</code> and the the number of <code>TD</code> as defined by parameter <code>numberOfTD</code>.
   * </p>
   *
   * @param numberOfTD number of TDs
   *
   * @return an HTML representation of this object
   *
   * @deprecated - look at jsps but still in use for a lot of objects
   */
  public abstract String createHTML(int numberOfTD);

  /**
   * TODO: 
   *
   * @param vector TODO
   */
  public void findUserInputfields(Vector vector) {
    if (this instanceof UserInputfield) {
      vector.add(this);
    }

    // if not instanceof check the childs
    for (int i = 0; i < this.fields.length; i++) {
      this.getField(i).findUserInputfields(vector);
    }
  }

  /**
   * creates a short version of this object containing information for this object
   * 
   * <p>
   * <b>the short version is as short as possible</b>
   * </p>
   *
   * @return a short version of this object containing information for this object
   */
  public abstract String toStringHTML();

  /**
   * set the status of this object
   *
   * @param active true or false
   */
  public void setActive(boolean active) {
    /*
    if (active != this.active) {
      debug("changing active status of " + this.getUOID() + " from " + this.active +  " to "  + active + " combined status for this is: " + this.isActive());
    }
    */
    
    
    
    /*
     * in case parent is active and 
     * in case the parent is multifield4clazz and
     * the number of element in this is 1
    if (this.parent instanceof MultiField4Clazz && !this.parent.active && this.getParent().getFields().length == 1){
        // DANGEROUS
        warning(this, "this.parent is a MultiField4Clazz");
        
        // HOTFIX: FOR GENOTYPE
        boolean loop = false;
        // making the parent active too
        if (this.parent instanceof GenotypeInputfield){
            loop = true;
        }
        
        FormObject ob = this;
        while (loop){
            ob.active = true;
            warning(this, "changing " + Util.getClassName(ob));
            
            // checking objects parent
            try {
                ob.parent.isActive();
                warning(this, Util.getClassName(ob) + " parent: " + Util.getClassName(ob.parent) + " is active: " + ob.parent.active);
                if (!ob.parent.active){
                    ob = ob.parent;
                } else {
                    loop = false;
                }
            }catch (NullPointerException e) {
                e.printStackTrace();
                // no parent
                loop = false;
            }
        }
    }
     */
    
    this.active = active;
    
    // super.isActive() kann weiterhin false sein, da super von diesem ebenfalls false ist.
    /*
     if (this.isActive() != active && this.getDescription().compareTo("type in the name of the genotype") == 0){
        
        warning(this, "[" + this.getUOID()+  "] problem: The field can not be activated. The status has not changed after calling setActive(" + active + ")");
        throw new RuntimeException("unable to store");
    }
    */
  }

  /**
   * is the object <b>active</b> ?
   *
   * @hibernate.property
   */
  public final boolean isActive() {
    //debug(this, "checking active");
    if (this instanceof Promt) {
      //debug(this, "promt is always active ");
      return true;
    } else if (this instanceof Clazz) {
      //debug(this, "class is active: " +this.active);
      return this.active;
    }

    boolean super_acitve = true;
    boolean sub_active = true;

    /* FIXME - is a loop
       if (this instanceof Page) {
           sub_active = false;
           debug("trying to find out if the sub_object are active");
           for (int i = 0; i < this.getFields().length; i++) {
               if (this.getField(i).isActive()){
                   sub_active = true;
               }
           }
           debug("result: " + sub_active);
       }
     */
    try {
      if (!(this.getParent() instanceof Promt)) {
        super_acitve = this.getParent().isActive();
      }
    } catch (Exception e) {
      if (this instanceof Promt) {
      } else {
        //e.printStackTrace();
        if (Config.LOGGING_SHOW_MISSING_ROOT) {
          debug(this, "isActive(): " + e);
        }
      }
    }

    boolean _a = active && super_acitve && sub_active;
    /*
    String s = "";
    String q = "";

    if (this instanceof InputField) {
      s = "" + ((InputField) this).getValue();
      q = "" + ((InputField) this).getQuestion();
    }

    log("[" + this.getUOID() +"] isActive():"  + _a + "    activeValue:" + active + "\t super:" + super_acitve + "\t sub:" + sub_active + "\t  value:" + s + "\t question: " + q);
         */
    
    return _a;
  }

  /**
   * @param longDescription long description
   */
  public void setLongDescription(String longDescription) {
    this.longDescription = longDescription;
  }

  /**
   * validates this object and return true if valid
   *
   * @return true if the object is valid
   */
  public boolean isValid() {
    return (this.validate() == null);
  }

  /**
   * @param doc TODO
   * @param parent TODO
   *
   * @deprecated
   */
  public void assignToDoc(Document doc, Node parent) {
    // adding myself
    Node node = null;
    node = this.toElement(doc);
    parent.appendChild(node);
  }

  /**
   * TODO: 
   *
   * @param promt TODO
   */
  public void cacheRoot(Promt promt) {
    this.root_cached = promt;
  }

  /**
   * change the status
   *
   * @deprecated
   */
  public void changeActivation() {
    setActive(!isActive());
  }

  /**
   * creates an HTML representation of all childs. this is done by getting the HTML-presentation from each included object. FIXME replace it by jsp
   *
   * @return an HTML representation of all childs.
   *
   * @see FormObject#createHTML()
   */
  public String createHTMLChilds(int numberOfColums) {
    StringBuffer buffer = new StringBuffer();

    for (int i = 0; i < this.getFields().length; i++) {
      FormObject object = this.getFields()[i];

      // only active objects will be shown
      if (object.isActive()) {
        buffer.append("\n<!-- START -- " + object.toString() + " -->\n");

        // if the HR is activated
        if (this.dividerHR) {
          buffer.append("<tr><td colspan=\"" + numberOfColums + "\"><hr></td></tr>");
        }

        if (Config.NEW_DESIGN) {
          // New Design
          buffer.append(object.createHTML(numberOfColums - 1));
        } else {
          // Old Design
          // HOTFIX --->
          if (object instanceof TreatmentInputfield) {
            buffer.append("<tr><td valign=\"top\" align=\"right\">" + (i + 1) + "/" + this.getFields().length + "  " + ((Removable) object).createRemoveButton() + "</td>");
            buffer.append("<td><table>");
            buffer.append(object.createHTML(numberOfColums - 1));
            buffer.append("</table></td>");
          } //<--- HOTFIX 
          else {
            if (object instanceof Removable && (((Removable) object).createRemoveButton().length() > 2)) {
              buffer.append("<th valign=\"top\" align=\"right\">" + (i + 1) + "/" + this.getFields().length + "  " + ((Removable) object).createRemoveButton() + "</th>");
            }

            buffer.append(object.createHTML(numberOfColums - 1));
          }
        }

        // Old design
        buffer.append("\n<!-- END -- " + object.toString() + " -->\n");
      } else {
          
          if (object instanceof InputField){
              buffer.append("\n<!-- START / END - INACTIVE: -- " + ((InputField)object).getQuestion()  + "   " +  object.toString() + " -->\n");
          } else {
              buffer.append("\n<!-- START / END - INACTIVE: -- " + object.toString() + " -->\n");
          }
      }
    }

    return buffer.toString();
  }

  /**
   * convert
   */
  public final static Set map(FormObject[] fields2) {
    resultSetMapping = new HashSet();

    for (int i = 0; i < fields2.length; i++) {
      // setting postion of the field in the array - cause otherwise the pos will be lost 
      fields2[i].setInternalPosition(i);

      resultSetMapping.add(fields2[i]);
    }

    return resultSetMapping;
  }

  /**
   * TODO: 
   *
   * @param set TODO
   *
   * @return TODO
   */
  public final static FormObject[] map(Set set) {
    FormObject[] results = new FormObject[set.size()];

    //debug(FormObject.class, "mapping from " + set.size() + " to " + results.length);
    Iterator iterator = set.iterator();

    FormObject f = null;

    HashSet positions = new HashSet();

    while (iterator.hasNext()) {
      f = (FormObject) iterator.next();

      try {
        if (f == null) {
          throw new RuntimeException("not mapping fields with null value.");
        }

        if (positions.contains(f.getInternalPosition() + "")) {
          warning(null, "dublicate postions in field :" + f);
        }

        results[f.getInternalPosition()] = f;
        positions.add(f.getInternalPosition() + "");

        //debug(FormObject.class, " mapped " + Util.getClassName(f) + " to pos " + f.getInternalPosition());
      } catch (ArrayIndexOutOfBoundsException e) {
        warning(FormObject.class, "------------- SERIOUS MISSMAPPING -----------------");
        warning(FormObject.class, "--- wrong internal position for " + Util.getClassName(f));
        e.printStackTrace();
        warning(null, "--- internal position  " + f.getInternalPosition());
        warning(null, "--- array size " + results.length);
        warning(FormObject.class, "--- fixing internal positions now -----------------");

        // NEW fixing the positions
        Iterator iteratorFixing = set.iterator();
        int i = 0;

        while (iteratorFixing.hasNext()) {
          FormObject element = (FormObject) iteratorFixing.next();
          element.setInternalPosition(i);
        }

        warning(FormObject.class, "--- fixing internal positions done -----------------");
        warning(FormObject.class, "--- restarting mapping -----------------");
      }
    }

    // checking results.

    /*
       for (int i = 0; i < results.length; i++) {
         if (results[i] == null) {
           throw new RuntimeException("illeagal value in resultArray");
         }
         debug(FormObject.class, "mapped " + Util.getClassName(results[i]) + " to pos " + i);
       }
     */
    return results;
  }

  /**
   * TODO: 
   *
   * @param set TODO
   *
   * @return TODO
   */
  public final static FormObject[] mapIgnoreInternalPos(Set set) {
    FormObject[] results = new FormObject[set.size()];

    Iterator iterator = set.iterator();

    FormObject f = null;

    HashSet positions = new HashSet();

    int count = 0;

    while (iterator.hasNext()) {
      f = (FormObject) iterator.next();

      try {
        if (f == null) {
          throw new RuntimeException("not mapping fields with null value.");
        }

        results[count] = f;
        count++;
      } catch (ArrayIndexOutOfBoundsException e) {
        e.printStackTrace();
        throw new RuntimeException(e);
      }
    }

    return results;
  }

  /**
   * set the parent object
   *
   * @param object parent
   */
  public void setParent(FormObject object) {
    this.parent = object;
  }

  /**
   * determine the root element of this promt-tree which is a <code>Promt</code>.
   * 
   * <p>
   * In case that this object is not related to the root object, a null value is retured.
   * </p>
   *
   * @return the root element or null if there is no relation to the root object.
   */
  public final Promt root() throws FormRootNotAvailableException {
    // while top object is not type of a Promt - continue getting the parent one
    if ((this.root_cached == null) || !(root_cached instanceof Promt)) {
      boolean root_found = false;
      this.root_cached = this;

      if (this.root_cached instanceof Promt) {
        //debug("found root " + this.root_cached.getUOID());
        root_found = true;
      }

      while (true && !root_found) {
        try {
          this.root_cached = root_cached.getParent();
        } catch (LazyInitializationException e) {
          root_cached = null;
        }

        if (root_cached instanceof Promt) {
          debug("found root " + root_cached);

          return (Promt) root_cached;
        }

        if (root_cached == null) {
          if (root_cached instanceof Page) {
            // ja this happens in case the page is not assigned to a promt yes.
          } else {
            if (Config.LOGGING_SHOW_MISSING_ROOT) {
              warning(this, "unable to find root - no parent for " + this.getName() + " <" + Util.getClassName(this) + ">");
            }
          }

          throw new FormRootNotAvailableException("unable to find root - no parent for " + this.getName() + " <" + Util.getClassName(this) + ">");
        }
      }
    }

    return (Promt) root_cached;
  }

  /*
   *  (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public final String toString() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("uoid:" + this.uoid + "\t");
    buffer.append(this.getName() + "\t");
    //buffer.append(SEPERATOR+ "\t");
    //buffer.append(getDescription()+ "\t");
    buffer.append(" active:" + this.isActive() + "\t");
    buffer.append(" Type: ");
    buffer.append(Util.getClassName(this) + "\t");
    buffer.append(" hash" + this.hashCode());

    if (this instanceof InputField) {
      buffer.append("\tvalue:" + ((InputField) this).getValue());
    }

    return buffer.toString();
  }

  /**
   * returns an icon - IMG_UNVALID if valid  - else IMG_VALID
   *
   * @return TODO
   *
   * @see FormObject#IMG_VALID
   * @see FormObject#IMG_UNVALID
   */
  public String createValidIcon() {
    if (this.isValid()) {
      return IMG_UNVALID;
    } else {
      return IMG_VALID;
    }
  }

  /**
   * just showit for debuggind
   */
  public final String showIt(int steps) {
    StringBuffer buffer = new StringBuffer(" ");

    for (int i = 0; i < steps; i++) {
      buffer.append("   ");
    }

    buffer.append(" \\-");
    buffer.append(this.getName() + "[" + this.getDescription() + "] [" + Util.getClassName(this) + " ]  active:" + isActive() + "  " + this.getUOID() + " " + this.hashCode());

    if (this instanceof InputField) {
      buffer.append(" value: \"" + ((InputField) this).getValue() + "\" ");
    }

    if (this instanceof MultiField4Clazz) {
      buffer.append("<<MultiField4Clazz>>");
    }

    if (this instanceof InputField) {
      buffer.append(" [" + ((InputField) this).getRestriction() + "]");

      if (((InputField) this).getRelations().length > 0) {
        AbstractRelation[] relations = ((InputField) this).getRelations();

        for (int j = 0; j < relations.length; j++) {
          AbstractRelation relation = relations[j];

          if (relation instanceof Relation) {
            Relation relation2 = (Relation) relation;
            buffer.append(" Relation:" + relation.uoid);
            buffer.append(" Condition:" + j + "  " + relation2.getConditions().length);
            buffer.append(" Targets:" + j + "  " + relation2.getRelationTargets().length);
          } else {
            buffer.append(" Condition:" + j + "  " + relation);
          }
        }
      }
    }

    try {
      buffer.append("parent:" + this.getParent().getName());
    } catch (NullPointerException e) {
      buffer.append("parent:   !!! NONE !!!");
    }

    buffer.append("\n");

    for (int i = 0; this.getFields().length > i; i++) {
      buffer.append(this.getField(i).showIt(steps + 1));
    }

    return buffer.toString();
  }

  /**
   * creates a tree view of the whole tree in which this element is included.
   *
   * @return a tree view.
   */
  public final String toStringTree() {
    try {
      return this.root().showIt(0);
    } catch (FormRootNotAvailableException e) {
      e.printStackTrace();

      return null;
    }
  }

  /**
   * find an instance of MultiFieldClazzes
   *
   * @return an instance of MultiFieldClazzes
   */
  public MultiFieldClazzes findMultifieldClasses() {
    //debug("findMultifieldClasses()");
    if (this instanceof MultiFieldClazzes) {
      return (MultiFieldClazzes) this;
    }

    if (this instanceof StringInputfield) {
      return null;
    }

    // if not instanceof check the childs
    if (this.getFields() == null) {
      throw new NullPointerException("the fields are NULL");
    }

    for (int i = 0; i < this.getFields().length; i++) {
      //log("checking " + this.getField(i));
      MultiFieldClazzes result = null;

      try {
        result = this.getField(i).findMultifieldClasses();
      } catch (NullPointerException e) {
        warning(this, "there is a field in " + Util.getClassName(this) + ": " + this.toString() + " on position " + i + " that is null.");
        result = null;
      }

      if (result != null) {
        return result;
      }
    }

    // nothing found 
    return null;
  }
  
  /**
   * find an instance of NBCISpeciesInputfield
   *
   * @return an instance of NBCISpeciesInputfield
   */
  public NBCISpeciesInputfield findNBCISpeciesInputfield() {
    if (this instanceof NBCISpeciesInputfield) {
      return (NBCISpeciesInputfield) this;
    }

    // if not instanceof check the childs
    for (int i = 0; i < this.fields.length; i++) {
      NBCISpeciesInputfield result = this.getField(i).findNBCISpeciesInputfield();

      if (result != null) {
        return result;
      }
    }

    // nothing found 
    return null;
  }

  /**
   * find a sample within this object
   *
   * @param sampleID the id
   *
   * @return
   */
  public Sample findSample(long sampleID) {
    //debug("looking for sample " + sampleID);

    if (this instanceof Sample) {
      Sample sample = (Sample) this;

      if (sample.getUOID() == sampleID) {
        //debug("found sample " + sampleID);

        return sample;  
      }
    }

    for (int i = 0; i < this.getFields().length; i++) {
      Sample sample = this.getFields()[i].findSample(sampleID);

      if (sample != null) {
        return sample;
      }
    }

    return null;
  }

  /**
   * find an instance of SpeciesInputfield2
   *
   * @return an instance of SpeciesInputfield2
   */
  public SpeciesInputfield2 findSpeciesInputfield2() {
    if (this instanceof SpeciesInputfield2) {
      return (SpeciesInputfield2) this;
    }

    // if not instanceof check the childs
    for (int i = 0; i < this.fields.length; i++) {
      SpeciesInputfield2 result = this.getField(i).findSpeciesInputfield2();

      if (result != null) {
        return result;
      }
    }

    // nothing found 
    return null;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.CoreObject#persistenceChilds(org.hibernate.Session, boolean)
   */
  public void persistenceChilds(Session session, boolean createIt)
    throws PersistenceActionException {
    for (int i = 0; i < this.getFields().length; i++) {
      try {
        this.getField(i).update(session, createIt);
      } catch (NullPointerException e) {
        warning(this, "Nullobject inside " + this.getUOID() + ": " + e);
      }
    }

    if (this.parent != null) {
        parent.update(session, true);
      }
    
    try {
        // -- new
        if (this.getMSIAttribute() != null) {
            this.getMSIAttribute().update(session, true);
        }
    } catch (Exception e) {
        warning(this, "Unable to store MSI Attribute.");
        warning(e);
    }


    super.persistenceChilds(session, createIt);
  }

  /**
   * TODO: 
   *
   * @param class1 TODO
   * @param id TODO
   *
   * @return TODO
   *
   * @throws PersistenceActionFindException TODO
   */
  public static final FormObject persistence_loadByID(Class class1, long id)
    throws PersistenceActionFindException {
    FormObject result = null;
    Session session = PersistenceConfiguration.createSessionFactory().openSession();
    session.beginTransaction();

    try {
      result = (FormObject) FormObject.persistence_loadByID(class1, session, id);
    } catch (PersistenceActionFindException e) {
      throw e;
    } finally {
      session.close();
    }

    return result;
  }

  /**
   * DEBUG: store the HTML representaiton of this in a file
   *
   * @param file
   *
   * @throws IOException
   * @throws FileNotFoundException
   */
  public void storeToFile(File file) throws FileNotFoundException, IOException {
    String content = this.createHTML(1);
    org.setupx.repository.core.util.File.storeData2File(file, content);
  }

  /**
   *
   */
  public void testParentField() {
    if (this.parent == null) {
      if (Config.LOGGING_SHOW_MISSING_ROOT) {
        warning(this, "parent not available.");
      }
    }

    FormObject[] formObjects = this.getFields();

    for (int i = 0; i < formObjects.length; i++) {
      FormObject object = formObjects[i];
      object.testParentField();
    }
  }

  /**
   * creating a node outof this object.
   *
   * @param doc needed for the creating a new node
   *
   * @return the new node of this object
   *
   * @deprecated
   */
  public Node toElement(Document doc) {
    if (!this.isActive()) {
      return null;
    }

    if (this instanceof InputField) {
      String name = ((InputField) this).getQuestion();
    }

    //log("converting to XML-Node");
    Element thisAsElement = doc.createElement(Util.getClassName(this));

    // now adding my attributes
    Hashtable ht = this.getAttributes();

    if (ht != null) {
      Enumeration enumeration = ht.keys();

      while (enumeration.hasMoreElements()) {
        String label = (String) enumeration.nextElement();
        String value = (String) ht.get(label);

        // creating an attrribute for label & value
        if ((label.length() != 0) && (value.length() != 0)) {
          // debug("creating attribute  \"" + label + ": " + value + "\"");

          thisAsElement.setAttribute(label, value);
        } else {
          // debug(this, "skipping " + label + " " + value);
        }
      }
    }

    // now adding my child elements
    for (int i = 0; i < this.getFields().length; i++) {
      Node child = this.getField(i).toElement(doc);
      // debug("assigning child node");

      if (child != null) {
        thisAsElement.appendChild(child);
      }
    }

    return thisAsElement;
  }

  /**
   * validates this object - if everything is ok, null is returned - otherwise a message what is wrong
   *
   * @return if everything is ok, null is returned - otherwise a message what is wrong
   */
  public String validate() {
    if (!this.isActive()) {
      return null;
    }

    //debug(this, "validaton start ");
    FormObject[] objects = this.getFields();
    StringBuffer stringBuffer = new StringBuffer();

    for (int i = 0; i < objects.length; i++) {
      // only active object are checked
      if ((objects[i] != null) && objects[i].active) {
        String validationResult = objects[i].validate();

        if (validationResult != null) {
          //debug("validationresult from " + Util.getClassName(objects[i]) + ": " + validationResult);
          stringBuffer.append(validationResult);
        }
      }
    }

    //debug(this, "validatonanswer:  #" + stringBuffer.toString() + "#");
    if ((stringBuffer.toString().compareTo("") == 0) || (stringBuffer.toString().length() < 1)) {
      return null;
    } else {
      return stringBuffer.toString();
    }
  }

  /**
   * Add the object to the <b>internal</b> field array.
   * 
   * <p>
   * The method used the root() method to determine the root element of the object tree to perform an update on the ClazzDimensionDetector.
   * </p>
   *
   * @param formObject the object that will be added
   *
   * @throws ArrayExpandException unable to update ClazzDimensionDetector cause the root element is not found
   *
   * @see ClazzDimensionDetector#update()
   * @see FormObject#root()
   */
  protected final void addInternalField(FormObject formObject) {
    // caching the root
    try {
      formObject.cacheRoot(this.root());
    } catch (FormRootNotAvailableException e) {
    } catch (Exception e) {
      //debug("unable to precache the root for " + Util.getClassName(formObject) + " cause " + e);
    }

    FormObject[] oldFields = this.getFields();
    FormObject[] newFields = new FormObject[oldFields.length + 1];

    int i;

    for (i = 0; i < oldFields.length; i++) {
      newFields[i] = oldFields[i];
    }

    // attatch the new field at the end
    formObject.setParent(this);
    newFields[i] = formObject;
    this.setFields(newFields);

    try {
      // updating the Clazzdiemnsions.
      this.root().getClazzDimensionDetector().update();
    } catch (NullPointerException e) {
      // in case the form object is Multifiled4class - you can ignore ?? WHY ?
      if (Config.LOGGING_SHOW_MISSING_ROOT) {
        this.warning(new ArrayExpandException("unable to update ClazzDimensionDetector cause the root element is not found - might be that the object is not connected to a root yet"));
      }
    } catch (FormRootNotAvailableException e) {
      if (Config.LOGGING_SHOW_MISSING_ROOT) {
        this.warning(this, "unable to update ClazzDimensionDetector cause the root element is not found - might be that the object is not connected to a root yet");
      }

      if (Config.LOGGING_WARNING_MISSING_ROOT) {
        this.warning(new ArrayExpandException("unable to update ClazzDimensionDetector cause the root element is not found - might be that the object is not connected to a root yet"));
      }
    }
  }

  /**
   * casts a an array of formfields into an Array of Inputfields
   *
   * @param formfields fields that will be converted
   *
   * @return the elements given -but casted
   */
  protected final static InputField[] convert(FormObject[] formfields) {
    InputField[] inputFields = new InputField[formfields.length];

    for (int i = 0; i < formfields.length; i++) {
      inputFields[i] = (InputField) formfields[i];
    }

    return inputFields;
  }

  /**
   * creates an HTML-Button which links to a help page
   *
   * @return HTML-code for the button
   */
  protected String createHelpButton() {
    if (this.helptext.compareTo("") != 0) {
      StringBuffer buffer = new StringBuffer();
      buffer.append("form?");
      buffer.append(this.getName());
      buffer.append("=");
      buffer.append("");
      buffer.append("&");
      buffer.append(WebConstants.PARAM_HELP);
      buffer.append("=");
      buffer.append(this.getName());

      /*<a href="foo.html" target="windowName"  onclick="window.open(this.href,this.target,'width=300,height=300'); return false;">*/
      return "<a href=\"" + buffer.toString() + "\"	target=\"second\" onclick=\"window.open(this.href,this.target,'width=400,height=500,menubar=no,status=no'); return false;\" >" + helpIMG + "</a>  ";
    } else {
      return "";
    }
  }
  
  
  /**
   * creates an HTML-Button which links to a help page
   *
   * @return HTML-code for the button
   */
  protected String createHistoryButton() {

      
      
      // MODIFICATION: former version did not submit the form before opening the new window

      // needs a check, which field will be autocompleted and which not
      if (true){
          StringBuffer buffer = new StringBuffer();
          buffer.append("history_complete.jsp?id=");
          buffer.append(this.getUOID());
          return "<a href=\"" + buffer.toString() + "\" target=\"second\" onclick=\"window.open(this.href,this.target,'width=400,height=500,resizable=yes,scrollbars=yes,menubar=no,status=no'); document.default.submit(); return false;\" ><img  border='0' src=\"pics/form_history.gif\"/></a>";
      } else {
          return "";
      }
      
      
      
  }

  /**
   * removes a fieldobject from this array and its childs.
   * 
   * <p>
   * The object is removed from this array and if it not found inside <code>this</code>  the object will be looked for in all the objects that are included inside <code>this</code>
   * </p>
   * 
   * <p></p>
   *
   * @param formObject the name of the object that will be removed
   *
   * @return
   *
   * @throws ArrayShrinkException unable to remove it, cause the key is <code>null</code> or cause it is the last remaining object in the array
   */
  protected final boolean removeInternalField(String key)
    throws ArrayShrinkException {
    debug(this.getDescription() + ": trying to remove " + key);

    if (key == null) {
      throw new ArrayShrinkException("unable to shrink array, cause the formobject is null");
    }

    debug(this, "removing key: " + key);

    for (int i = 0; i < this.getFields().length; i++) {
      FormObject object = this.getFields()[i];

      if (this.getFieldPos(key) != 9999) {
        // the object is inside this
        debug("number of objects in .this :" + this.getFields().length);

        if (this.getFields().length == 1) {
          throw new ArrayShrinkException("unable to shrink it cause the remaining object is the last one");
        }

        this.shrinkArray(this.getField(key));

        return true;
      } else {
        debug(" the object is not inside this - delegating to included component");

        // check if it is inside the object itself ...
        if (object instanceof FormContainer) {
          return object.removeInternalField(key);
        }
      }
    }

    return false;
  }

  /**
   * removes an formobject from the array of objects
   * <pre>the object must be inside the array of objects !!</pre>
   *
   * @param object the object to remove
   *
   * @throws ArrayShrinkException unable to find the object
   */
  protected void shrinkArray(FormObject object) throws ArrayShrinkException {
    FormObject[] oldFields = this.getFields();
    FormObject[] newFields = new FormObject[oldFields.length - 1];
    debug(this, "shrinking array from size: " + oldFields.length + "  to new size: " + newFields.length);

    int posOfRemovObj = this.getFieldPos(object.getName());

    if (posOfRemovObj == 9999) {
      throw new ArrayShrinkException("unable to shrink array, cause the formobject was not found");
    }

    // shrink the array 
    int newPos = 0;

    for (int oldPos = 0; oldPos < oldFields.length; oldPos++) {
      if (oldPos != posOfRemovObj) {
        newFields[newPos] = oldFields[oldPos];
        newPos++;
      } else {
        // the deleted object was found so leave this uncopied and remove it from db
        oldFields[oldPos].remove();
      }
    }

    this.setFields(newFields);
  }

  /**
   * setting the reverse relations in the formobjects to this
   */
  private void updateParents() {
    for (int i = 0; i < this.fields.length; i++) {
      try {
        this.fields[i].setParent(this);
      } catch (Exception e) {
        warning(this, "unable to update the parent : cause " + e);
        //e.printStackTrace();
      }
    }
  }
  
  
  protected void compare(FormObject formObject) throws ComparisonsException {
      //debug(" vs. ["  + formObject+ "]"); 
      if (this.getUOID() != formObject.getUOID()) throw new ComparisonsException(this, formObject, "uoid");
      if (this.isActive() != formObject.isActive()) throw new ComparisonsException(this, formObject, "active");
      //if (this.isValid() != formObject.isValid()) throw new ComparisonsException(this, formObject, "valid");
      if (this.getFields().length != formObject.getFields().length) throw new ComparisonsException(this, formObject, "number of fields");
      if (this.getName().compareTo(formObject.getName() ) != 0) throw new ComparisonsException(this, formObject, "name");

      // incl objects
      for (int i = 0; i < this.getFields().length; i++) {
          formObject.getField(i).compare(this.getField(i));
      }
      
      if (this instanceof InputField) {
          InputField _in1 = (InputField) this;
          InputField _in2 = (InputField) formObject;
        
          
        Vector vector = new Vector();
        for (int i = 0; i < _in1.getRelations().length; i++) {
            
            // problem: relations are not sortet - they show up in a random order.
            AbstractRelation relation1 = _in1.getRelations()[i];
            AbstractRelation relation2 = _in2.getRelations()[i];
            
            try {
                relation1.compare(relation2);
            } catch (ComparisonsException e) {
                vector.add(e);
            }
        }
        if (vector.size() != 0){
            Iterator iterator = vector.iterator();
            while (iterator.hasNext()) {
                Exception element = (Exception) iterator.next();
                warning(this, element.getMessage());
            }
            // exceptions are beeing ignored - todo - sort the relation and then the compare will work too.
            warning(this,"---->      Problems with " + vector.size() + " relations.");
        }
    }
      
  }
  
  
  public GenotypeInputfield findGenotypeInputfield() {
      if (this instanceof GenotypeInputfield) {
          return (GenotypeInputfield) this;
        }

        // if not instanceof check the childs
        for (int i = 0; i < this.fields.length; i++) {
            GenotypeInputfield result = this.getField(i).findGenotypeInputfield();

          if (result != null) {
            return result;
          }
        }

        // nothing found 
        return null;
  }

  
  /**
   * reset all relations. It is like a set with the same value meaning that all relations will be updated.
   */
  public void updateRelations() {
      // updating all my relations.
      if (this instanceof InputField){
          InputField inputField = ((InputField)this);
          String value = inputField.getValue() + "";
          AbstractRelation[] relations = inputField.getRelations();

          for (int j = 0; j < relations.length; j++) {
              AbstractRelation relation = relations[j];
              relation.update(value);
          }
      }   
      
      // updating each child
      for (int i = 0; i < this.getFields().length; i++) {
        FormObject formObject = this.getFields()[i];
        formObject.updateRelations();
      } 
  }

  /**
   * converts this to an XML Node. 
   * if there are derivates of this class - the objects have to be attached too.
   * 
   * @param doc the document the is being used to create the elements
   * @return the new created Node
   * @throws InActiveException the element is not even active so dont even ask for an xml representation ok?! 
   */
  public Node createXMLNode(Document doc) throws InActiveException{
      if (!this.isActive()) throw new InActiveException("element not active.");
      
      // create the element
      Element element = doc.createElement(this.getXMLChildname());

      
      // attributes
      Hashtable exportAttributes = this.getExportAttributes();
      Enumeration exportAttributesEnumeration = exportAttributes.keys();
      while (exportAttributesEnumeration.hasMoreElements()) {
        String key = "" + exportAttributesEnumeration.nextElement();
        String value = "" + exportAttributes.get(key);
        
        if (value.length() != 0 || key.compareTo("value") == 0){
            try {
                element.setAttribute(key, value);
            } catch (Exception e) {
                warning("key:" + key, "value: " + value);
                e.printStackTrace();
            }
        }
      }
            
      
      // elements
      Hashtable exportElements = this.getExportElements();
      Enumeration exportElementsEnumeration = exportElements.keys();
      while (exportElementsEnumeration.hasMoreElements()) {
        String key = "" + exportElementsEnumeration.nextElement();
        String value = "" + exportElements.get(key);
        
        if (value.length() != 0){
            try {
                element.appendChild(createXMLAttribute(doc, key, value));
            } catch (Exception e) {
                warning("key:" + key, "value: " + value);
                e.printStackTrace();
            }
        }
      }
      

      // add each of childs
      for (int i = 0; i < this.getFields().length; i++){
          try {
              element.appendChild(this.getField(i).createXMLNode(doc));
          } catch (InActiveException e) {
              // ok - ignore it.
          } catch (DOMException e) {
              warning(this,"can not attatch: " + this.getField(i).toString() + "");
          }
      }
      
      return element;
  }

  public Hashtable getExportAttributes() {
      Hashtable hashtable = new Hashtable();
      hashtable.put("sxuoid", ""+ this.getUOID());
      hashtable.put("array_pos", "" + this.getInternalPosition());
      return hashtable;
  }

  public Hashtable getExportElements() {
          Hashtable hashtable = new Hashtable();
          hashtable.put("desc", this.getDescription());
          hashtable.put("desc_long", this.getLongDescription());
          hashtable.put("help", this.getHelptext());
          return hashtable;
  }

  public String getXMLChildname() {
      debug("replace by real value");
      return Util.getClassName(this);
  }

  /**
   * Mapping into XML - creating a Node out of this entry.
   * @param doc
   * @param key
   * @param value
   * @return
   */
  protected final static Node createXMLAttribute(Document doc, String key, String value) {
      if (value.length()== 0) return null;
      Element element = doc.createElement(key);
      Text attribute = doc.createTextNode(key);
      attribute.setData(value);
      element.appendChild(attribute);
      return element;
  }

  protected final static Node createXMLAttribute(Document doc, String key, boolean value) {
      return createXMLAttribute(doc, key, "" + value);
  }

  protected final static Node createXMLAttribute(Document doc, String key, int value) {
      return createXMLAttribute(doc, key, "" + value);
  }
  
  protected final static Node createXMLAttribute(Document doc, String key, long value) {
      return createXMLAttribute(doc, key, "" + value);
  }
  
  
  
  /**
   * @hibernate.many-to-one column="msiattribute" class = "org.setupx.repository.core.communication.msi.MSIAttribute"
   */
  public MSIAttribute getMSIAttribute() {
      return this.msiAttribute;
  }

  public void setMSIAttribute(MSIAttribute attribute) {
      this.msiAttribute = attribute;
  }
}
