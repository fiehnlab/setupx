/**
 * ============================================================================ File:    PotatoImporter.java Package: org.setupx.repository.core.util.hotfix.fsa cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.hotfix.fsa;

import org.setupx.repository.core.util.xml.XMLFile;
import org.setupx.repository.core.util.xml.XPath;
import org.setupx.repository.core.util.xml.XPathException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.webservice.BinBaseServicePortSoapBindingStub;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.webservice.BinBaseServiceServiceLocator;
import edu.ucdavis.genomics.metabolomics.exception.CommunicationExcpetion;

import org.w3c.dom.DOMException;

import org.xml.sax.SAXException;

import java.io.UTFDataFormatException;

import java.net.MalformedURLException;
import java.net.URL;

import java.rmi.RemoteException;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.xml.rpc.ServiceException;


/**
 * Tool for Reading all XML-Files from one directory and trigger BinBase for import. Tools is only used as a HOTFIX, to start import on BinBase for OLD existing samples.
 */
public class PotatoImporter {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String xpath_expression_classID = "/sample/trayname_generic/class";
  public static final String xpath_expression_sampleID = "/sample/name";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new PotatoImporter object.
   */
  public PotatoImporter() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param classID
   *
   * @return
   */
  public static String cutSpace(String id) {
    try {
      return id.substring(0, id.indexOf(' '));
    } catch (StringIndexOutOfBoundsException e) {
      return id;
    }
  }

  /**
   */
  public static void main(String[] args) throws MalformedURLException, ServiceException {
    PotatoImporter.start();
  }

  /**
   * TODO: 
   *
   * @throws MalformedURLException TODO
   * @throws ServiceException TODO
   */
  public static void start() throws MalformedURLException, ServiceException {
    URL[] urls = new URL[] { 
        //new URL("http://128.120.136.203:8080/BCI/BinBaseService"), 
        //new URL("http://128.120.136.216:8080/BCI/BinBaseService"), 
        new URL("http://128.120.136.154:8080/BCI/BinBaseService")};

    BinBaseServiceServiceLocator baseServiceServiceLocator = new BinBaseServiceServiceLocator();

    Experiment experiment = createExperiment("FSA");

    ExperimentClass[] experimentClasses = experiment.getClasses();

    for (int k = 0; k < urls.length; k++) {
      BinBaseServicePortSoapBindingStub binding = (BinBaseServicePortSoapBindingStub) baseServiceServiceLocator.getBinBaseServicePort(urls[k]);

      // trigger import
      for (int i = 0; i < experimentClasses.length; i++) {
        ExperimentClass class1 = experimentClasses[i];
        debug("triggerImportClass(" + class1.getId() + ") on " + urls[k]);

        try {
          binding.triggerImportClass(class1);
        } catch (RemoteException e) {
          e.printStackTrace();
        }
      }

      try {
        // trigger experiment
        binding.triggerExport(experiment);
        //byte[] bArray = binding.triggerExperimentDownload(experiment.getId());
      } catch (CommunicationExcpetion e) {
        e.printStackTrace();
      } catch (RemoteException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * @param string
   */
  protected static void debug(String string) {
    System.out.println(string);
  }

  /**
   * TODO: 
   *
   * @param i TODO
   * @param samples TODO
   *
   * @return TODO
   */
  private static ExperimentClass createClass(int i, Hashtable samples) {
    HashSet classSamples = new HashSet();

    Enumeration sampleIDs = samples.keys();

    while (sampleIDs.hasMoreElements()) {
      String sampleID = (String) sampleIDs.nextElement();
      int classID = 0;

      if (((String) samples.get(sampleID)).compareTo("NA") == 0) {
        debug(sampleID + " without class information.");
      } else {
        try {
          classID = Integer.parseInt("" + samples.get(sampleID));
        } catch (java.lang.NumberFormatException e) {
          e.printStackTrace();
        }

        if (classID == i) {
          classSamples.add(sampleID);
        }
      }
    }

    if (classSamples.size() == 0) {
      return null;
    }

    debug("creating experimentclass classID " + i + "  numbOfSamples " + classSamples.size());

    ExperimentClass experimentClass = new ExperimentClass(null, "" + i, 0, 0, map(classSamples));

    return experimentClass;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  private static ExperimentClass[] createClasses() {
    // find all samples - hashset:  sampleID - classID 
    Hashtable samples = findSamples();

    Vector classes = new Vector();

    for (int i = 170; i < 230; i++) {
      ExperimentClass experimentClass = createClass(i, samples);

      if (experimentClass != null) {
        classes.add(experimentClass);
      }
    }

    ExperimentClass[] experimentClasses = map(classes);

    return experimentClasses;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  private static Experiment createExperiment(String experimentName) {
    ExperimentClass[] experimentClasses = createClasses();
    debug("creating experiment");

    Experiment experiment = new Experiment(experimentClasses, "", null, true, experimentName, 0, 0, "");

    return experiment;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  private static Hashtable findSamples() {
    Hashtable result = new Hashtable();
    java.io.File[] files = (new java.io.File("/mnt/metabolomics/Scholz/sampleinformation/fsa/fieldtrails2003")).listFiles();
    String classID = "";
    String sampleID = "";

    for (int i = 0; i < files.length; i++) {
      // open file
      try {
        XMLFile xmlFile = new XMLFile(files[i]);

        // take class
        classID = XPath.pathValue(xmlFile.getDocument(), xpath_expression_classID);
        sampleID = XPath.pathValue(xmlFile.getDocument(), xpath_expression_sampleID);

        classID = cutSpace(classID);
        sampleID = cutSpace(sampleID);

        debug(" \"" + files[i].getName() + "\" \t \"" + sampleID + "\" \t \"" + classID + "\"");
        // return (SAmpleName - Class)
        result.put(sampleID, classID);
      } catch (DOMException e) {
        e.printStackTrace();
      } catch (XPathException e) {
        debug("didnt find values " + e.getMessage());
      } catch (UTFDataFormatException e) {
        e.printStackTrace();
      } catch (SAXException e) {
        e.printStackTrace();
      }
    }

    return result;
  }

  /**
   * TODO: 
   *
   * @param classes TODO
   *
   * @return TODO
   */
  private static ExperimentClass[] map(Vector classes) {
    ExperimentClass[] experimentClasses = new ExperimentClass[classes.size()];
    int i = 0;

    for (Iterator iter = classes.iterator(); iter.hasNext();) {
      ExperimentClass experimentClass = (ExperimentClass) iter.next();
      experimentClasses[i] = experimentClass;
      i++;
    }

    return experimentClasses;
  }

  /**
   * TODO: 
   *
   * @param classSamples TODO
   *
   * @return TODO
   */
  private static ExperimentSample[] map(HashSet classSamples) {
    ExperimentSample[] experimentSamples = new ExperimentSample[classSamples.size()];
    int i = 0;

    for (Iterator iter = classSamples.iterator(); iter.hasNext();) {
      String name = (String) iter.next();
      debug("creating experimentsample " + name);
      experimentSamples[i] = new ExperimentSample(name, name);
      i++;
    }

    return experimentSamples;
  }
}
