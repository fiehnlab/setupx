/**
 * ============================================================================ File:    EuclidDistance.java Package: org.setupx.repository.core.util cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util;

import java.util.Enumeration;
import java.util.Hashtable;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class EuclidDistance {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static String gr;
  private static Gram gram;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   *
   */
  private EuclidDistance() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param str1 TODO
   * @param str2 TODO
   * @param ng TODO
   *
   * @return TODO
   */
  public static double compare(String str1, String str2, int ng) {
    Hashtable gramList1 = new Hashtable();
    int len = str1.length();

    for (int i = 0; i <= (len - ng); i++) {
      gr = str1.substring(i, i + ng);
      gram = new Gram(gr, str1);

      if (gram.occurences > 0) {
        gramList1.put(gram.gram, gram);
      }
    }

    Hashtable gramList2 = new Hashtable();
    len = str2.length();

    for (int i = 0; i <= (len - ng); i++) {
      String gr = str2.substring(i, i + ng);
      Gram gram = new Gram(gr, str2);

      if (gram.occurences > 0) {
        gramList2.put(gram.gram, gram);
      }
    }

    double difference = 0;
    double sum = 0;
    Hashtable diffGrams = new Hashtable();

    Enumeration keys1 = gramList1.keys();

    while (keys1.hasMoreElements()) {
      String key = (String) keys1.nextElement();
      Gram g = (Gram) gramList1.get(key);
      double plus = (g.occurences);
      double squarePlus = Math.pow(plus, 2);
      sum = sum + squarePlus;

      if (gramList2.containsKey(key)) {
        Gram gr2 = (Gram) gramList2.get(key);
        double minus = (g.occurences - gr2.occurences);
        double square = Math.pow(minus, 2);
        difference = difference + square;
      } else {
        double square = Math.pow(g.occurences, 2);
        difference = difference + square;
      }
    }

    Enumeration keys2 = gramList2.keys();

    while (keys2.hasMoreElements()) {
      String key = (String) keys2.nextElement();
      Gram gr2 = (Gram) gramList2.get(key);

      if (gramList1.containsKey(key)) {
      } else {
        diffGrams.put(key, gr2);
      }
    }

    Enumeration diffKeys = diffGrams.keys();

    while (diffKeys.hasMoreElements()) {
      String key = (String) diffKeys.nextElement();
      Gram diffGram = (Gram) diffGrams.get(key);
      double square = Math.pow(diffGram.occurences, 2);
      difference = difference + square;

      double squarePlus = Math.pow(diffGram.occurences, 2);
      sum = sum + squarePlus;
    }

    int totalLength = str1.length() - ng + 1 + diffGrams.size();
    double threshold = 2.486 + (0.025 * totalLength);
    difference = Math.sqrt(difference);
    sum = Math.sqrt(sum);

    double similarity = 0;

    if (difference < threshold) {
      similarity = 0.8 + ((threshold - difference) / (5 * threshold));
    }

    ;

    if (difference == threshold) {
      similarity = 0.8;
    }

    ;

    if (difference > threshold) {
      similarity = 0.8 - (((difference - threshold) * 4) / (((1 + difference) - threshold) * 5));
    }

    ;

    return similarity;
  }
}
