package org.setupx.repository.core.util.hotfix;

import junit.framework.TestCase;

public class HotFixTest extends TestCase {

    public void testHotFix31() throws HotFixException {
        new HotFix(31);
    }

    public void testHotFix32() throws HotFixException {
        new HotFix(32);
    }

}
