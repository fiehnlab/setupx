/**
 * ============================================================================ File:    Persistence.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.ServiceMediator;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.comparator.FileDateComparator;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.workflow.elements.ProcessingException;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.Sample;

import org.exolab.castor.mapping.MappingException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import java.util.Arrays;
import java.util.Date;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 */
public class Persistence extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private final static Persistence thiZ = new Persistence();
  private static FileInputStream fis = null;
  private static ObjectInputStream in = null;
  private static Promt promt;

  static {
    log(thiZ, "Logging: " + ServiceMediator.isLog());

    /*
       log(thiZ, "init persistence.");
       File directory = new File(Config.DIRECTORY_PERSISTENCE);
       File[] files = directory.listFiles();
    
       // instanciate each file/class
       for (int i = 0; i < files.length; i++) {
         File file = files[i];
         // extract classname
         String classname = file.getName();
         try {
             classname = "org.setupx.repository.web.forms." + file.getName().substring(0, file.getName().lastIndexOf('.'));
             unmarshall(Class.forName(classname));
             log (thiZ, ".");
         } catch (Exception e) {
             log (thiZ, "unable instance of: " + classname + " : " + e.getMessage());
             //e.printStackTrace();
         }
       }
     */
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Persistence object.
   */
  private Persistence() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * creates a list of available backup files stored in the local backup filesystem.
   * 
   * <p>
   * each of the available promts is beeing loaded.  <b>is much slower than the file representaion</b>
   * </p>
   *
   * @return HTML representation of the list of backuped promts
   */
  public static String createBackupList() {
    StringBuffer s = new StringBuffer();
    s.append("<h1>" + thiZ + "</h1>");
    s.append("<table>");
    s.append("<form action=\"load\" >");

    PromtBackup[] promtBackups = Persistence.loadBackups();

    for (int i = 0; i < promtBackups.length; i++) {
      PromtBackup promtBackup = promtBackups[i];

      s.append("<tr>");
      s.append("<th>load: <input type=\"radio\" name=\"id\" value=" + promtBackup.file.getName() + "></th>");
      s.append("<th>Promt: samples:" + promtBackup.promt.numberSamples() + "  classes:" + promtBackup.promt.numberClasses() + "</th>");
      s.append("<th>  last time edit: " + new Date(promtBackup.file.lastModified()).toLocaleString() + "</th>");
      s.append("<th>  file: " + promtBackup.file.toString() + "</th>");
      s.append("<th>owner: " + promtBackup.query.owner() + "</th>");

      Clazz[] clazzs = promt.getClazzDimensionDetector().getClazzes();

      for (int j = 0; j < clazzs.length; j++) {
        // each clazz
        Clazz clazz = clazzs[j];
        FormObject[] formObjects = clazz.getFields();

        for (int k = 0; k < formObjects.length; k++) {
          // each sample
          Sample sample = (Sample) formObjects[k];
          FormObject[] formObjects2 = sample.getFields();

          for (int index = 0; index < formObjects2.length; index++) {
            // each inputfield of a sample
            InputField object = (InputField) formObjects2[index];
            log(thiZ, object.getName());
            log(thiZ, "" + object.getValue());
            log(thiZ, object.getDescription());
            s.append("<tr><td>" + object.getDescription() + "</td><td>" + object.getValue() + "</td></tr>");
          }
        }

        s.append("</tr>\n");
      }
    }

    // submit button
    s.append("<input type=\"submit\" value=\"load\">");
    s.append("</form>");
    s.append("</table>");

    try {
      org.setupx.repository.core.util.File.storeData2File(new File("/mnt/storage/test.html"), s.toString());
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return s.toString();
  }

  /**
   * creates an HTML list similar to the createBackuplist -
   *
   * @return HTML list similar to the createBackuplist
   */
  public static String createFileList() {
    StringBuffer s = new StringBuffer();
    s.append("<h1>" + thiZ + "</h1>");
    s.append("<table>");
    s.append("<form action=\"load\" >");

    java.io.File directory = new java.io.File(Config.DIRECTORY_PERSISTENCE_SX);
    java.io.File[] files = directory.listFiles();

    Arrays.sort(files, new FileDateComparator());

    //Arrays.sort(files, new FileSizeComparator());
    for (int i = 0; i < files.length; i++) {
      File file = files[i];

      s.append("<tr>");
      s.append("<th>load: <input type=\"radio\" name=\"id\" value=" + file.getName() + "></th>");
      s.append("<th>  last time edit: " + new Date(file.lastModified()).toLocaleString() + "</th>");
      s.append("<th>  file: " + file.toString() + "</th>");
      s.append("</tr>\n");
    }

    // submit button
    s.append("<input type=\"submit\" value=\"load\">");
    s.append("</form>");
    s.append("</table>");

    try {
      org.setupx.repository.core.util.File.storeData2File(new File("/mnt/storage/test.html"), s.toString());
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return s.toString();
  }

  /**
   * load local backups from the backup folder.
   *
   * @return
   *
   * @throws ClassNotFoundException
   */
  public static PromtBackup[] loadBackups() {
    // directory containing all backups
    java.io.File directory = new java.io.File(Config.DIRECTORY_PERSISTENCE_SX);

    java.io.File[] files = directory.listFiles();

    Promt promt = null;
    java.util.Vector promtsVec = new java.util.Vector();

    for (int i = 0; i < files.length; i++) {
      java.io.File file = files[i];

      Arrays.sort(files, new FileDateComparator());

      try {
        promt = loadPromt(file);

        promtsVec.add(new PromtBackup(promt, file));
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }

    debug(thiZ, "number of promts found in backupfolder: " + promtsVec.size());

    // convert to array
    return (PromtBackup[]) promtsVec.toArray(new PromtBackup[promtsVec.size()]);
  }

  /**
   * load a Promt form a File.
   *
   * @param file the promt made persistentent in the file
   *
   * @return the reloaded Promt
   *
   * @throws IOException unable to load the file
   * @throws ClassNotFoundException TODO
   */
  public synchronized static Promt loadPromt(File file)
    throws IOException, ClassNotFoundException {
    in = new ObjectInputStream(new FileInputStream(file));
    promt = (Promt) in.readObject();
    in.close();

    return promt;
  }

  /**
   * TODO: 
   *
   * @param args TODO
   *
   * @throws ArrayExpandException TODO
   * @throws PersistenceException TODO
   * @throws MappingException TODO
   * @throws ProcessingException TODO
   * @throws ParameterException TODO
   * @throws TransformerConfigurationException TODO
   * @throws FileNotFoundException TODO
   * @throws TransformerFactoryConfigurationError TODO
   * @throws TransformerException TODO
   * @throws IOException TODO
   */
  public static void main(String[] args) throws ArrayExpandException, PersistenceException, MappingException, ProcessingException, ParameterException, TransformerConfigurationException, FileNotFoundException, TransformerFactoryConfigurationError, TransformerException, IOException {
    /*Promt promt = PromtCreator.create(null); //new Promt("a","b","c");
       DocumentExperiment documentExperiment = promt.toDocExperiment();
       // System.out.println(" ----> " + new XMLFile(documentExperiment.getDocument()).content());
       ServiceMediator.processDocument(documentExperiment, ServiceMediator.ACTION_STORE);
     */
  }

  /**
   * makes an object persistend and stores it in an XML-File.
   *
   * @param object TODO
   *
   * @throws PersistenceException TODO
   */
  public static void marshall(Object object) throws PersistenceException {
    marshall(object, "");
  }

  /**
   * TODO: 
   *
   * @param object TODO
   *
   * @throws PersistenceException TODO
   */
  public static void marshallBackup(Serializable object)
    throws PersistenceException {
    Persistence.marshall(object, "" + new Date().getTime());
  }

  /**
   * TODO: 
   *
   * @param clazz TODO
   *
   * @return TODO
   *
   * @throws PersistenceException TODO
   */
  public static Object unmarshall(Class clazz) throws PersistenceException {
    debug(thiZ, "-->READ: starting XML unmarshalling for " + Util.classString(clazz.getName()));

    FileWriter fileWriter;
    String filename = Config.DIRECTORY_PERSISTENCE + Util.classString(clazz.getName()) + ".xml";
    log(thiZ, "loading file: " + filename);

    FileReader reader;

    try {
      reader = new FileReader(filename);

      Unmarshaller unmarshaller = new Unmarshaller(clazz);
      unmarshaller.setValidation(false);

      Object object = unmarshaller.unmarshal(reader);
      debug(thiZ, "<--READ: finished XML unmarshalling for " + Util.classString(clazz.getName()));

      return object;
    } catch (Exception e) {
      //Logger.log(thiZ, e);
      throw new PersistenceException(e);
    }
  }

  /*
     public static void createMappingFile() throws MappingException, IOException{
         org.exolab.castor.tools.MappingTool mappingTool = new MappingTool();
         //mappingTool.addClass(Sample.class);
         //mappingTool.addClass(Promt.class);
         mappingTool.addClass(Promt.class);
         mappingTool.addClass(Sample.class);
         mappingTool.addClass(Clazz.class);
         mappingTool.addClass(UserDO.class);
  
         mappingTool.write(new FileWriter(new java.io.File("/mnt/storage/work/mapping_generated.xml")));
     }
   */

  /**
   * makes an object persistend and stores it in an XML-File.
   *
   * @param object TODO
   * @param addition a string that is added to the normal name
   *
   * @throws PersistenceException TODO
   */
  private static void marshall(Object object, String addition)
    throws PersistenceException {
    // TESTSETUP
    String filename = Config.DIRECTORY_PERSISTENCE + Util.getClassName(object) + addition + ".xml";

    //Mapping mapping = new Mapping();
    Marshaller marshaller;

    try {
      //log(thiZ, "..loading mapping.xml");
      //mapping.loadMapping("/mnt/storage/work/mapping_generated.xml");
      //log(thiZ, "creating marshaller incl file for : " + Util.getClassName(object));
      marshaller = new Marshaller(new FileWriter(new File(filename)));

      //log(thiZ, "..setting mapping");
      //marshaller.setMapping(mapping);
      debug(thiZ, "marshal: " + Util.getClassName(object));
      marshaller.marshal(object);

      debug(thiZ, "marshal - done: " + Util.getClassName(object));
    } catch (Exception e) {
      Logger.log(thiZ, "-- UNABLE TO MARSHAL OBJECT --" + Util.getClassName(object) + " " + e.getClass() + "  " + e.getMessage());
      Logger.logAnatomize(object);
      //e.printStackTrace();
      //throw new PersistenceException(e);
    }

    //  1. Load the mapping information from the file
    //mapping.loadMapping("mapping.xml");
    // 2. Unmarshal the data
    //Unmarshaller unmar = new Unmarshaller(mapping);
    //Object order = (Object)unmar.unmarshal(new InputSource(new FileReader("order.xml")));
    // TESTSETUP
    return; /*
    
                                                                                                             debug(thiZ, "--> STORE:starting XML persistence for " + Util.getClassName(object));
                                                                                                             Logger.logAnatomize(object);
                                                                                                             FileWriter fileWriter;
                                                                                                             String filename = Config.DIRECTORY_PERSISTENCE + Util.getClassName(object) + addition + ".xml";
                                                                                                             try {
                                                                                                               debug(object, "stored to " + filename);
                                                                                                               fileWriter = new FileWriter(filename);
    
                                                                                                               Marshaller.marshal(object, fileWriter);
    
                                                                                                             } catch (IOException e) {
                                                                                                               throw new PersistenceException(e);
                                                                                                             } catch (MarshalException e) {
                                                                                                               throw new PersistenceException(e);
                                                                                                             } catch (ValidationException e) {
                                                                                                               throw new PersistenceException(e);
                                                                                                             }
                                                                                                             debug(thiZ, "<-- STORE: finished XML persistence for " + Util.getClassName(object) + "\n\n");
                                                                                                             boolean debug = false;
                                                                                                             if (debug) {
                                                                                                               unmarshall(object.getClass());
                                                                                                             }
    
     */
  }
}
