/**
 * ============================================================================ File:    SampleTemplate.java Package: org.setupx.repository.core.util.hotfix.fsa.sximport cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.hotfix.fsa.sximport;

import org.setupx.repository.core.util.xml.XMLFile;

import java.io.File;
import java.io.IOException;


class SampleTemplate extends Template {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  String comment = "";
  String name = "";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new SampleTemplate object.
   *
   * @param file 
   */
  public SampleTemplate(File file) {
    try {
      XMLFile xmlfile = new XMLFile(file);
      this.name = xmlfile.pathValue("/sample/name");
      this.comment = xmlfile.pathValue("/sample/trayname_generic/Operator_comments");
    } catch (Exception e) {
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param comment TODO
   */
  public void setComment(String comment) {
    this.comment = comment;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getComment() {
    return comment;
  }

  /**
   * TODO: 
   *
   * @param name TODO
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getName() {
    return name;
  }

  /**
   * TODO: 
   *
   * @param file TODO
   *
   * @return TODO
   *
   * @throws IOException TODO
   * @throws UnsupportedOperationException TODO
   */
  public Template newChild(File file) throws IOException {
    throw new UnsupportedOperationException();
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String toString() {
    return this.getID() + " " + this.getName() + " " + this.comment;
  }
}
