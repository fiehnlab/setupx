package org.setupx.repository.core;

public class IllegalParameterValueException extends Exception {
    /**
     * Creates a new IllegalParameterValueException object.
     */
    public IllegalParameterValueException() {
        super();
    }

    public IllegalParameterValueException(String arg0) {
        super(arg0);
    }

    public IllegalParameterValueException(Throwable arg0) {
        super(arg0);
    }

    /**
     * Creates a new IllegalParameterValueException object.
     */
    public IllegalParameterValueException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }
}
