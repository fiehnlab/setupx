package org.setupx.repository.core.communication.exporting;

public class PositionUndefindedException extends Exception {

    public PositionUndefindedException(String string) {
        super(string);
    }

}
