/**
 * ============================================================================ File:    ParsingResult.java Package: org.setupx.repository.core.util.parse.xls cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.parse.xls;

import org.setupx.repository.core.CoreObject;

import java.io.File;
import java.io.IOException;

import java.util.Vector;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class ParsingResult extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @label dataset containing the ids
   */
  public Dataset ids;
  public Vector datasets;
  public int sheetnumber;
  File sourceFile;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * creates a new object - checks the file
   *
   * @param file
   *
   * @throws IOException
   */
  public ParsingResult(File file) throws IOException {
    org.setupx.repository.core.util.File.checkFile(file);
    this.sourceFile = file;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param i TODO
   *
   * @return TODO
   */
  public Dataset get(int i) {
    return (Dataset) this.datasets.get(i);
  }

  /**
   * @param labels
   */
  public void updateLabels(String[] labels) {
    for (int i = 0; i < this.datasets.size(); i++) {
      Dataset dataset = this.get(i);
      this.datasets.remove(0);

      // check values vs. keys
      if (dataset.data.length != labels.length) {
        warning(this, "number of labels (" + labels.length + ") does not match number of values (" + dataset.data.length + ")");
      }

      dataset.key = labels;
    }
  }
}
