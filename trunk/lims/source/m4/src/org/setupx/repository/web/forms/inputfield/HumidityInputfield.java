/**
 * ============================================================================ File:    HumidityInputfield.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import org.setupx.repository.web.forms.inputfield.multi.Restriction4Double;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @hibernate.subclass
 */
public class HumidityInputfield extends StringInputfield {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new HumidityInputfield object.
   *
   * @param question 
   * @param description 
   * @param i_removable 
   */
  public HumidityInputfield(String question, String description, boolean i_removable) {
    this(question, description);
    this.setRemovable(i_removable);
  }

  /**
   * Creates a new HumidityInputfield object.
   */
  public HumidityInputfield() {
    this("...", "...");
  }

  /**
   * @param string
   * @param string2
   */
  public HumidityInputfield(String string, String string2) {
    super(string, string2);
    this.setExtension("%");
    this.setRestriction(new Restriction4Double(0, 100));
    this.setSize_x(5);
  }
}
