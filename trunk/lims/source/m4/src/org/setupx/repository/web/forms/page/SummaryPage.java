/**
 * ============================================================================ File:    SummaryPage.java Package: org.setupx.repository.web.forms.page cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.page;

import org.setupx.repository.core.util.Util;
import org.setupx.repository.web.forms.PromtCreator;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.label.Label;

/**
 * 
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 */
public class SummaryPage extends Page {
    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @param name
     * @param description
     * @param longDescription
     */
    public SummaryPage(String name, String description, String longDescription) {
        super(name, description, longDescription);

        InputField field_label = new Label("created",
                "timestamp when this experiment was created", Util.getDate()
                        + " " + Util.getTime());
        this.addField(field_label);

        InputField field_cvs = new Label("cvs", "software version",
                PromtCreator.version);
        this.addField(field_cvs);
        this.dividerHR = true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.setupx.repository.web.forms.FormObject#createHTMLChilds(int)
     */
    /*
     * public String createHTMLChilds(int numberOfColums) { StringBuffer buffer =
     * new StringBuffer(); String msg = ""; try { msg = "The experiment that you
     * typed in is <b>sufficient</b>." + "<p>If you want you can change the
     * information in each page just by pressing the header of each page." + "<p>If
     * you are sure that all the information is correct, you can submit this
     * information to the central SetupX server" + "by pressing the <b>final
     * submit button</b>. " + "<p>Submitting the information to the central
     * SetupX server does NOT meen, that the samples will be run immediately. " +
     * "The scheduled time for your samples is be " + Schedule.DEFAULT_DATE +
     * "." + "<p>The estimated cost for running the " + root().numberSamples() + "
     * samples is " + root().totalPrice() + ". The price can vary." + "<p>The
     * Metabolomics Core Lab is going to contact you for further information and
     * is going to let you know how and when to send the samples in." + "<br><br><b>Thanks
     * for submitting the information</b><BR> ...we will get in contact with
     * you"; } catch (FormRootNotAvailableException e) { err("unable to create
     * message - root element can not be detected.", e); } buffer.append("<tr><td colspan=\"" + numberOfColums + "\">" +
     * msg + "</td></tr>"); for (int i = 0; i < this.getFields().length; i++) {
     * FormObject object = this.getFields()[i]; // only active objects will be
     * shown if (object.isActive()) { buffer.append("\n<!-- START -- " +
     * object.toString() + " -->\n"); if (this.dividerHR) { buffer.append("<tr><td colspan=\"" + numberOfColums + "\"><hr></td></tr>"); }
     * if (object instanceof Removable && (((Removable)
     * object).createRemoveButton().length() > 2)) { buffer.append("<th valign=\"top\" align=\"right\">" +
     * (i + 1) + "/" + this.getFields().length + " " + ((Removable)
     * object).createRemoveButton() + "</th>"); }
     * buffer.append(object.createHTML(numberOfColums - 1)); buffer.append("\n<!--
     * END -- " + object.toString() + " -->\n"); } } return buffer.toString(); }
     */
}
