package org.setupx.repository.server.workflow;

import org.setupx.repository.Config;
import org.setupx.repository.core.InitException;
import org.setupx.repository.core.util.RunTimeSubclassIdentification;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.UtilException;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.workflow.elements.Process;
import org.setupx.repository.server.workflow.elements.ProcessingException;

import org.xml.sax.SAXException;

import java.io.File;
import java.io.UTFDataFormatException;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;


/**
 * Controlls the WorkFlow of the System.
 * 
 * <p>
 * A process represents different actions that are executed one after the other. The Workflowmanager manages when which of the diffferent processes is executed.
 * </p>
 * 
 * <p>
 * Every process, that can be executed on this system must be registrated in the Workflowmanager. This is done in the Method <code>initProcessables();</code>.
 * </p>
 * 
 * <p>
 * If a Process is not registrated it can <b>not </b> be called and executed using the Workflowmanager. <br/>
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.3 $
 *
 * @link
 * @shapeType PatternLink
 * @pattern Singleton
 * @supplierRole Singleton factory
 */
public class WorkFlowManager {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  Hashtable workflows = new Hashtable();

  // containing all possible elements that can be registrated
  private WFElement[] workFlowElementsPossible = new WFElement[100];

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected WorkFlowManager() throws InitException, WorkFlowException {
    Logger.debug(this, "new instance");
    initProcessables();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * return an array containing all workflows
   *
   * @return an array containing all workflows
   *
   * @throws InitException
   */
  public Workflow[] getWorkflows() throws InitException {
    Vector resultVec = new Vector();

    Enumeration enumeration = workflows.elements();
    int i = 0;

    while (enumeration.hasMoreElements()) {
      Workflow element = (Workflow) enumeration.nextElement();

      if (element == null) {
        throw new InitException("unable to find all wf-Elements in this WF");
      }

      resultVec.add(element);
    }

    Workflow[] result = new Workflow[resultVec.size()];

    // convert vec to array
    for (int j = 0; j < result.length; j++) {
      result[j] = (Workflow) resultVec.get(j);
    }

    return result;
  }

  /**
   * checking an array of ids if all these ids exist as ids in the system - if not throw exception
   *
   * @param workflowElementIDs
   *
   * @throws WorkFlowException one of the ids was not found
   */
  public void checkWorkFlowIDs(int[] workflowElementIDs)
    throws WorkFlowException {
    for (int i = 0; i < workflowElementIDs.length; i++) {
      boolean found = false;

      for (int j = 0; ((j < workFlowElementsPossible.length) && !found); j++) {
        if ((workFlowElementsPossible[j] != null) && (workFlowElementsPossible[j].configuration.getId() == workflowElementIDs[i])) {
          Logger.debug(this, "check: found element for id " + workflowElementIDs[i] + " is " + workFlowElementsPossible[j].configuration.getLabel());
          found = true;
        }
      }

      if (!found) {
        throw new WorkFlowException("the workflowelement with the id: " + workflowElementIDs[i] + " is not registrated in the system.");
      }
    }
  }

  /**
   * TODO: 
   *
   * @param file TODO
   *
   * @return
   *
   * @throws InitException TODO
   */
  public Workflow newWorkFlow(WFConfigFile file) throws InitException {
    Workflow workflowTemp = new Workflow(file.getWorkFlowConfiguration());
    workflows.put(workflowTemp.configuration.id, workflowTemp);

    return workflowTemp;
  }

  /**
   * TODO update doc... register a WFElement in the manager. When the WFElement is registrated it can be executed just by using the id from the configuration. This method is <b>just </b> called by the constructor of WFElement.
   *
   * @param element the WFElement, which will be registrated in the manager.
   * @param replace defines if the WFElement will take the place of the older process if this already exists. Otherwise ther will be an exception.
   *
   * @throws WorkFlowException if a WFElement with the same id has already been registrated <b>AND </b> the flag for replacing older processables is not set.
   *
   * @see WFElement the elements
   */
  public void register(WFElement element, Workflow workflow, boolean replace)
    throws WorkFlowException {
    Logger.log(this, "--start registrating " + element + " at pos(id) " + element.getConfiguration().getId());

    // add a reference to the workflow ...
    element.setWorkflow(workflow);

    try {
      element.init();
    } catch (InitException e1) {
      throw new WorkFlowException(e1);
    }

    WFElement oldProcess = null;

    // debug
    boolean DEBUG = false;

    if (DEBUG) {
      for (int i = 0; i < workflow.registratedProcessables.length; i++) {
        WFElement element2 = (WFElement) workflow.registratedProcessables[i];

        //if (element2 != null) 
        Logger.log(this, "current element: " + i + "  " + element2);
      }
    }

    try {
      oldProcess = (WFElement) workflow.registratedProcessables[element.getConfiguration().getId()];
    } catch (Exception e) {
      oldProcess = null;
    }

    if (oldProcess != null) // in case that there is an existing process at this position.
     {
      Logger.debug(this, "there is already a process whith the id: " + element.getConfiguration().getId() + " registrated. The label is: " + element.getConfiguration().getLabel());

      if (replace) {
        // this.registratedProcessables.remove(oldProcess);
        Logger.log(this, "process with id " + element.getConfiguration().getId() + " is replaced.");
        workflow.registratedProcessables[element.getConfiguration().getId()] = element;
      } else {
        throw new WorkFlowException("process with this id is already registrated");
      }
    } else // starndard registration of elements ...  
     {
      Logger.log(this, "" + element);
      Logger.log(this, "" + workflow);

      workflow.registratedProcessables[element.getConfiguration().getId()] = element;
      Logger.log(this, " stored element at pos " + element.getConfiguration().getId());

      if (DEBUG) {
        Logger.debug(this, " DEBUG : _---------------------------------------");

        for (int i = 0; i < workflow.registratedProcessables.length; i++) {
          WFElement element2 = (WFElement) workflow.registratedProcessables[i];

          //if (element2 != null) 
          Logger.log(this, "current element: " + i + "  " + element2);
        }
      }
    }

    Logger.log(this, "--finished registrating " + element + " at pos(id) " + element.getConfiguration().getId());
  }

  /**
   * @param wfElementIds
   * @param workflow
   *
   * @throws WorkFlowException
   */
  public void register(int[] wfElementIds, Workflow workflow)
    throws WorkFlowException {
    for (int i = 0; i < wfElementIds.length; i++) {
      int j = wfElementIds[i];
      register(this.workFlowElementsPossible[j], workflow, false);
    }
  }

  /**
   * TODO: 
   *
   * @param i TODO
   *
   * @throws ProcessingException
   * @throws WorkFlowException
   */
  public void runWorkflow(String i) throws WorkFlowException, ProcessingException {
    ((Workflow) this.workflows.get(i)).start();
  }

  /**
   * creates an array of Objects, that do implements the interface WFElement in the package where the class Process is located -  this package contains the different implementations of WFelement, that will be added to the Array.
   * 
   * <p>
   * This is done by <code><b>REFLECTION</b></code> so avoid using it to often - it is designed to be used at startup
   * </p>
   *
   * @return an array containing all subclasses of WFElement.
   *
   * @throws WorkFlowException unable to init all classes
   * @throws InitException unable to init all classes
   *
   * @see WFElement
   * @see Process
   */
  protected WFElement[] determineProcessables() throws InitException, WorkFlowException {
    WFElement[] result;

    // finding all implementations that do implement WFElement in a package
    Package packagge = Process.class.getPackage();
    Class[] classes;

    try {
      classes = WFElement.findImplementations(packagge.getName());
    } catch (UtilException e1) {
      throw new InitException("problems finding implementaions: ", e1);
    }

    // create instances these classes
    result = new WFElement[classes.length];

    for (int i = 0; i < classes.length; i++) {
      try {
        result[i] = (WFElement) classes[i].newInstance();

        //Logger.log(this, result[i].toString());
      } catch (IllegalAccessException e) {
        throw new RuntimeException("unable to init all classes ", e);
      } catch (InstantiationException e) {
        throw new RuntimeException("unable to init all classes ", e);
      }
    }

    return result;
  }

  /**
   * @throws InitException
   */
  void initWorkflows() throws InitException {
    Logger.log(this, "start init the workflows ...");

    File directory = new File(Config.DIRECTORY_WORKFLOWS);

    File[] files = directory.listFiles();

    if (files.length == 0) {
      throw new InitException("unable to find the configfiles for the workflows in " + directory.getAbsolutePath());
    }

    for (int i = 0; i < files.length; i++) {
      try {
        // register each of the files
        newWorkFlow(new WFConfigFile(files[i]));
      } catch (UTFDataFormatException e) {
        Logger.log(this, e);
      } catch (InitException e) {
        Logger.log(this, e);
      } catch (SAXException e) {
        Logger.log(this, e);
      }
    }
  }

  /**
   * registrates a default setting of processes to the Manager. More processes can be added at every time manualy. This method is called one at <b>startup</b>. Later an WFElement can be registrated manually.
   *
   * @throws InitException
   * @throws WorkFlowException
   *
   * @deprecated since aug18 2004
   */
  private void initProcessables() throws InitException, WorkFlowException {
    boolean useNewImplementaion = false;
    Logger.debug(this, "---------------------------------------------------------------------------------------------------------------");
    Logger.debug(this, "                 			INIT MYSELF    -- START");
    Logger.debug(this, "---------------------------------------------------------------------------------------------------------------");
    Logger.debug(this, "initProcessables() ");

    if (!useNewImplementaion) {
      Logger.debug(this, " -start---- creating all possible WorkflowElements -----");

      WFElement[] elements = determineProcessables();
      Logger.debug(this, "found " + elements.length + " objects");
      Logger.debug(this, " -end  ---- creating all possible WorkflowElements -----");
      Logger.debug(this, " -start ---- registrating the new created WorkflowElements -----");

      for (int i = 0; i < elements.length; i++) {
        WFElement element = elements[i];

        this.registerPossibleElements(element);
      }

      Logger.debug(this, " -end   ---- registrating the new created WorkflowElements -----");
    } else {
      // test to load the files dynamicaly
      // superclass
      Class superClass = Process.class;
      RunTimeSubclassIdentification.find(superClass.getPackage().toString(), superClass.getName());
    }

    for (int i = 0; i < workFlowElementsPossible.length; i++) {
      WFElement element = workFlowElementsPossible[i];

      if (element != null) {
        Logger.log(this, "Registrated Element no. " + i + " " + Util.getClassName(element) + "\t" + element.getConfiguration().toString());
      }
    }

    Logger.debug(this, "---------------------------------------------------------------------------------------------------------------");
    Logger.debug(this, "                 			INIT MYSELF    -- DONE");
    Logger.debug(this, "---------------------------------------------------------------------------------------------------------------");
  }

  /**
   * TODO: 
   *
   * @param element TODO
   *
   * @throws InitException TODO
   */
  private void registerPossibleElements(WFElement element)
    throws InitException {
    if (this.workFlowElementsPossible[element.getConfiguration().getId()] != null) {
      throw new InitException("there are elements with the same id" + element.getConfiguration().getId());
    }

    this.workFlowElementsPossible[element.getConfiguration().getId()] = element;
  }
}
