package org.setupx.repository.web.forms.inputfield;

import junit.framework.TestCase;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.web.forms.inputfield.StringInputfield;

public class StringInputfieldTest extends TestCase {

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Constructor for StringInputfieldTest.
     * @param arg0
     */
    public StringInputfieldTest(String arg0) {
        super(arg0);
    }

    public void testCreateHTML() {
    }

    /*
     * Class under test for void StringInputfield()
     */
    public void testStringInputfield() {
    }

    /*
     * Class under test for void StringInputfield(String, String, Restriction)
     */
    public void testStringInputfieldStringStringRestriction() {
    }

    /*
     * Class under test for void StringInputfield(String, String, String, Restriction)
     */
    public void testStringInputfieldStringStringStringRestriction() {
    }

    public void testValidate() {
        StringInputfield inputfield = new StringInputfield("frage","description", false);
        Logger.log(this,inputfield.validate());
    }

    /*
     * Class under test for void InputField()
     */
    public void testInputField() {
    }

    /*
     * Class under test for void InputField(String, String, Restriction)
     */
    public void testInputFieldStringStringRestriction() {
    }

    public void testSetValue() {
    }

    public void testGetValue() {
    }

    public void testGetRestriction() {
    }

    public void testSetRestriction() {
    }

    public void testGetQuestion() {
    }

    public void testSetQuestion() {
    }

    public void testValidationAnswer() {
    }

    /*
     * Class under test for void FormObject(String, String)
     */
    public void testFormObjectStringString() {
    }

    /*
     * Class under test for void FormObject()
     */
    public void testFormObject() {
    }

    public void testGetDescription() {
    }

    public void testSetDescription() {
    }

    /*
     * Class under test for String toString()
     */
    public void testToString() {
    }

    public void testGetFields() {
    }

    public void testCreateHTMLChilds() {
    }

    public void testIsValid() {
    }

    /*
     * Class under test for FormObject getField(int)
     */
    public void testGetFieldint() {
    }

    /*
     * Class under test for FormObject getField(String)
     */
    public void testGetFieldString() {
    }

    public void testSetFields() {
    }

    public void testGetName() {
    }

    public void testSetName() {
    }

}
