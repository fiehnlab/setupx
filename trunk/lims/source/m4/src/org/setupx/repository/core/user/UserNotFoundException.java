package org.setupx.repository.core.user;

import org.setupx.repository.core.util.logging.Logger;

/**
 * unable to find user
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 */
public class UserNotFoundException extends Exception {

    {
        Logger.warning(this, " new instance");
    }

    /**
   * Creates a new UserNotFoundException object.
   *
   * @param msg 
   */
  public UserNotFoundException(String msg) {
    super(msg);
  }

  /**
   * Creates a new UserNotFoundException object.
   *
   * @param ex 
   */
  public UserNotFoundException(Exception ex) {
    super(ex);
  }

  /**
   * Creates a new UserNotFoundException object.
   *
   * @param msg 
   * @param ex 
   */
  public UserNotFoundException(String msg, Exception ex) {
    super(msg, ex);
  }
}
