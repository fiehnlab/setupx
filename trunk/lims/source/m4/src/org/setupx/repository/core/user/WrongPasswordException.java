package org.setupx.repository.core.user;

/**
 * woring password

 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class WrongPasswordException extends Exception {
  /**
   *
   */
  public WrongPasswordException() {
    super();

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public WrongPasswordException(String arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public WrongPasswordException(Throwable arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   */
  public WrongPasswordException(String arg0, Throwable arg1) {
    super(arg0, arg1);

    // TODO Auto-generated constructor stub
  }
}
