package org.setupx.repository.server.persistence;


public class PersistenceActionFindException extends PersistenceActionException {

    
    public PersistenceActionFindException(String string, Throwable  e) {
        super(string, e);
    }

    
    public PersistenceActionFindException(Throwable e) {
        super(e);
    }

    public PersistenceActionFindException(String string) {
        super(string, null);
    }

}
