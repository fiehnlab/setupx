/**
 * ============================================================================ File:    LexiRestriction.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.core.communication.vocabulary.Library;
import org.setupx.repository.core.communication.vocabulary.VocabularyChecker;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.ValidationAnswer;
import org.setupx.repository.web.forms.validation.ValidationException;


/**
 * Checks a word agains a libraries. You can use it with a single library or with all availble.
 * 
 * <p>
 * <b>If you dont define a library it uses all:</b>
 * </p>
 * 
 * <p>
 * <code>if (this.libraryID == null) {<br> response = VocabularyChecker.check4suggestion(entry);<br>} else {<br> response = VocabularyChecker.check4suggestion(entry, this.libraryID);<br>}<br></code><br>
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.17 $
 *
 * @hibernate.subclass
 */
public class LexiRestriction extends Restriction {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String libraryID = null;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new LexiRestriction object.
   */
  public LexiRestriction() {
    super();
  }

  /**
   * Creates a new LexiRestriction object.
   *
   * @param field 
   * @param library_id 
   */
  public LexiRestriction(String library_id) {
    this.libraryID = library_id;
  }

  /**
   * Creates a new LexiRestriction object.
   *
   * @param field 
   * @param library_id 
   */
  public LexiRestriction(Library library) {
    this.libraryID = Util.getClassName(library);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param libraryID TODO
   */
  public void setLibraryID(String libraryID) {
    this.libraryID = libraryID;
  }

  /**
   * @hibernate.property
   */
  public String getLibraryID() {
    return libraryID;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#cloneRestriction()
   */
  public Restriction cloneRestriction() {
    return new LexiRestriction(this.getLibraryID());
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws ValidationException TODO
   */
  public ValidationAnswer validateObject(InputField field)
    throws ValidationException {
    String entry = (String) field.getValue();
    String[] response;

    if (this.libraryID == null) {
      response = VocabularyChecker.check4suggestion(entry);
    } else {
      response = VocabularyChecker.check4suggestion(entry, this.libraryID);
    }

    // if empty return null 
    if ((response == null) || (response.length == 0)) {
      return null;
    }

    ValidationAnswer answer = new ValidationAnswer(field, "found misspelling: ");

    for (int i = 0; i < response.length; i++) {
      if (response[i] != null) {
        answer.add(response[i]);
      }
    }

    return answer;
  }
}
