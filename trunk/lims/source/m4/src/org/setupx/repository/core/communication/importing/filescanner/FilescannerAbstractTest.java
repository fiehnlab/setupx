package org.setupx.repository.core.communication.importing.filescanner;

import java.io.File;

import junit.framework.TestCase;
import org.setupx.repository.core.InitException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class FilescannerAbstractTest extends TestCase {
  /**
   * Creates a new FilescannerAbstractTest object.
   *
   * @param arg0 
   */
  public FilescannerAbstractTest(String arg0) {
    super(arg0);
  }

  /**
   * TODO: 
   *
   * @param args TODO
   */
  public static void main(String[] args) {
    junit.swingui.TestRunner.run(FilescannerAbstractTest.class);
  }

  /**
   * TODO: 
   *
   * @throws InitException TODO
   * @throws InterruptedException TODO
   */
  public void testFilescannerAbstract() throws InitException, InterruptedException {
    Thread scannerThread = new SimpleScannerThread(new File("C:\\testing"), 1000);
    long time = 10;
    Thread.sleep(time);

    //scannerThread.run();
  }
}
