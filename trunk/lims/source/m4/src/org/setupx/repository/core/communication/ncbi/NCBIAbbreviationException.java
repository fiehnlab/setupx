package org.setupx.repository.core.communication.ncbi;

public class NCBIAbbreviationException extends Exception {

    public NCBIAbbreviationException(String string) {
        super(string);
    }
}
