package org.setupx.repository.core.query.answer;

import java.util.Vector;

import org.setupx.repository.core.communication.ncbi.NCBIConnector;
import org.setupx.repository.core.communication.ncbi.NCBIException;
import org.setupx.repository.core.communication.ncbi.local.NCBIEntry;
import org.setupx.repository.core.query.QueryMasterAnswer;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.util.Link;

public class NCBIQueryMasterAnswer extends QueryMasterAnswer {

    private static String icon = null;
    private NCBIEntry ncbiEntry = null;

    static{
        icon = "pics/species.gif";
    }

    /**
     * create an {@link QueryMasterAnswer} based on an NCBI-Code
     * @param i
     */
    public NCBIQueryMasterAnswer(int ncbiID) {
        try {
            this.ncbiEntry = NCBIConnector.determineNCBI_Information(ncbiID);
        } catch (NCBIException e) {
            e.printStackTrace();
        }
    }

    /*
     * (non-Javadoc)
     * @see org.setupx.repository.core.query.QueryMasterAnswer#getIcon()
     */
    public String getIcon() {
        return icon;
    }

    /*
     * (non-Javadoc)
     * @see org.setupx.repository.core.query.QueryMasterAnswer#getRelatedObjectClass()
     */
    public Class getRelatedObjectClass() {
        return NCBIEntry.class;
    }

    /*
     * (non-Javadoc)
     * @see org.setupx.repository.core.query.QueryMasterAnswer#getRelatedObjectID()
     */
    public String getRelatedObjectID() {
        return ncbiEntry.getTaxID();
    }

    public String getInfo1() {
        return "NCBI ID: " + ncbiEntry.getTaxID();
    }

    public String getInfo2() {
        try {
            return "parent group: " + ncbiEntry.getSplitpointParent().getName();
        } catch (NCBIException e) {
            return "";
        }
    }

    public String getInfo3() {
        return "classification: " + ncbiEntry.getSpeciesTypeName();
    }

    public String getLabel() {
        return ncbiEntry.getName();
    }

    public Object getAnswerSource() {
        return this.ncbiEntry;
    }

    public Class getAnswerSourceType() {
        return ncbiEntry.getClass();
    }


    public Vector getRelatedObjectLinks() {
        Vector vector = new Vector();
        vector.add(new Link("species: list for " + ncbiEntry.getName(), "species_detail.jsp?ncbiID=" + this.ncbiEntry.getTaxID()));
        vector.add(new Link("compounds for this species " ,"compPerSpecies.jsp?ncbiID=" + ncbiEntry.getTaxID()));
        try {
            vector.add(new Link("search for " + ncbiEntry.getSplitpointParent().getName() + "(parent species)", "species_detail.jsp?ncbiID=" + this.ncbiEntry.getSplitpointParent().getTaxID()));
        } catch (NCBIException e) {
            e.printStackTrace();
        }
        vector.add(new NCBILink("NCBI Taxonomy", ncbiEntry.getTaxIdInt())); 

        return vector;
    }

    public boolean checkAccessRestriction(UserDO userDO) {
        throw new UnsupportedOperationException("Operation unsupported cause access is not restricted.");
    }

    /*
     * (non-Javadoc)
     * @see org.setupx.repository.core.query.QueryMasterAnswer#isAccessRestricted()
     */
    public boolean isAccessRestricted() {
        return false;
    }
}
