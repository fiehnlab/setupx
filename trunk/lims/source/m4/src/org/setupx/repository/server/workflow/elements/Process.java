package org.setupx.repository.server.workflow.elements;

import org.setupx.repository.core.InitException;
import org.setupx.repository.server.workflow.WFElement;
import org.setupx.repository.server.workflow.WorkFlowException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.1 $
 */
public abstract class Process extends WFElement {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * constructor is called by the WorkFlowManager - all instances of process are registered when the Instance of this class is created
   *
   * @param configuration
   *
   * @throws InitException
   * @throws WFMException
   */
  protected Process(ProcessConfiguration configuration)
    throws InitException, WorkFlowException {
    this.configuration = configuration;
  }

  protected Process() throws InitException, WorkFlowException {
    this.configuration = new StandardConfiguration(WFElement.DEFAULT_ID_PROCESS);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   * (non-Javadoc)
   *
   * @see org.setupx.repository.server.workflow.Processable#action()
   */
  public abstract void action() throws ProcessingException;
}
