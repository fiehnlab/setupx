/**
 * ============================================================================ File:    MachineRunInfo.java Package: org.setupx.repository.core.communication.leco cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.leco;

import java.util.Hashtable;
import java.util.List;

import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.SXQuery;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class MachineRunInfo {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private ColumnInformation columnInformation = null;
  private Hashtable information;
  private LecoACQFile sourcefile = null;
  private String acqname = "";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new MachineRunInfo object.
   *
   * @param sampleID 
   *
   * @throws MissingMachineInformationException 
   */
  public MachineRunInfo(long sampleID) throws MissingMachineInformationException {
    try {
      acqname = new SXQuery().findAcquisitionNameBySampleID(sampleID);
      // cut the extension at the end of the filename of
      acqname = acqname.substring(0, acqname.indexOf(":"));

      this.sourcefile = LecoACQFile.determineFile(acqname);

      // all informaiton that can be retrieved
      this.information = LecoACQFile.getValuesPerACQname(acqname);

      // determine the columntype
      this.columnInformation = ColumnDetector.getColumnInformationByMethod(this.sourcefile.findGC(acqname).getValue()); //determineColumn(this.getInformation());

      if (this.columnInformation == null) {
        throw new ColumnInformationNotAvailableException("columnInformation is null");
      }
    } catch (PersistenceActionFindException e) {
      throw new MissingMachineInformationException("Unable to find the Samplename (acqname) for Sample " + sampleID, e);
    } catch (AcquisitionFileQueryException e) {
      throw new MissingMachineInformationException("Unable to find a coressponding Acquistion for Acquistionsample " + acqname, e);
    } catch (ColumnInformationNotAvailableException e) {
      throw new MissingMachineInformationException("Unable to find a coressponding Column for Acquistionsample " + acqname, e);
    } catch (Exception e) {
      throw new MissingMachineInformationException("Unable to find a coressponding Column for Acquistionsample due to unknown reason." + acqname, e);
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * find out which column was used for a sample.
   * 
   * <p></p>
   *
   * @param promtID
   *
   * @return
   *
   * @throws MissingMachineInformationException
   */
  public static MachineRunInfo determineMachineRunInfo(long sampleID)
    throws MissingMachineInformationException {
    return new MachineRunInfo(sampleID);
  }

  /**
   * The result is based on the <b>first</b> sample.
   *
   * @param promtID the promt id.
   *
   * @return infos for the <b>first</b> samples
   *
   * @throws MissingMachineInformationException unable to find machine information for the first sample.
   */
  public static MachineRunInfo determineMachineRunInfoByPromt(long promtID)
    throws MissingMachineInformationException {
    // get the first sampleID 
    Logger.debug(MachineRunInfo.class, "looking for machineruninfo for " + promtID);

    List list = new SXQuery().findSampleIDsByPromtID(promtID);
    Logger.debug(MachineRunInfo.class, "number of sample insides " + promtID + ": " + list.size());

    int sampleID = Integer.parseInt(list.get(0) + "");
    Logger.debug(MachineRunInfo.class, "looking for machineruninfo for sample " + sampleID);

    return determineMachineRunInfo(sampleID);
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getAcqname() {
    return acqname;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public ColumnInformation getColumnInformation() {
    return columnInformation;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public Hashtable getInformation() {
    return information;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public LecoACQFile getSourcefile() {
    return sourcefile;
  }
  
  
  public static void test(){
      try {
          new Method("bla","foo",ColumnInformation.TESTCOLUMN.getBbid()).update(true);
          
        Logger.log(null, MachineRunInfo.determineMachineRunInfo(104411).getColumnInformation().getDescription());
        //Logger.log(null, org.setupx.repository.core.communication.leco.ColumnDetector.getColumnInformationByMethod(method.getValue()).getLable());
    } catch (MissingMachineInformationException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (PersistenceActionUpdateException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }

  }
}
