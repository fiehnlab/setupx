/**
 * ============================================================================ File:    SOP.java Package: org.setupx.repository.core.communication.sop cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.sop;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.InitException;
import org.setupx.repository.core.communication.leco.Method;
import org.setupx.repository.core.communication.notification.NotificationCentral;
import org.setupx.repository.core.communication.notification.SystemNotification;

import java.net.MalformedURLException;
import java.net.URL;

import org.hibernate.Session;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public abstract class SOP {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String description;
  private String version = "original";
  private URL link = null;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @deprecated use Alignment SOP
   */
  public static final String SOP_Vial_alignment_2005_09 = "align05";
  public static final int PREPERATION = 1;
  public static final int ALIGNMENT = 2;
  public static final int SPLITRATIO = 3;
  private static SOP sopPrep = new PrepSOP();
  private static SOP sopAlignment = new AlignmentSOP();
  private static SOP sopSplitratio = new SplitratioSOP();

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public final String getName() {
    return this.description;
  }

  /**
   * TODO: 
   *
   * @param description TODO
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * TODO: 
   *
   * @param link TODO
   */
  public void setLink(URL link) {
    this.link = link;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public URL getLink() {
    return this.link;
  }

  /**
   * TODO: 
   *
   * @param type TODO
   *
   * @return TODO
   */
  public static SOP[] determineSOPs(int type) {
    try {
      switch (type) {
      case PREPERATION:
        return sopPrep.instances();

      case ALIGNMENT:
        return sopAlignment.instances();

      case SPLITRATIO:
          // replaced with dynamic access to the available splitratios on the machine
          return sopSplitratio.instances();
          //return sops;

      default:
        throw new UnsupportedOperationException("SOP type: " + type + " is not supported.");
      }
    } catch (InitException e) {
      NotificationCentral.notify(new SystemNotification("unable to create SOP", e));

      return new SOP[] {  };
    }
  }

  
  /**
   * find splitratios defined as methods for the machines 
   * @return
   *//*
  private static SOP[] determineSplitratios() {
      Session s = CoreObject.createSession();
      
      // query for all methods
      java.util.Iterator iterator = s.createSQLQuery("select {method.*} from machinemethod method where method.machine is null order by method.label")
          .addEntity("method", Method.class).list().iterator();
      
      while (iterator.hasNext()){
          // filter methods 

          // create an SOP out of each method
          
      }
      
      // return SOPs
  }
  /*

/**
   * create an array of values containing the descriptions of the default values.
   *
   * @return
   */
  public abstract String[][] getValues();

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getDescription() {
    return description;
  }

  /**
   * TODO: 
   *
   * @param version TODO
   */
  public void setVersion(String version) {
    this.version = version;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getVersion() {
    return version;
  }

  /**
   * TODO: 
   *
   * @param value TODO
   * @param link TODO
   *
   * @return TODO
   *
   * @throws InitException TODO
   */
  public SOP createInstance(String value, URL link) throws InitException {
    SOP instance;

    try {
      instance = (SOP) this.getClass().newInstance();
    } catch (InstantiationException e) {
      throw new InitException(e);
    } catch (IllegalAccessException e) {
      throw new InitException(e);
    }

    instance.setDescription(value);
    instance.setLink(link);

    return instance;
  }

  /**
   * TODO: 
   *
   * @param string TODO
   *
   * @return TODO
   *
   * @throws InitException TODO
   */
  public SOP createInstance(String string) throws InitException {
    return this.createInstance(string, null);
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws InitException TODO
   */
  private final SOP[] instances() throws InitException {
    String[][] values = this.getValues();
    SOP[] result = new SOP[values.length];

    for (int i = 0; i < values.length; i++) {
      URL url;

      try {
        url = new URL(values[i][1]);
      } catch (MalformedURLException e) {
        url = null;
      }

      result[i] = this.createInstance(values[i][0], url);
    }

    return result;
  }
}
