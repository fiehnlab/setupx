/**
 * ============================================================================ File:    Move.java Package: org.setupx.repository.core.util cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util;

import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.core.util.xml.XMLFile;
import org.setupx.repository.core.util.xml.XPath;
import org.setupx.repository.core.util.xml.XPathException;

import org.w3c.dom.DOMException;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.UTFDataFormatException;

import java.util.HashSet;
import java.util.Vector;


/**
 * Copy a set files from one defined place to another. The Files must contain a given String
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.7 $
 */
public class Move {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final String FILE_IDS = "C:\\dev\\source\\M1_m2_14july04\\conf\\converter\\ids_fsa.txt";
  private static final Object thiz = new Move();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   *
   */
  public Move() {
    // TODO Auto-generated constructor stub
  }

  /**
   * @param directory
   * @param key
   * @param targetDirectory
   *
   * @throws IOException
   */
  public Move(java.io.File directory, String key, java.io.File targetDirectory)
    throws IOException {
    new Move(directory, new String[] { key }, targetDirectory);
  }

  /**
   * Creates a new Copy object and moves all Files containing one of the keys to the target directory
   *
   * @param directory the directory where the files will be looked after
   * @param keys one of these keys must be in the files to move it ...
   * @param targetDirectory the directory where the found files will be moved to.
   *
   * @throws IOException problems while reading or writing the files
   */
  public Move(java.io.File directory, String[] keys, java.io.File targetDirectory)
    throws IOException {
    Logger.debug(this, "\n\nlooking for files in " + directory.getAbsolutePath() + " that conatin a key ");

    for (int i = 0; i < keys.length; i++) {
      Logger.log(this, keys[i]);
    }

    Logger.debug(this, "and moves them to " + targetDirectory.getAbsolutePath());

    java.io.File[] files = directory.listFiles();

    String content = "";

    for (int i = 0; i < files.length; i++) {
      boolean found = false;
      java.io.File file = files[i];

      if (!file.isDirectory()) {
        // check if file contains String
        try {
          content = org.setupx.repository.core.util.File.getFileContent(file);
        } catch (IOException e) {
          if (file.isDirectory()) {
          } else {
            e.printStackTrace();
            Logger.err(this, file.getAbsolutePath());
          }
        }

        for (int j = 0; j < keys.length; j++) {
          if (content.lastIndexOf(keys[j]) > 0) {
            found = true;
          }
        }

        if ((i % 100) == 0) {
          System.out.print("\n");
        }

        if (found) {
          org.setupx.repository.core.util.File.move(targetDirectory, file);
          System.out.print('m');

          //Logger.log(this, "moved file " + file.getAbsolutePath());
        } else {
          System.out.print(' ');
        }
      } else {
        System.out.print('D');
      }
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param args TODO
   *
   * @throws IOException TODO
   */
  public static void main(String[] args) throws IOException {
    // look for fsa samples
    java.io.File RESULT_DIRECTORY = new java.io.File("c:\\result\\ok");
    java.io.File FSA_DIRECTORY = new java.io.File("C:\\result\\fsa");

    java.io.File FSA_BASELINE = new java.io.File("C:\\result\\fsa\\baseline");

    java.io.File FSA_ENV = new java.io.File("C:\\result\\fsa\\env");

    java.io.File FSA_FIELDTRIAL_2001 = new java.io.File("C:\\result\\fsa\\fieldtrails2001");
    java.io.File FSA_FIELDTRIAL_2003 = new java.io.File("C:\\result\\fsa\\fieldtrails2003");

    java.io.File FSA_ERROR = new java.io.File("C:\\result\\fsa\\err");

    java.io.File SOURCE1 = new java.io.File("C:\\result\\STATUS_INVALID_CLASSID");
    java.io.File SOURCE2 = new java.io.File("C:\\result\\splitfiles");
    java.io.File SOURCE3 = new java.io.File("C:\\result\\STATUS_INVALID_PARAMETER_VALUE");

    java.io.File SOURCE_CONFIGERRORS = new java.io.File("C:\\result\\STATUS_CONFIG_ERROR");

    java.io.File FSA_DAMAGED_DIR = new java.io.File("C:\\result\\fsa\\damaged");

    Logger.debug(null, " \n\n------ looking for fsa samples -----");

    // alle fsa proben ...
    new Move(RESULT_DIRECTORY, new String[] { "FSA", "fsa" }, FSA_DIRECTORY);
    new Move(RESULT_DIRECTORY, new String[] { "Dana Wiese" }, FSA_DIRECTORY);

    // wiederholungsmessungen, wenzel...
    new Move(FSA_DIRECTORY, new String[] { "Wiederholung", "wenzel", "Wenzel" }, FSA_ERROR);

    Logger.debug(thiz, " \n\n------ checking if all files are present ");

    Vector result = File.checkFileExisting(FSA_DIRECTORY.getAbsolutePath(), new java.io.File(FILE_IDS));
    Logger.debug(thiz, " num of files found : " + result.size());

    Logger.debug(thiz, " \n\n------ looking for solanum samples -----");
    new Move(FSA_DIRECTORY, "solanum", FSA_FIELDTRIAL_2003);

    Logger.debug(thiz, " \n\n------ looking for perturbation samples -----");
    new Move(FSA_DIRECTORY, new String[] { "high light", "highlight", "nitrogen", "cold", "fertilizer", "ammonium" }, FSA_ENV);

    Logger.debug(thiz, " \n\n------ looking for baseline samples -----");
    new Move(FSA_DIRECTORY, new String[] { "control", "Control" }, FSA_BASELINE);

    // alle die nicht konvertiert werden konnten (invalid class id,
    new Move(SOURCE1, new String[] { "Dana Wiese" }, FSA_DAMAGED_DIR);
    new Move(SOURCE2, new String[] { "Dana Wiese" }, FSA_DAMAGED_DIR);
    new Move(SOURCE3, new String[] { "Dana Wiese" }, FSA_DAMAGED_DIR);
    new Move(SOURCE1, new String[] { "FSA", "fsa" }, FSA_DAMAGED_DIR);
    new Move(SOURCE2, new String[] { "FSA", "fsa" }, FSA_DAMAGED_DIR);
    new Move(SOURCE3, new String[] { "FSA", "fsa" }, FSA_DAMAGED_DIR);
    new Move(FSA_DAMAGED_DIR, new String[] { "Wiederholung", "wenzel", "Wenzel" }, FSA_ERROR);
    new Move(new java.io.File("C:\\result\\waste"), new String[] { "FSA", "fsa" }, FSA_ERROR);

    int sum = 0;

    sum = sum + show(FSA_FIELDTRIAL_2001, 2182);
    sum = sum + show(FSA_FIELDTRIAL_2003, 1440);
    sum = sum + show(FSA_BASELINE, (476 + 270));
    sum = sum + show(FSA_ENV, (1280 + 259));
    Logger.debug(thiz, "-------------------------------------");
    sum = sum + show(FSA_ERROR, 1);
    sum = sum + show(FSA_DAMAGED_DIR, 1);

    // das wort FSA in excelsheet + dana wiese 
    int totalNeeded = 4186;

    double ratio = (100 / (double) totalNeeded) * ((double) (totalNeeded - sum));

    Logger.debug(thiz, "found " + sum + " of " + totalNeeded + " missing: " + (totalNeeded - sum) + "   " + ratio + "% missing");

    java.io.File[] directoriesToClean = new java.io.File[] { FSA_ERROR, FSA_DAMAGED_DIR, SOURCE_CONFIGERRORS };

    cleanUp(FSA_BASELINE, directoriesToClean);
    cleanUp(FSA_ENV, directoriesToClean);
    cleanUp(FSA_FIELDTRIAL_2001, directoriesToClean);
    cleanUp(FSA_FIELDTRIAL_2003, directoriesToClean);

    directoriesToClean = new java.io.File[] { SOURCE_CONFIGERRORS, SOURCE1, SOURCE2, SOURCE3 };
    cleanUp(RESULT_DIRECTORY, directoriesToClean);
    cleanUp(new java.io.File("C:\\result\\standard"), directoriesToClean);

    // checking the FSA samples again - and list all sample/trayname_generic/Extraction-SOP-no and the name
    XMLFile file = null;

    try {
      checkFSAfile(FSA_BASELINE);
      checkFSAfile(FSA_ENV);
      checkFSAfile(FSA_FIELDTRIAL_2001);
      checkFSAfile(FSA_FIELDTRIAL_2003);
    } catch (UTFDataFormatException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (XPathException e) {
      e.printStackTrace();
    }
  }

  /**
   * checks the FSA files - open them and look for a number of parameters ...
   *
   * @param fsa_fieldtrial_2003
   *
   * @throws SAXException
   * @throws UTFDataFormatException
   * @throws XPathException
   */
  private static void checkFSAfile(java.io.File fsa_direcory)
    throws UTFDataFormatException, SAXException, XPathException {
    java.io.File[] files = fsa_direcory.listFiles();
    XMLFile fsaSample = null;

    for (int i = 0; i < files.length; i++) {
      fsaSample = new XMLFile(files[i]);

      // checking the fields
      String xpath = "/sample/trayname_generic/Extraction-SOP-no";
      String[] values = new String[] { "002-2003 ", "002-2003-a " };

      checkValues(fsaSample, xpath, values);

      xpath = "/sample/trayname_generic/phase";
      values = new String[] { "polar ", "polar" };

      checkValues(fsaSample, xpath, values);
    }
  }

  /**
   * @param fsaSample
   * @param fieldValue
   * @param values
   *
   * @throws XPathException
   * @throws DOMException
   */
  private static void checkValues(XMLFile fsaSample, String xpath, String[] values)
    throws DOMException, XPathException {
    boolean ok = false;
    String fieldValue = XPath.path(fsaSample, xpath).item(0).getFirstChild().getNodeValue();

    for (int j = 0; j < values.length; j++) {
      //Logger.debug(null, "comparing   #" + sop + "#  and   #"  + values[j] + "#  " + (sop.compareTo(values[j])));
      if ((fieldValue.compareTo(values[j]) == 0)) {
        ok = true;
      }
    }

    if (!ok) {
      Logger.err(null, fsaSample.getSourceFile().getAbsolutePath() + "  " + fieldValue + " " + xpath);
      fsaSample.getSourceFile().renameTo(new java.io.File(fsaSample.getSourceFile().getAbsoluteFile() + "err"));
    }
  }

  /**
   * checks if one of the directories in the array contains files from the ok directory - if so - they will be deleted in the directories (not in the okFiles-directory)
   *
   * @param fsa_env
   * @param directoriesToClean
   */
  private static void cleanUp(java.io.File okDirectory, java.io.File[] directoriesToClean) {
    // all filenames
    java.io.File[] files = okDirectory.listFiles();
    HashSet hashSet = new HashSet();

    for (int i = 0; i < files.length; i++) {
      hashSet.add(files[i].getName());
    }

    // for each directory get the filenames
    for (int i = 0; i < directoriesToClean.length; i++) {
      Logger.debug(thiz, "checking " + directoriesToClean[i].getAbsolutePath() + " agains " + okDirectory.getAbsolutePath());

      java.io.File[] files2 = directoriesToClean[i].listFiles();

      for (int j = 0; j < files2.length; j++) {
        if (files2[j].isDirectory()) {
          cleanUp(okDirectory, new java.io.File[] { files2[j] });
        }

        // check if this filename exists in the okFiles direcotry 
        if (hashSet.contains(files2[j].getName())) {
          Logger.debug(thiz, "GOT ONE");

          // if yes delete it
          files2[j].delete();
        }
      }
    }
  }

  /**
   * prints a single line for a directory containing information like name, num of files found, and compares the numnber of  found files with the number of files expected in this directory
   *
   * @param dir the directory to check
   * @param needed the number of files expected
   *
   * @return number of files in the directory  TODO check the number of files - i expect that always one is missing compared to the real value
   */
  private static int show(java.io.File dir, int needed) {
    double ratio = (100 / (double) needed) * ((double) (dir.listFiles().length));

    Logger.log(thiz, " \t\t" + dir.getName() + " \t    found: " + dir.listFiles().length + "     needed:  " + needed + "   \t[" + ratio + "%]");

    return dir.listFiles().length;
  }
}
