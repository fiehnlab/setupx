/**
 * ============================================================================ File:    LecoACQFile.java Package: org.setupx.repository.core.communication.leco cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.leco;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.communication.exporting.AcquisitionFile;
import org.setupx.repository.core.communication.exporting.AcquisitionParameter;
import org.setupx.repository.core.communication.exporting.Machine;
import org.setupx.repository.core.communication.ldap.AcquisitionFileException;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.core.util.parse.ParsingException;


/**
 * File created by the Acquisiton Part of SetupX or the machine itself.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class LecoACQFile extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  Machine instrument = new GCTOF();
  public File sourceFile;
  private String content;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // position where the ID is in LECO files
  public static final int POSITION_ID_1 = 7;
  public static final int POSITION_ID_2 = 8;
  private static Object thiz = new LecoACQFile();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * open and parse a file. extension defined in AcquistionFile
   *
   * @param file the sourecfile
   *
   * @throws AcquisitionFileException
   *
   * @see AcquisitionFile#EXTENSION
   */
  public LecoACQFile(File file) throws AcquisitionFileException {
    
    if (org.setupx.repository.core.util.File.getExtension(file).compareTo(AcquisitionFile.EXTENSION) != 0) {
      throw new AcquisitionFileException("the file is not an ACQ file: the extension should be: " + AcquisitionFile.EXTENSION + " but it is: " + org.setupx.repository.core.util.File.getExtension(file));
    }

    this.sourceFile = file;

    // open file
    try {
      this.content = org.setupx.repository.core.util.File.getFileContent(this.sourceFile);
    } catch (IOException e) {
      throw new AcquisitionFileException("unable to open file", e);
    }
  }

  private LecoACQFile() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param acqname TODO
   *
   * @return TODO
   *
   * @throws ColumnInformationNotAvailableException TODO
   */
  public static ColumnInformation getColumnBySampleACQname(String acqname)
    throws ColumnInformationNotAvailableException {
    // cut the extension at the end of the filename of  acqname = acqname.substring(0, acqname.indexOf(":"));
    // format the acqname
    char a = '_';

    if (acqname.lastIndexOf(a) > 0) {
      acqname = acqname.substring(0, acqname.indexOf(a));
    }

    a = ':';

    if (acqname.lastIndexOf(a) > 0) {
      acqname = acqname.substring(0, acqname.indexOf(a));
    }

    String method = "";

    try {
      method = LecoACQFile.determine(acqname, AcquisitionParameter.AS_METHOD);
    } catch (ParameterException e) {
      throw new ColumnInformationNotAvailableException("Columninformation for sample \"" + acqname + "\" can not be found:", e);
    } catch (AcquisitionFileQueryException e) {
      throw new ColumnInformationNotAvailableException("Columninformation for sample \"" + acqname + "\" can not be found:", e);
    }

    return ColumnDetector.getColumnInformationByMethod(method);
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public static File[] getSeqFiles() {
    // find the file with the acqname
    File directory = new File(Config.DIRECTORY_ACQFILES);

    File[] files = directory.listFiles(new FileFilter() {
          public boolean accept(File pathname) {
            return (!pathname.isDirectory() && (org.setupx.repository.core.util.File.getExtension(pathname).compareTo("seq") == 0));
          }
        });

    return files;
  }

  /**
   * fill a hashset containing all values available for a specific acquistionname. <br>
   * The hashset is filled with values taken out of the leco acq containing informations about the run: <br>
   * the types of information in the files is defined as a acquistion parameter <br>
   * <b>key</B>: the name of the acquistionparameter <br>
   * <b>value</B>: the value of that specific acquistion parameter
   *
   * @param acqname Acquistionname for a single sample
   *
   * @return a hashset containing all information available for that acqsample
   *
   * @throws AcquisitionFileQueryException unabel to find informations
   *
   * @see AcquisitionParameter#getPostitionForParameter(String)
   * @see AcquisitionParameter#AS_METHOD
   */
  public static Hashtable getValuesPerACQname(String acqname)
    throws AcquisitionFileQueryException {
    Hashtable result = new Hashtable();

    // load the whole file
    LecoACQFile lecoACQFile = determineFile(acqname);
    Enumeration enumeration = AcquisitionParameter.positionsInLine.keys();

    while (enumeration.hasMoreElements()) {
      String key = (String) enumeration.nextElement();
      int position;

      try {
        position = AcquisitionParameter.getPostitionForParameter(key);

        String value = lecoACQFile.getValue(acqname, position);

        // take each .. and determine each value 
        result.put(key, value);
      } catch (Exception e) {
        warning(thiz, "unable to get value for " + key + ": " + e.toString());
      }
    }

    return result;
  }

  /**
   * TODO: 
   *
   * @param acq_name TODO
   * @param type TODO
   *
   * @return TODO
   *
   * @throws ParameterException TODO
   * @throws AcquisitionFileQueryException TODO
   */
  public static String determine(final String acq_name, final String type)
    throws ParameterException, AcquisitionFileQueryException {
    // check if type is realy a valid type.
    Enumeration enumeration = AcquisitionParameter.positionsInLine.keys();
    int pos = 0;

    while (enumeration.hasMoreElements()) {
      String key = (String) enumeration.nextElement();

      if (key.compareTo(type) == 0) {
        pos = Integer.parseInt("" + AcquisitionParameter.positionsInLine.get(key));
      }
    }

    if (pos == 0) {
      throw new ParameterException("the type " + type + " is unknown or no position is configured for it.");
    }

    // determine the position
    // find the file with the acqname
    File directory = new File(Config.DIRECTORY_ACQFILES); //new File(File.separator + "mnt" + File.separator + "gctof" + File.separator + "setupx"); 

    File[] files = directory.listFiles();

    for (int i = 0; i < files.length; i++) {
        File file = files[i];

      try {
        return new LecoACQFile(file).getValue(acq_name, pos);
      } catch (Exception e) {
          //e.printStackTrace();
      }
    }

    throw new AcquisitionFileQueryException("unable to find parameter " + type + " in files in " + Config.DIRECTORY_ACQFILES);
  }

  /**
   * find the file containing the acqname
   *
   * @return LecoACQFile
   *
   * @throws AcquisitionFileQueryException
   */
  public static LecoACQFile determineFile(final String acqname)
    throws AcquisitionFileQueryException {
    // find the file with the acqname
    File directory = new File(Config.DIRECTORY_ACQFILES); //new File(File.separator + "mnt" + File.separator + "gctof" + File.separator + "setupx"); 

    File[] files = directory.listFiles();

    for (int i = 0; i < files.length; i++) {
      File file = files[i];

      try {
        LecoACQFile lecoACQFile = new LecoACQFile(file);
        lecoACQFile.getValue(acqname, AcquisitionParameter.getPostitionForParameter(AcquisitionParameter.MS_METHOD));

        return lecoACQFile;
      } catch (Exception e) {
      }
    }

    throw new AcquisitionFileQueryException("unable to find file for acqname " + acqname + " in files in " + Config.DIRECTORY_ACQFILES);
  }

  /**
   * scan a while directory for acqfiles containing an acqname
   *
   * @param acq_name the acqname that the id is looked for
   *
   * @return the id
   *
   * @throws AcquisitionFileException unable to find id
   */
  public static int determineID(String acq_name) throws AcquisitionFileException {
    File directory = new File(Config.DIRECTORY_ACQFILES); //new File(File.separator + "mnt" + File.separator + "gctof" + File.separator + "setupx"); 

    File[] files = directory.listFiles();

    for (int i = 0; i < files.length; i++) {
      File file = files[i];

      try {
        int returnValue = new LecoACQFile(file).getSampleID(acq_name);
        log(thiz, "sampleid: " + returnValue + " for: " + acq_name);

        return returnValue;
      } catch (AcquisitionFileException e) {
        // the file is not an ACQ file - can ignore
        // debug(thiz, file.getName() + " is not an ACQ file. Ignoring it: " + e.getMessage());
      } catch (IOException e) {
        err(thiz, e);
      } catch (AcquisitionFileQueryException e) {
        //err(thiz, e);
      }
    }

    throw new AcquisitionFileException("unable to find id for " + acq_name);
  }

  /**
   * TODO: 
   *
   * @param args TODO
   *
   * @throws AcquisitionFileException TODO
   */
  public static void main(String[] args) throws AcquisitionFileException {
    Logger.log(null, "-->" + LecoACQFile.determineID("060308bylsa160"));
  }

  /**
   * fill a hashset containing all possible values for a specific parameter in the seq files.  <br> The hashset is filled with values taken from <b>all</b> seq files available. <br>
   *
   * @param acqname Acquistionname for a single sample
   *
   * @return a hashset containing all information available for that acqsample
   *
   * @throws AcquisitionFileQueryException unabel to find informations
   *
   * @see AcquisitionParameter#getPostitionForParameter(String)
   * @see AcquisitionParameter#AS_METHOD
   */
  public HashSet getValuesForParameterType(String parameterName)
    throws AcquisitionFileQueryException {
    try {
      int position = AcquisitionParameter.getPostitionForParameter(parameterName);
      HashSet values = this.getValuesPerPosition(position);

      values = filter(values, new ValueFilter() {
            public boolean accept(String value) {
              return (value.lastIndexOf('\\') > -1) && (value.length() > 15);
            }
          });

      //debug("found " + values.size() + " values in " + this.sourceFile.getName());
      return values;
    } catch (Exception e) {
      warning(thiz, "unable to get values for " + parameterName + ": " + e.toString());

      return new HashSet(0);
    }
  }

  /**
   * TODO: 
   *
   * @param acqname TODO
   *
   * @return TODO
   *
   * @throws AcquisitionFileQueryException TODO
   */
  public Method findDP(final String acqname) throws AcquisitionFileQueryException {
    return getMethod(acqname, "(DP)");
  }

  /**
   * TODO: 
   *
   * @param acqname TODO
   *
   * @return TODO
   *
   * @throws AcquisitionFileQueryException TODO
   */
  public Method findGC(final String acqname) throws AcquisitionFileQueryException {
    return getMethod(acqname, "(GC)");
  }

  /**
   * TODO: 
   *
   * @param acqname TODO
   *
   * @return TODO
   *
   * @throws AcquisitionFileQueryException TODO
   */
  public Method findMS(final String acqname) throws AcquisitionFileQueryException {
    return getMethod(acqname, "(MS)");
  }

  /**
   * TODO: 
   *
   * @param acqname TODO
   *
   * @return TODO
   *
   * @throws AcquisitionFileQueryException TODO
   */
  public Method findQC(final String acqname) throws AcquisitionFileQueryException {
    return getMethod(acqname, "(QC)");
  }

  /**
   * cut the sampleid out of this file
   *
   * @param acq_name the name of the acqname
   *
   * @return the id
   *
   * @throws IOException TODO
   * @throws AcquisitionFileException TODO
   *
   * @see LecoACQFile#POSITION_ID
   * @deprecated - use the flexible method getValue instead
   */
  private int getSampleID(String acq_name) throws IOException, AcquisitionFileQueryException {
    // take line containing acq_name
    StringTokenizer toker = new StringTokenizer(this.content, "\n");

    //debug("scanning " + this.sourceFile.getName() + " for " + acq_name + ". Number of lines: " + toker.countTokens());
    while (toker.hasMoreElements()) {
      String line = toker.nextToken();
      int positionOfName = line.indexOf(acq_name);

      if (positionOfName > -1) {
        // found it
        debug("(B)found " + acq_name + " in " + this.sourceFile.getName());

        int a1 = 0;

        // sample is always bigger 
        try {
          a1 = parse(get(line, POSITION_ID_1));
        } catch (ParsingException e) {
        }

        if (a1 < 100) {
          warning(this, "sampleID " + a1 + " is illegal. Going to check for an other one.");

          try {
            return parse(get(line, POSITION_ID_2));
          } catch (ParsingException e) {
            for (int pos = POSITION_ID_2; pos >= 0; pos--) {
              try {
                warning(this, "Problems with file: " + this.sourceFile + "     id of " + acq_name + " is not found correctly. - trying taken token at pos: " + pos);
                a1 = parse(get(line, pos));

                if (a1 > 100) {
                  return a1;
                }
              } catch (ParsingException exception) {
              }
            }

            try {
              // meaning that there IS no correct value - so send back the label
              return parse(get(line, POSITION_ID_2));
            } catch (ParsingException e1) {
              e1.printStackTrace();
            }
          }
        } else {
          return a1;
        }
      }
    }

    throw new AcquisitionFileQueryException("unable to find " + acq_name + " in " + this.sourceFile.getName());
  }

  /**
   * get an item out of the line  at a specific position
   *
   * @param line the line
   * @param pos positon of the token
   *
   * @return the token
   *
   * @throws ParsingException numnber of tokens in this line is less than the requested field
   */
  private static String get(String line, int pos) throws ParsingException {
    StringTokenizer tokenizer = new StringTokenizer(line, "\t");

    if (tokenizer.countTokens() < pos) {
      throw new ParsingException("numnber of tokens in this line is less than the requested field " + pos);
    }

    int i = 0;

    while (tokenizer.hasMoreElements()) {
      String token = tokenizer.nextToken();
      i++;

      if (i == pos) {
        //debug(thiz, "pos:" + pos + " " + token);
        return token;
      }
    }

    throw new ParsingException("unable to find element at " + pos + " in line: " + line);
  }

  /**
   * parses a value into an int.
   *
   * @param value string to be parsed
   *
   * @return value as int
   *
   * @throws ParsingException TODO
   */
  private static int parse(String value) throws ParsingException {
    try {
      return Integer.parseInt(value);
    } catch (NumberFormatException e) {
      warning(thiz, "unable to parse value " + value);
      throw new ParsingException(e);
    }
  }

  /**
   * @param methodKeyinfo something like <code>(GC)</code> that can identify the method
   *
   * @return the method
   *
   * @throws AcquisitionFileQueryException
   * @throws AcquisitionFileQueryException
   * @throws IOException
   */
  private Method getMethod(String acqname, final String methodKeyinfo)
    throws AcquisitionFileQueryException {
    for (int i = 1; i < 15; i++) {
      String value = "";

      try {
        value = this.getValue(acqname, i);
      } catch (AcquisitionFileQueryException e) {
        e.printStackTrace();
      } catch (Exception e) {
        Logger.warning(thiz, e.toString());
      }

      if (value.indexOf(methodKeyinfo) > 0) {
        Logger.debug(this, "found value for " + acqname + ": " + value);
        value = value.substring(value.lastIndexOf('\\') + 1);

        Logger.debug(this, "transformed value: " + value);

        Method method = Method.findByMethodLabel(value);
        Logger.debug(this, "found method for " + acqname + ": " + method.getLabel());

        return method;
      }
    }

    throw new AcquisitionFileQueryException("unable to find line for \"" + acqname + "\"");
  }

  /**
   * parses the file for a specific value.
   *
   * @param acq_name
   * @param pos
   *
   * @return
   *
   * @throws IOException
   * @throws AcquisitionFileQueryException
   *
   * @see org.setupx.repository.core.communication.exporting.AcquisitionParameter#AS_METHOD
   */
  private String getValue(String acq_name, int pos) throws IOException, AcquisitionFileQueryException {
    // take file line containing acq_name
    StringTokenizer toker = new StringTokenizer(this.content, "\n");



    while (toker.hasMoreElements()) {
      String line = toker.nextToken();
      int positionOfName = line.indexOf(acq_name);
      if (positionOfName > -1) {
          
        // found it
        debug("(A)found " + acq_name + " in " + this.sourceFile.getName());
        // sample is always bigger 
        try {
          return get(line, pos);
        } catch (ParsingException e) {
          throw new AcquisitionFileQueryException("unable to get Value at pos " + pos + ".", e);
        }
      }
    }

    throw new AcquisitionFileQueryException("unable to find " + acq_name + " in " + this.sourceFile.getName());
  }

  /**
   * TODO: 
   *
   * @param position TODO
   *
   * @return TODO
   *
   * @throws AcquisitionFileQueryException TODO
   */
  private HashSet getValuesPerPosition(final int position)
    throws AcquisitionFileQueryException {
    HashSet result = new HashSet();
    StringTokenizer toker = new StringTokenizer(this.content, "\n");

    while (toker.hasMoreElements()) {
      String line = toker.nextToken();

      try {
        result.add(get(line, position));
      } catch (ParsingException e) {
        warning(thiz, new AcquisitionFileQueryException("unable to get Value at pos " + position + ".").toString() + " " + this.sourceFile.getName());
      }
    }

    return result;
  }

  /**
   * TODO: 
   *
   * @param values TODO
   * @param filter TODO
   *
   * @return TODO
   */
  private HashSet filter(HashSet values, ValueFilter filter) {
    HashSet result = new HashSet();
    Iterator iterator = values.iterator();

    while (iterator.hasNext()) {
      String element = (String) iterator.next();

      if (filter.accept(element)) {
        result.add(element);
      } else {
        //debug("failed: " + element);
      }
    }

    return result;
  }
}
