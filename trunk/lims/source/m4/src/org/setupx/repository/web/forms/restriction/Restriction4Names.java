/**
 * ============================================================================ File:    Restriction4Names.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

/**
 * Restriction for Names and Strings without Numbers but including space and deactivated VocCheck and at least three charackters
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 *
 * @hibernate.subclass
 */
public class Restriction4Names extends StringRestriction {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // TODO add restriction for "yourFirstName yourLastname"
  public static final String REGEX = "[a-zA-Z\\s]{3,}+";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Restriction4Names object.
   */
  public Restriction4Names() {
    super(REGEX, false);
  }
}
