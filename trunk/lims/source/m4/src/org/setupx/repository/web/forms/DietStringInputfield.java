/**
 * ============================================================================ File:    DietStringInputfield.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.multi.MultiField4ClazzSimple;
import org.setupx.repository.web.forms.inputfield.multi.UnsupportedClazzMappingException;


/**
 * @hibernate.subclass
 */
public class DietStringInputfield extends MultiField4ClazzSimple {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new DietStringInputfield object.
   */
  public DietStringInputfield() {
    super();
  }

  /**
   * Creates a new DietStringInputfield object.
   *
   * @param string 
   */
  public DietStringInputfield(String string) {
    super(string, "Doses");

    FormObject stringField = new StringInputfield("dose", "please define the dietvariation");
    this.addField(stringField);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getPath()
   */
  public String getPath() throws UnsupportedClazzMappingException {
    throw new UnsupportedClazzMappingException();
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getSingleValueField()
   */
  public FormObject getSingleValueField() throws UnsupportedClazzMappingException {
    throw new UnsupportedClazzMappingException();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#cloneField()
   */
  public InputField cloneField() throws CloneException {
    return new DietStringInputfield(this.getQuestion());
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.clazzes.ClazzInformation#createShortLabel()
   */
  public String createShortLabel() {
    return this.toStringHTML();
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String toStringHTML() {
    return this.getQuestion() + " " + ((InputField) this.getField(0)).getValue();
  }
}
