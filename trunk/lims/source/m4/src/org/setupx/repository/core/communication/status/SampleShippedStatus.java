package org.setupx.repository.core.communication.status;

import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.server.persistence.PersistenceActionFindException;



/**
 * @hibernate.subclass
 */

public class SampleShippedStatus extends PersistentStatus {
    public SampleShippedStatus(){
        super();
    }
    
    public SampleShippedStatus(long sampleID, UserDO userDO, String message) throws StatusPersistenceException{
        super(sampleID, userDO, message);
    }

    public String createDisplayString() {
        return "sample was shipped.";
    }

    public PersistentStatus createClone() {
        try {
            return new SampleShippedStatus(this.sampleID, this.getUser(getUserID()), this.getMessage());
        } catch (StatusPersistenceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (PersistenceActionFindException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }}
