package org.setupx.repository.core.communication.vocabulary;

import junit.framework.TestCase;

public class LibraryTest extends TestCase {

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
        
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Constructor for LibraryTest.
     * @param arg0
     */
    public LibraryTest(String arg0) {
        super(arg0);
    }

    public void testFinalize() {
    }

    public void testLibrary() {
    }

    public void testGetValue() {
    }

    public void testSetValues() {
    }

    public void testAdd() {
    }

    public void testContains() {
    }

    public void testCreateSuggestion() {
    }

    public void testStore() {
    }

    public void testInit() {
        LocalLibrary library = LocalLibrary.instance();
        assertNotNull(library);
        assertTrue(library.getValues().length > 0);
    }

}
