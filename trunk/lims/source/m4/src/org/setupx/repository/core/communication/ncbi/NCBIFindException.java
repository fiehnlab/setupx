/*
 * Created on 28.03.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.setupx.repository.core.communication.ncbi;


public class NCBIFindException extends NCBIException{

	/**
	 * @param string
	 * @param e
	 */
	public NCBIFindException(String string, Exception e) {
		super(string, e);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param string
	 */
	public NCBIFindException(String string) {
		super(string);

	}

    /**
     * @param e
     */
    public NCBIFindException(Exception e) {
        super(e);
        }

}
