package org.setupx.repository.core.communication.status;

public class NoStatusAvailableException extends Exception {

    public NoStatusAvailableException(String string, Exception e) {
        super(string,e);
    }

}
