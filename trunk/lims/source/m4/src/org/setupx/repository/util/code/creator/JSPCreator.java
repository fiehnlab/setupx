/**
 * ============================================================================ File:    JSPCreator.java Package: org.setupx.repository.util.code.creator cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.util.code.creator;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;

import java.io.File;

import java.lang.reflect.Method;

import java.util.HashSet;
import java.util.Iterator;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class JSPCreator extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private HashSet components;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final String TARGET_DIRECTORY = Config.DIRECTORY_MAIN;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new JSPCreator object.
   *
   * @param class1 
   */
  public JSPCreator(Class class1) {
    // find all getter and settr
    Method[] methods = class1.getMethods();

    // take each method and find the name and type 
    for (int i = 0; i < methods.length; i++) {
      Method method = methods[i];

      String name = method.getName();

      if (name.startsWith("set")) {
        Class returntype = method.getReturnType();
        this.add(new JSPComponent(name, returntype));
      }
    }

    // take all of the methods and create a jsp from them 
    createJSP(new File(TARGET_DIRECTORY));

    // take all the variables and create a servlet from them
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param component TODO
   */
  private void add(JSPComponent component) {
    this.components.add(component);
  }

  /**
   * TODO: 
   *
   * @param file TODO
   */
  private void createJSP(File file) {
    StringBuffer result = new StringBuffer();

    result.append(JSPComponent.createHeader());

    Iterator iterator = this.components.iterator();

    while (iterator.hasNext()) {
      JSPComponent component = (JSPComponent) iterator.next();
      result.append(component.toJSP());
    }

    result.append(JSPComponent.createFooter());
  }
}
