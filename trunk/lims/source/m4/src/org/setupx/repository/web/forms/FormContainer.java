/**
 * ============================================================================ File:    FormContainer.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import java.util.Vector;


/**
 * Object containing several other objects of the same type
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 */
public interface FormContainer {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * return formobject from a defined position in the array
   * <pre>only objects contained inside <B>this</b> will be returned!</pre>
   *
   * @param i the position in the array
   *
   * @return formobject from a defined position in the array
   */
  public FormObject getField(int i);

  /**
   * formobject with the <B>key</B> from the array
   * 
   * <p>
   * it can happen that inside an field there are other field - these objects  containing other do all implement FormContainer.
   * </p>
   *
   * @param key key of the object
   *
   * @return the object - null if it is <b>NOT</b> found
   *
   * @see FormContainer
   * @see ExpandingField
   */
  public FormObject getField(String key);

  /**
   * find all Objects inside this object of the defined type
   *
   * @param class1
   * @param vec
   */
  public void getField(Class class1, Vector vec);

  /**
   * return the position of that specific page in the fieldarray
   *
   * @param firstName
   *
   * @return <p><p>
   */
  public int getFieldPos(String key);

  /**
   * sets all FormObjects (childobjects) related to this object
   *
   * @param formObjects all FormObjects (childobjects) related to this object
   */
  public void setFields(FormObject[] formObjects);

  /**
   * return all FormObjects (childobjects) related to this object
   *
   * @return all FormObjects (childobjects) related to this object
   */
  public FormObject[] getFields();

  /**
   * get Name of this item
   *
   * @return the name
   */
  public String getName();

  /**
   * adds an formobject into the array of inputfields. and creates a reversed reference as <code>parent</code> int the formobject.
   *
   * @param newInputfield the new formobject - the formObject will get a new parent refernece
   */
  public void addField(FormObject formObject);

  /**
   * removes an object from the container.
   * 
   * <p>
   * The object can only be removed if there will be at least <b>one</b> object remaining in the container after removing the formobject
   * </p>
   *
   * @param formObject the object to remove
   *
   * @return true if deleted
   *
   * @throws ArrayShrinkException unable to remove object <p><p>
   */
  public boolean remove(FormObject formObject) throws ArrayShrinkException;

  /**
   * @param key key of the field that will be deleted
   *
   * @return true if deleted
   *
   * @throws ArrayShrinkException
   *
   * @see FormContainer#remove(FormObject)
   */
  public boolean remove(String key) throws ArrayShrinkException;

  /**
   * @return number of objects contained in this
   */
  public int size();
}
