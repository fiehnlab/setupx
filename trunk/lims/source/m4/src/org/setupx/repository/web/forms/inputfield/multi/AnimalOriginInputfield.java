/**
 * ============================================================================ File:    AnimalOriginInputfield.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.inputfield.BigStringInputField;
import org.setupx.repository.web.forms.inputfield.DropDown;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.relation.Relation;
import org.setupx.repository.web.forms.restriction.Restriction4StringLength;


/**
 * @hibernate.subclass
 */
public class AnimalOriginInputfield extends MultiField4Clazz {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new AnimalOriginInputfield object.
   */
  public AnimalOriginInputfield() {
    //public AnimalOriginInputfield(Object location, Object locationDetail, Object seedingDate, Object transplantindDate, Object harvestDate, Object tempNight, Object tempDay, Object lightFluence, Object dailyLight, Object harvestTime)   {
    super("Animal Origin", "Conditions under which the Animal grew up.");

    DropDown down = new DropDown("Location", "define the location", new String[] { "California National Primate Research Center", DropDown.STRING_OTHER }, true);
    this.addField(down);

    //down.setValue(location);
    BigStringInputField bigStringInputField = new BigStringInputField("detailed Location", "when the location is not given in the List above, please specify it here");
    this.addField(bigStringInputField);
    //bigStringInputField.setValue(locationDetail);
    bigStringInputField.setRestriction(new Restriction4StringLength(20));

    Relation relation = new Relation(new String[] { DropDown.STRING_OTHER }, bigStringInputField);
    down.addRelation(relation);

    InputField field = new BigStringInputField("information", "any information related to the origin of the animal");
    field.setRestriction(null);
    this.addField(field);

    //this.addField(new DateInputfield("seeding date", "Date and Time when the sample was planted."));
    //this.addField(new DateInputfield("transplanting date", "Date and Time when the sample was transplanted."));
    //this.addField(new DateTimeInputfield("harvest date", "Date and Time when the sample was harvested."));

    /*
       humidity % day
       humidity% night
     */

    //this.addField(new TemperatureInputfield("night temp.", "temperature in Celsius at night"));
    //this.addField(new TemperatureInputfield("day temp.", "temperature in Celsius at day"));
    // LIGHT 
    //[�mol/m� s]
    // light - day length

    /*
       StringInputfield inputfield = new StringInputfield("light: daily period", "the hours of light per day ");
       inputfield.setRequiered(false);
       inputfield.setHelptext("also called \"light exposure time\"");
       inputfield.setRestriction(new Restriction4Numbers(inputfield, 0, 24));
       inputfield.setSize_x(10);
       inputfield.setExtension(" h/day");
       //inputfield.setValue(dailyLight);
       this.addField(inputfield);
       String unit = "&#181mol/m&#178&#183s";
       StringInputfield light = new StringInputfield("light: fluence", "light intensity at daytime");
       light.setRestriction(new Restriction4Numbers(light));
       light.setExtension(unit);
       light.setRequiered(false);
       light.setSize_x(4);
       light.setHelptext("Please define the light fluence that was used every day. ");
       //light.setValue(lightFluence);
       this.addField(light);
       // special add method
       BigStringInputField field = DropDown.create(this, "light source", "type of lamp mounted over the sample", "", new String[] { "sun", "UV-light", "cool white light", "HPIT", });
       field.setQuestion("lamp: detailed");
       this.setValue("species name or NCBI code");
     */
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.MultiField4Clazz#cloneField()
   */
  /*public InputField cloneField() throws CloneException {
     // take each value and take the value
     MultiField4Clazz instance = new AnimalOriginInputfield();
     if (instance.getFields().length != this.getFields().length) throw new CloneException("unable to clone " + org.setupx.repository.core.util.Util.getClassName(this) + " cause the number of Fields in the clone is not identical.");
  
             Object o ;
             for (int i = 0; i < this.getFields().length; i++) {
                 o = ((InputField)this.getField(i)).getValue();
                 ((InputField)instance.getField(i)).setValue(o);
             }
  
             return instance;
         }*/
  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getPath()
   */
  public String getPath() throws UnsupportedClazzMappingException {
    throw new UnsupportedClazzMappingException();
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getSingleValueField()
   */
  public FormObject getSingleValueField() throws UnsupportedClazzMappingException {
    throw new UnsupportedClazzMappingException();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.clazzes.ClazzInformation#createShortLabel()
   */
  public String createShortLabel() {
    return this.toStringHTML();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.MultiField4Clazz#toStringHTML()
   */
  public String toStringHTML() {
    return this.getQuestion() + ": " + ((InputField) this.getField(0)).getValue();
  }
}
