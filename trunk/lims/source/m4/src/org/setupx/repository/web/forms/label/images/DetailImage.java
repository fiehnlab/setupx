/**
 * ============================================================================ File:    DetailImage.java Package: org.setupx.repository.web.forms.label.images cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.label.images;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.setupx.repository.web.forms.InActiveException;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.multi.MultiField;


/**
 * @hibernate.subclass
 */
public class DetailImage extends StringInputfield {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new DetailImage object.
   */
  public DetailImage() {
    super("Detail-View", "");
    this.setDescription("press to open detailed view"); // + ((MultiField)this.parent()).getQuestion());
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() {
    return new DetailImage();
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String createField() {
    StringBuffer buffer = new StringBuffer();
    buffer.append(((MultiField) this.getParent()).createDetailButton());

    return buffer.toString();
  }

  public Node createXMLNode(Document doc) throws InActiveException {
      warning(this, "not xml exported.");
        return null;
  }
}
