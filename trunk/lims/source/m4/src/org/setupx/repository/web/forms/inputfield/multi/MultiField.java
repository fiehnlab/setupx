/**
 * ============================================================================ File:    MultiField.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import java.util.StringTokenizer;

import org.setupx.repository.Config;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.forms.ArrayExpandException;
import org.setupx.repository.web.forms.ArrayShrinkException;
import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.FormContainer;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.clazzes.Treatment;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.Removable;


/**
 * Implementation of Formcontainer.
 * 
 * <p>
 * It does not have an own value.
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.23 $
 *
 * @hibernate.subclass
 */
public class MultiField extends InputField implements FormContainer, Removable {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  boolean log = false;
  private boolean removable = false;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new MultiField object.
   *
   * @param question 
   * @param description 
   * @param fields 
   */
  public MultiField(String question, String description, InputField[] fields) {
    super(question, description);
    this.setFields(fields);
  }

  /**
   * @deprecated just for peristencelayer
   */
  public MultiField() {
  }

  /**
   * @param question
   * @param description
   */
  public MultiField(String question, String description) {
    super(question, description);
  }

  protected MultiField(String question, String description, InputField field)
    throws ArrayExpandException {
    super(question, description);
    this.addField(field);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormContainer#addField(org.setupx.repository.web.forms.FormObject)
   */
  public final void addField(FormObject formObject) {
    this.addInternalField(formObject);
  }

  /**
   * creates an HTML-Button activate the object - see detail.jsp
   *
   * @return an HTML-Button
   */
  public final String createActivateButton() {
    String msg = "";
    String icon = "";
    StringBuffer buffer = new StringBuffer();
    buffer.append("form?");
    buffer.append(WebConstants.PARAM_ACTIVATE);
    buffer.append("=");
    buffer.append(this.getName());
    buffer.append("&");
    buffer.append(this.getName());
    buffer.append("=");
    buffer.append("");

    if (isActive()) {
      msg = "Press to remove this " + this.getQuestion();
      icon = iconActiveIMG_ACTIVATE;
    } else {
      msg = "Press to activate this " + this.getQuestion();
      icon = iconActiveIMG_DEACTIVATE;
    }

    return "<a href=\"" + buffer.toString() + "\"	title=\"" + msg + "\">" + icon + "</a>  ";
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() throws CloneException {
    MultiField newInstance = new MultiField(this.getQuestion(), this.getDescription());
    newInstance.active = this.active;

    for (int i = 0; i < this.getFields().length; i++) {
      InputField orignal = (InputField) this.getField(i);

      // create clone
      FormObject formObject = orignal.cloneField();

      // set on old status
      formObject.active = orignal.isActive();

      newInstance.addField(formObject);
    }

    return newInstance;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTML()
   */
  public String createHTML(int col) {
    StringBuffer response = new StringBuffer();

    // cols within this table 
    int cols = 4;

    if (Config.NEW_DESIGN) {
      response.append("<!-- " + this.getName() + " (" + this.getDescription() + ") -->");
      response.append("<tr><td colspan=\"" + (col) + "\" align=\"center\">");
      response.append("<fieldset>");

      String removeButton = this.createRemoveButton() + "&nbsp &nbsp ";

      if (removeButton.length() < 4) {
        removeButton = "";
      }

      response.append("<legend>" + removeButton + this.getQuestion() + "</legend>");
      response.append("<table " + WebConstants.BORDER + " width=\"100%\">\n");

      // description
      response.append("<tr><td  colspan=\"" + cols + "\">");
      response.append("<i>" + this.getDescription() + "</i>");
      
      response.append("<tr><td  colspan=\"" + cols + "\">");
      response.append("" + this.getAdditionalInfo() + "");
      
      
      response.append("<font class=\"small\">" + this.getLongDescription() + "</font>");
      response.append("</td></tr> \n");
    } else {
      response.append("<tr><td colspan=\"" + (col) + "\" align=\"center\">" + "<table " + WebConstants.BORDER + " width=\"100%\">\n");
      // header
      response.append(htmlCreateHeader(cols));
    }

    // anchor
    response.append(this.createAnchor());

    // put every inputfield in there
    response.append(this.createHTMLChilds(cols));

    // add a help button
    response.append(this.createHelpButton());

    // close the table
    response.append("</table>");

    if (Config.NEW_DESIGN) {
      response.append("</fieldset>");
    }

    response.append("</td></tr>");

    return response.toString();
  }

  /**
   * any information that should be added in the UI
   */
  public String getAdditionalInfo() {
      return "";
}

/**
   * return the last item
   *
   * @return last item
   */
  public final FormObject lastItem() {
    return getField(this.size() - 1);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormContainer#remove(org.setupx.repository.web.forms.FormObject)
   */
  public boolean remove(FormObject formObject) throws ArrayShrinkException {
    return this.removeInternalField(formObject.getName());
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormContainer#remove(java.lang.String)
   */
  public final boolean remove(String key) throws ArrayShrinkException {
    FormObject formObject = this.getField(key);

    return this.remove(formObject);
  }

  /**
   * @return number of elements contained in this field
   */
  public final int size() {
    return this.getFields().length;
  }

  /**
   * @return the name of the fields contained in this Multifield
   */
  public String[] getFieldsNames() {
    String[] result = new String[this.size()];

    for (int i = 0; i < this.size(); i++) {
      result[i] = this.getField(i).getName();
    }

    return result;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.Removable#setRemovable(boolean)
   */
  public void setRemovable(boolean i_removable) {
    this.removable = i_removable;
  }

  /*
   * (non-Javadoc)
   *
   * @hibernate.property
   *
   * @see org.setupx.repository.web.forms.Removable#getRemovable()
   */
  public boolean getRemovable() {
    return removable;
  }

  /*
   * (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.InputField#setValue(java.lang.Object)
   */
  public void setValue(Object object) {
    //super.setValue(object);
    String _inText = (String) object;

    //debug("size of autofill string : " + _inText.length());
    if (_inText.length() < 1) {
      return;
    }

    // autofill
    debug("setting values - by setValue");

    StringTokenizer stringTokenizer = new StringTokenizer(_inText, "\n");
    int numberOfLines = stringTokenizer.countTokens();

    if (numberOfLines > this.size()) {
      log(this, "content for autoset is too long ");

      try {
        throw new ArrayExpandException("I cant expand myself.");
      } catch (ArrayExpandException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    // take each line and add set it to the sample
    int i = 0;

    while (stringTokenizer.hasMoreTokens()) {
      String element = stringTokenizer.nextToken();

      // hotfix
      if (this.getField(i) instanceof Sample) {
        ((Sample) this.getField(i)).setComment(element);
      } else {
        ((InputField) this.getField(i)).setValue(element);
      }

      i++;
    }
  }

  public void activateTreatmentsChilds(boolean active) {
    FormObject[] formObjects = this.getFields();

    for (int i = 0; i < formObjects.length; i++) {
      FormObject object = formObjects[i];
      object.setActive(active);

      //log(Util.getClassName(object) + "    \tMultiField: " + (object instanceof MultiField));
      if (object instanceof MultiField || object instanceof Treatment) {
        ((MultiField) object).activateTreatmentsChilds(active);
      }
    }
  }

  /**
   * creates an HTML-Button to a detail page - which then links the user to a detail page - see detail.jsp
   *
   * @return an HTML-Button to a detail page
   */
  public String createDetailButton() {
    String msg = "Press to get a detailed view of this item.";
    StringBuffer buffer = new StringBuffer();
    buffer.append("form?");
    buffer.append(this.getName());
    buffer.append("=");
    buffer.append("");
    buffer.append("&");
    buffer.append(WebConstants.PARAM_DETAIL);
    buffer.append("=");
    buffer.append(this.getName());

    return "<a href=\"" + buffer.toString() + "\"	target=\"second\"  title=\"" + msg + "\">" + iconDetailIMG + "</a>  ";
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#toStringHTML()
   */
  public String toStringHTML() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("<b>" + this.getQuestion() + "</b> " + this.getDescription());

    // method is single line now
    for (int i = 0; i < this.size(); i++) {
      if (this.getField(i).isActive()) {
        buffer.append("<br>" + ((InputField) this.getField(i)).toStringHTML());
      }
    }

    return buffer.toString();
  }

  /**
   * @return the header-line for a table
   */
  protected String htmlCreateHeader(int col) {
    return ("<th colspan=\"" + col + "\">" + this.getQuestion() + "\n" + "<br><font class=\"small\">" + this.getDescription() + "</font>" + "<br><font class=\"small\">" + this.getLongDescription() + "</font></td></th>");
  }
  
  
  public void setActive(final boolean activation){
      super.setActive(activation);
      
      // what am I doing if there is just a single element inside me? 
      // I do activate my self and that single childobject.
      
      // FormObject[] formObjects = this.getFields();
      
      
      
      /*
       * this is not ok - because it does not deactivate multifileds when they still contain an active object.
       */
      this.activateChilds(activation);
      
      /*
      for (int i = 0; i < formObjects.length; i++) {
          subactive = (subactive || formObjects[i].active);
          activeFormObject = formObjects[i];
      }
      
      // only for setActive(true)
      if (this.getFields().length > 0 && subactive ){
          debug("there are elements inside me. I am going to activate me.");

          warning(this, "activating first child!!");
          // ok this is the genomeinputfield in some cases and it does not change - major source of problem.
          // this.getField(0).active = true;
          debug(this, "activating myself to " + activation);
          this.active = activation;
          debug(this, "activating my child " + activeFormObject + " to " + activation);
          activeFormObject.active = activation;
          debug(this, "setting Active " + activeFormObject + " to " + activation);
          activeFormObject.setActive(activation);
      }
      */
  }

private void activateChilds(boolean active) {
    for (int i = 0; i < this.getFields().length; i++) {
        
        FormObject fo = this.getFields()[i];
        
        // special case - classes are not toughed by this
        if (fo instanceof Clazz) warning(this, "changes to Clazzes can not be made by the wrapping Multifield(" + Util.getClassName(this) + ")");
        else fo.setActive(active);
    }
}

public String getXMLChildname() {
    return "collection";
}

}
