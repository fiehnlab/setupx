/**
 * ============================================================================ File:    SampleTemplateMerger.java Package: org.setupx.repository.web.forms.clazzes cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.clazzes;

import org.setupx.repository.core.communication.experimentgeneration.template.SampleTemplate;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;
import org.setupx.repository.web.forms.inputfield.multi.Sample;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class SampleTemplateMerger {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Clazz clazz;
  private SampleTemplate[] sampleTemplates;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new SampleTemplateMerger object.
   *
   * @param clazz 
   * @param sampleTemplates 
   */
  public SampleTemplateMerger(final Clazz clazz, final SampleTemplate[] sampleTemplates) {
    this.clazz = clazz;
    this.sampleTemplates = sampleTemplates;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * taking each of the templates and map it as a real sample to the clazz. <br><b>Existing Classes will be removed !</b>
   */
  public final void merge() {
    //removing all existing samples !!!!        
    clazz.setFields(new FormObject[0]);

    SampleTemplate sampleTemplate = null;
    Sample sample = null;

    // adjust number of samples in the class to the number of templates
    // for each template
    for (int i = 0; i < sampleTemplates.length; i++) {
      sampleTemplate = sampleTemplates[i];

      // create a real sample out of the template
      sample = Sample.map(sampleTemplate);

      // add the sample to the real class
      this.clazz.addField(sample);
    }
  }
}
