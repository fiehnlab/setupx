/**
 * ============================================================================ File:    DateTimeInputfield.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.web.forms.inputfield.DateInputfield;
import org.setupx.repository.web.forms.inputfield.TimeInputfield;

import java.util.Date;


/**
 * Inputfield for the definition of the Date and Time
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 *
 * @hibernate.subclass
 */
public class DateTimeInputfield extends MultiField {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private DateInputfield date;
  private TimeInputfield time;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @deprecated just for peristencelayer
   */
  public DateTimeInputfield() {
    this("...", "...");
  }

  /**
   * Creates a new DateTimeInputfield object.
   *
   * @param question 
   * @param descri 
   */
  public DateTimeInputfield(String question, String descri) {
    super(question, descri);

    this.date = new DateInputfield();
    this.time = new TimeInputfield();

    this.addField(this.date);
    this.addField(this.time);
  }

  /**
   * @param string
   * @param string2
   * @param harvestDate
   * @param harvestTime
   */
  public DateTimeInputfield(String string, String string2, Object date, Object time) {
    super(string, string2);

    this.date = new DateInputfield(date);
    this.time = new TimeInputfield(time);

    this.addField(this.date);
    this.addField(this.time);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param col TODO
   *
   * @return TODO
   */
  public String createHTML(int col) {
    StringBuffer response = new StringBuffer();

    response.append("<!-- " + this.getName() + " (" + this.getDescription() + ") -->");

    response.append("<tr><td colspan=\"" + col + "\">" + this.getQuestion() + "<br><span class=small>" + this.getDescription() + "</span></td></tr>");
    response.append("<tr><td></td><td colspan=\"" + col + "\"><table>" + this.createHTMLChilds(4) + "</table></td></tr>");

    // anchor
    response.append(this.createAnchor());

    // add a help button
    response.append(this.createHelpButton());

    return response.toString();
  }
}
