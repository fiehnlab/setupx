/**
 * ============================================================================ File:    Mail.java Package: org.setupx.repository.core.util cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util;


import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class Mail extends CoreObject {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Mail() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param e
   *
   * @throws MessagingException
   */
  public static void postError(Exception e) {
    StringWriter stringWriter = new StringWriter();
    PrintWriter printWriter = new PrintWriter(stringWriter);

    e.printStackTrace(printWriter);

    try {
      postMail(new String[] { Config.OPERATOR_ADMIN.getEmailAddress() }, "[error] " + Util.getClassName(e), stringWriter.getBuffer().toString(), "scholz@zeypher.com");
    } catch (MessagingException e1) {
      e1.printStackTrace();
    }
  }

  /**
   * @param string
   */
  public static void postError(Object object, String string) {
    try {
      postMail(new String[] { Config.OPERATOR_ADMIN.getEmailAddress() }, "[error] " + Util.getClassName(object), string, "scholz@zeypher.com");
    } catch (MessagingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /**
   * TODO: 
   *
   * @param recipients TODO
   * @param subject TODO
   * @param message TODO
   * @param from TODO
   *
   * @throws MessagingException TODO
   */
  public static void postMail(String[] recipients, String subject, String message, String from)
    throws MessagingException {
    if (!Config.MAIL_ACTIVE) {
      return;
    }

    for (int i = 0; i < recipients.length; i++) {
      String string = recipients[i];
      debug(null, "sending mail " + message + " to: " + string);
    }

    boolean debug = false;

    //Set the host smtp address
    Properties props = new Properties();
    props.put("mail.smtp.host", Config.MAIL_SERVER);

    // create some properties and get the default Session
    Session session = Session.getDefaultInstance(props, null);
    session.setDebug(debug);

    // create a message
    Message msg = new MimeMessage(session);

    // set the from and to address
    InternetAddress addressFrom = new InternetAddress(from);
    msg.setFrom(addressFrom);

    InternetAddress[] addressTo = new InternetAddress[recipients.length];

    for (int i = 0; i < recipients.length; i++) {
      addressTo[i] = new InternetAddress(recipients[i]);
    }

    msg.setRecipients(Message.RecipientType.TO, addressTo);

    // Optional : You can also set your custom headers in the Email if you Want
    msg.addHeader("MyHeaderName", "myHeaderValue");

    // Setting the Subject and Content Type
    msg.setSubject(subject);
    msg.setContent(message, "text/plain");

    try {
      Transport.send(msg);
    } catch (NoSuchProviderException e) {
      warning(null, new MessagingException("Unable to start MailService on " + Config.MAIL_SERVER + " - System HALTED!").toString());
      // throw new MessagingException("Unable to start MailService on " + Config.MAIL_SERVER + " - System HALTED!");
    }
  }
}
