package org.setupx.repository.core.communication.msi;

import java.util.List;

import org.hibernate.Session;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;

public class MSIProvider {
    
    
    
    /**
     * return a list of all MSIparameters
     * @return list of MSIAttributes
     */
    public static List findAllMSIParameter(){
        List list = null;
        return list;
    }


    
    /**
     * search all Values for a certain msiParameter
     * @param msiParametername
     * @return
     */
    public static List findValues(final String parametername){
        return findValues(MSIAttribute.findMSIAttribute(parametername));
    }

    /**
     * search all Values for a certain msiParameter
     * @param msiParametername
     * @return
     */
    public static List findValues(MSIAttribute attribute){
        return findValues(attribute.getUOID());
    }

    /**
     * search all Values for a certain msiParameter
     * @param msiParametername
     * @return
     */
    public static List findValues(long msiAttributeID){
        return null;
    }

    
    
    /**
     * testing myseld
     * @param args
     */
    public static void main(String[] args) throws Exception{

        Session s = CoreObject.createSession();

        // determine all MSI Attributes
        
        try {
            List list = MSIAttribute.persistence_loadAll(MSIAttribute.class, s);
            
            System.out.println("--> " + list.size());
            
        } catch (PersistenceActionFindException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
        int sampleID = 343763;
        
        /*
        show(MSIQuery.searchAttributesPerClassBySampleID(sampleID));
        
        show(MSIQuery.searchAttributesPerSampleBySampleID(sampleID));
        
        show(MSIQuery.searchAttributesPerExperimentBySampleID(sampleID));
        */
        
        
        
        
        
        MSIQuery.testIt();
        

        /*
        // get a list of all custom attributes
        List allAttribues = MSIProvider.findAllMSIParameter();
        
        Iterator attIterator = allAttribues.iterator();
        while (attIterator.hasNext()) {
            MSIAttribute attribute = (MSIAttribute) attIterator.next();
            
            Logger.log(null, attribute.getLabel() + " " + attribute.getComment() + " " + attribute.getExtension() + " " + attribute.getUserID() + " " + attribute.getDateWhenIntroduced());
            List experimentIDs = attribute.findExperiments();
            Logger.log(null, "number of experiments: " + experimentIDs.size());
        }
        */
    }



    private static void show(AttributeValuePair[] attributeValuePairs) {
        try{
            for (int i = 0; i < attributeValuePairs.length; i++) {
                AttributeValuePair attributeValuePair = attributeValuePairs[i];
                Logger.log(null, attributeValuePair.toString());
                
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
