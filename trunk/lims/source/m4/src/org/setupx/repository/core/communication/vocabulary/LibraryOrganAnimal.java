/**
 * ============================================================================ File:    LibraryOrganAnimal.java Package: org.setupx.repository.core.communication.vocabulary cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.vocabulary;

import org.setupx.repository.core.communication.tissueDB.TissueDBReader;

import java.util.Enumeration;
import java.util.HashSet;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.8 $
 */
public class LibraryOrganAnimal extends LibraryOrgan {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  protected HashSet receiveValues() {
    HashSet hashSet = new HashSet();
    Enumeration enumeration = TissueDBReader.data.keys();

    while (enumeration.hasMoreElements()) {
      hashSet.add(enumeration.nextElement());
    }

    return hashSet;
  }
}
