/**
 * ============================================================================ File:    ExportPart.java Package: org.setupx.repository.core.communication.export cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.export;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.w3c.dom.Document;
import org.w3c.dom.Node;


import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.msi.AttributeValuePair;
import org.setupx.repository.core.communication.msi.MSIAttribute;
import org.setupx.repository.core.communication.msi.MSIQuery;
import org.setupx.repository.core.communication.ncbi.NCBIConnector;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;
import org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.pub.data.PubAttribute;


/**
 * Export part represents a combined set of attributes. These Attributes will be combined in the export to one set.
 * <br>For example in an xls sheet this export part is combined in one worksheet.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class ExportPart extends CoreObject{

    {
        Logger.log(this, "creating new instance.");
    }
    //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private String label;
    private Vector lines = new Vector();

    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * create a set of export part for all the samples
     */
    public ExportPart(HashSet samples) {
        this.label = "Samples";

        Iterator iter = samples.iterator();
        ExportSample exportSample = null;

        while (iter.hasNext()) {
            exportSample = (ExportSample) iter.next();
            this.addLine(exportSample.toLine());
        }

        // position of sample ID
        if (exportSample != null) {
            this.sortLines(exportSample.getIDposition());
        }
    }

    /**
     * Creates a new ExportPart object for a set of classes.
     */
    public ExportPart(Clazz[] clazzs) {
        this.label = "Classes";

        for (int i = 0; i < clazzs.length; i++) {
            Clazz clazz = clazzs[i];

            // ignore when class is empty 
            if (clazz.getSamples().size() != 0){
                Vector clazzLine = new Vector();

                // add general information
                clazzLine.add(new Entry("Class ID", "" + clazz.getUOID()));
                clazzLine.add(new Entry("number of Samples", "" + clazz.getSamples().size() + ""));
                // clazzLine.add(new Entry("number of Factors", "" + clazz.getMultiField4Clazzs().length));
                clazzLine.add(new Entry("factor ID", ""));
                clazzLine.add(new Entry("factor label", ""));
                clazzLine.add(new Entry("factor value", ""));
                clazzLine.add(new Entry("factor short label", ""));

                this.addLine(clazzLine);

                MultiField4Clazz[] field4Clazzs = clazz.getMultiField4Clazzs();

                for (int j = 0; j < field4Clazzs.length; j++) {
                    MultiField4Clazz multiField4Clazz = field4Clazzs[j];

                    Vector mf4ClazzLine = new Vector();
                    mf4ClazzLine.add(new Entry("", ""));
                    mf4ClazzLine.add(new Entry("", ""));
                    // mf4ClazzLine.add(new Entry("",   ""));
                    mf4ClazzLine.add(new Entry("factor ID", "" + multiField4Clazz.getUOID()));
                    mf4ClazzLine.add(new Entry("factor label", "" + multiField4Clazz.getQuestion()));
                    mf4ClazzLine.add(new Entry("factor value", "" + multiField4Clazz.getDescription()));
                    mf4ClazzLine.add(new Entry("factor short label", multiField4Clazz.createShortLabel()));

                    this.addLine(mf4ClazzLine);
                }
            }
        }
    }

    /**
     * Creates a new ExportPart object for a set of classes.
     */
    public ExportPart(Promt promt) {
        this.label = "Experiment";

        // add general information
        Vector vector = new Vector();
        vector.add(new Entry("Experiment ID", "" + promt.getUOID()));
        vector.add(new Entry("number of Samples", "" + promt.getSamples().length + ""));
        //vector.add(new Entry("number of Dimensions", "" + promt.getClazzDimensionDetector().getClazzDimensions().length));
        vector.add(new Entry("number of Classes", "" + promt.getClazzes().length));
        try {
            vector.add(new Entry("title", new SXQuery().findPromtTitleByPromtID(promt.getUOID())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            vector.add(new Entry("abstract", new SXQuery().findPromtAbstractByPromtID(promt.getUOID())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            vector.add(new Entry("comment", new SXQuery().findPromtComment(promt.getUOID())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            vector.add(new Entry("collaboration", new SXQuery().findPromtCollaborationString(promt.getUOID())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {

            Iterator specieses = new SXQuery().findSpeciesbyPromtID(promt.getUOID()).iterator();
            while (specieses.hasNext()) {
                // species fields
                String element = (String) specieses.next();
                Entry entry = new Entry("species", element);
                vector.add(entry);

                // try to match NCBI code
                try {
                    entry = new Entry("NCBI code", "" + NCBIConnector.determineNCBI_Id(element));
                    vector.add(entry);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } // specieses
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.lines.add(vector);
    }

    /**
     * Creates a new ExportPart object.
     *
     * @param string 
     */
    public ExportPart(String string) {
        this.label = string;
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * create an export part based on the classID and the promt ID.
     * currently supported are {@link MSIAttribute}s and {@link PubAttribute}s.
     * @throws PersistenceActionFindException 
     * 
     * 
     */
    public ExportPart(Class class1, long id) throws PersistenceActionFindException {
        debug("creating ExportPart for class: " + class1.getName() + " and promtID " + id);
        int SAMPLE = 0;
        int CLASS = 1;
        int PROMT = 2;
        int NON_MSI = 3;

        // chooose matching export
        if (class1.getName().compareTo(MSIAttribute.class.getName()) == 0){          
            AttributeValuePair[] _attributeValuePairs = null;
            Hashtable[] cache = new Hashtable[]{
                    new  Hashtable(),
                    new  Hashtable(),
                    new  Hashtable(),
                    new  Hashtable()
            };


            debug("creating ExportPart for MSIAttributes");
            // find all MSIattributes for all samples
            this.label = "MSI Attributes";

            long promtID = id;
            long classID = 0;
            long sampleID = 0;

            // find promt MSI attributes

            List sampleIDs = new SXQuery().findSampleIDsByPromtID(promtID);
            Logger.debug(this, "number of sampleIDs :" + sampleIDs.size());

            // finding all possible values for attributes
            String queryAttributeNames = "select att.label as l from pubattribute as att, pubsample as samp, pubdata where pubdata.relatedExperimentID = " + promtID + " and att.parent = samp.uoid and samp.parent = pubdata.uoid group by att.label "; 
            Session hibernateSession =CoreObject.createSession();
            List attributeNames = hibernateSession.createSQLQuery(queryAttributeNames).addScalar("l", Hibernate.STRING).list();

            for (int sampleCount = 0 ; sampleCount < sampleIDs.size(); sampleCount++){
                Vector line = new Vector();

                sampleID = Long.parseLong(sampleIDs.get(sampleCount).toString());
                line.add(new Entry("SampleID", "" + sampleID));
                
                classID = new SXQuery().findClazzIDBySampleID((int)sampleID);
                line.add(new Entry("ClassID", "" + classID));
                
                

                // promt
                String key = "" + new SXQuery().findPromtIDbySample(sampleID); 
                if (cache[PROMT].containsKey(key)){
                    Logger.debug(this, " is cached already.");
                    _attributeValuePairs = (AttributeValuePair[])cache[PROMT].get(key);
                } else {
                    _attributeValuePairs = MSIQuery.searchAttributesPerExperimentBySampleID(sampleID);
                    cache[PROMT].put(key, _attributeValuePairs);
                }

                storeInLine(_attributeValuePairs, line);

                // class
                key = "" + new SXQuery().findClazzIDBySampleID((int)sampleID); 
                if (cache[CLASS].containsKey(key)){
                    Logger.debug(this, " is cached already.");
                    _attributeValuePairs = (AttributeValuePair[])cache[CLASS].get(key);
                } else {
                    _attributeValuePairs = MSIQuery.searchAttributesPerClassBySampleID(sampleID);
                    cache[CLASS].put(key, _attributeValuePairs);
                }

                storeInLine(_attributeValuePairs, line);


                // sample
                _attributeValuePairs = MSIQuery.searchAttributesPerSampleBySampleID(sampleID);
                storeInLine(_attributeValuePairs, line);

                
                this.addLine(line);

            }// end loop over samples



        } else if (class1.getName().compareTo(PubAttribute.class.getName()) == 0){
            // find all pubattribtues for all samples in each export part
            
            debug("creating ExportPart for MSIAttributes");
            // find all MSIattributes for all samples
            this.label = "Annotation Data";

            long promtID = id;
            long classID = 0;
            long sampleID = 0;

            // find promt MSI attributes

            List sampleIDs = new SXQuery().findSampleIDsByPromtID(promtID);
            Logger.debug(this, "number of sampleIDs :" + sampleIDs.size());

            // finding all possible values for attributes
            String queryAttributeNames = "select att.label as l from pubattribute as att, pubsample as samp, pubdata where pubdata.relatedExperimentID = " + promtID + " and att.parent = samp.uoid and samp.parent = pubdata.uoid group by att.label "; 
            Session hibernateSession =CoreObject.createSession();
            List attributeNames = hibernateSession.createSQLQuery(queryAttributeNames).addScalar("l", Hibernate.STRING).list();

            for (int sampleCount = 0 ; sampleCount < sampleIDs.size(); sampleCount++){
                Vector line = new Vector();

                sampleID = Long.parseLong(sampleIDs.get(sampleCount).toString());
                line.add(new Entry("SampleID", "" + sampleID));
                
                classID = new SXQuery().findClazzIDBySampleID((int)sampleID);
                line.add(new Entry("ClassID", "" + classID));

                AttributeValuePair[] _attributeValuePairs = new SXQuery().findPubdataBySampleID(sampleID, attributeNames);
                storeInLine(_attributeValuePairs, line);
                this.addLine(line);
            }
        } else {
            throw new UnsupportedClassVersionError("the class " + class1 + " is not supported.");
        }
    }

    
    /*
    public ExportPart(Class class1, boolean desc, long promtID) {
        if (class1.getName().compareTo(MSIAttribute.class.getName()) == 0){
            // find all MSI Attributes used in this exp and put each of their desc in a line
            
            String queryAttributeNames = "select att.label as l from pubattribute as att, pubsample as samp, pubdata where pubdata.relatedExperimentID = " + promtID + " and att.parent = samp.uoid and samp.parent = pubdata.uoid group by att.label "; 
            Session hibernateSession =CoreObject.createSession();
            List attributeNames = hibernateSession.createSQLQuery(queryAttributeNames).addScalar("l", Hibernate.STRING).list();
            
            

        } else {
            throw new UnsupportedClassVersionError("class " + class1.getName() + " is not supported.");
        }
    }
    */
    
    private void storeInLine(AttributeValuePair[] valuePairs, Vector line) {
        Logger.log(this, "_start: storing: " + valuePairs.length);

        for (int i = 0; i < valuePairs.length; i++) {
            try {
                AttributeValuePair attributeValuePair = valuePairs[i];
                Logger.log(this, attributeValuePair.toString());

                Entry entry = new Entry(attributeValuePair.getLabel1() + " " + attributeValuePair.getLabel2(), attributeValuePair.getValue());
                line.add(entry);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }

        Logger.log(this, "_end: storing: " + valuePairs.length);
    }

    /**
     * @return label of this export part.
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * @return a {@link Vector} filled with more {@link Vector}s, which containcs {@link Entry} objects. 
     */
    public Vector getLines() {
        return this.lines;
    }

    /**
     * @param hashtable
     */
    protected void addLine(Vector line) {
        this.lines.add(line);
    }

    /**
     * carful not tested. sort the lines by comparing two objects.values in the line at the given position
     *
     * @param position
     */
    protected void sortLines(int position) {
        Object[] objects = lines.toArray();
        Arrays.sort(objects, new LineComparator(position));
        this.lines = toVector(objects);
    }

    /**
     * converts to an vector
     */
    private static Vector toVector(Object[] objects) {
        Vector vector = new Vector();

        for (int i = 0; i < objects.length; i++) {
            vector.add(objects[i]);
        }

        return vector;
    }

    /**
     * TODO: 
     *
     * @param key TODO
     * @param value TODO
     */
    private void addLine(String key, String value) {
        Vector s = new Vector();
        s.add(new Entry(key, value));
        this.addLine(s);
    }

    /**
     * converts this to an {@link Node}
     * @param doc
     * @return
     */
    public Node toXML(Document doc) {
        debug("creating XML representaion.");

        Node nodeExportPart = doc.createElement(this.getLabel());

        // create all child elements
        for (int i = 0 ; i < this.getLines().size(); i++){
            Vector vector = (Vector)this.getLines().get(i);
            Node nodeVector = doc.createElement("part");

            // creata a new node for each
            for(int j = 0; j < vector.size(); j++){
                Entry entry = (Entry)vector.get(j);
                nodeVector.appendChild(entry.createXMLNode(doc));
            }
            nodeExportPart.appendChild(nodeVector);
        }
        return nodeExportPart;
    }
}
