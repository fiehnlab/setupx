/**
 * ============================================================================ File:    GCTOF.java Package: org.setupx.repository.core.communication.leco cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.leco;

import org.setupx.repository.core.communication.exporting.Machine;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * @hibernate.subclass
 */
public class GCTOF extends Machine {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** methods defined for this machine. */
  private Set methods;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final long serialVersionUID = 9114162325970713831L;
  private static final String NAME_PATTERN = "(.{6})([abz])([a-z]{2})sa(.{1,4})(:)(.*)";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new GCTOF object.
   */
  public GCTOF() {
    super();
  }

  /**
   * Creates a new GCTOF object.
   *
   * @param label 
   * @param machineLetter 
   * @param comment 
   */
  public GCTOF(String label, char machineLetter, String comment) {
    super(label, machineLetter, comment);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.communication.instrument.Instrument#getFileExtension()
   */
  public String getFileExtension() {
    return "bin";
  }

  /**
   * TODO: 
   *
   * @param methods TODO
   */
  public void setMethods(Set methods) {
    this.methods = methods;
  }

  /**
   * @hibernate.set lazy="true"
   * @hibernate.collection-key column="machine"
   * @hibernate.collection-one-to-many  class="org.setupx.repository.core.communication.leco.Method"
   */
  public Set getMethods() {
    return methods;
  }

  /**
   * TODO: 
   *
   * @param pattern TODO
   *
   * @return TODO
   */
  public Set getMethods(String pattern) {
    // load all samples
    Iterator iterator = this.getMethods().iterator();
    HashSet subset = new HashSet();

    while (iterator.hasNext()) {
      Method method = (Method) iterator.next();

      // check value for pattern
      boolean match = (method.getValue().indexOf(pattern) > 0);

      // match then add it.
      if (match) {
        subset.add(method);
      }
    }

    return subset;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.communication.instrument.Instrument#getSampleNamePattern()
   */
  public String getSampleNamePattern() {
    return NAME_PATTERN;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.communication.instrument.Instrument#getTokenDivider()
   */
  public String getTokenDivider() {
    return "" + '\u0100' + '\u0000' + '\u0000' + '\u0000';
  }
}
