/**
 * ============================================================================ File:    LogFile.java Package: org.setupx.repository.core.communication.importing.logfile cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.importing.logfile;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.exporting.Machine;
import org.setupx.repository.server.logging.Message;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;

import java.io.File;
import java.io.IOException;

import java.util.HashSet;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.sound.midi.Instrument;


/**
 * Containing all samplenames, that were found in a specific file.
 * 
 * <p>
 * create a new instance of this file and then get the samples that were found in it.
 * </p>
 * 
 * <p>
 * <code>new LogFile(new File("log.bin"), new GCTOF());</code>
 * </p>
 * 
 * <p>
 * The Pattern for a samplename is defined in the Instrument itself.
 * </p>
 *
 * @author scholz
 *
 * @see Instrument#getSampleNamePattern()
 */
public final class LogFile extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private File file;
  private HashSet samplesNames = new HashSet();
  private Machine instrument;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final long serialVersionUID = 6683117950631732034L;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new LogFile object.
   *
   * @param file The file that will be scanned for samplenames.
   * @param i_instrument The instrument that created the logfile.
   *
   * @throws IOException 
   * @throws ParserException 
   */
  public LogFile(File file, Machine i_instrument) throws IOException, ParserException {
    this.instrument = i_instrument;
    this.file = file;
    scanEntries();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @return an iterator containing all samplenames found in this file
   */
  public Iterator samples() {
    return this.samplesNames.iterator();
  }

  /**
   * scanning for entries
   *
   * @throws IOException unable to open the file
   */
  private void scanEntries() throws IOException {
    String content = org.setupx.repository.core.util.File.getFileContent(this.file);

    //String startToken =
    StringTokenizer stringTokenizer = new StringTokenizer(content, this.instrument.getTokenDivider());

    while (stringTokenizer.hasMoreElements()) {
      String token = stringTokenizer.nextToken();

      // all non word characters
      token = token.replaceAll("[^a-zA-Z_0-9_:]", "");

      try {
        int size = token.lastIndexOf(":") + 2;
        token = token.substring(0, (size));

        if (instrument.validateSampleName(token)) {
          // found a name
          samplesNames.add(token);
        } else {
          //debug("illegal token: " + token);
          if ((token.indexOf("sa") > 0) && (token.indexOf(":") > 0)) {
            //debug("ignored token: " + token);

            /*try {
               new Message(this, "ignored token: " + token).update(true);
               } catch (PersistenceActionUpdateException e) {
                   e.printStackTrace();
               }
             */
          }
        }
      } catch (StringIndexOutOfBoundsException e) {
        //e.printStackTrace();
      }
    }
  }
}
