/**
 * ============================================================================ File:    ColumnDetector.java Package: org.setupx.repository.core.communication.leco cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.leco;

import java.util.Enumeration;
import java.util.Hashtable;

import org.setupx.repository.core.communication.exporting.AcquisitionParameter;
import org.setupx.repository.core.util.logging.Logger;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class ColumnDetector {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static Hashtable columns = new Hashtable();
  private static Object thiz = new ColumnDetector();

  static {
    columns = new Hashtable();
    columns.put("TEST", ColumnInformation.TESTCOLUMN);
    columns.put(Method.transform("quick-split-1:200"), ColumnInformation.RTX5);
    columns.put(Method.transform("quick-split-1:100"), ColumnInformation.RTX5);
    columns.put(Method.transform("quick-split-1:5"), ColumnInformation.RTX5);
    columns.put("gcxgx (unused)", ColumnInformation.RTX5GCxGC);
    columns.put("mdm35 (unused)", ColumnInformation.MDM35);
    columns.put("unused(testcolumn)", ColumnInformation.TESTCOLUMN);
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private ColumnDetector() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @return TODO
   */
  public static Hashtable getAllColumnConfigurations() {
    return columns;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public static String[][] getAllColumnConfigurationsArray() {
    Enumeration methods = getAllColumnConfigurations().keys();
    int i = 0;
    String[][] result = new String[getAllColumnConfigurations().size()][2];

    while (methods.hasMoreElements()) {
      String method = (String) methods.nextElement();
      ColumnInformation columnInformation;

      try {
        columnInformation = getColumnInformationByMethod(method);
        result[i][0] = columnInformation.getBbid();
        result[i][1] = columnInformation.getLable() + ": " + columnInformation.getDescription();
      } catch (ColumnInformationNotAvailableException e) {
        result[i][0] = "-err-";
        result[i][1] = "-err-";
      }
      i++;
    }

    return result;
  }

  /**
   * TODO: 
   *
   * @param columnID TODO
   *
   * @return TODO
   *
   * @throws ColumnInformationNotAvailableException TODO
   */
  public static ColumnInformation getColumnInformationByID(String columnID)
    throws ColumnInformationNotAvailableException {
    java.util.Enumeration enumeration = columns.elements();

    while (enumeration.hasMoreElements()) {
      ColumnInformation columnInformation = (ColumnInformation) enumeration.nextElement();

      if (columnInformation.getBbid().compareTo(columnID) == 0) {
        return columnInformation;
      }
    }

    throw new ColumnInformationNotAvailableException("Columninformation for \"" + columnID + "\" not available.");
  }

  /**
   * TODO: 
   * 
   * <br>
   * new: added search bia Methods
   *
   * @param qcmethod TODO
   *
   * @return TODO
   *
   * @throws ColumnInformationNotAvailableException TODO
   */
  public static ColumnInformation getColumnInformationByMethod(String qcmethod)
    throws ColumnInformationNotAvailableException {
      
      try {
          String colID = Method.findByMethodLabel(qcmethod).getColumnID();
          return getColumnInformationByID(colID);
      } catch (Exception e) {
          Logger.warning(thiz, "unknown column for method: " + qcmethod + ".");
      }
  
      
      
    try {
      qcmethod = qcmethod.substring(qcmethod.lastIndexOf('\\') + 1);
      qcmethod = Method.transform(qcmethod);

      ColumnInformation columnInformation = (ColumnInformation) columns.get(qcmethod);

      if (columnInformation == null) {
        throw new NullPointerException();
      }

      return columnInformation;
    } catch (NullPointerException e) {
      throw new ColumnInformationNotAvailableException("Columninformation for method \"" + qcmethod + "\" not available.");
    }
  }

  /**
   * TODO: 
   *
   * @param information TODO
   *
   * @return TODO
   *
   * @throws ColumnInformationNotAvailableException TODO
   *
   * @deprecated
   */
  protected static ColumnInformation determineColumn(Hashtable information)
    throws ColumnInformationNotAvailableException {
    String qcmethod = (String) information.get(AcquisitionParameter.QC_METHOD);

    return getColumnInformationByMethod(qcmethod);
  }
}
