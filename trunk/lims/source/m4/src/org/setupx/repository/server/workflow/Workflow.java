package org.setupx.repository.server.workflow;

import org.setupx.repository.Config;
import org.setupx.repository.core.InitException;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.workflow.elements.ProcessingException;


/**
 * Representation of a workflow.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 */
public class Workflow {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * shared memory containing all information that are accassable between the different systems
   *
   * @link aggregationByValue
   * @undirected
   * @supplierCardinality 1
   * @clientCardinality 1
   */
  public WorkflowMemory workflowMemory = new WorkflowMemory();
  protected WorkFlowConfiguration configuration;

  /**
   * an array containing WFElement objects. the position in the vector is the id.
   *
   * @associates org.setupx.repository.server.workflow.WFElement
   * @directed
   * @link aggregationByValue
   * @labelDirection reverse
   * @label registrated at
   * @clientCardinality 1
   * @supplierCardinality 0..
   */
  protected WFElement[] registratedProcessables = new WFElement[Config.DEFAULT_NUM_OF_WFELEMENTS];

  /** the manager managing the accesses to the workflows */
  private WorkFlowManager flowManager;

  /** id, showing which workflow might be executet next. */
  private int followingWorkFlowID = 0;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Workflow object.
   *
   * @param configuration the Configuration defining the ID, label, ... of this new WorkFlowElement
   */
  protected Workflow(WorkFlowConfiguration configuration)
    throws InitException {
    Logger.debug(this, "new instance " + configuration.toString());

    // creating an empty shared memory for this workflow
    this.workflowMemory = new WorkflowMemory();

    try {
      // flowmanager init ...
      flowManager = WorkFlowManagerFactory.getInstance();

      this.configuration = configuration;

      // check if all the elements are available
      flowManager.checkWorkFlowIDs(configuration.wfElementIds);

      // init these in the workflow
      Logger.debug(this, "start registrating " + configuration.wfElementIds.length + " elements ...");
      flowManager.register(configuration.wfElementIds, this);

      // DEBUGGING --->
      if (this.getNumWFElements() != configuration.wfElementIds.length) {
        throw new RuntimeException("the elements were not all registrated ...");
      }

      for (int i = 0; i < this.getWorkflowElementIDs().length; i++) {
        if (this.getWorkflowElement(this.getWorkflowElementIDs()[i]) == null) {
          throw new RuntimeException("element is null");
        }

        if (this.getWorkflowElement(this.getWorkflowElementIDs()[i]).getConfiguration() == null) {
          throw new RuntimeException("element is null");
        }
      }

      // <---- DEBUGGING
    } catch (WorkFlowException e) {
      throw new InitException(e);
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * return the number of WFElements
   *
   * @return the number of WFElements
   *
   * @see WFElement
   */
  public int getNumWFElements() {
    int result = 0;

    for (int i = 0; i < this.registratedProcessables.length; i++) {
      if (this.registratedProcessables[i] != null) {
        result++;
      }
    }

    return result;
  }

  /**
   * checks if a process is registrated by checking if the elemt is not null and the id is registrated
   *
   * @param processable the process that will be checked
   *
   * @return true if the process is registrated
   */
  public boolean isValidProcess(WFElement i_element) {
    /*
     * chaged to vector while
     * (registratedProcessables.keys().hasMoreElements()) { if
     * (processable.getConfiguration().getId().compareTo( (String)
     * registratedProcessables.keys().nextElement()) == 0) { return true; } }
     */
    if (i_element == null) {
      return false;
    }

    for (int i = 0; i < this.registratedProcessables.length; i++) {
      WFElement element = (WFElement) this.registratedProcessables[i];

      if (element.getConfiguration().getId() == i_element.getConfiguration().getId()) {
        return true;
      }
    }

    return false;
  }

  /**
   * @param id the id of the <b>registrated </b> element
   *
   * @return the WFElement whith the defined id <p>
   *
   * @throws WorkFlowException unable to get the element - (outside the array - or the element was not found)
   *
   * @see Config#DEFAULT_NUM_OF_WFELEMENTS
   */
  public WFElement getWorkflowElement(int id) throws WorkFlowException {
    WFElement result = null;

    if (id > this.registratedProcessables.length) {
      throw new WorkFlowException("the element you tried to access is not registrated in " + "the manager - (outside the range of " + this.registratedProcessables.length);
    }

    try {
      result = this.registratedProcessables[id];
    } catch (ArrayIndexOutOfBoundsException e) {
      throw new WorkFlowException("the element you tried to access is not registrated in " + "the manager - (you tried to access an object outside the ranges of all registrated WFElements ", e);
    }

    if (result == null) {
      Logger.log(this, " got problems ....");
      this.showRegistratedElements();
      throw new WorkFlowException("no element with the id " + id + " is registrated in the system.");
    }

    return result;
  }

  /**
   * @return
   */
  public int[] getWorkflowElementIDs() {
    return this.configuration.wfElementIds;
  }

  /**
   * @return all ids for
   */
  public int[] getWorkflowElementIDsSorted() {
    int[] ids = new int[this.getNumWFElements()];

    int count = 0;

    for (int i = 0; i < this.registratedProcessables.length; i++) {
      if (this.registratedProcessables[i] != null) {
        ids[count] = this.registratedProcessables[i].getConfiguration().getId();
        count++;
      }
    }

    boolean DEBUG = false;

    if (DEBUG) {
      for (int i = 0; i < ids.length; i++) {
        Logger.debug(this, i + "  " + ids[i]);
      }
    }

    return ids;
  }

  /**
   * showes all registrated elements on the screen
   */
  public String showRegistratedElements() {
    int[] ids = this.getWorkflowElementIDsSorted();
    StringBuffer buffer = new StringBuffer();

    buffer.append("\n --- All Registrated Elements --------------------");

    for (int i = 0; i < ids.length; i++) {
      int id = ids[i];

      WFElement element = null;

      try {
        element = this.getWorkflowElement(id);
      } catch (WorkFlowException e) {
        e.printStackTrace();
      }

      buffer.append("\n Registrated Element no. " + id + " " + Util.getClassName(element) + "\t" + element.getConfiguration().toString());
    }

    Logger.log(this, buffer.toString());

    return buffer.toString();
  }

  /**
   * @return the id, of the next process that has to be called after this one ...
   *
   * @throws WorkFlowException
   * @throws ProcessingException
   */
  public int start() throws WorkFlowException, ProcessingException {
    Logger.log(this, " ########################################################################################");
    Logger.log(this, "            starting this workflow " + this.configuration + "  "); // + this.showRegistratedElements());
    Logger.log(this, " ########################################################################################");

    WFElement element = null;

    // start the workflow
    for (int i = 0; i != this.getWorkflowElementIDs().length; i++) {
      // get id for first element
      int j = this.getWorkflowElementIDs()[i];

      // find the matching element
      element = this.getWorkflowElement(j);

      // starting the current element ...
      Logger.debug(this, "starting element nr " + i + " " + element);
      element.action();
    }

    return this.followingWorkFlowID = 0;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String toString() {
    return this.configuration.id + "  " + this.configuration.comment + " wfMem: " + this.workflowMemory;
  }
}
