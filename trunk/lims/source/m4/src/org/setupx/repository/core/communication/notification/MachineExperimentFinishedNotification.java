/**
 * ============================================================================ File:    MachineExperimentFinishedNotification.java Package: org.setupx.repository.core.communication.notification cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.notification;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class MachineExperimentFinishedNotification extends Notification {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new MachineExperimentFinishedNotification object.
   *
   * @param promtID 
   */
  public MachineExperimentFinishedNotification(int promtID) {
    super();
    setMessage("Experiment is finished: " + promtID);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String[] getRecipients() {
    return new String[] { "mscholz@ucdavis.edu" };
  }
}
