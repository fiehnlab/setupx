/**
 * ============================================================================ File:    NBCISpeciesInputfield.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import java.util.Hashtable;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.setupx.repository.core.communication.ncbi.local.NCBIEntry;
import org.setupx.repository.core.communication.ncbi.local.NCBI_Classifier;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.web.forms.ArrayExpandException;
import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.InActiveException;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.relation.AbstractRelation;
import org.setupx.repository.web.forms.relation.Relation;
import org.setupx.repository.web.forms.relation.RelationKingdom;
import org.setupx.repository.web.forms.restriction.NCBIVocRestriction;
import org.setupx.repository.web.forms.restriction.Restriction;
import org.setupx.repository.web.forms.validation.Link;


/**
 * Specific input field for the definition of Species. It includes a <b>single</b> StringInputfiled.
 * 
 * <p>
 * <b>Species information:</b> The only information stored in the System is the refernce ID related to the NCBI Taxonomy. All attributes of the object are taken dynamicly from the NCBI system.
 * </p>
 * 
 * <p>
 * <b>Organ:</b>TODO - will be added as a seperate field in the fieldarray
 * </p>
 * 
 * <p>
 * <b>Special organisms:</b> in some cases there are additional informationfrom different databases - like TAIR
 * </p>
 * 
 * <p></p>
 * 
 * <p>
 * <br><b>TODO </b>add arabidobsis library - TAIR, plantontology.org
 * </p>
 * <pre>contains String-Inputfields for seperate definitons</pre>
 * <pre>replaced the storage of the related information in an hashtable by a real object</pre>
 *
 * @hibernate.subclass
 *
 * @see http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=9615
 * @see org.setupx.repository.web.forms.SpeciesInputfield
 */
public class NBCISpeciesInputfield extends MultiField {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** relation to an entry object from NCBI */
  public NCBIEntry entry;

  /** the ncbi ID for this organism */
  private int ncbi_ID = NCBIVocRestriction.UNKNOWN_NCBI_ORGANISM_ID;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String ncbiIMG = "<img  border='0' src=\"pics/ncbi.gif\"/>";
  public static final String listIMG = "<img  border='0' src=\"pics/select-grille.gif\">";
  public static final String selectIMG = "<img  border='0' src=\"pics/select-thesaurus.gif\">";
  public static final String googleIMG = "<img  border='0' src=\"pics/google.gif\">";
  public static final String cameraIMG = "<img  border='0' src=\"pics/camera.gif\">";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  //private static DictionaryWebService dictionary = new DictionaryWebService();

  /**
   * Creates a new NBCISpeciesInputfield object.
   * 
   * <p>
   * The field contains by default three <code>Relations</code> pointing form this filed to the organinputfields. These relations active the correct organinputfield.
   * </p>
   *
   * @throws ArrayExpandException 
   */
  public NBCISpeciesInputfield() throws ArrayExpandException {
    this("");
  }

  /**
   * Creates a new NBCISpeciesInputfield object.
   *
   * @param TEST 
   *
   * @throws ArrayExpandException 
   */
  public NBCISpeciesInputfield(String speciesNameString)
    throws ArrayExpandException {
    super("Species", "Definition of the Species. For example type in \"Bos taurus\" or even \"cow\" to specify a cow.");
    try {
      if (this.uoid != 0) {
        throw new RuntimeException("ARRRR!!");
      }

      debug("checking myself (" + this.hashCode() + "): START  number of internfields: " + this.size());

      boolean create = false;

      try {
        for (int i = 0; i < this.size(); i++) {
          FormObject object = this.getField(i);
          debug("pos:" + i + " " + object.toString());
        }

        StringInputfield inputfield = (StringInputfield) this.getField(0);

        if (inputfield == null) {
          create = true;
        }
      } catch (ArrayIndexOutOfBoundsException e) {
        create = true;
      }

      if (!create) {
        return;
      }

      debug("my UOID: " + uoid);

      // in case it is an init - there will be no field added yet - so create a new one
      warning(this, "the internal StringInputfield is not set yet - creating a new one.");

      // warning(this, this.toStringTree());
      // the name
      StringInputfield speciesName = new StringInputfield("species", "please type the name OR the NCBI code if you know");
      speciesName.setAutosubmit(true);

      // ncbi restriction
      debug("creating a new NCBIVocRestriction()");

      Restriction restriction = new NCBIVocRestriction();

      debug("adding the new speciesName-Inputfield");
      this.addField(speciesName);
      speciesName.setRestriction(restriction);

      this.getSpeciesInputfield().setValue(speciesNameString);

      // new comment filed for species that are not declared in the ncbi taxonomy
      StringInputfield stringInputfield_speciesComment = new StringInputfield("comment", "in case you can't define the species by the ncbi taxonomy, please define it here");
      stringInputfield_speciesComment.setRestriction(null);

      // relation that only for defined species the comment field is shown
      Relation relation = new Relation(NCBI_Classifier.UNKNOWN, stringInputfield_speciesComment);
      speciesName.addRelation(relation);
      this.addField(stringInputfield_speciesComment);

      debug("checking myself (" + this.hashCode() + "): END number of internfields: " + this.size());
    } catch (Exception e) {
      e.printStackTrace();
    }

    debug("constructor:  number of relations: " + this.getRelations().length);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param id TODO
   * @param entry TODO
   */
  public void addInformation(int id, NCBIEntry entry) {
    setNCBI_ID(id);

    try {
      getSpeciesInputfield().setValue(entry.getName());
    } catch (NBCISpeciesInputfieldException e) {
      e.printStackTrace();
    }

    this.entry = entry;
    //setInformation(hashtable); @deprecated
  }

  /**
   * cloning is different to normal StringInputfield cause there is a special relation in it.
   *
   * @throws CloneException
   *
   * @see org.setupx.repository.web.forms.inputfield.InputField#cloneField()
   */
  public InputField cloneField() throws CloneException {
    NBCISpeciesInputfield speciesInputfield;

    try {
      speciesInputfield = new NBCISpeciesInputfield(getSpeciesName());
    } catch (ArrayExpandException e) {
      throw new CloneException(e);
    }

    // copying the relation to parent 
    speciesInputfield.setParent(this.getParent());

    return speciesInputfield;
  }

  /**
   * based on MultiField.
   * <pre>TODO  add img preview for species</pre>
   *
   * @see MultiField#createHTML()
   * @see org.setupx.repository.web.forms.FormObject#createHTML()
   */
  public String createHTML(int col) {
    StringBuffer response = new StringBuffer();

    // create a table contaitaining 
    response.append("<!-- " + this.getName() + " (" + this.getDescription() + ") -->");
    // response.append("<tr><td colspan=\"" + col + "\" align=\"center\">");
    // response.append("<table " + WebConstants.BORDER + " width=\"95%\">\n");
    // col = 4;
    // header
    // response.append("<th colspan=\"" + col + "\" > Species: " + this.getValue() + " </th>\n");
    // long description
    response.append("<tr><td colspan=\"" + col + "\" ><span class=\"small\">" + this.getLongDescription() + "</span></td></tr>");

    // put every inputfield in there
    /* for (int i = 0; i < this.getFields().length; i++) {
       InputField inputField = (InputField) this.getFields()[i];
       response.append("<tr><td colspan=\"2\"></td></tr>");
       response.append(inputField.createHTML());
       }
     */
    response.append(this.createHTMLChilds(col));

    try {
      if (entry.isSpecies()) {
        response.append("<tr><td>Species</td><td colspan=\"" + (col - 1) + "\">" + entry.getSpeciesTypeName() + "</td></tr>");
      }
    } catch (NullPointerException e) {
      warning(this, "species not available");
    }

    try {
      if (entry.isSpecies()) {
        response.append("<tr><td>Rank</td><td colspan=\"" + (col - 1) + "\">" + entry.getRank() + "</td></tr>");
        //response.append("<tr><td>Definition</td><colspan=\"" + (col - 1) + "\"<td><font class=\"small\">" + dictionary.define(entry.getName())[0].getWordDefinition() + "</font></td></tr>");
      }
    } catch (NullPointerException e) {
      warning(this, "rank not available");
    }

    try {
      // link to parent NCBI object
      if (entry.isSpecies()) {
        response.append("<tr><td>Upper Rank</td><td  colspan=\"" + (col - 1) + "\">" + listIMG + " " + this.entry.toLinkSettingParent(this.getSpeciesInputfield()) + "</td></tr>");
      }

      //children
      int size = this.entry.getChilds().size();
      debug("creating " + size + " childs ...");

      if (size > 0) {
        String msg = "";

        if (!this.entry.isSpecies()) {
          msg = "The name that you selected is not a species. It is a " + this.entry.getRank() + ". Please select one of the Species. <br>In case it is not listed here, <br>keep browsing down in the tree";
        } else {
          msg = "The species is a valid species. <br>But there are subdefenitions available. <br>In case your sample are one of the subgroups, <br>just press the icon next to the item.";
        }

        response.append("<tr><td valign=\"top\" rowspan=\"" + (size + 1) + "\">Sub-Elements <br><font class=\"small\">" + msg + "</font></td><td></tr>");

        for (int i = 0; i < size; i++) {
          response.append("<tr><td  colspan=\"" + (col - 1) + "\">" + selectIMG + " <span class=\"small\">[" + (i + 1) + "/" + size + "]</span> " + this.entry.toLinkSettingChild(this.getSpeciesInputfield(), i) + "</td></tr>");
        }
      }

      if (entry.isSpecies()) {
        response.append("<tr><td>Image<br><span class=small>external link to Images of a " + this.entry.getName() + " (if available)</span></td><td colspan=\"" + (col - 1) + "\"><a href=\"http://images.google.com/images?q=" + this.entry.getName() + "\" target=\"new\">" + cameraIMG +
          "</a> Images of a " + this.entry.getName() + " </td></tr>");
      }

      if (entry.isSpecies()) {
        response.append("\n<tr>\n<td>NCBI-Definiton" + "<br><span class=small>external link to the Taxonomy Database with information about " + this.entry.getName() + "</span>" + "</td>\n<td><a href=\"" + createLinkExternNCBI(this.ncbi_ID + "") + "\" target=\"_new\">" + ncbiIMG + "</a></td></tr>");
      }
    } catch (Exception e) {
      // this happens when the entry is not found in the system yet.
      err("entry not found or parent- or child-field not found: " + e.toString());
    }

    // add a help button
    response.append(this.createHelpButton());

    // close the table
    //response.append("</table></td></tr>");
    return response.toString();
  }

  /**
   * creates a link that referes to the external ncbi database
   *
   * @param id ncbi_id
   *
   * @return a link referes to the external ncbi database
   */
  public static Link createLinkExternNCBI(String id) {
    String baseURL = "http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=";
    String link = baseURL.concat(id + "");

    return new Link(link);
  }

  /**
   * @throws NBCISpeciesInputfieldException
   */
  public StringInputfield getSpeciesInputfield() throws NBCISpeciesInputfieldException {
    int pos = 0;

    if ((this.getFields().length == 0) || (this.getField(pos) == null)) {
      throw new NBCISpeciesInputfieldException("the stringinputfield for species is not initalized.");
    }
    
    debug(this, "returning " + this.getField(pos).toString());

    return (StringInputfield) (this.getField(pos));
  }

  /**
   * @return
   */
  public String getSpeciesName() {
    debug("determine name() - current number of fields: " + this.getFields().length);

    try {
      return getSpeciesInputfield().getValue() + "";
    } catch (NBCISpeciesInputfieldException e) {
      warning(e);

      return "--";
    }
  }

  /**
   * TODO: 
   *
   * @throws NBCISpeciesInputfieldException TODO
   */
  public void resetInformation() throws NBCISpeciesInputfieldException {
    debug("resetting information in the fields ");
    setNCBI_ID(NCBIVocRestriction.UNKNOWN_NCBI_ORGANISM_ID);
    this.getSpeciesInputfield().setValue(NCBI_Classifier.UNKNOWN_SPECIES);
    this.entry = null;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#validate()
   */
  public String validate() {
    try {
      return this.getSpeciesInputfield().validate();
    } catch (NBCISpeciesInputfieldException e) {
      return e.toString();
    }
  }

  /**
   * TODO: 
   *
   * @param id TODO
   */
  private void setNCBI_ID(int id) {
    this.ncbi_ID = id;
  }
  
 
  
  /*
   * not realy a setter - because this is a multifield and does not have an value! so it just forwards the value to the speciesinputfield
   * which is a regular StringInputField.
   * (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField#setValue(java.lang.Object)
   */
  public void setValue(Object object){
      if (object.toString().length() > 1){
          warning(this, "setValue() XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
          warning(this, "setValue()                        setValue is only usable for a ncbientry");
          warning(this, "setValue() XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
          warning(this, "setValue() ");
          warning(this, "setValue() trying to set value : \""  + object + "\" on this.");
          warning(this, "setValue() setting it inside the correct field.");
          try {
              // apperently there is nothing happening

            warning(this, "setValue() --- Relations of myself: ---");
            AbstractRelation[] myAbstractRelations = this.getRelations();
            for (int d = 0; d < myAbstractRelations.length; d++) {
                AbstractRelation relation = myAbstractRelations[d];
                warning(this, "setValue() Relation: " + d + ":  " + Util.getClassName(relation) );
                if (relation instanceof RelationKingdom){
                    FormObject[] targets = ((RelationKingdom ) relation).getRelationTargets();
                    for (int j = 0; j < targets.length; j++) {
                        warning(this, "setValue()   target: " + targets[j].isActive() + "  " + targets[j]);
                    }
                }
            }

            this.getSpeciesInputfield().setValue(object);
            warning(this, "setValue() --- Relations of the speciesinputfield: ---");
            // checking which of the relations is connected to that specific inputfield.
            AbstractRelation[] abstractRelations  =this.getSpeciesInputfield().getRelations();
            for (int i = 0; i < abstractRelations.length; i++) {
                AbstractRelation relation = abstractRelations[i];
                warning(this, "setValue() Relation: " + i + ":  " + Util.getClassName(relation) );
                
                if (relation instanceof RelationKingdom){
                    FormObject[] targets = ((RelationKingdom ) relation).getRelationTargets();
                    for (int j = 0; j < targets.length; j++) {
                        warning(this, "setValue()   target: " + targets[j].isActive() + "  " + targets[j]);
                    }
                }
            }
          } catch (NBCISpeciesInputfieldException e) {
              e.printStackTrace();
          }
          warning(this, "setValue() ");
          warning(this, "setValue() ");
          warning(this, "setValue() XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
      }
  }
  
  
  public Hashtable getExportElements() {
      Hashtable hashtable = super.getExportElements();
      hashtable.put("NCBI_Tax_ID", this.entry.getTaxID());
      hashtable.put("speciestype", this.entry.getSpeciesTypeName());
      return hashtable;
}
}
