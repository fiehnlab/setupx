package org.setupx.repository.core.communication.binbase;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class GenerationJobID {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String id;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new GID object.
   *
   * @param id 
   */
  public GenerationJobID(String id) {
    this.id = id;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getID() {
    return id;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return this.id;
  }

  /**
   * TODO: 
   *
   * @param ids TODO
   *
   * @return TODO
   */
  protected static String[] asStringArray(GenerationJobID[] ids) {
    String[] result = new String[ids.length];

    for (int i = 0; i < ids.length; i++) {
      result[i] = ids[i].getID();
    }

    return result;
  }
}
