package org.setupx.repository.web;

public class InvalidIpException extends Exception {

    public InvalidIpException(String string) {
        super(string);
    }

}
