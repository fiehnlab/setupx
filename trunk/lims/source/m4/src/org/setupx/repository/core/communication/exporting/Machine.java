/**
 * ============================================================================ File:    Machine.java Package: org.setupx.repository.core.communication.exporting cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.exporting;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample;
import org.setupx.repository.server.persistence.hibernate.SXSerializable;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Representing a single machine in the lab.<br> NEW: now merged with former object Instrument
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 *
 * @hibernate.class table="machine"
 * @hibernate.discriminator column="discriminator"
 */
public abstract class Machine extends CoreObject implements SXSerializable {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private ACQMachineConfigurationIterator configurationIterator = null;
  private Matcher m = null;
  private String comment;
  private String label;
  private char machineLetter;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static int lastPos = 1;
  public static final int E_MACHINE = 164;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Machine object.
   */
  public Machine() {
  }

  /**
   * Creates a new Machine object.
   *
   * @param label 
   * @param machineLetter 
   * @param comment 
   */
  public Machine(String label, char machineLetter, String comment) {
    this.setLabel(label);
    this.setMachineLetter(machineLetter);
    this.setComment(comment);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * return an iterator containing all Parameters in the correct order for a Machine
   *
   * @return an iterator containing all Parameters in the correct order for a Machine
   */
  public ACQMachineConfigurationIterator getConfiguration() {
    return ACQMachineConfigurationIterator.iterator(this);
  }

  /**
   * creates a DEFAULT configuration for this Machine
   *
   * @return
   */
  public final Hashtable getDefaultValues() {
    //TODO
    Hashtable hashtable = new Hashtable();
    String value;
    Iterator iterator = this.getConfiguration();

    while (iterator.hasNext()) {
      value = (String) iterator.next();

      if (value.compareTo(AcquisitionParameter.TYPE) == 0) {
        hashtable.put(value, "Sample");
      } else if (value.compareTo(AcquisitionParameter.FOLDER) == 0) {
        hashtable.put(value, "\\Acquired Samples\\SetupX");
      } else if (value.compareTo(AcquisitionParameter.QC_METHOD) == 0) {
        hashtable.put(value, "\\Quality Control (QC) Methods\\No daily instrument optimization");
      } else if (value.compareTo(AcquisitionParameter.AS_METHOD) == 0) {
        hashtable.put(value, "");
      } else if (value.compareTo(AcquisitionParameter.GC_METHOD) == 0) {
        hashtable.put(value, "\\Gas Chromatograph (GC) Methods\\Tobias\\quick-split-1:100");
      } else if (value.compareTo(AcquisitionParameter.MS_METHOD) == 0) {
        hashtable.put(value, "\\Mass Spectrometer (MS) Methods\\Tobias\\quick");
      } else if (value.compareTo(AcquisitionParameter.DP_METHOD) == 0) {
        hashtable.put(value, "\\Data Processing (DP) Methods\\core lab standard");
      } else if (value.compareTo(AcquisitionParameter.STATUS) == 0) {
        hashtable.put(value, "");
      } else if (value.compareTo(AcquisitionParameter.REPETITIONS) == 0) {
        hashtable.put(value, "1");
      } else if (value.compareTo(AcquisitionParameter.VIAL) == 0) {
        lastPos = lastPos++;
        hashtable.put(value, "" + lastPos);
      } else {
        hashtable.put(value, "XXX" + value + "XXX");
      }
    }

    return hashtable;
  }

  /**
   * The setter method for Unique Object identifier.
   */
  public void setUOID(long lId) {
    uoid = lId;
  }

  /**
   * The getter method for Unique Object identifier.
   * 
   * <p>
   * The id is generated by the persistence layer.
   * </p>
   *
   * @return unique id for this object
   *
   * @hibernate.id column = "uoid" generator-class="native"
   */
  public long getUOID() {
    return (uoid);
  }

  /**
   * randomise samples based on the jobinformation
   */
  public final AcquisitionSample[] randomise(AcquisitionSample[] samples, JobInformation information) {
    debug("start randomize samples. ");

    return Randomisation.newInstance(this, information.getRandomisationSOP()).randomise(samples, information);
  }

  /**
   * File extension of Files created by this machine
   *
   * @return File extension of Files created by this machine
   */
  public abstract String getFileExtension();

  /*
   *  (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return "Machine " + this.getComment() + "(" + this.getUOID() + ") Config: " + this.getConfiguration().toString();
  }

  /**
   * valides a sample name for this specific machine
   */
  public final boolean validateSampleName(String token) {
    Pattern pattern = Pattern.compile(this.getSampleNamePattern());
    m = pattern.matcher(token);

    return m.matches();
  }

  /**
   * regex for a SampleName
   */
  public abstract String getSampleNamePattern();

  /**
   * characters used to divide the different entries in the file
   */
  public abstract String getTokenDivider();

  /**
   * TODO: 
   *
   * @param comment TODO
   */
  public void setComment(String comment) {
    this.comment = comment;
  }

  /**
   * @hibernate.property
   */
  public String getComment() {
    return comment;
  }

  /**
   * TODO: 
   *
   * @param label TODO
   */
  public void setLabel(String label) {
    this.label = label;
  }

  /**
   * @hibernate.property
   */
  public String getLabel() {
    return label;
  }

  /**
   * TODO: 
   *
   * @param machineLetter TODO
   */
  public void setMachineLetter(char machineLetter) {
    this.machineLetter = machineLetter;
  }

  /**
   * @hibernate.property
   */
  public char getMachineLetter() {
    return machineLetter;
  }
}
