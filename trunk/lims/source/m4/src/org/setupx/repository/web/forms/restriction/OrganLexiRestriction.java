/**
 * ============================================================================ File:    OrganLexiRestriction.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.core.communication.vocabulary.LibraryOrgan;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.5 $
 *
 * @hibernate.subclass
 */
public class OrganLexiRestriction extends LexiRestriction {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new OrganLexiRestriction object.
   *
   * @param field 
   * @param lib 
   */
  public OrganLexiRestriction(LibraryOrgan lib) {
    super(lib);
  }

  /**
   * Creates a new OrganLexiRestriction object.
   */
  public OrganLexiRestriction() {
    super();
  }
}
