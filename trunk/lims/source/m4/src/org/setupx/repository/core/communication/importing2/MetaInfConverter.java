/**
 * ============================================================================ File: MetaInfConverter.java Package:
 * org.setupx.repository.core.communication.importing2 cvs: $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21
 * scholz Exp $ ============================================================================ Martin Scholz Copyright (C)
 * 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/
 * ============================================================================
 */
package org.setupx.repository.core.communication.importing2;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.communication.exporting.Machine;
import org.setupx.repository.web.forms.event.Event;
import org.setupx.repository.web.forms.event.MachineRunEvent;

/**
 * 
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class MetaInfConverter extends CoreObject {
    // ~ Instance fields
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private Machine machine;
    private Set events;
    private String content;
    private String id;
    private String line;
    private String status;
    private StringTokenizer stringTokenizer;

    // ~ Static fields/initializers
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private static final int NAME = 1;
    private static final int FOLDER = 3;
    private static final int DATE = 4;
    private static final int TIME = 5;
    private static final int STATUS = 6;
    private static final int TYPE = 8;
    private static final String DELIMITER_TAB = "\t";
    private static final String DELIMITER_LINE = "\n";

    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new MetaInfConverter object.
     * 
     * @param i_machine
     *                
     * @param i_file
     *                
     * @throws IOException
     *                 
     * @throws MetaInfConverterException
     *                 
     */
    public MetaInfConverter(Machine i_machine, File i_file) throws IOException, MetaInfConverterException {
        this(i_machine, org.setupx.repository.core.util.File.getFileContent(i_file));
    }

    /**
     * Creates a new MetaInfConverter object.
     * 
     * @param i_machine
     *                
     * @param fileContent
     *                
     * @throws MetaInfConverterException
     *                 
     */
    public MetaInfConverter(Machine i_machine, String fileContent) throws MetaInfConverterException {
        this.content = fileContent;
        this.machine = i_machine;

        try {
            this.events = this.convert();
        } catch (ParameterException e) {
            throw new MetaInfConverterException(e);
        }
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @return
     */
    public Set getEvents() {
        return this.events;
    }

    /**
     * converts the string into events
     * 
     * @throws ParameterException
     */
    private Set convert() throws ParameterException {
        HashSet hashSet = new HashSet();

        // cut into lines
        this.stringTokenizer = new StringTokenizer(this.content, DELIMITER_LINE);

        while (this.stringTokenizer.hasMoreTokens()) {
            try {
                this.line = this.stringTokenizer.nextToken();

                // find status
                this.status = this.find(STATUS);

                // create event
                if (this.status.compareTo("Finished") == 0) {
                    // debug("got a sample: " + id + " status: " + status);
                    // find id : like: 051010akbsa05:1
                    this.id = this.find(NAME);

                    Event event = new MachineRunEvent(this.machine, this.find(NAME), this.toDate(this.find(DATE), this
                            .find(TIME)), this.status, this.find(FOLDER), this.find(TYPE));
                    hashSet.add(event);
                }
            } catch (Exception e) {
                warning(this, "unable to convert: " + e);
                // e.printStackTrace();
            }
        }

        return hashSet;
    }

    /**
     * TODO: 
     * 
     * @param typeOfInformation
     *                TODO
     * @return TODO
     * @throws ParameterException
     *                 TODO
     */
    private String find(int typeOfInformation) throws ParameterException {
        StringTokenizer tokenizer = new StringTokenizer(this.line, DELIMITER_TAB);
        int pos = 0;

        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();

            if (typeOfInformation == pos) {
                return token;
            }

            pos++;
        }

        throw new ParameterException("unable to find value at pos " + typeOfInformation + " in: \"" + this.line + "\"");
    }

    /**
     * TODO: 
     * 
     * @param date
     *                TODO
     * @param time
     *                TODO
     * @return TODO
     */
    private Date toDate(String date, String time) {
        warning(this, "has to be replaced !! ");

        try {
            Date returnValue = new Date(date); // + " " + time);

            return returnValue;
        } catch (Exception e) {
            return new Date();
        }
    }
}
