/**
 * ============================================================================ File:    HotFixInsertScannedSamples.java Package: org.setupx.repository.core.util.hotfix cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.hotfix;

import org.setupx.repository.core.communication.importing.logfile.ScannedPair;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;

import java.util.Enumeration;
import java.util.Hashtable;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class HotFixInsertScannedSamples {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @throws PersistenceActionUpdateException TODO
   */
  public static void run() throws PersistenceActionUpdateException {
    Hashtable hashtable = new Hashtable();
    hashtable.put("060615abrsa01:1", "21202");
    hashtable.put("060615abrsa02:1", "21269");
    hashtable.put("060615abrsa03:1", "21249");
    hashtable.put("060615abrsa04:1", "21259");
    hashtable.put("060615abrsa05:1", "21212");
    hashtable.put("060615abrsa06:1", "21238");
    hashtable.put("060615abrsa07:1", "21233");
    hashtable.put("060615abrsa08:1", "21254");
    hashtable.put("060615abrsa09:1", "21274");
    hashtable.put("060615abrsa10:1", "21197");
    hashtable.put("060615abrsa11:1", "21192");
    hashtable.put("060615abrsa12:1", "21207");
    hashtable.put("060615abrsa13:1", "21228");
    hashtable.put("060615abrsa14:1", "21243");
    hashtable.put("060615abrsa15:1", "21218");
    hashtable.put("060615abrsa16:1", "21264");
    hashtable.put("060615abrsa17:1", "21187");
    hashtable.put("060615abrsa18:1", "21223");

    Enumeration enumeration = hashtable.keys();

    while (enumeration.hasMoreElements()) {
      String key = (String) enumeration.nextElement();
      String label = (String) hashtable.get(key);

      new ScannedPair(key, label).update(true);
    }
  }
}
