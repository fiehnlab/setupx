/**
 * ============================================================================ File: ScanEventHandler.java Package:
 * org.setupx.repository.core.communication.importing cvs: $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21
 * scholz Exp $ ============================================================================ Martin Scholz Copyright (C)
 * 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/
 * ============================================================================
 */
package org.setupx.repository.core.communication.importing;

import java.util.HashSet;
import java.util.List;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.CommunicationException;
import org.setupx.repository.core.communication.notification.MachineExperimentFinishedNotification;
import org.setupx.repository.core.communication.notification.NotificationCentral;
import org.setupx.repository.server.logging.Message;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;

/**
 * 
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class ScanEventHandler extends CoreObject {
    // ~ Instance fields
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /** all samplesnames that have been imported */
    public HashSet completedClassIDs = new HashSet();

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * TODO: 
     * 
     * @param clazzID
     *                TODO
     * @param sampleACQNames
     *                TODO
     * @throws CommunicationException
     *                 TODO
     */
    public void clazzCompleted(int clazzID, String[] sampleACQNames) throws CommunicationException {
	Message.createMessage(Clazz.class, clazzID, "Class was finished.");

	// in case all classes of this experiment are done notiy tecs
	List clazzIDs;

	try {
	    clazzIDs = new SXQuery().findClazzesIDbyClazz(clazzID);

	    if (this.completedClassIDs.contains(clazzIDs)) {
		// ok the whole experiment is done
		int promtID = new SXQuery().findPromtIDbyClazz(clazzID);
		this.experimentCompleted(promtID);
	    }
	} catch (PersistenceActionFindException e) {
	    throw new CommunicationException("unable to find clazz ids that are part of the same experiment.");
	}
    }

    public void experimentCompleted(int promtID) {
	NotificationCentral.notify(new MachineExperimentFinishedNotification(promtID));
    }
}
