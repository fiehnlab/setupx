package org.setupx.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;



public class SystemCommand {

    
    /**
     * client for the whois directory service
     */

    public static String whois(final String address, final String queryWord) throws SystemCommandException {
        try {
            InputStream inputStream = Runtime.getRuntime().exec("whois " + address).getInputStream();
            return parse(inputStream, queryWord);
        } catch (IOException e) {
            throw new SystemCommandException(e);
        }
    }

    private static String parse(InputStream inputStream, String string) throws IOException, SystemCommandException {
        String line;

        BufferedReader input = new BufferedReader(new InputStreamReader(inputStream));
          while ((line = input.readLine()) != null) {
              if (line.indexOf(string) > -1) {
                  // got the line containing the word
                  return line;
              }
            }
          input.close();
          throw new SystemCommandException("unable to find " + string + " in the output.");
        }

    private static String parse(InputStream inputStream) throws IOException, SystemCommandException {
        String text = "";
        String line ;
        BufferedReader input = new BufferedReader(new InputStreamReader(inputStream));
          while ((line = input.readLine()) != null) {
              text = text.concat(line);
              text = text.concat("\n"   );
            }
          input.close();
          return text;
        }

    /**tell how long the system has been running
     * @throws SystemCommandException 
     */
    public static String uptime() throws SystemCommandException {
        try {
            InputStream inputStream = Runtime.getRuntime().exec("uptime").getInputStream();
            return parse(inputStream);
        } catch (IOException e) {
            throw new SystemCommandException(e);
        }
    }


    /**
     * netstat  -  
     * Anzeige von Netzwerksverbindungen, Routentabellen, Schnittstellenstatistiken, maskierten Verbindungen, Netlink-Nachrichten und Mitgliedschaft in Multicastgruppen
            */
    public static String netstat() throws SystemCommandException {
        try {
            InputStream inputStream = Runtime.getRuntime().exec("netstat").getInputStream();
            return parse(inputStream);
        } catch (IOException e) {
            throw new SystemCommandException(e);
        }
    }
    
    /**
     *  report file system disk space usage
     */
    public static String df() throws SystemCommandException {
        try {
            InputStream inputStream = Runtime.getRuntime().exec("df").getInputStream();
            return parse(inputStream);
        } catch (IOException e) {
            throw new SystemCommandException(e);
        }
    }
}
