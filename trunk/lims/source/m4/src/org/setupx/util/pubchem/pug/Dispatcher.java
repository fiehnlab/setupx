package org.setupx.util.pubchem.pug;

import java.io.File;

/**
 * Example of how to use the {@link PowerUserGatewayRequest}
<hr>
<a rel="license" href="http://creativecommons.org/licenses/by/3.0/us/">
<img alt="Creative Commons License" style="border-width:0" src="http://creativecommons.org/images/public/somerights20.png" />
</a>
<br />This work is licensed under a 
<a rel="license" href="http://creativecommons.org/licenses/by/3.0/us/">Creative Commons Attribution 3.0 United States License</a>.
<hr> 
 * @author <a href=mailto:mscholz@ucdavis.edu>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.0 $
 */
public class Dispatcher {

    public static void main(String[] args) throws Exception {

        // PubChem ids requested from the Power User Gateway
        int[] pctIDs = new int[]{1,2}; //,3,4,5,6,7,8,9,234,234,123,1,4,135,2435,234,62,63,65356};

        PowerUserGatewayRequest request = new PowerUserGatewayRequest(pctIDs);
        request.submitInitalRequest();

        while(!request.getStatus().isFinished()){
            // looping and waiting
            System.out.println("waiting for " + request.getRequestID());
            request.refresh();
        }

        // download it to your directory
        // the target File where the data will be stored.
        File targetFile = new File("\\c:\\pug.sdf.gz"); //new File("\\mnt\\storage\\x");
        PowerUserGatewayRequest.store(request.getResponseURL(), targetFile);
    }
}
