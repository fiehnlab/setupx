cd /open_sx
rm source/m4/testresult -r
rm source/m4/apidoc -r
rm source/m4/build -r
rm source/m4/dist -r
filename=open_sx-`date +"%Y%m%d"`-`hostname`
echo $filename;
tar -cvf /mnt/metabolomics/Scholz/server/setupx/$filename.tar .