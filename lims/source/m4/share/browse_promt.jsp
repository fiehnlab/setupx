<%@ include file="checknonlogin.jsp"%>

<%@page
	import="org.setupx.repository.core.communication.technology.platform.Datafile"%>
<%@page
	import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>



<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<%    
	long id = Long.parseLong(request.getParameter("id"));
%>


<!--  head containing logo and description -->

	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2><%=new SXQuery().findPromtTitleByPromtID(id) %></h2>
					Experiment <%=id%><br/>
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>

	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} %>

	<%
    // requires one hashtable 
    Hashtable roots = new Hashtable();

    Iterator classIDs = new SXQuery().findClazzIDsByPromtID(id)
            .iterator();

    Hashtable classes = new Hashtable();
    while (classIDs.hasNext()) {
        long classID = Long.parseLong(classIDs.next().toString());
        Hashtable samples = new Hashtable();
        
        Iterator sampleIter = new SXQuery().findSampleIDsByClazzID(
                (int) classID).iterator();

        while (sampleIter.hasNext()) {
            long sampleID = Long.parseLong(sampleIter.next() + "");

            Hashtable datafileHashtable = new Hashtable();
            Iterator technoFileIter = TechnologyProvider
                    .getDatafiles(sampleID).iterator();
            while (technoFileIter.hasNext()) {
                Datafile datafile = (Datafile) technoFileIter.next();
                if (datafile.getSourceFile().exists()){
                	datafileHashtable.put(
                        "<a href=\"techno_download.jsp?t=file&id=" + datafile.getUOID() + "&files=15\">"
                               +
                        datafile.getTechnologyFileType().getTechnology().getLabel()
                        + "-" +
                        datafile.getTechnologyFileType().getTechnology().getLabel()
						+ "-" +
                        datafile.getSourceFile().getName()
                        + "</a>"
                        ,
                        new Hashtable());
                } else {
                	datafileHashtable.put(
                            datafile.getTechnologyFileType().getTechnology().getLabel()
                            + "-" +
                            datafile.getTechnologyFileType().getTechnology().getLabel()
    						+ "-" +
                            datafile.getSourceFile().getName()
                            + " (missing)"
                            ,
                            new Hashtable());
                }
                
            }
            Logger.log(this, "adding " + sampleID + " - datafiles " + datafileHashtable.size());
            samples.put(
                    "<a href=\"sample_detail.jsp?id=" + sampleID + "\">" +
                    "sample " + sampleID +
                    "</a>", datafileHashtable);
        }
        Logger.log(this, "adding " + classID + " - samples " + samples.size());
        classes.put("class " + classID, samples);
    }
    Logger.log(this, "adding " + id + " - classes " + classes.size());
    roots.put("experiment " + id, classes);
%>
	

<table align="center" width="60%" cellpadding="5">	
	<!--  back to the menu -->
	<tr>
		<td>

		<%@ include file="incl_orga_chart.jsp"%>
		</td>
	</tr>
</table>






<%@ include file="footer.jsp"%>
