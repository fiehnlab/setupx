<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.List"%>

<%@ include file="checklogin.jsp"%>
<%@ include file="header_standard.jsp"%>

<%
String question_orig = request.getParameter("question_orig");
String question_new = request.getParameter("question_new");

String hqlUpdate = "update FormObject c set c.question = :question where c.question = :original";

int updatedEntities = CoreObject
							.createSession()
							.createQuery(hqlUpdate)
							.setString("question", question_new)
							.setString("original", question_orig)
							.executeUpdate();
%>
<meta http-equiv="refresh" content="0; URL=curator.jsp">
