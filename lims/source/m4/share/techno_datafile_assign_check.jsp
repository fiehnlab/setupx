<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net">
<jsp:directive.page import="org.setupx.repository.core.util.logging.Logger"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"/>
<jsp:directive.page import="java.io.File"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Technology"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"/>
  <jsp:directive.page contentType="text/html; charset=UTF-8" />
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.hibernate.Hibernate"/>
<jsp:directive.page import="org.setupx.repository.core.CoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.query.QueryMaster"/>
<jsp:directive.page import="org.setupx.repository.server.persistence.SXQuery"/>
<jsp:directive.page import="java.util.Vector"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>


  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>

	<jsp:scriptlet>
//		Logger.log(this, "parameternames: " + request.getParameterNames());
	
		File file = new File(request.getParameter("file"));
		long technofiletypeID = Long.parseLong(request.getParameter("technofiletype"));
		TechnologyFileType technologyFileTypeParent = (TechnologyFileType)TechnologyFileType.persistence_loadByID(TechnologyFileType.class, technofiletypeID);
	</jsp:scriptlet>


		<jsp:include page="techno_header.jsp"></jsp:include>

	<h2>
		<a href="techno_techno.jsp">all platforms</a> - 
		assigning 
		<jsp:expression>file.getName()</jsp:expression> to 
		<jsp:expression>technologyFileTypeParent.getLabel()</jsp:expression> 
		
	</h2>

	Please select the technology/filetype and the sample ID that you want to add the datafile to. 
	<br/>


<form action="techno_datafile_assign_process.jsp">
	
	<![CDATA[<input type="text" readonly="readonly" name="path" value=]]>"<jsp:expression>file.getPath()</jsp:expression>"<![CDATA[/>]]>
	<select name="tid">
	<jsp:scriptlet>
		List list =	TechnoCoreObject.createSession().createSQLQuery("select technologyfiletype.* from technologyfiletype join technology on technology.uoid = technologyfiletype.technology").addEntity(TechnologyFileType.class).list();
		Logger.log(this, "number of items found: "  + list.size());
		Iterator iterator = list.iterator();
		String selected = "";
        while (iterator.hasNext()) {
            TechnologyFileType technologyFileType = (TechnologyFileType) iterator.next();
            if (technologyFileType.getUOID() == technofiletypeID) {
                selected = "SELECTED";
            } else {
                selected = "";
            }
	</jsp:scriptlet>
		<![CDATA[<option value=]]>"<jsp:expression>technologyFileType.getUOID()</jsp:expression>" <jsp:expression>selected</jsp:expression><![CDATA[>]]>
		<jsp:expression>technologyFileType.getTechnology().getLabel()</jsp:expression> - <jsp:expression>technologyFileType.getLabel()</jsp:expression>
		<![CDATA[</option>]]>

	<jsp:scriptlet>
        }
	</jsp:scriptlet>
	</select> <input type="text" name="id"/>  <input type="submit" value="assign datafile"/>	
</form>
</jsp:root>