<%@page import="org.setupx.repository.web.forms.inputfield.multi.SopRefField"%>
<%@page import="org.setupx.repository.web.forms.PromtCreator"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.SOPMultiField"%>
<%@page import="org.setupx.repository.web.forms.FormObject"%>
<%@page import="org.setupx.repository.web.WebConstants"%>


<%@ include file="checklogin.jsp"%>
<%

	
	// take an sop ID and add or remove it from a promt

	long sopID = Long.parseLong(request.getParameter("refID"));
	short action = Short.parseShort(request.getParameter("action"));

	// the actual experiment received from the session
	Promt promt = (Promt)session.getAttribute(WebConstants.SESS_FORM_PROMT);
	
	if (action == 1){
	    // adding
		promt.addSOP(new SopRefField(sopID));
	} else if (action == 2){
	    // remove
	    SopRefField sopRefField = promt.findSOPMultifield().getSopRefField(sopID);
	    promt.findSOPMultifield().remove(sopRefField);
	}
%>


<meta http-equiv="refresh" content="0; URL=sop_form_select.jsp">
