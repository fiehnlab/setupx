<%@ include file="header.jsp"%>

<%@ include file="header_query.jsp"%>

<%@ page import="org.setupx.repository.core.document.*"%>
<%@ page import="org.setupx.repository.core.*"%>


<hr/>
<% XMLabstractDocument[] documents = (XMLabstractDocument[])session.getAttribute("documents");
int pos = 0;
try {
pos = Integer.parseInt(request.getParameter("pos"));
} catch (Exception e){
}


// decide if anything was found
if (documents.length!=0) {

//                            showing the document
// ````````````````````````````````````````````````````````````````````````````````````````````

	XMLabstractDocument doc = documents[pos];	%>
	
	
	
	
	pos : <%=pos%> size: <%=documents.length-1%>
	
	
	<h2 id="document" class="tableheader1" align="center"><%=doc.getTypeTagName()%> &nbsp<%=pos+1%>/<%=documents.length%></h2>
	
	
	<table border="1" align="center" width="80%">
	<tr>
	<td>current document in the session is a </td><td><%=doc.getTypeTagName()%> </td>
	</tr>
	<tr>
	<td>number of documents found:</td><td>
	<% if(pos !=0){ %><a href="result.jsp?pos=<%=pos-1%>">prev</a><% }  else {%>prev<%}%>
	<%=pos+1%>/<%=documents.length%>
	<% if(pos !=documents.length-1){ %><a href="result.jsp?pos=<%=pos+1%>">next</a><% }  else {%>next<%}%>
	</td>
	</tr>
	<tr>
	<td>stylesheet</td><td><%=doc.getStylesheet(doc.getType())%></td>
	</tr>
	<% if (doc instanceof DocumentExperiment){%> 
	<tr><td>status</td><td><b><%=((DocumentExperiment)doc).getStatusDescription()%></b></td></tr>
	<%}%>
	<tr><td>link:</td><td><a href="#document">document</a></td></tr>
	<tr><td></td><td>	<a href="#visibility">modify Visibility</a></td></tr>
	</table>
	
	<hr/>
	<h2 class="tableheader1" align="center">content</h2>
	<%=doc.getContentHTML() %>
	<hr/>


<%@include file="document.jsp"%>


<% if (doc instanceof DocumentExperiment){%>

<%@include file="acquisition.jsp"%>

<%@include file="cal.jsp"%>

<%@include file="visibility.jsp"%>

<%}%>
<h2 class="tableheader1" align="center">Debugging</h2>
Class: <a href="apidoc/<%=doc.getAPIlink()%>.html"><%=doc.getClass()%></a>

<table>
	<tr>
		<td>apilink</td>
		<td><%=doc.getAPIlink()%></td>
	</tr>
	<tr>
		<td>label</td>
		<td><%=doc.getLabel()%></td>
	</tr>
	<tr>
		<td>system ID</td>
		<td><%=doc.getSystemID()%></td>
	</tr>
	<tr>
		<td>type</td>
		<td><%=doc.getType()%></td>
	</tr>
	<tr>
		<td>content</td>
		<td><%=doc.getContent()%></td>
	</tr>
</table>
<%} else {



//                            nothing was found
// ````````````````````````````````````````````````````````````````````````````````````````````%>
	<h2 class="tableheader1" align="center">NOTHING FOUND</h2>

<p align="center">
No, there is nothing for the given parameters.... <br/>
Try again
</p>
<%}%>
<%@ include file="footer.jsp"%>
