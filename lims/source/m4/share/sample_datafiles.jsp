<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.core.communication.technology.platform.Datafile"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.List"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<%
Long sampleID = Long.parseLong(request.getParameter("id"));

// get all datafiles for this sample

List list = Datafile.findDataFilesBySampleID(sampleID);

Iterator datafiles = list.iterator();


%>

	<h3>
		<a href="techplatform.jsp">all platforms</a>
		> 
		all <%=list.size() %> datafiles for sample <%=sampleID %> 
	</h3>

<%

while(datafiles.hasNext()){
    Datafile datafile = (Datafile)datafiles.next();
	%>
	<%=datafile.getSourceFile().getName() %>
	<%=datafile.getTechnologyFileType().getLabel()%>
	<%=datafile.getTechnologyFileType().getTechnology().getLabel()%>
	<%
}
%>


</body>
</html>