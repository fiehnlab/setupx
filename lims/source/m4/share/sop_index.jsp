<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net">
<jsp:directive.page import="org.setupx.repository.core.communication.document.DocumentCategory"/>
<jsp:directive.page import="java.io.File"/>
<jsp:directive.page import="java.util.Locale"/>
<jsp:directive.page import="java.text.DateFormat"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.setupx.repository.core.CoreObject"/>
<jsp:directive.page import="org.hibernate.Session"/>
<jsp:directive.page import="org.setupx.repository.core.communication.document.DocumentObject"/>
<jsp:directive.page import="org.setupx.repository.server.persistence.SXQuery"/>

<jsp:directive.page contentType="text/html; charset=UTF-8" />

<jsp:include page="checklogin.jsp"></jsp:include>

  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
  
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>

  <link rel="stylesheet" href="print.css" type="text/css" media="print" />
  
  </head>

	<jsp:scriptlet>
		Session s = CoreObject.createSession();
		String catName = "";
		
		// checking if there is a category given or not
		try {
			long catID = Long.parseLong("" + request.getParameter("catid"));
			
		 	catName = " for Category " + ((DocumentCategory)DocumentCategory.persistence_loadByID(DocumentCategory.class, catID)).getLabel();
			
			String query = "select * from document where category = " + catID;
			List l = s.createSQLQuery(query).addEntity(DocumentObject.class).list();
			request.setAttribute( "test" , l); 
		} catch (Exception e){
			List l = new SXQuery(s).persistence_loadAll(DocumentObject.class, s);
			request.setAttribute( "test" , l); 
		}
		DateFormat dFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US);	
	</jsp:scriptlet>

  <body>
	<jsp:include page="sop_header.jsp"></jsp:include>
 
 	<h2>
		<a href="sop_index.jsp"><img src="pics/sop.gif" border="0"/> Standard Operation Procedures <jsp:expression>catName</jsp:expression></a>
	</h2>
  
	<display:table name="test" id="row">
		<display:column title="SOP name" 	property="label" href="sop_detail.jsp?" paramId="id" paramProperty="UOID" sortable="true"></display:column>
		<display:column title="Version" 	property="version" sortable="true"></display:column>
	    <display:column title="Category" sortable="true">
	    
	    
   			<![CDATA[<a href="sop_index.jsp?catid=]]>
				<jsp:expression>((DocumentObject)row).getCategory().getUOID()</jsp:expression>				
			<![CDATA[">]]>    
		    	<jsp:expression>((DocumentObject)row).getCategory().getLabel()</jsp:expression>
			<![CDATA[</a>]]>
			
	    </display:column>
		<display:column title="issued"	property="dateIssued"  sortable="true"></display:column>
		<display:column title="valid (start)"	sortable="true"><jsp:expression>dFormat.format(((DocumentObject)row).getDateValidStart())</jsp:expression></display:column>
		<display:column title="valid (end)" 	sortable="true"><jsp:expression>dFormat.format(((DocumentObject)row).getDateValidEnd())</jsp:expression></display:column>
		<display:column title="active" sortable="true">
	    	<jsp:scriptlet>
	    		String img = "";
	    		if (((DocumentObject)row).isActive()){
	    			img = "pics/check.gif";
	    		} else {
	    			img = "pics/circle.gif";
	    		}
	    	</jsp:scriptlet>
			<![CDATA[<img src=]]>"<jsp:expression>img</jsp:expression>"<![CDATA[/>]]>
		</display:column>
		<display:column title="public" sortable="true">
			    	<jsp:scriptlet>
	    		String img = "";
	    		DocumentObject documentObject = ((DocumentObject)row);
	    		if (documentObject.getCategory().isPublic()) {
		    		if (documentObject.isPubliclyAvailable()) {
		    			img = "pics/check.gif";
		    		} else {
		    			img = "pics/circle.gif";
		    		}
	    		} else {
	    			img = "pics/x.gif";
	    		}
	    	</jsp:scriptlet>

			<![CDATA[<a href="sop_alter.jsp?type=1&id=]]>
			<jsp:expression>((DocumentObject)row).getUOID()</jsp:expression>				
			<![CDATA[">]]>
				<![CDATA[<img src=]]>"<jsp:expression>img</jsp:expression>"<![CDATA[/>]]>
			<![CDATA[</a>]]>
		</display:column>
		<display:column title="Attachment" sortable="false">
	    	<jsp:scriptlet>
	    		String img = "";
	    		String link = "";
	    		
	    		if (((DocumentObject)row).getFilePath().length() > 0){
		    		if (new File(((DocumentObject)row).getFilePath()).exists()){
		    			img = "pics/icon_download.gif";
		    			link = "sop_download.jsp?sopID=" + ((DocumentObject)row).getUOID();
		    		} else {
		    			img = "pics/warning.gif";
		    			link = "";
		    		}
	    		} else {
	    			img = "pics/Blank.gif	";
	    			link = "";
	    		}
	    	</jsp:scriptlet>
			<![CDATA[<a href="]]><jsp:expression>link</jsp:expression><![CDATA["><img border="0" src=]]>"<jsp:expression>img</jsp:expression>"<![CDATA[/></a>]]>
		</display:column>
	</display:table>
</body>
	<jsp:scriptlet>
	s.close();
	</jsp:scriptlet>
</jsp:root>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	