<jsp:root version="1.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net"  xmlns:c="urn:jsptld:http://java.sun.com/jstl/core">
<jsp:directive.page import="org.setupx.repository.web.pub.data.cache.CacheObject"/>
<jsp:directive.page import="org.setupx.repository.web.WebAccess"/>
<jsp:directive.page import="org.setupx.repository.core.communication.importing.logfile.ScannedPair"/>
<jsp:directive.page import="org.setupx.repository.web.forms.inputfield.multi.Promt"/>
<jsp:directive.page import="org.setupx.repository.web.forms.FormObject"/>
<jsp:directive.page import="org.exolab.castor.xml.schema.Form"/>
<jsp:directive.page import="org.hibernate.Session"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.hibernate.Hibernate"/>
<jsp:directive.page import="org.setupx.repository.core.CoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.query.QueryMaster"/>
<jsp:directive.page import="org.setupx.repository.server.persistence.SXQuery"/>
<jsp:directive.page import="java.util.Vector"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
    <title>SetupX</title>
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>

<h2>header</h2>

<jsp:scriptlet>

	Session s = CoreObject.createSession();
	Class c = UserDO.class;
	
	List l = CoreObject.persistence_loadAll(c, s);

	request.setAttribute("test", l); 

	// style
	pageContext.setAttribute("tableclass", "its");

	</jsp:scriptlet>





<display:table name="test" defaultsort="1" defaultorder="descending" class="${tableclass}">
    <display:column property="display" />
    <display:column title="static value">static</display:column>
    <display:column title="row number (testit_rowNum)">
      <c:out value="${testid.abbreviation}"/>
    </display:column>
</display:table>

</jsp:root>

