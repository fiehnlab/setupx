<%@page import="java.util.HashSet"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Technology"%>
<%@page	import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"%>
<%@page import="java.util.Iterator"%>
<%@page	import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page	import="org.setupx.repository.core.communication.document.sop.StandardOperationProcedure"%>
<%@page	import="org.setupx.repository.core.communication.document.DocumentObject"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page	import="org.setupx.repository.core.communication.document.DocumentCategory"%>

<jsp:include page="sop_header.jsp"></jsp:include>

<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<style type="text/css" media="all">
@import url("maven-base.css");

@import url("maven-theme.css");

@import url("site.css");

@import url("screen.css");
</style>
<%@ include file="checknonlogin.jsp"%>

<link rel="stylesheet" href="print.css" type="text/css" media="print" />

<%
	Hashtable roots = new Hashtable();

	// get all SOPs that do not replace any older ones
	Iterator iterator = CoreObject.createSession().createSQLQuery("select * from documentcategory").addEntity(DocumentCategory.class).list().iterator();
	
	while(iterator.hasNext()){
	    DocumentCategory category = (DocumentCategory)iterator.next();
	    if (user.isPublic()){
	        if (category.isPublic()){
	    		add(user, roots, category);
	        }
	    } else {
			add(user, roots, category);
	    }
	}
	%>



<h2><a href="sop_index.jsp">all Standard Operation Procedures</a></h2>
Below you can see all SOPs in the repository. SOPs that are valid and
currently in use are tagged with a green note. While all SOPs that are
outdated or that are not valid yet are marked with a red tag.
<br>
The date on top of each SOP shows the
<b>first date that the SOP is valid</b>
and the bottom date shows the
<b>expiration date of the SOP</b>
.
<p>If an SOP replaces an existing SOP the newer one will be placed
below the old SOP. Within a line of SOPs for a certain case / purpose
one <b>one</b> SOP is active.
<table>
	<tr>
		<td><%@ include file="incl_orga_chart.jsp"%>
		</td>
	</tr>
</table>



<%! 

		public static DateFormat dFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US);	

public void add(UserDO user, Hashtable hashtable, DocumentCategory documentCategory){
    Hashtable childLevel = new Hashtable();

    // find the "youngest" of the documents assigned to this id 
    Iterator iterator = CoreObject.createSession().createSQLQuery("select * from document where replcesDocument is Null and category = " + documentCategory.getUOID()).addEntity(DocumentObject.class).list().iterator();
	
	while (iterator.hasNext()){
	    DocumentObject documentObject = (DocumentObject)iterator.next();
		add(user, childLevel, documentObject);
	}

	

    String name = ""; 
        
    name = name.concat("<font style=\"font-size: xxsmall\"><nobr>Category</nobr></font><br>");
    
    name = name.concat("<a href=\"sop_index.jsp?catid=" + documentCategory.getUOID() + "\"><font style=\"font-size: large\"><nobr>" + documentCategory.getLabel() + "</nobr></font><br>");

    if (documentCategory.isPublic()) name = name.concat("<br><font style=\"font-size: small\"><i>PUBLIC</i></font>");
    name = name.concat("<br><font style=\"font-size: small\">" + documentCategory.getDescr() + "</font>");
    name = name.concat("<a name=" + documentCategory.getUOID() + "/>");

    hashtable.put(name , childLevel);
}

public void add(UserDO user, Hashtable hashtable, DocumentObject documentObject){
    Hashtable childLevel = new Hashtable();
    try{
	        DocumentObject child = documentObject.isReplacedBy();
			Logger.log(this, "found: child " + child.getLabel());
			add(user, childLevel, child);
    } catch (Exception e){
        e.printStackTrace();
    }

    String name = "<nobr>" + dFormat.format(documentObject.getDateValidStart()) + "</nobr><br>"; 

    if (user.isPublic()){
        if (documentObject.isPubliclyAvailable()){
            name = name.concat("<a href=\"sop_detail.jsp?id=" + documentObject.getUOID() + "\"><font style=\"font-size: large\"><nobr>" + documentObject.getLabel() + "</nobr></font></a><br>");
        } else {
            name = name.concat("<font style=\"font-size: large\"><nobr><i>-not public-</i></nobr></font><br>");
        }
    } else {
        name = name.concat("<a href=\"sop_detail.jsp?id=" + documentObject.getUOID() + "\"><font style=\"font-size: large\"><nobr>" + documentObject.getLabel() + "</nobr></font></a><br>");
    }
    
    if (documentObject.isActive()){
	    name = name.concat("<font style=\"background-color: green;\">");
	    name = name.concat("<font style=\"font-size: small\">ACTIVE</font></font>");
    } else {
    	name = name.concat("<font style=\"background-color: red;\">");
        name = name.concat("<font style=\"font-size: small\">inactive</font></font>");
    }
    if (documentObject.isPubliclyAvailable()) name = name.concat("<br><font style=\"font-size: small\"><i>PUBLIC</i></font>");
    name = name.concat("<br><font style=\"font-size: small\">" + documentObject.getComment() + "</font>");
    name = name.concat("<br><nobr>" + dFormat.format(documentObject.getDateValidEnd()) + "</nobr>");
    name = name.concat("<a name=" + documentObject.getUOID() + "/>");

    hashtable.put(name , childLevel);
}
%>

