<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Enumeration"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<%@include file="incl_xls_header.jsp"%>


<form method="post">



<table align='center' border='0' cellpadding='4' width="60%" border = 1 >
<tr>	
	<th colspan="21">Edit data
<tr>	
	<td colspan="21">
		Before the data is being assigned to the samples you can modify the values.

<% 
	Vector rows = new Vector();
	Vector labels = new Vector();
	rows = (Vector)session.getAttribute("data"); 
    labels = (Vector)session.getAttribute("labels"); 

	// update values
	Enumeration names = request.getParameterNames();
	while (names.hasMoreElements()){
	    try {
		    String name = "" + names.nextElement();
		    String value = request.getParameter(name);
		    // cut values in half
		    int posX = name.indexOf("X");
		    int row = Integer.parseInt(name.substring(0, posX));
		    int col = Integer.parseInt(name.substring(posX + 1));
			
			// get correct cell and update it
			((Vector)rows.get(row)).setElementAt(value,col);
			
	    } catch (Exception e){
	    }
	}
    
    // find the max length of the vectors
	int maxRow = 0;
	for(int i = 0; i < rows.size(); i++){
	    try {
		    if (((Vector)rows.get(i)).size() > maxRow) maxRow = ((Vector)rows.get(i)).size() ;
	    } catch (Exception e){
	        // some dont have that many elements in a row
	    }
	}
	%>

	<tr>
		<%
		for(int i = 0; i < labels.size(); i++){
			String label = "" + labels.get(i);
			if (label.compareTo("null")==0) label = "";
			%>
	        <th width="2" align="center">
	        	<%=label%>
	   	    </th>
		<%}%>
	</tr>
    <%
	Iterator rowIterator = rows.iterator();
      	int rowCounter = -1;
      	while(rowIterator.hasNext()){
      	    rowCounter++;
      	    Vector row = (Vector)rowIterator.next();
              %>
	<tr>
        </td>
        	<% for (int i = 0; i < row.size(); i++){
        	   %> 
		        <td align="center">
        			<input type="text" size="8" name="<%=rowCounter %>X<%=i%>" value="<%=row.get(i)%>" onBlur="document.forms[0].submit()">
	    	    </td>
        	   <%  
        	}%>
       	</td>
	</tr>
	<% } // row iterator %>
</table>



</form>

<%@include file="incl_xls_footer.jsp"%>

</body>
</html>