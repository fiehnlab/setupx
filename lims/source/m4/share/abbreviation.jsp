<%@ page contentType="text/html" %>
<%@ page import="org.setupx.repository.web.WebConstants"%>
<%@ page import="org.setupx.repository.web.QueryServlet"%>
<%@ page import="org.setupx.repository.web.forms.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.multi.*"%>
<%@ page import="org.setupx.repository.web.forms.clazzes.*"%>
<%@ page import="org.setupx.repository.web.forms.page.*"%>
<%@ page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@ page import="org.setupx.repository.core.communication.exporting.*"%>
<%@ page import="org.setupx.repository.core.communication.ncbi.local.*"%>
<%@ page import="org.setupx.repository.core.communication.ncbi.*"%>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<%@ include file="checklogin.jsp"%>
<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%></h2>
			Abbriviations<br/>
			<font size="-2">Abbriviations commenly used to define Species that are <strong>NOT</strong> defined as a synonym in the NCBI Taxonomy.</font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<%@ include file="navi_admin.jsp"%>


	<table align="center" width="60%" cellpadding="7">
	<tr>
		<td colspan="15"></td>
	</tr>
    	<tr>
    		<th align="center"></td>
    		<th align="center" colspan="14">NCBI Abbriviations
		</tr>
		<tr>
			<td colspan="15" align="center" width="60%">
				<table border="0" width="100%">


<%

org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();


// delete entries
try {
    long id = Integer.parseInt("" + request.getParameter("id"));
    org.setupx.repository.core.CoreObject.persistence_loadByID(NCBIAbbreviationMapping.class, hqlSession, id).remove();
} catch (Exception e){
    e.printStackTrace();
}


//new entry
try {
    String abbreviation= ("" + request.getParameter("abbreviation"));
    int ncbiid = Integer.parseInt("" + request.getParameter("ncbiid"));
	NCBIConnector.determineNCBI_Information(ncbiid);
    new NCBIAbbreviationMapping(abbreviation,ncbiid); 
    new org.setupx.repository.server.logging.Message(null, "NCBIAbbreviation (" + abbreviation + ")has been created.").update(true);
}catch (NCBIFindException findException){
    %>



    
    				<tr>
						<td ></th>
						<td colspan="6" ><font class="error"><strong>Error:</strong> Abbreviation can not be created.</td>
						<td ></th>
					<tr>
    				<tr>
						<td ></th>
						<td colspan="6" ><font class="error"><%=findException.getMessage() %></font></td>
						<td ></th>
					<tr>
    
    <% 
}catch (Exception e){
    e.printStackTrace();
} 

%>


					<tr>
						<td ></td>
						<th ><strong>Abbriviation</strong></th>
						<th ><strong>NCBI ID</strong></th>
						<th >NCBI Name</th>
						<th >Classification</th>
						<th >Childs</th>
						<th ></th>
						<td ></td>
					<tr>
	

				<!--  get all existing abbriviations -->
				<% 
				java.util.Iterator iterator = NCBIAbbreviationMapping.persistence_loadAll(NCBIAbbreviationMapping.class, hqlSession).iterator();  
			    NCBIAbbreviationMapping mapping = null;
			    int col = 1;
				while (iterator.hasNext()){
					mapping = (	NCBIAbbreviationMapping )iterator.next();
					NCBIEntry entry =  NCBIConnector.determineNCBI_Information(mapping.getNcbiID());
					
				    col = col * -1;
				    String bgcolor = "style=\"background-color:#FFFFFF\"";
				    if (col > 0) bgcolor = "";
				    if ((entry.isSpecies() && entry.getSpeciesType() == NCBIEntry.UNKNOWN)) bgcolor = "style=\"background-color:#FF545A\"";

					%>
					<tr>
						<td></td>
						<td <%=bgcolor %> align="center"><i><%=mapping.getAbbreviation() %></i></td>
						<td <%=bgcolor %> align="center"><%=mapping.getNcbiID() %></td>
						<td <%=bgcolor %> align="center"><a href="status_info.jsp#<%=entry.getTaxID()%>"><%=entry.getName() %></td>
						<td <%=bgcolor %> align="center"><%=entry.getSpeciesTypeName()%></td>
						<td <%=bgcolor %> align="center"><%=entry.getChilds().size()%></td>
					    <td <%=bgcolor %> ><a href="?id=<%=mapping.getUOID()%>"><img  border='0' src="pics/trash.gif"></a></td>
						<td></td>
					</tr>
					<% 	
				}
				%>
					<tr>
						<td ></td>
						<th ><strong>create new Abbreviation</strong></th>
						<th >NCBI ID</th>
						<th ></th>
						<th ></th>
						<th ></th>
						<th ></th>
						<td ></td>
					<tr>
				<form action="" method="post">
					<tr>
						<td></td>
						<td align="center"><input type="text" name="abbreviation" width="10"></td>
						<td align="center"><input type="text" name="ncbiid" width="10"></td>
						<td  align="center" colspan="2">create a new entry</td>
						<td colspan="2"><input type="submit" value="create"></td>
						<td colspan="2"></td>
					</tr>
				</form>
					<tr>
						<td ></td>
						<th ><strong>Link</strong></th>
						<th ></th>
						<th ></th>
						<th ></th>
						<th ></th>
						<th ></th>
						<td ></td>
					<tr>
					<tr>
						<td></td>
						<td><strong>NCBI 1stCache </strong></td>
						<td>NCBI first level cache. Lookup NCBI IDs</td>
						<td colspan="2"><a href="."></a></td>
						<td colspan="2"><a href="status_info.jsp"><img  border='0' src="pics/go.gif"></a></a></td>
						<td colspan="2"></td>
					</tr>
				
				
				
			</td>
		</tr>
	</table>
	</table>
	




<%@ include file="../footer.jsp"%>
