<%@ include file="checklogin.jsp"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.Settings"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.core.util.Sort"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.web.forms.InsufficientUserRightException"%>
<%@page import="java.util.Set"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>


<%
	// check user
	if (!user.isAdmin()){
	    throw new InsufficientUserRightException();
	}




	// check incoming parameters
	Enumeration names = request.getParameterNames();
	while (names.hasMoreElements()){
	    String _name = "" + names.nextElement();
		String _value = request.getParameter(_name);	

		String _recentValue = "" + Settings.getValue(_name);
		
		if (_recentValue.compareTo(_value) != 0){
		    Logger.log(this, "changing " + _name + " from \"" + _recentValue + "\" to \"" + _value + "\"");
		    %>
			<table align="center">
		    <th>
		    <%="changing " + _name + " " + _recentValue + " " + _value %><br>
		    </table>
		    <%
			Settings.setValue(_name, _value);
		}
//		Settings.refresh();
	}
%>
<table align="center">
	<form target="." method="post">
	<tr>
		<th>Key
		<th>Value

<%
	// List of all settings 
	
	Enumeration keys = Settings.keys();

	List list = Collections.list(keys);
	Collections.sort(list);

	Iterator iterator = list.iterator();
	
	while (iterator.hasNext()){
	    
	    String key = iterator.next() + "";
	    String value = Settings.getValue(key);

//	    Settings.setValue(key, value);
	    %>
	
		<tr>
			<td><%=key %>
			<td><input type="text" size="40" name="<%=key %>" value="<%=value %>">
	    <%
	}
%>
	<tr>
		<td>
			change settings
		<td><input type="submit">

	</form>
</table>


</body>
</html>