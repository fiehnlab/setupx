<%@page import="org.setupx.repository.Config"%>
<%@page import="java.util.Iterator"%>
<%
//  JSP creating a JSON response for ajax requests
%>

<%@ include file="checknonlogin.jsp"%>
<%@page import="org.setupx.repository.core.query.QueryMasterAnswer"%>
<%@page import="org.setupx.repository.core.query.QueryMaster"%>
<%@page import="org.hibernate.criterion.Expression"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.setupx.repository.web.pub.data.cache.VariationsCache"%>
<%@page import="java.util.Vector"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%
boolean cachedQueries = org.setupx.repository.Config.QUERY_CACHING_ACTIVE_JSP;

try {
	String cache = request.getParameter("cache");
    if (cache.compareTo("false") == 0){
        cachedQueries = false;
    }
} catch (Exception e){
    
}



	//Session s = CoreObject.createSession();
	Enumeration e = request.getParameterNames();
	int y = 0;
	while (e.hasMoreElements()) {
	    y++;
	    Logger.debug(this, "Name of Param " + y + " : "
	            + (String) e.nextElement());
	}
	Logger.debug(this, "Total Parameters : " + y);

	//Session s = CoreObject.createSession();
	
	Logger.log(this, "start getting parameters");
	String searchTerm = request.getParameter("input").toString();
	Logger.log(this, "term: " + searchTerm );
	String queryType = request.getParameter("queryID").toString();
	Logger.log(this, "queryID: " + queryType);
	
	String query = "select value from formobject where discriminator like \"%InputField\" and value like \"%" + searchTerm + "%\" and question = \"" + queryType + "\" group by value";

	Logger.log(this, query);
	
	List values = CoreObject.createSession().createSQLQuery(query).addScalar("value", org.hibernate.Hibernate.STRING).setCacheable(cachedQueries).list();

	Logger.log(this, "results: "  + values.size());
%>


{ results: [
<% 
	Iterator iterator = values.iterator();
	int i  = 0;
	while (iterator.hasNext()){
	    String value = "" + 	iterator.next();
	    Logger.log(this, i + ": " + value);
		%>
			{ id: "<%=i%>", value: "<%=value%>", info: "<%=value%>" },
		<%
	    i++;
		} %>
] }