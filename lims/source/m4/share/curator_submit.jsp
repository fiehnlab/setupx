<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.List"%>

<%@ include file="checklogin.jsp"%>
<%@ include file="header_standard.jsp"%>

<%
String question = request.getParameter("question");
String original = request.getParameter("original");
String update = request.getParameter("update");

String hqlUpdate = "update FormObject c set c.value = :value where c.question = :question and c.value = :original";

int updatedEntities = CoreObject.createSession().createQuery(hqlUpdate).setString("value", update).setString("question", question).setString("original", original).executeUpdate();
%>
	    <meta http-equiv="refresh" content="0; URL=curator_values.jsp?question=<%=question%>#<%=update%>">
