<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Vector"%>
<%
	Enumeration paramNames = request.getParameterNames();

    while (paramNames.hasMoreElements()) {
        String paramName = paramNames.nextElement().toString();
        if (paramName.startsWith("field")){
            // got a value in the queryform that needs to be updated
            String value = request.getParameter(paramName);
            
            
            // update this in the vector containing all the settings for the query
			Vector criteraVector = new Vector();
		    criteraVector = (Vector) session.getAttribute("restricitons");
		    if (criteraVector == null) {
		        criteraVector = new Vector();
		    } else {
			    // find position
			    int posInVector = Integer.parseInt(paramName.substring(paramName.indexOf('_') + 1, paramName.lastIndexOf('_')));
			    int posInArray = Integer.parseInt(paramName.substring(paramName.lastIndexOf('_') + 1, paramName.length() ));
			    Logger.log(this, "found in " + paramName + ": " + posInVector + " and " + posInArray);		        
			    String[] strings = (String[])criteraVector.get(posInVector);			    
			    strings[posInArray] = value;
		    }
		}
    }
%>
<jsp:forward page="pubsample.jsp"/>
	    