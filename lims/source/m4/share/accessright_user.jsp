<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.communication.importing.metadata.*,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ include file="incl_setaccessright.jsp"%>
<%
//list of all experiments
List promtIDs = new SXQuery().findPromtIDs();
int userID = Integer.parseInt("" + request.getParameter("uid")); 

%>

	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%> Administration</h2>
			Control Access to Experiment for User <%=userID %><br/>
			<font size="-2">Define which Experiments the User can access.</font>
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>
	
	
	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} %>
	
	<table align="center" width="60%" >	
	<tr>
		<td colspan="2" align="center" style="background-color:#FFFFFF"><a href="main.jsp"><img  border='0' src="pics/back.gif"></a></td>	
		<th colspan="20">Experiment Access for User <%=userID %></th>
	</tr>
	
	
<%@ include file="incl_accessheader.jsp"%>

<% 

// show each experiment with the accessrestioction bar
java.util.Iterator iterator = promtIDs.iterator();
while (iterator.hasNext()){
    long promtID = Long.parseLong("" + iterator.next());
    String my_abstract = Promt.createShortAbstractHTML(promtID, 60);
    // int numberOfSamples = new SXQuery().findSampleIDsByPromtID(promtID).size();

    //System.out.println("#" + my_abstract + "#");
    %>

			
	<!--  hotfix -->
	
	<% if ((my_abstract.compareTo("<br><font class='small'>-please type here your own abstract-</font>") != 0 )
	    &&
	      (my_abstract.compareTo("-unknown-<br><font class='small'>-unknown-</font>") != 0 )) { %>

	<tr>
		<td></td>
		<td align="center">
		<%=promtID %>
		</td>
		<td>
		<%=my_abstract %> <%/* %><fond class="small"><%=numberOfSamples %> samples.<% */ %>
		</td>
		
		<!-- report / summary button -->
		<td align="center" style="background-color:#FFFFFF">
		<a href="load?id=<%=promtID %>&action=<%=PromtUserAccessRight.READ%>"><img  border='0' src="pics/go.gif"></a>
		</td>

			<td align="center" style="background-color:#FFFFFF"><a href="usr_share_exp.jsp?id=<%=promtID%>"><img  border='0' src="pics/go_share.gif" title="All users for <%=promtID%>"></a></td>

		<td>
		<%@ include file="incl_accessright.jsp"%>
		</td>
	</tr>
    <%
	}
}
%>
</table>


<%@ include file="footer.jsp"%>
