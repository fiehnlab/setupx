<%@ include file="checklogin.jsp"%>
<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.communication.leco.*,
                 org.setupx.repository.core.communication.exporting.*,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="org.apache.axis.constants.Enum"%>
<%@page import="org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>



<!--  updating related methods -->


<% 
try {
    long refresh = Integer.parseInt("" + request.getParameter("refresh"));
    if (refresh > 0){
        MachinePool.scan4NewMachineMethods();
    }
} catch (Exception e){
    e.printStackTrace();
}



try {
	org.hibernate.Session s = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();
	
	// expression missing to remove methods without an machineID
	  int lenght = s.createSQLQuery("select {method.*} from machinemethod method where method.label = \"qc standard\"").addEntity("method", Method.class).list().size();
		System.out.println("-->:" + lenght);
}catch(Exception e){
    e.printStackTrace();
}


try {
    long id = Integer.parseInt("" + request.getParameter("id"));
    long machineID = Integer.parseInt("" + request.getParameter("target"));
    int action = Integer.parseInt("" + request.getParameter("action"));
    
    System.out.println("id:  " + id + "  machine: " + machineID);

    org.hibernate.Session s = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();
	org.hibernate.Transaction t = s.beginTransaction();
	//org.hibernate.Transaction t =     s.beginTransaction();
	Machine machine1 = (Machine)Machine.persistence_loadByID(Machine.class, s, machineID);
	if (machine1 instanceof GCTOF){
	    GCTOF gctof = (GCTOF)machine1;
	    Set set = gctof.getMethods();
	    Set new_set = new HashSet();

	    if (action == 1) {
	        // remove a sample
		    java.util.Iterator meth = set.iterator();
		    while (meth.hasNext()){
				Method method = ((Method)meth.next());
				if (method.getUOID() != id){
				    new_set.add(method);
				} else {
				    method.update(s, true);
			        System.out.print("did not add method:" + method.getUOID() + " to: " + machineID);
				}
		    }
	    } else if (action == 2) {
	
			Method myMethod = (Method)Method.persistence_loadByID(Method.class,  s,id );
	        set.add(myMethod);
	        System.out.print("added method:" + myMethod.getUOID() + " to: " + machineID);
	        new_set = set;
	    }
	    gctof.setMethods(new_set);
	    gctof.update(s,true);
	    t.commit();
		s.close();
	}


//    s.createSQLQuery("update machinemethod set machine = \"" + machineID + "\" where uoid = \"" + id + "\"");
//    t.commit();
} catch (Exception e){
    e.printStackTrace();
}



%>



<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%></h2>
			Assing Methods to Machines<br/>
			<font size="-2">Below you will find the experiments that you ran in the lab, <br>that you do in collaboration with others and experiments that are available for everybody.</font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<%@ include file="navi_admin.jsp"%>

<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td colspan="15"></td>
	</tr>

	<tr>
		<td>
		<th colspan="2">Unassigned Methods<br><font class="small">These Methods have been used but not been assigned to a specific Machine</th>
		<td colspan="4"></td>
		<th colspan="2">Machines<br><font class="small">List of all machines registered for the lab right now.</th>
		<td><b></td>
	</tr>

	<tr>
		<td>
		<td colspan="2" valign="top">
			<!--  table containing all machines with there methods-->
			<table cellspacing="3" cellpadding="2" width="100%" valign="top" align="center">
				
			  <tr>
			    <th ></th>
			    <th ></th>
			    <th >Method <br><font class="small">The label that is used in the machine.</th>
			  </tr>
			<% 
			org.hibernate.Session s = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();
			// expression missing to remove methods without an machineID
			//java.util.Iterator iterator = s.createCriteria(org.setupx.repository.core.communication.leco.Method.class).addOrder(org.hibernate.criterion.Order.asc("label")).list().iterator();
		    
			java.util.Iterator iterator = s.createSQLQuery("select {method.*} from machinemethod method where method.machine is null order by method.label")
							.addEntity("method", Method.class).list().iterator();

		    while (iterator.hasNext()){
				Method method = (org.setupx.repository.core.communication.leco.Method)iterator.next();	
				%>
			  <tr>
			    <td >
			    	<table>
		    		<tr>
	<% 
Iterator machineIter = Machine.persistence_loadAll(Machine.class, s).iterator();  
while (machineIter.hasNext()){
	Machine m = (Machine)machineIter.next();%>
						    <td><a href="?id=<%=method.getUOID()%>&target=<%=m.getUOID()%>&action=2"><B><%=m.getMachineLetter()%></B></a></td>
<%} %>


			    		</tr>
			    	</table>
			    </td>
			    <td><font class="small"><%=method.getUOID()%></font></td>
			    <td><%=method.getLabel()%><br><font class="small"><%=method.getValue()%></font></td>
			  </tr>
			  <tr>
			    <td colspan="3"><hr></td>
			  </tr>
				
				<% 
			}
			%>


			<!--  REFRESH button  -->
			  <tr>
			    <th colspan="3" align="center">Update the list of methods <a href="?refresh=2"><img  border='0' src="pics/go_search.gif"></a></td>
			  </tr>
			
			</table>
		</td>
		<td colspan="4"></td>
		<td colspan="2" valign="top">
			<!--  table containing each machine with the methods -->
			<%
			// get a list of all machines
        s = PersistenceConfiguration.createSessionFactory().openSession();
        try {
            iterator = Machine.persistence_loadAll(Machine.class, s).iterator();
            while (iterator.hasNext()) {
                Machine machine = (Machine) iterator.next();
                %>

				<table cellspacing="3" cellpadding="2" width="100%" align="center">
				  <tr>
				  		<td style="background-color:#FFFFFF" align="center"><a href="machine.jsp"><img  border='0' src="pics/go_setup.gif"></a></th><th colspan="21"><%=machine.getLabel() %>   [<%=machine.getMachineLetter() %>-machine]<br><font class="small"><%=machine.getComment() %>  ID:<%=machine.getUOID() %> </th>
				  </tr>

                <% if (machine instanceof GCTOF) {
                    GCTOF gctof = (GCTOF) machine;
                    
					Iterator methods = gctof.getMethods().iterator();
                    while (methods.hasNext()) {
                        Method method = (Method) methods.next();
                        %>

			  <tr>
			    <td colspan="2">
			    	<%=method.getLabel() %><font class="small"> [<%=method.getUOID()%>]<br>
				    <font class="small"><%=method.getValue()%>
				</td>
			    <td><a href="?id=<%=method.getUOID()%>&target=<%=machine.getUOID() %>&action=1"><img  border='0' src="pics/trash.gif"></a></td>
			    <td align="center">
			        <% if (method.getValue().indexOf("(GC)") > 0){ 
			            ColumnInformation columnInformationSelected = null;
			            try {
			                columnInformationSelected = ColumnDetector.getColumnInformationByMethod(method.getValue());
			            } catch(Exception e){
			                e.printStackTrace();
			            }
			            Hashtable colInfs = ColumnDetector.getAllColumnConfigurations();
			            %>
			            <select>
							  <option>--- unknown --</option>
			            
			            <%
			            Enumeration keys = colInfs.keys();
		                HashSet filter = new HashSet();
			            while(keys.hasMoreElements()){
			                String key = "" + keys.nextElement();
			                ColumnInformation columnInformation = (ColumnInformation)colInfs.get(key);
			                if (!filter.contains(columnInformation.getLable().toString())){
			                    filter.add(columnInformation.getLable().toString());
			                    if (columnInformationSelected != null && columnInformationSelected.getLable().compareTo(columnInformation.getLable()) == 0){
			                        %>
			                        <option selected="selected">--> <%=columnInformation.getLable()%>]</option>
			                        <%
			                    }else{
			                        %>
			                        <option><%=columnInformation.getLable()%></option>
			                        <%
			                    }
				            %>
						<%  }
			                else {

			                }
			                }
			            %>
						</select>
					<% } %>
			  </tr>
                        <%
                    }
	                } else { 
	                // in case it is not a GCTOF
	                %>
			  <tr>
			    <td><font class="small">--</td>
			    <td colspan="2">machine has no defined methods.</td>
			    <td></td>
			  </tr>
	                <% 
	                }
	            }
		        } catch (PersistenceActionFindException e) {
		            e.printStackTrace();
		        }          
      				%>
			</table>
			
		</td>
		<td width="3%"><b></td>
	</tr>
	</table>
</body>

<%@ include file="footer.jsp"%>

