<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<%@page import="org.setupx.repository.web.forms.inputfield.StringInputfield"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.setupx.repository.web.forms.FormObject"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>



<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<br>
<br>
<br>
<br>
<br>

<table align="center" >
<tr>
<td width="100" height="100" valign="middle" align="center" style="background-color: white;">
<img src="pics/waiting.gif" border=0/>
<tr>
<th align="center">
<h3>processing</h3>
adding parameter <%=request.getParameter("name")%>

</table>



<%
	Session s = CoreObject.createSession();

	Transaction tx = null;

	MSIAttribute msiAttribute = null;


	// get ID of the promt that attributes will be added to
	long promtID = Long.parseLong(request.getParameter("promt"));
	
	// get name of the attribute
	String question = request.getParameter("name");
	
	Iterator iterator = new SXQuery().findSampleIDsByPromtID(promtID).iterator();

	
	// checking if it is an MSI Attribute

    String msiID = request.getParameter("name").toString();

	try {
        msiAttribute = (MSIAttribute)CoreObject.persistence_loadByID(MSIAttribute.class, s, Long.parseLong(msiID));
        question = msiAttribute.getMsiLabel();
    } catch (Exception e){
        Logger.debug(this, "not msi ");
        question = msiID;
    }

    
	// looping over all sample IDs and add a new field
	while(iterator.hasNext()){
	    long sampleID = Long.parseLong(iterator.next().toString());

		// create an inputfiled for each sample
		StringInputfield field = new StringInputfield(question , "custom field", "--" , true);
		field.setRestriction(null);
		if (msiAttribute != null) field.setMSIAttribute(msiAttribute);
		field.setSize_x(20);
		
		// SAVE the inputfield
		field.update(s, true);

		// set the parent ID of new attribute to samplesID
		int position = Integer.parseInt(s.createSQLQuery("select (max(internalPosition) + 1) as m from formobject where parent = " + sampleID).addScalar("m", Hibernate.INTEGER).list().get(0).toString());

		try {
		    tx = s.beginTransaction();

			Query hqlUpdate = s.createQuery("update " + FormObject.class.getName() + " set parent = " + sampleID  + ", internalposition = " + position +  " where UOID = '" + field.getUOID() + "'");
			hqlUpdate.executeUpdate();
		     
		    tx.commit();
		 } catch (Exception e) {
		    if (tx!=null) tx.rollback();
		    throw e;
		 }
	}
	s.close();
		
%>


<%@page import="org.hibernate.Transaction"%>
<meta http-equiv="refresh" content="0; URL=sample_custom_load.jsp?id=<%=promtID %>">
