<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Datafile"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.Enumeration"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.io.File"%>

<%@ include file="checklogin.jsp"%>

<%
	Session sess = TechnoCoreObject.createSession();

	File _tmp = File.createTempFile("xyz", ".tmp", null);
    Logger.log(this, "Saving uploaded files to " + _tmp.getParentFile().getAbsolutePath());

    MultipartRequest multiRequest = new MultipartRequest(request, _tmp.getParentFile().getAbsolutePath());
    _tmp.delete();

	long technofiletype = Long.parseLong(multiRequest.getParameter("technofiletype"));
	long sampleID = Long.parseLong(multiRequest.getParameter("sampleid"));

	TechnologyFileType technologyFileType = (TechnologyFileType)TechnologyFileType.persistence_loadByID(TechnologyFileType.class, sess, technofiletype);

    Enumeration fileNames = multiRequest.getFileNames();

	while (fileNames.hasMoreElements()) {
      String filename = (String) fileNames.nextElement();
      
	  File file = multiRequest.getFile(filename);
	 
	  Logger.log(this, "uploaded file: " + file.getName() + "   " + file.getAbsolutePath());

      String path = multiRequest.getParameter("path");
      String localFilename = path + File.separator + sampleID + "_" + file.getName();

      File localfile = new File(localFilename);
      org.setupx.repository.core.util.File.copyFile(file, localfile);
	  Logger.log(this, "and a copied to " + localfile);
	
	  Datafile datafile = new Datafile(sampleID, localfile);
	  datafile.setSubmitterUserID(user.getUOID());
	  technologyFileType.addDatafile(datafile);
	}
	
	technologyFileType.getTechnology().updatingMyself(sess, true);

	Logger.debug(this, "saved technology: " + technologyFileType.getTechnology().getUOID());
	
	pageContext.forward("techno_technologyfiletype.jsp?id=" + technologyFileType.getUOID());
%>