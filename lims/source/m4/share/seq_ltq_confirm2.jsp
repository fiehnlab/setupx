<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.List"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Datafile"%>
<%@page import="java.util.Date"%>

<%@page import="com.oreilly.servlet.Base64Decoder"%>
<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.forms.page.Page"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Clazz"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.setupx.repository.web.forms.FormObject"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%
	// target folder
	File targetDir = new File(Config.DIRECTORY_LTQ_SEQ + "/" + request.getParameter("id"));

	try {
		targetDir.mkdirs();
	} catch (Exception e){
	    
	}


	boolean random = false;
	try {
	    random = (request.getParameter("random").compareTo("true") == 0);
	} catch (Exception e){
	    
	}
		
	boolean randomTotal = false;
	try {
	    randomTotal = (request.getParameter("randomTotal").compareTo("true") == 0);
	} catch (Exception e){
	    
	}
		
	// type of tray
	String tray = request.getParameter("tray");
	int x_max = 0;
	int y_max = 0;
	if (tray.compareTo("96") == 0){
	    x_max = 12;
	    y_max = 8;
	} else if (tray.compareTo("360") == 0){
	    x_max = 24;
	    y_max = 16;
	}

	// offset
	int sampleCount = Integer.parseInt(request.getParameter("startPos")) ;
	
	// containing all positioned samples
	Hashtable samplesPositioned = new Hashtable();

	Session hqlSession = CoreObject.createSession();

	// sampleIDs
        String basic = "select sample.id from " 
	        + Promt.class.getName() + " as promt, " + Page.class.getName() + " as page, " 
	        + Clazz.class.getName() + " as clazz, " + Sample.class.getName() + " as sample, " 
	        + FormObject.class.getName() + " as child " + "where page.parent = promt " 
	        + "and child.parent = page " + "and clazz.parent = child " + "and sample.parent = clazz " 
	        + "and promt = '" + request.getParameter("id") + "' and sample.active = true and clazz.active = true ";
	
		// exclude all samples that were scheduled earlier
		List exList = (List)session.getAttribute("exList");
		if (exList != null){
			Iterator exIter = exList.iterator();
			while(exIter.hasNext()){
			    basic = basic.concat(" and sample.id != " + exIter.next());
			}
		}


	    basic = basic.concat(" and sample.id in (");

		
		// actaully selected
		Enumeration selectedSampels = request.getParameterNames();
		boolean isFirst = true;
		while(selectedSampels.hasMoreElements()){
		    String paramName = selectedSampels.nextElement().toString();
		    if (paramName.startsWith("active")){
			    boolean paramActive = request.getParameter(paramName).compareTo("true") == 0;
			    if (paramActive){
			        if (isFirst) {
			            isFirst = false;
			        } else {
			            basic = basic.concat(", ");
			        }
				    basic = basic.concat(paramName.substring("active".length()));
			    }
		    }
		}
		
	    basic = basic.concat(" )");
		
		Logger.log(this, basic);

        Query query = hqlSession.createQuery(basic);

        List sampleIDsTMP = query.list();
        
        if (randomTotal){
            Collections.shuffle(sampleIDsTMP);
        }

        
        List sampleIDs = null;
        boolean lastSequence = false;
        %>
        samples: <%=sampleIDsTMP.size() %>  
        traysize: <%=(x_max * y_max)%>
        <%
        if (sampleIDsTMP.size() > (x_max * y_max)){
            sampleIDs = sampleIDsTMP.subList(0, (x_max * y_max));
        } else {
            sampleIDs = sampleIDsTMP;
            lastSequence = true;
        }
                
        
        if (exList == null) {
            exList = sampleIDs;
        } else {
            exList.addAll(sampleIDs);
        }
		session.setAttribute("exList", exList);
        
        
//	List sampleIDs = new SXQuery().findSampleIDsByPromtID(Long.parseLong(request.getParameter("id"))).subList(1, (x_max * y_max));
	
	
	
	Iterator sampleIter = sampleIDs.iterator();
	
		
	while(sampleIter.hasNext()){
	    
	    long sampleID = Long.parseLong(sampleIter.next().toString());
	    
		// method
		String method = request.getParameter("method" + sampleID);
		
		// active 
		boolean active = false;
		try {
		    active = request.getParameter("active" + sampleID).compareTo("true") == 0;
		} catch (Exception e){
		}
		
		if (active){
		    Logger.log(this, "adding " + sampleCount + "" + sampleID + "");
			// put into vector
			samplesPositioned.put(sampleCount + "", sampleID + "");
		    sampleCount ++;
		}
	}
%>




<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Random"%>
<%@page import="java.util.Collections"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>


<table align="center" width="600" cellpadding="5">
	<tr>
		<td>
			<table>
				<tr>
					<th>Exit</th>
					<th width="90%">&nbsp;</th>
					<th>Administration</th>
				</tr>
				<tr>
					<td style="background-color:#FFFFFF" align="center"><a href="login.jsp"><img src='pics/go_exit.gif' border="0" valign="middle" align="center"></a></td>
					<td style="background-color:#FFFFFF" align="center"></a></td>
					<td style="background-color:#FFFFFF" align="center"><a href="admin.jsp"><img src='pics/go.gif' border="0" valign="middle" align="center"></a></td>
				</tr>
			</table>					
	<tr>
		<th>
			<h1 align="center">samples</h1>
			<h2 align="center">samples on the tray</h2>
			The chart below shows how the samples are positioned on the tray.
			<br>
			<%=exList.size() %> samples excluded.


	<tr>
		<td>	
		<%
			int pos = 1;
		%>
	
		<table border="1" width="90%" align="center">
		<%
			for (int rowN = 0; rowN < y_max; rowN++){
			    %>
			    	<tr>
					<%
						for (int colN = 0; colN < x_max; colN++){
						    int x = 1 + ((pos-1) % (x_max));
						    int y = ((pos-1) / x_max) ;
						    
						    long sampleID = 0;
						    String sampleIDprinted = "";
	
						    String styleHTML = "";
						    try {
						        sampleID = Long.parseLong(samplesPositioned.get(pos + "").toString());
						        sampleIDprinted = "" + sampleID;
						        styleHTML = "style=\"background-color: red\";";
						    } catch (Exception e){
						        sampleIDprinted = "-";
						    }
					    	%>
						    <td align="center" <%=styleHTML %> width="<%=100/x_max %>%">
						    	<font style="font-size: xx-small;">
							    	<%=(char)(y+65)  %><%=x %>
							    	<br>
							    	<%=pos %>
							    	<b>
							    	<br>
	   						    	<%=sampleIDprinted %>
	   						    	</b>
						    	</font>
						    </td>
						    <%
						    pos++;
						}
						%>
			    	</tr>
			    <%
			}
		%>
		</table>


	<tr>
		<th>	
			<h1 align="center">Samples sorted by Sequence</h1>
		<tr>
			<td>	
		<table width="90%" border="1" align="center">	
			<th>sequence
			<th>ID
			<th>sample label
			<th>position
		<%
		// Random number generator
		List shuffeled = sampleIDs;
		if (random) Collections.shuffle(shuffeled);
	
		sampleIter = shuffeled.iterator();

    	String newLine = "" + (char)13 + (char)10; 

	    StringBuffer seqAdvionBuff = null;
	    StringBuffer seqXCaliBuff = null;

	    // file
	    int filecounter = 0; 
	    try {
	        filecounter =  Integer.parseInt(request.getParameter("filecounter"));
	    } catch (Exception e){
	        
	    }
	    
	    
		int c = 0;
		while(sampleIter.hasNext()){    
		    
		    // init 
		    if (seqAdvionBuff == null){
		    	// Advion 
		    	// advion get the actual position because it is controlling the arm
		    	//0;
		    	//;C:\Program Files\Advion\ChipSoft\methods\ETS_FTMS_4MIN.meth;A01;;-1;-1;
		    	//;C:\Program Files\Advion\ChipSoft\methods\ETS_FTMS_4MIN.meth;A02;;-1;-1;
		    	seqAdvionBuff = new StringBuffer();
		    	seqAdvionBuff.append("0\n");
		    	
		    	
		    	// Xcalibur
		    	// Bracket Type=4
		    	// 
		    	// Sample Type,File Name,Path,Instrument Method,Position,Inj Vol,Level,Process Method,Sample ID,Calibration File,Sample Wt,Sample Vol,ISTD Amt,Dil Factor,L1 Study,L2 Client,L3 Laboratory,L4 Company,L5 Phone,Comment,Sample Name,
		    	// 
		    	// Unknown,DriedAlage1_with_MTBE,C:\Xcalibur\data\2008\SKZ\LC for Algae,L:\Yup\Test for SetupX\LKH_2008_nanomate_Pos_Profile_30sec_MS,1,5.000,,,1,,0.000,0.000,0.000,1.000,,,,,,,,
		    	// 
		    	// Unknown,DriedAlgae2_with_MTBE,C:\Xcalibur\data\2008\SKZ\LC for Algae,L:\Yup\Test for SetupX\LKH_2008_nanomate_Pos_Profile_30sec_MS,1,5.000,,,1,,0.000,0.000,0.000,1.000,,,,,,,,
		    	// new line for csv file
		    	
		    	seqXCaliBuff = new StringBuffer();
		    	seqXCaliBuff.append("Bracket Type=4" + newLine);
		    	seqXCaliBuff.append("Sample Type,File Name,Path,Instrument Method,Position,Inj Vol,Level,Process Method,Sample ID,Calibration File,Sample Wt,Sample Vol,ISTD Amt,Dil Factor,L1 Study,L2 Client,L3 Laboratory,L4 Company,L5 Phone,Comment,Sample Name," + newLine);
		    	// Unknown,DriedAlage1_with_MTBE,C:\Xcalibur\data\2008\SKZ\LC for Algae,L:\Yup\Test for SetupX\LKH_2008_nanomate_Pos_Profile_30sec_MS,1,5.000,,,1,,0.000,0.000,0.000,1.000,,,,,,,,
		    }
		    
		    
		    
		    long sampleID = Long.parseLong(sampleIter.next().toString());
			int samplePos = -1;
			// receive the actual pos
			Enumeration keys = samplesPositioned.keys();
			while(samplePos == -1 && keys.hasMoreElements()){
			    int position = Integer.parseInt(keys.nextElement().toString());
				if (samplesPositioned.get(position + "").toString().compareTo(sampleID + "") == 0){
				    samplePos = position;
			    }
			}
			
			if (samplePos > -1){
			    int x = 1 + ((samplePos-1) % (x_max));
			    int y = ((samplePos-1) / x_max) ;
			    c++;
			    String sampleLabel = new SXQuery(hqlSession).findSampleLabelBySampleID(sampleID);
				%>
				<tr>
					<td align="center"><%=c %> /  <%=(x_max * y_max) %>
					<td align="center"><%=sampleID %> 
					<td align="center"><%=sampleLabel %>
					<td align="center"><%=(char)(y+65)  %><%=x %>
					<td>x: <%=x %>  y: <%=y %>
				<%
				seqXCaliBuff.append("Unknown," + sampleID + ",L:\\setupx\\data\\"  + request.getParameter("id") + "," + request.getParameter("method" + sampleID) + "," + ((char)(y+65)) + x + ",5.000,,," + sampleID + ",,0.000,0.000,0.000,1.000,,,,,,," + sampleLabel + "," + newLine);
				
				seqAdvionBuff.append(sampleID + ";C:\\Program Files\\Advion\\ChipSoft\\methods\\ETS_FTMS_4MIN.meth;");
				seqAdvionBuff.append((char)(y+65));
				seqAdvionBuff.append(x);
				seqAdvionBuff.append(";" + sampleLabel + ";-1;-1;");
				//;C:\Program Files\Advion\ChipSoft\methods\ETS_FTMS_4MIN.meth;A01;;-1;-1;
				seqAdvionBuff.append(newLine);
	
			}
			
		}
	

		
    	File seqAdvionFile = new File(targetDir, "advion-" + request.getParameter("id") + "-" + filecounter + ".sld");
    	File seqXCaliFile = new File(targetDir, "xcalibur-" + request.getParameter("id") + "-" + filecounter + ".csv");

    	
    	/*
    	Logger.log(this, seqAdvionBuff.toString());
    	Logger.log(this, seqXCaliBuff.toString());
    	Logger.log(this, seqAdvionFile.toString());
    	Logger.log(this, seqXCaliFile.toString());
    	*/
	    
		org.setupx.repository.core.util.File.storeData2File(seqAdvionFile,seqAdvionBuff.toString());
		org.setupx.repository.core.util.File.storeData2File(seqXCaliFile,seqXCaliBuff.toString());
				
		filecounter++;
		c = 0;
		%>	
		</table>

	<tr>
		<th>	
		<h1 align="center">Samples sorted by Position</h1>
	
	<tr>
		<td>	
	<table width="90%" border="1" align="center">	
		<th>Position
		<th>ID
		<th>sample label
		
	<%
	for (int xx = 0; xx < (x_max * y_max); xx++){
		int samplePos = xx;
		try {
		    long sampleID = Long.parseLong(samplesPositioned.get(samplePos + "").toString());
			
			if (samplePos > 0){
			    int x = 1 + ((samplePos-1) % (x_max));
			    int y = ((samplePos-1) / x_max) ;
			    c++;
				%>
				<tr>
					<td align="center"><%=(char)(y+65)  %><%=x %>
					<td align="center"><%=sampleID %> 
					<td align="center"><%=new SXQuery(hqlSession).findSampleLabelBySampleID(sampleID) %>
				<%
			}
		} catch (Exception e){
		    
		}
	}
	
		
		

	%>
	
	</td></tr></table>
	
	
	        
<%        // link to next seq 
	


		if (!lastSequence){
		    %>
			 <form action="" method="post">
			 	<input type="text" name="filecounter" value="<%=filecounter + 1 %>">
			 		<br><br>
								
								
							<% 
								Enumeration names = request.getParameterNames();
								while(names.hasMoreElements()){
								    String name = names.nextElement().toString();
								    String value = request.getParameter(name).toLowerCase();
								    %><input type="hidden" name="<%=name %>" value="<%=value %>">
								    <%
								}
					   		%>
				<input type="submit" value="create additional sequence">		 	
			 </form>
			 <%
		}
%>
	
	
	
	
	