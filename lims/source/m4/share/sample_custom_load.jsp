<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="java.util.Date"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%

	// get the id
	long promtID = Long.parseLong(request.getParameter("id") + "");
	
	// load the promt
	Promt promt = Promt.load(promtID);
	
	// set the promt in the session
	session.setAttribute(WebConstants.SESS_FORM_PROMT, promt);
	
	// put the time in the session
	session.setAttribute("TIME", "" + new Date().getTime());
	
	// put position in the session
	session.setAttribute("pos", new Integer(0));

%>

<meta http-equiv="refresh" content="0; URL=sample_sutom_meta.jsp">
