<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIClassifierID"%>




<%
	int kingdom = Integer.parseInt(request.getParameter("kingdom"));
	int id = Integer.parseInt(request.getParameter("id"));

	new NCBIClassifierID(id, kingdom).update(true);
	
	
	Logger.log(this, "resetting the NCBI cache.");
	
	NCBIConnector.resetCache();

	Logger.log(this, "finished resetting the NCBI cache.");
%>


<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBI_Classifier"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<jsp:forward page="ncbi_classifyed.jsp?ncbiid=<%=id%>"/>
