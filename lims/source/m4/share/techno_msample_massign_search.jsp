  <%@page import="java.util.StringTokenizer"%>
<%@page import="java.util.Enumeration"%>
<jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
    <title>SetupX</title>
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>

<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Technology"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Location"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.core.util.FilenamePatternFilter"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Datafile"%>
<%@page import="java.util.Date"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"%>


	<jsp:include page="techno_header.jsp"></jsp:include>

	<h2>
		<a href="techno_techno.jsp">all platforms</a> - search for datafiles
	</h2>
	<p>

<form action="techno_msample_assign_create_process.jsp" method="post"> 
<table align="center">


<%
	Session s = TechnoCoreObject.createSession();

	// convert inputfiled values in arrays

	StringTokenizer tokenizerPattern = new StringTokenizer(request.getParameter("samplePattern"));
	StringTokenizer tokenizerIDs = new StringTokenizer(request.getParameter("sampleIDs"));
	String[] samplePatternArray = new String[tokenizerPattern.countTokens()];
	long[] sampleIDs = new long[tokenizerIDs.countTokens()];
	
	if (sampleIDs.length != samplePatternArray.length) throw new Exception("there are not as many patterns as sample IDs.");

	int t = 0;
	while(tokenizerIDs.hasMoreElements()){
	    samplePatternArray[t] = tokenizerPattern.nextToken();
	    sampleIDs[t] = Long.parseLong(tokenizerIDs.nextToken());
	    t++;
	}

	// String[] samplePatternArray = new String[]{"asia","world"};
	// long[] sampleIDs = new long[]{44,55};


	int fileC = 0;
	Iterator technologyIterator = TechnologyProvider.getTechnologies().iterator();
	while(technologyIterator.hasNext()){

	    // open every technology
	    Technology technology = (Technology)technologyIterator.next();
	    %>
	    
	    <tr>
    	<th colspan="21" style="background-color:#989898 ;"><%=technology.getLabel() %>:</th>
    	<%
	    
	    Iterator technoFiletypeIterator = technology.getTechnologyFileTypes().iterator();
	    while(technoFiletypeIterator.hasNext()){

			// open every filetype
	        TechnologyFileType technologyFileType = (TechnologyFileType)technoFiletypeIterator.next();
	        %>
			<tr>
   			<td><img src="pics/aq_arrow.gif">
			<td colspan="21" style="background-color:#B0B0B0 ;">filetype: <%=technologyFileType.getLabel() %> (filtered by pattern: <%=technologyFileType.getPattern() %>)<%
	        
			Iterator locationIterator = technologyFileType.getDefaultLocations().iterator();
	        while(locationIterator.hasNext()){
				
				// open every location
				Location location = (Location)locationIterator.next(); 
		        %>
				<tr>
				<td>
				<td>
				<td colspan="21" style="background-color:#E0E0E0;">location: <%=location.getPath() %><%
				
				// check each file agains each samplePattern
				File[] files = new File(location.getPath()).listFiles(new FilenamePatternFilter(technologyFileType.getPattern()));
		        for (int i = 0; i < files.length; i++){
		            File file = files[i];
		            
		            // looping over all sample pattern
		            for(int patternCount = 0; patternCount < samplePatternArray.length; patternCount++){  
		                try {

	//		                Logger.log(this, "looking for sample " + sampleIDs[patternCount] +  " pattern: " + samplePatternArray[patternCount]);
			                
			                long sampleID = sampleIDs[patternCount];
			                String samplePattern = samplePatternArray[patternCount];
			                
					
				            if (file.getName().indexOf(samplePattern) > -1){ // || file.getName().indexOf(sampleID) > -1){
				                
				                // check if the file has been assigned already
								String query1 = "select * from datafile where datafile.sampleID = " + sampleID + " and datafile.technologyfiletype = " + technologyFileType.getUOID() + " and source = \"" + file.getPath() + "\"";
								List assignedAlready = s.createSQLQuery(query1).addEntity(Datafile.class).list();
								
								String query2 = "select * from datafile where source = \"" + file.getPath() + "\"";
								List assignedOthers = s.createSQLQuery(query2).addEntity(Datafile.class).list();
				
								String checked = "checked=\"checked\"";
								if (assignedOthers.size() > 0){
								    checked = "";
								}
				                
						        %>
						        <tr>
		       					<td width="2">
		       					<td width="2">
		       					<td width="2" align="center" valign="middle">
		       						<% if (file.isDirectory()){ %>
		       							<img src="pics/circle.gif"> 
		       						<%} else if (assignedAlready.size()>0){ %>
		       							<img src="pics/check.gif"> 
		       						<%} else { 
		       						 	fileC++;
		       							%>
		       							<input align="top" type="checkbox" <%=checked %> name="<%=sampleID + "_" + technologyFileType.getUOID() + "_" + fileC %>" value="<%=file.getPath()%>"/>
		       						<%} %>
						        <td width="2"><nobr><font style="font-size: xx-small">sampleID</font> <b><%=sampleID%></b></nobr>
						        <td width="2"><b><%=file.getName()%></b>
		       					<td width="2"><nobr><font style="font-size: xx-small">matches "<%=samplePattern %>"</font></nobr>
		       					<td width="2"><nobr><font style="font-size: xx-small"><%=new Date(file.lastModified()).toLocaleString() %></nobr>
		       					<td >
		       						<%if (assignedAlready.size() > 0){
		       						 %><nobr>File is assigned to <b>this</b> sample already.</nobr><%
		       						} else if (assignedOthers.size() > 0){
		       						 %><nobr>This file is assigned to <b><%=assignedOthers.size() %></b> samples already.</nobr><%
		       						}
		       					%>
		       					<td colspan="21">
						        <%
				            }
			            }catch (Exception exception){
			                exception.printStackTrace();
			            }

		            }
		        }
	        }
	    }
	}
%>
</td>
</tr>
<tr>
	<td colspan="21">
		<input type="submit">
</table>
</form>