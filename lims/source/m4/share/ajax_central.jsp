<%@page import="java.util.Date"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBI_Name"%>
<%@page import="java.util.Hashtable"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.LocalDatabaseInstance"%>
<%@page import="java.util.HashSet"%>
<%@page import="org.setupx.repository.core.communication.ontology.obo.OBOTerm"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.Settings"%>
<%@page import="org.setupx.repository.SettingsTest"%>
<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.core.communication.ontology.obo.OBOConnectorPool"%>
<%@page import="org.setupx.repository.web.forms.restriction.RestrictionOBO_human_anatomy"%>
<%@page import="org.setupx.repository.web.forms.restriction.OrganLexiRestriction"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.OrganInputfield"%>
<%@page import="org.setupx.repository.core.communication.ontology.obo.OBOConnector"%>
<%@page import="org.setupx.repository.web.ajax.AjaxContants"%>
<%
    //  JSP creating a JSON response for ajax requests
%>

<%@ include file="checknonlogin.jsp"%>

<%@page import="org.setupx.repository.core.query.QueryMasterAnswer"%>
<%@page import="org.setupx.repository.core.query.QueryMaster"%>
<%@page import="org.hibernate.criterion.Expression"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.setupx.repository.web.pub.data.cache.VariationsCache"%>
<%@page import="java.util.Vector"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%!
	
	final static Hashtable[] hashtables = new Hashtable[]{
        LocalDatabaseInstance.names_real, 
        LocalDatabaseInstance.names_common, 
        LocalDatabaseInstance.names_equi, 
        LocalDatabaseInstance.names_misspelling, 
        LocalDatabaseInstance.names_synonym         
};
%>
<%



    //Session s = CoreObject.createSession();
    Enumeration e = request.getParameterNames();
    int y = 0;
    while (e.hasMoreElements()) {
        y++;
        Logger.info(this, "Name of Param " + y + " : "
                + (String) e.nextElement());
    }
    Logger.info(this, "Total Parameters : " + y);

    Logger.info(this, "start receiving parameters.");

    String searchTerm = request.getParameter("input").toString();
    Logger.info(this, "input:" + searchTerm);

    int type = Integer.parseInt(request.getParameter("type").toString());
    Logger.info(this, "type:" + type);

    List list = new Vector();
    
    long timeSTART = new Date().getTime();

    switch (type) {

    // --- QUERY for usernames
    case AjaxContants.QUERY_USER:
        if (user.isAdmin() || user.isLabtechician()) {
            String query = "select u.uoid, u.displayName, u.organisation, u.emailString from user as u "
                    + "where u.displayName like \"%"
                    + searchTerm
                    + "%\" "
                    + " or u.username like \"%"
                    + searchTerm
                    + "%\" "
                    + " or u.emailString like \"%"
                    + searchTerm
                    + "%\" "
                    + " or u.organisation like \"%"
                    + searchTerm
                    + "%\" ";

            list = CoreObject.createSession().createSQLQuery(query)
                    .addScalar("uoid", Hibernate.LONG).addScalar(
                            "displayName", Hibernate.STRING).addScalar(
                            "organisation", Hibernate.STRING)
                    .addScalar("emailString", Hibernate.STRING).list();

        } else if (searchTerm.length() > 3) {
            String query = "select u.uoid, u.displayName from user as u "
                    + "where u.displayName like \"%"
                    + searchTerm
                    + "%\" "
                    + " or u.username like \"%"
                    + searchTerm
                    + "%\" "
                    + " or u.emailString like \"%"
                    + searchTerm + "%\" ";

            list = CoreObject.createSession().createSQLQuery(query)
                    .addScalar("uoid", Hibernate.LONG).addScalar(
                            "displayName", Hibernate.STRING).list();

        }
        break;

        
	// --- QUERY for organs
    case AjaxContants.QUERY_ORGAN:
        
        if (searchTerm.length() > 2){
            OBOConnector[] connectors = new OBOConnector[]{
                    OBOConnector.newInstance(new File(Settings.getValue("data.obo.humananatomy"))),
                    //OBOConnector.newInstance(new File(Settings.getValue("data.obo.mouse_anatomy"))),
                    OBOConnector.newInstance(new File(Settings.getValue("data.obo.plantanatomy"))),
            };
            //Enumeration keys = connector.getFileReader().getoboHashtable().keys();
            HashSet t = new HashSet();
            for (int arrayCounter = 0; arrayCounter < connectors.length; arrayCounter++){
                Iterator values = connectors[arrayCounter].getFileReader().getoboHashtable().values().iterator();
                while (values.hasNext()){
                    OBOTerm value = (OBOTerm)values.next();
                    //Logger.warning(this, value.toString());
                    if (! t.contains(value.getTerm()) ){
                        if (	
                                value.getTerm().indexOf(searchTerm) >= 0
                                ||                         
                                value.getDefinition().indexOf(searchTerm) >= 0
                                
            				){
                            t.add(value.getTerm());
                            
                            Object[] o = new Object[4];
                            o[1] = value.getTerm();
                            o[2] = value.getDefinition();
                            o[3] = connectors[arrayCounter].getFileReader().sourceFile.getName();
                            list.add(o);
                        }
                    }
                }
            }
	}

        
        
        
        
    	// --- QUERY for SPECIES
        case AjaxContants.QUERY_SPECIES:
            
            if (searchTerm.length() > 3){
				
				for (int htCounter = 0; htCounter < hashtables.length; htCounter++){
				    Hashtable ht = hashtables[htCounter];
					
				    String key = "";
	                    
	                Enumeration enumeration = ht.keys();
	                
	                while (enumeration.hasMoreElements() && (new Date().getTime() - timeSTART < 300)) {
	                    key = enumeration.nextElement().toString();

	                        if (	
	                                key.indexOf(searchTerm) >= 0
	            				){
	                            
	                            NCBI_Name name = (NCBI_Name)ht.get(key);
	                            
	                            NCBIEntry entry = NCBIConnector.determineNCBI_Information(Integer.parseInt(name.getTAX_id()));
	                            
	                            Object[] o = new Object[4];
	                            o[1] = entry.getName();
	                            o[2] = entry.getSpeciesTypeName();
	                            o[3] = entry.getRank() + " " + entry.getTaxID();
	                            list.add(o);
	                        }
	                }
					Logger.warning(this, "" + (new Date().getTime() - timeSTART));
				}
    	}
        
    default:
        break;
    }
    
    
    
	// limit the length of the list
    if (list.size() > 13) {
        Logger.log(this, "results are limited to 12 !!! ");
        list = list.subList(0, 12);
    }

%>


{ results: [
<%
    Logger.log(this, "found : " + list.size() + " final results	.");

    for (int i = 0; i < list.size(); i++) {
        Object[] objects = (Object[]) list.get(i);

        if (user.isAdmin() || user.isLabtechician()) {
%>
				{ id: "<%=i%>", value: "<%=objects[1]%>", info: "<%=objects[1]%> <%=objects[2]%> <%=objects[3]%>" },
			<%
     } else {
 %>
				{ id: "<%=i%>", value: "<%=objects[1]%>", info: "<%=objects[1]%>" },
			<%
     }
     }
 %>
] }

