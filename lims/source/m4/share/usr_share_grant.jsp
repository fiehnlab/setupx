<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="org.setupx.repository.core.user.UserLocal"%>
<%@page import="org.setupx.repository.core.communication.mail.EMailAddress"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.core.util.Mail"%>
<%@page import="org.setupx.repository.core.communication.notification.Notification"%>
<%@page import="java.util.Date"%>

<%@ include file="checklogin.jsp"%>


<%

	long expID = Long.parseLong(request.getParameter("id"));

	// all parameters
	Enumeration parameterNames = request.getParameterNames();
	while (parameterNames.hasMoreElements()){
	    
		
		// if parameter is a users name
		String parameterName = parameterNames.nextElement().toString();
		
		if (parameterName.startsWith("name_")){
		    try{
			    int number = Integer.parseInt(parameterName.substring(parameterName.indexOf('_') + 1, parameterName.length()));

			    String _name = request.getParameter("name_" + number);
			    String _email = request.getParameter("email_" + number);
			    String _username = _email.substring(0, _email.indexOf("@"));
			    
				// generate password
				String _password = ("" + new Date().getTime());
				
				// create user
				UserDO userDO = new UserLocal(_username, _password, new EMailAddress(_email));
				userDO.setDisplayName(_name);
				userDO.setLastExperimentID(expID);
				
				userDO.update(true);
				
				MailController.inform(userDO.getMailAddress(), "account created", "\nHello " + userDO.getDisplayName() + ","
				        + "\nA personal account has been created for you on SetupX in order to share experimental design, experiemental results, ...\n" 
				        
				        + "\n\n You can access the system via:   http://setupx.fiehnlab.ucdavis.edu/m1/ "
				        + "\nYour login information is " 
				        + "\nusername:  " + userDO.getUsername()
				        + "\npassword:  " + userDO.getPassword()
				        + "\n\n"
				        + user.getDisplay() + "("+ user.getEmailString() + ") has created that account for you. " 
				        + "" + user.getDisplay() + " also granted you access to the experiment: "
				        + "\n\ntitle: " +new SXQuery().findPromtTitleByPromtID(expID) 
				        + "\n\nabstract: " + new SXQuery().findPromtAbstractByPromtID(expID)
				        + "\n\nIf you have any further questions feel free to contact us." 
				        + "\n" + Config.SYSTEM_EMAIL);
				
				MailController.inform(Config.OPERATOR_ADMIN, "new user created", user.getDisplay() + "just created a new user: " + userDO.getDisplayName() + " and granted access to experiment " + expID + " (" + new SXQuery().findPromtTitleByPromtID(expID)  + ")");

		 		PromtUserAccessRight.createFullAccess(expID, userDO.getUOID());

			    %>
				<%@page import="org.setupx.repository.server.logging.Message"%>
				<%@page import="org.setupx.repository.core.communication.mail.MailController"%>
				<%@page import="org.setupx.repository.Config"%>
				<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<BR>
				<%=_name%> created.

			    <%
		    } catch (Exception e){
			    %>
			    <BR>
			    Unable to create user.
			    <%
		    }
		}
	}
%>


<meta http-equiv="refresh" content="0; URL=usr_share_exp.jsp?id=<%=expID %>">
