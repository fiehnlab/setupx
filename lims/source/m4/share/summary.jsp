<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ include file="checklogin.jsp"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>
<%@ page import="org.setupx.repository.web.forms.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.multi.*"%>
<%@ page import="org.setupx.repository.web.forms.clazzes.*"%>
<%@ page import="org.setupx.repository.web.forms.page.*"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/	>



<% 
Promt promt = (Promt)session.getAttribute("form_promt");
Integer pos = (Integer)session.getAttribute("pos");%>
<table align='center' border='0' cellpadding='4' width='90%'>
		<td align="center">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2> summary for experiment [id:0<%=promt.getUOID()%>_0]</h2>
		</th>
</table>

<table align='center' border='0' cellpadding='4' width='90%'>

<tr>
<td>
Thank you very much for submitting / updating your experiment.<p>
Your experiment is stored under the <B>ID [<%=promt.getUOID()%>] </B>in our system.<p>
Please keep this ID for further communication with us. <p>

In case you want to make any modifications to your experiment or simply look up the status of your samples 
you can log back into the system by using the following ids:<bR><bR>
Username: <b><%=user.getUsername()%></b><br>
Password: <b><%=user.getPassword()%></b><br>
</td>
</tr>
<tr>
<td>
In case you submitted the experiment right now for the first time, we will get in contact with you soon, to discuss details about the experiment.
<br><br>
Thank you, <br>
The <b>fiehnlab-team</b>,<br>
& The <b>metabolomics-core-team</b>,
<br><br><br><br>
</td>
</tr>

<tr>
<td>
<h4>Download</h4>
	To download an Microsoft Excel File for this <b>experiment <%=promt.getUOID()%>
	
	<a href="<%=session.getAttribute(org.setupx.repository.web.WebConstants.SESS_LINK_DOWNLOAD)%>">
		<img src='pics/xls.gif' border="0" valign="middle" align="center"> 
	</a>
</td>
</tr>


<tr>
<td>
<h4>Link</h4>
<a href="form.jsp">back to <b>experiment <%=promt.getUOID()%></b></a>
</td>
</tr>

</table>

<%@ include file="footer.jsp"%>
