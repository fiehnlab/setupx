<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<%@include file="incl_xls_header.jsp"%>

<%
	Vector rows = new Vector();
	rows = (Vector)session.getAttribute("data"); 
	Logger.debug(this, "got rows");
	
	Vector labels = new Vector();
	// labels by arrow from another row

	try {
		int labelsPosition = Integer.parseInt("" + request.getParameter("labels")); 
		labels = (Vector)rows.get(labelsPosition);
		rows.remove(labelsPosition);
	} catch (Exception xe){
		try {
		    labels = (Vector)session.getAttribute("labels"); 
		    if (labels == null) throw new Exception("no labels");
		} catch (Exception e){
		    labels = new Vector();
		}
	}
	session.setAttribute("labels", labels);
	Logger.debug(this, "got labels");
%>


<table align='center' border='0' cellpadding='4' width="60%" border = 1 >
<tr>	
	<th colspan="21">Select the row containing labels.
<tr>	
	<td colspan="21">
		Please select the row containing the labels for each of the columns.
		<br>
		Try the tool to match each line to an existing sample in the system. <br>
		<a href="xls_automap.jsp"><img src="pics/go.gif" border="0"></a> 

<%
	// find the max length of the vectors
	int maxRow = 0;
	for(int i = 0; i < rows.size(); i++){
	    try {
		    if (((Vector)rows.get(i)).size() > maxRow) maxRow = ((Vector)rows.get(i)).size() ;
	    } catch (Exception e){
	        // some dont have that many elements in a row
	    }
	}

	if (labels.size() == 0){
	    labels.setSize(maxRow);
	}

	%>
        <tr>
	        <td>
    	    </td>
		<%
		Logger.debug(this, "looping over labels " + labels.size());
		for(int i = 0; i < labels.size(); i++){
			String label = "" + labels.get(i);
			if (label.compareTo("null")==0) label = "";
			%>
	        <th align="center">
	        	<%=label%>
	   	    </th>
		<%}%>
    	</tr>
        <%
			Iterator rowIterator = rows.iterator();
        	int rowCounter = -1;
        	while(rowIterator.hasNext()){
        	    rowCounter++;
        	    Vector row = (Vector)rowIterator.next();
                %>
		<tr>
            <th>
            	<table>
            		<tr>
            			<th>
            				<a href="?labels=<%=rowCounter%>">
								<img src='pics/title.gif' border="0" valign="middle" align="center">
							</a>
            			</th>
            			<th><%=(rowCounter+1) %></th>
            		</tr>
            	</table>	
            </th>
	        </td>
	        	<% for (int i = 0; i < row.size(); i++){
	        	   %> 
			        <td align="right">
	        		<%=row.get(i)%>
		    	    </td>
	        	   <%  
	        	}%>
	       	</td>
		</tr>
		<% } // row iterator %>
</table>


<%@include file="incl_xls_footer.jsp"%>


</body>
</html>