<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.List"%>

<%@ include file="checklogin.jsp"%>


<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Metadata</h2>
			List of custom metadata attributes<br/>
			<font size="-2"></font>
		</th>

		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>

		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>


<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>

<table align="center" width="60%" cellpadding="5">
	<tr>
		<th>
		<th>custom field of metadata
		<th>
		<th>related experiment
		<th rowspan="331">
	<tr>
		<td rowspan="201" valign="top"><%@ include file="navi_tree.jsp"%>
		

<%

String pubQuery = "select question, experiment from formobject,  samples where description = \"custom field\"  and question != \"\" and samples.sampleID = formobject.PARENT group by question order by question";

	List pubList = CoreObject.createSession()
					.createSQLQuery(pubQuery)
					.addScalar("question", Hibernate.STRING)
					.addScalar("experiment", Hibernate.LONG)
					.list();

	Iterator i = pubList.iterator();
	while (i.hasNext()){
	    Object[] term = (Object[])i.next();
	    %>
	    	
	    	<tr>
	    		<td><%=term[0] %>
	    		<td align="center" style="background-color: white"><a href="dev_msiattribute.jsp?question=<%=term[0] %>"><img src="pics/go_msi.gif" border="0"></a>
	    		<td><a href="load?id=<%=term[1] %>&action=20"><%=term[1] %></a> <font class="small"><%=new SXQuery().findPromtTitleByPromtID(Long.parseLong(term[1].toString())) %> </font>
	    
	    <%
	}
%>

</table>


<%@ include file="footer.jsp"%>
