<%@page import="org.setupx.repository.Config"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="org.w3c.dom.EntityReference"%>
<%@page import="org.w3c.dom.ProcessingInstruction"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.setupx.repository.core.util.xml.XMLFile"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.setupx.repository.core.util.xml.XMLUtil"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.Iterator"%>
<%
	// util	
	DateFormat dateFormat = new SimpleDateFormat("EEE', 'dd' 'MMM' 'yyyy' 'HH:mm:ss' 'Z", Locale.US);// DateFormat.getDateInstance(DateFormat.FULL);

	// create an xml output representing the attributes devclared as MSI fields in SetupX

	Document doc = XMLUtil.createNewDocument();

	String data = "type=\"text/xsl\" href=\"rss.xslt\"";
	ProcessingInstruction pi = doc.createProcessingInstruction("xml-stylesheet",data); 
	//doc.appendChild(pi);
	
	doc.appendChild(doc.createElement("rss"));
	Element element;
	
	Element root = doc.getDocumentElement();
	root.setAttribute("version", "2.0");
	
	// @see http://cyber.law.harvard.edu/rss/rss.html#optionalChannelElements
	
	// the channel
	Node channel = doc.createElement("channel");
	root.appendChild(channel);
	// title	The name of the channel. It's how people refer to your service. If you have an HTML website that contains the same information as your RSS file, the title of your channel should be the same as the title of your website. 	GoUpstate.com News Headlines
	element = doc.createElement("title");
	element.appendChild(doc.createTextNode("MSI Attributes"));
	channel.appendChild(element);

	// link	The URL to the HTML website corresponding to the channel.	http://www.goupstate.com/
	element = doc.createElement("link");
	element.appendChild(doc.createTextNode("msi.jsp"));
	channel.appendChild(element);
	
	// description       	Phrase or sentence describing the channel.	The latest news from GoUpstate.com, a Spartanburg Herald-Journal Web site.
	element = doc.createElement("description");
	element.appendChild(doc.createTextNode("The latest MSI Standards as defined my the Metabolomics Society."));
	channel.appendChild(element);

	// language	The language the channel is written in. This allows aggregators to group all Italian language sites, for example, on a single page. A list of allowable values for this element, as provided by Netscape, is here. You may also use values defined by the W3C.	en-us
	element = doc.createElement("language");
	element.appendChild(doc.createTextNode("en-us"));
	channel.appendChild(element);
	
	// copyright	Copyright notice for content in the channel.	Copyright 2002, Spartanburg Herald-Journal
	element = doc.createElement("copyright");
	element.appendChild(doc.createTextNode("This work is licensed under a Creative Commons Attribution 3.0 United States License."));
	channel.appendChild(element);
	
	// managingEditor	Email address for person responsible for editorial content.	geo@herald.com (George Matesky)
	element = doc.createElement("managingEditor");
	element.appendChild(doc.createTextNode(Config.OPERATOR_MANAGER.getEmailAddress()));
	channel.appendChild(element);
	
	// webMaster	Email address for person responsible for technical issues relating to channel.	betty@herald.com (Betty Guernsey)
	element = doc.createElement("webMaster");
	element.appendChild(doc.createTextNode(Config.OPERATOR_MANAGER.getEmailAddress()));
	channel.appendChild(element);
	
	// pubDate	The publication date for the content in the channel. For example, the New York Times publishes on a daily basis, the publication date flips once every 24 hours. That's when the pubDate of the channel changes. All date-times in RSS conform to the Date and Time Specification of RFC 822, with the exception that the year may be expressed with two characters or four characters (four preferred).	Sat, 07 Sep 2002 00:00:01 GMT

	// lastBuildDate	The last time the content of the channel changed.	Sat, 07 Sep 2002 09:42:31 GMT

	// category	Specify one or more categories that the channel belongs to. Follows the same rules as the <item>-level category element. More info.	<category>Newspapers</category>

	// generator	A string indicating the program used to generate the channel.	MightyInHouse Content System v2.3
	element = doc.createElement("generator");
	element.appendChild(doc.createTextNode(Config.SYSTEM_NAME));
	channel.appendChild(element);
	
	// docs	A URL that points to the documentation for the format used in the RSS file. It's probably a pointer to this page. It's for people who might stumble across an RSS file on a Web server 25 years from now and wonder what it is.	http://blogs.law.harvard.edu/tech/rss

	// cloud	Allows processes to register with a cloud to be notified of updates to the channel, implementing a lightweight publish-subscribe protocol for RSS feeds. More info here.	<cloud domain="rpc.sys.com" port="80" path="/RPC2" registerProcedure="pingMe" protocol="soap"/>
	
	// ttl	ttl stands for time to live. It's a number of minutes that indicates how long a channel can be cached before refreshing from the source. More info here.	<ttl>60</ttl>
	element= doc.createElement("ttl");
	element.appendChild(doc.createTextNode("2"));
	channel.appendChild(element);

	// image	Specifies a GIF, JPEG or PNG image that can be displayed with the channel. More info here.	
	element = doc.createElement("image");
	Element element2 = doc.createElement("url");
	element2.appendChild(doc.createTextNode("pics/MSI-Icon.gif"));
	element.appendChild(element2);
	
	element2= doc.createElement("title");
	element2.appendChild(doc.createTextNode("MSI Icon"));
	element.appendChild(element2);
	
	element2 = doc.createElement("link");
	element2.appendChild(doc.createTextNode("msi.jsp"));
	
	element.appendChild(element2);

	channel.appendChild(element);

	
	// rating	The PICS rating for the channel.	

	// textInput	Specifies a text input box that can be displayed with the channel. More info here.	
	
	// skipHours	A hint for aggregators telling them which hours they can skip. More info here.	
	
	// skipDays	A hint for aggregators telling them which days they can skip. More info here.	

	
	
	


	// load all attributes
	Session s = CoreObject.createSession();
	List attributes = CoreObject.persistence_loadAll(MSIAttribute.class, s);
	
	
	
	
	Iterator iterator = attributes.iterator();
	while (iterator.hasNext()){
	    MSIAttribute attribute = (MSIAttribute)iterator.next();

	    Element item  = doc.createElement("item");
	    channel.appendChild(item);

	    //title	The title of the item.	Venice Film Festival Tries to Quit Sinking		
	    element = doc.createElement("title");
	    element.appendChild(doc.createTextNode(attribute.getMsiLabel()));
		item.appendChild(element);
		
		//link	The URL of the item.	http://nytimes.com/2004/12/07FEST.html
	    element = doc.createElement("link");
	    element.appendChild(doc.createTextNode("msi_detail_attribute.jsp?id=" + attribute.getUOID()));
		item.appendChild(element);		
		
		//description     	The item synopsis.	Some of the most heated chatter at the Venice Film Festival this week was about the way that the arrival of the stars at the Palazzo del Cinema was being staged.
	    element = doc.createElement("description");

		StringBuffer sb = new StringBuffer();
		sb.append("MSI Attribute " + attribute.getMsiLabel() + 
		        ".<br> Stored in SetupX-MSI-Repository SXUOID " + attribute.getUOID() + 
		        ". Created / modified by " + ((UserDO)CoreObject.persistence_loadByID(UserDO.class, s, attribute.getUserID())).getDisplayName() + " on " + attribute.getVersion().toLocaleString() + ". " + 
		        "<br>Info: " + attribute.getDescribtion() +
		        "<br>Comment: " + attribute.getComment() + 
		        "<br>"
		);

		// elements:
		List l = s.createSQLQuery("select uoid, question from formobject where parent > 0 and msiattribute = " + attribute.getUOID())
			.addScalar("uoid", Hibernate.STRING)
			.addScalar("question", Hibernate.STRING)
			.list();

		List valueList = s.createSQLQuery("select value from formobject where msiattribute = " + attribute.getUOID() + " group by value").addScalar("value", Hibernate.STRING).list();
		sb.append("<a href=msi_detail_attribute.jsp?id=" + attribute.getUOID() + ">" + l.size() + " different entries.</a>");
		
		
	    /*			
		Iterator iterator2 = valueList.iterator();

		while(iterator2.hasNext()){
			String value = iterator2.next().toString();
			sb.append(" <a href=\"msi_addattribute.jsp?msi=" + attribute.getUOID() + "&value=" + value + "\">" + value + " " + attribute.getExtension() + "</a>");
		}
		*/

		element.appendChild(doc.createTextNode(sb.toString()));
		item.appendChild(element);		
		
		//author	Email address of the author of the item. More.	oprah\@oxygen.net
	    element = doc.createElement("author");
	    UserDO userDO = (UserDO)CoreObject.persistence_loadByID(UserDO.class, s, attribute.getUserID());
	    element.appendChild( doc.createTextNode(userDO.getMailAddress().getEmailAddress() + " (" + userDO.getDisplayName() + ")"));
		item.appendChild(element);		
		
		//category	Includes the item in one or more categories. More.	 
		element = doc.createElement("comments");
	    element.appendChild(doc.createTextNode("msi_attribute_xml.jsp?id=" + attribute.getUOID()));
		item.appendChild(element);		

		//comments	URL of a page for comments relating to the item. More.	http://www.myblog.org/cgi-local/mt/mt-comments.cgi?entry_id=290
		element = doc.createElement("comments");
	    element.appendChild(doc.createTextNode("msi_attribute_xml.jsp?id=" + attribute.getUOID()));
		item.appendChild(element);		
		
		//enclosure	Describes a media object that is attached to the item. More.	
		
		//guid	A string that uniquely identifies the item. More.	http://inessential.com/2002/09/01.php#a2
	    element = doc.createElement("guid");
//	    element.appendChild(doc.createTextNode(attribute.getMsiLabel()));
	    element.appendChild(doc.createTextNode("msi_detail_attribute.jsp?id=" + attribute.getUOID()));
		item.appendChild(element);
		
		//pubDate	Indicates when the item was published. More.	Sun, 19 May 2002 15:21:36 GMT
	    element = doc.createElement("pubDate");
	    element.appendChild(doc.createTextNode(dateFormat.format(attribute.getVersion())));
		item.appendChild(element);		
		
		//source	The RSS channel that the item came from. More.	 
		element = doc.createElement("source");
	    element.appendChild(doc.createTextNode("msi_attribute_xml.jsp?id=" + attribute.getUOID()));
		item.appendChild(element);		

		
		// <comments>http://ekzemplo.com/entry/4403/comments</comments>		
		Element comment = doc.createElement("comments");
		comment.appendChild(doc.createTextNode("msi_attribute_xml.jsp?id=" + attribute.getUOID()));
		item.appendChild(comment);
		

		
	}
	out.clear();
	response.setContentType("text/xml");
	response.setCharacterEncoding("UTF-8");
	out.write(new XMLFile(doc).content());
%>

            	



<%!
public String iso8859Convert(String utf8String) throws java.io.UnsupportedEncodingException
{
byte[] bytes = new byte[utf8String.length()];
for (int i = 0; i < utf8String.length(); i++){
	bytes[i] = ' '; //(byte) utf8String.charAt(i);
}
return new String(bytes, "UTF-8");
}
%>

