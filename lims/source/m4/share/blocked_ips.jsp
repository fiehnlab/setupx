<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="checklogin.jsp"%>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>





<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.web.ip.BlockedIP"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIClassifierID"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.setupx.util.SystemCommand"%>
<%@page import="org.setupx.repository.Config"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Blocked IPs</title>
</head>
<body>




<body>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">	
			<img src='pics/logo.gif' border="0" valign="middle" align="center"  border="0">
			</a>
		</td>
		<th>
		
			<h2>Limited IP</h2><br>List of ips that have limited access.</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center"  border="0">
			</a>
		</td>
	</table>





<table align='center' border='0' cellpadding='4' width="60%">


<%

boolean cachedQueries = org.setupx.repository.Config.QUERY_CACHING_ACTIVE_JSP;

try {
	String cache = request.getParameter("cache");
    if (cache.compareTo("false") == 0){
        cachedQueries = false;
    }
} catch (Exception e){
    
}


	// most active IPs 
	String query = "select count(uoid) as number, remoteAddr from WebAccess group by remoteAddr order by count(uoid) desc limit 20 ";
%>
	<tr>
		<td>
			<table width="100%">
			  <tr>
			    <th colspan="3">20 most active IPs</th>
			  </tr>
			  <tr>
			    <th width="12"></th>
			    <th>IP</th>
			    <th></th>
			  </tr>
			
				<%
				Iterator l = CoreObject.createSession().createSQLQuery(query).addScalar("number", Hibernate.INTEGER).addScalar("remoteAddr", Hibernate.STRING).setCacheable(cachedQueries).list().iterator();
				while (l.hasNext()){
				   	Object[] objects = (Object[])l.next();
				    String access = objects[0] + "";
				    String ip = objects[1] + "";
			  %>
			  <tr>
			    <td>
			    	<% if(BlockedIP.isblocked(ip)){%>
					    <a href="ipblock.jsp?ip=<%=ip%>&block=false">
						    <img src="pics/locked.png" border="0"/>
						</a>
			    	<%} else { %>    	
					    <a href="ipblock.jsp?ip=<%=ip%>&block=true">
						    <img src="pics/unlocked.png"  border="0"/>
						</a>
			    	<%}  %>
			    	
			    	<br><font class="small"><%=access %> hits</font> 
			    	</td>
			    <td><%=ip %></td>
			    <td>
			    <%try {
			        %> <%=SystemCommand.whois(ip, "OrgName") %></td>
			        <%
			    }catch (Exception e){
			        
			    }
			    
			    %>
			    
			  </tr>
			  <%}  %>    	
			</table>
	</tr>
	
	
		<tr>
		<td width="100%">	
			<table width="100%">
			  <tr>
			    <th colspan="3">blocked / limited IPs</th>
			  </tr>
			  <tr>
			    <th width="12"></th>
			    <th>IP</th>
			    <th></th>
			  </tr>
			<%
				Iterator blockedIPs = new SXQuery().persistence_loadAll(BlockedIP.class, CoreObject.createSession()).iterator();
				while(blockedIPs.hasNext()){
				    BlockedIP blockedIP = (BlockedIP)blockedIPs.next();
			%>
			  <tr>
			  	<td>
				    <a href="ipblock.jsp?ip=<%=blockedIP.getBlockedIP()%>&block=false">
					    <img src="pics/locked.png"/  border="0">
					</a>
			    <td><%=blockedIP.getBlockedIP()%></td>
			    <td>
			    <%try {
			        %> <%=SystemCommand.whois(blockedIP.getBlockedIP(), "OrgName") %></td>
			        <%
			    }catch (Exception e){
			        
			    }
			    
			    %>
			    
			  </tr>
			<%
				}
			%>
			</table>
	
	
</table>




<%@ include file="footer.jsp"%>