
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Technology"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Location"%>
<%

	Session s = TechnoCoreObject.createSession();
	long technologyFileType = Long.parseLong(request.getParameter("technologyFileType"));
	String location = request.getParameter("location");
	
	TechnologyFileType fileType = (TechnologyFileType)TechnoCoreObject.persistence_loadByID(TechnologyFileType.class, s, technologyFileType);
	
	fileType.addDefaultlocation(new Location(location));
	
	fileType.getTechnology().updatingMyself(s,true);
	
	pageContext.forward("techno_technologyfiletype.jsp?id=" + fileType.getUOID());
%>
