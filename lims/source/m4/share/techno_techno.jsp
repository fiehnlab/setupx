<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net">
<jsp:directive.page import="org.setupx.repository.web.HttpSessionPool"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />

<jsp:directive.page import="org.setupx.repository.Config"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Technology"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"/>
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.hibernate.Hibernate"/>
<jsp:directive.page import="java.util.Vector"/>
  
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
  
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>

  <link rel="stylesheet" href="print.css" type="text/css" media="print" />
  
  </head>

	
	<jsp:scriptlet>
	
	HttpSessionPool.add(session);

	UserDO user = null;
	try {
	    user = (UserDO)session.getAttribute("user"); 
	    if (user.isPublic()){
	        throw new Exception("not a valid user");
	    }
	} catch (Exception e){
	    user = null;
	}

	if (user == null) { 
		</jsp:scriptlet>
		<p align="center"><b>you are not logged IN</b></p>
		<jsp:forward page="login.jsp"/>
		<jsp:scriptlet>
	 } else {  }

	
	
	boolean cachedQueries = org.setupx.repository.Config.QUERY_CACHING_ACTIVE_JSP;
	
	try {
		String cache = request.getParameter("cache");
	    if (cache.compareTo("false") == 0){
	        cachedQueries = false;
	    }
	} catch (Exception e){
	    
	}
	</jsp:scriptlet>
  <body>
	<jsp:include page="techno_header.jsp"></jsp:include>

	<h2>
		<a href="techno_techno.jsp">all platforms</a>
	</h2>


	<table>
		<tr>
			<th>
				Timeline of assigned datafiles
			</th>
		</tr>
		<tr>
			<td>
				<jsp:include page="dev_graph.jsp"></jsp:include>
				<br/>
				<br/>
				<font style="font-size: xx-small;">Timeline containing all datafiles stored in the system. Move slider left and right or adjust the windows by moving the start and end on the lower timeline left or right.</font>
			</td>
		</tr>
	</table>


	<br/>

	<jsp:scriptlet>
	request.setAttribute("test", TechnologyProvider.getTechnologies()); 

	// style
	pageContext.setAttribute("tableclass", "its");
	
	</jsp:scriptlet>
	The following Technology Platforms are connected to the system.


	<jsp:scriptlet>
	
		String q = "select technology.label as a, technologyfiletype.label as b, count(distinct datafile.uoid ) as c , count(distinct datafile.sampleID) as d"
			+ " from datafile join technologyfiletype on technologyfiletype.uoid = datafile.technologyfiletype join technology on technology.uoid = technologyfiletype.technology group by technologyfiletype";
		
		List list = TechnoCoreObject.createSession().createSQLQuery(q).addScalar("a", Hibernate.STRING).addScalar("b", Hibernate.STRING).addScalar("c", Hibernate.LONG).addScalar("d", Hibernate.LONG).setCacheable(cachedQueries).list();
		
		Iterator iterator = list.iterator();
		while(iterator.hasNext()){
		    Object[] objects = (Object[])iterator.next();
		    String techLabel = objects[0].toString();
		    String techfileLabel = objects[1].toString();
		    long nrOfDatafiles = Long.parseLong(objects[2].toString());
		    long nrOfSamples = Long.parseLong(objects[3].toString());
		}
	</jsp:scriptlet>


	<display:table name="test" id="row">
		<display:column property="label" href="techno_technology.jsp?" paramId="id" paramProperty="UOID" sortable="true"></display:column>
	    <display:column property="description" sortable="true" headerClass="sortable"/>
		<display:column href="techno_technology_remove.jsp?" paramId="id" paramProperty="UOID"><img src="pics/trash.gif"/></display:column>
	    <display:column title="number of Filetypes" sortable="true">
	    	<jsp:expression>((Technology)row).getTechnologyFileTypes().size()</jsp:expression>
	    </display:column>
	</display:table>


	<form action="techno_technology_create.jsp">
		<table align="center">
			<tr>
				<th colspan="2">
					Create a new Technology platform
				</th>
			</tr>
			<tr>
				<td colspan="2">
					In order to add a new Technology platform to your system please enter a label that describes the technology in one or two words and enter a more detailed description below then. 
				</td>
			</tr>
			<tr>
				<td>Label</td>
				<td><input type="text" name="label"/></td>
			</tr>
			<tr>
				<td>Descr.</td>
				<td><input type="text" name="describtion" size="50"/></td>
			</tr>
			<tr>
				<td>create</td>
				<td><input type="submit" value="create new Technology"/></td>
			</tr>
		</table>
	</form>


<form action="techno_searchFiles.jsp">
	<table>
		<tr>
			<th>
				List datafiles 
			</th>
		</tr>
		<tr>
			<td>
				<input type="submit" value="list for datafiles"/>
			</td>
		</tr>
	</table>
</form>




<form action="techno_sampleFiles.jsp">
	<table>
		<tr>
			<th>
				List datafiles for sample
			</th>
		</tr>
		<tr>
			<td>
				Enter the sample ID and the system will list all available datafiles for this specific sample that are assigned in the system right now.
			</td>
		</tr>
		<tr>
			<td>
				<input type="text" name="id"/>
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit" value="search for datafiles"/>
			</td>
		</tr>
	</table>
</form>

<form action="techno_msample_massign.jsp">
	<table>
		<tr>
			<th colspan="2">
				Assign multiple datafiles to multiple samples
			</th>
		</tr>
		<tr>
			<td colspan="2">
				In order to assign a large number of samples to a large number of files use this wizzard. It will ask you for the samples IDs and parts of the coressponding filename. 
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="submit" value="search for datafiles"/>
			</td>
		</tr>
	</table>
</form>
</body>

	</jsp:root>