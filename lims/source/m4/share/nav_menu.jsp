<!DOCTYPE HTML PUBL		IC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style>
BODY {	margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; }

		
.leftMenu {	text-align: left; }
		
.centerMenu { text-align: center;}
		
.rightMenu { text-align: right;	}
		
a.MenuLabelLink	{ COLOR: #000066;	FONT-SIZE: 14px;
FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None;
margin: 0px; padding: 0px; font-weight: bold; }
a.MenuLabelLink:link { COLOR: #000066;	FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None; }
a.MenuLabelLink:visited	{ COLOR: #000066; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None;	}
a.MenuLabelLink:hover{ COLOR: #000066; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None; }
		
a.MenuLabelLinkOn {	COLOR: #ff00ff; FONT-SIZE: 14px;
FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None;
margin: 0px; padding: 0px; font-weight: bold; }
a.MenuLabelLinkOn:link { COLOR: #ffffff; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None; }
a.MenuLabelLinkOn:visited { COLOR: #ffffff; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None; }
a.MenuLabelLinkOn:hover { COLOR: #ffffff; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None; }
		
a.MenuItemLink { COLOR: #000066; FONT-SIZE: 14px;
FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None;
margin: 0px; padding: 0px; font-weight: bold; }
a.MenuItemLink:link { COLOR: #000066; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None; }
a.MenuItemLink:visited { COLOR: #000066; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None; }
a.MenuItemLink:hover { COLOR: #000066; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None; }
		
a.MenuItemLinkOn { COLOR: #ffffff; FONT-SIZE: 14px;
FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None;
margin: 0px; padding: 0px; font-weight: bold; }
a.MenuItemLinkOn:link { COLOR: #ffffff; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None; }
a.MenuItemLinkOn:visited { COLOR: #ffffff; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None; }
a.MenuItemLinkOn:hover { COLOR: #ffffff; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: None; }
		
.myMenu { position: absolute; visibility: hidden; z-index: 5; border-color: black; border: 1;}		
		
.myMenuLabelleft { padding: 0px 0px 0px 0px; text-align: left; }		
.myMenuLabelcenter { padding: 0px 0px 0px 0px; text-align: center; }		
.myMenuLabelright { padding: 0px 0px 0px 0px; text-align: right; }		
.myMenuItemleft { padding: 0px 0px 0px 0px; text-align: left; }		
.myMenuItemcenter { padding: 0px 0px 0px 0px; text-align: center; }		
.myMenuItemright { padding: 0px 0px 0px 0px; text-align: right; }		
		
#myTest { 
width: 600px;
padding: 0px 0px 0px 0px;
z-index: 1;
}
</style>
<script language="JavaScript1.2" src="api.js" type="text/javascript">
<!-- 

-->
</script>
<script language="JavaScript1.2" src="menucode.js" type="text/javascript">
<!-- 

-->
</script>
<script language="JavaScript1.2" type="text/javascript">
<!--
sniffBrowsers();

menuItemBullet = new bulletPoint("pics/bullets/one/menu_off.gif","bullets/one/menu_on.gif");
labelBullet = new bulletPoint("pics/bullets/one/header_off.gif","bullets/one/header_on.gif");
subMenuBullet = new bulletPoint("pics/bullets/one/sub_header_off.gif","bullets/one/sub_header_on.gif");

<% // Administration  	Track  	MSI  	Repository  	public  	my Experiments  	Users  	Search  	Help %>

myTest = new menuBar('myTest',480, 'horizontal', '#000000', '#000000');
myTest.addLabel('labelBullet', 'SetupX', 1, 180, '#E7f3ff', '#0000aa', 'main.jsp', 'left');
myTest.addLabel('labelBullet', 'Experiments', 2, 180, '#E7f3ff', '#0000aa', '', 'left');
myTest.addLabel('labelBullet', 'Laboratory', 3, 180, '#E7f3ff', '#0000aa', 'admin.jsp', 'left');
myTest.addLabel('labelBullet', 'Search', 3, 180, '#E7f3ff', '#0000aa', 'q_main_ajax.jsp?type=99', 'left');
myTest.addLabel('labelBullet', 'Exit', 4, 180, '#CC0000', '#0000aa', 'login.jsp', 'left');
myTest.height = 20;

menus[1] = new menu(200, 'vertical', '#000000', '#000000');
menus[1].height = 25;
menus[1].addItem('menuItemBullet', 'MSI', null, 135, '#E7f3ff', '#0000aa', 'msi.jsp', 'left');
menus[1].addItem('menuItemBullet', 'Fiehn Lab', null, 135, '#E7f3ff', '#0000aa', 'http://fiehnlab.ucdavis.edu', 'left');
menus[1].addItem('menuItemBullet', 'HowTo', null, 135, '#E7f3ff', '#0000aa', '', 'left');
menus[1].writeMenu();

menus[2] = new menu(200, 'vertical', '#000000', '#000000');
menus[2].height = 25;
menus[2].addItem('menuItemBullet', 'My Experiments', null, 125, '#E7f3ff', '#0000aa', 'main.jsp', 'left');
menus[2].addItem('menuItemBullet', 'Create new Experiment', null, 125, '#E7f3ff', '#0000aa', 'load?id=0', 'left');
menus[2].addItem('subMenuBullet',  'Repository', 5, 121, '#E7f3ff', '#0000aa', 'content.jsp', 'left');
menus[2].addItem('menuItemBullet', 'Public Data', null, 121, '#E7f3ff', '#0000aa', 'main_public.jsp', 'left');
menus[2].writeMenu();

menus[3] = new menu(200, 'vertical', '#000000', '#000000');
menus[3].addItem('subMenuBullet', 'Lab-Equipment', 6, 121, '#E7f3ff', '#0000aa', 'admin.jsp', 'left');
menus[3].addItem('menuItemBullet', 'Messages', null, 121, '#E7f3ff', '#0000aa', 'messages.jsp?type=4', 'left');
menus[3].addItem('subMenuBullet', 'File Handling', 7, 121, '#E7f3ff', '#0000aa', '', 'left');
menus[3].height = 25;
menus[3].writeMenu();

menus[4] = new menu(200, 'vertical', '#000000', '#000000');
menus[4].height = 25;
menus[4].addItem('menuItemBullet', 'Content of the Repository', null, 135, '#E7f3ff', '#0000aa', 'content.jsp', 'left');
menus[4].addItem('menuItemBullet', 'List of ALL Experiments', null, 135, '#E7f3ff', '#0000aa', 'main_all.jsp', 'left');
menus[4].writeMenu();

menus[5] = new menu(300, 'vertical', '#000000', '#000000');
menus[5].height = 25;
menus[5].addItem('menuItemBullet', 'Content of the Repository', null, 135, '#E7f3ff', '#0000aa', 'content.jsp', 'left');
menus[5].addItem('menuItemBullet', 'List of ALL Experiments', null, 135, '#E7f3ff', '#0000aa', 'main_all.jsp', 'left');
menus[5].writeMenu();

menus[6] = new menu(300, 'vertical', '#000000', '#000000');
menus[6].height = 25;
menus[6].addItem('menuItemBullet', 'Status', null, 135, '#E7f3ff', '#0000aa', 'adminstatus.jsp', 'left');
menus[6].addItem('menuItemBullet', 'Status (tracker)', null, 135, '#E7f3ff', '#0000aa', 'adminstatus.jsp', 'left');
menus[6].addItem('menuItemBullet', 'Acquisition', null, 135, '#E7f3ff', '#0000aa', 'aq/query.jsp', 'left');
menus[6].addItem('menuItemBullet', 'Methods', null, 135, '#E7f3ff', '#0000aa', 'machine_method_assign.jsp', 'left');
menus[6].addItem('menuItemBullet', 'Result Data', null, 135, '#E7f3ff', '#0000aa', 'resultfiles.jsp', 'left');
menus[6].addItem('menuItemBullet', 'User Administration', null, 135, '#E7f3ff', '#0000aa', 'user.jsp', 'left');
menus[6].writeMenu();

menus[7] = new menu(300, 'vertical', '#000000', '#000000');
menus[7].height = 25;
menus[7].addItem('menuItemBullet', 'Single Run', null, 135, '#E7f3ff', '#0000aa', 'assign_samples.jsp', 'left');
menus[7].addItem('menuItemBullet', 'Entire Experiment', null, 135, '#E7f3ff', '#0000aa', 'assign_samples.jsp', 'left');
menus[7].writeMenu();

menus[1].align='left';
menus[2].align='left';
menus[3].align='left';
menus[4].align='right';
menus[5].align='right';
menus[6].align='right';
menus[7].align='right';



//-->
</script>


<script language="JavaScript1.2" type="text/javascript">
<!--
myTest.writeMenuBar();
//-->
</script>

<%
/*

Vector linksPersonal = new Vector();
linksPersonal.add(new Link("new Experiment", "load?id=0"));
linksPersonal.add(new Link("my Experiments","main.jsp"));
linksPersonal.add(new Link("public data", "main_public.jsp"));


Vector linksLabTech = new Vector();
linksLabTech.add(new Link("administration","admin.jsp"));
linksLabTech.add(new Link("lab status","adminstatus.jsp"));
linksLabTech.add(new Link("tracking","sample_status_create.jsp", "pics/barcode.jpg"));
linksLabTech.add(new Link("acquistion","aq/query.jsp"));

Vector linksPub = new Vector();
linksPub.add(new Link("publish data", "pubxlsimport0.jsp"));
linksPub.add(new Link("public data", "main_public.jsp"));


Vector linksQuery = new Vector();
linksQuery.add(new Link("Query Frontend", "q_main_ajax.jsp", "pics/details.gif"));
linksQuery.add(new Link("Query Stats","query_cache.jsp","pics/stats.gif"));
// linksQuery.add(new Link("Query Test", "querytest.jsp"));
linksQuery.add(new Link("Access Statistic","access2.jsp"));




if (user.isMasterUser(user.getUOID())) linksLabTech.add(new Link("users","user.jsp","pics/icon_forum.gif"));
if (user.isMasterUser(user.getUOID())) linksLabTech.add(new Link("load nonedit mode", "load_nonedit.jsp", "pics/warning.gif"));
linksLabTech.add(new Link("export by regrouping", "export_C_promtselect.jsp"));

Vector linksAdmin = new Vector();

linksAdmin.add(new Link("machine methods", "machine_method_assign.jsp"));
linksAdmin.add(new Link("machine setup", "machine.jsp"));
linksAdmin.add(new Link("messages", "messages.jsp?type=1"));
linksAdmin.add(new Link("taxonomy abbreviation", "abbreviation.jsp"));
linksAdmin.add(new Link("taxonomy ", "status_info.jsp"));
linksAdmin.add(new Link("ontology test","obo_test.jsp?term=seed&ontology=po_anatomy.obo"));
linksAdmin.add(new Link("assign samples", "assign_sample.jsp"));
linksAdmin.add(new Link("assign sample", "assign_samples.jsp"));
linksAdmin.add(new Link("data curation","curator.jsp"));
linksAdmin.add(new Link("custom metadata","metadata_list.jsp"));

        
        
Vector links = new Vector();
links.add(new Link("main","main.jsp","pics/icon_home.gif"));
links.add(linksQuery);
links.add(linksPub);
links.add(linksPersonal);
if (user.isLabTechnician()) links.add(linksLabTech);
if (user.isMasterUser(user.getUOID()))links.add(linksAdmin);

links.add(new Link("exit","login.jsp"));
*/
%>