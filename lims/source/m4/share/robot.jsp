<%@page import="org.setupx.repository.web.ip.BlockedIP"%>
<%@page import="org.setupx.repository.server.logging.Message"%>
<%@page import="org.setupx.repository.core.util.web.WebException"%>
<%

// Handling queries from search engines.

// it actually checkes the load of the machine answers queries only if load is low.

if (BlockedIP.isblocked(request.getRemoteAddr())){
    new Message(this, "blocked " + request.getRemoteAddr(), "The request from " + request.getRemoteAddr() + " was blocked. The request was " + 
            request.getQueryString() + " " + 
			request.getRequestURI() 
    ).update(true); 
    throw new WebException("ip is temp. blocked");
}
%>