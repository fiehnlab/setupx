<meta http-equiv="refresh" content="60; URL=admin.jsp">

	<%@ include file="checklogin.jsp"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="org.setupx.repository.web.HttpSessionPool"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.util.SystemCommand"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.communication.leco.Method"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.server.persistence.PersistenceActionFindException"%>
<%@page import="org.setupx.repository.server.persistence.SXQueryCacheObject"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashSet"%><%@page import="org.hibernate.criterion.Expression"%>
<%@page import="org.setupx.repository.server.logging.Message"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<body>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">	
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Administration</h2><br>Manage UserAccess, View status of samples, ...</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>


<%

java.util.Locale locale = java.util.Locale.US;

org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

hqlSession.close();
%>



<%@ include file="navi_admin.jsp"%>

<table align='center' border='0' cellpadding='4' width="60%">

<tr>
<th></th>
<th colspan="4">LabEquipment Communication</th>
<th></th>
</tr>







<tr>
<td rowspan="201" valign="top"><%@ include file="navi_tree.jsp"%>
<td></td>
<td>Filemanagement<br><font class="small">Manage / assign Datafiles to technology platforms</font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="techno_techno.jsp"><img  border='0' src="pics/go_data.gif"></a></td>
<td></td>
</tr>




<tr>
<td></td>
<td>Acquistion<br></s><font class="small">create a new acquisition task for the <b>LTQ / Nanomate</b></font></td>
<td></td>
<td></td>
<td><a href="seq_ltq.jsp"><img  border='0' src="pics/go_setup.gif"></a></td>
<td></td>
</tr>

<tr>
<td></td>
<td>Acquistion<br></s><font class="small">create a new acquisition task for the <b>GC</b></font></td>
<td></td>
<td></td>
<td><a href="aq/query.jsp"><img  border='0' src="pics/go_setup.gif"></a></td>
<td></td>
</tr>



<tr>
<th></th>
<th colspan="4">Data / SOPs</th>
<th></th>
</tr>

<tr>
<td></td>
<td>Publish<br><font class="small">publish your data</font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="pubxlsimport0.jsp"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>

<tr>
<td></td>
<td>SOPs<br><font class="small">Standard Operation Procedures / Docuemnts</font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="sop_index.jsp"><img  border='0' src="pics/go_sop.gif"></a></td>
<td></td>
</tr>



<tr>
<th></th>
<th colspan="4">User Administration</th>
<th></th>
</tr>

<tr>
<td></td>
<td>Users<br><font class="small">Total number of user useraccounts.</font></td>
<td>create, modify, assign experiments, ...</td>
<td align="center"></td>
<td width="2"><a href="user.jsp"><img  border='0' src="pics/go_share.gif"></a></td>
<td></td>
</tr>


<tr>
<td></td>
<td>create new User</td>
<td></td>
<td></td>
<td><a href="user.jsp"><img  border='0' src="pics/go_user_create.gif"></a></td>
<td></td>
</tr>



<!-- open sessions -->
<tr>
<th></th>
<th colspan="4">Logged in Users <br><font class="small">all open sessions</th>
<th></th>
</tr>
		<% 
			// list of sessions
			java.util.Hashtable sessions = org.setupx.repository.web.HttpSessionPool.getSessions();
			
			java.util.Enumeration enumeration = sessions.keys();
			while (enumeration.hasMoreElements()){
			    String key = (String)enumeration.nextElement();
				HttpSession httpSession = (javax.servlet.http.HttpSession)sessions.get(key);
				
				try {
				UserDO _user = (UserDO)httpSession.getAttribute(WebConstants.SESS_USER);
				
				
				// checking for sessions that are old
				long diff = new java.util.Date().getTime() - new java.util.Date(httpSession.getLastAccessedTime()).getTime();
				
				if (diff > 1800000) throw new IllegalStateException("outdated");
				%>
			<tr>
			<td></td>
			<td></td>
			<td colspan="3" >
				<table align="right" border="0">
					<tr>
						<td colspan="2"  align="center"><b><%=_user.getDisplayName() %> <a href="mailto:<%=_user.getMailAddress().getEmailAddress()%>"><img  border='0' src="pics/mail.gif"></a></td>
						<td align="center"><b></b><%=_user.getLastExperimentID()%></b> </td>
						
					<tr>
						<td style="background-color:LIGHTGREEN"><font class="small">Login:
						<td style="background-color:LIGHTGREEN"><%=new java.util.Date(httpSession.getCreationTime()).toLocaleString() %></td>
						<td rowspan="2" align="center"><a href="load?id=<%=_user.getLastExperimentID()%>&action=<%=PromtUserAccessRight.READ%>" title="Load the Experiment  <%=new SXQuery().findPromtTitleByPromtID(_user.getLastExperimentID()) %>"><img  border='0' src="pics/go.gif"></a> </td>					
					<tr>
		
						<td style="background-color:RED"><font class="small">Last activity:
						<td style="background-color:RED"><%=new java.util.Date(httpSession.getLastAccessedTime()).toLocaleString()%></td>
					</tr>
				</table>
			</td>
			<td></td>
			</tr>
				<%
			} catch(IllegalStateException exception){
			    HttpSessionPool.getSessions().remove(key);
			    exception.printStackTrace();
			} // catch
			} // while
			%>
<!-- end open sessions -->


<tr>
<th></th>
<th colspan="4">Status</th>
<th></th>
</tr>


<tr>
<td></td>
<td>Load<br><font class="small">Check the number of requests</font></td>
<td colspan="2" align="center">
</td>
<td width="2"><a href="dev_access.jsp"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>



<tr>
<td></td>
<td>Messages<br><font class="small">See what is / was happening.</font></td>
<td colspan="2" align="center">
</td>
<td width="2"><a href="messages.jsp?type=4"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>



<tr>
<td></td>
<td>Connections<br><font class="small">See status of services connected to the LIMS</font></td>
<td colspan="2" align="center">
</td>
<td width="2"><a href="connections.jsp"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>


<tr>
<td></td>
<td>System HDD<br><font class="small">Capazity on the connected HDD</font></td>
<td colspan="2" align="left">
<%=SystemCommand.uptime().replaceAll("\n", "<br>")%>
<p>
<%=SystemCommand.df().replaceAll("\n", "<br>")%>


</td>
<td width="2">

</td>
<td></td>
</tr>





<tr>
<th></th>
<th colspan="4">File handling</th>
<th></th>
</tr>

<tr>
<td></td>
<td colspan="4">Use this option to mark samples as "successfully run". Use this option <B>only</B> if you are absolutly sure that this sample has run!<br><font class="small">If you are not 100% sure - contact the admin.</font></td>
<td></td>
</tr>

<tr>
<td></td>
<td>single run <br><font class="small">If you have a single file that you want to assign to a sample.</font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="assign_sample.jsp"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>

<tr>
<td></td>
<td>experiment<br><font class="small">If you have a whole experiment where you want to assign the comment as filename.<br>Adds by default _1 as the run to the comment field.</font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="assign_samples.jsp"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>

<tr>
<td></td>
<td>files and ids<br><font class="small">If you have a list containing the sampleIDs and the filenames use this option</font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="assign_samples2.jsp"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>






<tr>
<th></th>
<th colspan="4">Development!</th>
<th></th>
</tr>

<tr>
<td></td>
<td>Ontology Test<br><font class="small"><a href="http://obo.sourceforge.net/main.html">Open Biomedical Ontologies</a></font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="obo_test.jsp?term=seed&ontology=po_anatomy.obo"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>
<tr>
<td></td>
<td>XLS Mapper<br><font class="small"></font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="xlsimport2.jsp"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>


<tr>
<td></td>
<td>Machines<br><font class="small">List of all machines in the Lab - Define new machines.</font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="machine.jsp"><img  border='0' src="pics/go_setup.gif"></a></td>
<td></td>
</tr>


<tr>
<td></td>
<td>Methods <br><font class="small">Assign methods used for the acquistion </font></td>
<td colspan="2">
<%

hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

int unassigned = hqlSession.createSQLQuery("select {method.*} from machinemethod method where method.machine is null order by method.label").addEntity("method", Method.class).list().size();

if (unassigned > 0){
%>

<font class=error>There are <%=unassigned%> methods that are not assigned to one of the machines.</font>
<%}%>
</td>
<td width="2"><a href="machine_method_assign.jsp"><img  border='0' src="pics/go_setup.gif"></a></td>
<td></td>
</tr>


<tr>
<td></td>
<td>Results<br><font class="small">Results delivered from BinBase</font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="resultfiles.jsp"><img  border='0' src="pics/go_download.gif"></a></td>
<td></td>
</tr>


</table>

<%@ include file="footer.jsp"%>