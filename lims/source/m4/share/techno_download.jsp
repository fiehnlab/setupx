<%@ include file="checklogin.jsp"%>

<%@page import="java.util.List"%>
<%@page import="java.util.Vector"%>
http://setupx.fiehnlab.ucdavis.edu:8080/m1/techno_download.jsp?t=file&id=282674&files=3

http://setupx.fiehnlab.ucdavis.edu:8080/m1/techno_download.jsp?t=file&id=282673&files=15

<%
    // Download of techno platform files 

    // request: 
    // t=class ... 		determining which group it is / classes [file, sample, class, prompt ]
    // id=41223 	ID of the object
    // zip=true 	zip it or not
    // files=all	all files [all, 4"ID of the filetype"]

    // default values
    String type = "sample";
    long id = 0;
    boolean zip = false;
    long fileSelect = 0;

    try {
        if (request.getParameter("t").compareTo("null") == 0) throw new Exception("illegal type");
        type = request.getParameter("t");
    } catch (Exception e) {

    }

    try {
        id = Long.parseLong(request.getParameter("id"));
    } catch (Exception e) {

    }
    try {
        zip = (request.getParameter("zip").compareTo("true") == 0);
    } catch (Exception e) {

    }
    try {
        fileSelect = Long.parseLong(request.getParameter("files"));
    } catch (Exception e) {

    }

    // in case it is multiple types it has to be zipped
    if ((fileSelect == 0) || (type.compareTo("file") != 0))
        zip = true;
%>


type: <%=type %>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"%>
<%@page import="java.util.zip.ZipOutputStream"%>
<%@page import="org.setupx.repository.web.DownloadServlet"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.util.zip.ZipEntry"%>
<%@page import="java.io.IOException"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Datafile"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="java.util.zip.ZipException"%>
<%@page import="java.io.FileNotFoundException"%>
<br>
id: <%=id %>
<br>
zip: <%=zip %>
<br>
fileSelect: <%=fileSelect %>
<br>

<% 
	// determining which files to include
	List listOfFiles = new Vector();

	// createing an inner select for the sample IDs
	String sqlFileIDs = "select datafile.* from datafile ";
	if (type.compareTo("file") == 0){
	    sqlFileIDs = sqlFileIDs.concat("where uoid = " + id );
	} else if (type.compareTo("sample") == 0){
	    sqlFileIDs = sqlFileIDs.concat("where sampleID = " + id);
	} else if (type.compareTo("class") == 0){
	    sqlFileIDs = sqlFileIDs.concat("join samples on samples.sampleID = datafile.sampleID where samples.classID = "+ id);
	} else if (type.compareTo("promt") == 0){
	    sqlFileIDs = sqlFileIDs.concat("join samples on samples.sampleID = datafile.sampleID where samples.experiment = " + id);
	}


	if (fileSelect == 0){
	    // thats it - no filtering
	} else {
	    sqlFileIDs = sqlFileIDs.concat(" and datafile.technologyfiletype = " + fileSelect);
	}
%>

<%=sqlFileIDs%>
<br>
result: 
<%=TechnoCoreObject.createSession().createSQLQuery(sqlFileIDs).list().size()%>

<%
// creating a vector with all the filenames in it
Logger.debug(this, sqlFileIDs);
List files = TechnoCoreObject.createSession().createSQLQuery(sqlFileIDs).addEntity(Datafile.class) .list();
 

%>


<%
// creating the actual download file
	    // Create a buffer for reading the files
	    byte[] buf = new byte[1024];
	    
	    try {
	        // Create the ZIP file
	        ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream());
		    response.setHeader("Content-Disposition","inline;filename=" + type + "_" + id + ".zip");
		    response.setContentType(DownloadServlet.CONTENT_TYPE_ZIP);

	    
	        // Compress the files
	        Iterator fileIterator = files.iterator();
	        //for (int i=0; i < filenames.length; i++) {
            Logger.debug(this, "zipping " + files.size() + " files.");
            boolean isFirstSample = true;
			while (fileIterator.hasNext()){
			    Datafile datafile = (Datafile)fileIterator.next();
			    String filenameOriginal = datafile.getSource();

			    File fileOriginal = new File(filenameOriginal);
			    
			    long promtID = new SXQuery().findPromtIDbySample(datafile.getSampleID());
			    if (isFirstSample){
			        PromtUserAccessRight.checkAccess(user.getUOID(), promtID, PromtUserAccessRight.DOWNLOAD_RESULT);
			        isFirstSample = false;
			    }
			    long classID = new SXQuery().findClazzIDBySampleID((int)datafile.getSampleID());
			    
			    String fileNewFilenamePath = "tmp" + File.separator + "exp_" + promtID + File.separator + "class_" + classID + File.separator + "sample_" + datafile.getSampleID() + File.separator + datafile.getTechnologyFileType().getTechnology().getLabel() + File.separator + datafile.getTechnologyFileType().getLabel() + File.separator;
			    File path = new File(fileNewFilenamePath);
		        if (!path.exists()){
		            path.mkdirs();
		        } 
		        String fileNewFilename = fileNewFilenamePath + File.separator + fileOriginal.getName();
			    Logger.log(this, "tmp filename " + fileNewFilename);
				
			    try {
				    File fileNewTMP = new File(fileNewFilename);
				    org.setupx.repository.core.util.File.copyFile(fileOriginal, fileNewTMP);
		        	
				    String filename = fileNewTMP.getPath();
			        
				    // copying the file to a tmp location
		            FileInputStream in = new FileInputStream(filename);
		    
		            Logger.debug(this, "zipping " + filename);
		            
		            // Add ZIP entry to output stream.
		            // zipOutputStream.setMethod(ZipOutputStream.STORED); 
		            zipOutputStream.setLevel(0);
					try {
			            zipOutputStream.putNextEntry(new ZipEntry(filename));
			            
			            // Transfer bytes from the file to the ZIP file
			            int len;
			            while ((len = in.read(buf)) > 0) {
			                zipOutputStream.write(buf, 0, len);
			            }
			    
			            // Complete the entry
			            zipOutputStream.closeEntry();
			            in.close();
					} catch (ZipException zipException) {
					    // usual for dublicated files...
					    Logger.err(this, zipException.getMessage());
					    
					}
		            
		            // remove tmpfile
		            fileNewTMP.delete();
			    } catch (FileNotFoundException e){
			        e.printStackTrace();
			    }
	        }
	    
	        // Complete the ZIP file
	        zipOutputStream.close();
	    } catch (IOException e) {
	        e.printStackTrace();
	        response.reset();
	        %><%=e.getMessage() %><%
	    }
	
%>


	