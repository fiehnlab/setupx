<%@ include file="checklogin.jsp"%>

<body>
<!--  head containing logo and description -->

	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Report</h2>
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>

	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} %>
	

<table align="center" width="60%" cellpadding="5">	
	<!--  back to the menu -->


	<% if (!user.isLabTechnician()){ %>
	<tr>
		<td colspan="1" align="right" style="background-color:#FFFFFF"><a href="main.jsp"><img  border='0' src="pics/back.gif"></td>
		<td colspan="8" style="background-color:#6682B6" ></td>
		<td colspan="1" style="background-color:#6682B6" ></td>
	</tr>
	<%} %>




<%
//		REPORT WITHOUT FORMAT
// 		for statistics.
%>


 	<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.web.forms.inputfield.StringInputfield"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>



<%
long promtID = Long.parseLong(request.getParameter("id"));

%>



<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<th><a href="load?id=<%=promtID %>&action=20"><img src="pics/details.gif" border="0"></a>  <%=new SXQuery().findPromtTitleByPromtID(promtID) %>
	<tr>
		<td><%=new SXQuery().findPromtAbstractByPromtID(promtID) %>
	<tr>
		<td><%=new SXQuery().findPromtCollaborationHTML(promtID) %>
	<tr>
		<td><%=new SXQuery().findSpeciesbyPromtID(promtID).get(0) %>
</table>



<table align='center' border='0' cellpadding='4' width="60%">
<tr>
	<th>sample
	<th>label
	<th>class
	<th colspan="21">variations


<%

List sampleIDs = new SXQuery().findSampleIDsByPromtID(promtID);

Iterator iterator = sampleIDs.iterator();

while(iterator.hasNext()){
 	%>
<tr>
 	<%
    
	// sample
	long sampleID = Long.parseLong(iterator.next().toString());
	
	%>
	<td><a href="sample_detail.jsp?id=<%=sampleID %>"><%=sampleID%></a>
	<%
	
	
	// samplelabel
	%>
	
	<%
	
	// sample description
	%>
	<td><%=new SXQuery().findSampleLabelBySampleID(sampleID)%>
	<%
	
	
	// classID
	int classID = Integer.parseInt(new SXQuery().findClazzIDBySampleID((int)sampleID) + "");
	
	%>
	<td style="background-color: <%=Util.getColor(classID) %>"><%=classID %>
	<%
	
	%>
	<td>
		<%=Util.replace(  new SXQuery().findClassInformationString(new SXQuery().findClazzIDBySampleID((int)sampleID)), "\n", "<td	>")%>




	<%
	Sample sample = Sample.loadByID(sampleID);
	String label_value = "";
	String label_key = "";
	
	for (int mySampleCounter = 0; mySampleCounter < sample.getFields().length ; mySampleCounter++){
	    if (sample.getField(mySampleCounter) instanceof StringInputfield){
	        label_key = "" +  ((StringInputfield)sample.getField(mySampleCounter)).getQuestion();
	        label_value = "" + ((StringInputfield)sample.getField(mySampleCounter)).getValue();
	        if (label_value.length() > 0){
	        %>
		<td style="background-color: white">
				<font style="font-size: xx-small;"><%=label_key %>:</font>
		<td>
			<%=label_value %>
			<%
			try {
				if (((StringInputfield)sample.getField(mySampleCounter)).getMSIAttribute() != null){%>
					<%=(((StringInputfield)sample.getField(mySampleCounter)).getMSIAttribute().getExtension())%>
				<%}
			} catch (Exception e){
				Logger.err(this, "can not find extension for msi attribute.");
			}
	        }
	    }
	}
}	
%>
	
