<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Vector"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%@ include file="checklogin.jsp"%>

<%
// checking if ids are beeing added
try {
    String value = request.getParameter("add");
	if (value.length() > 1){
	    Vector selectedIDs = null;
	    try {
	        selectedIDs = (Vector)session.getAttribute("selectedPromts");
	        if (selectedIDs == null) throw new NullPointerException();
	    } catch (Exception e){
	        selectedIDs = new Vector();
	    }
	    
	    
	    selectedIDs.add(value);
	    session.setAttribute("selectedPromts",selectedIDs ); 
	} 
} catch (Exception e){
    e.printStackTrace();
}

// clear selection
try {
    String value = request.getParameter("clear");
    if (value.compareTo("true") == 0){
        session.setAttribute("selectedPromts",null);
    }
} catch (Exception e){
    e.printStackTrace();
}
%>







<%
Vector selectedIDs = null;

try {
    selectedIDs = (Vector)session.getAttribute("selectedPromts");
    if (selectedIDs == null) throw new NullPointerException();
} catch (Exception e){
    selectedIDs = new Vector();
}
Session hqlsession = CoreObject.createSession();

List ids = new SXQuery(hqlsession).findPromtUserAccessRightForUserID(user.getUOID());
%>


<%@ include file="header_standard.jsp"%>
<table align="center" width="80%" cellpadding="7">
<tr>
	<th rowspan="301">&nbsp;&nbsp;&nbsp;
	<th colspan="21">Please select the experiments that you want to combine in the export. When finished press <a href="export_C_classselect.jsp">here</a>



<%
	for (int i = 0 ; i < ids.size(); i++){
	    long promtID = (((PromtUserAccessRight)ids.get(i)).getExperimentID());
	    
%>
	<tr>
		<td align="center" <%if (selectedIDs.contains(""+promtID) ){%> style="background-color: #669933"<%} %>  >	
		<%if (selectedIDs.contains(""+promtID) ){%>
			<a href="?clear=true"><img border="0" src="pics/delete-all-o.gif"></a>
		<%} else { %>
			<a href="?add=<%=promtID%>"><img border="0" src="pics/c_1.gif"></a>
		<%} %>

		<td align="center" <%if (selectedIDs.contains(""+promtID) ){%> style="background-color: #669933"<%} %>  >
			<%=promtID%>
		<td align="center" <%if (selectedIDs.contains(""+promtID) ){%> style="background-color: #99CC66"<%} %>  >
			<%=new SXQuery(hqlsession).findPromtTitleByPromtID(promtID) %>
		<%if (selectedIDs.contains(""+promtID) ){%> 
			<font class="small"><br><%=new SXQuery(hqlsession).findPromtAbstractByPromtID(promtID) %></font>
			<%if(! new SXQuery(hqlsession).determinePromtFinished(promtID)){ %><br><font style="background-color: red; color: white; font-style: italic">&nbsp; Experiment has not been completed. &nbsp;</font><%} %>
		<%} %>  
			
			
<%}%>