<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.List,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.hibernate.criterion.Expression,
                 org.setupx.repository.server.persistence.*"%>



<%@page import="java.util.HashSet"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.core.communication.mail.EMailAddress"%>
<%@page import="org.hibernate.criterion.Order"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">


<link href="stylesheet.css" rel="stylesheet" type="text/css"/>





<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%></h2>
			Index<br/>
			<font size="-2">Below you will find the experiments that you ran in the lab, <br>that you do in collaboration with others and experiments that are available for everybody.</font>
		</th>

		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>

		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} else { %>
<table align='center' border='0' cellpadding='4' width="60%">		
		<td style="background-color:#FFFFFF" align="left"><a href="login.jsp"><img src='pics/go_exit.gif' border="0" valign="left" align="left"></a></td>
</table>
<br>

<%} %>

	<table align="center" width="80%" cellpadding="7">
	<tr>
		<td colspan="15"></td>
	</tr>

	<tr>
		<td colspan="20"></td>
	</tr>

	<tr>
	<td colspan="1"></td>
	<th colspan="1" >My Experiments</th>
	<th colspan="15" align="left" class="small">the following experiments are owned and managed by you. You are the main contact person for these experiments.</th>
	</tr>
	<tr>
		<td></td>
		<th></th>
		<td width="6%" align="center"><font>samples</font></td>
		<td width="6%" align="center"><font>classes</font></td>
		<td width="6%" align="center"><font>scheduled</font></td>
		<td width="6%" align="center"><font>running</font></td>
		<td width="6%" align="center"><font>finished</font></td>
		<td width="6%" align="center"><font>
			<% if (user.isLabTechnician() ){ %>
				Share
			<% } else { %> 
				Edit 
			<%} %></font></td>
		<td width="6%" align="center"><font>Summary</font></td>
		<td width="6%" align="center">Track It!</td>
		<td width="3%"><b></td>
	</tr>

	
	<!-- each of my experiment -->
	<% 
	try{
		org.hibernate.Session hqlSession = 	org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();
	
	    // List ids = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID());
		java.util.Iterator iter= hqlSession.createCriteria(PromtUserAccessRight.class).add(Expression.eq("userID", new Long(user.getUOID()))).addOrder(Order.desc("experimentID")).list().iterator();
		
		boolean colorize = false;
	
		while(iter.hasNext()){
			long promtID = ((PromtUserAccessRight)iter.next()).getExperimentID();
			
			if (new SXQuery().determineSamplesFinishedByPromtID(promtID) > 0 && new SXQuery().findSampleIDsByPromtID(promtID).size() != new SXQuery().determineSamplesFinishedByPromtID(promtID)){
				    if (colorize) {
				        colorize = false;
				    } else {
				        colorize = true;
					}
					%>
	
					<%@ include file="incl_experiment_line.jsp"%>
	
					<%
			 }
		} // end for experiment 
		hqlSession.close();
	} catch (Exception e){
	    e.printStackTrace();
	}
	
	%>

	
	<tr>
		<td colspan="20"></td>
	</tr>

	
	</table>
</body>

<%@ include file="footer.jsp"%>
