<%@page import="org.setupx.repository.server.persistence.SXQueryCacheObject"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<table border="1">
<%

	Session hqlSession = CoreObject.createSession();
	
	List promtList = new SXQuery(hqlSession).findPromtIDs();
	
	Iterator promtIterator = promtList.iterator();
	
	while(promtIterator.hasNext()){
	    long promtID = Long.parseLong(promtIterator.next().toString());
	    
		// delete all of the cache
		try {
			SXQueryCacheObject.findByExperimentID(promtID).remove();
		} catch (Exception e){
		    e.printStackTrace();
		}

	    
	    int numberOfScannedSamples = new SXQuery(hqlSession).determineNumberOfSamplesFinished(promtID);

	    // each experiment gets a refresh
		new SXQueryCacheObject(promtID, numberOfScannedSamples);
	    %><tr><td><%=promtID%><td><%=numberOfScannedSamples%><%
	}
	
	
	
    hqlSession.close();

%>



	<a href="cache.jsp">Done</a>
