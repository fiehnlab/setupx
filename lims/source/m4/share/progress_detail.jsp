<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Technology"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page import="java.util.Set"%>

<jsp:directive.page contentType="text/html; charset=UTF-8" />



  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
    <title>SetupX</title>
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>


	<jsp:include page="techno_header.jsp"></jsp:include>

<jsp:scriptlet>

	long experimentID = Long.parseLong(request.getParameter("id"));

	// get all datafiles for this sample

</jsp:scriptlet>

	<h2>
		<a href="techno_techno.jsp">all platforms</a> - 
		
		<a href="techno_sampleFiles_promt.jsp?id=<%=experimentID%>">
		experiment <jsp:expression>experimentID</jsp:expression>
		</a>
	</h2>
	
<%
	//session 
	Session s = TechnologyProvider.createSession();

	// promt ID
	long promtID = Integer.parseInt(request.getParameter("id"));
	
	Collection technologies = TechnologyProvider.getTechnologies();

	// sample IDs
	List sampleIDs = new SXQuery(s).findSampleIDsByPromtID(promtID);
	
	Iterator sampleIterator = sampleIDs.iterator();

	%>
				
	<%
		session.setAttribute("imgpromtid", promtID + "");
	 %>
	<%@page import="org.setupx.repository.core.communication.technology.platform.Datafile"%>
<table align="center" width="10">
	 	<tr>
	 		<td align="center">
				<jsp:include page="progress_techno.jsp"/>
	</table>
	
	<table align="center">
	<tr>
		<th>
		
	<%
    Iterator techIterator = technologies.iterator();
	while (techIterator.hasNext()){
	    Technology technology = (Technology)techIterator.next();
		%>
		<td rowspan="500" style="background-color: gray;"><font style="font-size: 1pt;">&nbsp;</font>
		<td align="center" colspan="<%=technology.getTechnologyFileTypes().size() %>"><%=technology.getLabel() %>
		<%
	}
	%>
	<td rowspan="500" style="background-color: gray;"><font style="font-size: 1pt;">&nbsp;</font>
	<%

	while(sampleIterator.hasNext()){
	    long sampleID = Long.parseLong(sampleIterator.next().toString());
 		%>
 		<tr>
 			<th width="60" align="right" valign="bottom">
	 		<a href="techno_sampleFiles.jsp?id=<%=sampleID %>">
	 			<%=sampleID%>
	 		</a><br>
	 		<nobr><font style="font-size: xx-small"><%=new SXQuery(s).findSampleLabelBySampleID(sampleID) %></font></nobr>
 		<%


	    techIterator = technologies.iterator();
		while (techIterator.hasNext()){
		    Technology technology = (Technology)techIterator.next();
		 	
			Iterator technofiletypeIterator = technology.getTechnologyFileTypes().iterator();
		    while(technofiletypeIterator.hasNext()){
		 		TechnologyFileType technologyFileType = (TechnologyFileType)technofiletypeIterator.next();
		 		
		 		Set datafiles = technologyFileType.getDatafiles(sampleID);
		 		%>
		 		
		 		<td align="center" width="5">
		 		<a href="techno_sampleFiles.jsp?id=<%=sampleID %>">

		 		<%
		 		if (datafiles.size() > 0) {
		 		    
		 		    // check if all files are ok
		 		    boolean fileOK = true;
		 		    Iterator datafileIterator = datafiles.iterator();
					try {
			 		    while(datafileIterator.hasNext()){
							Datafile datafile = (Datafile)datafileIterator.next();
						    datafile.checkFile();
						}
					} catch (Exception e){
					    fileOK = false;
					}

					if (fileOK){
						%>
						<img src="pics/checked.jpg" border="0"> 
			 		    <%
					} else {
						%>
						<img src="pics/x.jpg" alt="A datafile is missing."  border="0"> 
			 		    <%
					}
		 		} else {
		 		    %>
					<img src="pics/blank.jpg"  border="0"> 
		 		    <%
		 		}
		 		%>
		 		</a>
		 		
		 		<%
		 	}
		}
	}
%>
		<td align="center" width="90%">
 		

</tr>
</table>

