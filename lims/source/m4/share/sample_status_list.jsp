<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.communication.status.StatusTracker"%>
<%@page import="org.setupx.repository.core.communication.status.Status"%>
<%@page import="org.setupx.repository.core.communication.status.Timestamped"%>
<%@page import="org.setupx.repository.core.communication.status.UserAssigned"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="org.setupx.repository.core.communication.status.DynamicStatus"%>
<%@page import="org.setupx.repository.core.communication.status.PersistentStatus"%>

<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Comparator"%>
<%@page import="org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration"%>
<%@page import="org.setupx.repository.core.communication.status.SampleArrivedStatus"%>
<%@page import="org.setupx.repository.core.communication.status.LabStatus"%>
<%@page import="org.setupx.repository.core.communication.status.FilecreatedStatus"%>
<%@page import="org.setupx.repository.core.communication.status.SampleShippedStatus"%>
<%@page import="org.setupx.repository.core.communication.status.PostprocessEventStatus"%>
<%@page import="org.setupx.repository.core.communication.status.SampleRunStatus"%>
<%@page import="org.setupx.repository.server.persistence.PersistenceActionFindException"%>
<%@page import="org.setupx.repository.core.communication.status.ResultReceivedStatus"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<table align="center" cellpadding="5">	
<%
	org.hibernate.Session hqlSession = PersistenceConfiguration.createSessionFactory().openSession();

	try {
	    long id = Integer.parseInt("" + request.getParameter("rid"));
	    org.setupx.repository.core.CoreObject.persistence_loadByID(PersistentStatus.class, hqlSession, id).remove();
	    hqlSession.close();
	} catch (Exception e){
	    e.printStackTrace();
	}
%>



<%
	long sampleID = Integer.parseInt("" + request.getParameter("id"));

	List list = StatusTracker.getStatus(sampleID);
	
	Collections.sort(list, new Comparator(){
	    public int compare(Object o1, Object o2) {
	        Timestamped timestamped1 ;
	        Timestamped timestamped2 ;
	        
	        if (o1 instanceof Timestamped && o2 instanceof Timestamped){
	            timestamped1 = (Timestamped)o1;
	            timestamped2 = (Timestamped)o2;
	        } else {
	            Logger.debug(this, "unable to compare cause not both timestamped");
	            if (o1 instanceof Timestamped) return -1;
	            else return +1;
	        }

	        // identical
	        if (timestamped1.getDate().getTime() == timestamped2.getDate().getTime()){
	            return 0;
	        }
	        
	        if (timestamped1.getDate().getTime() > timestamped2.getDate().getTime()){
	            return -1;
	        } else {
	            return +1;
	        }
	    }
	});
	
	Iterator iterator = list.iterator();
	
	Calendar midnight = new GregorianCalendar();
	midnight.set(midnight.get(Calendar.YEAR),midnight.get(Calendar.MONTH), midnight.get(Calendar.DATE),0,1);
	
	
	while(iterator.hasNext()){
	    Status status = (Status)iterator.next();
        %><tr>
        
        
        
        
        <%
        // --------------TIME---------------------
	    if (status instanceof Timestamped){
			%>
			<td style="background-color:white;" align="center">
			<%	
			if (((Timestamped)status).getDate().after(midnight.getTime())){
			// todays samples
			%>	
			<nobr>
					<%=DateFormat.getTimeInstance(DateFormat.SHORT, Locale.US).format(((Timestamped)status).getDate())%>
			</nobr>
			<font class="small"><br>today</font>
			<%} else { %>
			<nobr>
				<font class="small"><%=DateFormat.getTimeInstance(DateFormat.SHORT, Locale.US).format(((Timestamped)status).getDate())%><br></font>
			<nobr>
				  <%=java.text.DateFormat.getDateInstance(java.text.DateFormat.MEDIUM, Locale.US).format(((Timestamped)status).getDate()) %>
			</nobr>
			<%} 	        
	    } else {
	        %><td style="background-color:white;" >---<%
	    }

        
        %>
        
        
        
        <%
        // --------------ICON---------------------
		if (status instanceof SampleArrivedStatus){%>
	        <td style="background-color:white;" align="center"><img  border='0' src="pics/frost.jpg">	        
		<%} else if (status instanceof LabStatus){%>
	        <td style="background-color:white;" align="center"><img  border='0' src="pics/sample_prep.jpg">	        
		<%} else if (status instanceof FilecreatedStatus){%>
	        <td style="background-color:white;" align="center"><img  border='0' src="pics/file.png">	        
		<%} else if (status instanceof SampleShippedStatus){%>
	        <td style="background-color:white;" align="center"><img  border='0' src="pics/package.gif">	        
		<%} else if (status instanceof ResultReceivedStatus){%>
	        <td style="background-color:white;" align="center"><img  border='0' src="pics/chart.png">	        
		<%} else if (status instanceof PostprocessEventStatus){%>
	        <td style="background-color:white;" align="center"><img  border='0' src="pics/chart.png">	        
		<%} else if (status instanceof SampleRunStatus){%>
	        <td style="background-color:white;" align="center"><img  border='0' src="pics/run.jpg">	        
		<%} else {%>
	        <td>
	    <%}%>
        
        
        
        
        <%
        // --------------ID---------------------
	    /*if (status instanceof PersistentStatus){
	        %><td><font class="small"><%=((PersistentStatus)status).getUOID()%></font><%
	    } else {
	        %><td>--<%
	    }
	    */
	    %>


        <%
        // --------------MSG---------------------
        %>
		<td style="background-color:white;" 
				<%if (!(status instanceof SampleArrivedStatus)){%> colspan="2" <%} %>
		>
			<%=status.createDisplayString() %>
			<br>
				<font class="small"><%=status.getMessage() %></font>
		</td>





		<%if (status instanceof SampleArrivedStatus){%>
			<td style="background-color:white;" ><font class="small">Location:<br></font>
				<a href="sample_status_location.jsp?id=<%=sampleID%>&location=<%=((SampleArrivedStatus)status).getLocation()%>">
					<%=((SampleArrivedStatus)status).getLocation()%>
				</a>
		<%} else { %>
		<%} %>



		<%
        // --------------USER---------------------
	    if (status instanceof UserAssigned){
	        try { 
		        UserDO userDO = (UserDO)UserDO.persistence_loadByID(UserDO.class, DynamicStatus.createSession(), ((UserAssigned)status).getUserID());
		        %>
				<td style="background-color:white;" align="center">
				<%
	    				if (UserDO.isMasterUser(userDO.getUOID())){
	    					%> <img  border='0' src="pics/super.gif"><%
	    				} else if (userDO != null && userDO.isLabTechnician()) {
	    					%> <img  border='0' src="pics/lab_small.gif"><%
	    				} else { %><img  border='0' src="pics/icon-module-admin.png"><%} %>
				<td style="background-color:white;" align="center"><%=userDO.getDisplayName() %><%
	        }catch (PersistenceActionFindException e){
		        %>
		        <td style="background-color:white;" align="center"><img  border='0' src="pics/system.jpg">	        
		        <td style="background-color:white;" align="center"><i>system message</i><font class="small"><br>userID: 0</font>
		        <%
	        }
	    } else {
	        %>
	        <td style="background-color:white;" align="center"><img  border='0' src="pics/system.jpg">	        
			<td style="background-color:white;" align="center"><i>system message</i><font class="small"><br>non userassigned</font>
	        <%
	    }

		
		
		
		
        // --------------REMOVE---------------------
		if (status instanceof PersistentStatus && user.isLabTechnician()){
	        %>
	        <td style="background-color:white;" >
				<a href="sample_detail.jsp?id=<%=sampleID%>&rid=<%=((PersistentStatus)status).getUOID() %>#hist"><img src='pics/trash.gif' border="0" valign="middle" align="center"></a>
	        <%
	    } else {
	        %><td><%
	    }
		%>

	</tr>    
	    <%   
	}
%>
</table>