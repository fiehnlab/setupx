<%! 
public static String[] searchTypesDef = new String[]{
    	"all species",
    	"compounds that have been detected in samples",
    	"different factors that spanned the experimental design", 
		"title of the experiment",
		"sample label or sample comment"
	};

public static String[] searchTypesImg = new String[]{
    	"species.gif",
    	"compound.png",
    	"cube.png", 
		"Publication-pic.gif",
		"sample_prep.jpg"
	};

public static String[] searchTypes = new String[]{
    	"species",
    	"compound",
    	"factor", 
		"title",
		"sample"
	};
%>