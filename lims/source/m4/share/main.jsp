

<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.List,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.hibernate.criterion.Expression,
                 org.setupx.repository.server.persistence.*"%>


<%
	// check the users email
	
	
	if ((user.getEmailString() == null) || user.getEmailString().compareTo("null") == 0 || user.getEmailString().length() < 3){ 
	%>
	    <%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.criterion.Order"%>
<jsp:forward page="user_modify_email.jsp"/>
	<%	    
	}
%>




<%@page import="java.util.HashSet"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.core.communication.mail.EMailAddress"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">


<link href="stylesheet.css" rel="stylesheet" type="text/css"/>





<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%></h2>
			Index<br/>
			<font size="-2">Below you will find the experiments that you ran in the lab, <br>that you do in collaboration with others and experiments that are available for everybody.</font>
		</th>

		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>

		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} else { %>
<table align='center' border='0' cellpadding='4' width="60%">		
		<td style="background-color:#FFFFFF" align="left"><a href="login.jsp"><img src='pics/go_exit.gif' border="0" valign="left" align="left"></a></td>
</table>
<br>

<%} %>

	<table align="center" width="80%" cellpadding="7">
	<tr>
		<td colspan="15"></td>
	</tr>

<% if (user.isSuperUser()){
    %>

	<tr>
		<% // <td rowspan="221" valign="top">			<%@ include file="portletLeft.jsp" %>
		<td colspan="1"></td>
		<th colspan="1" ><b>all</b> experiments</th>
		<th colspan="15" align="left" class="small">To see all experiments choose the option click here. You will see all experiment and not only the experiment <br>
		that are yours or that you share with someone.
		</th>
	</tr>

	<tr>
	<td colspan="1"></td>
	<th align="right"><font class="small">See all experiments.	</font></td>
	<td align="center" style="background-color:#FFFFFF" colspan="5"></td>
	<td align="center" style="background-color:#FFFFFF"><a href="main_all.jsp"><img  border='0' src="pics/go.gif"></a></td>
	<td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_blank.gif"></td>
	<td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_blank.gif"></td>
	<td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_blank.gif"></td>
	</tr>
	<%    
}%>
	
	<tr>
	<td colspan="1"></td>
	<th align="right"><font class="small">My current experiments in progress</font></td>
	<td align="center" style="background-color:#FFFFFF" colspan="5"></td>
	<td align="center" style="background-color:#FFFFFF"><a href="main_progress.jsp"><img  border='0' src="pics/go.gif"></a></td>
	<td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_blank.gif"></td>
	<td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_blank.gif"></td>
	<td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_blank.gif"></td>
	</tr>
	<tr>
	<tr>
		<td colspan="20"></td>
	</tr>
	
	
	
<% if (!user.isPublic()) { %>
	<tr>
		<td colspan="1"></td>
		<th colspan="1" >Create a new Experiment</th>
		<th colspan="15" align="left" class="small">To start a new experiment, you have two options. You can start your experiment from scratch 
													<br>or you can choose an exising experiment and use this this as a template for your new experiment.
		</th>
	</tr>
<%    
}%>
	



	<!-- new Experiment -->
	<tr>
	<td colspan="1"></td>
	<th align="right"><font class="small">To start a NEW Experiment from scratch, choose this option.</font></td>
	<td align="center" style="background-color:#FFFFFF" colspan="5"></td>
	<td align="center" style="background-color:#FFFFFF"><a href="load?id=0"><img  border='0' src="pics/go_new.gif"></a></td>
	<td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_blank.gif"></td>
	<td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_blank.gif"></td>
	<td align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/go_blank.gif"></td>
	</tr>
	<!-- END new Experiment -->

	<tr>
		<td colspan="20"></td>
	</tr>

	<tr>
	<td colspan="1"></td>
	<th colspan="1" >My Experiments</th>
	<th colspan="15" align="left" class="small">the following experiments are owned and managed by you. You are the main contact person for these experiments.</th>
	</tr>
	<tr>
		<td></td>
		<th></th>
		<td width="6%" align="center"><font>samples</font></td>
		<td width="6%" align="center"><font>classes</font></td>
		<td width="6%" align="center"><font>scheduled</font></td>
		<td width="6%" align="center"><font>running</font></td>
		<td width="6%" align="center"><font>finished</font></td>
		<td width="6%" align="center"><font>
			<% if (user.isLabTechnician() ){ %>
				Share
			<% } else { %> 
				Edit 
			<%} %></font></td>
		<td width="6%" align="center"><font>Summary</font></td>
		<td width="6%" align="center">Track It!</td>
		<td width="3%"><b></td>
	</tr>

	
	<!-- each of my experiment -->
	<% 
	org.hibernate.Session hqlSession = 	org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

    // List ids = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID());
	java.util.Iterator iter= hqlSession.createCriteria(PromtUserAccessRight.class).add(Expression.eq("userID", new Long(user.getUOID()))).addOrder(Order.desc("experimentID")).list().iterator();
	
	boolean colorize = false;

	while(iter.hasNext()){
	    if (colorize) {
	        colorize = false;
	    } else {
	        colorize = true;
		}
		long promtID = ((PromtUserAccessRight)iter.next()).getExperimentID();
		%>
		<%@ include file="incl_experiment_line_new.jsp"%>
		<%
	
	} // end for experiment 
	try {
		hqlSession.close();
	} catch(Exception e){
	    // session was closed already
	}
	
	%>

	
	<tr>
		<td colspan="20"></td>
	</tr>
<% if (!user.isPublic()) { %>

	<tr>
		<td colspan="20" ></td>
	</tr>

	<tr>
		<td colspan="20"></td>
	</tr>
	
	
	<tr>
	<th colspan="2">Userinformation</th>
	<th colspan="15"></th>
	</tr>

	<tr>
	<td rowspan="9"></td>
	<th rowspan="9"></th>
	<td align="center" colspan="2"></td>
	<td align="center" style="background-color:#FFFFFF" colspan="3" align="left" ><%=user.getDisplayName() %></td>
	<td align="center" style="background-color:#FFFFFF" colspan="10"></td>
	</tr>
	
	<tr>
	<td align="center" colspan="2">User ID number</td>
	<td align="center" style="background-color:#FFFFFF" colspan="3" align="left" ><%=user.getUOID() %></td>
	<td align="center" style="background-color:#FFFFFF" colspan="10"></td>
	</tr>
	
	<tr>
	<td align="center" colspan="2">Organistation</td>
	<td align="center" style="background-color:#FFFFFF" colspan="3" align="left" ><%=user.organisation %></td>
	<td align="center" style="background-color:#FFFFFF" colspan="10"></td>
	</tr>
	
	<tr>
	<td align="center" colspan="2">Phone</td>
	<td align="center" style="background-color:#FFFFFF" colspan="3" align="left" ><%=user.phone%></td>
	<td align="center" style="background-color:#FFFFFF" colspan="10"></td>
	</tr>
	
	<tr>
	<td align="center" colspan="2"> Username:</td>
	<td align="center" style="background-color:#FFFFFF" colspan="3" align="left" ><%=user.username %></td>
	<td align="center" style="background-color:#FFFFFF" colspan="10"></td>
	</tr>
	
	<tr>
	<td align="center" colspan="2">Abbreviation:</td>
	<td align="center" style="background-color:#FFFFFF" colspan="3" align="left" ><%=user.getAbbreviation() %></td>
	<td align="center" style="background-color:#FFFFFF" colspan="10"></td>
	</tr>
	
	<tr>
	<td align="center" colspan="2">eMail</td>
	<td align="center" style="background-color:#FFFFFF" colspan="3" align="left" ><%=user.getMailAddress() %></td>
	<td align="center" style="background-color:#FFFFFF" colspan="10"></td>
	</tr>
	
	<tr>
	<td align="center" colspan="2"></td>
	<td align="center" style="background-color:#FFFFFF" colspan="3" align="left" ><%=user.getUserinformation() %></td>
	<td align="center" style="background-color:#FFFFFF" colspan="10"></td>
	</tr>
	
	<tr>
	<td align="center" colspan="2">my last Experiment: </td>
	<td align="center" style="background-color:#FFFFFF" colspan="3" align="left" ><%=Promt.createShortAbstractHTML(user.getLastExperimentID(), 50)%> <b>ID:<%=user.getLastExperimentID() %></b></td>
	<td align="center" style="background-color:#FFFFFF" colspan="1">			
			<a href="load?id=<%=user.getLastExperimentID()%>&action=<%=PromtUserAccessRight.READ%>"><img  border='0' src="pics/go.gif"></a>			</td>
	<td align="center" style="background-color:#FFFFFF" colspan="10"></td>
	</tr>	
	
	<% } %>
	
	</table>
</body>

<%@ include file="footer.jsp"%>
