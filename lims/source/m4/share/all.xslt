<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:m0="http://www.mpimp-golm.mpg.de/m1"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="experiment">
		<table align="center" border="1">
			<tbody>
				<xsl:apply-templates select="m0:owner"/>
				<xsl:apply-templates select="m0:sample"/>
			</tbody>
		</table>
	</xsl:template>
	<xsl:template match="m0:owner">
		<tr>
				<td><xsl:value-of select="m0:firstname"/> <xsl:value-of select="m0:lastname"/></td>
		</tr>
	</xsl:template>
	<xsl:template match="m0:sample">
		<tr>
				<td>related samples
				<xsl:apply-templates select="@referenceID"/>
				</td>
		</tr>
	</xsl:template>
	<xsl:template match="@referenceID">
		<a href=""><xsl:value-of select="."/></a>
	</xsl:template>
</xsl:stylesheet>





