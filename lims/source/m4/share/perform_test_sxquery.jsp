<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%
        // testing the different speeds of caching in SXQuery
%>




<%
        boolean caching = false;
        int max = Integer.parseInt("" + request.getParameter("max"));
        for (int i = 0; i < max; i++){
            
            if (i > max/2) caching = true;

            long start = new GregorianCalendar().getTimeInMillis();
            List ids = new SXQuery(caching).findPromtIDs();
            if (ids.size() > 30){
                ids = ids.subList(0, 30);
            }

            for (int c = 0; c < 10; c++){
                    Iterator idIterator = ids.iterator();
                    
                    while(idIterator.hasNext()){
                            long promtID = Long.parseLong("" + idIterator.next());
                            CoreObject.setCaching(caching);
                            new SXQuery(caching).findSampleIDsByPromtID(promtID);
                            new SXQuery(caching).findClazzIDsByPromtID(promtID);
                            new SXQuery(caching).findPromtAbstractByPromtID(promtID);
                            new SXQuery(caching).findPromtClassesString(promtID);
                            new SXQuery(caching).findPromtCollaborationString(promtID);
                            new SXQuery(caching).findPromtTitleByPromtID(promtID);
                    }
            }
            long end = new GregorianCalendar().getTimeInMillis();
            %><%@page import="org.setupx.repository.core.CoreObject"%>
<br><%=(end - start) %><%
        }
%>
