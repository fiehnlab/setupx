<%@ include file="checklogin.jsp"%>

<%@ page import="org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="org.setupx.repository.Config"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>
<%
int promtID = Integer.parseInt("" + request.getParameter("pid")); 


boolean cachedQueries = org.setupx.repository.Config.QUERY_CACHING_ACTIVE_JSP;

try {
	String cache = request.getParameter("cache");
    if (cache.compareTo("false") == 0){
        cachedQueries = false;
    }
} catch (Exception e){
    
}


%>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<%@ include file="incl_setaccessright.jsp"%>






<body>

	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%> Administration</h2>
			Control Access to Experiment <%=promtID %><br/>
			<font size="-2">Define who can access the Experiment  <%=Promt.createShortAbstractHTML(promtID, 60)%></font>
			
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>
	
	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} %>

	
	<table align="center" width="60%" >	
	<tr>
		<td colspan="1" align="right" style="background-color:#FFFFFF"><a href="main.jsp"><img  border='0' src="pics/back.gif"></td>
		<th colspan="20" align="right"><table><tr><th align="center">Details of this Experiment <br><a href="load?id=<%=promtID%>&action=20"><img  border='0' src="pics/go.gif"></a></th></tr></table></th>
	</tr>

<%@ include file="incl_accessheader.jsp"%>


<% 
// list of all experiments
org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();


List users = hqlSession.createSQLQuery("select uoid, displayname, organisation from user")
			.addScalar("uoid", org.hibernate.Hibernate.LONG)
			.addScalar("displayname", org.hibernate.Hibernate.STRING)
			.addScalar("organisation", org.hibernate.Hibernate.STRING)
			.setCacheable(cachedQueries)
			.list();
			
//List users = hqlSession.createCriteria(UserDO.class).list();

java.util.Iterator iterator = users.iterator();

while (iterator.hasNext()){
	Object[] objects = (Object[])iterator.next();
	
    long userID = Long.parseLong("" + objects[0]);
    String displ = (String)(objects[1]);
    String orga = (String)(objects[2]);
    
    //long userID = userDO.getUOID();
    %>

			
	<tr>
		<td></td>
		<td align="center">
		<%=userID %>
		</td>
		<td>
		<b><%=displ%></b><br><font class="small"><%=orga %></font>
		</td>
		<td>
			<a href="accessright_user.jsp?uid=<%=userID%>&pid=<%=promtID%>"><img  border='0' src="pics/go_share.gif" title="All users for <%=promtID%>"></a>
		</td>
		<td>
		</td>
		<td>
		<%@ include file="incl_accessright.jsp"%>
		</td>
	</tr>
    <%
}
%>
</table>

<%@ include file="footer.jsp"%>
