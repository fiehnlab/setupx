<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 org.setupx.repository.server.logging.Message,
                 java.util.List,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="org.setupx.repository.core.CoreObject"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<%java.util.Locale locale = java.util.Locale.US;




// delete entries

org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

try {
    long id = Integer.parseInt("" + request.getParameter("id"));
    org.setupx.repository.core.CoreObject.persistence_loadByID(Message.class, hqlSession, id).remove();
    hqlSession.close();
} catch (Exception e){
    e.printStackTrace();
}
%>



<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width='60%'>
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%></h2>
			Messages<br/>
			<font size="-2">Below you find information about events that happend in the system.
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>	
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>



	<table align="center" width="60%" cellpadding="7">
	<tr >
		<th ></th>

		<% 
		String[] links = new String[]{"Login","Files","System","General","Export"};
		for (int i = 0; i < links.length; i++){
		    %><th align="center"><%=links[i] %></th>
		<% 
		}
		%>

		<th align="center"></th>
	</tr>
		
	<tr >
		<th ></th>
		<% 
		for (int i = 0; i < links.length; i++){
		    %>
		    <td width="2" style="background-color:#FFFFFF" align="center"><a href="?type=<%=i+1%>"><img src='pics/go_report.gif' border="0" valign="middle" align="center"></a></td>
		<% 
		}
		%>

		<th align="center"></th>
	</tr>
		


	<table align="center" width="60%" cellpadding="7">
	<tr>
		<td colspan="15"></td>
	</tr>


<%
		// checking which query is requested		
		int type = 0;
		try {
			type = Integer.parseInt("" + request .getParameter("type"));
		} catch (Exception e){
		 	e.printStackTrace();   	
		}
		String query = ""; 
		switch (type) {
	    case 1:
			query = "select {message.*} from message as message where message.message LIKE \"%logged%\" order by message.uoid desc ";
	        break;
	    case 2:
			query = "select {message.*} from message as message where message.className = \"org.setupx.repository.web.forms.inputfield.multi.Sample\" order by message.uoid desc ";
	        break;
	    case 3:
			query = "select {message.*} from message as message where message.label = \"shutting down\" or message.label = \"starting system\" order by message.uoid desc ";
	        break;
	    case 4:
			query = "select {message.*} from message as message where !(message.label > 0) and message.label != message.message and message.className != \"org.setupx.repository.web.forms.inputfield.multi.Sample\" and message.label != \"Class is finish...\" and className != \"org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector\" and message.label != \"shutting down\"  and message.label != \"starting system\" and NOT (message.message LIKE \"%logged%\") and NOT (message.message LIKE \"%binbase%\") order by message.uoid desc ";
	        break;

	    case 5:
			query = "select {message.*} from message as message where message.className = \"org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector\" or message LIKE \"%binbase%\" order by message.uoid desc ";
	        break;

	    default:
			query = "select {message.*} from message as message order by message.uoid desc ";
	        break;
	    }
	%>

	<tr>
			<% 
			org.hibernate.Session s = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

			//java.util.Iterator iterator = s.createCriteria(org.setupx.repository.core.communication.leco.Method.class).addOrder(org.hibernate.criterion.Order.asc("label")).list().iterator();	    
			java.util.Iterator iterator = s.createSQLQuery(query).addEntity("message", Message.class).setMaxResults(100).list().iterator();

			int col = -1;
		    while (iterator.hasNext()){
		        Message message= (Message)iterator.next();	

		        col = col * -1;
				String bgcolor = "style=\"background-color:#FFFFFF\"";
				if (col > 0) bgcolor = "";
				if (message.getMessage().indexOf("xception") > 0) bgcolor = "style=\"background-color:#FF545A\"";
		        %>
				<td>
				<td  <%=bgcolor %>align="center"><b><%=message.getLabel() %></b>
				<td  <%=bgcolor %>><font class="small"><%=message.getMessage() %>			
				<td  <%=bgcolor %> align="center" >
				<% if (message.getClassName().compareTo(Sample.class.getName()) == 0 && message.getRelatedObjectID() > 1) {				    
				    String link = "sample_detail.jsp?id=" + message.getRelatedObjectID();
				    %>
								<a href="<%=link %>">
								<img src='pics/details.gif' border="0" valign="middle" align="center">
								</a>
						<% 
				}
				%>
				<td  <%=bgcolor %>align="center"><font class="small">				  
				<% try{
			    %>
		    	<%=message.getUser().getDisplayName()%>
				<% 
				}catch(Exception e){
					// no user related    
				}
				%>
				
				<td  <%=bgcolor %>align="center"><font class="small">				  
					  <%=java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT, locale).format(message.getDate()) %><br>
					  <%=java.text.DateFormat.getDateInstance(java.text.DateFormat.LONG, locale).format(message.getDate()) %>
				<td <%=bgcolor %> align="center"><a href="?type=<%=type%>&id=<%=message.getUOID()%>"><img  border='0' src="pics/trash.gif"></a></td>				<td>
	</tr>	
		        
		        <% 
		    }
		    %>

		    
		    </table>
		    
<%@ include file="footer.jsp"%>