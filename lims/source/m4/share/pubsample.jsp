<%@ include file="checknonlogin.jsp"%>

<%@ include file="pub_header.jsp"  %>

<%@ include file="robot.jsp"%>

<%@page import="org.setupx.repository.web.forms.PromtCreator"%>
<%@page import="org.setupx.repository.web.pub.data.PubSample"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="org.setupx.repository.core.util.Sort"%>
<%@page import="java.util.SortedSet"%>
<%@page import="org.hibernate.type.SortedSetType"%>
<%@page import="java.util.TreeSet"%>
<%@page import="org.setupx.repository.web.pub.data.PubAttribute"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Vector"%>
<%@page import="org.setupx.repository.web.forms.restriction.Restriction"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.hibernate.SQLQuery"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="java.util.Hashtable"%>

<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>


<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.setupx.repository.web.pub.data.PubDataSet"%>
<!--  head containing logo and description -->

	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>List of samples</h2>The samples below contain certain compounds specified below.<br/>
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>

	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} else {%>
	<table align="center" width="60%" cellpadding="5">	
		<tr>
			<td align="right" colspan="41" style="background-color:white;" align="left"><a href="q_main_ajax.jsp?type=1"><img src="pics/go_search.gif" border="0" align="middle">
		</tr>
	</table>
	<%} %>




<table align='center' border='0' cellpadding='4' width="60%">

	<%
	Hashtable cache = new Hashtable();

	Enumeration paramNames = request.getParameterNames();
	Vector criteraVector = new Vector();

    criteraVector = (Vector) session.getAttribute("restricitons");
    if (criteraVector == null) {
        criteraVector = new Vector();
        Logger.debug(this, "no criteraVector found");
    } else {
        Logger.debug(this, "found criteraVector with " + criteraVector.size() + " entries.");
    }

    // figure out if all compounds should be shown
	// trying to remove items from the restriction vector
	boolean showNonsearchedCompounds = false;
	try {
		String t = request.getParameter("show");
		if (t.length()>0){
		    Logger.log(this, t);
		    showNonsearchedCompounds = Boolean.valueOf(t).booleanValue();
		    Logger.log(this, "" + showNonsearchedCompounds );
		    session.setAttribute("showNonsearchedCompounds", "" + showNonsearchedCompounds);
		}
	} catch (Exception e){
	    //
	}

	try {
	    showNonsearchedCompounds = Boolean.valueOf(session.getAttribute("showNonsearchedCompounds").toString()).booleanValue();
	} catch (Exception e){
	    //
	}
	


    
	// trying to remove items from the restriction vector
	try {
	    String removePosition = request.getParameter("rm");
	    int removePositionInt = Integer.parseInt(removePosition);
        Logger.debug(this, "found remove parameter : " + removePositionInt);
	    criteraVector.remove(removePositionInt);	
	} catch (Exception e){
	    //
	}

	String limit = "";
	try {
	    limit = request.getParameter("limit");
	    if (limit.compareTo("null") == 0) throw new Exception(); 
	} catch (Exception e){
	    limit = "100";
	}
	

	    
	while (paramNames.hasMoreElements()) {
	    String key = "" + paramNames.nextElement();
	    String value = request.getParameter(key);
	
	    String[] excludedkeys = new String[] { "order", "se", "rm", "field", "uoid",  "limit", "promt", "show" };
	    boolean loop = true;
	    for (int y = 0; y < excludedkeys.length && loop; y++) {
	        if (key.startsWith(excludedkeys[y])) {
	            loop = false;
	        }
	    }
	    if (loop){
			// in case no value is given
	        try {
		        int valueInt = Integer.parseInt(value);
		        criteraVector.add(new String[]{key, "" + (valueInt - valueInt/20), "" + (valueInt + valueInt/20)} );        
	        }catch (Exception e){
		        criteraVector.add(new String[]{key, "" + (0), "" + (10000000)} );        
	        }
	    }
	
	    session.setAttribute("restricitons", criteraVector);
	}

	// debugging
	StringBuffer queryString = new StringBuffer(); 
	queryString.append("select s.* from pubsample as s ");
	
	if (criteraVector.size() > 0 )queryString.append(','); 
	for (int i = 0; i < criteraVector.size(); i++){
	    queryString.append(" pubattribute as a" + i);
	    if (i+1 !=  criteraVector.size()){
	        queryString.append(", ");
	    }
	}


	
	
	
    // figure out if all compounds should be shown
	// trying to remove items from the restriction vector
	try {
		String t = request.getParameter("show");
		if (t.length()>0){
		    Logger.log(this, t);
		    showNonsearchedCompounds = Boolean.valueOf(t).booleanValue();
		    Logger.log(this, "" + showNonsearchedCompounds );
		    session.setAttribute("showNonsearchedCompounds", "" + showNonsearchedCompounds);
		}
	} catch (Exception e){
	    //
	}
	try {
	    showNonsearchedCompounds = Boolean.valueOf(session.getAttribute("showNonsearchedCompounds").toString()).booleanValue();
	} catch (Exception e){
	    //
	}

	if (criteraVector.size() == 0) showNonsearchedCompounds = true;


	
	
	
	// limit by experiment
	try {
		long requestPromtUOID = 0;
	    requestPromtUOID = Long.parseLong("" + request.getParameter("promt"));
		if (requestPromtUOID>0){
		    Logger.log(this, "" + requestPromtUOID);
		    session.setAttribute("requestPromtUOID", "" + requestPromtUOID);

		} else {
		    session.removeAttribute("requestPromtUOID");
		}
	} catch (Exception e){
	    e.printStackTrace();
	}

	long requestPromtUOID = 0;
	try {
	    requestPromtUOID = Integer.parseInt(session.getAttribute("requestPromtUOID").toString());

	    queryString.append(" , samples as realSample " );
	    queryString.append(" where  realSample.experiment = " + requestPromtUOID + " and realSample.sampleID = s.setupXsampleID ");
	} catch (Exception e){
	    //
	}

	
	for (int i = 0; i < criteraVector.size(); i++){
		if (i == 0 && requestPromtUOID == 0){
		    queryString.append(" where ");
		} else {
		    queryString.append(" and ");
		}
		String[] values = (String[])criteraVector.get(i);
		queryString.append("a" + i + ".parent = s.uoid and a" + i + ".label = \"" + values[0] + "\" and a" + i + ".value between " + values[1] + " and " + values[2] + " ");	
	}

	// in case a single uoid is given
	try {
	    long requestUOID = Long.parseLong("" + request.getParameter("uoid"));
	    if (criteraVector.size() > 0) queryString.append(" and " );
	    else queryString.append(" where " );
	    queryString.append(" s.uoid = " + requestUOID + " ");
	} catch (Exception e){
	    Logger.log(this, "no uoid givven");
	}
	
	
	
	
	
	// limit by species
	
	
	
	
	// limitation
	queryString.append("  LIMIT 0, " + limit);


	Logger.log(this, queryString.toString());
	
	
	
	SQLQuery sqlQuery = hqlSession.createSQLQuery(queryString.toString());
	%>

	<tr>
		<td>
		<td valign="top">Compound selection.
		<td colspan="21">
			<table align="center">

	<form action="pub_sample_update.jsp">
	<%
	for (int i = 0; i < criteraVector.size(); i++){
				String[] values = (String[])criteraVector.get(i);
				%>
	<tr>
		<td colspan="4" style=background-color:white; >
			 	<a href="advancedquery_dev.jsp?term=<%=values[0] %>">
					<img src="pics/details.gif" border="0" align="middle" title="details">
				</a>
		<td colspan="2" style=background-color:<%=Util.getColor(i) %>>
					<input type="text" name="field_<%=i %>_0" value="<%=values[0] %>">
		<td colspan="2" style=background-color:<%=Util.getColor(i) %>>
				between 
		<td colspan="2" style=background-color:<%=Util.getColor(i) %>>
				<input type="text" size="6" name="field_<%=i %>_1" value="<%=values[1] %>">
		<td colspan="2" style=background-color:<%=Util.getColor(i) %>>
				<input type="text" size="6" name="field_<%=i %>_2" value="<%=values[2] %>">
		<td colspan="2" style=background-color:white;>
				<input type="image" name="search" src="pics/refresh.gif" >
		<td colspan="2" style=background-color:<%=Util.getColor(i) %>>
				<a href="pub_sample_update.jsp?field_<%=i %>_0=<%=values[0] %>&field_<%=i %>_1=0&field_<%=i %>_0=<%=values[0] %>&field_<%=i %>_2=10000000">any</a>
				<a href="pub_sample_update.jsp?field_<%=i %>_0=<%=values[0] %>&field_<%=i %>_1=0">min</a>
				<a href="pub_sample_update.jsp?field_<%=i %>_0=<%=values[0] %>&field_<%=i %>_2=10000000">max</a>
		<td colspan="2" style=background-color:white;" >
			 	<a href="pubsample.jsp?rm=<%=i %>">
					<img src="pics/trash.gif" border="0" align="middle" title="remove this limitation">
				</a>

		<%
			}
		%>
	</td>
	</table>

	<tr>
		<td>
		<td>Unselected Compounds
		<td colspan="21">hide non selected compounds: 
		<%if (showNonsearchedCompounds){ %>
			<a href="pubsample.jsp?show=false"><img src="pics/off.gif" border="0" align="middle"></a>
		<%} else { %>
			<a href="pubsample.jsp?show=true"><img src="pics/on.gif" border="0" align="middle"></a>
		<%} %>

			
	<%
	String id = null;
	try {
	    id = session.getAttribute("requestPromtUOID").toString();
	}catch (Exception e){

	}
	if (id != null && id.compareTo("null") != 0){
	    %>
		<tr>
			<td>
			<td>Experiment
			<td colspan="21">
				currently you only see compounds from the experiment <b><%=session.getAttribute("requestPromtUOID") %></b><br>Remove this limitation here: <a href="pubsample.jsp?promt=0"><img src="pics/delete-all-o.gif" border="0" align="middle"></a> 
	    <% } else {%>		
	    <!--  TODO 
		<tr>
			<td>
			<td>Experiment
			<td colspan="21">
				Compounds were found in the following experiments. Check box to limit to only that specific experiment.
				<table>
					<tr>
						<td><a href="pubsample.jsp?promt=115958">115958
						<td>Fatb Induction Experiment (FatBIE)
					<tr>
						<td><a href="pubsample.jsp?promt=115958">115958
						<td>Fatb Induction Experiment (FatBIE)
					<tr>
						<td><a href="pubsample.jsp?promt=115958">115958
						<td>Fatb Induction Experiment (FatBIE)
					<tr>
						<td><a href="pubsample.jsp?promt=115958">115958
						<td>Fatb Induction Experiment (FatBIE)
				</table>
				 -->
	    <% } %>		
	    
	    


	<tr>
		<td>
		<td>Limit
		<td colspan="21">
		current limit of elements per page is <input type="text" size="3" value="<%=limit %>" name="limit"/>

	</table>
	</form>

	<table align="center" width="60%" cellpadding="5">	
	<tr>
		<td rowspan="1401">&nbsp;
		<th>sample ID
		<th colspan="2">class ID
		<th>sample Label
		<%
			if (id == null){
	    %>
		<th>experiment
		<th>species
		<%} %>
		<th>
		<th colspan="21" ali>compounds
	
	<%	
	boolean[] runs = new boolean[]{true, false};
	List listSamples = sqlQuery.addEntity(PubSample.class).list();
	for (int i = 0; i < runs.length; i++){
	    
	}
	Iterator iterator = listSamples.iterator();
	while (iterator.hasNext()) {
	    PubSample sample = (PubSample) iterator.next();
	    long promtID = new SXQuery(hqlSession).findPromtIDbySample(sample.getSetupXsampleID());
	    
    	boolean locked = true;
    	try {
	    	locked = (PromtUserAccessRight.READ > new SXQuery(hqlSession).findPromtUserAccessRightForUserID(user.getUOID(), promtID).getAccessCode());
		} catch (Exception e){
		    // no access
		}

	    
		//String promtStructure = new SXQuery(hqlSession).findPromtStructure(promtID);
	    long clazzID = new SXQuery(hqlSession).findClazzIDBySampleID((int)sample.getSetupXsampleID());
	    String species = new SXQuery(hqlSession).findSpeciesbyPromtID(promtID).get(0).toString();
%>
	<tr>
		<td align=center align="center" style=background-color:white;><%=sample.getSetupXsampleID()%>
		<a href="sample_detail.jsp?id=<%=sample.getSetupXsampleID() %>">
					<br><img src="pics/details.gif" border="0" align="middle"></a>

		<td style=background-color:<%=Util.getColor((int)(clazzID/3)) %> align="center">
			&nbsp;
		<td style=background-color:white; align="center">
				<%=clazzID%>
				<br><font class="small"><nobr>
					<%if (!locked) { %>
						<%=Util.replace(new SXQuery(hqlSession).findClassInformationString(clazzID), '\n', "<br>")%></nobr>
					<%} else { %>
						<img src="pics/locked.jpg" border="0" align="middle">
					<%}%>
				</font>
				
		<td align=center style=background-color:white;>
					<%if (!locked) { %>
						<nobr><%=new SXQuery(hqlSession).findSampleLabelBySampleID(sample.getSetupXsampleID())%></nobr></a>
					<%} else { %>
						***********
					<%}%>
			<a href="sample_detail.jsp?id=<%=sample.getSetupXsampleID() %>">
			<br><img src="pics/details.gif" border="0" align="middle"></a>

		<%
			if (id == null){
	    %>
	
			<td style=background-color:white; align="center">
					<%if (!locked) { %>
					<%
						String pString = new SXQuery(hqlSession).findPromtTitleByPromtID(promtID);
						if (pString.length() > 0){
						    %><%=pString%><br><%
						}
					%>
					<%} else { %>
						***********
					<%}%>
					<font class="small"> <%=promtID%><br></font>
					<a href="load?id=<%=promtID %>&action=20"><img src="pics/details.gif" border="0" align="middle" title="details"></a>
					<a href="pubsample.jsp?promt=<%=promtID %>"><img src="pics/c_2.gif" border="0" align="middle" title="Select only samples that are in the experiment <%=promtID %>"> </a>
					
			<td align="center" style=background-color:white;><nobr><%=species%></nobr>
				<a href="advancedquery_dev.jsp?term=<%=species%>&type=0"><br><img src="pics/details.gif" border="0" align="middle"></a>
		<%} %>
		<th>&nbsp;
		
		<%
                 Set attributes = sample.getPubAttributes();
                 HashSet hashSet = new HashSet(attributes);
                 TreeSet set = new TreeSet(new Comparator() {
                     public int compare(Object o1, Object o2) {
                         PubAttribute att1 = (PubAttribute) o1;
                         PubAttribute att2 = (PubAttribute) o2;
                         return att1.getLabel().compareTo(att2.getLabel());
                     }
                 });
                 set.addAll(attributes);
                 Iterator att = set.iterator();
                 // position of the criteria in the vector
                 int numberOfCriteria = 0;
                 while (att.hasNext()) {
                     String colorize = "";
                     PubAttribute pubAttribute = (PubAttribute) att.next();
                     // figure out if this is a attribute that was filtered by
                     for (int a = 0; a < criteraVector.size(); a++){
        				 String[] values = (String[])criteraVector.get(a);

        				 if (values[0].compareTo(pubAttribute.getLabel()) == 0) {
        				     numberOfCriteria = a;
        				     colorize = "style=background-color:" + Util.getColor(numberOfCriteria);
        				 }
                     }
                     %>
		
		
				<% if(colorize.length()> 0){ %>
					<td <%=colorize %> align="center">
					<%
						List maxMinQueryList = null;
						if (cache.containsKey(pubAttribute.getLabel())){
						    maxMinQueryList = (List)cache.get(pubAttribute.getLabel());
						} else {
							StringBuffer maxMinQueryString = new StringBuffer();
							maxMinQueryString.append("select max(value) as max, min(value) as min, avg(value) as avg from pubattribute where label = \"" + pubAttribute.getLabel() + "\"");
							SQLQuery maxMinQuery = hqlSession.createSQLQuery(maxMinQueryString.toString());
							maxMinQuery.addScalar("max", Hibernate.LONG);
							maxMinQuery.addScalar("min", Hibernate.LONG);
							maxMinQuery.addScalar("avg", Hibernate.LONG);
							maxMinQueryList = maxMinQuery.list();
							cache.put(pubAttribute.getLabel(), maxMinQueryList);
						}
						
						long max = Long.parseLong(
					        	((Object[])maxMinQueryList.get(0))[0]
					        		.toString());
						long min = Long.parseLong(
					        	((Object[])maxMinQueryList.get(0))[1]
					        		.toString());
						long avg = Long.parseLong(
					        	((Object[])maxMinQueryList.get(0))[2]
					        		.toString());
						
						
						String[] searchcriteria = ((String[])criteraVector.get(numberOfCriteria)); 
						

						double minCriteria = (100 * (Double.parseDouble(searchcriteria[1])) / (max-min)); 
						double maxCriteria  = (100 * (Double.parseDouble(searchcriteria[2])) / (max-min)); 
						
						long percentAverage = (100 * (avg - min) / (max-min)); 

						long percent = (100 * (pubAttribute.getValue() - min) / (max-min)); 
						%>


					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" width="<%=(minCriteria) %>%" style=background-color:black;><font class="small">&nbsp;</font>
							<td align="center" width="<%=(percent-minCriteria) %>%" style=background-color:white;><font class="small">&nbsp;</font>
							<td align="center" width="<%=1%>" style=background-color:lime;><font style="font-size:-5">&nbsp;</font>
							<td align="center" width="<%=(maxCriteria-percent) %>%" style=background-color:white;><font class="small">&nbsp;</font>
							<td width="<%=(100-maxCriteria)%>%" style=background-color:black;>
					</table>		
					<%if (showNonsearchedCompounds){ %>
						<nobr>
							min: <%=min %>					
							max: <%=max %>
						</nobr>
					<br>
					<%} %>
					<nobr>
						<font class="small">
						<a href="pub_sample_update.jsp?field_<%=numberOfCriteria%>_0=<%=pubAttribute.getLabel()%>&field_<%=numberOfCriteria%>_1=<%=pubAttribute.getValue()-(pubAttribute.getValue()*0.1)%>">-10%</a>
							</font>
							<%=pubAttribute.getValue()%>
							<font class="small">
						<a href="pub_sample_update.jsp?field_<%=numberOfCriteria%>_0=<%=pubAttribute.getLabel()%>&field_<%=numberOfCriteria%>_2=<%=pubAttribute.getValue()+(pubAttribute.getValue()*0.1)%>">+10%</a>
						
					</nobr>
				<%} else if (showNonsearchedCompounds){ %>
					<td <%=colorize %> align="center">
						<nobr><a href="pubsample.jsp?<%=pubAttribute.getLabel()%>=<%=pubAttribute.getValue()%>"><%=pubAttribute.getLabel()%></a></nobr> <br>
						<a href="advancedquery_dev.jsp?term=<%=pubAttribute.getLabel()%>&type=1">
					<br>
					<font class="small">
					<%=pubAttribute.getValue()%></font>
				<% } %>
		<%
		}
	}
	%>
</table>
<%@ include file="footer.jsp"%>




		