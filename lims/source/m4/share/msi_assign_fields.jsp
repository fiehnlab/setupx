<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<html>


<%@ include file="checklogin.jsp"%>
<%@ include file="incl_header_msi.jsp"%>

<body>

<table align="center" width="60%">

	<%
		String question = request.getParameter("question");
		
		// search for all formobjects that are not mapped and that have the same question
		
		
		String query = "select description, helptext, count(uoid) as number from formobject where question = \"" + question + "\" and msiattribute is null or msiattribute not in (select uoid from msiattribute) group by helptext";

		Session s = CoreObject.createSession();
		
		Object[] objects = (Object[]) s.createSQLQuery(query)
					.addScalar("description", Hibernate.STRING)
					.addScalar("helptext", Hibernate.STRING)
					.addScalar("number", Hibernate.INTEGER)
					.list().get(0);
	%>
	

		<tr>
			<th rowspan="21">&nbsp;&nbsp;
			<th colspan="3">Declaring fields as MSI Attributes.
			<th rowspan="21">&nbsp;&nbsp;
		<tr>
			<td colspan="3">All <%=objects[2] %> attributes called <b><%=question %></b> will be declared as a MSI attribute.
			<p>Please select the MSI Attribute that you want to assign them to.
		

	<form action="msi_assign_commit.jsp">


	<%
	List attributes = CoreObject.persistence_loadAll(MSIAttribute.class, s);
	
	Iterator iterator = attributes.iterator();
	while (iterator.hasNext()){
	    MSIAttribute attribute = (MSIAttribute)iterator.next();
	    %>
		<tr>
		
  		<td>
			<input type="radio" name="msi" value="<%=attribute.getUOID() %>">
			<input type="hidden" name="question" value="<%=question %>">
	    <%@ include file="incl_msiattribute.jsp"%>


		</tr>
		<%
		}
%>	
	<tr><td colspan="21"><input type="submit">	
	</table>
	</form>



</body>
</html>