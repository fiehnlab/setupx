<%@ include file="incl_searchtypes.jsp"%>
<%@ include file="checknonlogin.jsp"%>
<%@page import="org.setupx.repository.Config"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>


<%
boolean cachedQueries = org.setupx.repository.Config.QUERY_CACHING_ACTIVE_JSP;

try {
	String cache = request.getParameter("cache");
    if (cache.compareTo("false") == 0){
        cachedQueries = false;
    }
} catch (Exception e){
    
}
%>
<%
// -----------------------------
//      general search jsp	
// -----------------------------

String searchTerm = request.getParameter("term");    

// numberOfResultsPerPage
final int numberOfResultsPerPage = 20;

// limit it to certain number of elements.
int limitMax = numberOfResultsPerPage;
int limitMin = 0;
String publicOnly = "";

try {
	limitMax = (Integer.parseInt(request.getParameter("limitmax").toString()));
	limitMin = (Integer.parseInt(request.getParameter("limitmin").toString()));
} catch (Exception e){
    Logger.log(this, "illegal limit.");
}


try {
	publicOnly = request.getParameter("public").toString();
} catch (Exception e){
    Logger.log(this, "no public parameter.");
}
boolean pubOnly = (publicOnly.compareTo("true") == 0);


int selectedSearchTypeInt = 99;
try {
	selectedSearchTypeInt = (Integer.parseInt(request.getParameter("type") + ""));
} catch (Exception e){
	e.printStackTrace();	    
}

String selectedSearchTypeString = "";
if (selectedSearchTypeInt != 99){
    selectedSearchTypeString = searchTypes[selectedSearchTypeInt];
} else {
    selectedSearchTypeString = "";
}


Session s = CoreObject.createSession();
%>








<table align='center' border='0' cellpadding='0' cellspacing="0" width="60%" style="background-color:white;" >
	<tr>
		<td colspan="21" style="background-color:#6682B6;" align="right"><font class="small"><%=new Date().toLocaleString() %>&nbsp;</font>

	<tr>
		<td width="5%" style="background-color:#6682B6;" align="left" valign="top">
			<img  border='0' src="pics/corner_white2.gif" >
		</td>	

		<td colspan="21" style="background-color:white;" align="center">
			<font style="text-align: center; font-size:20">Samples containing <i><%=searchTerm%></i></font>
		</td>
			
	<tr>
			<td style="background-color: white" align="center" colspan="21">
			<font class="small">The following information was found in the repository.<br>&nbsp;
			</font>

	<tr>
		<td colspan="7">
		<table align="center" width="100%" border=0 style="background-color:white; ">
			<tr>
							<form action="advancedquery_dev.jsp">
								<td style="background-color:white;" align="right" valign="top"><a href="q_main_ajax.jsp?type=1"><img  border='0' src="pics/back.gif"></td>
								<td style="background-color:white;" align="right" valign="top" width="400"></td>
								<td align="right" style="background-color: white"><input type="text" type="text" id="testinput" name="term" size="20" style="text-align: center; font-size:20" value="<%=searchTerm	%>"> 
								<td style="background-color:white;" align="left"><input type="image" src="pics/go_search.gif" />
								<input type="hidden" name="type" value="1"> 
								</form>
		</table>
	</tr>		
</table>
		
<table align='center' border='0' cellpadding='4' cellspacing="0" width="60%">

<%
List sampleIDList = new Vector();


switch (selectedSearchTypeInt) {
case 1:
    /* samples */
	String queryCompoundNames = "select sample.setupXsampleID as sampleID from pubattribute as att, pubsample as sample where sample.uoid = att.parent and att.label like \"%" + searchTerm + "%\" group by sample.uoid order by sample.uoid desc";
	sampleIDList.addAll(s.createSQLQuery(queryCompoundNames).addScalar("sampleID", Hibernate.STRING).setCacheable(cachedQueries).list()); 
    
    break;

case 0:
	Iterator promtIDiter = new SXQuery().findPromtIDbySpecies(searchTerm).iterator();
	while(promtIDiter.hasNext()){
		sampleIDList.addAll(new SXQuery().findSampleIDsByPromtID(Long.parseLong(promtIDiter.next().toString()) )); 
	}

case 4:
	String querySamples = "select sample.uoid as id from formobject as sample, formobject as child where sample.uoid = child.parent " +
	"and child.value like \"%" + searchTerm + "%\" and sample.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.Sample\" order by sample.uoid desc";
	sampleIDList.addAll(s.createSQLQuery(querySamples).addScalar("id", Hibernate.STRING).setCacheable(cachedQueries).list()); 

    break;

default:
    break;
}
int counter = 0;
Iterator sampleIDIterator = sampleIDList.iterator();


%>

	<tr>
		<td style="background-color: white">
		<td colspan="5" style="background-color: white">
		<table style="background-color: white" width="100%">
			<tr>
			<td style="background-color: white"> 
				<a href="?term=<%=searchTerm%>&type=<%=selectedSearchTypeInt%>&limitmax=<%=numberOfResultsPerPage%>&limitmin=<%=0%>">
					<img  border='0' src="pics/page-prev-o.gif" title="first entry">
				</a>
			<td style="background-color: white"> 
				<a href="?term=<%=searchTerm%>&type=<%=selectedSearchTypeInt%>&limitmax=<%=limitMax-numberOfResultsPerPage%>&limitmin=<%=limitMin-numberOfResultsPerPage%>">
					<img  border='0' src="pics/page-prec.gif" title="prev page">
				</a>
			<td style="background-color: white"> 
				<a href="?term=<%=searchTerm%>&type=<%=selectedSearchTypeInt%>&limitmax=<%=limitMax+numberOfResultsPerPage%>&limitmin=<%=limitMin+numberOfResultsPerPage%>">
					<img  border='0' src="pics/page-suiv.gif" title="next page">
				</a>
			<td style="background-color: white"> 
				<a href="?term=<%=searchTerm%>&type=<%=selectedSearchTypeInt%>&limitmax=<%=sampleIDList.size()%>&limitmin=<%=sampleIDList.size()-numberOfResultsPerPage%>">
					<img  border='0' src="pics/page-next-o.gif" title="last page">
				</a>
			<td style="background-color: white"> 
				<a href="?term=<%=searchTerm%>&type=<%=selectedSearchTypeInt%>&limitmax=1000000&limitmin=0&public=<%=!pubOnly%>">
					<img  border='0' src="pics/paste.gif" title="public">
				</a>
			<td style="background-color: white" colspan="21" align="right" width="90%" > <font class="small"> [<%=limitMin %> to <%=limitMax %> out of <%=sampleIDList.size() %>]</font>
		</table>
		<td style="background-color:white;" align="right" valign="top"><a href="pubsample.jsp?<%=searchTerm%>"><img  border='0' src="pics/advanced.gif"></td>

		
    </tr>
    <tr>
    	<th>
    	<th colspan="3">
    	<th colspan="2">
    	<th>
    </th>
		
    </tr>
    <tr>
    	<th>
    	<th colspan="3">sample
    	<th colspan="2">experiment
    	<th>species
    </th>
    <tr>
    	<td>
    	<td><font class="small">sampleID
    	<td><font class="small">detail
    	<td><font class="small">label
    	<td><font class="small">StudyID
    	<td><font class="small">details
    	<td><font class="small">
    </th>
    
    
			
<%
while(sampleIDIterator.hasNext() && counter < limitMax ){
    counter++;
    if (counter >= limitMin){
        	
        
    	long sampleID = Long.parseLong(sampleIDIterator.next().toString());
    	String sampleLabel = new SXQuery().findSampleLabelBySampleID(sampleID);
    	long experimentID = new SXQuery().findPromtIDbySample(sampleID);

    	boolean locked = true;
    	try {
	    	locked = (PromtUserAccessRight.READ > new SXQuery().findPromtUserAccessRightForUserID(user.getUOID(), experimentID).getAccessCode());
		} catch (Exception e){
		    // no access
		}
    	String species = new SXQuery().findSpeciesbyPromtID(experimentID).get(0).toString();
    	
    	// in case only public samples should be shown
    	if ((pubOnly && !locked) || !pubOnly){
    	    %>
		    <tr>
		    	<td align="right"><font class="small"><%=counter %></font> 
		    	<td align="center"><%=sampleID %> 
		    	<%if(locked){%>    
		    		<td colspan="2" align="center" style="background-image: url('pics/locked.jpg'); background-repeat: no-repeat; background-position: center center; background-color: white;"><font class="x-small" ><i><b>not public</b></i></font>
		    	<%}else{ %>
			    	<td width="2" align="center" style="background-color: white"><a href="sample_detail.jsp?id=<%=sampleID %>"><img  border='0' src="pics/details.gif"></a>
			    	<td align="center"><%=sampleLabel %>
		    	<%} %>
		    	
		    	<%if(locked){%>    	
		    		<td colspan="2" align="center" style="background-image: url('pics/go_locked.gif'); background-repeat: no-repeat; background-position: center center; background-color: white;"><font class="x-small" ><i><b>not public</b></i></font>
		    	<%}else{ %>
			    	<td align="center"><%=experimentID %>
			    	<td width="2" align="center" style="background-color: white"><a href="pubexperiment.jsp?id=<%=experimentID %>&action=20"><img  border='0' src="pics/details.gif"></a>	
		    	<%} %>


		    	<td align="center">
		    		<a href="advancedquery_dev.jsp?term=<%=species %>&type=0"><%=species %></a>
		    </tr>
    	    <%
    	}
    	%>
    	<%
    } else {
        sampleIDIterator.next();
    }
} %>

<!-- 
	<tr>
		<td colspan="21" style="background-color: white" align="center">
						<img border='0' src="http://upload.wikimedia.org/wikipedia/commons/c/cb/<%=searchTerm.toUpperCase().charAt(0)%><%=searchTerm.toLowerCase().substring(1)%>.png" >
 -->
 </table>

<%@ include file="footer.jsp"%>




