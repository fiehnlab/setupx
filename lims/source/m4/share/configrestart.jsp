<%@page import="javax.management.MBeanServerInvocationHandler"%>
<%@page import="javax.management.MBeanServer"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="javax.naming.Context"%>
<%@page import="javax.management.ObjectInstance"%>
<%@page import="javax.management.ObjectName"%>

<%

    Context ic = new InitialContext();
	String jndiName = "java:comp/env/jmx/GlobalMBeanServer";
    MBeanServer _mbeanServer = (MBeanServer) ic.lookup(jndiName);

    try {
      String webAppName = request.getParameter("webapp");

      String action = request.getParameter("action");

      
      String name = ("resin:" +
              "Server=default," +
              "Host=default," +
              "type=WebApp," +
              "name=" + webAppName);
	
	ObjectName objectName = new ObjectName(name);
	
	ObjectInstance object = _mbeanServer.getObjectInstance(objectName);
	
	Object webApp = (WebAppMBean) MBeanServerInvocationHandler.newProxyInstance(_mbeanServer,
	                                               objectName,
	                                               Object.class,
	                                               false);

	
	/*
      if (webApp == null) {
        out.println("Can't find web-app " + webAppName);
        return;
      }

     if ("start".equals(action)) {
        webApp.start();
        out.println("started " + webAppName);
      }
      else if ("stop".equals(action)) {
        webApp.stop();
        out.println("stopped " + webAppName);
      }
      else if ("update".equals(action)) {
        webApp.update();
        out.println("updated " + webAppName);
      }
      else
        out.println(action + " is an unknown action.");
    } catch (Exception e) {
      throw new ServletException(e);
    }
    */

%>
