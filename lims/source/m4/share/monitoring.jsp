<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>


<%@ include file="checklogin.jsp"%>

<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.hibernate.Session"%>


<%@page import="java.util.Date"%>
<%@page import="org.setupx.repository.web.forms.FormObject"%>
<%@page import="org.setupx.repository.web.forms.inputfield.InputField"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/	>



<%
	// refresh 
	int refreshSec = Integer.parseInt(request.getParameter("refresh"));


Hashtable sessions = org.setupx.repository.web.HttpSessionPool.getSessions();


	Enumeration keys = sessions.keys();
	
    javax.servlet.http.HttpSession httpSession = null;

	while(keys.hasMoreElements()){
	    try {
		    String key = "" + keys.nextElement();
		    UserDO sessionUser = null;
		    httpSession = (javax.servlet.http.HttpSession) sessions.get(key);
			sessionUser = (UserDO)httpSession.getAttribute("user");
			
			%>
		<table>
		  <tr>
		    <th colspan="2"><%=sessionUser.getDisplayName() %>
		  </tr>
			
			<%
		    try {
				// load the promt from the current session
				Promt promt = (Promt)httpSession.getAttribute(WebConstants.SESS_FORM_PROMT);
				Integer pagePos = (Integer)httpSession.getAttribute("pos");
				Date lastAccess =new Date(httpSession.getLastAccessedTime());

		%>
	
		  <tr>
		    <td>Last Access:</td>
		    <td><%=lastAccess.toLocaleString() %></td>
		  </tr>
		  <tr>
		    <td>Promt:</td>
		    <td><%=promt.getUOID() %></td>
		  </tr>
		  <tr>
		    <td>Last savepoint:</td>
		    <td><%=((new Date().getTime() - Long.parseLong(httpSession.getValue("TIME") + "")) / 1000)%> sec ago</td>
		  </tr>
		  <tr>
		    <td>Classes:</td>
		    <td><%=promt.getClazzes().length %></td>
		  </tr>
		  <tr>
		    <td>Samples:</td>
		    <td><%=promt.getSamples().length %></td>
		  </tr>
		  <tr>
		    <td>Page:</td>
		    <td><%=pagePos %></td>
		  </tr>
		  <tr>
		  	<td>
		  	<td>
		  		<table>
		  					<%
		  						FormObject[] formObjects = promt.getFields();
		  						for (int f = 0; f < formObjects.length; f++){
		  						    if ( formObjects[f].isValid() ){
		  						    	%>
										<tr>
											<td style="background-color: white"><img src="pics/ok.gif">
											<td>
										<%    
		  						    } else {
		  						    	%>
		  						    	<tr>
		  						    		<td style="background-color: white"><img src="pics/err.gif">
		  						    		<td>
						  						<%
							  						FormObject[] formObjectss = formObjects[f].getFields();
							  						for (int f1 = 0; f1 < formObjectss.length; f1++){
							  						    if (! formObjectss[f1].isValid() ){
							  						    	%>
							  						    	<%=((InputField)formObjectss[f1]).getQuestion() %>: <%=formObjectss[f1].validate() %><br>
		  						    						<%    
							  						    }
							  						}
		  						    }
		  						}
		  					
		  					%>
		  				
		  		</table>
		  <tr>
		    <td>session attributes:</td>
		    <td><%
				String[] valueNames = httpSession.getValueNames();
		    	for (int i = 0; i < valueNames.length; i++){
		    	    %><%=valueNames[i]%>: <%=httpSession.getValue(valueNames[i])%><br><%
		    	} %>
		    	</td>
		  </tr>
			<%
		    } catch (Exception e){
		    	%>
		  	  <tr>
			    <td>Err:</td>
			    <td><%=e %></td>
			  </tr>
				<%
		    }
	%>

	</table>
	<%	
	
	} catch (Exception e){
	    e.printStackTrace();
	}
	}
%>

<meta http-equiv="refresh" content="<%=refreshSec %>; URL=monitoring.jsp?refresh=<%=refreshSec %>">


