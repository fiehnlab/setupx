<%@page import="org.setupx.repository.Config"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<font style="font-family: sans-serif;">
<table align="center">
	<tr>
		<td><a href="main.jsp">back to <%=Config.SYSTEM_NAME %></a>&nbsp;
		<td><a href="pubexperiment.jsp?id=<%=promtID%>">Report</a>&nbsp;
		<td><a href="pubexperiment_extended.jsp?id=<%=promtID%>">Experimental Design</a>&nbsp;
		<td><a href="compoundPerExp.jsp?id=<%=promtID %>">Compound Charts</a></ol>&nbsp;

					<% 
						String filename = Config.DIRECTORY_RELATED + File.separator + promtID + File.separator + promtID + "_public.zip";
						File file = new File(filename);
						if (file.exists()){
						    %>
							    <td style="background-color: white" align="center"><a href="download?<%=WebConstants.PARAM_FILENAME%>=<%=file.getName() %>&<%=WebConstants.PARAM_PROMTID%>=<%=promtID%>">Annotation Result</a></td>
						    <%
						} else {
						    %>
						    <td style="background-color: white" align="center">Annotation Result not available</td>
						    <%
						}
					%>

		<td><a href="download_result.jsp?id=<%=promtID %>">Instrument results</a></ol>&nbsp;
		<td><a href="load?id=<%=promtID %>&action=20">LIMS View</a></ol>&nbsp;
		<td><a href="msi_report_xml.jsp?id=<%=promtID %>"><img src="pics/xml3.gif" border="0"></a></ol>&nbsp;
		<td><a href="msi_report_xml.jsp?id=<%=promtID %>&data=true"><img src="pics/xml.gif" border="0"></a></ol>&nbsp;	
		<td rowspan="2"><a href="report_xls.jsp?id=<%=promtID %>&data=true"><img src="pics/xls.gif" border="0"></a></ol>&nbsp;	
	</tr>
	<tr>
		<td>
		<td>
		<td>
		<td>
		<td>
		<td align="center"><font style="font-size: xx-small;">metadata
		<td align="center"><font style="font-size: xx-small;">complete	
	</tr>
</table>
</font>
