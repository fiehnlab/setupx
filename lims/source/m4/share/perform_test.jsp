<%@page import="java.util.GregorianCalendar"%><%@page import="java.util.Calendar"%><%@page import="org.hibernate.Hibernate"%><%@page import="java.util.List"%><%@page import="org.hibernate.Session"%><%@page import="org.hibernate.SessionFactory"%><%@page import="org.hibernate.cfg.Environment"%><%@page import="org.hibernate.cfg.Configuration"%><%@page import="java.util.Enumeration"%><%@page import="java.io.File"%><%@page import="org.setupx.repository.Config"%><%@page import="java.io.FileInputStream"%><%@page import="java.util.Properties"%><%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.web.forms.inputfield.StringInputfield"%>
<%@page import="org.exolab.castor.xml.schema.Form"%>
<%@page import="org.setupx.repository.web.forms.FormObject"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="org.exolab.castor.xml.AccessRights"%>
<%@page import="org.setupx.repository.server.logging.Message"%>
<%@page import="org.setupx.repository.web.WebAccess"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.web.forms.event.Event"%>
<%@page import="org.setupx.repository.web.pub.data.PubAttribute"%>
<%@page import="org.setupx.repository.core.communication.status.Status"%>
<%@page import="org.setupx.repository.core.communication.importing.logfile.ScannedPair"%><%	// testing performance of different cache implementations	for (int c = 0; c < 4; c++){	    try {	SessionFactory factory = null; 	Logger.debug(this, "[init()]creating session factory ");        if (factory == null) {            String key;            String value;            Logger.debug(this, "[init()]setting up: SessionFactory");            // reading properties            Properties properties = new Properties();            Logger.debug(this, "[init()]start reading properties.");            try {                // check if file is ok                File fileConfiguration = new File(Config.CONF_HIBERNATE_FILE);                Logger.debug(this, "[init()]reading config: " + fileConfiguration.toString());                if (!fileConfiguration.exists() ||  !fileConfiguration.canRead()) throw new RuntimeException("can not init Persistence: ConfigurationFile " + Config.CONF_HIBERNATE_FILE + " can not be read.");                properties.load(new FileInputStream(fileConfiguration));                Logger.debug(this, "[init()]read config: " + Config.CONF_HIBERNATE_FILE);            } catch (Exception e) {                throw new RuntimeException();            }            Logger.debug(this, "[init()]creating new Configuration");            Configuration cfg = new Configuration();            Enumeration enumeration = properties.keys();            while (enumeration.hasMoreElements()) {                key = (String) enumeration.nextElement();                value = (String) properties.get(key);
                if (key.compareTo("hibernate.cache.provider_class") != 0){
                    Logger.debug(this, "[init()]setting values: " + key + ": " + value);
                    cfg.setProperty(key, value);
                }
            }                        // caching            
            
            %>
			<br> changing cache provider: was: <%=cfg.getProperty("hibernate.cache.provider_class")%><br><%
            // cfg.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.HashtableCacheProvider");
			            
			
			
			switch (c) {
			case 0:
			    cfg.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.HashtableCacheProvider");
			    cfg.setProperty("hibernate.cache.use_query_cache", "false");
			    cfg.setProperty("hibernate.cache.use_second_level_cache", "false");
			    break;
			
			case 1:
			    cfg.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.HashtableCacheProvider");
			    cfg.setProperty("hibernate.cache.use_query_cache", "true");
			    cfg.setProperty("hibernate.cache.use_second_level_cache", "true");
			    break;
			
			case 2:
			    cfg.setProperty("hibernate.cache.use_query_cache", "true");
			    cfg.setProperty("hibernate.cache.use_second_level_cache", "true");
			    cfg.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.EhCacheProvider");
			    break;
			
			case 3:
			    cfg.setProperty("hibernate.cache.use_query_cache", "true");
			    cfg.setProperty("hibernate.cache.use_second_level_cache", "true");
			    cfg.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.OSCacheProvider");
			    break;
			
			default:
			    break;
			}


            Logger.debug(this, "[init()]adding mapping directory: " + Config.DIRECTORY_CONF_HIBERNATE + "mapping");
            cfg.addDirectory(new File(Config.DIRECTORY_CONF_HIBERNATE + "mapping"));

            cfg.setProperty("hibernate.show_sql", "true");
            cfg.setProperty(Environment.HBM2DDL_AUTO, "update"); //'create', 'create-drop' and 'update'.

			try {
	            Logger.debug(this, "[init()]building SessionFactory");        	    factory = cfg.buildSessionFactory();			} catch (Exception e){
			    e.printStackTrace();
	            Logger.debug(this, "[init()]deactivating caching");
                cfg.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.HashtableCacheProvider");
			    cfg.setProperty("hibernate.cache.use_second_level_cache", "false");
			    cfg.setProperty("hibernate.cache.use_query_cache", "false");
	            Logger.debug(this, "[init()]building SessionFactory");
        	    factory = cfg.buildSessionFactory();
			}
            %> changing cache provider: 
            	now: <%=cfg.getProperty("hibernate.cache.provider_class")%> 			  
			    <%=cfg.getProperty("hibernate.cache.use_query_cache") %>
			    <%=cfg.getProperty("hibernate.cache.use_second_level_cache") %>
			    <br><%
            Logger.debug(this, "[init()]sessionfactory: " + factory);            Logger.debug(this, "[init()]done setting up: SessionFactory");                                }            	Session hqlSession = factory.openSession();//    	int num_samples = (int)Integer.parseInt("" + hqlSession.createSQLQuery("select count(DISTINCT s.sampleID) as result from scanned_samples as s where s.label != '' and s.sampleID != ' ' and s.sampleID != '1'").addScalar("result", org.hibernate.Hibernate.INTEGER).uniqueResult());    	     	String queryNumbers = " select count(distinct value) as species, sum(c) as experiments, sum(samples) as samples from (     " +    	"select label.value as value, count(promt.uoid) as c, sum(cache.numberofScannedSamples) as samples  " +        "from formobject as promt  " +        "inner join formobject as page   " +        "on page.parent = promt.uoid  " +        "inner join formobject as species  " +        "on species.parent = page.uoid   " +        "inner join formobject as ncbispecies  " +        "on ncbispecies.parent = species.uoid  " +         "inner join formobject as label   " +        "on label.parent = ncbispecies.uoid   " +        "inner join cache as cache  " +        "on cache.experimentID = promt.uoid " +         "inner join formobject as f  " +        "on f.uoid = cache.experimentID " +         "where ncbispecies.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\" " +        "and label.value != \"\"    " +        "and cache.numberofScannedSamples > 0 " +         "group by label.value " +     	") as a";    	
     	
     	int limit = 5000;     	boolean cache = false;     	for (int run = 0; run < 20; run++){
     	    if (run < 10){
     	       cache = false;
     	    } else {
     	        cache = true;
     	    }     	    long start = new GregorianCalendar().getTimeInMillis();
     	    List l = hqlSession.createSQLQuery(queryNumbers).addScalar("species", Hibernate.STRING).addScalar("experiments", Hibernate.STRING).addScalar("samples", Hibernate.STRING).setCacheable(cache).list();        	Object[] obs = (Object[])l.get(0);          	         	long num_experiment = Long.parseLong("" + hqlSession.createQuery("select count(*) from " + Promt.class.getName()).setCacheable(cache).uniqueResult());        	String num_experiments = "" + obs[1];        	String num_species = "" + obs[0];        	String num_samples = "" + obs[2];

        	hqlSession.createCriteria(Message.class).setCacheable(cache).addOrder(Order.asc("id")).setMaxResults(limit).list();
        	hqlSession.createCriteria(Event.class).setCacheable(cache).addOrder(Order.asc("id")).setMaxResults(limit).list();
        	hqlSession.createCriteria(PubAttribute.class).setCacheable(cache).addOrder(Order.asc("id")).setMaxResults(limit).list();
        	hqlSession.createCriteria(Status.class).setCacheable(cache).addOrder(Order.asc("id")).setMaxResults(limit).list();
        	//hqlSession.createCriteria(WebAccess.class).setCacheable(cache).addOrder(Order.asc("id")).setMaxResults(limit).list();
        	hqlSession.createCriteria(ScannedPair.class).setCacheable(cache).addOrder(Order.asc("id")).setMaxResults(limit).list();
        	     	    long end = new GregorianCalendar().getTimeInMillis();        	%>
        	<%=(end-start)%><br>     	    <%      	}	} catch (NoClassDefFoundError e){		e.printStackTrace();
		%><%=e.getMessage() %><br><%	}	     	}     	%>