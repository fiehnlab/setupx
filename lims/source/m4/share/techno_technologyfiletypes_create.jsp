<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Technology"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Location"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.util.FilenamePatternFilter"%>
<%@page import="java.util.regex.PatternSyntaxException"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.LocationException"%>
<%@page import="java.util.regex.Pattern"%>


<%
	Session hqlSession = Technology.createSession();

	// creating the pattern and check if it is valid
	try {
	    Pattern.compile(request.getParameter("pattern"));

		String technologyID = request.getParameter("technoid");

		Technology technology = (Technology)Technology.persistence_loadByID(Technology.class, hqlSession, Long.parseLong(technologyID));

		TechnologyFileType technologyFileType = 
		    new TechnologyFileType(
		            request.getParameter("label"), 
		            request.getParameter("desc"), 
		            new Location(request.getParameter("location")),
					request.getParameter("pattern"));
		
		technology.addTechnologyFileType(technologyFileType); 

		technology.updatingMyself(hqlSession, true);
		
		

	%>

<meta http-equiv="refresh" content="0; URL=techno_technology.jsp?id=<%=technologyID%>">

	<%	
	} catch (PatternSyntaxException e){
	    %>
	    Unable to create Filetype because it is not a valid pattern.
		<br>
	    <%=e.getMessage() %>
		<meta http-equiv="refresh" content="6; URL=techno_technology.jsp?id=<%=request.getParameter("technoid")%>">
	    <%
	} catch (LocationException e){
	    %>
	    The Location that you specified does not exist.
		<br>
	    message: (<%=e.getMessage() %>)
		<meta http-equiv="refresh" content="6; URL=techno_technology.jsp?id=<%=request.getParameter("technoid")%>">
	    <%
	}
%>
