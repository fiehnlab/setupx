<%@page import="java.util.List"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.web.ip.BlockedIP"%>



<%

	try {
		String ip = request.getParameter("ip");
		
		String blockString = request.getParameter("block");
	
		boolean block = blockString.compareTo("true") == 0;
		
		
		if (block){
			// create a new blocked ip
			BlockedIP blockedIP = new BlockedIP(ip);
			blockedIP.update(true);
		} else {
		    // find the blocked ip and delete it
		    Session s = CoreObject.createSession();
	        List l = s.createCriteria(BlockedIP.class).add(Restrictions.eq("blockedIP", ip)).list();
	        %><%=l.size() %><%        
	        BlockedIP blockedIP = (BlockedIP)l.get(0);
	
	        blockedIP.remove();
	
	        s.close();
		}
	}catch(Exception e){
	    e.printStackTrace();
	}
%>

<jsp:forward page="blocked_ips.jsp"/>
