<%@ include file="checknonlogin.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<body>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">	
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Repository</h2><br>Content of the Repository</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>




<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} else { %>
<%@ include file="navi_public.jsp"%>
<%} %>



<table align='center' border='0' cellpadding='4' width="60%">
							<tr>
								<th colspan="4"><h3>Content
								<%

								// query is used for the chart in repository_species
								String __query = "select label.value as value, count(promt.uoid) as c, sum(cache.numberofScannedSamples) as samples " +  
								" from formobject as promt " +
								" inner join formobject as page " +
								" on page.parent = promt.uoid " +
								" inner join formobject as species " +
								" on species.parent = page.uoid  " +
								" inner join formobject as ncbispecies " +
								" on ncbispecies.parent = species.uoid  " +
								" inner join formobject as label  " +
								" on label.parent = ncbispecies.uoid  " +
								" inner join cache as cache " +
								" on cache.experimentID = promt.uoid " +
								" inner join formobject as f " +
								" on f.uoid = cache.experimentID " +
								" where ncbispecies.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\"   " +
								" and label.value != \"\"   " +
								" and cache.numberofScannedSamples > 0 " +
								" group by label.value " +
								" order by label.value ";

								Logger.log(this, __query);
								
								int MINNUMBER_OF_EXPERIMENTS = 2;
								boolean IN3D = false;

								%>
									<%@ include file="repository_species.jsp"%>
									<br>
									<table align="center" width="80%" cellpadding="7">
										<tr>
											<td align="center">
												<a href="species_chart.jsp"><img src="<%=url1%>" border="0"></a>
										<tr>
											<td align="center">
												<a href="species_chart.jsp"><img src="<%=url2%>" border="0"></a>
										<tr>
											<td align="center">
												<font class="small">* charts contain species with > <%=MINNUMBER_OF_EXPERIMENTS %> experiments.</font> <a href="species_chart.jsp">More ...</a>
										<tr>
											<td>
												<ul>
												
										<%
										// list of the species
										
								listIter = _list.iterator();
								while (listIter.hasNext()){
								    Object[] objects = (Object[])listIter.next();
									
								    String label = objects[1] + "";
								    String value = objects[0] + ""; 
								    String nr_samples = objects[2] + "";
								    %>
									<li><a href="advancedquery_dev.jsp?term=<%=label %>&&type=0"><%=label %></a>
										<font class="small"> 
											<br>&nbsp; <%=value%> Experiments 
											<br>&nbsp; <%=nr_samples %> Samples
										</font>
									</li>
							    <%
								}										
							    %>
										
									</table>
</table>

<%@ include file="footer.jsp"%>