<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page import="org.setupx.repository.core.communication.file.SampleFile"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Datafile"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.communication.importing.logfile.ScannedPair"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%

	// mapping where which type goes = matching TYPE and TechnologyfiletypeID
	long[] techoIDs = new long[]{5, 1, 4, 3};
	//    public static final short[] TYPES = new short[]{PEG,TXT,SMP,CDF};

	// creating assigned datafiles out of the deprecated scannedpairs
	Iterator promtIDs = new SXQuery().findPromtIDs().iterator();
	while (promtIDs.hasNext()){
		long promtID = Long.parseLong("" + promtIDs.next());
		
		Iterator sampleIDs = new SXQuery().findSampleIDsByPromtID(promtID).iterator();
		while(sampleIDs.hasNext()){
			long sampleID = Long.parseLong("" + sampleIDs.next());

			try {
				SampleFile sampleFile = new SampleFile(sampleID);
				for (int i = 0; i < sampleFile.TYPES.length; i++){
				    Logger.log(this, sampleID + " " + sampleFile.exists(sampleFile.TYPES[i])  + " - " + sampleFile.getFile(sampleFile.TYPES[i]));

				    //if (sampleFile.exists(sampleFile.TYPES[i])){
					    // creating a new datafile
					    Datafile datafile = new Datafile(sampleID, sampleFile.getFile(sampleFile.TYPES[i]));        
					    // find the correct FileType
						Session s = TechnoCoreObject.createSession();
					    TechnologyFileType technologyFileType = (TechnologyFileType)TechnoCoreObject.persistence_loadByID(TechnologyFileType.class, s, techoIDs[i]);					    

						try {

						    %><br> loaded techFileType <%=technologyFileType.getLabel() %>  techoIDs[i]:<%=techoIDs[i]%>   i: <%=i%> <%
						    technologyFileType.addDatafile(datafile);
						    technologyFileType.getTechnology().updatingMyself(s, true);
						    
						    %><BR>added <%=datafile.getSampleID()%> <%=datafile.getTechnologyFileType().getLabel()%> to: <%=technologyFileType.getLabel() %><br> <%
						} catch (Exception pEx){
						    Logger.err(this, pEx.getMessage());
//						    pEx.printStackTrace();
						}
				    //}
				}
			} catch (Exception e){
				Logger.log(this, e.getMessage() );
			}
		}
	}


%>