<%@page import="java.util.Date"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%
out.clear();
response.setContentType("text/xml");
response.setCharacterEncoding("UTF-8");

%><%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<data>
<%
	/*

String query = " select event.SAMPLE as sampleID, event.timestamp as created, message.date as ran " 
			+ " from event join message on message.relatedObjectID = event.SAMPLE "
			+ " group by message.date order by message.date desc";

	*/
	
	//  count(distinct relatedObjectID)
	String query = "select root(relatedObjectID) as a, min(date) as min, max(date) as max " +
	    " from message " +
	    " where message.message like \"%finished%\"  " +
	    //" group by substr(date, 1, locate(' ', date)) " +
	    " group by root(relatedObjectID) " +
	    " order by uoid desc";

	Session hibernateSession = CoreObject.createSession();

	List result = hibernateSession.createSQLQuery(query).addScalar("a", Hibernate.LONG).addScalar("min", Hibernate.STRING).addScalar("max", Hibernate.STRING).list();

	Iterator resultIterator = result.iterator();
	
	/*
Date and Time Pattern 	Result
"yyyy.MM.dd G 'at' HH:mm:ss z" 	2001.07.04 AD at 12:08:56 PDT
"EEE, MMM d, ''yy" 	Wed, Jul 4, '01
"h:mm a" 	12:08 PM
"hh 'o''clock' a, zzzz" 	12 o'clock PM, Pacific Daylight Time
"K:mm a, z" 	0:08 PM, PDT
"yyyyy.MMMMM.dd GGG hh:mm aaa" 	02001.July.04 AD 12:08 PM
"EEE, d MMM yyyy HH:mm:ss Z" 	Wed, 4 Jul 2001 12:08:56 -0700
"yyMMddHHmmssZ" 	010704120856-0700
"yyyy-MM-dd'T'HH:mm:ss.SSSZ" 	2001-07-04T12:08:56.235-0700
*/
// "2008-03-17 17:54:01.0"

	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.");
	dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
	
	
	while(resultIterator.hasNext()){
	    Object[] objects = (Object[])resultIterator.next();
	   	String a = "" + objects[0];


	   	
	   	java.util.Date start = dateFormat.parse("" + objects[1]);
	   	java.util.Date end = dateFormat.parse("" + objects[2]);
	   	//java.util.Date end = (java.util.Date)objects[2];
	   	//start.setMinutes(0);
	   	//start.setHours(0);
	   	//start.setMinutes(0);
	   	//start.setMinutes(0);

	//   	Logger.log(this, " " + a + "  " + start.toGMTString() + "  "  + end.toGMTString());
	   	
	boolean done = false;
	String title = a;
	String apstract = "  ";
	String link = "";
	try {
	    //title = new SXQuery().findPromtTitleByPromtID(Long.parseLong(a)) ;
	    //apstract = new SXQuery().findPromtAbstractByPromtID(Long.parseLong(a));
	    done = new SXQuery().determinePromtFinished(Long.parseLong(a));
		link = "load?id=" + a + "&action=20";
		
	} catch (Exception e){
	    e.printStackTrace();
	}
	   	%><event start="<%=start.toGMTString() %>" end="<%=end.toGMTString() %>" title="<%=(a)%>" icon="<% if (done){ out.print("pics/check.gif"); }else{ out.print("pics/circle.gif"); }%>">
	   	</event>
	   	<%
	}
%></data>
