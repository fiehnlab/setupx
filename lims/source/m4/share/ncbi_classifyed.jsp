<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="checklogin.jsp"%>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBI_Classifier"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIClassifierID"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.setupx.repository.web.forms.restriction.NCBIVocRestriction"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SetupX NCBI Classification </title>
</head>

<body>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">	
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Classification of Species</h2><br>Group species in groups of animal, human. ...</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>
<%
	// list of possible kingdoms
	int[] kingdomIDs = new int[]{
        NCBIEntry.ANIMAL,
		NCBIEntry.PLANT,
		NCBIEntry.HUMAN,
		NCBIEntry.MIRCOORGANISM};
%>



<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<th>
			<h2>List of classifiers</h2>
	
	<tr>
		<td>
		<table align="center">
			<tr>
		<%
			for(int i = 0; i < kingdomIDs.length; i++){
			    %>
		<td width="<%=100/kingdomIDs.length %>%" align="center" valign="top">
		
		<table width="100%">
		  <tr>
		    <th align="center" colspan=4><%=NCBIEntry.getSpeciesTypeName(kingdomIDs[i])%></th>
		  </tr>
		  
		  <% 
		  	// list of all classifiers
		  	List classifiers = NCBIClassifierID.getNCBIIDsPerKingdom(kingdomIDs[i]);
		  	Iterator classifierIDIterator = classifiers.iterator();
		  	while(classifierIDIterator.hasNext()){
		  	  NCBIClassifierID classifierID = (NCBIClassifierID) classifierIDIterator.next();
		  	  %>
		  	  <tr>
		  	    <td><%=classifierID.getNcbiID() %></td>
		  	    <td>
		  	    	<% try{ %>
		  	    	<%=NCBIConnector.determineNCBI_Information(classifierID.getNcbiID()).getName() %>
		  	    	<font class="small"><br><%=NCBIConnector.determineNCBI_Information(classifierID.getNcbiID()).getRank() %></font>
		  	    	<% }catch (Exception e){
		  	    	    %>
		  	    		<i>- unknown -</i>
		  	    		<%
		  	    	}%>
		  	    </td>
		  	    <td><a href="ncbi_classi_remove.jsp?id=<%=classifierID.getUOID()%>"><img src="pics/delete-all-o.gif"/> </a></td>
		  	    <td></td>
		  	  </tr>
		  	  <%  
		  	}
		  	%>
		</table>
		</td>
			    <%
			}
		%>
		</table>
	</tr>



	<%
	NCBIEntry entry = NCBIConnector.determineNCBI_Information(32644);

	try {
		String speciesString = request.getParameter("species");
		int id = NCBIConnector.determineNCBI_Id(speciesString);
		entry = NCBIConnector.determineNCBI_Information(id);
	} catch (Exception e){
	    
	}

	%>
	<tr>
		<th>
			<h2>Search species</h2>
	<tr>
		<td>
			<form action="ncbilookup.jsp">
				<input type="text" name="species" value="<%=entry.getName() %>">
				<br>species: <%=entry.getSpeciesTypeName() %>

				<input type="submit">
			</form>


	<tr>
		<th>
			<h2>Add classifier</h2>
			in order to add another classifier, search for it and then define the group that it should be assigned to.
	<tr>
		<td>
			<form action="ncbi_calli_create.jsp">
			<%
			
			%>
				<input name="id" value="<%=entry.getTaxID()%>">
				<%
				for(int i = 0; i < kingdomIDs.length; i++){
					String selected = "";
					if (entry.getTaxIdInt() == kingdomIDs[i]){
			   		    selected = " checked=\"checked\"";
			   		}
				    %>
				   	<input type="radio" name="kingdom" value="<%=kingdomIDs[i]%>" <%=selected%> /> <%=NCBIEntry.getSpeciesTypeName(kingdomIDs[i])%>
				<%} %>
				
				<input type="submit">
			</form>
</table>

<%@ include file="footer.jsp"%>