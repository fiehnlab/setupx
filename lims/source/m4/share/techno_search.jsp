<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net" xmlns:c="urn:jsptld:http://java.sun.com/jstl/core">
<jsp:directive.page import="java.net.URL"/>
<jsp:directive.page import="org.setupx.repository.server.persistence.SXQuery"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"/>
<jsp:directive.page import="org.hibernate.Session"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Datafile"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Technology"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.hibernate.Hibernate"/>
<jsp:directive.page import="java.util.Vector"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>

  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>


	<jsp:include page="techno_header.jsp"></jsp:include>

	<jsp:scriptlet>
	
	
	// ------------------------------------------------------------------
	// 	List of all datafiles containing the searchterm in the filename or the id
	// ------------------------------------------------------------------
	char qMark = (char)38; // ascii 38 
	char percentMark = (char)37; // ascii 38 
	char Mark = (char)34; // ascii 34 

	String term = request.getParameter("term");

	Session s = TechnoCoreObject.createSession(); 

    String query = "select * from datafile where sampleID like '" + percentMark + term + percentMark + "' or source like '" + percentMark + term +  percentMark + "' order by uoid desc ";
	
	List list = s.createSQLQuery(query).addEntity(Datafile.class).list();

	//
	request.setAttribute("test", list); 

	// style
	pageContext.setAttribute("tableclass", "its");

</jsp:scriptlet>

	<h2>
		<a href="techno_techno.jsp">all platforms</a> - 
		datafile containing <jsp:expression>term</jsp:expression>
	</h2>
		
   	<jsp:include page="inc_techno_datafile.jsp"></jsp:include>
</jsp:root>





