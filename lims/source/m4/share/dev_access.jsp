<meta http-equiv="refresh" content="30; URL=dev_access.jsp">

<%@ include file="checklogin.jsp"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.Date"%>


<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<body>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">	
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Requests to the system</h2><br></th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>


<%

java.util.Locale locale = java.util.Locale.US;

org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

hqlSession.close();
%>



<%@ include file="navi_admin.jsp"%>

<table align='center' border='0' cellpadding='4' width="60%">

<tr>
	<th></th>
	<th colspan="4">Requests to the system</th>
	<th></th>
</tr>







<tr>
	<td></td>
	<td colspan="4">
		Status<br>
		<font class="small">check the status of the lab equipment.</font>
	</td>
	<td align="center"></td>
</tr>



<%
	String[] label = new String[]{"Last 100 Days", "Last 100 Hours", "Last 100 minutes"};
	String[] qSub = new String[]{"(3600000 * 24)", "(3600000)", "(600000)"};

	Session hqlsession = CoreObject.createSession();

	for (int i = 0; i < qSub.length; i++){
	    
	    %>


	    <%

		String query = " select time as t, count(uoid) as n from WebAccess "
			+ "	group by round(time /  " + qSub[i] + ") "
			+ " order by round(time /  " + qSub[i] + ") desc limit 100 ";
		
		List list = hqlsession.createSQLQuery(query).addScalar("t", Hibernate.LONG).addScalar("n", Hibernate.LONG).list();

		long[] dataArray = new long[list.size()];
	    long max = 0;
		
		Iterator it = list.iterator();
			
		Date first = null;
		Date last = null;
		
		int z = -1;
		while (it.hasNext()){
		    Object[] objects = (Object[])it.next();
		    z++;
		    
		    try{
			    dataArray[z] = Long.parseLong(objects[1].toString());
			    if( max < dataArray[z]) max = dataArray[z]; 
			    
			    if ( z == 0) first = new Date(Long.parseLong(objects[0].toString()));
			    last = new Date(Long.parseLong(objects[0].toString()));
		    } catch (Exception e){
		        e.printStackTrace();
		    }
		}
//		label[i] = first.toLocaleString() + " - " + last.toLocaleString();

	    String data = "";
	    
	    for (int d = dataArray.length - 1; d >= 0 ; d--){
		    data = data.concat("" + (dataArray[d] * 100 / max));
		    if (d >= 1) data = data.concat(",");
	    }
	    %>
	    
<tr>
	<td></td>
	<td colspan="4" style="background-color: white;">
	    <img src="http://chart.apis.google.com/chart?chs=600x200&chxt=x,y&chtt=<%=label[i] %>&chxl=1:|0|<%=max/2 %>|<%=max %>&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<%=data%>"/>
	</td>
	<td align="center"></td>
</tr>
	    
		<%
	}
%>
</table>