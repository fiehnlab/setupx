<%@ include file="checklogin.jsp"%>

<%@ include file="head_blank.jsp"%>

<%@ include file="navi_admin.jsp"%>

<%@ page import="java.util.*,
                 org.setupx.repository.server.persistence.*"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<table align='center' border='0' cellpadding='4' width="60%">
<%
org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

Iterator iter = hqlSession.createSQLQuery("select f.uoid as id, comment.value as comment ,label.value as label, f.PARENT as clazz"
        + " from formobject as f, formobject as label, formobject as comment "
        + " where f.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.Sample\" "
        + " and f.active = true and label.value != \"\" and label.question = \"label\" and comment.value != \"\" " 
        + " and comment.PARENT = f.uoid and label.PARENT = f.uoid and comment.question = \"comment\" "
        + " order by f.uoid desc")
        .addScalar("id", org.hibernate.Hibernate.INTEGER)
		.addScalar("comment", org.hibernate.Hibernate.STRING)
		.addScalar("label", org.hibernate.Hibernate.STRING)
		.addScalar("clazz", org.hibernate.Hibernate.INTEGER)
		.list()
		.iterator();

int i = 0;
while(iter.hasNext()){
Object[] array = (Object[])iter.next();
%>

<% if (i%20 == 0) {%>
	<tr>
		<td></td>
		<th align="center" colspan="2">ID</th>
		<th align="center" colspan="2">comment</th>
		<th align="center">class</th>
	</tr>
<%} 
i++;
%>
	<tr>
		<td></td>
		<td align="center"><b><%=array[0]%></b></td>
		<td align="center" style="background-color:#FFFFFF"><a href="sample_detail.jsp?id=<%=array[0]%>"><img  border='0' src="pics/details.gif"></a></td>
		<td align="center"><%=array[1]%></td>
		<td align="center"><%=array[2]%></td>
		<td align="center"><%=array[3]%></td>
	</tr>
    <%
}
%>

</table>

<%@ include file="footer.jsp"%>