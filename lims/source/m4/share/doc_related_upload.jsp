<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Enumeration"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="java.io.File"%>


<%
	// uploading related documents to one promt
	
	

	// just creating a dummy file for finding tmp folder
	File _tmp = File.createTempFile("xyz", ".tmp", null);
	
	MultipartRequest multiRequest = new MultipartRequest(request, _tmp.getParentFile().getAbsolutePath(), 10424162);

	long promtID = Long.parseLong(multiRequest.getParameter("promt"));

	_tmp.delete();
    Enumeration fileNames = multiRequest.getFileNames();

	while (fileNames.hasMoreElements()) {
	      String filename = (String) fileNames.nextElement();
		  File file = multiRequest.getFile(filename);
		 
		  Logger.log(this, "uploaded file: " + file.getName() + "   " + file.getAbsolutePath());

		  // folder and filename where the files are stored
	      String localFilename = Config.DIRECTORY_RELATED + File.separator + promtID + File.separator + file.getName();
	      File localfile = new File(localFilename);

	      // copying to new location
	      localfile.getParentFile().mkdirs();
	      org.setupx.repository.core.util.File.copyFile(file, localfile);
		  Logger.log(this, "and a copied to " + localfile);
	}
%>

<meta http-equiv="refresh" content="0; URL=doc_related_list.jsp?promt=<%=promtID%>">
