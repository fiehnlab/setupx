<%@ include file="checknonlogin.jsp"%>

<%@page import="java.util.Date"%>
<%@page import="org.setupx.repository.core.communication.document.DocumentObject"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.document.sop.StandardOperationProcedure"%>
<%@page import="java.io.File"%>


<meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>

<%
	StandardOperationProcedure sop = null;
	Session s = StandardOperationProcedure.createSession();
	String readonly = "";
	
	// detail page for an sop
	long sopID = Long.parseLong(request.getParameter("id"));
									 
	sop = StandardOperationProcedure.load(s, sopID);
	s.close();
	
	Logger.debug(this, "loaded document: " + request.getParameter("id") + "   " +  sop);
	
	DateFormat dFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US);	
	// dFormat.format(date));
	
	if (user.isPublic() && !sop.isPubliclyAvailable()){%>
		<jsp:forward page="login.jsp"/>
	<%
	    
	}
%>

	<jsp:include page="sop_header.jsp"></jsp:include>

 	<h2>
		<a href="sop_index.jsp"><img src="pics/sop.gif" border="0"/> Standard Operation Procedures</a> - <%=sop.getLabel()  %> (<%=sop.getVersion() %>) - 
		<% try{ %>
			[category: <%=sop.getCategory().getLabel() %>]
		<% } catch (Exception e){} %>
	</h2>


<table width="800" align="center" cellpadding="2" cellspacing="2">

	<% if (!sop.isActive()){
	    if (sop.getDateValidEnd().before(new Date())){%>
		<tr>
			<th colspan="3" style="background-color: red; color:black;">
				Document is not valid anymore.  
				<% if (sop.isReplacedBy() != null){ %>
				    It expired on <%=dFormat.format(sop.getDateValidEnd()) %> and was replaced by Document <a href="sop_detail.jsp?id=<%=sop.isReplacedBy().getUOID()%>"><%=sop.isReplacedBy().getLabel() %></a>.
				<%} %>				    
			</th>
		</tr>
		    <%} else if (sop.getDateValidStart().after(new Date())){ %>
		<tr>
			<th colspan="3" style="background-color: red; color:black;">
				Document is not active yet. It will be in use beginning <%=dFormat.format(sop.getDateValidStart()) %>.
				<%
				if (sop.getReplcaesDocument() != null){%>
					Document <a href="sop_detail.jsp?id=<%=sop.getReplcaesDocument().getUOID()%>"><%=sop.getReplcaesDocument().getLabel() %></a> will be active until then. 
				<%} %>
			</th>
		</tr>
	<%  } 
	}  else {
		%>
			<tr>
			<th colspan="3" style="background-color: green; color:black;">
				Document is valid.
				Valid until: <%=dFormat.format(sop.getDateValidEnd()) %>
				<%
				if (sop.isReplacedBy() != null){%>
					Document <a href="sop_detail.jsp?id=<%=sop.isReplacedBy().getUOID()%>"><%=sop.isReplacedBy().getLabel() %></a> will be active by then. 
				<%} %>
			</th>
		</tr>
	<%
	}
	%>

	<tr>
		<th><%=sop.getAreaValid() %></th>
		<th>SOP Standard Operating Procedure</th>
		<td>page 1</td>
	</tr>
	<tr>
		<th>date: <%=dFormat.format(new Date()) %></th>
		<th><%=sop.getLabel() %></th>
		<th>Code no.: <%=sop.getVersion() %> (<%=sop.getUOID() %>)</th>
	</tr>
	<tr>
		<td>
			<table>
			  <tr>
			    <th>Issued</th>
			    <td><%=dFormat.format(sop.getDateIssued()) %></td>
			  </tr>
			  <tr>
			    <th>valid beginning:</th>
			    <td><%=dFormat.format(sop.getDateValidStart()) %></td>
			  </tr>
			  <tr>
			    <th>valid until:</th>
			    <td><%=dFormat.format(sop.getDateValidEnd()) %></td>
			  </tr>

			</table>
		
		
		</td>
		<td colspan="2">Validity area: <%=sop.getAreaValid() %></td>
	</tr>
	<tr>
		<td>Responsible: <nobr><a href="mailto:<%=sop.getResponsible().getEmailString()%>"><%=sop.getResponsible().getDisplayName()%></a></nobr></td>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			&nbsp;
			<%=sop.getComment() %>
			<font style="font-size:  xx-small;">
			<%
			if (sop.getReplcaesDocument() != null){
				%>
				<br>
				this SOP replaces the document: <br>
			<%} %>
			<%
			if (sop.isReplacedBy() != null){
				%>
				<br>
				this SOP is replaced by document: <br>
				<a href="sop_detail.jsp?id=<%=sop.isReplacedBy().getUOID() %>"><%=sop.isReplacedBy().getLabel() %></a> (<%=sop.isReplacedBy().getVersion() %> - <%=dFormat.format(sop.isReplacedBy().getDateIssued())%>)
			<%} %>
			</font>
		<td>created: <%=dFormat.format(sop.getDateIssued())%> <%=DateFormat.getTimeInstance(DateFormat.SHORT, Locale.US).format(sop.getDateIssued()) %>, <a href="mailto:<%=sop.getDefinedBy().getEmailString()%>"><%=sop.getDefinedBy().getDisplayName()%></a></td>
	</tr>
	<tr>
		<td colspan="3">
			<table>
				<tr>
					<th colspan="3">Description</th>
				<tr>
					<td colspan="3"><%=sop.getContent().replaceAll("\n", "<br>")%></td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>	
		<td colspan="3">
			<%
				if (sop.getFilePath().length() > 0){
					File relatedFile = new File(sop.getFilePath());
					%>
					<table>
						<tr>
							<th colspan="3">Related Files</th>
						<tr>
							<td rowspan="2">
								<a href="sop_download.jsp?sopID=<%=sopID %>"><img src="pics/file.jpg" border="0"/></a>
							<td>
								<nobr><%=relatedFile.getName()%></nobr>
							<td width="80%">
								&nbsp;
						<tr>
							<td>
								<nobr><font style="font-size: xx-small;"><%=dFormat.format(new Date(relatedFile.lastModified())) %></font></nobr>
							</td>
						</tr>
					</table>
					<%
				} else {
					%>
					<table>
						<tr>
							<th colspan="3">Related Files</th>
						<tr>
							<td colspan="3">
								There is no file to this SOP - all the information is show.
							</td>
						</tr>
					</table>
					<%
				}
			%>
	</tr>
	<%
		boolean granted = false;
		if (sop.getResponsible().getUOID() == user.getUOID() || sop.getDefinedBy().getUOID() == user.getUOID()){
			granted = true;		    
		}
	%>
	<tr>
		<td colspan="3">
			<table>
				<tr>
					<th colspan="3">Settings</th>
				<tr>
					<td>
						First day of valid: 
					</td>
					<td colspan="2">
						<input type="text" size="2" value="<%=sop.getDateValidStart().getMonth() + 1%>" readonly="readonly"></input> /
						<input type="text" size="2" value="<%=sop.getDateValidStart().getDate() %>" readonly="readonly"></input> /
						<input type="text" size="2" value="<%=sop.getDateValidStart().getYear() + 1900 %>" readonly="readonly"></input> 
						<% if(granted){ %>
						<% if (sop.isReplacedBy() == null){ %>
							&nbsp;&nbsp;&nbsp;<a href="sop_mod.jsp?a=2&id=<%=sop.getUOID() %>">activate now! </a> 
							&nbsp;&nbsp;<a href="sop_mod.jsp?a=3&id=<%=sop.getUOID() %>">retire now</a> 
						<% }  
						}%>
						<br>
						<font style="font-size: xx-small;">MM/DD/YYYY</font>						
					</td>
				</tr>
				<tr>
					<td>
						Last day of valid: 
					</td>
					<td colspan="2">
						<input type="text" size="2" value="<%=sop.getDateValidEnd().getMonth() + 1%>" readonly="readonly"></input> /
						<input type="text" size="2" value="<%=sop.getDateValidEnd().getDate() %>" readonly="readonly"></input> /
						<input type="text" size="2" value="<%=sop.getDateValidEnd().getYear() + 1900 %>" readonly="readonly"></input> 
						<br>
						<font style="font-size: xx-small;">MM/DD/YYYY</font>						
					</td>
				</tr>
				<tr>
					<td >
						active
					</td>
					<td >
						<% if(sop.isActive()){ %>
							<img border="0" src="pics/checked.jpg"/>
						<% } else {%>
							<img border="0" src="pics/blank.jpg"/>
						<% } %>
					</td>
					<td>
						<font style="font-size: xx-small;">(activation of an SOP can be changed by modifying the Start date of the valid timeperiod.)
				</tr>
				<tr>
					<td >
						available to the public
					</td>
					<td >
						<% if(sop.isPubliclyAvailable()){ %>
							<a href="sop_mod.jsp?a=1&v=0&id=<%=sop.getUOID() %>"><img border="0" src="pics/checked.jpg"/></a>
						<% } else {%>
							<a href="sop_mod.jsp?a=1&v=1&id=<%=sop.getUOID() %>"><img border="0" src="pics/blank.jpg"/></a>
						<% } %>
					</td>
				</tr>
				<% if(granted){ %>
				<% if(sop.isReplacedBy() == null && sop.getResponsible().getUOID() == user.getUOID()){ %>
				<tr>
					<td>
						Replace SOP
					<td >				
						<a href="sop_create.jsp?replaceid=<%=sop.getUOID() %>"><img border="0" src="pics/go.gif"></a> 
					</td>
					<td >
						<font style="font-size: xx-small;">(replacing this SOP with a newer version retires this SOP and references to the newer one.)
				</tr>
				<%} } %>
			</table>
		</td>
	</tr>
</table>







