<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.core.util.web.UiHttpServlet"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>
<%@page import="java.util.HashSet"%>

<%

	// the list of sampels that were found
	HashSet samples = new HashSet();

	// get experiment ID from request
	long pormtID = UiHttpServlet.getRequestParameterLong(request, WebConstants.QUERYSERVLET_EXPERIMENT_ID);
    
	// get term from request
	String term = UiHttpServlet.getRequestParameter(request, "term");
	
	// query for samples where root is expID and comment like (%term%)
	

	
	String query = "select f.* from formobject as f join samples as s on f.uoid = s.sampleID where s.label like  \"%" + term + "%\" and s.experiment = \"" + pormtID + "\"" ;
	samples.addAll(CoreObject.createSession().createSQLQuery(query).addEntity(Sample.class).list());
	
	Logger.debug(this, "query: " + query + " found: " + samples.size());
	
	// add list of samples to session 
	request.getSession().putValue(WebConstants.SESS_SAMPLES, samples);
	
	// forward to samples list
%>

<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<jsp:forward page="/aq/sample_list.jsp"/>