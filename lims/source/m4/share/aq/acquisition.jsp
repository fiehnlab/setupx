<%@ page contentType="text/html" %>
<%@ page import="org.setupx.repository.web.WebConstants"%>
<%@ page import="org.setupx.repository.web.QueryServlet"%>
<%@ page import="org.setupx.repository.web.forms.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.multi.*"%>
<%@ page import="org.setupx.repository.web.forms.clazzes.*"%>
<%@ page import="org.setupx.repository.web.forms.page.*"%>
<%@ page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@ page import="org.setupx.repository.core.communication.exporting.*"%>
<%@ page import="org.setupx.repository.core.communication.exporting.sampletypes.*"%>

	<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">	
			<img src='../pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Acquistion</h2><br>Acquistion Task</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='../pics/metabolomics1-1.jpg' border="0" valign="middle" align="center">
			</a>
		</td>
		
			<tr>
	</tr>
	<tr>
		<td align="center" ><a href="../admin.jsp"><img src='../pics/back.gif' border="0" valign="middle" align="center"></a></td>
		<td align="center" ></td>
		<td align="center" ><a href="../form?logout"><img src='../pics/exit.jpg' border="0" valign="middle" align="center"></a></td>
	</tr>
	</table>


<link href="../stylesheet.css" rel="stylesheet" type="text/css"/>



<%	
	Acquisition acquisition = (Acquisition)session.getAttribute(org.setupx.repository.web.WebConstants.SESS_ACQUISTION); 		
	try {
%>
<table>
<tr>
<td align="center">
</td>
</tr>


<%	




		acquisition.createAcquisitionFile().store();
    } catch (Exception e) {
        e.printStackTrace();
        e.getMessage();
    }
%>

	

	<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
			<th>
    		<th colspan="2">estimated start for sample<br><font class="small">date below the time shows estimated date <br>in case sequence is started in 30min.</font>
			<th>filename<br><font class="small">filename for the resultfile</font>
			<th><%= AcquisitionParameter.GC_METHOD %>
			<th>type<br><font class="small">type or sample</font> 
			<th>postion<br><font class="small">on<br> the tray</font> 
			<th colspan="2">label
			<th>class
			<th>
	</tr>


	<%
	// number of seconds needed for a single sample
	final int averageTimeSec = 1463;

	final java.text.DateFormat df1 = new java.text.SimpleDateFormat("HH:mm:ss '(day' d')'");
	final java.text.DateFormat df2 = new java.text.SimpleDateFormat("EEE, MMM d h:mm a");
	
	java.util.GregorianCalendar calendar = new java.util.GregorianCalendar();
	java.util.GregorianCalendar counter = new java.util.GregorianCalendar(0,0,1,0,0,0);
	calendar.add(java.util.Calendar.MINUTE, 30);
	for (int i = 0; i < acquisition.samples.length; i++){
	 %>
    	<tr>
    		<td align="center"></td>
    		<td align="center"><font class="small"><%=(i+1) %> / <%=acquisition.samples.length %></font></td>
    		<td align="center">
    			<b><font class="small"><%=df1.format(counter.getTime())%></font></b><br>
    			<font class="small"><%=df2.format(calendar.getTime())%></font>
    			<%calendar.add(java.util.GregorianCalendar.SECOND, averageTimeSec);%>
    			<%counter.add(java.util.GregorianCalendar.SECOND, averageTimeSec);%>
    		</td>
			<td class="<%=acquisition.samples[i].typeShortcut %>" align="center"><%=acquisition.samples[i].getParamter(AcquisitionParameter.NAME).getValue() %>
			<% String gc = acquisition.samples[i].getParamter(AcquisitionParameter.GC_METHOD).getValue(); %>
			<td align="center"><%=gc.substring((gc.lastIndexOf('\\')+1),gc.length())%>
			<td class="<%=acquisition.samples[i].typeShortcut %>" align="center"><%=acquisition.samples[i].getParamter(AcquisitionParameter.TRAY).getValue() %>
			<td class="<%=acquisition.samples[i].typeShortcut %>" align="center"><b><%=acquisition.samples[i].getParamter(AcquisitionParameter.VIAL).getValue() %></b>
			<!--  class -->
			<%try{
			    int sampleID = Integer.parseInt(acquisition.samples[i].getParamter(AcquisitionParameter.TRAY).getValue()); 
			    int classID = new SXQuery().findClazzIDBySampleID(sampleID);
			    //String label = new SXQuery().findSampleLabelBySampleID(sampleID);
				org.hibernate.Session s = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

				//java.util.Iterator iterator = s.createCriteria(org.setupx.repository.core.communication.leco.Method.class).addOrder(org.hibernate.criterion.Order.asc("label")).list().iterator();	    

				String query = 
				    "select label.value as label, comment.value as comment " + 
				    "from formobject as label, " +
				    "formobject as comment " +
				    "where label.parent = comment.parent " +
				    "and label.question  = \"label\" " +
				    "and comment.question = \"comment\" " +  
				    "and label.parent = " + sampleID	;
				Object[] objects = (Object[] )s.createSQLQuery(query).addScalar("label", org.hibernate.Hibernate.STRING).addScalar("comment",  org.hibernate.Hibernate.STRING).list().get(0);
				String label = (String)objects[0];
				String comment = (String)objects[1];
			%>
			<!--  <td align="center" style="background-color:#FFFFFF"><a href="../sample_detail.jsp?id=<%=sampleID%>"><img  border='0' src="../pics/details.gif"></a></td> -->
			<td align="center" class="<%=acquisition.samples[i].typeShortcut %>" ><%=label %><b>
			<td align="center" class="<%=acquisition.samples[i].typeShortcut %>" ><%=comment %><a href="../sample_detail.jsp?id=<%=sampleID%>">[Details]</a><b>
			<td align="center" class="<%=acquisition.samples[i].typeShortcut %>" ><%=classID %>
			<%
			} catch (Exception e){
			%>
				<td align="center">
				<td align="center">
				<td align="center">
			<%    
			}
			%>
			<td align="center">
	</tr>
	<%} %>
    	<tr>
    		<td align="center"></td>
    		<td align="center" colspan="9" height="20">
			<td align="center">
		</tr>

    	<tr>
    		<td align="center"></td>
    		<td align="center" colspan="4">Estimated duration for complete run: <b><%=df1.format(counter.getTime())%></font><br>
			<td align="center" colspan="5">Estimated completion <b><%=df2.format(calendar.getTime())%></b>
			<td align="center">
		</tr>
    	<tr>
    		<td align="center"></td>
    		<td align="center" colspan="4"><font class="small">This is an estimated time. It is based on the average time take by each sample.</font>
			<td align="center" colspan="5"><font class="small">This is an date for the completion. It is based on the average time take by each sample and based on the assumption that the sequence is started in the next 30 minutes.</font>
			<td align="center">
		</tr>
    	<tr>
    		<th align="center"></td>
    		<th align="center" colspan="9">
			<th align="center">
		</tr>

	</table>
	
	
	<table align='center' border='0' cellpadding='4' width="700">
		<!--  -->
		<% 
		Session s = CoreObject.createSession();
		for (int i = 1; i < 15 /*acquisition.samples.length*/; i++){
		    %>
		    <tr>
		    <%
		    // zeile incl 1,15, ...
		    for (int pos = i; pos < 99/*acquisition.samples.length*/; pos = pos+14){
		        AcquisitionSample acquisitionSample = null;
		        try {
		            boolean loop = true;
	      	        int c = 0;
		      	    while (loop && c < 1000){
		      	        acquisitionSample = acquisition.samples[c];
		      	        if (acquisitionSample.getParamter(AcquisitionParameter.VIAL).getValue().compareTo(pos + "") == 0){
		      	            loop = false;
		      	        }
		      	        c++;
		      	    }
		      	
		      	String comment = "";    
		      	String label = "";

				try {
				    int sampleID = Integer.parseInt(acquisitionSample.getParamter(AcquisitionParameter.TRAY).getValue()); 
				    
			        if (sampleID != 0){
				      	String query = 
						    "select label.value as label, comment.value as comment " + 
						    "from formobject as label, " +
						    "formobject as comment " +
						    "where label.parent = comment.parent " +
						    "and label.question  = \"label\" " +
						    "and comment.question = \"comment\" " +  
						    "and label.parent = " + sampleID	;
				      	Logger.debug(this, "checking for label for " + sampleID + "   : "  + query ); 
						Object[] objects = (Object[] )s.createSQLQuery(query).addScalar("label", org.hibernate.Hibernate.STRING).addScalar("comment",  org.hibernate.Hibernate.STRING).list().get(0);
						label = (String)objects[0];
						comment = (String)objects[1];
						if (comment.compareTo("-your comment-") == 0 ) comment = "";
			        }
			        
				} catch (Exception E){
				    
				}
		      	%>
		      	
				<td width="14.3%" align="center" class="<%=acquisitionSample.typeShortcut %>" >
					<!-- <a href="../sample_detail.jsp?id=<%=acquisitionSample.uoid%>"><%=pos %><%=acquisitionSample.getUOID()%></a> -->
					<%=pos %> <font class="small">[<%=comment %>]</font>
					<br>
					<font class="small"><%=acquisitionSample.getParamter(AcquisitionParameter.NAME).getValue() %></font>		      	            
					<br>
					<font class="small"><%=acquisitionSample.getParamter(AcquisitionParameter.TRAY).getValue() %></font>		      	            
				</td>
		      	<% 
		      	} catch (Exception e){
			      	%>
				<td width="14.3%" align="center" >
					<%=pos %>
				</td>
		      	<% 
		      	}
		      	%>
		      	
				<%
		    }
		}
		%>
	</table>
	




<%@ include file="../footer.jsp"%>
