<%@page contentType="text/html" %>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.QueryServlet"%>
<%@ page import="org.setupx.repository.web.forms.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.multi.*"%>
<%@ page import="org.setupx.repository.web.forms.clazzes.*"%>
<%@ page import="org.setupx.repository.web.forms.page.*"%>

<%@ include file="header.jsp"%>

<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<table align='center' border='0' cellpadding='4' width='800'>
<tr>
<th><h1>Acquistion</h1>selecting samples for the Acquistion Tasks</th>
</tr>

<tr><td>
<table border="0">

<tr>
<td>
<!-- left side -->
<td valign="top">
<table align="center" border=0>

	<tr>
		<th width="48%"><h3>Search Result</h3></th>
		<th></th>
		<th width="48%"><h3>Samples selected for Acquisition</h3></th>
	<tr>
		<td valign="top">These following samples match your description. All samples that have not been run yet are selected by <b>default</b>. You can still rerun the samples that ran in a previous sequence. Just select the samples that you want to run and move them to the right.
		<td>
		<td valign="top">These following samples are selected for acquistion.</td>

<tr>
	<td valign="top">
<table border ="0" width="100%">
<tr>
<th></th>
<th>Sample ID</th>
<th>Label</th>
<th>Class ID</th>
<th>last Run</th>
</tr>

<% 
boolean first = true;

// samples that were found by the queryservlet
java.util.HashSet samplesFound = (java.util.HashSet)session.getAttribute(WebConstants.SESS_SAMPLES); 		


// samples that were already selected for acquisition
java.util.HashSet samplesSelected = (java.util.HashSet)session.getAttribute(WebConstants.SESS_SAMPLES_SELECTED);
Sample sample = null;

if (samplesSelected == null) samplesSelected = new java.util.HashSet();
	if (samplesFound != null && samplesFound.size() != 0) {
        java.util.Iterator iterator = samplesFound.iterator();
        while (iterator.hasNext()) {
			sample = (Sample) iterator.next();  
			// just in case it has not been selected already
			if (SampleComparator.contains(samplesSelected,sample)){
 			
			} 
			else {
			    
		        if (sample != null && first) { 
		            first = false;
		%>
		        
		    	<form action="query_commentterm.jsp" method="POST">
		    	<tr>
		    		<td>
		    		<td>
		    			<input type="hidden" name="<%=WebConstants.QUERYSERVLET_EXPERIMENT_ID%>" value="<%=sample.getPromtID() %>"/><p>
		    		<td>
		    			<input type="text" name="term"/><p>
		    		<td style="background-color: white"><font class="small">filter by label</font>
		    		<td><input type="image" src='../pics/aq_find.gif' alt="Query"></td>
		    	</tr>
		    	</form>

		<%        
		        }

			    
			    
// acq information
String acqName = "";
String checked = "checked";
try {
    acqName = new SXQuery().findAcquisitionNameBySampleID(sample.getUOID());
    checked = "";
}catch (Exception e){
    
}

// 
%>
<form action="select" method="POST">

<tr>
	<td><input type="checkbox" name="sample<%=sample.getUOID()%>" value="<%=sample.getUOID()%>" <%=checked %> /></td>
	<td>ID:<%=sample.getUOID()%></td>
	<td><%=sample.getComment()%></td>
	<td><%=sample.determineClazzID()%></td>
	<td><%=acqName %></td> 
</tr>
<% 			
        
			}
		}
	} else {
%>
<tr>
<td>--</td>
<td>--no samples found --</td>
<td><img src='../pics/blank.jpg' border="0" valign="middle" align="center"></td>
<td><img src='../pics/blank.jpg' border="0" valign="middle" align="center"></td>
</tr>
<%
}
%>



<!-- middle spalte -->
</th></tr>
</table>
</td>
<td valign="top" style="background-color: white">
<table align="center" background="white">
<tr><td><input type="image" src='../pics/RightArrowBlue.jpg' alt="Add"></td></tr>
</table>
</td>

</form>
<td valign="top">
<table border ="0" width="100%">
<!-- left side -->
<tr><td valign="top">
<table border ="0">
<tr>
<th>ID</th>
<th></th>
<th></th>
<th></th>
</tr>

<%

	if (samplesSelected != null && (samplesSelected.size() != 0) ) {
        java.util.Iterator iterator = samplesSelected.iterator();
        while (iterator.hasNext()) {
            sample = (Sample) iterator.next(); %>
<tr>
	<td>ID:<%=sample.getUOID()%></td>
	<td><%=sample.getComment()%></td>
	<td><%=sample.determineClazzID()%></td>
</tr>
</tr>

<% 			
        }
}else {
%>
<tr>
<td>-- no samples selected yet --</td>
<td>--------</td>
<td><img src='../pics/blank.jpg' border="0" valign="middle" align="center"></td>
<td><img src='../pics/blank.jpg' border="0" valign="middle" align="center"></td>
</tr>
<%
        }

%>
</table>
</table>
</td></tr>

<tr>
<td align="center" colspan="3">
<table > 
<th colspan="3">Action</th>
<tr><td><a href="query.jsp"><img src='../pics/aq_find.gif' border="0" valign="middle" align="center"></a></td><td>Search more samples</td><td></td></tr>
<tr><td><a href="clean.jsp"><img src='../pics/editer.gif' border="0" valign="middle" align="center"></a></td><td>Clean selection</td><td></td></tr>
<tr><td><a href="jobinformation.jsp"><img src='../pics/aq_generate.gif' border="0" valign="middle" align="center"></a></td><td>Create Acquistion Task </td><td></td></tr>
<tr><td><a href="help.jsp"><img src='../pics/aq_help.gif' border="0" valign="middle" align="center"></a></td><td>Help</td><td></td></tr>
<tr><td><a href=".."><img src='../pics/aq_exit.gif' border="0" valign="middle" align="center"></a></td><td><b>Exit</b></td><td></td></tr>
</table></td>
</tr>
</table>
</table>

<%@ include file="../footer.jsp"%>

