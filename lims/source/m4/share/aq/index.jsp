<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>
<%@ page import="org.setupx.repository.web.forms.inputfield.multi.*"%>
<%@ page import="org.setupx.repository.web.forms.page.*"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.QueryServlet"%>

	<link href="../stylesheet.css" rel="stylesheet" type="text/css"/	>
<%@ include file="header.jsp"%>

<table width="800" cellpadding='4' align='center'>
	<th colspan="3">Edit<br>
			<font class="small">search and edit an experiment</font>
	</th>
	<tr>
		<td class="dark">
			<a href="report.jsp"><img align='center' border="0" border="0" src="../pics/view.gif"/></a>
		</td>
		<td>
			Experiment-Report
			<br>
			<font class="small">get Report about a whole experiment</font>
		</td>
		<td>
		</td>
	</tr>
	<th colspan="3">Machine Communication</th>
	<tr>
		<td class="dark">
			<a href="query.jsp"><img align='center' border="0" border="0" src="../pics/add.gif"/></a>
		</td>
		<td>
			Acquistion for Leco Machines
			<br>
			<font class="small">select samples and generate Acquistion Tasks for the Machine</font>
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td class="dark">
			<a><img align='center' border="0" src="../pics/view.gif"/></a>
		</td>
		<td>
			See Log File
			<br>
			<font class="small">view the logfiles and related samples from each of the machines</font>
		</td>
		<td>
		</td>
	</tr>
	<th colspan="3">User Administration			<br>
			<font class="small">add and remove users</font>
	</th>
	<tr>
		<td class="dark">
			<a><img align='center' border="0" src="../pics/view.gif"/></a>
		</td>
		<td>
			Search Users
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td class="dark">
			<a><img align='center' border="0" src="../pics/delete.gif"/></a>
		</td>
		<td>
			Remove Users
		</td>
		<td>
		</td>
	</tr>
	<th colspan="3">Advanced Settings			<br>
			<font class="small">..	</font>
	</th>
	<tr>
		<td class="dark">
			<a><img align='center' border="0" src="../pics/settings.gif"/></a>
		</td>
		<td>
			Settings
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td class="dark">
			<a href="services"><img align='center' border="0" src="../pics/view.gif"/></a>
		</td>
		<td>
			Status of WebService Connector
		</td>
		<td>
		</td>
	</tr>
</table>
<%@ include file="../footer.jsp"%>