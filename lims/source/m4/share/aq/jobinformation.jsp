	<%@page contentType="text/html" %>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.QueryServlet"%>
<%@page import="org.setupx.repository.core.communication.exporting.*"%>

<link href="../stylesheet.css" rel="stylesheet" type="text/css"/>


<body>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">	
			<img src='../pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Acquistion</h2><br>Additional Information for the Acquistion Task, ...</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='../pics/metabolomics1-1.jpg' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>




<form action="acquisition" method="POST">
<table align='center' border='0' cellpadding='4' width="60%">

<tr>
	<td/>
	<td colspan="8">There are a few addional information that are nessessacrry for the generation of the acquisition task.</td>
	<td/>
</tr>

<tr>
	<th/>
	<th colspan="8">Machine<br><font class="small">Select machine to run the samples on.</font></th>
	<th/>
</tr>
<tr>
	<th align="center"></td>
	<th align="center"></td>
	<th align="center"></td>
	<th align="center" colspan="2"><b>label</b></td>
	<th align="center" colspan="3" ></td>
	<th align="center"><b>mehtods</td>
	<th align="center"></td>
</tr>

<%
java.util.Iterator machines = MachinePool.getAvailableMachines().iterator();

while(machines.hasNext()){
    org.setupx.repository.core.communication.leco.GCTOF machine = (org.setupx.repository.core.communication.leco.GCTOF)machines.next();
    %>
<tr>
	<td align="center"></td>
	<td align="center"></td>

	<td align="center"  style="background-color:#FFFFFF">
	<a href="?sm=<%=machine.getUOID()%>">
	
		<%
		long selectedMachine = 0;
		try {
		    selectedMachine = Long.parseLong(request.getParameter("sm"));
		} catch (Exception e){
		}
		if (selectedMachine == machine.getUOID()){ %>
			<img src='../pics/checked.jpg' border="0" valign="middle" align="center">
	    <%} else {%>
	    			<img src='../pics/blank.jpg' border="0" valign="middle" align="center">
	    <%} %>
	    </a>
	</td>
	<td align="center" colspan="2"><b><%=machine.getLabel() %></b></td>
	<td align="center" colspan="3" ><%=machine.getComment() %></td>
	<td align="center"><%=machine.getMethods().size() %></td>
	<td align="center"></td>
</tr>
<% if (selectedMachine == machine.getUOID()){ %>
	<input type="hidden" value="<%=machine.getUOID()%>" name="<%=WebConstants.PARAM_MACHINE_ID %>">

    <%
    String[] methodTypes = new String[]{"(GC)","(MS)","(DP)"};    
    for (int i = 0; i < methodTypes.length ; i++ ){
	%>
	<tr>
    	<td align="center"></td>
    	<td align="center"></td>
    	<td align="center"></td>
    	<td align="center" colspan="2"><%=methodTypes[i] %></b></td>
    	<td align="center" colspan="4"   style="background-color:#FFFFFF">
			<% java.util.Set set = machine.getMethods(methodTypes[i]);

			java.util.Iterator meth = set.iterator();
			if (set.size() > 0){
			%>
			<select name="<%=methodTypes[i] %>">
			<option value="-undefined-">-- undefined - please select the method from this list [-] --</OPTION>
    		<%
    		while (meth.hasNext()){
    		    org.setupx.repository.core.communication.leco.Method method = (org.setupx.repository.core.communication.leco.Method)meth.next();
    		    %>
					<OPTION VALUE="<%=method.getValue() %>"><%=method.getLabel() %>  [<%=method.getUOID() %>]</OPTION>
    		    <% 
    		}
    		%>
			</SELECT>
			<% 
			} else {  
			%>
				<input size="55" name="<%=methodTypes[i] %>" type="text" value="there are no predefined methods availavble. define it now.">
			<%}%>
			
		</td>
    	<td align="center"></td>
    </tr>
   <% }
}

} //selectedMachine == machine.getUOID()
%>





<tr>
	<th/>
	<th colspan="8">Positioning</th>
	<th/>
</tr>


<tr>
	<td align="center"></td>
	<td align="center">What should the position of the first vail on the tray be?</td>
	<td align="center"  style="background-color:#FFFFFF"><input type="text" name="<%=WebConstants.PARAM_VAILOFFSET%>" value="10" size="1" maxlength="2"/></td>
	<td align="center" colspan="2"><b>Offset	</b></td>
	<td align="center" colspan="3"><font class="small">First position of a sample on the tray.</font></td>
	<td align="center"></td>
	<td align="center"></td>
</tr>

<tr>
	<td align="center"></td>
	<td align="center"></td>
	<td align="center" colspan="6"><small>Default is always <b>10</b>. Modify only in case you have existing vails on the tray</small>.</td>
	<td align="center"></td>
	<td align="center"></td>
</tr>

<tr>
	<th/>
	<th colspan="8">Blanks / ALEX</th>
	<th/>
</tr>

<tr>
	<td align="center"></td>
	<td align="center">Alex</td>
	<td align="center"   style="background-color:#FFFFFF"><input type="checkbox" name="<%=WebConstants.PARAM_ALEX%>" value="true" disabled="disabled"/></td>
	<td align="center" colspan="2"><b>use ALEX</b></td>
	<td align="center" colspan="3" ><font class="small"></font></td>
	<td align="center"></td>
	<td align="center"></td>
</tr>

<tr>
	<td align="center"></td>
	<td align="center">Blanks</td>
	<td align="center" style="background-color:#FFFFFF"><input type="checkbox" name="<%=WebConstants.PARAM_BLANKS%>" value="true" checked/></td>
	<td align="center" colspan="2"><b>use Blanks</b></td>
	<td align="center" colspan="3" ><font class="small">Do you want to run a blank in before each sample?</font></td>
	<td align="center"></td>
	<td align="center"></td>
</tr>

<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<td align="center"></td>
<td colspan="8" align="center">

<table>
<th colspan="3">Action</th>
<tr><td align="center"><input type="image" src='../pics/aq_generate.gif' alt="Add"></td><td align="center">Create Acquistion Task </td><td align="center"></td></tr>
<tr><td align="center"><a href="help.jsp"><img src='../pics/aq_help.gif' border="0" valign="middle" align="center"></a></td><td align="center">Help</td><td align="center"></td></tr>
<tr><td align="center"><a href="index.jsp"><img src='../pics/aq_exit.gif' border="0" valign="middle" align="center"></a></td><td align="center"><b>Exit</b></td><td align="center"></td></tr>
</table></td>
<td align="center"></td>
</tr>

</table>
</td>
</tr>
</table>

</form>

