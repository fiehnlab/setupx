<%@page import="org.setupx.repository.core.communication.technology.scanner.ScanningProcessMonitor"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.communication.technology.scanner.ScanningProcess"%>
<%


	//scanner_mod.jsp

	ScanningProcess scanningProcess = null;

	// find the scanner
	String scannerID = request.getParameter("tid");
	scanningProcess = (ScanningProcess)ScanningProcessMonitor.getSessions().get(scannerID);
	
	// on OR off
	boolean on = (request.getParameter("on").compareTo("true") == 0);
	
	Logger.debug(this, "switching scanner " +  scanningProcess.getName() + " : " + on);

	// actualy do it
	scanningProcess.setExternalPaused(on);
	
	
	
	// forward to the main view again
%>
<meta http-equiv="refresh" content="0; URL=scanner_monitor.jsp">
