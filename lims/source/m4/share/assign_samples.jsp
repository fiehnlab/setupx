<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.communication.importing.metadata.*,
                 org.setupx.repository.server.persistence.*"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ include file="incl_setaccessright.jsp"%>

	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="./admin.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Util: Assign "virtual" Runs (Beta) 1/3</h2>
			Tool marks samples that were entered in <%=org.setupx.repository.Config.SYSTEM_NAME%> as finished. <br/>
			Therefor all samples in the experiment have to have the actual filename as comment!
			<font size="-2"></font>
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>
	
	
	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} %>
	
	<table align="center" width="60%" >	
	<tr>
		<td colspan="2" align="center" style="background-color:#FFFFFF"><a href="admin.jsp"><img  border='0' src="pics/back.gif"></a></td>	
		<th colspan="20">ExperimentID</th>
	</tr>
	
	
	<tr>
	<form action="assign_samples_agreement.jsp">
		<td colspan="2"></td>
		<td>
			Please enter the experimentID
		</td>
		<td align="center" style="background-color:#FFFFFF">
			<input type="text" name="promtID" id="promtID">
		</td>
		
		<td align="center" style="background-color:#FFFFFF">
			<input type="image" src="pics/go.gif"></a>
		</td>
		<td>
		</td>
		</form>
	</tr>
</table>


<%@ include file="footer.jsp"%>
