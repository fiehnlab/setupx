<%@page import="org.setupx.repository.core.communication.status.StatusTracker"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.communication.status.LabStatus"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.HashSet"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector"%>
<%@page import="org.setupx.repository.core.communication.file.SampleFile"%>
<%@page import="org.setupx.repository.core.communication.binbase.BBConnector"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ include file="checklogin.jsp"%>

<body>
<!--  head containing logo and description -->

	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Lab Progress Status</h2>
					Progress of Samples in the Lab<br/>
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>

	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} %>
	




<table align='center' border='0' cellpadding='4' width="60%">

	<tr>
		<th align="left" colspan="11">Progress<br><font class="small">&nbsp;&nbsp;progress of samples currently in the lab</font> </td>
		<th align="center"><img border='0' src="pics/aq_help.gif"></td>		
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="10" style="background-color:white;" >
				<%@ include file="incl_status_processbar.jsp"%>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	
<%
	boolean showList = false;

	try {
	    showList = Boolean.valueOf(request.getParameter("show")).booleanValue();
	} catch (Exception e){
	    // 
	}


	if (showList){

%>

	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>


	
	<tr>
		<th align="left" colspan="11">Stages<br><font class="small">&nbsp;&nbsp;Samples in the different stages in the workflow of the lab</font> </td>
		<th align="center"><img border='0' src="pics/aq_help.gif"></td>		
	</tr>
	
	
<%String[] icon = new String[]{
        "pics/package.gif",
		"pics/frost.jpg",
        "pics/sample_prep.jpg",	        
        "",
        "pics/run.jpg",
        "pics/chart.png"
    };


		for (int i = 0; i < lists.length; i++){
		    %>		
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td colspan="4" style="background-color:<%=colors[i] %>"><font color="white"><%=labels[i] %><br>&nbsp;&nbsp;<nobr><%=lists[i].size()%> Samples</nobr></font> </td>
			<td colspan="6" style="background-color:white;" >
			<%
				HashSet experiments = filter(lists[i]);
				Iterator iter = experiments.iterator();
				while (iter.hasNext()){
				    long expID = Long.parseLong("" + iter.next());
				    %>
						<a href="load?id=<%=expID%>&action=20">
							<%=new SXQuery().findPromtTitleByPromtID(expID)%> <font class="small">[<%=expID %>]</font>
						</a>
						<%
							if (BBConnector.determineBBResultsArrived(expID)){
							    %>
							    <a href="sample_status_result_received.jsp?promtID=<%=expID %>">mark as finished</a>
							    <%
							}						
						%>
						<table><tr><td><font class="small"><%=new SXQuery().findPromtCollaborationHTML(expID)%></font></table>
						<% if(iter.hasNext()){ %>
							<hr>
						<%} %>
					<%    
				}
				%>
			</td>
			<td style="background-color:#6682B6" width="20"></td>
		</tr>
		    <%
		}
		
%>		
		
		
		<tr>
		<th align="left" colspan="11">Samples<br><font class="small">&nbsp;&nbsp;Samples in the different stages in the workflow of the lab</font> </td>
		<th align="center"><img border='0' src="pics/aq_help.gif"></td>		
	</tr>
	
	<%

		for (int i = 0; i < lists.length; i++){
		    %>		
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td colspan="4" style="background-color:<%=colors[i] %>"><font color="white"><%=labels[i] %><br>&nbsp;&nbsp;<nobr><%=lists[i].size()%> Samples</nobr></font> </td>
			<td colspan="6" style="background-color:white;" >
					<%
						Iterator iterator = lists[i].iterator();
						while(iterator.hasNext()){
						    Object sampleID = iterator.next();
						    try {
							    long _prID = new SXQuery().root(sampleID.toString());
							    %><a href="sample_detail.jsp?id=<%=sampleID %>"> <%=sampleID %> <font class="small"><%=_prID %></font></a><%
						    } catch (Exception e){
						        
						    }
						}
						%>
			</td>
			<td style="background-color:#6682B6" width="20"></td>
		</tr>
		    <%
		}
	} // showit 
	else {
%>

	<tr>
		<th align="left" colspan="11">Stages<br><font class="small">&nbsp;&nbsp;<a href="sample_status_overview.jsp?show=true">click to see details</a> about the different stages</font> </td>
		<th align="center"><img border='0' src="pics/details_small.gif"></td>		
	</tr>
	


<%    
	}
%>

</tr>
</table>




<%@ include file="footer.jsp"%>

		