<%@ include file="checklogin.jsp"%>
<html>
  <head>
    <script src="http://simile.mit.edu/timeline/api/timeline-api.js" type="text/javascript"></script>

    <script>
var tl;
function onLoad() {
  var eventSource = new Timeline.DefaultEventSource();
  var bandInfos = [
    Timeline.createBandInfo({
        eventSource:    eventSource,
        date:           "<%=new Date().toGMTString()%>",
        width:          "70%", 
        intervalUnit:   Timeline.DateTime.DAY, 
        intervalPixels: 100
    }),
    Timeline.createBandInfo({
        showEventText:  false,
        trackHeight:    0.5,
        trackGap:       0.2,
        eventSource:    eventSource,
        date:           "<%=new Date().toGMTString()%>",
        width:          "30%", 
        intervalUnit:   Timeline.DateTime.MONTH, 
        intervalPixels: 200
    })
  ];
  
  
  bandInfos[1].syncWith = 0;
  bandInfos[1].highlight = true;
  
  tl = Timeline.create(document.getElementById("my-timeline"), bandInfos);
  Timeline.loadXML("dev_timeline.jsp", function(xml, url) { eventSource.loadXML(xml, url); });
}
    </script>

  </head>
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>capacity utilisation</h2><br>timeline below shows utilisation of connected instruments per experiments
		</th>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/connections.gif' height="100px" border="0" valign="middle" align="center">
			</a>
		</td>
	</table>

<%@ include file="navi_admin.jsp"%>





<%@page import="java.util.Date"%>
  <body onload="onLoad();" onresize="onResize();">
	<div id="my-timeline" style="height: 500px; border: 1px solid #aaa"></div>
  </body></html>


<%@ include file="footer.jsp"%>

