


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.core.communication.msi.AttributeValuePair"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIQuery"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Hashtable"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="java.util.HashSet"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

<%
long promtID = Long.parseLong("" + session.getAttribute("promtID"));

%>

<%@ include file="pub_nav.jsp"%>


<table width="700" align="center" >

<tr>
<td align="center"><a href="http://fiehnlab.ucdavis.edu"><img src='pics/uc_logo.gif' border="0" valign="middle" align="center"></a></td>
<tr><td colspan="21" align="center"><font face="Verdana"><hr>
<tr align="center">
	<td>
		<strong>
			<font style="font-variant: small-caps;">
				<%=new SXQuery().findPromtTitleByPromtID(promtID) %>
			</font>
		</strong>
		<a href="pubexperiment.jsp?id=<%=promtID%>"><img src="pics/details.gif" border="0"></a>
<tr align="center"><td>
<%
	HashSet users = new SXQuery().findPromtCollaboration(promtID);

	Iterator iterator = users.iterator();
	while(iterator.hasNext()){
	    UserDO userDO = (UserDO)iterator.next();
	    if (userDO.getUsername().compareTo("public") != 0){
	    %>
		    <a href="mailto:<%=userDO.getMailAddress().getEmailAddress()%>">
		    	<img src='pics/mail.gif' border="0" valign="middle" align="center">
		    </a><%=userDO.getDisplayName()%>, <%
	    }
	}
%>

<tr align="center"><td><font size="-1">
<%
	iterator = users.iterator();
	while(iterator.hasNext()){
	    UserDO userDO = (UserDO)iterator.next();
	    %><%=userDO.getOrganisation()%>, <%
	}
%>

<tr align="center"><td><font size="-1">Genome Center

<tr align="center"><td><font size="-1">451 E. Health Sci. Drive

<tr align="center"><td><font size="-1">Davis, California 95616, USA

<tr align="center"><td><font style="font-family: sans-serif; font-size: x-small">Summary created by <%=org.setupx.repository.Config.SYSTEM_NAME%>. This report has been created based on the information store in the system.
</font>
<hr>



<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.List"%>
<%
int SAMPLE = 0;
int CLASS = 1;
int PROMT = 2;
int NON_MSI = 3;

String color = "";

Hashtable[] cache = new Hashtable[]{
				        new  Hashtable(),
				        new  Hashtable(),
				        new  Hashtable(),
				        new  Hashtable()
					};

			// changing values 
			String[] _paramterLabels = new String[]{
			        	"experiment information",
			        	"class information", 
			        	"sample information", 
			        	"non msi information", 
			        	"show compounds",
			        	"labels"
			        };
			String[] _paramters = new String[]{
			        	"showPromtinfos",
			        	"showClassinfos", 
			        	"showSampleinfos", 
			        	"showNonMSIinfos", 
			        	"showData",
			        	"showLabels" 
			        };
			
			boolean[] _active = new boolean[]{
			        false,
			        false,
			        false,
			        false,
			        false,
			        false,
			        false
			};

				
					
			// positions
            long sampleStart = 0;
            try{ 
                sampleStart = Long.parseLong(request.getParameter("s"));
            } catch(Exception e){
                
            }
            
            long sampleEnd = 20;
            try{ 
                sampleEnd = Long.parseLong(request.getParameter("e"));
            } catch(Exception e){
                
            }


			
			%>
			<table align="center">
				<tr>
					<td align="center" colspan="2">
						<font style="font-size: xx-small;">Please select from the following options which information <br>should be displayed and included in your dataset.</font>
				<tr>
					<%
			for (int _i = 0; _i < _paramters.length; _i++){
		        Logger.debug(this, "parsing parameter :" + _i);
			    try {
			        
			        Logger.debug(this, "parsing for :" + _paramters[_i] );
			        Logger.debug(this, "value for :" + _paramters[_i] + ": " + request.getParameter(_paramters[_i]));

					
			        if ("true".compareTo(request.getParameter(_paramters[_i])) == 0){
				        Logger.debug(this, _paramters[_i] + "is true ");
					    session.setAttribute(_paramters[_i],"" +  1);
					} else if ("false".compareTo(request.getParameter(_paramters[_i])) == 0){
					    session.setAttribute(_paramters[_i],"" +  0);
				        Logger.debug(this, _paramters[_i] + "is false");
					}
			    } catch (Exception e){
			        e.printStackTrace();
			    }

			    boolean _a = false;
			    try {
			        if (Integer.parseInt(session.getAttribute(_paramters[_i]) + "") > 0 ){
			            _a=true;
			        }
			    } catch (Exception e){
			        
			    }
			    
			    
			        %>
			        	<tr>
			        		<td>
			        			<%=_paramterLabels[_i]%>: 
			        		<%
			        		if (_a){ %>
				        		<td><a href="?<%=_paramters[_i]%>=false"><img src="pics/checked.jpg" border=0></a>
				        	<%} else { %>
								<td><a href="?<%=_paramters[_i]%>=true"><img src="pics/blank.jpg" border=0></a>
				        	<%} %>
<!-- 			        		<td><%=session.getAttribute(_paramters[_i]) %>
			        		<td><%=_a %> -->
			        	
			         <br><%
			}

			%>
			</table>
			<%

            
            long showPromtinfos = 0;
            long showClassinfos = 0;
            long showNonMSIinfos = 0;
			long _showLabels = 0;
			long showSampleinfos = 0;
			long showData = 0;
			


            try {
                _showLabels = Long.parseLong(""
                        + session.getAttribute("showLabels"));
		            } catch (Exception e) {

            }
            try {
                showPromtinfos = Long.parseLong(""
                        + session.getAttribute("showPromtinfos"));
            } catch (Exception e) {

            }
            try {
                showClassinfos = Long.parseLong(""
                        + session.getAttribute("showClassinfos"));
            } catch (Exception e) {

            }
            try {
                showNonMSIinfos = Long.parseLong(""
                        + session.getAttribute("showNonMSIinfos"));
            } catch (Exception e) {
	
            }
            try {
                showSampleinfos = Long.parseLong(""
                        + session.getAttribute("showSampleinfos"));
            } catch (Exception e) {

            }
            try {
                showData = Long.parseLong(""
                        + session.getAttribute("showData"));
            } catch (Exception e) {

            }
            
            
            
            List msiAttributes = (List) session.getAttribute("msiIDs");
	        Logger.debug(this, "number of msi attributes :" + msiAttributes.size());

            List sampleIDs = new SXQuery().findSampleIDsByPromtID(promtID);
	        Logger.debug(this, "number of sampleIDs :" + sampleIDs.size());
	        
	        
	        



	     // finding all possible values for attributes
	     String queryAttributeNames = "select att.label as l from pubattribute as att, pubsample as samp, pubdata where pubdata.relatedExperimentID = " + promtID + " and att.parent = samp.uoid and samp.parent = pubdata.uoid group by att.label "; 
	     Session hibernateSession =CoreObject.createSession();
	     List attributeNames = hibernateSession.createSQLQuery(queryAttributeNames).addScalar("l", Hibernate.STRING).list();
	

%>

<table align="center">
	<tr>
		<td colspan="4">
			<font style="font-size: xx-small;">Please select the samples that you want to display. </font>
	<tr>
		<td>
			<a href="?s=<%=sampleStart-100 %>&e=<%=sampleEnd -100%>"><img src="pics/move_left_o.gif" border="0"></a></td>
		<td>
			<a href="?s=<%=sampleStart-10 %>&e=<%=sampleEnd -10%>"><img src="pics/move_left.gif"  border="0"></a></td>
		<td>
			<a href="?s=<%=sampleStart+10 %>&e=<%=sampleEnd +10%>"><img src="pics/move_right.gif"  border="0"></a></td>
		<td>
			<a href="?s=<%=sampleStart+100 %>&e=<%=sampleEnd +100%>"><img src="pics/move_right_o.gif"  border="0"></a></td>
			
	</tr>
</table>

<table align="center">	<tr>
	<form action="">
		<td>
			start
		<td>
			<input type="text" size="3" name="s" value="<%=sampleStart %>">
		<td>
			end
		<td>
			<input type="text" size="3" name="e" value="<%=sampleEnd %>">
		<td>
			<input type="image" src="pics/go.gif"> 
	</form>
</table>

<table align="center" border="1" cellpadding="1">




<%
boolean isHeader = true;
Logger.debug(this, "starting queries.");


// loop over the samples
AttributeValuePair[] _attributeValuePairs = null;
for (int sampleCount = (int)sampleStart ; sampleCount < sampleIDs.size() && sampleCount < sampleEnd; sampleCount++){
    
    long sampleID = Long.parseLong(sampleIDs.get(sampleCount).toString());
    long classID = new SXQuery().findClazzIDBySampleID((int)sampleID);
%>
<tr>
    <%if (isHeader){ %>
		<td>
	<%} else { %>
		<td><font style="font-size: xx-small;"><%=sampleCount+1 %>/<%=sampleIDs.size() %></font>
	<%} %>

    <%if (isHeader){ %>
		<td><nobr>Sample ID</nobr>
	<%} else { %>
		<td><%=sampleID %> 
	<%} %>

    <%if (isHeader){ %>
		<td><nobr>ClassID</nobr>
	<%} else { %>
		<td><%=classID %>
	<%} %>

	

<!-- promt information -->
<%Logger.debug(this, "getting promt infos for sampleID: " + sampleID); %>
<%if (showPromtinfos > 0){
    String key = "" + new SXQuery().findPromtIDbySample(sampleID); 
    if (cache[PROMT].containsKey(key)){
        Logger.debug(this, " is cached already.");
        _attributeValuePairs = (AttributeValuePair[])cache[PROMT].get(key);
    } else {
        _attributeValuePairs = MSIQuery.searchAttributesPerExperimentBySampleID(sampleID);
        cache[PROMT].put(key, _attributeValuePairs);
    }
    //color = "style=\"background-color: " + Util.getColor(PROMT) + "\"";
    %>
    
    <%if (isHeader){ %>
		<%@ include file="incl_attributeValuePairsHeader_horizontal.jsp"%>
	<%} else {%>
		<%@ include file="incl_attributeValuePairs_horizontal.jsp"%>
	<%} %>
<%} else {%>
<%} %>

<!-- class information -->
<%Logger.debug(this, "getting class infos for sampleID: " + sampleID); %>
<%if (showClassinfos > 0){
    String key = "" + new SXQuery().findClazzIDBySampleID((int)sampleID); 
    if (cache[CLASS].containsKey(key)){
        Logger.debug(this, " is cached already.");
        _attributeValuePairs = (AttributeValuePair[])cache[CLASS].get(key);
    } else {
        _attributeValuePairs = MSIQuery.searchAttributesPerClassBySampleID(sampleID);
        cache[CLASS].put(key, _attributeValuePairs);
    }
    int step = sampleCount;
    //if (step % 2 == 1) color = "style=\"background-color: WHITE \"";

    %>
    <%if (isHeader){ %>
		<%@ include file="incl_attributeValuePairsHeader_horizontal.jsp"%>
	<%} else {%>
		<%@ include file="incl_attributeValuePairs_horizontal.jsp"%>
	<%} %>

<%}  else {%>
<%} %>

<!-- sample information -->
<%Logger.debug(this, "getting sample infos for sampleID: " + sampleID); %>
<%if (showSampleinfos > 0){
    _attributeValuePairs = MSIQuery.searchAttributesPerSampleBySampleID(sampleID);
    int step = SAMPLE;
    /*if (step % 2 == 1) {
        color = "style=\"background-color: WHITE \"";
    } else {
        color = "style=\"background-color: GREY \"";
    }*/
    %>
        <%if (isHeader){ %>
		<%@ include file="incl_attributeValuePairsHeader_horizontal.jsp"%>
	<%} else {%>
		<%@ include file="incl_attributeValuePairs_horizontal.jsp"%>
	<%} %>

<%} else {%>

<%} %>

<!-- sample information additional -->
<%Logger.debug(this, "getting non msi infos for sampleID: " + sampleID); %>
<%if (false && showNonMSIinfos > 0){
    _attributeValuePairs = MSIQuery.searchAttributesPerSampleBySampleID(sampleID);
    //color = "style=\"background-color: " + Util.getColor(NON_MSI) + "\"";
	%>
	<td <%=color %>>
	<%if (isHeader){ %>
		<%@ include file="incl_attributeValuePairsHeader_horizontal.jsp"%>
	<%} else {%>
		<%@ include file="incl_attributeValuePairs_horizontal.jsp"%>
	<%} %>

<%} else {%>
<%} %>



<%
	if (showData > 0){
	    Logger.debug(this, "getting datapoints for sampleID: " + sampleID); 
		if (isHeader){ 
			// show each attribute
			Iterator attributeNameIterator = attributeNames.iterator();

			while(attributeNameIterator.hasNext()){
			    String attribute = attributeNameIterator.next().toString();
			    %>
			    <td>
			    	<%=attribute %>
				<%
				} // -- attrIterator
  
		} else {
			// show each attribute

			
			// NEW WAY
		    String valueQuery = "select att.value as v, att.label as l from pubattribute as att , pubsample as s where s.setupXsampleID = " + sampleID + " and s.uoid = att.parent";
	    	List values = hibernateSession.createSQLQuery(valueQuery).addScalar("v", Hibernate.STRING).addScalar("l", Hibernate.STRING).list();
	    	
	    	// sort the values
	    	Iterator xaf = attributeNames.iterator();
	    	while(xaf.hasNext()){
	    	    String attName = xaf.next().toString();
			    %>
			    <td align="center">
			    <% if (_showLabels>0){ %>
			    	<%=attName %>
			    <%}
			    
	    	    
	    	    // loop and get the correct value
	    	    Iterator valueIterator = values.iterator();
	    	    while(valueIterator.hasNext()){
	    	        Object[] objects = (Object[])valueIterator.next();
	    	        if (objects[1].toString().compareTo(attName) == 0){ %>
	    	        	<%=objects[0] %><%
	    	        }
	    	    }
	    	}
		} 
	}
%>


</tr>	
<%

	if (isHeader){
	    sampleCount--;
	    isHeader = false;
	}

} // end sampleIDs loop %>



