
<%@page import="org.setupx.repository.server.persistence.SXQueryCacheObject"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="org.hibernate.criterion.Expression"%><tr> 

<% 

// this incl file requieres an object of the type queryCacheObject - which is valid and contains the information for the experiment and the user.


org.hibernate.Session _hqlSessionTMPincl = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

	    String dateLabel = "";
		int size = new SXQuery().findSampleIDsByPromtID(queryCacheObject.getExperimentID()).size();
		if (size == 0 ) size = 1;
		int percent1 = (100 * queryCacheObject.getNumberofScannedSamples()) / size; 
		int percent2 = 100 - percent1  ;
		
		// the last six samples from this experiment
		String queryStringInt = "select message.date as date from formobject as promt, formobject as sample, formobject as clazz, "
			+ "formobject as child2, formobject as page, message as message "
			+ "where promt.discriminator = \"" + Promt.class.getName() + "\" and page.parent = promt.uoid and child2.parent = page.uoid " 
			+ "and clazz.parent = child2.uoid and sample.parent = clazz.uoid " 
			+ "and sample.discriminator = \"" + Sample.class.getName() + "\" and promt.uoid = \"" + queryCacheObject.getExperimentID() + "\" " 
			+ "and message.relatedObjectID = sample.uoid order by message.uoid desc limit 7";

		
		List dates = _hqlSessionTMPincl.createSQLQuery(queryStringInt).addScalar("date", Hibernate.TIME).setCacheable(org.setupx.repository.Config.QUERY_CACHING_ACTIVE_JSP).list();
		Date estimatedFinalDate = null;
	
		// if no six samples exist - stop
		if ((dates.size() < 7 && percent1 < 100) ||  dates.size() < 1) { 
		    // no prediction possible
		} else {
		
			// take the first and the last sample as average
			Date sampleMinus0 = (Date)dates.get(0);
			Date sampleMinus6 = (Date)dates.get(dates.size()-1);
			
			int _____samplesTotal = new SXQuery().findSampleIDsByPromtID(queryCacheObject.getExperimentID()).size();
		    int numberOfScannedSamples = queryCacheObject.getNumberofScannedSamples();
	
			int samples_remaining = _____samplesTotal-numberOfScannedSamples;
			
			// milseconds for six samples
			long duration6 = (long)(sampleMinus0.getTime() - sampleMinus6.getTime());
			long duration1 = duration6 / 6;
			long duration_remaining = duration1 * samples_remaining;
			
			// minutes
			//int minutes = (int)(duration_remaining*1000*60);
			
			Calendar calendar = new GregorianCalendar();
			calendar.add(calendar.MILLISECOND, (int)duration_remaining);
			
			estimatedFinalDate = calendar.getTime();
			dateLabel = "estimated";
		}
		%>

	<tr>
	<td colspan="2">
		<table width="100%">
		<tr>
			<td align="center" style="background-color:#32CD32" width="<%=((percent1 * 3) + 1)%>"> <strong><%=percent1 %>%</strong></td>
					<%
					// showing remaining numbers only for unfinished experiments
					if (size - queryCacheObject.getNumberofScannedSamples()> 0) {%>
						<td style="background-color:#FF4500" width="<%=percent2 * 3%>" align="center" >
							<font color="white" class="small" >
							<%=( size - queryCacheObject.getNumberofScannedSamples()) %> Samples
							</font> 
						</td>
					<%
					} else {
										estimatedFinalDate = null;
										dateLabel = "finished";
					}%>
		</tr>
		
		<% if(estimatedFinalDate != null){ %>
			<tr>
				<td colspan="21" align="right" style="background-color:white">
												<font class="small"><%=dateLabel %> </font>
												<font class="small"> <strong><%=DateFormat.getTimeInstance(DateFormat.SHORT, Locale.US).format(estimatedFinalDate)%></strong></font> 
												<font class="small"><%=DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US).format(estimatedFinalDate)%></font>
				</td>
			</tr>
		<% } %>
		</table>
