<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net" xmlns:c="urn:jsptld:http://java.sun.com/jstl/core">
<jsp:directive.page import="org.setupx.repository.server.persistence.PersistenceActionFindException"/>
<jsp:directive.page import="org.setupx.repository.server.persistence.SXQuery"/>
<jsp:directive.page import="org.hibernate.Session"/>
<jsp:directive.page import="org.setupx.repository.core.CoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.text.SimpleDateFormat"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Datafile"/>

<jsp:scriptlet>
	char qMark = (char)38; // ascii 38 
</jsp:scriptlet>

<jsp:scriptlet>

	SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd yy HH:mm");
	boolean access = false;
	
	UserDO userDO = null;
	try {
	    userDO = (UserDO)session.getAttribute("user"); 
	} catch (Exception e){
	    userDO = UserDO.load("public");
	}


</jsp:scriptlet>

<display:table id="row" name="test" defaultsort="8" defaultorder="descending" pagesize="20" >
	<jsp:scriptlet>
	    if (!userDO.isLabtechician()){
	        Session s = CoreObject.createSession();
	        
	        try {
	            if (new SXQuery(s).findPromtUserAccessRightForUserID(
	                    userDO.getUOID(), 
	                    (new SXQuery(s).findPromtIDbySample(((Datafile)row).getSampleID()))
	                    ).getAccessCode() > 20){
	                access = true;
	            }
	            s.close();
	        } catch (PersistenceActionFindException e) {
	        }
	    } else {
	        access = true;
	    }
    </jsp:scriptlet>


    <display:column  title="access">
		<jsp:scriptlet>	
			if(access){
		</jsp:scriptlet>
			<img src="pics/ok.gif"/>
		<jsp:scriptlet>	
			} else {
		</jsp:scriptlet>
			<img src="pics/locked.jpg"/>
		<jsp:scriptlet>	
			}
		</jsp:scriptlet>
	
		
		
		
		
		
		
		
    </display:column>
    <display:column title="Sample ID" property="sampleID" sortable="true" headerClass="sortable" href="techno_sampleFiles.jsp?" paramId="id" paramProperty="sampleID" group="1"/>
	<display:column title="Technology" sortable="true" headerClass="sortable"  href="techno_technology.jsp?" paramId="id" paramProperty="technologyFileType.technology.UOID" group="2">
	      <c:out value="${row.technologyFileType.technology.label}"/>
    </display:column>
	<display:column title="FileType" sortable="true" headerClass="sortable"  href="techno_technologyfiletype.jsp?" paramId="id" paramProperty="technologyFileType.UOID" group="3">
	      <c:out value="${row.technologyFileType.label}"/>
    </display:column>
	<display:column title="File name" sortable="true" headerClass="sortable"  href="techno_sampleFile.jsp?" paramId="path" paramProperty="sourceFile" group="3">
	      <c:out value="${row.sourceFile.name}"/>
    </display:column>
	<display:column title="location" property="sourceFile.parent" sortable="true" ></display:column>
    <display:column title="created on"  property="dateCreated" sortable="true" headerClass="sortable" format="{0,date,MMM-dd-yyyy HH:mm}"/>
    <display:column title="assigned"  property="dateDetected" sortable="true" headerClass="sortable" format="{0,date,MMM-dd-yyyy HH:mm}"/>
	<display:column title="assigned by" sortable="true">
    	<jsp:scriptlet>
    		String username = "unassigned";
    		if (((Datafile)row).getSampleID() > 0){
	    		username = "-system-";
	    		try {
		    		UserDO submitter = (UserDO)UserDO.persistence_loadByID(UserDO.class, CoreObject.createSession(), ((Datafile)row).getSubmitterUserID());
		    		username = submitter.getDisplayName();
	    		} catch (Exception e){
	    		}
    		}
    		
    	</jsp:scriptlet>
		<jsp:expression>username</jsp:expression>
    </display:column>
    <display:column title="exists" sortable="true">
    	<jsp:scriptlet>
    		String img = "";
    		if (((Datafile)row).getSourceFile().exists()){
    			img = "pics/check.gif";
    		} else {
    			img = "pics/x.gif";
    		}
    	</jsp:scriptlet>
		<![CDATA[<img src=]]>"<jsp:expression>img</jsp:expression>"<![CDATA[/>]]>
    </display:column>

	<jsp:scriptlet>		
		String urlParameter = "";
		try{
			String t = "t=file";
			urlParameter = t + qMark + "id=" + ((Datafile)row).getUOID() + qMark + "files=" + ((Datafile)row).getTechnologyFileType().getUOID();
		} catch(Exception e){
		    e.printStackTrace();
		}
	</jsp:scriptlet>

	<display:column title="download" sortable="false" headerClass="sortable" href="techno_sampleFiles.jsp?" paramId="id" paramProperty="sampleID">
	<jsp:scriptlet>	
	if(((Datafile)row).getUOID() > 0){
		if(access){
	</jsp:scriptlet>
	
								<![CDATA[<a href="techno_download.jsp?]]>
								<jsp:expression>urlParameter</jsp:expression>				
								<![CDATA[">]]>
								<img width="33%" height="33%" src="pics/go_download.gif"/>
								<![CDATA[</a>]]>
	<jsp:scriptlet>	
		} else {
			</jsp:scriptlet>
				<![CDATA[<a href="">]]>
					<img width="33%" height="33%" src="pics/go_blank.gif"/>
				<![CDATA[</a>]]>
			<jsp:scriptlet>	
		}
	}
	</jsp:scriptlet>
	</display:column>	
</display:table>
	
</jsp:root>