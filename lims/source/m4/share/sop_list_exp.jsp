<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.communication.document.sop.StandardOperationProcedure"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%

	long promtID = Long.parseLong(request.getParameter("id"));

	String query = "select * from document as d join formobject as f " +
				" on f.value = d.uoid and f.uoid <" + (promtID + 1000) +
				" and f.uoid > " + (promtID - 1000)  + 
				" and f.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.SopRefField\" " + 
				" and root(f.uoid) = " + (promtID) + 
				" order by d.uoid ";

    List l = CoreObject.createSession().createSQLQuery(query).addEntity(StandardOperationProcedure.class).list();

    
    Iterator iter = l.iterator();
    while (iter.hasNext()){
        try {
	        StandardOperationProcedure standardOperationProcedure = (StandardOperationProcedure)iter.next(); 
			StringBuffer buffer = new StringBuffer();
	           buffer.append("\n<!-- " + Util.getClassName(this) + " -->\n<tr><td>\n");
	           
	           buffer.append("<table border=\"0\">");
	           buffer.append("<tr>");
	           buffer.append("<td colspan=\"2\"><hr>");
	           buffer.append("</tr>");
	           buffer.append("<tr>");
	           buffer.append("<td colspan=\"2\">");
	           buffer.append("<font style=\"font-size: xx-small;\">issued by: Responsible: <nobr><a href=\"mailto:" + standardOperationProcedure.getResponsible().getEmailString() + "\">" + standardOperationProcedure.getResponsible().getDisplayName() + "</a></nobr>");
	           buffer.append("</font>");
	           buffer.append("</td></tr>");
	           buffer.append("<tr><td colspan=\"2\">");
	           buffer.append("<h3> " + standardOperationProcedure.getLabel() + " (Version " + standardOperationProcedure.getVersion() + ")</h3>");
	           buffer.append("</td></tr><tr><td>");
	           buffer.append("<b>Description</b><br>");
	           buffer.append(standardOperationProcedure.getContent().replaceAll("\n", "<br>"));
	           buffer.append("</td>");
	           buffer.append("</td> </tr>  </table>");
	
	           buffer.append("\n</td></tr>\n");
			   %><%=buffer.toString()%><%
        } catch (Exception e){
            e.printStackTrace();
        }
    }
%>