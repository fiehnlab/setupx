
<%@ include file="checklogin.jsp"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.core.communication.leco.ColumnInformation"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.file.SampleFile"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

<%@ include file="header_standard.jsp"%>

<%
	String[] colums =new BinBaseServiceConnector().determineColumns();

%>

<table align="center" width="80%" cellpadding="7">

	<tr>
		<th rowspan="301">&nbsp;&nbsp;&nbsp;
		<th colspan="21">&nbsp;&nbsp;&nbsp;
	<tr>
		<td colspan="21">
			The following classes will be combined in a one export. 
			<p>		
			<b>Enter a label for your dataset.</b> 
			<p>
			<form action="export_C_submit.jsp">
			<input type="text" name="name">

			<p>
			And select the column that the samples were run on:
			<select name="column">
				<%
				for(int colC = 0; colC < colums.length; colC++){
				    %>
					<option value="<%=colums[colC] %>"  
					<%if (colums[colC].indexOf("rtx") > -1){ %> SELECTED <%}%> ><%=colums[colC] %>
					</option>
				    <%
				}
			%>
			</select>
			
			<input type="submit">
			</form>
		
	<tr>
		<th colspan="21">The following classes have been selected for export.
	<tr>
		<td><b>ClassID
		<td><b>Number of Samples
		<td><b>Class Information
		<td><b>Experiment ID
	

<%
Vector classesList = (Vector)request.getSession().getAttribute("selectedClasses");


for (int i = 0 ; i < classesList.size(); i ++){
	Session hqlSessionClass = CoreObject.createSession();

    int id = Integer.parseInt("" + classesList.get(i));
    List sampleIDs = new SXQuery(hqlSessionClass).findSampleIDsByClazzID(id);
%>
	<tr>
		<td><%=id %>
		<td><%=sampleIDs.size() %>
		<td><%=new SXQuery(hqlSessionClass).findClassInformationString(id) %>
		<td><%=new SXQuery(hqlSessionClass).findPromtIDbyClazz(id) %>


	<tr>
		<td colspan="21">
			<table>
		<%

	List l = new SXQuery(hqlSessionClass).findSampleIDsByClazzID(id);
	try{
		hqlSessionClass.close();
	}catch (Exception e){
	    e.printStackTrace();
	}

	Iterator sampleIDIterator = l.iterator();
	while (sampleIDIterator.hasNext()){
		Session hqlSessionSamples = CoreObject.createSession();

		try{
		    long sampleID = Long.parseLong("" + sampleIDIterator.next());
		    
		    String label = "" + sampleID;
		    Logger.debug(this, "sampleID: " + sampleID);
	
		    // latest file
		    List scannedPairs = new SXQuery(hqlSessionSamples).findScannedPair(sampleID);
			try{
			    hqlSessionSamples.close();
			}catch (Exception e){
			    e.printStackTrace();
			}


		    
			if (scannedPairs.size() < 1) throw new Exception("unable to find ScannedPair for sample " + sampleID);
		    String acqname = "" + scannedPairs.get(0);
	
		    // filecheck
		    %>
		        		<tr>
	        				<td><font class="small"><%=sampleID %></font></td>
	        				<td><font class="small"><%=acqname %></font></td>
		        			<%if (SampleFile.exists(acqname, SampleFile.TXT)){ %>
		        				<td style="background-color: green"><font class="small">TXT</font></td>
		        			<%} else { %>
		        				<td style="background-color: red"><font class="small">TXT</font></td>
		        			<%} %>
		        			<%if (SampleFile.exists(acqname, SampleFile.PEG)){ %>
		        				<td style="background-color: green"><font class="small">PEG</font></td>
		        			<%} else { %>
		        				<td style="background-color: red"><font class="small">PEG</font></td>
		        			<%} %>
		        			<%if (SampleFile.exists(acqname, SampleFile.CDF)){ %>
		        				<td style="background-color: green"><font class="small">CDF</font></td>
		        			<%} else if (SampleFile.exists(acqname, SampleFile.xCDF)){ %>
		        				<td style="background-color: green"><font class="small">xCDF</font></td>
		        			<%} else { %>
		        				<td style="background-color: red"><font class="small">CDF</font></td>
		        			<%} %>
		        			<%if (SampleFile.exists(acqname, SampleFile.SMP)){ %>
		        				<td style="background-color: green"><font class="small">SMP</font></td>
		        			<%} else { %>
		        				<td style="background-color: red"><font class="small">SMP</font></td>
		        			<%} %>
	        			</tr>		    
		    <%
		}catch(Exception e){
		    e.printStackTrace();
		}
	}
%>
</table>	
<% 
}
%>
	
</table>