<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Enumeration"%>

<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBI_Tree"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>



<%
Hashtable splitpoints = new Hashtable();
Iterator iterator = NCBI_Tree.splitpointIDs.iterator();
while (iterator.hasNext()){
    try {
	    NCBIEntry entry = NCBIConnector.determineNCBI_Information(Integer.parseInt(iterator.next()+""));
		NCBIEntry splitPoint = NCBI_Tree.findGroup(entry.getParent().getName());

		splitpoints.put(entry, splitPoint);
		%><br><b><%=entry.getName()%></b> Splitpoint:<%=splitPoint.getName()%>   <b>Exp:<%=NCBI_Tree.findExperiments(entry.getTaxIdInt())%></b><% 
    }catch (Exception e){
        e.printStackTrace();
    }
}	

String currentSplitPointName = "Viridiplantae"; //"Magnoliophyta";
Enumeration keys;
NCBIEntry splitpoint;
NCBIEntry key = null ;
NCBIEntry currentSplitPoint;
int relatedSplitpoint;
int thisID;
boolean identical;
String[] tmp = new String[100];
Enumeration[] keyTMP = new Enumeration[100];
int level = 0;
thisID = NCBIConnector.determineNCBI_Information(NCBIConnector.determineNCBI_Id(currentSplitPointName)).getTaxIdInt();

%>

<table>
<tr><td>


<%@include file="currentSplitPoint.jsp" %>
</table>