<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Date"%>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>



<%
	Date date = new Date();
	Date releaseDate = new Date(108,05,01);

	
	boolean available = date.after(releaseDate);

	long remainingHRs = (releaseDate.getTime() - date.getTime()) / 1000 / 60 / 60;
%>




<table width="100%" height="100%" cellspacing="20" border="0">
	<tr valign="middle" align="center">
		<td align="center">
			<table width="60%" style="background-color: white" cellspacing="3">
			<% if (available){ %>
				<tr>
					<td align="center"><h2>SetupX is released and is available as an open source project. 
			<% } else { %>
				<tr>
					<td align="center"><h2>SetupX will be released as an open source project in <%=remainingHRs %> hours. 
					<font class="small"> <br>This will be at <%=releaseDate.toLocaleString()%></font>.
				<tr>
					<td align="center">  
					<font class="small">Then you will be able to download the sourcecode and create your own local SetupX installation.</font>.
			<% } %>
				<tr>
					<td align="center">
						<a href="index.jsp"><img src="pics/logo.gif" border="0"></a>
				<tr>
					<td>
						<table width="70%" align="center">
							<tr>
								<th colspan="2">
									Sources
							<tr>
								<td width="90%">
									source code
								<td>
									<% if (available){ %>
										<a href="http://setupx.googlecode.com/">
											<img src="pics/go_report.gif" border="0">
										</a>
									<% } else { %>
										<img src="pics/go_locked.gif" border="0">
									<%} %>
							<tr>
								<td>
									life image
								<td>
									<% if (false){ %>
										<a href="">
											<img src="pics/go_report.gif" border="0">
										</a>
									<% } else { %>
										<img src="pics/go_locked.gif" border="0">
									<%} %>
							<tr>
								<th colspan="2">
									Documentation
							<tr>
								<td>
									view documentation
								<td>
									<% if (available){ %>
										<a href="http://setupx.googlecode.com/">
											<img src="pics/go_report.gif" border="0">
										</a>
									<% } else { %>
										<img src="pics/go_locked.gif" border="0">
									<%} %>
							<tr>
								<th colspan="2">
									contact
							<tr>
								<td>
									Martin Scholz (architect)
								<td>
									<a href="mailto: mscholz@ucdavis.edu">
										<img src="pics/go_globe.gif" border="0">
									</a>
							<tr>
								<td>
									Dr. Oliver Fiehn (PI)
								<td>
									<a href="mailto: ofiehn@ucdavis.edu">
										<img src="pics/go_globe.gif" border="0">
									</a>
							<tr>
								<th colspan="2">
									Exit
							<tr>
								<td>
									back to SetupX
								<td>
										<a href="index.jsp">
											<img src="pics/go_exit.gif" border="0">
										</a>
							<tr>
								<td colspan="2">
									<br><br>&nbsp;&nbsp;

							<tr>
								<th colspan="2">
									related Projects

							<tr>
								<td width="90%">
									binbase
								<td>
									<% if (available){ %>
										<a href="http://binbase.fiehnlab.ucdavis.edu:8080/confluence/display/BinBase/Welcome/">
											<img src="pics/go_report.gif" border="0">
										</a>
									<% } else { %>
										<img src="pics/go_locked.gif" border="0">
									<%} %>
							
							
							
</table>


