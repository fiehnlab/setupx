<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.Hashtable"%>
<%@page import="org.setupx.repository.Config"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<%@include file="incl_xls_header.jsp"%>

<%
	Hashtable relations = null; //(Hashtable) session.getAttribute("relations");
	if (relations == null){
	    relations = new Hashtable();
	}

	Session hqlsession = CoreObject.createSession();

	Vector rows = new Vector();
	rows = (Vector)session.getAttribute("data"); 
	Logger.debug(this, "got rows");
	
	Vector labels = new Vector();
	// labels by arrow from another row

	try {
		int labelsPosition = Integer.parseInt("" + request.getParameter("labels")); 
		labels = (Vector)rows.get(labelsPosition);
		rows.remove(labelsPosition);
	} catch (Exception xe){
		try {
		    labels = (Vector)session.getAttribute("labels"); 
		    if (labels == null) throw new Exception("no labels");
		} catch (Exception e){
		    labels = new Vector();
		}
	}
	session.setAttribute("labels", labels);
	Logger.debug(this, "got labels");
%>


<table align='center' border='0' cellpadding='4' width="60%" border = 1 >
<tr>	
	<th colspan="21">Select the row containing labels.
<tr>	
	<td colspan="21">
		The system is looking in your dataset of indicators that match existing samples in <%=Config.SYSTEM_NAME%>. (Lables, SampleIDs, ...)
<%
	// find the max length of the vectors
	int maxRow = 0;
	for(int i = 0; i < rows.size(); i++){
	    try {
		    if (((Vector)rows.get(i)).size() > maxRow) maxRow = ((Vector)rows.get(i)).size() ;
	    } catch (Exception e){
	        // some dont have that many elements in a row
	    }
	}

	if (labels.size() == 0){
	    labels.setSize(maxRow);
	}

	%>
        <tr>
	        <td>
    	    </td>
		<%
		Logger.debug(this, "looping over labels " + labels.size());
		for(int i = 0; i < labels.size(); i++){
			String label = "" + labels.get(i);
			if (label.compareTo("null")==0) label = "";
			%>
	        <th align="center">
	        	<%=label%>
	   	    </th>
		<%}%>
    	</tr>
        <%
			Iterator rowIterator = rows.iterator();
        	int rowCounter = -1;
        	while(rowIterator.hasNext()){
        	    rowCounter++;
        	    Vector row = (Vector)rowIterator.next();
                %>
		<tr>
            <th>
            	<table>
            		<tr>
            			<th><%=(rowCounter+1) %></th>
            		</tr>
            	</table>	
            </th>
	        </td>
	        	<% 
	        	boolean keepLooping = true;
	        	for (int i = 0; i < row.size() && keepLooping; i++){
	        	   %> 
			        <td align="right">
			        
			        
			        <%
			        	// looking for ID or sampleLabel
			        	
			        	int parsedValue = 0; 
			        	try {
			        	    if (row.get(i).toString().length() < 3) throw new Exception("too short");
				        	Logger.debug(this, " autofind 0 " + row.get(i));
				        	String unparsedValue = "" + row.get(i);
			        	    parsedValue = Integer.parseInt(unparsedValue.substring(0, unparsedValue.indexOf('.')));
			        	} catch (Exception e){
			        	    // cant parse
			        	}
			        	Logger.debug(this, " autofind 1 " + parsedValue);
			        	
			        	long detectedSXID = 0;
			        	// try to find a matching sample
			        	if (parsedValue > 10000) { // valid samples start beyond that
			        	    try {
			        	        // try it if the value is an int.
			        	        String query = "select sample.uoid from formobject as sample " +
			        	        	" where sample.discriminator like \"%Sample\" and sample.uoid = " + parsedValue;
			        	        
			        	        detectedSXID = Long.parseLong(hqlsession.createSQLQuery(query).addScalar("uoid", Hibernate.STRING).list().get(0) + "");
					        	Logger.debug(this, " autofind 2 " + detectedSXID);
			        	    }catch (Exception e){
			        	        
			        	        try {
			        	            if (row.get(i).toString().length() < 3) throw new Exception ("too short");
				        	        // search by the value
				        	        String query = "select sample.uoid from formobject as sample, " +
				        	        	" formobject as label where sample.discriminator like \"%Sample\" " +
				        	        	" and label.parent = sample.uoid and label.value = \"" + row.get(i) + "\"";
				        	        
				        	        detectedSXID = Long.parseLong(hqlsession.createSQLQuery(query).addScalar("uoid", Hibernate.STRING).list().get(0) + "");
						        	Logger.debug(this, " autofind 3 " + detectedSXID);
			        	        } catch (Exception exceptione){
			        	            
			        	        }
			        	    }
			        	}
			        %>
	        		<%=row.get(i)%>
			        <%if (detectedSXID != 0){ %>
		        		[<%=detectedSXID %>]
		        		<%
		        		relations.put(""+detectedSXID, rowCounter + "");
		        		Logger.log(this, "creating relation: " + detectedSXID + " - " + rowCounter);
	                    keepLooping = false;
	                    %>
			        <%} %>
		    	    </td>
	        	   <%  
	        	}%>
	       	</td>
		</tr>
		<% } // row iterator %>
</table>

<%
session.setAttribute("relations", relations);
%>
<meta http-equiv="refresh" content="3; URL=xlsimport6.jsp">

</body>
</html>