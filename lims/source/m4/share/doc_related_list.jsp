<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.web.DownloadServlet"%>
<%@page import="java.util.Date"%>


<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ include file="checknonlogin.jsp"%>


<%@ include file="header_standard.jsp"%>


<%
	// List of all files related to one experimet
	
	long promtID = Long.parseLong(request.getParameter("promt"));

	// checking user access
	PromtUserAccessRight.checkAccess(user.getUOID(),  promtID, PromtUserAccessRight.DOWNLOAD_RESULT);

	File[] files = Promt.determineRelatedFields(promtID);
%>

<table align='center' border='0' cellpadding='4' width="60%">
	<tr>	
		<td style="background-color: white;" align="center">
			<a href="load?id=<%=promtID %>&action=20"><img src="pics/back.gif" border="0"></a>
			<font size=\"-3\"><br>Experiment <%=promtID %></font>
		<td colspan="3">

	<tr>
		<th colspan="21">
			Files related to Experiment <%=promtID %>
			<font size=\"-3\"><br>Download any of the following files by clicking the file symbol.</font>
			
<%
	
	for(int c = 0; c < files.length; c++){
	    File file = files[c];
	    %>
	<tr>
		<td style="background-color: white;" align="center">
			<a href="<%=DownloadServlet.createDownloadLink(file.getName(), "" + promtID)%>"><img src="pics/file.jpg" border="0"/></a>
		<td><%=file.getName() %>
		<td><font size=\"-3\">last modified: <%=new Date(file.lastModified()).toLocaleString()%></font></span></td>
		<td style="background-color: white;" align="center"><a href="<%=DownloadServlet.createDownloadLink(file.getName(), "" + promtID)%>">
				<img src="<%=org.setupx.repository.core.util.File.determineFileIMG(file)%>" border="0" valign="middle" align="center">
			</a>
	</tr>
	    <%
	}
%>
	<tr>
		<th colspan="21">
			Add additional files Files
			<font size=\"-3\"><br>select the file from your computer and upload it into SetupX.</font>
		</th>
	</tr> 
<form action="doc_related_upload.jsp" method="post" enctype="multipart/form-data">
	<tr>
		<td>
		<td>Select file
		<td>Experiment ID
		<td>
	</tr>
	<tr>
		<td>
		<td><input type="file" name="uploadfile"/>
		<td><input type="text" name="promt" value="<%=promtID %>" readonly="readonly"/></td>
		<td><input type="submit" value="upload">
		
	</tr>
</form>

</table>


