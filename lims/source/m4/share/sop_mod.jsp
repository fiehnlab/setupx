<%@page import="org.hibernate.Transaction"%>
<%@page import="java.util.Date"%>
<%@page import="org.setupx.repository.core.communication.document.DocumentObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.document.sop.StandardOperationProcedure"%>
<% 
	//sop_mod.jsp?a=1&v=1	
%>

<%
	// id 
	long sopID = Long.parseLong(request.getParameter("id"));
	
	// attribute
	int attribute = Integer.parseInt(request.getParameter("a"));
	
	// value
	String value = "";
	try {
		value = request.getParameter("v");	
	} catch (Exception e){
	    // stays empty
	}

	
	Session s = StandardOperationProcedure.createSession();
	StandardOperationProcedure standardOperationProcedure = StandardOperationProcedure.load(s, sopID);

    switch (attribute) {
    case 1: // public
    	if ( value.compareTo("1") == 0){
        	standardOperationProcedure.setPubliclyAvailable(true);
        	%>made private<%
    	} else {
        	standardOperationProcedure.setPubliclyAvailable(false);
        	%>made public<%
    	}
        break;

    case 2: // date
		Date date = new Date();
    	standardOperationProcedure.setDateValidStart(date);
        break;

    case 3: // date
		date = StandardOperationProcedure.NEVER;
    	standardOperationProcedure.setDateValidStart(date);
        break;

    default:
        break;
    }

//    Transaction t = s.beginTransaction();
    standardOperationProcedure.update(s);
//    t.commit();
    
    
%>
<%@page import="org.setupx.repository.core.communication.sop.SOP"%>
<jsp:forward page="sop_detail.jsp?id=<%=sopID%>"/>

	