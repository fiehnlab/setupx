<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.communication.importing.logfile.*,
				 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 	org.setupx.repository.core.communication.leco.*,
                 org.setupx.repository.server.persistence.*"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%
int promtID = Integer.parseInt(request.getParameter("id"));

%>
<body>



<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width='90%'>
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Export</h2>
			Samples & Column
			<br/>
			<font size="-2">Below you find all samples including every available run for each sample related to the selected experiment.<br>Choose all samples that you want to include in the result.</font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>


<form method="post" action="export_summary.jsp">

<!--  column information -->
	<table align="center" width="60%" cellpadding="2">






	<table align="center" width="60%" cellpadding="2">	<tr>
		<th colspan="10">Select Samples for Export <%=promtID %></th>
	</tr>

<%


List classList = new SXQuery().findClazzIDsByPromtID(promtID);
Iterator iterator = classList.iterator();

// each class
int staticCount = -1;
while (iterator.hasNext()) {
    staticCount++;
    int classID = Integer.parseInt("" + iterator.next());

    // class labels
    java.util.Iterator iterator1 = new SXQuery().findLabelsforClazz(classID).iterator();
    %>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<%
		int group = staticCount;
		try {
		    group = Integer.parseInt(request.getParameter("group" + classID));    
		} catch (Exception e){
	
		}
		
		%>
		<th></th>
		<th colspan="2" align="center" style="background-color:<%=org.setupx.repository.core.util.Util.getColor(group*2)%>">Class <b><%=classID %></b></th>

		<th colspan="6">
			<table align='center' border='0' cellpadding='4'>
			  <tr>
				<% for (int cl = 0; cl < classList.size(); cl++){ %>
				    <!-- <th valign="middle" align="center" style="background-color:<%=org.setupx.repository.core.util.Util.getColor(cl*2)%>" ><input type="radio" name="group<%=classID%>" value="<%=cl%>"></th> -->
					<th><a href="export_super_class.jsp?id=<%=promtID%>&group<%=classID%>=<%=cl%>"><img src='<%if (cl==group){ %>pics/checked.jpg<%} else {%>pics/blank.jpg<%}%>' border="0" valign="middle" align="center"></a></th>
				    <th valign="middle" align="center" style="background-color:<%=org.setupx.repository.core.util.Util.getColor(cl*2)%>" ><b><%=cl%></b></th>
			    <%} %>
			  </tr>
			</table>
		</th>
		<th></th>
	</tr>
	<tr>
	<td colspan="1"></td>
	<td style="background-color:<%=org.setupx.repository.core.util.Util.getColor(group*2)%>"  colspan="1"></td>
	<td colspan="7">
		<table>
    <% 
    while (iterator1.hasNext()) {
    Object[] element = (Object[]) iterator1.next();
      %>
			<tr>
					<td><b><font class="small"><%=element[0].toString()%>:</font><b></td>
					<td><font class="small"><%=element[1].toString()%></font></td>
			</tr>
      <%
    }
 	%>
			</table>
	    </td>
	  <td></td>
	</tr>
	
	
	<%
    // samples in that class
    Iterator sampleIDs = new SXQuery().findSampleIDsByClazzID(classID).iterator();
    while (sampleIDs.hasNext()){
        int sampleID = Integer.parseInt("" +  sampleIDs.next());
        //String label = new SXQuery().findSampleLabelBySampleID(sampleID);
        
		List list = new SXQuery().findScannedPair(sampleID);
        Iterator scannedPairs = list.iterator();
        boolean checked = true;
        
        %>

		<% if(list.size() == 0){%>
        <tr>
        <td ></td>        
        <td style="background-color:<%=org.setupx.repository.core.util.Util.getColor(group*2)%>" align="center"></td>        
        <td align="center">Sample <%=sampleID%></td>        
        <td align="center"><%=new SXQuery().findSampleLabelBySampleID(sampleID) %></td>        
        <td align="center" colspan="3">Sample has not been run yet.</td>        
		<td align="center"><img src='pics/hprio_tsk.gif' border="0" valign="middle" align="center"></td>        
        <td><input type="checkbox" disabled="disabled"></td>        
        <td></td>        
        </tr>
		<%} %>        
        
        
        <%
        HashSet hash = new HashSet();
        boolean first= true;
        while (scannedPairs.hasNext()) {
			String acqname = (String) scannedPairs.next();
			if (!hash.contains(acqname)){
	            hash.add(acqname);
	            %>
	        <tr>
	        <td></td>        
	        <td align="center"><% if (first){ %><a href="sample_detail.jsp?id=<%=sampleID%>"><img src='pics/details.gif' border="0" valign="middle" align="center"></a><%}%></td>        
	        <td><% if (first){ %>Sample <%=sampleID%> <%}%></td>        
	        <td><% if (first){ %><%=new SXQuery().findSampleLabelBySampleID(sampleID) %> <%}%></td>        
	        <td align="center" colspan="3"><%=acqname%></td>
	        <td align="center" valign="middle">
				<a href="run_detail_acq.jsp?acq=<%=acqname%>">	
		    	    <img src='pics/details.gif' border="0" valign="middle" align="center">
		        </a>
	        </td>        
	        <td><input type="checkbox" name="<%=acqname%>" value="<%=sampleID%>" <%if (checked){ %>checked="checked" <%} %>></td>        
	        <td></td>        
	        </tr>
			<%
            if (checked){   
                checked = false;
            }
        	}
			first = false;
        }
    }
}
%>
	  		<tr>
  					<td><input type="hidden" name="promt" value="<%=promtID%>"></td>
  					<td colspan="6" align="right">Send selection to Export.  </td>
  					<td colspan="2" align="left"><input src='pics/go_export.gif' type="image" value="connect"/></td>
  					<td></td>
	  		</tr>
	<tr>
		<td colspan="1"></td>
		<td colspan="8">
		<td colspan="1"></td>
	</tr>
	<%
	%>
</table>
</form>
</body>

<%@ include file="footer.jsp"%>
