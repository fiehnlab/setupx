<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.document.DocumentCategory"%>
<%
	
	// sop_category_alter.jsp
	
	// attribute to alter
	// 1 - public
	// 2 - in sx frontend
	// 3 - ...
	
	Session s = CoreObject.createSession();
	
	int type = Integer.parseInt(request.getParameter("type"));
	
	// DocumentCategory to alter
	long id = Long.parseLong(request.getParameter("id"));
	DocumentCategory category = (DocumentCategory) DocumentCategory.persistence_loadByID(DocumentCategory.class,s, id);

	if (type == 1){
	    boolean value = category.isPublic();
		category.setPublic(!value);
	}

	if (type == 2){
	    boolean value = category.isInSXFrontEnd();
		category.setInSXFrontEnd(!value);
	}
	
	category.update(s);

	// 
%>
	
<meta http-equiv="refresh" content="0; URL=sop_category.jsp">
