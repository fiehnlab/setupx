<%@ include file="incl_header_msi.jsp"%>



<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.Iterator"%>

<%

long id = Long.parseLong(request.getParameter("id"));

%>



<%
String bigQuery = "select " 
    + "clazz.uoid as class, " 
    + "mf_container.uoid as container, " 
    + "mf_container.question as question1, " 
    + "mf_label.question as question2,  " 
    + "mf_label.value, "  
    + "mf_label.msiattribute as msi1, " 
    + "mf_container.msiattribute as msi2 " 
    + "from   " 
    + "formobject as multiField4Clazzes,      " 
    + "formobject as mf_label,       " 
    + "formobject as clazz,       " 
    + "formobject as mf_container,       " 
    + "multiField4ClazzesHibernate as x       " 
    + "where clazz.uoid = x.multiField4Clazzs       " 
    + "and x.elt = multiField4Clazzes.uoid       " 
    + "and multiField4Clazzes.active = true       " 
    + "and mf_label.PARENT = multiField4Clazzes.uoid       " 
    + "and clazz.discriminator like \"%Clazz\" " 
    + "and mf_container.uoid = mf_label.PARENT      " 
    + "and root(clazz.uoid) = " + id + " "
    + "and mf_label.value != \"\"  " 
    + "and (   select count(*)  from formobject where parent = multiField4Clazzes.parent) > 1      " 
    + "order by clazz.uoid, mf_container.uoid, mf_label.question";

    
Logger.debug(this, bigQuery);
    
Session s = CoreObject.createSession();

List bigList = s.createSQLQuery(bigQuery)
	.addScalar("class", Hibernate.LONG)
	.addScalar("container", Hibernate.LONG)
	.addScalar("question1", Hibernate.STRING)
	.addScalar("question2", Hibernate.STRING)
	.addScalar("value", Hibernate.STRING)
	.addScalar("msi1", Hibernate.LONG)
	.addScalar("msi2", Hibernate.LONG)
	.list();



Iterator bigIterator = bigList.iterator();

%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<table align="center" width="60%" cellpadding="5">
<%
while(bigIterator.hasNext()){
    Object[] objects = (Object[] )bigIterator.next();
    long clazz = Long.parseLong(objects[0].toString());
    MSIAttribute attribute = null;
    try {
        long msi = Long.parseLong(objects[5].toString());
        attribute = (MSIAttribute)CoreObject.persistence_loadByID(MSIAttribute.class, s, msi);
    } catch (Exception e){
        
    }
	
    %>
    <tr>
    <td style="background-color: <%=Util.getColor((int)clazz)%>" >
    	<font style="font-size: xx-small;"><%=objects[0] %>
    	<a href="msi_samples.jsp?id=<%=objects[0] %>">details</a>
    </font>
    
    <%
    for (int i  = 1 ; i < objects.length; i++){
        if (objects[i] != null && objects[i].toString().compareTo("null") != 0){
	    %>
		    <td><font style="font-size: xx-small;"><%=objects[i] %></font>
	    <%
		} else {
	    %>
		    <td>
	    <%
		}
    } // -for
    
    if (attribute != null){
    %>
    <td><a href="msi_detail_attribute.jsp?id=<%=attribute.getUOID() %>"><%=attribute.getMsiLabel() %></a>
    <%
    }
} // -while
%>

