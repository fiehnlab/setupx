<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBI_Tree"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Enumeration"%>
<%
Hashtable splitpoints = new Hashtable();
Iterator iterator = NCBI_Tree.splitpointIDs.iterator();
while (iterator.hasNext()){
    try {
	    NCBIEntry entry = NCBIConnector.determineNCBI_Information(Integer.parseInt(iterator.next()+""));
		NCBIEntry splitPoint = NCBI_Tree.findGroup(entry.getParent().getName());

		splitpoints.put(entry, splitPoint);
%><br>
<a href="?species=<%= entry.getName()%>">
<%=entry.getName()%>
</a>
<% 
    }catch (Exception e){
        e.printStackTrace();
    }
}

String currentSplitPointName = request.getParameter("species");
NCBIEntry entry = NCBIConnector.determineNCBI_Information(NCBIConnector.determineNCBI_Id(currentSplitPointName));
NCBIEntry splitPoint = NCBI_Tree.findGroup(currentSplitPointName);
NCBIEntry nextSplitPoint = splitPoint;
if (nextSplitPoint.getTaxIdInt() == entry.getTaxIdInt()){
    nextSplitPoint = NCBI_Tree.findGroup(entry.getParent().getName());
}
%>

<br>
<hr>
<a href="?species=<%=nextSplitPoint.getName() %>">[+SPLIT]<font class="small"><%=nextSplitPoint.getName() %></font></a><br>
<a href="?species=<%=entry.getParent().getName() %>">[parent]<font class="small"><%=entry.getParent().getName() %> </font></a>
<br>
<b><%=entry.getName() %></b> <%=entry.getSpeciesTypeName() %>
<%



// list all the childs
Iterator iteratorChilds  = entry.getSplitpointChilds().iterator();

while (iteratorChilds.hasNext()){
    NCBIEntry child = (NCBIEntry)iteratorChilds.next();
    %>
    <br>
	<a href="?species=<%=child.getName()%>">
		<font class="small"><%=child.getName()%></font>
	</a>
    <%
}

%><br>Lineage<br><%

boolean loopIt = true;

while (loopIt){
    try {
        if (entry.getTaxIdInt() == 1) loopIt = false; %>    	
       	.<a href="?species=<%=entry.getName()%>"><%=entry.getName()%></a> 
    	<%
        entry = entry.getSplitpointParent();
    } catch (Exception exception){
        loopIt = false;
    }
}

%>





<%

NCBIEntry e = NCBIConnector.determineNCBI_Information(1); // root




%>
