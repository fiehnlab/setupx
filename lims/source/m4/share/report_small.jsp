<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.List,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.communication.export.reporting.*,
                 org.setupx.repository.server.persistence.*"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%
Report report = (Report)session.getAttribute(org.setupx.repository.web.WebConstants.SESS_REPORT);
List clazzIDs = new SXQuery().findClazzIDsByPromtID(report.getPromtID());
%>	


<body>
<!--  head containing logo and description -->

	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2><%=new SXQuery().findPromtTitleByPromtID(report.getPromtID()) %></h2>
					Experiment <%=report.getPromtID()%><br/>
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>

	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} %>
	

	<table align="center" width="60%" cellpadding="7">	
	<!--  back to the menu -->

	<tr>
		<td colspan="1" align="right" style="background-color:#FFFFFF"><a href="main.jsp"><img  border='0' src="pics/back.gif"></td>
		<td colspan="5"></td>
		<td colspan="1"></td>
	</tr>

	<tr>
		<td ></td>
		<th colspan="5">Short Summary</th>
		<td ></td>
	</tr>
	<tr>
		<td ></td>
		<td colspan="4">This Summary contains a <b>brief overview</b>, including all classes but not all samples. Press this icon to receive the <strong>whole</strong> report.</td>
		<td align="center" style="background-color:#FFFFFF"><h2>Details: </h2><a href="report.jsp"><img  border='0' src="pics/go.gif"></a></td>
		<td ></td>
	</tr>
	<tr>
		<td ></td>
		<td width="0">Title</td>
		<td colspan="4" width="2100" style="background-color:#FFFFFF"><b><%=new SXQuery().findPromtTitleByPromtID(report.getPromtID()) %></b></td>
		<td ></td>
	</tr>
	<tr>
		<td ></td>
		<td width="0">Abstract</td>
		<td colspan="4" style="background-color:#FFFFFF"><%=new SXQuery().findPromtAbstractByPromtID(report.getPromtID()) %></td>
		<td ></td>
	</tr>
	<tr>
		<td ></td>
		<td width="0">Classes</td>
		<td colspan="4" style="background-color:#FFFFFF"><%=clazzIDs.size() %></td>
		<td ></td>
	</tr>

<%
org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();
	
String comment = (String) hqlSession.createSQLQuery("select element3.value as comment from formobject as promt, formobject as page, " +
        "formobject as element3 where promt.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.Promt\" " +
        "and page.parent = promt.uoid and element3.parent = page.uoid and element3.question = \"comment\" and " + 
        "element3.discriminator =\"org.setupx.repository.web.forms.inputfield.BigStringInputField\" " + 
        "and promt.uoid = "+report.getPromtID()+ "").addScalar("comment", org.hibernate.Hibernate.STRING).list().get(0);
        
hqlSession.close();

comment = org.setupx.repository.core.util.Util.replace(comment,'\n',"<br>");
%>
	
		<tr>
			<td ></td>
			<td width="0">Comment</td>
			<td colspan="4" style="background-color:#FFFFFF"><%=comment %></td>
			<td ></td>
		</tr>
	
		<tr>
			<td ></td>
			<td width="0">Progress</td>
			<td colspan="4" style="background-color:#FFFFFF">
			<% SXQueryCacheObject queryCacheObject = SXQueryCacheObject.findByExperimentID(report.getPromtID()); %>
			<table width="100%">
			<% if (queryCacheObject != null && queryCacheObject.getNumberofScannedSamples() > 0){ %>
				<%@ include file="incl_processbar.jsp"%> 
		    <%} %>
			</table>
			</td>
			<td ></td>
		</tr>
		

	
	<% if (!user.isLabTechnician()){ %>
	
	<%} else { %>	
	
	<%} %>
	
	<tr>
		<td>
		<td align="center" colspan="21">
		<table cellpadding='0'>
	<% if (user.isLabTechnician()){ %>
		<tr>
			<td ></td>
			<td width="0">Share Experiment</td>
			<td colspan="4" style="background-color:#FFFFFF"><a href="usr_share_exp.jsp?id=<%=report.getPromtID()%>"><img  border='0' src="pics/go_share.gif"></a></td>
			<td ></td>
		</tr>
	<%} %>


		<tr>
			<td ></td>
			<td width="0">Edit Experiment</td>
			<td colspan="4" style="background-color:#FFFFFF"><a href="load?id=<%=report.getPromtID()%>&action=40"><img  border='0' src="pics/go.gif"></a></td>
			<td ></td>
		</tr>

		<tr>
			<td ></td>
			<td width="0">Download Result</td>
			<td align="left" colspan="4" style="background-color:#FFFFFF">
				<% if(org.setupx.repository.core.communication.binbase.BBConnector.determineBBResultsArrived(report.getPromtID())){%> 
					<a href="load?id=<%=report.getPromtID()%>&action=<%=org.setupx.repository.core.user.access.PromtUserAccessRight.DOWNLOAD_RESULT%>"><img  border='0' src="pics/go_download.gif"></a>
				<% } else {%>
					<img  border='0' src="pics/go_blank.gif">			
				<% } %>
			</td>
			<td ></td>
		</tr>

			<% try{
			    PromtUserAccessRight.checkAccess(user.getUOID(), report.getPromtID(), PromtUserAccessRight.EXPORT);
			    %>

			<tr>
				<td ></td>
				<td width="0">Export Experiment</td>
				<td colspan="4" style="background-color:#FFFFFF">
				<a href="export_selection.jsp?id=<%=report.getPromtID()%>">
				<img  border='0' src="pics/go_export.gif"></a></td>
				<td ></td>
			</tr>

			<% 
			}catch (Exception e)			    {%> 
			<tr>
				<td ></td>
				<td width="0">Export Experiment</td>
				<td colspan="4" style="background-color:#FFFFFF"><a href="load?id=<%=report.getPromtID()%>&action=40"><img  border='0' src="pics/go_locked.gif"></a></td>
				<td ></td>
			</tr>
			<%}%>

			<tr>
				<td ></td>
				<td width="0">List View</td>
					<td colspan="4" style="background-color:#FFFFFF"><a href="experiment_small_list.jsp?id=<%=report.getPromtID()%>"><img  border='0' src="pics/go_report.gif"></a></td>
				<td ></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td ></td>
		<th colspan="5">Classes / Samples</th>
		<td ></td>
	</tr>
	
	<!--  comment field -->
	<!--  show information and a field to add comments -->


	
	<!--  each class -->
	<%  
	java.util.Iterator iterator = clazzIDs.iterator();
    int i = 0;
	while (iterator.hasNext()){
	    i++;
		long clazzID = Long.parseLong("" + iterator.next());%>
		<!-- clazz: <%=clazzID%> -->
		
	<tr>
		<td ></td>
		<th colspan="5"></th>
		<td ></td>
	</tr>
		<tr>
			<td ></td>
			<th ><%=clazzID%>  <font size="-2"><%=i%>/<%=clazzIDs.size() %></font></th>
			<td colspan="4">


			
			<table>
			<% 
		java.util.Iterator iterator1 = new SXQuery().findLabelsforClazz(clazzID).iterator();
		while (iterator1.hasNext()) {
		  Object[] element = (Object[]) iterator1.next();
		%>
			<tr>
				<td></td>
				<td align="right"><font class="small"><b><%=element[0].toString()%></b></font></td>
				<td align="left"><font class="small"><%=element[1].toString()%></font></td>
				<td></td>
				<td></td>
			</tr>
		
		  <%
		}
		%>
		</table>
	
			
			</td>
			<td></td>
		</tr>
		
		
		
<% 
List sampleIDs = new SXQuery().findSampleIDsByClazzID((int)clazzID);
%> 

<tr>
<td ></td>
<th >number of samples</th>
<td align="left" colspan="4"><%=sampleIDs.size() %></td>
<td ></td>
</tr>

<% 

java.util.Iterator samples = sampleIDs.iterator();
int j = 0;
int counter = 0;
final int maxNumber = 4;
while (samples.hasNext() && counter < maxNumber){
    	counter++;
    	long sampleID = Long.parseLong("" + samples.next());
%>
		<!--  each sample -->		
		<tr>
			<td ></td>
			<td align="right"><%=sampleID%></td>
			<td style="background-color:#FFFFFF" width="2">
			</td>
			<td align="center" style="background-color:#FFFFFF"><a href="sample_detail.jsp?id=<%=sampleID%>"><img  border='0' src="pics/details.gif"></a></td>
			<td style="background-color:#FFFFFF">
				<% try { %>
					<a href="sample_detail.jsp?id=<%=sampleID%>"><%=new SXQuery().findSampleLabelBySampleID(sampleID) %></a>
				<% } catch (PersistenceActionFindException e){%>
					----
				<% }%>
			</td>
			<td style="background-color:#FFFFFF">
			</td>
			<td ></td>
		</tr>
	<% } 
	if (samples.hasNext()){
	%>

		<tr>
			<td ></td>
			<td align="right">...</td>
			<td style="background-color:#FFFFFF" width="2">
			</td>
			<td align="center" style="background-color:#FFFFFF"></td>
			<td style="background-color:#FFFFFF">There is <%=(sampleIDs.size() - maxNumber) %> more samples in this class. <a href="report.jsp">Click to <img  border='0' src="pics/details.gif"></a> the receive all samples/information.
			</td>
			<td style="background-color:#FFFFFF">
			</td>
			<td ></td>
		</tr>
	<% } %>

	<!--  end of samples -->  
	<% } %>

	<!--  back to the menu -->
	<tr>
		<td colspan="1" align="right" style="background-color:#FFFFFF"><a href="main.jsp"><img  border='0' src="pics/back.gif"></td>
		<td colspan="5"></td>
		<td colspan="1"></td>
	</tr>




</table>
</body>





<%@ include file="footer.jsp"%>
