<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@ include file="checknonlogin.jsp"%>
<%
long id = Long.parseLong(request.getParameter("id"));
PromtUserAccessRight accessRight = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID(), id);
if (accessRight.getAccessCode() >= accessRight.READ){
%>
<%=Promt.load(id,true).createXML().content()%>
<%
} else {
%>

Insufficient permissions to access this experiment.

<%}%>