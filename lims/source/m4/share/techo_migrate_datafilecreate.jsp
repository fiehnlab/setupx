<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.communication.importing.logfile.ScannedPair"%>
<%@page import="org.setupx.repository.core.communication.binbase.BBConnector"%>
<%@page import="org.setupx.repository.core.communication.file.SampleFile"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Datafile"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page import="org.setupx.repository.core.communication.importing.metadata.Data"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"%>
<table border="1">
	<tr>
		<th>
		<th>
		<th>all
		<th>CDF
		<th>TXT
		<th>PEG
		<th>SMP
		<th>datafile
		<th>
<%

	//techno_migrate.jsp
	
	
	Session hqlSession = CoreObject.createSession();
	
	Iterator iterator = hqlSession.createSQLQuery("select * from scanned_samples where sampleID != \"\" order by uoid desc limit 20").addEntity(ScannedPair.class).list().iterator();
	
	while(iterator.hasNext()){
	    try {
	        ScannedPair scannedPair = (ScannedPair)iterator.next();
	        SampleFile sampleFile = new SampleFile(scannedPair.getLabel());
	        %>
	        <tr>
	        	<td>
	        		<%=scannedPair.getSampleID()%>
	        	<td>
			        <%=scannedPair.getLabel()%>
	        	<td>
	        		<%
	        		if (!sampleFile.existsAll()){
	        		 	%>
	        		 	<img src="pics/blank.jpg">
	        		 	<%   
	        		} else {
	        		 	%>
	        		 	<img src="pics/checked.jpg">
	        		 	<%   
	        		}
	        		%>
	        	<td>
	        		<%
	        		short type = SampleFile.TXT;
	        		
        		    File file = sampleFile.getFile(type);
        		    if (file.exists()){
        		        // get datafiletype
        		        try {
        		            Session _prSession = TechnoCoreObject.createSession();
							TechnologyFileType technologyFileType = (TechnologyFileType)_prSession.createSQLQuery("select * from technologyfiletype where pattern like \"%" + sampleFile.getExtension(type) + "\"").addEntity(TechnologyFileType.class).list().get(0);
							Datafile datafile = Datafile.create(Long.parseLong(scannedPair.getSampleID()), file);
							technologyFileType.addDatafile(datafile);
							technologyFileType.getTechnology().updatingMyself(_prSession, true);
        		        } catch (Exception e){
        		            e.printStackTrace();
        		        }
        		    }
	        		%> 
	        	
		        <%
	    } catch (Exception e){
	        e.printStackTrace();
	    }
	}
%>