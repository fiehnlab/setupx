<%@ include file="checknonlogin.jsp"%>

<%
	int promtID = (Integer.parseInt(request.getParameter("id") + ""));
%>

<%@ include file="pub_nav.jsp"%>


<%@page import="java.util.List"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>


<%@page import="java.util.HashSet"%>
<table width="700" align="center" >

<tr>
<td align="center"><a href="http://fiehnlab.ucdavis.edu"><img src='pics/uc_logo.gif' border="0" valign="middle" align="center"></a></td>
<tr><td colspan="21" align="center"><font face="Verdana"><hr>
<tr align="center">
	<td>
		<strong>
			<font style="font-variant: small-caps;">
				<%=new SXQuery().findPromtTitleByPromtID(promtID) %>
			</font>
		</strong>
		<a href="pubexperiment.jsp?id=<%=promtID%>"><img src="pics/details.gif" border="0"></a>
<tr align="center"><td>
<%
	HashSet users = new SXQuery().findPromtCollaboration(promtID);

	Iterator iterator = users.iterator();
	while(iterator.hasNext()){
	    UserDO userDO = (UserDO)iterator.next();
	    if (userDO.getUsername().compareTo("public") != 0){
	    %>
		    <a href="mailto:<%=userDO.getMailAddress().getEmailAddress()%>">
		    	<img src='pics/mail.gif' border="0" valign="middle" align="center">
		    </a><%=userDO.getDisplayName()%>, <%
	    }
	}
%>

<tr align="center"><td><font size="-1">
<%
	iterator = users.iterator();
	while(iterator.hasNext()){
	    UserDO userDO = (UserDO)iterator.next();
	    %><%=userDO.getOrganisation()%>, <%
	}
%>

<tr align="center"><td><font size="-1">Genome Center

<tr align="center"><td><font size="-1">451 E. Health Sci. Drive

<tr align="center"><td><font size="-1">Davis, California 95616, USA

<tr align="center"><td><font style="font-family: sans-serif; font-size: x-small">Summary created by <%=org.setupx.repository.Config.SYSTEM_NAME%>. This report has been created based on the information store in the system.
</font>
<hr>

<tr>
	<td colspan="2">
		<font style="font-family: sans-serif">The following compounds were found in the study. Each data point represents the average of a class.</font>
	</td>
</tr>	 
<tr>
	<td colspan="2">
		<hr>

<%

int width = 0;
String tmpName = "";
String compoundName = "";
String values = "chd=t:";
String labels = "chl=";

width = 50 * new SXQuery().findClazzIDsByPromtID(promtID).size();


String myQuery = " select sample.PARENT as c, pAtt.label as l, avg(pAtt.value) as v from pubsample as pSample, pubattribute as pAtt, formobject as sample where pAtt.parent = pSample.uoid "
	+ " and sample.uoid = pSample.setupXsampleID  and sample.parent in ( " ;
	        
Iterator classIDs = new SXQuery().findClazzIDsByPromtID(promtID).iterator();
	
while (classIDs.hasNext()){
	// 343626, 343651, 343676, 343701, 343726, 343751 
    myQuery = myQuery.concat("" + classIDs.next());
	if (classIDs.hasNext()){
	    myQuery = myQuery.concat(",");
	}
}
	        
myQuery = myQuery.concat(") group by label, sample.PARENT");

Iterator result = CoreObject.createSession().createSQLQuery(myQuery)
	.addScalar("c",Hibernate.INTEGER)
	.addScalar("l",Hibernate.STRING)
	.addScalar("v",Hibernate.INTEGER)
	.list()
	.iterator();

int start = 0;
while(result.hasNext()){
 
    Object[] objects = (Object[])result.next();
    compoundName = objects[1] + "";
    
    
    if (compoundName.compareTo(tmpName) != 0){
        values = values.substring(0, values.length()-1);
        labels = labels.substring(0, labels.length()-1);

        if (tmpName.length() > 1){
            if (width > 1000) width = 1000;
        %>
      	<tr>
    		<td colspan="2">
    			<img src="http://chart.apis.google.com/chart?cht=lc&chs=<%=width %>x220&chco=0000ff&<%=values%>&chtt=<%=tmpName%>&<%=labels%>">
      	<tr>
    		<td colspan="2"><font>&nbsp;</font>
    		<%
        }
        tmpName = compoundName;
        start = Integer.parseInt("" + objects[2]);
		values = "chd=t:";
		labels = "chl=";
		
    }
    %>
	   	<%
	    int tmpValue = Integer.parseInt(objects[2] + "");
	    
	    values = values.concat("" + (tmpValue * 50 / start));
	    values = values.concat(",");

	    labels = labels.concat("" + objects[0]);
	    labels = labels.concat("|");
	    %>
	</tr>
		
<%
}
%>