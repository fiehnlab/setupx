<%@page import="org.setupx.repository.Settings"%>
<%@page import="org.setupx.repository.core.communication.ontology.obo.OBOConnectorPool"%>
<%@page import="org.setupx.repository.core.communication.ontology.obo.OBOConnector"%>
<%@page import="org.setupx.repository.web.forms.restriction.RestrictionOBO_human_anatomy"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.AnimalOrangInputfield"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.HumanOriginInputfield"%>
<%@page import="org.setupx.repository.web.forms.restriction.OrganLexiRestriction"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.communication.ontology.obo.OBOTerm"%>


<script language="JavaScript" src="overlib.js"></script>



<%
	String _XA_query = "select SUBSTRING_INDEX(lower(organ.value), ' ', 2) as term from "
				+ "  formobject as organ where organ.msiattribute = 4 and organ.value != \"-total-\" and organ.value != \"dummy\" and organ.value != \"yxadsfasdf\"  "
				+ " and organ.value != \"total\" and organ.value != \"\" and organ.active = true group by SUBSTRING_INDEX(lower(organ.value), ' ', 1) asc order by RAND() limit 30";
	
	
	Iterator _XA_iter = CoreObject.createSession().createSQLQuery(_XA_query).addScalar("term", Hibernate.STRING).list().iterator();

	
	OBOConnector connHuman = null;
	OBOConnector connPlant = null;
	OBOConnector connAnim = null;
	try {
		 connHuman = OBOConnector.newInstance(new java.io.File(Settings.getValue("data.obo.humananatomy")));
	} catch (Exception e){
	    
	}
	try {
		 connPlant = OBOConnector.newInstance(new java.io.File(Settings.getValue("data.obo.plantanatomy")));
	} catch (Exception e){
	    
	}
	try {
		 connAnim = OBOConnector.newInstance(new java.io.File(Settings.getValue("data.obo.mouse_anatomy")));
	} catch (Exception e){
	    
	}
	
	
	
	
	while (_XA_iter.hasNext()){
	    String term = _XA_iter.next().toString();
		OBOTerm oboTerm = null;
	    
	    try {
	        oboTerm = connAnim.determineTerm(term);
		} catch (Exception e){
		    
		}
	    try {
	        oboTerm = connHuman.determineTerm(term);
		} catch (Exception e){
		    
		}
	    try {
	        oboTerm = connPlant.determineTerm(term);
		} catch (Exception e){
		    
		}
		
	    %>
	    <%
		if (oboTerm != null && oboTerm.getDefinition().length() > 2){
		    %>
		    <a href="javascript:void(0);" onmouseover="return overlib('<%=oboTerm.getDefinition()%>');" onmouseout="return nd();"><%=term%></a>, 
		    <%
		} else {
		    %>
		    <%=term%>, 
		    <%
		}
	}
	
 %>
... 