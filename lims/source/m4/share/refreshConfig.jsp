<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="javax.management.MBeanServerConnection"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="javax.management.MBeanInfo"%>
<%@page import="javax.management.ObjectName"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

<%

	try {
		//for connecting to JBoss...
		
		%>... establishing connection.<%
		// jboss.web.deployment
		

		javax.naming.InitialContext ic = new javax.naming.InitialContext();
		
		
		%><%=ic.list("jboss.web.deployment") %><%
		
		javax.management.MBeanServer mbeanServer =(javax.management.MBeanServer)ic.lookup("java:Catalina");
		%><%=mbeanServer %><%

		MBeanServerConnection mBeanServerConn=(MBeanServerConnection)new InitialContext().lookup("jmx/rmi/RMIAdaptor");
		%><%=mBeanServerConn %><%
		//for getting the ObjectName of 'MainDeployer' MBean...
		
		ObjectName objectName = new ObjectName("jboss.system:service=MainDeployer");
		%><%=objectName %><%
		MBeanInfo beanInfo=mBeanServerConn.getMBeanInfo(objectName);
		%><%=beanInfo %><%
		//for deploying application...its working fine and myServlet.war is getting deployed..

		// Object arguments[]={"\\D:\\jboss examples\\myServlet\\myServlet.war"};
		// String signatures[]={"java.lang.String"};
		// mBeanServerConn.invoke(objectName, "deploy",arguments,signatures);
		
		//for undeploying application...here its giving java.net.MalformedURLException..
		
		Object arguments[] = {"myServlet.war"};
		String signatures[] = {"java.lang.String"};
		mBeanServerConn.invoke(objectName, "undeploy",arguments,signatures);	
	} catch (Exception e){
		%><%=e.getLocalizedMessage() %><%
	    e.printStackTrace();
	}

%>
</body>
</html>