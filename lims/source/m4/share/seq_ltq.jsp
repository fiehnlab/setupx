<%@page import="org.setupx.repository.Config"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

`<%@page import="java.util.List"%>
<%@page import="java.io.File"%>
<%

// JSPs to generate seq for LTQ

// select method used for the samples 
File[] allMethod = new File[0];
try {
    allMethod =  new File(Config.DIRECTORY_LTQ_METHODS).listFiles(); 
} catch (Exception e){
    e.printStackTrace();
}

%>



<form action="seq_ltq_confirm.jsp" method="post">

<table align="center" width="600" cellpadding="5">
	<tr>
		<td>
			<table>
				<tr>
					<th>Exit</th>
					<th width="90%">&nbsp;</th>
					<th>Administration</th>
				</tr>
				<tr>
					<td style="background-color:#FFFFFF" align="center"><a href="login.jsp"><img src='pics/go_exit.gif' border="0" valign="middle" align="center"></a></td>
					<td style="background-color:#FFFFFF" align="center"></a></td>
					<td style="background-color:#FFFFFF" align="center"><a href="admin.jsp"><img src='pics/go.gif' border="0" valign="middle" align="center"></a></td>
				</tr>
			</table>					
	<tr>
		<th>
			<h1>Generate Sequence for LTQ</h1>
	<tr>	
		<td>
			<h2>Default Method</h2>
			Please select the Methods that will be used for <b>most</b> of the samples.
			<br>
			<select name="method">
			
			<% for (int x = 0; allMethod != null && x < allMethod.length; x++){ %>
				<option value="<%=allMethod[x].getName() %>"><%=allMethod[x].getName() %></option>
			<% } %>
	
			</select>
	<tr>	
		<td>
			<h2>Experiment ID</h2>
			Please enter the sampleID of the experiment that contains the samples you will be running.
			<br>
			<input type="text" name="id"/>
	<tr>	
		<td>
			<h2>Filter</h2>
			Please enter any set of characters that is part of the sample label - only those will be preselected for you.
			<br>
			<input type="text" name="filter"/>
	<tr>	
		<td>
			<input type="submit"> 
</form>

