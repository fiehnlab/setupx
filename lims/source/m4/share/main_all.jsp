<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 org.setupx.repository.core.communication.binbase.BBConnector,
                 java.util.List,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.util.logging.Logger,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="java.util.HashSet"%>
<%@page import="org.setupx.repository.web.forms.PromtCreator"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBI_Classifier"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector"%>
<%@page import="org.setupx.repository.core.communication.binbase.BinBaseUtil"%>
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<% int sampleCounter = 0;
int studyCounter = 0;%>


<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4'>

	<!-- each of my experiment -->
	<% 
	org.hibernate.Session hqlSession = 	org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

	List ids = new SXQuery().findPromtIDs();
	//List ids = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID());
	java.util.Iterator iter= ids.iterator();
	
    PromtUserAccessRight accessRight = new PromtUserAccessRight();
    int xss = 0;
	while(iter.hasNext() ){
	    xss++;
	    
	    // temp accessright - not persistent
	    accessRight.setUOID(user.getUOID());
	    accessRight.setExperimentID(Long.parseLong(iter.next() + ""));
	    accessRight.setAccessCode(99);
	    
		//PromtUserAccessRight accessRight = (PromtUserAccessRight)iter.next();
		
		Logger.log(this, accessRight.getExperimentID() + " start");
		HashSet otherUserHashset = new HashSet();

		long promtID = accessRight.getExperimentID();
		// number of samples finished 
		int samplesTotal = new SXQuery().findSampleIDsByPromtID(promtID).size();
		
		
		int classes = new SXQuery().findClazzIDsByPromtID(promtID).size();

		
		if (accessRight.getAccessCode() != 0  && samplesTotal != 6 && classes != 1) {
		    
	
			boolean resultFileExists = BBConnector.determineBBResultsArrived(promtID);
			Logger.log(this, accessRight.getExperimentID() + " exists: " + resultFileExists);			
			
			// number of all samples already that already ran.
			int numberOfScannedSamples = 0;
			
			//new: deactivate unnessary queries
			if (resultFileExists){
			    numberOfScannedSamples = samplesTotal;
			} else {
				Logger.log(this, accessRight.getExperimentID() + " looking for scanned samples");
			    numberOfScannedSamples = new SXQuery().determineSamplesFinishedByPromtID(accessRight.getExperimentID());
				Logger.log(this, accessRight.getExperimentID() + " looking for scanned samples done");
			}
			boolean scheduled = (numberOfScannedSamples != 0);

			int samplesFinished = numberOfScannedSamples;//new SXQuery().determineSamplesFinishedByPromtID(promtID);
			
			boolean running = (samplesFinished > 0); // new SXQuery().determinePromtRunStarted(promtID);

			boolean finished  = (samplesFinished == samplesTotal);//new SXQuery().determinePromtFinished(promtID);
			
			Logger.log(this, accessRight.getExperimentID() + " looking for user: ");

		%>
		
		<% if (user.isMasterUser(user.getUOID()) && finished && samplesFinished > 0	){ %>
		<tr>
		<td colspan="1"></td>

		<!-- lable and ID -->
		<th align="justify">
			<b><font style="color:#6682B6"> ID:<%=promtID%></font></b>

		<!-- finished runs? -->
		<td>
			<% if (new SXQuery().determinePromtFinished(promtID)){ %>
				<img  border='0' src="pics/check.gif">
			<% } else {%>
				<img  border='0' src="pics/circle.gif">
			<% }  %>

		<td>
			<% if (BBConnector.determineBBResultsArrived(promtID)){ %>
				<img  border='0' src="pics/check.gif">
			<% } else {%>
				<img  border='0' src="pics/circle.gif">
			<% }  %>
		
		
		
		<td><%=new SXQuery().findPromtTitleByPromtID(promtID)%>

			 
			 
		</th>

		<!-- number of samples -->
		<td align="center" style="background-color:#FFFFFF">
				<%
				
				
				%>
		
				<%=samplesTotal %></td>
		
		
		

		<!-- number of classes -->
		<td align="center" style="background-color:#FFFFFF"><%=classes %></td>



		<td>
		<%
			int ncbiID = 32644;
		
			try {
				String query = "select speciesinfo.value as s from formobject as page, formobject as ncbi, formobject as biosource, formobject as bioinfo, formobject as speciesinfo where ncbi.discriminator = " +
					"\"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\" and ncbi.PARENT = biosource.uoid and bioinfo.PARENT = ncbi.PARENT " + 
					" and bioinfo.active = true and bioinfo.value != \"\" and biosource.PARENT = page.uoid and page.parent = " + promtID + " and speciesinfo.PARENT = ncbi.uoid " + 
					" and speciesinfo.active = true";
				Logger.debug(this, query);
				Iterator iteratorX = hqlSession.createSQLQuery(query).addScalar("s", Hibernate.STRING).list().iterator();
				ncbiID = NCBIConnector.determineNCBI_Id(iteratorX.next().toString());
			} catch (Exception e){
			    e.printStackTrace();
			}

			
			if (ncbiID == 32644) {
			    String strings = new SXQuery().findSpeciesbyPromtID(promtID).get(0).toString();
			    ncbiID  = NCBIConnector.determineNCBI_Id(strings); 
			}
			NCBIEntry entry = NCBIConnector.determineNCBI_Information(ncbiID);
			
		%>
		<td>
		<%= entry.getName()%>
		<td>
		<%= entry.getRank()%>
		<td style="background-color: <%=Util.getColor(entry.getSpeciesType() * 2) %>">
		<%= entry.getSpeciesTypeName()%>
		<td style="background-color: <%=Util.getColor(entry.getSpeciesType() * 2) %>">
		<%= entry.getSpeciesType() %>
		
		
		<td>
		<%= entry.getTaxID()%>
		<td>
		<font class = "small">
		<%try {  %>
			<%= entry.getSplitpointParent().getName()%>
		<%} catch (Exception exception){ %>
		---
		<%} %>
		</font>
		
		
		<%
		
		/*
		
			studyCounter = studyCounter+1;
			sampleCounter = sampleCounter+samplesFinished ;
		%>
		<td><%=sampleCounter%>
		<td><%=studyCounter %>
		
		<% */ %>
		<td><%=new SXQuery().findPromtAbstractByPromtID(promtID)%>
	</tr>
	
	<%		}
		} // end of 
	} // end for experiment 
	hqlSession.close();
	
	%>
