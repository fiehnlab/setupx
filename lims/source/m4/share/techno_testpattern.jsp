<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="java.io.FilenameFilter"%>
<%@page import="java.io.File"%>
<%
File folder = new File("/mnt/binbase/data/");

//String patternString = request.getParameter("pattern");

File[] fileArray = folder.listFiles(new FilenameFilter(){
    public boolean accept(File dir, String name) {
        try{
            
            String patternString = "(.{6})([abz])([a-z]{2})sa(.{1,4})"; // (:)(.*)
            Logger.debug(this, "checking " + name + " vs. " + patternString);
            Pattern pattern = Pattern.compile(patternString);
            Matcher m = pattern.matcher(name);
            return m.matches();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
});


for(int i = 0; i<fileArray.length; i++){
    %><%=fileArray[i]%><br><%
    
}
%>