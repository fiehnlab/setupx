
<%
//  JSP creating a JSON response for ajax requests
%>

<%@ include file="checknonlogin.jsp"%>

<%@page import="org.setupx.repository.core.query.QueryMasterAnswer"%>
<%@page import="org.setupx.repository.core.query.QueryMaster"%>
<%@page import="org.hibernate.criterion.Expression"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.setupx.repository.web.pub.data.cache.VariationsCache"%>
<%@page import="java.util.Vector"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%
            //Session s = CoreObject.createSession();
            Enumeration e = request.getParameterNames();
            int y = 0;
            while (e.hasMoreElements()) {
                y++;
                Logger.debug(this, "Name of Param " + y + " : "
                        + (String) e.nextElement());
            }
            Logger.debug(this, "Total Parameters : " + y);

            Logger.debug(this, "start receiving parameters.");
            String searchTerm = request.getParameter("input").toString();
            Logger.debug(this, "input:" + searchTerm);
            int type = Integer
                    .parseInt(request.getParameter("type").toString());
            Logger.debug(this, "type:" + type);

            List list = new QueryMaster().search(user, type, searchTerm);
            
			if (list.size() > 13){
			    Logger.log(this, "results are limited to 12 !!! ");
	            list = list.subList(0, 12);
			}
%>


{ results: [
<%
            Logger.log(this, "found : " + list.size() + " final results	.");

            for (int i = 0; i < list.size(); i++) {
                QueryMasterAnswer answer = (QueryMasterAnswer) list.get(i);
%>
	{ id: "<%=i%>", value: "<%=answer.getLabel()%>", info: "<%=answer.getInfo1()%> <%=answer.getInfo2()%> <%=answer.getInfo3()%>" },
<%
 }
 %>
] }