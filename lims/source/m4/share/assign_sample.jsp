<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.communication.importing.metadata.*,
                 org.setupx.repository.server.persistence.*"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ include file="incl_setaccessright.jsp"%>

	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="./admin.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Util: Assign "virtual" Run (Beta) </h2>
			Tool marks sample that was entered in <%=org.setupx.repository.Config.SYSTEM_NAME%> as finished. <br/>
			<font size="-2"></font>
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>
	
	
	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} %>
	
	<table align="center" width="60%" >	
	<tr>
		<td colspan="2" align="center" style="background-color:#FFFFFF"><a href="admin.jsp"><img  border='0' src="pics/back.gif"></a></td>	
		<th colspan="20">ExperimentID</th>
	</tr>
	
	and the filename
	<form action="assign_sample2.jsp">
	<tr>
		<td colspan="2"></td>
		<td>
			Please enter the sampleID 
		</td>
		<td align="center" style="background-color:#FFFFFF">
			<input type="text" name="sampleID" id="sampleID"><br><font class=small>must be the correct sample ID from <%=org.setupx.repository.Config.SYSTEM_NAME%> !</font> 
		</td>
		
		<td align="center" style="background-color:#FFFFFF">
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td>
			Please enter the filename
			
		</td>
		<td align="center" style="background-color:#FFFFFF">
			<input type="text" name="filename" id="filename" value="060731abrsa425_1"><br><font class=small>this is an example - change it to the real name</font> 
		</td>
		
		<td align="center" style="background-color:#FFFFFF">
			<input type="image" src="pics/go.gif"></a>
		</td>
		<td>
		</td>
	</tr>
	</form>
</table>


<%@ include file="footer.jsp"%>
