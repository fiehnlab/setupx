<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net">
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Datafile"/>
<jsp:directive.page import="java.io.File"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Location"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Technology"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"/>
  <jsp:directive.page contentType="text/html; charset=UTF-8" />
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.hibernate.Hibernate"/>
<jsp:directive.page import="org.setupx.repository.core.CoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.query.QueryMaster"/>
<jsp:directive.page import="org.setupx.repository.server.persistence.SXQuery"/>
<jsp:directive.page import="java.util.Vector"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>

	<jsp:include page="techno_header.jsp"></jsp:include>


	<jsp:scriptlet>
		char qMark = (char)38; // ascii 38 

		long id = Long.parseLong(request.getParameter("id"));
		TechnologyFileType technologyFileType = (TechnologyFileType)TechnoCoreObject.persistence_loadByID(TechnologyFileType.class, CoreObject.createSession(), id);
		
	</jsp:scriptlet>

	<h2>
		<a href="techno_techno.jsp">all platforms</a> - 
		
		<![CDATA[<a href="techno_technology.jsp?id=]]>
		<jsp:expression>technologyFileType.getTechnology().getUOID()</jsp:expression>
		<![CDATA[">]]>
		<jsp:expression>technologyFileType.getTechnology().getLabel()</jsp:expression>
		<![CDATA[</a>]]>	
		- 
		<jsp:expression>technologyFileType.getLabel()</jsp:expression>
	</h2>
	<jsp:expression>technologyFileType.getDescribtion()</jsp:expression>
	
	
	<h3>
	files
	</h3>
	The following files of the Type "<jsp:expression>technologyFileType.getLabel()</jsp:expression>" are assigned to files in the System. 
	
	
	<jsp:scriptlet>
	request.setAttribute("test", technologyFileType.getDatafiles()); 
	// style
	pageContext.setAttribute("tableclass", "its");
	</jsp:scriptlet>
	
	
	<jsp:include page="inc_techno_datafile.jsp" />
	

	<form action="techno_datafile_upload.jsp" method="post" enctype="multipart/form-data" >
		<table>
			<tr>
				<th colspan="2">upload datafile</th>
			</tr>
			<tr>
				<td colspan="2">If you do have a datafile on <b>your local machine</b> that you want to bring into the SetupX repository you can do this here. Select the file that you want to upload and assign to the sample by specifying the sampleID. The system will save your uploaded file under a new name. This name will contain the sample ID an underscore and the original filename.</td>
			</tr>
			<tr>
				<td>File</td>
				<td><input type="file" name="file"></input></td>
			</tr>
			<tr>
				<td>Sample ID</td> 
				<td><input type="text" name="sampleid"/></td> 
			</tr>
			<tr>
				<td>Target</td> 
				<td>
				<select name="path">
				<jsp:scriptlet>
			        Iterator iterator = technologyFileType.getDefaultLocations().iterator();
			        while (iterator.hasNext()) {
			            Location location = (Location) iterator.next();
				</jsp:scriptlet>
					<![CDATA[<option value=]]>"<jsp:expression>location.getPath()</jsp:expression>"<![CDATA[>]]>
					<jsp:expression>location.getPath()</jsp:expression>
					<![CDATA[</option>]]>
				<jsp:scriptlet>
			        }
				</jsp:scriptlet>
				</select>
				</td> 
			</tr>
			<tr>
				<td></td> 
				<td>
					<![CDATA[<input type="hidden" name="technofiletype" value=]]>"<jsp:expression>technologyFileType.getUOID()</jsp:expression>"<![CDATA[/>]]>
					<input type="submit" value="upload file"/>
				</td> 
			</tr>
		</table>
	</form>

	<form action="techno_datafile_assign.jsp" method="post">
		<table>
			<tr>
				<th colspan="2">assign datafile</th>
			</tr>
			<tr>
				<td colspan="2">Assign an file that exists on the server to a certain sample.</td>
			</tr>
			<tr>
				<td width="120">Path</td> 
				<td>
				<select name="path">
				<jsp:scriptlet>
			        Iterator iterator2 = technologyFileType.getDefaultLocations().iterator();
			        while (iterator2.hasNext()) {
			            Location location = (Location) iterator2.next();
				</jsp:scriptlet>
					<![CDATA[<option value=]]>"<jsp:expression>location.getPath()</jsp:expression>"<![CDATA[>]]>
					<jsp:expression>location.getPath()</jsp:expression>
					<![CDATA[</option>]]>
				<jsp:scriptlet>
			        }
				</jsp:scriptlet>
				</select>
				</td> 
			</tr>
			<tr>
				<td></td> 
				<td>
					<![CDATA[<input type="hidden" name="technofiletype" value=]]>"<jsp:expression>technologyFileType.getUOID()</jsp:expression>"<![CDATA[/>]]>
					<input type="submit" value="assign file"/>
				</td> 
			</tr>
		</table>
	</form>



	<form action="techno_datafile_searchpattern.jsp" method="post">
		<table>
			<tr>
				<th colspan="2">search datafiles by pattern</th>
			</tr>
			<tr>
				<td colspan="2">This will search all location specified for this filetype using this pattern and show all files that were found.</td>
			</tr>
			<tr>
				<td width="120">Pattern</td> 
				<td>
					<![CDATA[<input type="text" name="pattern" value=]]>"<jsp:expression>technologyFileType.getPattern()</jsp:expression>"<![CDATA[/>]]>
				</td> 
			</tr>
			<tr>
				<td></td> 
				<td>
					<![CDATA[<input type="hidden" name="tid" value=]]>"<jsp:expression>technologyFileType.getUOID()</jsp:expression>"<![CDATA[/>]]>
					<input type="submit" value="search files"/>
				</td> 
			</tr>
		</table>
	</form>


	<form action="techno_datafile_convert.jsp" method="post">
		<table>
			<tr>
				<th colspan="2">Update datafiles</th>
			</tr>
			<tr>
				<td colspan="2">Assigning datafiles based on existing "scannedPairs"</td>
			</tr>
			<tr>
				<td></td> 
				<td>
					<input type="submit" value="search files"/>
				</td> 
			</tr>
		</table>
	</form>




	<h3>
	location
	</h3>
	Every datafile has certain default locations where the datafiles are stored.
	The following list shows the locations where those files can be / are stored.<br/>
	Feel free to add additional locations if. In case you want the automated file scanned to associate 
	your datafiles automaticly to the your files make sure that the location where all the datafiles 
	are stored are listed below.

	<jsp:scriptlet>
	request.setAttribute("location", technologyFileType.getDefaultLocations()); 
	// style
	pageContext.setAttribute("tableclass", "its");
	</jsp:scriptlet>
	<display:table name="location">
		<display:column title="Path" property="path"  sortable="true" headerClass="sortable"></display:column>
	</display:table>
	<form action="techno_location_create.jsp">
	<table>
		<tr>
			<th colspan="2">
				Add additional Location where files (<jsp:expression>technologyFileType.getLabel()</jsp:expression>) are stored.
			</th>
		</tr>
		<tr>
			<td>Default location</td>
			<td><input type="text" name="location"/></td>
		</tr>
		<tr>
			<td>create</td>
			<td>
				<input type="submit" value="create location"/>
				<![CDATA[<input type="hidden" name="technologyFileType" value=]]>"<jsp:expression>technologyFileType.getUOID()</jsp:expression>"<![CDATA[/>]]>
			</td>
		</tr>
	</table>
	</form>



</jsp:root>


