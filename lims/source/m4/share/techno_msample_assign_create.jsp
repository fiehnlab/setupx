<%@page import="java.util.List"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"%>
<%@page import="java.util.Date"%>
<%@page import="java.io.FilenameFilter"%>
<%@page import="org.setupx.repository.core.util.FilenamePatternFilter"%>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
    <title>SetupX</title>
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>

<%
	Session s = TechnoCoreObject.createSession();

	String sampleID = request.getParameter("sampleID");
	String samplePattern = request.getParameter("pattern");

%>


<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Technology"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Location"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Datafile"%>



  <link rel="stylesheet" href="print.css" type="text/css" media="print" />
  
  </head>

	<jsp:include page="techno_header.jsp"></jsp:include>




	<h2>
		<a href="techno_techno.jsp">all platforms</a> - search for datafiles
	</h2>
	<p>
	Please enter the ID of the sample that you want to add additional datafiles to. 
	</p>
	<p>By specifying a part of the filename you can limit the number of listed
	files to a minimum which allows you to find the corret sample much faster.
	</p>
<table align="center">
<form action="techno_msample_assign_create.jsp">
		<tr>
			<th width="10"><nobr>Enter the sample IDs</nobr>
			<th align="left">Enter a part of the filename
		</tr>
		<tr>
			<td>
				<input name="sampleID" type="text" value="<%=sampleID%>"></input>
			</td>
			<td align="left">
				<input name="pattern" type="text" value="<%=samplePattern%>"></input>
			</td>
		</tr>
		<tr>
			<td><input type="submit"></td>
	</table>
</table>
</form>

	<h3>
		Search result
	</h3>

	<p>
	The list below shows all samples that match 
	<ul>
		<li>the Pattern of that specific filetype </li>
		<li>the filename contains the part that was defined in the search
	</ul>
	
	<p>
	The system preselects all the samples for you that 
	<ul>
		<li>are <b>not</b> assigned to another sample yet</li>
		<li>are not assigned to this sample
	</ul>

	<p>
	Please select the samples that you want to assign to the sample with the ID <b><%=sampleID %></b>. <br>All selected files will be assigned to your samples then. 


<form action="techno_msample_assign_create_process.jsp">
	<input type="hidden" value="<%=sampleID%>" name="sampleID"/>
<table align="center">
	<th colspan="21">
		Assign files to sample <b><%=sampleID %></b>.
	</th>
		
<%
	int fileC = 0;
	Iterator technologyIterator = TechnologyProvider.getTechnologies().iterator();
	while(technologyIterator.hasNext()){

	    // open every technology
	    Technology technology = (Technology)technologyIterator.next();
	    %>
	    
	    <tr>
    	<th colspan="21"><%=technology.getLabel() %>:</th>
    	<%
	    
	    Iterator technoFiletypeIterator = technology.getTechnologyFileTypes().iterator();
	    while(technoFiletypeIterator.hasNext()){

			// open every filetype
	        TechnologyFileType technologyFileType = (TechnologyFileType)technoFiletypeIterator.next();
	        %>
			<tr>
   			<td><img src="pics/aq_arrow.gif">
			<td colspan="21">filetype: <%=technologyFileType.getLabel() %> (filtered by pattern: <%=technologyFileType.getPattern() %>)<%
	        
			Iterator locationIterator = technologyFileType.getDefaultLocations().iterator();
	        while(locationIterator.hasNext()){
				
				// open every location
				Location location = (Location)locationIterator.next(); 
		        %>
				<tr>
				<td>
				<td>
				<td colspan="21">location: <%=location.getPath() %><%
				
				// check each file agains each samplePattern
				File[] files = new File(location.getPath()).listFiles(new FilenamePatternFilter(technologyFileType.getPattern()));
		        for (int i = 0; i < files.length; i++){
		            File file = files[i];
		            if (file.getName().indexOf(samplePattern) > -1){ // || file.getName().indexOf(sampleID) > -1){
		                
		                // check if the file has been assigned already
						String query1 = "select * from datafile where datafile.sampleID = " + sampleID + " and datafile.technologyfiletype = " + technologyFileType.getUOID() + " and source = \"" + file.getPath() + "\"";
						List assignedAlready = s.createSQLQuery(query1).addEntity(Datafile.class).list();
						
						String query2 = "select * from datafile where source = \"" + file.getPath() + "\"";
						List assignedOthers = s.createSQLQuery(query2).addEntity(Datafile.class).list();
		
						String checked = "checked=\"checked\"";
						if (assignedOthers.size() > 0){
						    checked = "";
						}
		                
				        %>
				        <tr width="2">
       					<td width="2">
       					<td width="2">
       					<td width="2" align="center" valign="middle">
       						<% if (file.isDirectory()){ %>
       							<img src="pics/circle.gif"> 
       						<%} else if (assignedAlready.size()>0){ %>
       							<img src="pics/check.gif"> 
       						<%} else { 
      	 						 fileC++;
    							%><input align="top" type="checkbox" <%=checked %> name="<%= sampleID + "_" + technologyFileType.getUOID() + "_" + fileC%>" value="<%=file.getPath()%>"/>
       						<%} %>
				        <td width="2"><b><%=file.getName()%></b>
       					<td width="2"><nobr><%=new Date(file.lastModified()).toLocaleString() %></nobr>
       					<td >
       						<%if (assignedAlready.size() > 0){
       						 %><nobr>File is assigned to <b>this</b> sample already.</nobr><%
       						} else if (assignedOthers.size() > 0){
       						 %><nobr>This file is assigned to <b><%=assignedOthers.size() %></b> samples already.</nobr><%
       						}
       					%>
       					<td colspan="21">
				        <%
		            }
		        }
	        }
	    }
	}
%>
</td>
</tr>
<tr>
	<td colspan="21">
		<input type="submit">
</table>
</form>