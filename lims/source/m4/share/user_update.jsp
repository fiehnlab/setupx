<%@ include file="checklogin.jsp"%>
<%@page import="org.setupx.repository.core.communication.mail.EMailAddress"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.user.UserLocal"%>
<%@page import="org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration"%>
<% 
long id ;
try {
     id = Long.parseLong(""+ request.getParameter("userID"));    
} catch (NumberFormatException exception){
    id = 0;
}
String username = request.getParameter("username");
String displayname = request.getParameter("displayName");
String password = request.getParameter("password");
String phone = request.getParameter("phone");
String orga = request.getParameter("orga");
try {
    Logger.log(this, "trying to load " + id);
    UserDO userDO = null;
    if (id != 0){
        Session session2 = PersistenceConfiguration.createSessionFactory().openSession();
        userDO = (UserLocal)UserLocal.persistence_loadByID(UserLocal.class, session2, id);
    } else {
        EMailAddress email = new EMailAddress(request.getParameter("mailAddress"));
        if (username != null && username.compareTo("null") != 0){
            Logger.log(this, "creating new User:" + username + " " + password);
            userDO = new UserLocal(username, password, email);
        } else {
            // ignoring - invalid request
        }
    }


    
    userDO.setDisplayName(displayname);
    userDO.setPhone(phone);
    userDO.setOrganisation(orga);
    userDO.setEmailString(request.getParameter("mailAddress"));
    
    userDO.update(true);
    
    org.setupx.repository.server.logging.Message message = new org.setupx.repository.server.logging.Message();
    message.setLabel("user created");
    message.setMessage(userDO.toString());
    message.update(true);
    
    session.setAttribute("user", userDO);
    
} catch (Exception e){
    e.printStackTrace();
}
%>



<jsp:forward page="main.jsp"/>