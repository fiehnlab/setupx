<%@page import="org.hibernate.Transaction"%>

<%@ include file="checklogin.jsp"%>

<%@page import="org.setupx.repository.core.communication.document.sop.StandardOperationProcedure"%>
<%@page import="java.util.Date"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="java.io.File"%>

<%
	File _tmp = File.createTempFile("xyz", ".tmp", null);
	MultipartRequest multiRequest = new MultipartRequest(request, _tmp.getParentFile().getAbsolutePath(), 10424162);

	Enumeration names = multiRequest.getParameterNames();
	while(names.hasMoreElements()){
		String key = "" + names.nextElement();
		String val = multiRequest.getParameter(key);
	    Logger.log(this, key + " " + val);
	}
	
	
	// sop_new.jsp?replaceid=1
	Session s = StandardOperationProcedure.createSession();
	long replaceID = Long.parseLong(multiRequest.getParameter("replaceid"));
	        
	// load old sop
	StandardOperationProcedure sopOld = StandardOperationProcedure.load(s, replaceID);

	// load category
	long catID = Long.parseLong(multiRequest.getParameter("catid"));
	DocumentCategory category = (DocumentCategory) DocumentCategory.persistence_loadByID(DocumentCategory.class, s, catID);
	
	// create a new SOP
	StandardOperationProcedure sopNew = new StandardOperationProcedure("",user, user, category);
	
	// prefill it with values from older one
	sopNew.setAreaValid(multiRequest.getParameter("areaValid"));
	sopNew.setComment(multiRequest.getParameter("comment"));
	sopNew.setContent(multiRequest.getParameter("content"));
	sopNew.setDateIssued(new Date());
	sopNew.setDefinedBy(user);
	sopNew.setLabel(multiRequest.getParameter("label"));
	sopNew.setModificationsToLastVersion(multiRequest.getParameter("setModificationsToLastVersion"));
	sopNew.setOrganisation(multiRequest.getParameter("organisation"));
	sopNew.setPubliclyAvailable(false);
	sopNew.setResponsible((UserDO)UserDO.persistence_loadByID(UserDO.class,s, Long.parseLong(multiRequest.getParameter("responsible"))));

	if (sopOld != null){
		// outdate the older one
		sopNew.setReplcaesDocument(sopOld);
	}
	sopNew.setVersion(multiRequest.getParameter("version"));

	
	// uploading file
	Session sess = CoreObject.createSession();
    Logger.log(this, "Saving uploaded files to " + _tmp.getParentFile().getAbsolutePath());

    _tmp.delete();
    Enumeration fileNames = multiRequest.getFileNames();

	while (fileNames.hasMoreElements()) {
      String filename = (String) fileNames.nextElement();
	  File file = multiRequest.getFile(filename);
	 
	  Logger.log(this, "uploaded file: " + file.getName() + "   " + file.getAbsolutePath());

      String localFilename = "/mnt/" + file.getName();

      File localfile = new File(localFilename);
      org.setupx.repository.core.util.File.copyFile(file, localfile);
	  Logger.log(this, "and a copied to " + localfile);
	  sopNew.setFilePath(localfile.getPath());
	}
	
	sopNew.save(s);
	
	Logger.log(this, "created new sop: " + sopNew);
	
%>

wrote new sop with id <%=sopNew.getUOID() %>.

<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.core.communication.document.DocumentCategory"%>
<meta http-equiv="refresh" content="0; URL=sop_detail.jsp?id=<%=sopNew.getUOID() %>">
