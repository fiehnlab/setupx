<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.web.DownloadServlet"%>
<%@page import="org.setupx.repository.core.communication.document.DocumentObject"%>
<%@page import="org.setupx.repository.core.communication.document.sop.StandardOperationProcedure"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.web.WebException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.OutputStream"%>
<%@page import="org.setupx.repository.ConfigurationException"%>
<%
    // request: sop_download.jsp?id=<%=sopID

    // get sopID & sop
    long sopID = Long.parseLong(request.getParameter("sopID"));
	DocumentObject documentObject = (DocumentObject)DocumentObject.persistence_loadByID(DocumentObject.class, sopID);

    // get filename
    String filename = documentObject.getFilePath();

    // create file
    File file = new File(filename);

    // check file
    try {
        org.setupx.repository.core.util.Util.checkFile(filename);
    } catch (ConfigurationException e) {
        throw new WebException("file is not available");
    }

    // return file


    // check if access allowed
	// public access checken

    // determine content type 
    String contentType = org.setupx.repository.core.util.File.determineContentType(file);

    // Set the headers.
//  response.setContentType(contentType);
	response.setHeader("Content-Disposition", "attachment; filename=" + file.getName().replaceAll(" ", "_"));

    // Send the file.
    try {
        DownloadServlet.returnFile(file, response.getOutputStream());
    } catch (IOException e1) {
        throw new WebException("unable to open file", e1);
    }
    
    // forward to detail view of sop
  

%>