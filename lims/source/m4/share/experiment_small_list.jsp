<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.communication.importing.logfile.*,
				 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 	org.setupx.repository.core.communication.leco.*,
                 	edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.*,
                 org.setupx.repository.server.persistence.*"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

</head>


<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width='90%'>
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Export</h2>
			Summary
			<br/>
			<font size="-2"></font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>



<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>



<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td></td>
		<th align="center" colspan="2">experiment</th>
		<th align="center" colspan="2" >class</th>		
		<th align="center" colspan="3" >sample</th>
	</tr>

<% 

final long promtID = Integer.parseInt(request.getParameter("id"));

final String query = "select promt.uoid as  experiment, clazz.uoid as classID, sample.uoid as sampleID, child5.value as label from formobject as promt, formobject as child5,  formobject as sample,  formobject as clazz,  formobject as child2,  formobject as page where promt.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.Promt\" and page.parent = promt.uoid and child2.parent = page.uoid and clazz.parent = child2.uoid and sample.parent = clazz.uoid and child5.parent = sample.uoid and child5.question = \"comment\" and sample.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.Sample\" and promt.uoid = \"" + promtID + "\"";  

org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

Iterator iter = hqlSession.createSQLQuery(query)
.addScalar("experiment",  org.hibernate.Hibernate.STRING)
.addScalar("classID",  org.hibernate.Hibernate.STRING)
.addScalar("sampleID",  org.hibernate.Hibernate.STRING)
.addScalar("label",  org.hibernate.Hibernate.STRING)
.list()
.iterator();


int i = 0;
String clazzz = "";
String desc = "";
int color = -1;
while(iter.hasNext()){
Object[] array = (Object[])iter.next();
%>

<% if (clazzz.compareTo(array[1]) != 0) {
    clazzz = (String)array[1];
    desc = new org.setupx.repository.core.ws.M1_BindingImpl().getClassInfos(clazzz);
	//desc = org.setupx.repository.core.util.Util.replace(desc, "\n", "<br>");
	color = color * -1;
%>
<%}

i++;
%>
	<tr>
		<td></td>
		<td align="center" <% if (color < 0) {%> style="background-color:#FFFFFF" <%} %>><b><%=array[0]%></b></td>
		<td align="center" style="background-color:#FFFFFF"><a href="load?id=<%=array[0]%>&action=20"><img  border='0' src="pics/details.gif"></a></td>
		<td align="center" <% if (color < 0) {%> style="background-color:#FFFFFF" <%} %>><%=array[1]%></td>
		<td align="center" <% if (color < 0) {%> style="background-color:#FFFFFF" <%} %>><font class="small"><%=desc%></font></td>
		<td align="center" <% if (color < 0) {%> style="background-color:#FFFFFF" <%} %>><%=array[2]%></td>
		<td align="center" <% if (color < 0) {%> style="background-color:#FFFFFF" <%} %>><b><%=array[3]%><b></td>
		<td align="center" style="background-color:#FFFFFF"><a href="sample_detail.jsp?id=<%=array[2]%>"><img  border='0' src="pics/details.gif"></a></td>
	</tr>
    <%
}
%>
</table>



<%@ include file="footer.jsp"%>
