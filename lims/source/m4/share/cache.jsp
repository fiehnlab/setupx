<%@ page contentType="text/html" %>
<%@ page import="org.setupx.repository.web.WebConstants"%>
<%@ page import="org.setupx.repository.web.QueryServlet"%>
<%@ page import="org.setupx.repository.web.forms.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.*"%>
<%@ page import="org.setupx.repository.web.forms.inputfield.multi.*"%>
<%@ page import="org.setupx.repository.web.forms.clazzes.*"%>
<%@ page import="org.setupx.repository.web.forms.page.*"%>
<%@ page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@ page import="org.setupx.repository.core.communication.exporting.*"%>
<%@ page import="org.setupx.repository.core.communication.ncbi.*"%>

<%@page import="org.setupx.repository.server.persistence.SXQueryCacheObject"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<%@ include file="checklogin.jsp"%>
<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%></h2>
			Cached Objects<br/>
			<font size="-2">Objects cached in the Database. In case you have results that are not up to date - refresh the cache by deleting the objects.</font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<%@ include file="navi_admin.jsp"%>


	<table align="center" width="60%" cellpadding="7">
	<tr>
		<td colspan="15"></td>
	</tr>
    	<tr>
    		<th align="center"></th>
    		<th align="center" colspan="14">Cached Objects
		</tr>
		<tr>
			<td colspan="15" align="center" width="60%">
				<table border="0" width="100%">


<%

org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();


// delete entries
try {
    long id = Integer.parseInt("" + request.getParameter("id"));
    org.setupx.repository.core.CoreObject.persistence_loadByID(  SXQueryCacheObject.class, hqlSession, id).remove();
    new SXQuery(hqlSession).determineSamplesFinishedByPromtID(id);
} catch (Exception e){
    e.printStackTrace();
}



%>


					<tr>
						<td ></td>
						<th ></th>
						<th ></th>
						<th ></th>
						<th >Samples cached</th>
						<th ></th>
						<th ></th>
						<td ></td>
					<tr>
	

				<% 
				java.util.Iterator iterator = SXQueryCacheObject.persistence_loadAll(SXQueryCacheObject.class, hqlSession).iterator();  
				SXQueryCacheObject queryCacheObject = null;
			    int col = 1;
				while (iterator.hasNext()){
					queryCacheObject = (	SXQueryCacheObject)iterator.next();
					
				    col = col * -1;
				    String bgcolor = "style=\"background-color:#FFFFFF\"";
				    if (col > 0) bgcolor = "";

					%>
					<tr>
						<td></td>
						<td <%=bgcolor %> align="center"><i><%=new SXQuery(hqlSession).findPromtTitleByPromtID(queryCacheObject.getExperimentID()) %></i></td>
						<td <%=bgcolor %> align="center"><font class="small"><%=new SXQuery(hqlSession).findPromtAbstractByPromtID(queryCacheObject.getExperimentID()) %></td>
						<td <%=bgcolor %> align="center"><i><%=queryCacheObject.getExperimentID() %></i></td>
						<td <%=bgcolor %> align="center"><%=queryCacheObject.getNumberofScannedSamples() %></td>
					    <td <%=bgcolor %> ><a href="?id=<%=queryCacheObject.getUOID()%>"><img  border='0' src="pics/trash.gif"></a></td>
						<td></td>
					</tr>
					<% 	
				}
				%>
			</td>
		</tr>
	</table>
	</table>
	
	
	<a href="cache_refresh.jsp">Refresh cache</a>
	




<%@ include file="../footer.jsp"%>
