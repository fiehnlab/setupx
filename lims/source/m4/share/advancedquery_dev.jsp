<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.query.QueryMasterAnswer"%>
<%@page import="org.setupx.repository.core.query.QueryMaster"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.util.Link"%>
<%@ include file="checknonlogin.jsp"%>
		<%@ include file="incl_searchtypes.jsp"%>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<% // include file="query_cache.jsp"%>

<%@page import="java.util.List"%>


<table align='center' border='0' cellpadding='0' cellspacing="0" width="60%">
<%
	boolean publicAvailable = true;


	String searchterm = request.getParameter("term");
	String variation = request.getParameter("var");
	String type = request.getParameter("type");

	if (type == null ||  type.compareTo("null") == 0) type = "99";
	
	// getting rid of starting and ending spacers
	boolean loop = true;
	try {
		while(loop){
		    // starting character
		    if (searchterm.charAt(0) == ' '){
		        searchterm = searchterm.substring(1);
		    } else if (searchterm.charAt(searchterm.length() -1) == ' '){
		        searchterm = searchterm.substring(0, searchterm.length()-1);
		    } else {
		        loop = false;
		    }
		}
	} catch (Exception e){
	    // in case the searchterm is ""
	}
	
	
	
	// THE ACTUAL SERACH
	Logger.log(this, "user: " + user);
	List listOfResults = new QueryMaster().search(user, Integer.parseInt(type), searchterm, variation);
	
	// determine which JSP to use for the results
    switch (listOfResults.size()) {
	    case 0:
	        Logger.debug(this, "didnt find any results");
	        %>
	        NO RESULTS
	        <br>
	        <% String url = "q_main_ajax.jsp?type=" + type + "&term=" + searchterm + "&msg=no results..."; %>
	        <meta http-equiv="refresh" content="0; URL=<%=url%>">

	        <%
	        break;

	    case 1:
	        // forward straight to the single answer
	        
	        QueryMasterAnswer answer = (QueryMasterAnswer)listOfResults.get(0);
	        
	        // if there is only one link - then go there
	        if (answer.getRelatedObjectLinks().size() == 1){
	            %>
	    	        <% String myurl = ((Link)answer.getRelatedObjectLinks().get(0)).getRelatedObjectLink(); %>
	    	        <meta http-equiv="refresh" content="1; URL=<%=myurl%>">
	            <%
	        }
	        break;
		default:
		    // show a list with all answers
		    
		    // show them group by ...
		    int groupedBy = 0;
			
		
    }
	%>
	
	<tr>
		<td colspan="21" style="background-color:#6682B6;" align="right"><font class="small"><%=new Date().toLocaleString() %>&nbsp;</font>
		

	<tr>
		<td width="1" style="background-color:#6682B6;" align="left" valign="top">
			<img  border='0' src="pics/corner_white2.gif" >
		<td colspan="21" style="background-color:white;" align="center">
			<font style="text-align: center; font-size:20">
				Search result
				<%if (searchterm.length() > 0){ %> 
				 for <b><i><%=searchterm %></i></b> 
				<%} %>
			
			</font>
	<tr>
		<td style="background-color: white" align="center" colspan="21">
			<font class="small">The following <%=listOfResults.size() %> results were found in the repository.<br>
			<% try {
			    int acaa = Integer.parseInt(type);
			    String stringA = searchTypes[acaa];
			    String stringB = searchTypesDef[acaa] ;
			    %>
				The search was limited on the category <%=stringA %> (<%=stringB%>).
			<%}catch (Exception e){} %>	
			
			</font>
		
					


	
	<tr>
	<td colspan="6">
		<table align="center" width="100%" border=0 style="background-color:white; ">
			<tr>
							<form>
								
								<td style="background-color:white;" align="right" valign="top"><a href="q_main_ajax.jsp?type=<%=type %>"><img  border='0' src="pics/back.gif"></td>
								<td style="background-color:white;" align="right" valign="top" width="400"></td>
							<% try {
							 	String img = searchTypesImg[Integer.parseInt(type)];
							 	%>								
							 	<td width="80%" style="background-color:white;" align="right" valign="top"><img  border='0' src="pics/<%=img%>"></td>
								<%
							}catch (Exception e){
							}%>
								<td align="right" style="background-color: white"><input type="text" type="text" id="testinput" name="term" size="20" style="text-align: center; font-size:20" value="<%=searchterm%>"> 
								<td style="background-color:white;" align="left"><input type="image" src="pics/go_search.gif" />
								<input type="hidden" name="type" value="<%=type%>"> 
							</form>
		</table>
	</tr>
	<tr>
		<td colspan="6">
		<table style="background-color: white" border="0" cellpadding="0" cellspacing="0">
			<tr>
<%
	for (int im = 0; im < searchTypesImg.length; im++){
	    String imageString = searchTypesImg[im];
%>
	<%if (im == Integer.parseInt(type)){ %>
		<td style="background-color: white"><img border='0' src="pics/ongRed_in0.gif">
		<td style="background-color: red;"><b><%=searchTypes[im]%></b>
		<td style="background-color: white"><img border='0' src="pics/ongRed_out0.gif">
	<%}else { %>
		<td style="background-color: white"><img border='0' src="pics/ongRed_in1.gif">
		<th>
			<a href="advancedquery_dev.jsp?term=<%=searchterm%>&type=<%=im %>">
				<font style="color: white;"><%=searchTypes[im]%></font> 
			</a>
		<td style="background-color: white"><img border='0' src="pics/ongRed_out1.gif">
	<%} %>
<% }%>
	<td width="100%" style="background-color: white">&nbsp;&nbsp;&nbsp;</td>
</table>
	
<tr>
	<td>
	<%
	Iterator iterator = listOfResults.iterator();

	while (iterator.hasNext()){
	    try {
		    QueryMasterAnswer answer = (QueryMasterAnswer)iterator.next();
		    
		    String value = answer.getLabel();
		    
			int posSearchTerm = value.toLowerCase().indexOf(searchterm.toLowerCase());
//			Logger.debug(this, posSearchTerm +"");
			int start = 0;
			String startAdd = "";
			int end = value.length();
			String endAdd = "";
//			Logger.debug(this, end +"");
			if (posSearchTerm > 20) {
			    start = posSearchTerm-10;
			    startAdd = "...";
			}
//			Logger.debug(this, start +"");
			if (value.length() - posSearchTerm - searchterm.length() > 20) {
			    end = posSearchTerm + searchterm.length() + 10;
			    endAdd = "...";
			}
//			Logger.debug(this, end +"");
		%>
			  <tr>
			    <th>&nbsp;</th>
			    <th>&nbsp;</th>
			    <th>&nbsp;</th>
			    <th>&nbsp;</th>
			    <th>&nbsp;</th>
			    <th>&nbsp;</th>
			  </tr>
			 <tr bordercolor="black">
			 	<th rowspan="8" ">
			 	</td>
			 	<td rowspan="8" align="center" valign="top" style="background-color: white;" >
					
			  		<!--  img -->
		    	    	<!--  <img  border='0' src="pics/Corner_TopBreadCrumb.gif"><br>-->
			  		
			  		
		        	<% if (answer.getIcon()!= null && answer.getIcon().compareTo("null") != 0 && answer.getIcon().length() > 0){ %>
		    	    	<img  border='0' src="<%=answer.getIcon()%>">
		        	<%} %>
		        	<br>
		        	<br>
					<!--  img if the information is public of access restricted. -->
					<% if(answer.isAccessRestricted()){%>
						<img  border='0' src='pics/logon.gif'/>
					<%} %>
		        </td>
		        <td>&nbsp;</td>
			 	<th rowspan="8" ">
		        <td>&nbsp;</td>
			 	<th rowspan="8" ">&nbsp;
	     	</tr>
	     	
	     	
	     	
			<% if (false){ %>
			     	
		     	
				<tr>
					<td>
						<!--  label  -->
						<h3>&nbsp;&nbsp;&nbsp;
	
				        <%try{ %>
					        	<font><%=startAdd%><%=value.substring(start, posSearchTerm) %></font><font style="background-color: graytext;"><%=value.substring(posSearchTerm, posSearchTerm+searchterm.length()) %></font><font ><%=value.substring(posSearchTerm + searchterm.length(),end)%><%=endAdd%></font>
				        <%}catch (Exception e){%>
					        	<font ><%=answer.getLabel() %></font>
				        <%} %>
				        </h3>
				  	</td>
				  	<td rowspan="6" valign="top">
	
				  		<!--  links  -->
			        	<table width="200" border="0" align="right">
			        		<tr>
			        		<td colspan="3"><b>Links:</b>
			        		
			        	<%
			        	for(int i = 0; i < answer.getRelatedObjectLinks().size(); i++){
			        	    Link link = (Link)answer.getRelatedObjectLinks().get(i);
			        	    %>
			        	<tr>
				        	<td colspan="3"><img  border='0' src='pics/bg-menu.gif' width="200" height="3" />
			        	<tr>
				        	<td valign="top" align="center" width="0" style="background-color: white;"><a href="<%=link.getRelatedObjectLink()%>"><img  border='0' src='pics/move_right.gif'></a>
				        	<td valign="top" align="left" width="99%" ><font class="small"> <%=link.getLabel()%></font>
				        	<td valign="top" align="right"><img  border='0' src='<%=link.getImg()%>'></td>
			        	</tr>
			        	<%}%>
			        	</table>
	
	
	
			        </td>	
			  	</tr>
				  	
	
				<%
					String[] infos = new String[]{answer.getInfo1(), answer.getInfo2(), answer.getInfo3()};
					for (int infoC = 0; infoC < infos.length; infoC++){
					    if (infos[infoC] != null && infos[infoC].length() > 1){
						    %>
							<tr>
							  	<td>
							  		&nbsp;&nbsp;&nbsp;<font ><%=infos[infoC]%></font>
							</tr>
							<%
					    } else { 
						    %>
							<tr>
							  	<td>
							</tr>
							<%
					    }
					}
				%>
				<tr>
				  	<td>
			  	</tr>
				<tr>
				  	<td>
			  	</tr>
				<tr>
				  	<td>
			  	</tr>
			  	
			<%} %>
		  	
		  	
		  	
		  	
		  	
		  	
		  	
		  	
	     	<!--  PRE RELEASE  -->
	     				<tr>
				<td>
					<!--  label  -->
					<h3>&nbsp;&nbsp;&nbsp;
				        	<font ><%=answer.getLabel() %></font>
			        </h3>
			  	</td>
			  	<td rowspan="6" valign="top">

			  		<!--  links  -->
		        	<table width="200" border="0" align="right">
		        		<tr>
		        		<td colspan="3"><b>Links:</b>
		        		
		        	<%
		        	for(int i = 0; i < answer.getRelatedObjectLinks().size(); i++){
		        	    Link link = (Link)answer.getRelatedObjectLinks().get(i);
		        	    %>
		        	<tr>
			        	<td colspan="3"><img  border='0' src='pics/bg-menu.gif' width="200" height="3" />
		        	<tr>
			        	<td valign="top" align="center" width="0" style="background-color: white;"><a href="<%=link.getRelatedObjectLink()%>"><img  border='0' src='pics/move_right.gif'></a>
			        	<td valign="top" align="left" width="99%" ><font class="small"><%=link.getLabel()%></font>
			        	<td valign="top" align="right"><img  border='0' src='<%=link.getImg()%>'></td>
		        	</tr>
		        	<%}%>
		        	</table>



		        </td>	
		  	</tr>
			  	

			<%
				String[] infos = new String[]{answer.getInfo1(), answer.getInfo2(), answer.getInfo3()};
				for (int infoC = 0; infoC < infos.length; infoC++){
				    if (infos[infoC] != null && infos[infoC].length() > 1){
					    %>
						<tr>
						  	<td>
						  		&nbsp;&nbsp;&nbsp;<font ><%=infos[infoC]%></font>
						</tr>
						<%
				    } else { 
					    %>
						<tr>
						  	<td>
						</tr>
						<%
				    }
				}
			%>
			<tr>
			  	<td>
		  	</tr>
			<tr>
			  	<td>
		  	</tr>
			<tr>
			  	<td>
		  	</tr>
		  	
		  	
		  	<!-- prerelease end  -->
	     			
		  	
		  	
		  	
		  	
		  	
		  	
		  	
		  	
		  	
		  	
		  	
		    <%
		    if (!iterator.hasNext()){
		        %>
		        			  <tr>
			    <th>&nbsp;</th>
			    <th>&nbsp;</th>
			    <th>&nbsp;</th>
			    <th>&nbsp;</th>
			    <th>&nbsp;</th>
			    <th>&nbsp;</th>
			  </tr>
		        
		        
		        <%
		    }
		    
		    
			} catch(Exception exception) {
			}
	}%>
</table>

<%@ include file="footer.jsp"%>

