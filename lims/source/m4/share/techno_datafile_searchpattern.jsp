<%@page import="java.util.HashSet"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="java.io.FilenameFilter"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Location"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Set"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>

<%

	// the pattern
	String searchpattern = request.getParameter("pattern");
	
	// the techofiletypeID
	long techofiletypeID = Long.parseLong(request.getParameter("tid"));
	
	Session s = TechnoCoreObject.createSession();
	TechnologyFileType technologyFileType = (TechnologyFileType)TechnologyFileType.persistence_loadByID((TechnologyFileType.class), s, techofiletypeID);


	// all files that were found
	HashSet files = new HashSet();
	
	
	// get each folder 
	Set locations = technologyFileType.getDefaultLocations();
	
	Logger.debug(this, "checking " + locations.size() + " locations.");
	
	Iterator locationIterator = locations.iterator();
	while(locationIterator.hasNext()){
		Location location = (Location)locationIterator.next();
		Logger.debug(this, "checking " + location.getPath());
		
		File folder = new File(location.getPath());
		
		// check for all files matching the pattern
		File[] fileArray = folder.listFiles(new FilenamePatternFilter(searchpattern));		
		
		for(int i = 0; i < fileArray.length; i++){
		    files.add(fileArray[i]);
		}
	}	
	
	Logger.debug(this, "found: " + files.size());
	
	// store in the session
	session.putValue("files", files);
%>
<%@page import="org.setupx.repository.core.util.FilenamePatternFilter"%>

<meta http-equiv="refresh" content="0; URL=techno_datafile_assing_list.jsp?tid=<%=request.getParameter("tid")%>">
