<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.server.logging.Message"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="org.setupx.repository.server.persistence.PersistenceActionFindException"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.criterion.Expression"%>
<table width="100%"> 
<% 
hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();
Iterator iter = hqlSession.createSQLQuery("select label, sampleID  from scanned_samples where sampleID > 1000 and sampleID != \" \" order by uoid desc limit 5 ")
.addScalar("label", org.hibernate.Hibernate.STRING)
.addScalar("sampleID", org.hibernate.Hibernate.STRING)
.list()
.iterator();

int i = 0;
while(iter.hasNext()){
Object[] array = (Object[])iter.next();

if (i%20 == 0) {%>
	<tr>
		<td></td>
		<th align="center">name</th>
		<th align="center" colspan="2">sample</th>
		<th align="center" colspan="2">experiment</th>
		<th align="center">time</th>
		<th align="center">label</th>
		<th align="center">details</th>
	</tr>
<%} 
i++;
int promtID = 0;
try {
    promtID = new SXQuery().findPromtIDbySample(Integer.parseInt("" + array[1])); 
}catch (Exception e){
    e.printStackTrace();
}%>
	<tr>
		<td></td>
		<td align="center"><%=array[0]%></td>
		<td align="center"><font class="small"><%=array[1]%></td>
		<td align="center" style="background-color:#FFFFFF"><a href="sample_detail.jsp?id=<%=array[1]%>"><img  border='0' src="pics/details.gif"></a></td>
		<td align="center"><font class="small"><%=promtID %></td>
		<td align="center" style="background-color:#FFFFFF"><a href="load?id=<%=promtID%>&action=20"><img  border='0' src="pics/details.gif"></a></td>
		<td align="center">
				
					<%
					try {
					    
						List _list = hqlSession.createCriteria(Message.class).add(Expression.eq("relatedObjectID", new Long("" + array[1]))).addOrder(Order.asc("date")).list();
						Message message = (Message) _list.get(_list.size()-1); 
						
						Calendar midnight = new GregorianCalendar();
						midnight.set(midnight.get(Calendar.YEAR),midnight.get(Calendar.MONTH), midnight.get(Calendar.DATE),0,1);

						if (message.getDate().after(midnight.getTime())){
							// todays samples
							%>	
								<font class="small">
									<%=DateFormat.getTimeInstance(DateFormat.SHORT, Locale.US).format(message.getDate())%>
								</font>
							<%} else { %>
								<font class="small">
								  <%=java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT, locale).format(message.getDate()) %>
								</font>
							<%} 
					} catch (Exception e){
					
						// occours when messages can not be found
					}
					%>
		</td>
		<td style="background-color:#FFFFFF" align="center">
				<% try {
				    String labelX = new SXQuery().findSampleLabelBySampleID(Integer.parseInt("" + array[1]));
				    if (labelX.compareTo("-your comment-") == 0) labelX = "";
				    %>
					<a href="sample_detail.jsp?id=<%=array[1]%>"><%=labelX %></a>
				<% } catch (PersistenceActionFindException e){%>
					----
				<% }%>
		</td>
		<td align="center" style="background-color:#FFFFFF"><a href="run_detail.jsp?id=<%=array[1]%>"><img  border='0' src="pics/details.gif"></a></td>	
	</tr>
    <%
}
%>
	<tr>
		<td></td>
		<td align="center">...</th>
		<td align="center" colspan="5"> </th>
		<td align="center">more</th>
		<td align="center" style="background-color:#FFFFFF" ><a href="scanned_samples.jsp"><img  border='0' src="pics/resize_x_plus.gif"></a></th>
	</tr>

</table>