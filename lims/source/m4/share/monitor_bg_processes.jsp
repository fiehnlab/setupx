<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net" xmlns:c="urn:jsptld:http://java.sun.com/jstl/core">
<jsp:directive.page import="org.setupx.repository.core.util.Util"/>
<jsp:directive.page import="org.setupx.repository.core.communication.BackgroundThread"/>
<jsp:directive.page import="org.setupx.repository.core.communication.BackgroundThreadMonitor"/>
<jsp:directive.page import="java.util.Locale"/>
<jsp:directive.page import="java.text.DateFormat"/>
<jsp:directive.page import="java.text.SimpleDateFormat"/>
<jsp:directive.page import="org.hibernate.Session"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Datafile"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Technology"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Location"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="java.util.Date"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.scanner.ScanningProcess"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.scanner.ScanningProcessMonitor"/>
<jsp:directive.page import="org.setupx.repository.web.HttpSessionPool"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />

  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>

  <link rel="stylesheet" href="print.css" type="text/css" media="print" />


	<h2>
		<a href="techno_techno.jsp">all platforms</a> - all scanners
	</h2>
	
	<jsp:expression>BackgroundThreadMonitor.getBackgroundThreads().size()</jsp:expression>
		
	<jsp:scriptlet>
		request.setAttribute("scanner", BackgroundThreadMonitor.getBackgroundThreads()); 
	</jsp:scriptlet>


	<display:table name="scanner" id="row">
    	<jsp:scriptlet>
    		BackgroundThread backgroundThread = ((BackgroundThread)row);
		</jsp:scriptlet>
		<display:column title="type">
			<jsp:expression>Util.getClassName(backgroundThread)</jsp:expression>
		</display:column>	
		<display:column title="pause (in sec)" sortable="true">
			<jsp:expression>backgroundThread.getState().toString()</jsp:expression>
		</display:column>
	</display:table>
	
</jsp:root>