<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net" xmlns:c="urn:jsptld:http://java.sun.com/jstl/core">
<jsp:directive.page import="java.io.File"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Location"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Datafile"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Technology"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"/>
  <jsp:directive.page contentType="text/html; charset=UTF-8" />
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.hibernate.Hibernate"/>
<jsp:directive.page import="org.setupx.repository.core.CoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.query.QueryMaster"/>
<jsp:directive.page import="org.setupx.repository.server.persistence.SXQuery"/>
<jsp:directive.page import="java.util.Vector"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
    <title>SetupX</title>
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />
  </head>
<jsp:scriptlet>

	Long sampleID = Long.parseLong(request.getParameter("id"));
	
	// get all datafiles for this sample

	List possible = TechnologyProvider.getPossibleDatafiles(sampleID);

	//
	request.setAttribute("possible", possible); 

	// style
	pageContext.setAttribute("tableclass", "its");

</jsp:scriptlet>


	<h3>
		<a href="techno_all.jsp">all platforms</a> - 
		Add Datafiles to repository for sample <jsp:expression>sampleID</jsp:expression>
	</h3>
	In order to store the datafile, choose the technology platform and the file type. 
		
	<p>
	<jsp:scriptlet>

	Iterator techIterator = TechnologyProvider.getTechnologies().iterator();

    while(techIterator.hasNext()){
        Technology technology = (Technology)techIterator.next();
        
        Iterator iterator = technology.getTechnologyFileTypes().iterator();
        while (iterator.hasNext()){
            TechnologyFileType technologyFileType = (TechnologyFileType)iterator.next();
            
            Iterator locations = technologyFileType.getDefaultLocations().iterator();
            while (locations.hasNext()){
                Location location = (Location)locations.next();
                
                File directory = new File(location.getPath());
                
                File[] files = directory.listFiles(technologyFileType);

                for (int i = 0; i != files.length; i++){
	                </jsp:scriptlet>
	                	<jsp:expression>technology.getLabel() + " " + technologyFileType.getLabel() + " " +  files[i].getName()</jsp:expression><br/>
	            	<jsp:scriptlet>
                }
            }
        }
    }
	</jsp:scriptlet>
	</p>
	
	<hr/>
	
	<p>
	You can also choose from the list of possible matches that the system found for this specific sample.
	</p>
	
	<display:table id="row" name="possible">
		<display:column title="File name" property="sourceFile"></display:column>
	    <display:column title="created on" property="creationDate" sortable="true" headerClass="sortable"/>
	    <display:column title="Sample ID" property="sampleID" sortable="true" headerClass="sortable" href="techno_sampleFiles.jsp?" paramId="id" paramProperty="sampleID"/>
	</display:table>

</jsp:root>


