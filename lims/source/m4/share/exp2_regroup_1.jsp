<%@page
	import="org.setupx.repository.core.communication.importing.logfile.ScannedPair"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>

<%

			Session hqlsession = CoreObject.createSession();
			
			Vector labelVector = new Vector();

			int counter_class = -1;

			
            // get all sampleIDs and regroup them according to the given selection
            int promtID = Integer.parseInt(request.getParameter("id"));
            
            // check if all runs are supposed to be selected
            Boolean select_all_runs = Boolean.valueOf(request.getParameter("sel"));
            Logger.log(this, select_all_runs + "");	

            // get classes
            Iterator clazzesIDs = new SXQuery().findClazzIDsByPromtID(promtID).iterator();

            while (clazzesIDs.hasNext()) {
                int clazzID = Integer.parseInt(clazzesIDs.next() + "");
                counter_class++;
                
                // get sample
                Iterator sampleIDs = new SXQuery().findSampleIDsByClazzID(clazzID).iterator();

    			int counter_samples = -1;
                while (sampleIDs.hasNext()) {
                    // get run
                    int sampleID = Integer.parseInt("" + sampleIDs.next());
                    counter_samples++;

                    //String label = new SXQuery().findSampleLabelBySampleID(sampleID);
                    List list = new SXQuery(hqlsession).findScannedPair(sampleID);

				    Logger.debug(this, "Number of runs: " + list.size());

                    Iterator scannedPairs = list.iterator();
        			int counter_run = -1;
                    while (scannedPairs.hasNext()) {
						String scannedPairLabel = (scannedPairs.next()) + "";

						/*
							// only if all samples are supposed to be exported
							if (scannedPairs.hasNext() && select_all_runs.booleanValue()){
							    looping = true;
							} else {
							    Logger.debug(this, "deavtivating all samples after this one: " + scannedPairLabel);
							    looping = false;
							}
						*/
						
	                    counter_run++;
						
						// defining in which group it actually goes.
			            int groupingType = Integer.parseInt(request.getParameter("grouping"));
						int assignedGroup = 0;

						switch (groupingType) {
			            case 1:
							assignedGroup = counter_class;
			                break;

			            case 2:
							assignedGroup = counter_run;
			                break;

			            case 3:
							assignedGroup = counter_samples;
			                break;

			            case 4:
			                // find the dimension
			                Iterator labels = new SXQuery().findLabelsforClazz(clazzID).iterator();
			                boolean loop = true;
			                while(labels.hasNext() && loop){
			                    String label = ((Object[])labels.next())[1] + "";
			                    Logger.debug(this, " " + label );
			                    
			                    // check if label is used already
			                    for (int i = 0; i < labelVector.size() && loop; i++){
			                        Logger.debug(this, "comparing: " + labelVector.get(i) + " vs. " + label);
			                        if (labelVector.get(i).toString().compareTo(label) ==0){
				                        assignedGroup = i;
				                        loop = false;
			                        }
			                    }

			                    // if not known yet store it.
			                    if (loop){
			                        Logger.debug(this, "adding: " + label);
			                        labelVector.add(label);
			                        //assignedGroup = labelVector.indexOf(label);
			                    }
			                    
			                    Logger.debug(this, " " + assignedGroup );
			                }
			                
			                break;

			            case 5:
			                // mark every x-th together
			                int numberOfGroups = Integer.parseInt(request.getParameter("groupNumbers"));
			                
			                // modulo on class count 
		                    assignedGroup = counter_class % numberOfGroups;
			                
			                break;

			            default:
			                break;
			            }

        		        request.getSession().setAttribute("" + scannedPairLabel , "" + assignedGroup);
                    }
                }
            }
%>

<%@page import="java.util.Vector"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<meta http-equiv="refresh"
	content="1; URL=exp2_regroup.jsp?id=<%=promtID%>">