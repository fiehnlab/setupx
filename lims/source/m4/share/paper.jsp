<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<%
org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

//Integer num_samples = (Integer) hqlSession.createQuery("select count(DISTINCT s.sampleID) from " + org.setupx.repository.web.forms.inputfield.multi.Sample.class.getName() + " as s where s.label != '' and s.sampleID != ' ' and s.sampleID != '1'" ).uniqueResult();

Long num_samples = (Long) hqlSession.createSQLQuery("select count(DISTINCT s.sampleID) as result from scanned_samples as s where s.label != '' and s.sampleID != ' ' and s.sampleID != '1'").addScalar("result", org.hibernate.Hibernate.LONG).uniqueResult();
Integer num_experiment = (Integer) hqlSession.createQuery("select count(*) from " + org.setupx.repository.web.forms.inputfield.multi.Promt.class.getName()).uniqueResult();
Integer num_users = (Integer) hqlSession.createQuery("select count(*) from " + org.setupx.repository.core.user.UserDO.class.getName()).uniqueResult();

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.web.forms.PromtCreator"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body >

<table width="500" align="center">
<tr>
<td align="center"><a href="http://fiehnlab.ucdavis.edu"><img src='pics/uc_logo.gif' border="0" valign="middle" align="center"></a></td>
<tr><td colspan="21" align="center"><font face="Verdana"	><hr>

<tr align="center"><td><strong>A PUBLIC STUDY DESIGN DATABASE FOR METABOLOMIC PROJECTS </strong>
<tr align="center"><td><a href="mailto:mscholz@ucdavis.edu"><img src='pics/mail.gif' border="0" valign="middle" align="center"></a> MARTIN SCHOLZ, <a href="mailto:mscholz@ucdavis.edu"><img src='pics/mail.gif' border="0" valign="middle" align="center"></a> OLIVER FIEHN
<tr align="center"><td><font size="-1">University of California, Davis
<tr align="center"><td><font size="-1">Genome Center
<tr align="center"><td><font size="-1">451 E. Health Sci. Drive
<tr align="center"><td><font size="-1">Davis, California 95616, USA

<tr>
<td colspan="2" align="justify" style="background-color:#FFFFFF"><font size="3" face="Times">
Metabolomic databases are useless without accurate description of the biological study
design and accompanying metadata reporting on the laboratory workflow from sample
preparation to data processing. Here we report on the implementation of a database
system that enables investigators to detail and set up a biological experiment, and that
also steers laboratory workflows by direct access to the data acquisition instrument.
<a href="."><%=org.setupx.repository.Config.SYSTEM_NAME%></a> utilizes orthogonal biological parameters such as genotype, organ, and
treatment(s) for delineating the dimensions of a study which define the number of classes
under investigation. Publicly available taxonomic and ontology repositories are utilized to
ensure data integrity and logic consistency of class designs. Class descriptions are
subsequently employed to schedule and randomize data acquisitions, and to deploy
metabolite annotations carried out by the seamlessly integrated mass spectrometry
database, BinBase. Annotated result data files are housed by <a href="."><%=org.setupx.repository.Config.SYSTEM_NAME%></a> for downloads and
queries. Currently, <%=num_users %> users have generated <%=num_experiment %>  studies, some of which are made public.


<tr><td colspan="21" align="center"><p><p><br><font face="Verdana"	>Download the whole publication. <a href="http://psb.stanford.edu/psb-online/proceedings/psb07/scholz.pdf"><br><img src='pics/pdf.gif' border="0" valign="middle" align="center"></a>
<tr><td colspan="21" align="center"><p><p><br><font face="Verdana"	><hr>

<tr><td align="center" colspan="21"><a href="http://fiehnlab.ucdavis.edu"><br><img src='pics/logo-fiehnlab.gif' border="0" valign="middle" align="center"></a>
<tr><td align="center" colspan="21"><font size="-1" face="Verdana"><a href="mailto:mscholz@ucdavis.edu"><img src='pics/mail.gif' border="0" valign="middle" align="center"></a> Martin Scholz<br>Nov 2006</a>
</body>
</html>