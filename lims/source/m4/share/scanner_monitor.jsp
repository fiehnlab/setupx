<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net" xmlns:c="urn:jsptld:http://java.sun.com/jstl/core">
<jsp:directive.page import="java.util.Locale"/>
<jsp:directive.page import="java.text.DateFormat"/>
<jsp:directive.page import="java.text.SimpleDateFormat"/>
<jsp:directive.page import="org.hibernate.Session"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Datafile"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Technology"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Location"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="java.util.Date"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.scanner.ScanningProcess"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.scanner.ScanningProcessMonitor"/>
<jsp:directive.page import="org.setupx.repository.web.HttpSessionPool"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />

  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>

  <link rel="stylesheet" href="print.css" type="text/css" media="print" />




	<jsp:include page="techno_header.jsp"></jsp:include>
	
	<h2>
		<a href="techno_techno.jsp">all platforms</a> - all scanners
	</h2>
	<jsp:scriptlet>
		request.setAttribute("scanner", ScanningProcessMonitor.getSessions()); 
	</jsp:scriptlet>

	<h3>Scanners checking for updated/incoming filedata</h3>
	The list shows all scanners that are currently scanning the defined locations for new datafiles.
	
	<display:table name="scanner" id="row">
    	<jsp:scriptlet>
			char qMark = (char)38; // ascii 38 
			DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);// DateFormat.getDateInstance(DateFormat.FULL);

    		ScanningProcess scanningProcess = ((ScanningProcess)row);
    		long nextScanINms = 0;
    		try {
    		    nextScanINms = (scanningProcess.getLastScan().getTime() + scanningProcess.getTimeBetweenScan() - new Date().getTime()) / 1000 ;	
    		} catch (Exception e){
    		    
    		}
    	</jsp:scriptlet>
		<display:column title="Technology" sortable="true" headerClass="sortable"  href="techno_technology.jsp?" paramId="id" paramProperty="technologyFileType.technology.UOID">
		      <c:out value="${row.technologyFileType.technology.label}"/>
	    </display:column>
		<display:column title="FileType" sortable="true" headerClass="sortable"  href="techno_technologyfiletype.jsp?" paramId="id" paramProperty="technologyFileType.UOID">
		      <c:out value="${row.technologyFileType.label}"/>
	    </display:column>
		
		
		<display:column title="last scan" property="lastScan" format="{0,date,HH:mm:ss}" sortable="true"></display:column>
		<display:column title="next scan" sortable="true">
			<jsp:scriptlet>
				if (!scanningProcess.isExternalPaused()){
					if ( ("" + nextScanINms).indexOf('-') == -1){
			</jsp:scriptlet>
				<jsp:expression>dateFormat.format(new Date(scanningProcess.getLastScan().getTime() + scanningProcess.getTimeBetweenScan()))</jsp:expression>
				(in <jsp:expression>nextScanINms</jsp:expression> sec)
			<jsp:scriptlet>
					} else {
			</jsp:scriptlet>
					- calculating -
					(<jsp:expression>nextScanINms</jsp:expression> sec)
			<jsp:scriptlet>
					}
				} else {
			</jsp:scriptlet>
				- paused - 
			<jsp:scriptlet>
				}
			</jsp:scriptlet>
		</display:column>	

		
		<display:column title="paused">
			<![CDATA[<a href="scanner_mod.jsp?tid=]]>
				<jsp:expression>scanningProcess.getName()</jsp:expression>
				<jsp:expression>qMark + "on="</jsp:expression>
				<jsp:expression>!scanningProcess.isExternalPaused()</jsp:expression>
			<![CDATA[">]]>
				<jsp:expression>scanningProcess.isExternalPaused()</jsp:expression>
			<![CDATA[</a>]]>	
		</display:column>
		
		
		<display:column title="start on boot" sortable="true">
			<jsp:expression>scanningProcess.getTechnologyFileType().isAutomaticAssignment()</jsp:expression>
		</display:column>



		<display:column title="locations">
			<jsp:scriptlet>
				Iterator iterator = scanningProcess.getTechnologyFileType().getDefaultLocations().iterator();
				while(iterator.hasNext()){
				    Location location = (Location)iterator.next();
			</jsp:scriptlet>
			<jsp:expression>
				location.getPath()
			</jsp:expression>
			<br/>
			<jsp:scriptlet>
				}
			</jsp:scriptlet>
		</display:column>
			
		<display:column title="pause (in sec)" sortable="true">
				<jsp:expression>scanningProcess.getTimeBetweenScan() / 1000</jsp:expression>
		</display:column>
	</display:table>



	<h3>Most recent incoming/assigned datafiles</h3>
	Samples added in the last 24 month per month.
	<table>
	<tr>
	<td>
   	<jsp:include page="incl_stat_samples.jsp"></jsp:include>
	</td>
	</tr>
	</table>


	<h3>Most recent incoming/assigned datafiles</h3>
	List of datafiles assingned to samples. The list contains Datafiles that were assigned automaticly by the scanners above or were assigned by a user.

	<jsp:scriptlet>
		Session s = TechnoCoreObject.createSession();
		request.setAttribute("test", s.createSQLQuery("select * from datafile order by uoid desc limit 15").addEntity(Datafile.class).list()); 
	</jsp:scriptlet>


   	<jsp:include page="inc_techno_datafile.jsp"></jsp:include>
   	
   	
   	<jsp:scriptlet>
   			s.close();
   	</jsp:scriptlet>

	<meta http-equiv="refresh" content="3; URL=scanner_monitor.jsp"/>

</jsp:root>








