<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.CoreObject"%>

<%
// 		General numbers of the system
//
//			- Percentage of Varaiations used in the exp Design
//			- avg Size of Class, Experiment
//			- number of exp/users
// 			- number of users/exp
//			- access statistics


	Session s = CoreObject.createSession();


	// number of samples per exp
	String q1 = "select avg(numberofScannedSamples) from cache where numberofScannedSamples > 1";

	%>
<h3>Experiment</h3>
	<p>
	Average size of an experiment: <%=s.createSQLQuery(q1).list().get(0) %>




	<h3>Variations</h3>
	Typical Variations in an Experiment:
	<%
	String q2 = "select l2, count(distinct promt) as c from (          select promt, l2, count(distinct l2_uoid) as c from (              select * from (                   select                       promt.uoid as promt, mf_container.parent, mf_container.uoid as l2_uoid, mf_container.question as l2, mf_label.uoid as l1_uoid, mf_label.question as l1, mf_label.value as l3                  from                    formobject as multiField4Clazzes,                   formobject as promt,                   formobject as page,                   formobject  as clazz,                   formobject  as child,                   formobject as mf_label,                   formobject as mf_container,                        multiField4ClazzesHibernate as x                        where clazz.uoid = x.multiField4Clazzs                        and x.elt = multiField4Clazzes.uoid                        and multiField4Clazzes.active = true                        and mf_label.PARENT = multiField4Clazzes.uoid                        and mf_container.uoid = mf_label.PARENT                       and (select count(*) from formobject where parent = multiField4Clazzes.parent) > 1                    and mf_label.value != \"\"                  and mf_label.value != '--other--'                    and page.parent = promt.uoid                   and child.parent = page.uoid                   and clazz.parent = child.uoid                                      group by mf_container.uoid                   order by mf_container.uoid              ) as x2          ) as x3           join cache          on cache.experimentID = promt          where cache.numberofScannedsamples > 1          group by parent  ) as x4 group by l2  order by count(distinct promt) desc  ";

	Iterator iter = s.createSQLQuery(q2).addScalar("l2", Hibernate.STRING).addScalar("c",Hibernate.INTEGER).list().iterator();
	
	while (iter.hasNext()){
	    Object[] objects = (Object[])iter.next();
	    %><br> <%=objects[0]%> <%=objects[1]%> <%
	}
%>

