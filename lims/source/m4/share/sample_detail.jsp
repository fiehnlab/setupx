<%@ include file="checknonlogin.jsp"%>


<%@ page import="java.util.*,
                 org.setupx.repository.core.communication.importing.metadata.*,
                 org.setupx.repository.server.persistence.*"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<body>

<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>

<%
Session hqlsession = CoreObject.createSession();

boolean cachedQueries = org.setupx.repository.Config.QUERY_CACHING_ACTIVE_JSP;

try {
	String cache = request.getParameter("cache");
    if (cache.compareTo("false") == 0){
        cachedQueries = false;
    }
} catch (Exception e){
    
}

Logger.debug(this, "start sample DETAIL -----------------");

int ID = Integer.parseInt(request.getParameter("id"));

org.setupx.repository.core.communication.importing.metadata.Data data = null;
try {
    data =  org.setupx.repository.core.communication.importing.metadata.CustomerMetaDataRetrieval.querySample(ID);
} catch (Exception e){
    e.printStackTrace();
}

Logger.debug(this, "lookig for clazzID");
int clazz = new SXQuery().findClazzIDBySampleID(ID);

Logger.debug(this, "lookig for promtid");
int promt = new SXQuery().findPromtIDbyClazz(clazz);
try {
    Logger.debug(this, "lookig for access rights");
    int access = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID(), promt).getAccessCode();
    if (access < 20){
		throw new PersistenceActionFindException("The user has insufficient right not access this information.");
    }
} catch (Exception exception){
    request.getSession().setAttribute(org.setupx.repository.web.WebConstants.SESS_FORM_MESSAGE, exception.getMessage());
    %>
    <jsp:forward page="error.jsp"/>
    <% 
}

Logger.debug(this, "lookig for other clazzIDs");
List s = new SXQuery().findSampleIDsByClazzID(clazz);
%>


	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Sample <%=ID %></h2> 
			Report for sample containing Labinformation and Biological Background<br/>
			<font class="small">Report also available in XML for MSI reporting.<br><a href="msi_sample_xml.jsp?id=<%=ID %>"> <img src="pics/xml3.gif"></a></font>
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>


	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} %>
	









<%
	Logger.debug(this, "lookig for determine sample run");
	boolean running = new SXQuery().determineSampleRun(ID);

	Logger.debug(this, "lookig for determine sample scheduled");
	boolean scheduled = false;
 %>







<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>


	<tr>
		<th align="left" colspan="11">Lab Information: <br><font class="small">&nbsp;&nbsp;information related to this sample stored in the LIMS</font> </th>
		<th align="center"><img border='0' src="pics/aq_help.gif"></th>		
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">Label<br><font class="small">&nbsp;&nbsp;the label that was printed on the sample</font> </td>
		<td style="background-color:white;" colspan="6"><%=new SXQuery().findSampleLabelBySampleID(ID)%></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>

	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">run: <br><font class="small">&nbsp;&nbsp;did the sample actually run yet</font> </td>
		<td colspan="6" style="background-color:#FFFFFF" >
			<table>
				<tr>
					<td style="background-color:#FFFFFF" align="center" ><font class="small">yes</font>
					<td style="background-color:#FFFFFF" align="center" ><font class="small">no</font>
				<tr>
			<%
			Logger.debug(this, "lookig for scanned pairs");
			List allRuns = new SXQuery(hqlsession).findScannedPair(ID);
				if (allRuns.size() > 0){
				    %>
						<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/checked.jpg"></a>
						<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/blank.jpg"></a>			
						<td colspan="4" style="background-color:#FFFFFF" >
				    	<font class="small">The sample was run <%=allRuns.size()%> times.<br>
				    	<% /* %>
				    	<table width="100%">
				    	<% Iterator iterator = allRuns.iterator();
				    		while(iterator.hasNext()){%>
				    		    <tr>
					    		    <td style="background-color:#FFFFFF" align="center"><%=iterator.next()%></td>
				    		    </tr>
				    		<% }%>
				    	</table>
				    	<% */ %>
				    	</td>
				    <% 
				} else {
				    %>
						<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/blank.jpg"></a>			
						<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/checked.jpg"></a>
						<td colspan="4" style="background-color:#FFFFFF" >
				    	</td>
				    
				    
				    
				    <%
				}
			
			// possible unrecognized samples 
			Vector possibleMatches = new Vector();
//			possibleMatches.add("abc");
			
			// find all files that match 
			// -a scheduled sample
			// -the base of an older sample
			// -...
			
			/*
			String _acqname = "";
			try {
				Logger.debug(this, "lookig for acqname1");
				_acqname = new SXQuery().findAcquisitionNameBySampleID(ID);
			} catch (Exception e){
			    scheduled = false;
			}

			if (user.isLabtechician() && _acqname.length() > 2){
				// String _acqname = new SXQuery().findAcquisitionNameBySampleID(ID);
				
	            _acqname = _acqname.replace(':','_');
				
	            String acqpart = _acqname.substring(0, _acqname.indexOf('_'));
				
	            for (int counter = 0; counter < 5; counter++){
	                SampleFile sampleFile = new SampleFile(acqpart + ":" + counter);
					Logger.debug(this, "lookig for file " + sampleFile + " TxT ");
	                boolean exists = sampleFile.exists(SampleFile.TXT);  
	    			if (	exists 
	    			        && !allRuns.contains(sampleFile.getACQName().replace(':','_')) 
	    			    	&& !allRuns.contains(sampleFile.getACQName().replace('_',':'))){
    			    	possibleMatches.add(sampleFile.getACQName());
	    			}
	    			    	
	            }
				
				
				
				if (possibleMatches.size() > 0){
				    
				    // possible match
				  	%>
				  		<tr>
				  			<td>
				  			<td>
							<td colspan="4" style="background-color:#FFFFFF" >
					    	<font class="small">Possible matches
					<%
						Iterator possMatchIterator = possibleMatches.iterator();
						while (possMatchIterator.hasNext()){
						    String possMatch = possMatchIterator.next() + "";
							
							%>	
						  		<tr>
									<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/checked.jpg"></a>
									<td style="background-color:#FFFFFF" align="center" ><a><img  border='0' src="pics/blank.jpg"></a>			
									<td colspan="4" style="background-color:#FFFFFF" align="center" >
									<%=possMatch %>
									
									<font class="small">
										<a href="assign_sample2.jsp?sampleID=<%=ID %>&filename=<%=possMatch %>">
											assign this file as an acutal run 
										</a>
						    		</font>
						    <%
						}
				}
			    
			}
			*/
			
			%>
	    	</table>
		</td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">collaborations<br><font class="small">&nbsp;&nbsp;</font> </td>
		<% 				Logger.debug(this, "lookig for collaborator"); %>
		<td style="background-color:white;" colspan="6"><%=new SXQuery().findPromtCollaborationHTML(promt)%></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>

	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">progress<br><font class="small">&nbsp;&nbsp;progress of this experiment</font> </td>
		<td colspan="6" style="background-color:white;" >
		
				
		<%
			session.setAttribute("imgpromtid", promt + "");
		 %>
		 
		<jsp:include page="progress_techno.jsp"/>
		
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
			
	<!--  link to results -->
	<%
	String pubQuery = "select d.published as date, d.publishedUserID as userID, sample.uoid as sampleID, d.label as label  from pubsample as sample, pubdata as d where sample.setupXsampleID = " + ID + " and d.uoid = sample.parent";
	List pubList = CoreObject.createSession()
					.createSQLQuery(pubQuery)
					.addScalar("date", Hibernate.DATE)
					.addScalar("userID", Hibernate.LONG)
					.addScalar("sampleID", Hibernate.LONG)
					.addScalar("label", Hibernate.STRING)
					.setCacheable(cachedQueries)
					.list();
	%>

	<!--  link to results -->
	<%
	Logger.debug(this, "lookig for pub compounds ");

	String compounds = "select a.label as label, a.comment, a.value, sample.uoid as compoundID from pubsample as sample, pubdata as d, pubattribute as a where sample.setupXsampleID = " + ID + " and d.uoid = sample.parent 	    and a.parent = sample.uoid 	    order by a.label ";
	List compList = CoreObject.createSession()
					.createSQLQuery(compounds)
					.addScalar("label", Hibernate.STRING)
					.addScalar("comment", Hibernate.STRING)
					.addScalar("value", Hibernate.STRING)
					.addScalar("compoundID", Hibernate.LONG)
					.setCacheable(cachedQueries)
					.list();
	if (compList.size() > 0){
	    %>
		<tr>
			<td style="background-color:#6682B6" width="20" rowspan="2"></td>
			<td rowspan="2" colspan="4">substances<br><font class="small">&nbsp;&nbsp;substances detected in this sample and published in datasets</font> </td>
			<td colspan="6" style="background-color:white;" >		
				<table width="100%">
					<tr>
						<td rowspan="21" style="background-color:white; " valign="top" align="center">
							<img  border='0' src="pics/shared.gif">
						<th>link to data
						<th>
				
				<%
				boolean showCompound = Boolean.valueOf(request.getParameter("showCompound")).booleanValue();

				for (int pl = 0; ( pl < 5 || showCompound) && pl < compList.size(); pl++){
				    Object[] objects = (Object[])compList.get(pl);
				    %>
					<tr>
						<td><a href="pubsample.jsp?uoid=<%=objects[3] %>"><%=objects[0] %></a>
						<td>
							<font class="small">
								<a href="pubsample.jsp?uoid=<%=objects[3] %>"><%=objects[2] %></a> &nbsp; 
								<a href="pubsample.jsp?uoid=<%=objects[3] %>"><%=objects[3] %></a>
							</font>
				    <%
				}
				if (!showCompound && compList.size() > 5){
				%>
					<tr>
						<td colspan="2"><font class="small"><a href="?id=<%= ID %>&showCompound=true"> show all <%=compList.size() %> compounds.</a> </font>
				<%} %>

			</table>
			<td style="background-color:#6682B6" width="20"></td>
			</tr>
			<tr>
				<td colspan="6" style="background-color:white;" >
				
			<%
		if (pubList.size() > 0){
	    %>
				<table width="100%">
						<th colspan="2">Datasets published for this sample.
				<%
				showCompound = Boolean.valueOf(request.getParameter("showCompound")).booleanValue();

				for (int pl = 0; ( pl < 5 || showCompound) && pl < pubList.size(); pl++){
				    Object[] objects = (Object[])pubList.get(pl);
				    %>
					<tr>
						<td align="center"><%=objects[3] %>
						<td>by <%=((UserDO)UserDO.persistence_loadByID(UserDO.class, DynamicStatus.createSession(), Long.parseLong(objects[1].toString()))).getDisplay()%>
						<br><font class="small"> on <%=((Date)objects[0]).toString() %></font>
				    <%
				}  
				if (!showCompound && pubList.size() > 5){
				%>
					<tr>
						<td colspan="2"><font class="small"><a href="?id=<%= ID %>&showCompound=true"> show all <%=pubList.size() %> public datasets.</a> </font>
				<%} %>
			</table>
			<td style="background-color:#6682B6" width="20"></td>
		</tr>
	    <%
	}
		%>
	    <%
	}
	%>

	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">
				<a href="techno_sampleFiles.jsp?id=<%=ID%>">List of all files</a><br>
		<font class="small">&nbsp;&nbsp;Files created be the connected instrument. To download the actual files use the link below.</font> </td>
		<td colspan="6" align="left" style="background-color:white;" >
			<table width="100%">
		<%
			Logger.debug(this, "lookig for technoplatform files");

			if (TechnologyProvider.getDatafiles((long)ID).size() > 0){
			    Collection techs = TechnologyProvider.getTechnologies();
			    Iterator technologies = techs.iterator();
			    
			    while(technologies.hasNext()){
					Technology technology = (Technology)technologies.next();
					
					if (technology.getFilesForSample((long)ID).size() > 0){
						%><tr>
						<th colspan="3"><a href="techno_technology.jsp?id=<%=technology.getUOID()%>"></a>
						<%=technology.getLabel()%><br><font class="small"><%=technology.getDescription() %></font><%

						Iterator technoFileTypes = technology.getTechnologyFileTypes().iterator();
					    
					    while(technoFileTypes.hasNext()){
					        TechnologyFileType technoFiletype = (TechnologyFileType)technoFileTypes.next();
					        Set datafilesList = technoFiletype.getDatafiles((long)ID);

					        
					        if (datafilesList.size() > 0){
						        %>
						        <tr><td><b><%=technoFiletype.getLabel() %></b><br><font class="small"><%=technoFiletype.getDescribtion() %></font>
						        <%
					        }
					        
					        Iterator datafiles = datafilesList.iterator();
							while(datafiles.hasNext()){
							    Datafile datafile = (Datafile)datafiles.next();
							    %>
		
								<tr>
									<td><%=datafile.getSourceFile().getName()%>
									
									<%
										if(datafile != null && datafile.getSourceFile() != null && datafile.getSourceFile().exists()) {

										    String size;
										    long number = datafile.getSourceFile().length();
										    if (number > 1000000000) {
										        size = Long.toString (number / 1000000000) + " GB";
										    } else if (number > 1000000) {
										        size = Long.toString (number / 1000000) + " MB";
										    } else if (number > 1000) {
										        size = Long.toString (number / 1000) + " KB";
										    } else {
										        size  = Long.toString (number) + " bytes";
										    } 
									%>
									<br><font class="small"><%=size%> (uncnompressed)</font>
									<br><font class="small">
									<% try { %>
									<%=datafile.getCreationDate().toLocaleString()%>
									<%} catch(Exception e){
									    
									}%>
									</font>

									<td width="4">
										<a href="techno_sampleFiles.jsp?id=<%=ID%>"><img  border='0' src="pics/checked.jpg"></a><br>
									<td width="4">
										<a href="techno_download.jsp?t=file&id=<%=datafile.getUOID() %>&files=<%=technoFiletype.getUOID() %>"><img  border='0' src="pics/icon_download.gif"></a>
									<%} else {%>
									<td width="4">
										<img  border='0' src="pics/x-amber.jpg" title="missing file">
									<td width="4">
									<%}
							}
					    }
					} 
			    }
			} else {
			    %><tr><td>no datafiles assigned yet<%
			}
		%>
		</table>
		
		<br>
		
		<table width="100%">
			<tr>	
				<th colspan="2">
					Download data for entire experiment
				</th>
			</tr>
			<tr>
				<td>
					Download all datafiles related to all samples in this experiment.
				</td>
				<td>
					<a href="techno_sampleFiles.jsp?id=<%=ID %>#download"><img src="pics/go.gif" border="0"></a>
				</td>
			</tr>
		</table>
		
					
		
		<td style="background-color:#6682B6" width="20"></td>
	</tr>

	
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>


	
	<tr>
		<th align="left" colspan="11">Biological information<br><font class="small">&nbsp;&nbsp;information stored about the orign of this sample</font> </th>
		<th align="center"><img border='0' src="pics/aq_help.gif"></th>		
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">Species<br><font class="small">&nbsp;&nbsp;</font> </td>
		<td colspan="6" style="background-color:white;" ><%=new SXQuery().findSpeciesbyPromtID(promt) %>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>

<% 
	Sample sample = Sample.loadByID(ID);
	String label_value = "";
	String label_key = "";
	
	for (int mySampleCounter = 0; mySampleCounter < sample.getFields().length ; mySampleCounter++){
	    if (sample.getField(mySampleCounter) instanceof StringInputfield){
	        label_key = "" +  ((StringInputfield)sample.getField(mySampleCounter)).getQuestion();
	        label_value = "" + ((StringInputfield)sample.getField(mySampleCounter)).getValue();
	        if (label_value.length() > 0){
	        %>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">
			<%=label_key %>:
			<br><font class="small">&nbsp;&nbsp;<%=((StringInputfield)sample.getField(mySampleCounter)).getDescription() %></font> 
			<%if (((StringInputfield)sample.getField(mySampleCounter)).getMSIAttribute() != null){%>
				<font style="font-size: xx-small;">MSI-Attribute</font>
			<%} %>
		</td>
		<td colspan="6" style="background-color:white" >
			<%=label_value %>&nbsp
			
			<%
			try {
				if (((StringInputfield)sample.getField(mySampleCounter)).getMSIAttribute() != null){%>
					<%=(((StringInputfield)sample.getField(mySampleCounter)).getMSIAttribute().getExtension())%>
				<%}
			} catch (Exception e){
				Logger.err(this, "can not find extension for msi attribute.");
			}%>
		</td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	        <%
	        }
	    }
	}
%>	



	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>


	<tr>
		<th align="left" colspan="11">Class information<br><font class="small">&nbsp;&nbsp;information about the class (set of replicates)</font> </th>
		<th align="center"><img border='0' src="pics/aq_help.gif"></th>		
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">ClassID:<br><font class="small">&nbsp;&nbsp;id for this class</font> </td>
		<td style="background-color:white;" colspan="6" ><%=clazz%></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">description <br><font class="small">&nbsp;&nbsp;description for this class</font> </td>
		<td colspan="6" style="background-color:white;"  ><%=Util.replace(  new SXQuery().findClassInformationString(clazz), "\n", "<p>")%></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	
	<% if (user.isLabTechnician()){ %>
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td colspan="4">Move<br><font class="small">&nbsp;&nbsp;move sample</font> </td>
			<td colspan="6" style="background-color:white;"  >In case the sample is located in the wrong class you can move it to another class.</td>
			<td style="background-color:#6682B6" width="20"></td>
		</tr>	
		<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td colspan="4"></td>
			<td colspan="6" style="background-color:white;"  >
							<%@ include file="incl_move_sample.jsp"%>
			<td style="background-color:#6682B6" width="20"></td>
		</tr>	
	<% }%>
	




	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>


	<tr>
		<th align="left" colspan="11">Experimental Design<br><font class="small">&nbsp;&nbsp;information about the whole experiment</font> </th>
		<th align="center"><img border='0' src="pics/aq_help.gif"></th>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">ID:<br><font class="small">&nbsp;&nbsp;id for this class</font> </td>
		<td style="background-color:white;"  colspan="6"><%=promt %></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">Title:<br><font class="small">&nbsp;&nbsp;Title of this experiment</font> </td>
		<td style="background-color:white;"  colspan="5"><%=new SXQuery().findPromtTitleByPromtID(promt)%></td>
		<td align="center" style="background-color:#FFFFFF"><a href="load?id=<%=promt%>&action=20"><img  border='0' src="pics/go.gif"></a>	
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">abstract<br><font class="small">&nbsp;&nbsp;Abstract for this experiment</font> </td>
		<%
			String myabstract = new SXQuery().findPromtAbstractByPromtID(promt);
			myabstract = Util.replace( myabstract, "\n", "<p>");
		
		%>

		<td style="background-color:white;"  colspan="6"><%=myabstract%></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td colspan="4">design<br><font class="small">&nbsp;&nbsp;experimental design of this experiment</font> </td>
		<td style="background-color:white;"  colspan="6">
				<% int promtID = promt; %>
				<%@ include file="incl_experiment_structure.jsp"%>
		</td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	









<%
	try {
		String acqname = "" + allRuns.get(0);
		acqname = acqname.substring(0, acqname.indexOf(":"));
		LecoACQFile lecofile = LecoACQFile.determineFile(acqname);
		Hashtable ht = lecofile.getValuesPerACQname(acqname);
	%>


	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>


	<tr>
		<th align="left" colspan="11">run details
			<br>
			<font class="small">&nbsp;&nbsp;Information for the actual run on the GC
			&nbsp;&nbsp;The data has been retrieved from 
				<%=lecofile.sourceFile.getName() %>.
			<br>&nbsp;&nbsp;File was modified on <%=new Date(lecofile.sourceFile.lastModified()).toLocaleString()%>. 
			<br><br><img border='0' src="pics/warning.gif">Use data with care - due to version changes of the GC there can be missmappings.
				
				</font> </th>
				
		<th align="center"><img border='0' src="pics/aq_help.gif"></th>		
	</tr>


		<% 
		
		Enumeration enumeration = ht.keys();
		while(enumeration.hasMoreElements()) { 
			String key = (String)enumeration.nextElement();
			String value = (String)ht.get(key);		
			value = value.replace('\\',' ');
			if (key.length() > 0 && value.length()>0){%>
			<tr>
				<td style="background-color:#6682B6" width="20"></td>
				<td colspan="4"><%=key%><br><font class="small">&nbsp;&nbsp;</font> </td>
				<td style="background-color:white;"  colspan="6"><%=value%></td>
				<td style="background-color:#6682B6" width="20"></td>
			</tr>
			<%} 
			} 
		}
	catch (Exception e){
	    e.printStackTrace();
	    
	}
	%>

	
	<tr>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
	
	
	<tr>
		<th align="left" colspan="11"><a id="hist">Sample History<br><font class="small">&nbsp;&nbsp;tracking information</font> </th>
		<th align="center"><img border='0' src="pics/aq_help.gif"></th>
	</tr>
	




	<tr>
		<td style="background-color:#6682B6" width="20"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td width="50%"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td width="0"></td>
		<td style="background-color:#6682B6" width="20"></td>
	</tr>
</table>









<%@ include file="footer.jsp"%>
</body>
<%@page import="org.setupx.repository.core.communication.importing.logfile.ScannedPair"%>
<%@page import="org.setupx.repository.web.forms.inputfield.StringInputfield"%>
<%@page import="org.setupx.repository.core.communication.leco.LecoACQFile"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Datafile"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Technology"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page import="org.setupx.repository.core.communication.file.SampleFile"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.core.communication.status.DynamicStatus"%>
</html>
