<%@ include file="checklogin.jsp"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/	>



<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>
<%@page import="org.setupx.repository.web.forms.FormObject"%>
<%@page import="org.setupx.repository.web.forms.inputfield.InputField"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIProvider"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIQuery"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.mysql.jdbc.Field"%>
<%@page import="org.setupx.repository.web.forms.inputfield.StringInputfield"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Arrays"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Clazz"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customize your sample information in <%=Config.SYSTEM_NAME %>.</title>
</head>
<body>

<%
	int pos = 0;
	
	// find the experiemnt ID
	Promt promt = (Promt)session.getAttribute(WebConstants.SESS_FORM_PROMT);
	
	
	
	Logger.debug(this, " classids: " + new SXQuery().findClazzIDsByPromtID(promt.getUOID()));
	

	// list current attributes and option to remove them in case they are "custom"
	Sample[] samples = new Sample[promt.getSamples().length];
	Logger.debug(this, "number of samples in promt: " + samples.length);
	
	
	try {
		Clazz[] classes = promt.getClazzDimensionDetector().getClazzes();
		String[] classStrings = new String[classes.length];
		Logger.debug(this, "number of classes in promt: " + classes.length);

		for (int iClass = 0; iClass < classes.length; iClass++){
		    try {
		        
		        classStrings[iClass] = new SXQuery().findClassInformationString(classes[iClass].getUOID());
		    } catch (Exception e){
		    	classStrings[iClass] = "-unknown-";
		    }
			// for each class the samples move into the array
			int size = classes[iClass].getSamples().size();
			for (int sampleC = 0; sampleC < size; sampleC++){
			    Sample sample = (Sample)classes[iClass].getField(sampleC);
			    samples[pos] = sample;
			    pos++;
			}
	    }
	} catch (Exception e){
	    e.printStackTrace();
	}
		

%>

<!--  head containing logo and description -->
<table align='center' border='0' cellpadding='2' width="70%">
<tr>
		<td align="center" width="2">
			<a href="form.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h3> <%=promt.getName()%>
			<% if (promt.getUOID() != 0){ %> [<%=promt.getUOID() %>]
			<%} %>
			</h3>
			Add your custom information for each sample.
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			


		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
</tr>
</table>

<%		String tableBackgroundColor = "#1B5289"; //"#E7f3ff"; // "white";// "#6682B6" ; %>
<table border="0" width="70%" align='center' cellpadding="5">
	<tr>
		<th>&nbsp;
		<th>How to ...?
		<th>
	<tr>
		<th>
		<td>	Below you see all the samples that are part of your experiment. <br>
				In order to add additional attributes to each sample please select from the MSI Attributes or define your own custom label.<br>
				Attributes can also be removed by using the <img src="pics/remove-all.gif" border="0"> icon.
			<p>
				In order to add information for multiple samples at once select them check the <img src="pics/form_history.gif" border="0"> icon next to the row you want to fill.
			<p>
				The samples are sorted as they are sorted in the regular sample list.
			<br>
		<th>
		
	<tr>
		<th>
		<td align="left" width="100%">
<table border="0" align='center' cellpadding="0"  cellspacing="0" background="<%=tableBackgroundColor %>">
<%


	// adjusting the set of attributes for every sample
	Hashtable questions = new Hashtable();

	// reading all possible questions and values
	for (int i = 0; i < samples.length; i++){
	    Sample sample = samples[i];
	    FormObject[] formObjects = sample.getFields();
	    for (int j = 0; j < formObjects.length; j++){
	        InputField inputField = (InputField)formObjects[j];
			
	        // check if the question is valid at this position
	        InputField questionDummy = (InputField)questions.get("" + j);
	        
	        //Logger.log(this, " comparing: " + inputField + "   and   " + questionDummy);
	        
			if (questionDummy == null || questionDummy.getQuestion().compareTo("null") == 0 || questionDummy.getQuestion().length() < 2){
			    // it is not even set yet. So set it ...
			    // ... but only if it is a custom label
			    if (inputField.getDescription().compareTo("custom field") == 0){
				    // adding it to list of custom fields
			        if (questions.get("" + j) != null ) Logger.log(this, "there is something in the questions already.");
				    questions.put("" + j, inputField);
				    
			       //Logger.debug(this, "found custom field :" + j + "  " + inputField.getQuestion());
			    }
			}
	    }
	}

	
	// matching all samples in number of question
	Logger.debug(this, "matching samples now ");
	// looping over samples
	boolean addedField = false;
	for (int i = 0; i < samples.length; i++){
		// Logger.debug(this, "sample " + i + "/" + samples.length);
	    Sample sample = samples[i];


	    for (int cound = 0; cound < 1000; cound++){
		    //Logger.debug(this, "cound: " + cound + " "  + questions.containsKey("" + cound));
			 if (questions.containsKey("" + cound)){
		        int key = cound;
		      
			    InputField dummyField = (InputField)questions.get("" + key);
	    		// Logger.debug(this, "question: " + dummyField.getQuestion());
	
			    // comparing every inputfield with hashset of questions
			    InputField inputField = null;
		        try {
		            inputField = (InputField)sample.getField(key);
		            if (inputField.getQuestion().compareTo(dummyField.getQuestion()) == 0){
			            // Logger.debug(this, "field " + dummyField.getQuestion() + " was found");
		            } else {
			            Logger.debug(this, "field " + dummyField.getQuestion() + " was found BUT wrong question.");
		            }
		        	
		        } catch (ArrayIndexOutOfBoundsException e){
		            Logger.debug(this, "field " + dummyField.getQuestion() + " not found at pos " + key + " for sample " + i);
	
		            Logger.debug(this, "creating field: " + dummyField.getQuestion());
		            StringInputfield field = new StringInputfield(dummyField.getQuestion() , "custom field", "--" , true);
		            if (dummyField.getMSIAttribute() != null) field.setMSIAttribute(dummyField.getMSIAttribute());
		            field.setSize_x(20);
		            field.setInternalPosition(dummyField.getInternalPosition());
		            samples[i].addField(field);
		            Logger.debug(this, "adding field");
		            addedField = true;
			        }
		        }
    	}
	 }	
	// in case any mods were made / update the promt.
	if (addedField) promt.update(true);
	//samples = promt.getSamples();
	
	int maxNrAttributes = questions.size();
	%>

	<tr>
		<th>ID
		<th colspan="2">class
		<th>
			label 
			<a href="sample_custom_autofill.jsp?row=label"><img src="pics/form_history.gif" border="0"></a>
			<!-- <a href="sample_custom_numbering.jsp?row=label"><img src="pics/editcomment.gif" border="0"></a> -->
		<th>comment <a href="sample_custom_autofill.jsp?row=comment"><img src="pics/form_history.gif" border="0"></a>
		<%

	    for (int cound = 0; cound < 1000; cound++){
		    //Logger.debug(this, "cound: " + cound + " "  + questions.containsKey("" + cound));
			 if (questions.containsKey("" + cound)){
		        int key = cound;
		      
			    InputField field = (InputField)questions.get("" + key);
	    		// Logger.debug(this, "question: " + field.getQuestion());
			    if(field != null || field.getDescription().length() == 0 || field.getDescription().compareTo("custom field") == 0){
					    %>
						<th><%=field.getQuestion() %>
						<% 
					    // remove button
					    %>
						<a href="sample_custom_del.jsp?label=<%=field.getQuestion() %>&pos=<%=field.getInternalPosition() %>"><img src="pics/remove-all.gif" border="0"></a>
					<%} %>
					<a href="sample_custom_autofill.jsp?row=<%=field.getQuestion() %>"><img src="pics/form_history.gif" border="0"></a>
					<%	
//				} catch (Exception e){
//				    e.printStackTrace();
//				}
			 }
		} %>


		<th >
		<font class="small">
		add additional fields:<br>
		MSI Attributes<br>
		<%

		Session s = CoreObject.createSession();
		List attributes = CoreObject.persistence_loadAll(MSIAttribute.class, s);
		
				
	    %>
			<form name="1" action="sample_custom_add.jsp">
				<input type="hidden" name="promt" value="<%=promt.getUOID() %>">
				<select name="name" onChange="document.forms[0].submit()">
					<option value="--" disabled="disabled" selected="selected">-MSI Attributes-</option>
					<%
					Iterator iterator2 = attributes.iterator();
						while (iterator2.hasNext()){
						    MSIAttribute attribute =(MSIAttribute )iterator2.next();
						    
						    //FormObject[] formObjects = //sample.getFields();
						    Enumeration elements = questions.elements();
						    boolean found = false;

						    while (elements.hasMoreElements()){
						        FormObject formObject = (FormObject)elements.nextElement();
//						    for (int k = 0; k < formObjects.length; k++){
						        if (((InputField) formObject).getQuestion().compareTo(attribute.getMsiLabel()) == 0){
						            found = true;
						        }
						    }
						    
						    // take out the ones that exist
						    if (!found){
								%>
								<option value="<%=attribute.getUOID()%>"><%=attribute.getMsiLabel()%></option>
								<%
						    }
						}
					%>
				</select>
			</form>
						<br>
			or you custom field<br>
			<form action="sample_custom_add.jsp">
				<input type="hidden" name="promt" value="<%=promt.getUOID() %>">
				<input type="text" name="name" value="... your label">
				<INPUT TYPE="image" NAME="Submit" SRC="pics/link.gif" ALT="Submit">
			</form>



		

	<%
	long tmpClassID = 0;
	for (int i = 0; i < samples.length; i++){
	    Sample _sample = samples[i];
	    
	    // get number of samples for this class
	    int classID = (int)_sample.getParent().getUOID();
	    int numberOfSamplesPerClass = new SXQuery().findSampleIDsByClazzID(classID).size();
	    
	    
	    if (_sample.getUOID() == 0) promt.update(true);
	    
	    %>
	    <tr>
	    	<td>
	    		&nbsp;<%=_sample.getUOID() %>&nbsp;
	    	<td style="background-color:<%=Util.getColor((int)_sample.getParent().getUOID()) %>">
	    		<font style="font-size: xx-small;"> &nbsp;<%=_sample.getParent().getUOID() %>&nbsp;</font>
	    	<%
	    		// in case it is the first sample of this class / show the classdesciption
	    		
	    		if (tmpClassID != classID){
	    		    tmpClassID = classID;
	    		    %>
			    	<td style="background-color:<%=Util.getColor((int)_sample.getParent().getUOID()) %>" rowspan="<%=numberOfSamplesPerClass%>">
			    		<font class="small">
			    			<%= Util.replace(new SXQuery().findClassInformationString(classID), "\n", "<BR>") %>
			    		</font>
			    	</td>
	    		    <% 
	    		}
	    	%>	
	    		

	    <%
		// list preexisting attributes
		  	    
	    FormObject[] formObjects = _sample.getFields();
	    for (int j = 0; j < formObjects.length; j++){
	        try {
		        InputField inputField = (InputField)formObjects[j];
		        // why?
		        if (inputField.getQuestion().compareTo("Sample ID") == 0){
		            // excluded - show above already		
		        } else {
			        %>
			        <td align="center">
			        	<nobr>
				        	<input type="text" readonly="readonly" value="<%=inputField.getValue() %>">
				        	<%
				        		if (inputField.getMSIAttribute() != null){
				        		    %>
				        		    <%=inputField.getMSIAttribute().getExtension() %>
				        		    <%
				        		}
				        	%>
			        	</nobr>
			        </td>
			        <%
		        }
	        } catch (Exception e){
	            e.printStackTrace();
	        }
	    }
	    %>
	<td>
	<%	    
	}
	
%>
	<tr>
	<td colspan="200" align="center">
	<table width="100%">
		<tr>
			<th colspan="2">When completed go back to the overview or the edit-form
		</tr>
		<tr>
			<th width="50%">Edit <strong>Experiment <%=promt.getUOID() %></strong>
			<th width="50%">List view of <strong>Experiment <%=promt.getUOID() %></strong>
		</tr>
		<tr>	
			<td align="center"><a href="form.jsp" ><img src="pics/go.gif" border="0"></a>
			<td align="center"><a href="promt_save_detail.jsp?id=<%=promt.getUOID() %>&action=20" ><img src="pics/go.gif" border="0"></a>
		</tr>
	</table>
</table>

	<th>
</table>
