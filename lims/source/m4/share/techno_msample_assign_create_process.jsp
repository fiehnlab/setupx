<%@ include file="checklogin.jsp"%>

<%@page import="java.util.Enumeration"%>
<%@page import="java.io.File"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Technology"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Datafile"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%
	int count = 0;
	// read the parameters
	Enumeration parameterNames = request.getParameterNames();
//	long sampleID = Long.parseLong(request.getParameter("sampleID"));
	
	while (parameterNames.hasMoreElements()){
	    String parameterName = parameterNames.nextElement().toString();
	    String parameterValue = request.getParameter(parameterName);
	    
    	// check each if valid file or not
	    // indicator for concatenated name
	    if (parameterName.lastIndexOf('_') > 0){
	        //Logger.log(this, (parameterName.substring(0,parameterName.lastIndexOf('_'))));
	        
	        long fileTypeID = Long.parseLong(parameterName.substring(parameterName.indexOf('_') + 1,parameterName.lastIndexOf('_'))); // ok

	        long sampleID = Long.parseLong(parameterName.substring(0,parameterName.indexOf('_'))); // ok
			
	    	// get the file path
	    	File file = new File(parameterValue);
	    	
	    	// determine the platform and filetype
			Session s = Technology.createSession();
	    	TechnologyFileType technologyFileType = (TechnologyFileType)TechnologyFileType.persistence_loadByID(TechnologyFileType.class, s, fileTypeID);

	    	// create Datafile object
	    	Datafile datafile = new Datafile(sampleID, file);
	    	datafile.setSubmitterUserID(user.getUOID());
	        %><br>datafile: <%=sampleID%> <%=file.getPath()%> created.<%

	    	// assign it to the filetype
	    	technologyFileType.addDatafile(datafile);

	    	// save it
	    	technologyFileType.getTechnology().updatingMyself(s,true);
			count++;

	    }
	}

%>

<table align="center">
<tr>

</td>
<h3>
<br>
<br>
<br>
<br>
<br>

assigned <%=count %> files to samples.
</h3>
<meta http-equiv="refresh" content="0; URL=techno_techno.jsp">

