<%@ include file="checknonlogin.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIQuery"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<%@page import="org.setupx.repository.core.communication.msi.AttributeValuePair"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIProvider"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<html>
<head>




<%


	long promtID = Long.parseLong("" + request.getParameter("id"));
	try {
		PromtUserAccessRight.checkAccess(user.getUOID(), promtID, PromtUserAccessRight.READ);

	String speciesName =new SXQuery().findSpeciesbyPromtID(promtID).get(0).toString();

	int ncbiID = NCBIConnector.determineNCBI_Id(speciesName);
	
	NCBIEntry species = NCBIConnector.determineNCBI_Information(ncbiID);

%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%> Report: <%=new SXQuery().findPromtTitleByPromtID(promtID) %></title>
</head>
<body>

<%@ include file="pub_nav.jsp"%>


<table width="700" align="center" >

<tr>
<td align="center"><a href="http://fiehnlab.ucdavis.edu"><img src='pics/uc_logo.gif' border="0" valign="middle" align="center"></a></td>
<tr><td colspan="21" align="center"><font face="Verdana"><hr>

<tr align="center">
	<td>
		<strong>
			<font style="font-variant: small-caps;">
				<%=new SXQuery().findPromtTitleByPromtID(promtID) %>
			</font>
		</strong>

<tr align="center"><td>

	<%@ include file="incl_collaboration.jsp"%>	


<tr align="center"><td><font size="-1">
<%
	iterator = users.iterator();
	while(iterator.hasNext()){
	    UserDO userDO = (UserDO)iterator.next();
	    %><%=userDO.getOrganisation()%>, <%
	}
%>

<tr align="center"><td><font size="-1">Genome Center

<tr align="center"><td><font size="-1">451 E. Health Sci. Drive

<tr align="center"><td><font size="-1">Davis, California 95616, USA

<tr align="center"><td><font style="font-family: sans-serif; font-size: x-small">Summary created by <%=org.setupx.repository.Config.SYSTEM_NAME%>. This report has been created based on the information store in the system.
</font>
<hr>

<!-- abstract -->
<tr>
	<td>
		<font size="+1">
		<b>Abstract</b>
		</font>
<tr>
	<td>
			<%=Util.replace(new  SXQuery().findPromtAbstractByPromtID(promtID), '\n', "<br>") %>
	</td>
</tr>
	
<!-- introduction -->
<tr>
	<td>
		<br>
		<font size="+1">
			<b>Introduction</b>
		</font>	
<!-- experimental procedures -->
<tr>
	<td>
		<br>
		<font size="+1">
			<b>Experimental Procedures</b>
		</font>
<tr>
	<td>
This section describes the metadata supported by the <%=org.setupx.repository.Config.SYSTEM_NAME%> database (Scholz and Fiehn, 2007),
following the recommendations by the Metabolomics Standards Initiative (Fiehn et al., 2007b;
Sumner et al., 2007). Details for the analysis of the <%=species.getName() %> mutant are given below.

<%if (species.getSpeciesType() == species.PLANT) { %>Plant growth metadata are separated by the physical object of the study (BioSource) and growth, treatment and harvest parameters. <%} %>
<%if (species.getSpeciesType() == species.ANIMAL) { %>Animal growth metadata are separated by the physical object of the study (BioSource) and growth, treatment and harvest parameters. <%} %>
<%if (species.getSpeciesType() == species.MIRCOORGANISM) { %>Growth metadata of the microorganism are separated by the physical object of the study (BioSource) and growth, treatment and harvest parameters. <%} %>





<!-- all MSI Attributes for the entire experiment -->
<p>
<table >
	<tr>
		<th width="40%">
			<h4>Attribute (MSI Attribute)</h4>
		<th width="60%">
			<h4>Different Values</h4>
	</tr>
			
<%
	
	//String query = "select value, msiattribute from formobject where msiattribute is not null and value != \"\" and value != \"false\" and value != \"true\" and active = 1 and root(uoid) = " + promtID;
	String query = "select f.value, f.msiattribute from formobject as f, msiattribute as m where f.msiattribute is not null and f.active = 1 and root(f.uoid) = " + promtID + " and f.msiattribute = m.uoid order by m.question ";
	


	Session s = CoreObject.createSession();
	List list = s.createSQLQuery(query)
		.addScalar("value", Hibernate.STRING)
		.addScalar("msiattribute", Hibernate.STRING)
		.list();
	
	
    Logger.debug(this, "result size: " +list.size());
    Hashtable cache = new Hashtable();
	Iterator iterator2 = list.iterator();
	while(iterator2.hasNext()){
	    Object[] objects = (Object[])iterator2.next();  
	    try {
		    String value = objects[0].toString();
		    if (value != null && value.compareTo("null") != 0 && value.length() > 0){
			    try {
				    Logger.debug(this, value + " cached? " + ((Vector)cache.get(objects[1].toString())).contains(value));
				} catch (Exception e){
				    //Logger.debug(this, "creating new Vector for " + objects[1].toString());
				    cache.put(objects[1].toString(), new Vector());
				    ((Vector)cache.get(objects[1].toString())).add(value);
				    Logger.debug(this, "added value (2) " + value);
			    }
			    
			    if (((Vector)cache.get(objects[1].toString())).contains(value)){
	
			    } else {
				    if (value.length() > 1){
					    long msiLong = Long.parseLong(objects[1].toString());
					    try {
					        ((Vector)cache.get(objects[1].toString())).add(value);
						    Logger.debug(this, "added value (1) \"" + value + "\"");
						} catch (Exception e){
						    Logger.debug(this, "creating new Vector for " + objects[1].toString());
						    cache.put(objects[1].toString(), new Vector());
						    ((Vector)cache.get(objects[1].toString())).add(value);
						    Logger.debug(this, "added value (2) \"" + value + "\"");
					    }
				    }    
			    } // end else
		    }// end if
	    }catch (Exception e){
	        e.printStackTrace();
	        
	    }		
	}

	String color = "";
	int cMod = 1;
	Enumeration keys = cache.keys();
	while (keys.hasMoreElements()){
	    String key = keys.nextElement().toString();
		long msiLong = Long.parseLong(key);
		try {
			MSIAttribute _attribute = (MSIAttribute)CoreObject.persistence_loadByID(MSIAttribute.class, s, msiLong);
			Vector values = (Vector)cache.get(key);
			if (cMod > 0){ 
			    color = "style=\"background-color:#D3D3D3;\"";
			} else {
			    color = "";
			}
			cMod = cMod * -1;
			%>
			<tr >
				<td  <%=color %>><%=_attribute .getQuestion() %>
					<font style="font-size: xx-small;">
						<br>
						<%=_attribute.getMsiLabel() %>
						<br>
						<%=_attribute.getDescribtion() %>
					</font>
				<td  <%=color %>>
					<%for (int a = 0; a < values.size(); a++){ %>
						<nobr><%=values.get(a) %> <%=_attribute.getExtension() %> </nobr>
						<%if (values.size() < 6) {%><br>
						<%} else {%>
						,&nbsp;
						<%} %>
					<%} %>
			<%
		} catch (Exception e){
		    
		}
	}
%>

</table>



		<br>
		<br>
		<br>
			<font size="+1">
			<b>Experimental Design</b>
		</font>
		<br>
		This section describes the structure of the experiment. 
		The design of the study is based on the number of different classes which represent a set of replicates.
		The following list shows the classes and the relevant attributes that differentiate them from each other.
		<br>
		<%=new SXQuery().findPromtStructure(promtID) %>

		<table width="100%" >
		<tr>
			<th width="40%">
				<h4>Class (<%=org.setupx.repository.Config.SYSTEM_NAME%>ID)</h4>
			<th width="60%">
				<h4>defining MSI Attributes</h4>
		</tr>
<%

// load all classes
Iterator clazzIDIterator = new SXQuery().findClazzIDsByPromtID(promtID).iterator();

// loop over classes and get MSI attributes for ONLY class
while(clazzIDIterator.hasNext()){
    long clazzID = Long.parseLong(clazzIDIterator.next() + "");
    
	if (cMod > 0){ 
	    color = "style=\"background-color:#D3D3D3;\"";
	} else {
	    color = "";
	}
	cMod = cMod * -1;

	// show one class per <tr> and the list of MSI attributes as additional table
    AttributeValuePair[] _attributeValuePairs = MSIQuery.searchAttributesPerClassByClassID((int)clazzID);
	Logger.debug(this, "found " + _attributeValuePairs.length + " attributes for this class " + clazzID);
	%>
	<tr>
		<td <%=color %>>Class <%=clazzID %>
		<td <%=color %>><%@ include file="incl_attributeValuePairs.jsp"%>
	</tr>
<%
}
%>
</table>


<!-- ref -->
<tr>
	<td>
		<br>
		<b>Detailed Information</b>
<tr>
	<td>
		<% // <ol><a href="pubexperiment_extended.jsp?id=<%=promtID...">List of Samples</a></ol> %>
		<% // <ol>List of Samples [tmp. deactivated]</ol> %>
		<ol><a href="pubexperiment_extended.jsp?id=<%=promtID%>">Experimental Design</a></ol>
		
			<% 
		String myFilename = Config.DIRECTORY_RELATED + File.separator + promtID + File.separator + promtID + "_public.zip";
		File myFile = new File(myFilename);
		if (myFile.exists()){
		    %>
			<ol><a href="download?<%=WebConstants.PARAM_FILENAME%>=<%=file.getName() %>&<%=WebConstants.PARAM_PROMTID%>=<%=promtID%>">Annotation Result</a></ol>
		    <%
		} else {
		    %>
		    <ol>Annotation Result not available.</ol>
		    <%
		}
	%>
		<ol><a href="download_result.jsp?id=<%=promtID %>">Instrument results</a></ol>




<!-- ref -->
<tr>
	<td>
		<br>
		<b>References	</b>
<tr>
	<td>
		<ol>Scholz, M. and Fiehn, O. (2007) SetupX - a public study design database for metabolomic projects. <i>Pacific Symposium on Biocomputing</i> 12, 169-180.</ol>
		<ol>Fiehn, O., Sumner, L.W., Rhee, S.Y., Ward, J., Dickerson, J., Lange, B.M., Lane, G., Roessner, U., Last, R. and Nikolau, B. (2007b) Minimum reporting standards for plant biology context information in metabolomic studies. <i>Metabolomics</i>, 3, (in press).</ol>
		<ol>Fiehn, O., Wohlgemuth, G. and Scholz, M. (2005) Setup and annotation of metabolomic experiments by integrating biological and mass spectrometric metadata. <i>Proc. Lect. Notes Bioinformatics</i>, 3615, 224-239.</ol>
		
</body>
</html>


<%	} catch(Exception e){
    e.printStackTrace();

	    %>
	    
	    The user has insufficient right not access this information.
	    
	    <% 
	    
	}
%>