<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 org.setupx.repository.core.communication.binbase.BBConnector,
                 java.util.List,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.util.logging.Logger,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="java.util.HashSet"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.web.forms.PromtCreator"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>





<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%></h2>
			Index<br/>
			<font size="-2">Below you will find the experiments that you ran in the lab, <br>that you do in collaboration with others and experiments that are available for everybody.</font>
		</th>

		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>

		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<%

org.hibernate.Session hqlSession = 	org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>

	<table align="center" width="80%" cellpadding="7">
	
	<% long id = Long.parseLong("" + request.getParameter("id")); %>
	
	
	
	<%  // START %>
	<tr>
		<th></th>
		<th colspan="2">Remove Access for Experiment <%=id %></th>
		<th></th>
	</tr>


	<tr>	
		<td></td>
		<td colspan="2">You are going to remove access to experiment nr.<%=id %> for the following users.</td>
		<td></td>
	</tr>		
	
	<%
		List list = new SXQuery().findPromtUserAccessRightForPromtID(id);
		Iterator iterator = list.iterator();
		while(iterator.hasNext()){
		    PromtUserAccessRight accessRight = (PromtUserAccessRight)iterator.next();
			UserDO _u = (UserDO)UserLocal.persistence_loadByID(UserLocal.class, hqlSession, accessRight.getUserID());
		    %>
	<tr>	
		<td></td>
		<td style="background-color:#FFFFFF" align="center" ><img src='pics/user.gif' border="0" valign="middle" align="center"></td>
		<td>
			<b><%=_u.getDisplay() %></b><br>
			<font class="small"><%=_u.getOrganisation() %></font><br>
			AccessLevel <b><%=accessRight.getAccessCode()%></b>
		</td>
		<td></td>
	</tr>			    
	<% } %>

	
	<tr>	
		<td></td>
		<td style="background-color:#FFFFFF" align="center" width="2"><img src='pics/error2.gif' border="0" valign="middle" align="center"></td>
		<td>  Please confim that the access can be removed for all of the users listed.</td>
		<td></td>
	</tr>		

	<tr>	
		<td></td>
		<td colspan="2" align="center">
			<table cellpadding="4" align="center">
				<tr>
					<th style="background-color:#FFFFFF" align="center" width="2">
						<a href="remove.jsp?id=<%=id%>"%>
							<img src='pics/anomalies.gif' border="0" valign="middle" align="center"> 
						</a>
					<th align="center">delete 

					<th style="background-color:#FFFFFF" align="center" width="2">
						<a href="load?id=<%=id%>&action=20">
							<img src='pics/experiment.gif' border="0" valign="middle" align="center"> 
						</a>
					<th align="center">back to <br>experiment

					<th style="background-color:#FFFFFF" align="center" width="2">
						<a href="main.jsp">
							<img src='pics/return_back.gif' border="0" valign="middle" align="center"> 
						</a>
					<th align="center">chancel
				</tr>
			</table>
		
		</td>
		<td></td>
	</tr>		
	<tr>
		<th></th>
		<th colspan="2">
		</th>
		<th></th>
	</tr>
	
	
	
	