<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Iterator"%>
<%

Session s = CoreObject.createSession();
List l = s.createSQLQuery("select remoteAddr as a, count(uoid) as b from WebAccess group by remoteAddr")
                .addScalar("a",Hibernate.STRING)
                .addScalar("b",Hibernate.STRING)
                .list();
Hashtable countries = new Hashtable();
Iterator i = l.iterator();
int maxAccessNumbers = 0;

while (i.hasNext()){
    Object[] objects = (Object[])i.next();
    String ip = "" + objects[0];
    String count = "" + objects[1];
    
    // ip = "128.120.136.41";
    
    if (Integer.parseInt(count) > 1){
            URL url = new URL("http://api.hostip.info/get_html.php?ip=" + ip);
            URLConnection connection = url.openConnection();
        
            connection.connect();
        
            String contentType = connection.getContentType();
        
            InputStream in = connection.getInputStream();
            BufferedReader dis = new BufferedReader(new InputStreamReader(in));
            StringBuffer fBuf = new StringBuffer();
            String line;
        
            while ((line = dis.readLine()) != null) {
                        if (line.indexOf("Country") > -1){
                            String country = line.substring(line.lastIndexOf("(") + 1, line.lastIndexOf("(") + 3 );
                            
                            if ((country.compareTo("US") != 0) && (country.compareTo("UK") != 0)){
                                %><%=country %> <%=count %><%@page import="org.setupx.repository.core.util.logging.Logger"%>
<BR><%
	                            int accessNumbers = Integer.parseInt(count);
	                            try {
	                                    accessNumbers = Integer.parseInt("" + countries.get(country));
	                                    accessNumbers = accessNumbers + Integer.parseInt(count);
	                                    
	                            } catch (Exception e){
	                            
	                            }
	                            countries.put(country, "" + accessNumbers);
	                            if (accessNumbers > maxAccessNumbers) maxAccessNumbers = accessNumbers;
                            }
                        }
                fBuf.append(line + "<br>");
            }
            in.close();
    }
}
        String countryKeys = "";
        String countryValues = "";

        Enumeration keys = countries.keys();
        while (keys.hasMoreElements()){
            
            String key = "" + keys.nextElement();
            
            // value needs to be in percentage to the existing one
            String valueS = "" + countries.get(key);
            int value = Integer.parseInt(valueS);
            
            // 
            value = ( 80 * value / maxAccessNumbers )+ 20;
            
                countryKeys = countryKeys.concat(key);
                countryValues = countryValues.concat("" + value);
                if (keys.hasMoreElements()){
                    countryValues = countryValues.concat(",");
                }
                
                %>
	                <%=key %>: 
	                <%=value %>
	                <br>
                <%
        }
        
        countryKeys = "";
        countryValues = "";
        int num = 1;
        
        for (int k = 0; k < 1000000; k++){
            keys = countries.keys();
            while (keys.hasMoreElements()){
                String key = "" + keys.nextElement();   
				int value = Integer.parseInt("" + countries.get(key));
			
				if (value == k){
				    // 
	                countryKeys = countryKeys.concat(key);
	                countryValues = countryValues.concat("" + num);
	                num++;
	                if (keys.hasMoreElements()){
	                    countryValues = countryValues.concat(",");
	                    countryValues = countryValues.concat(",");
	                }
				}
            }
        }
        
        
        String[] geoArea = new String[]{"africa","asia","europe","middle_east","south_america","world"};

        for (int k = 0; k < geoArea.length; k++){
            %>
            <img src="http://chart.apis.google.com/chart?cht=t&chco=ffffff,edf0d4,13390a&chs=400x200&chld=<%=countryKeys %>&chd=t:<%=countryValues%>&chtm=<%=geoArea[k] %>"> 
            <br>
            <%
        }
%>

