<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net" xmlns:c="urn:jsptld:http://java.sun.com/jstl/core">
<jsp:directive.page import="java.net.URL"/>
<jsp:directive.page import="org.setupx.repository.server.persistence.SXQuery"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"/>
<jsp:directive.page import="org.hibernate.Session"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Datafile"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Technology"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.hibernate.Hibernate"/>
<jsp:directive.page import="java.util.Vector"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
    <title>SetupX</title>
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>


	<jsp:include page="techno_header.jsp"></jsp:include>

<jsp:scriptlet>

	long experimentID = Long.parseLong(request.getParameter("id"));

	// get all datafiles for this sample

	Session s = TechnoCoreObject.createSession(); 
    String query = "select * from datafile join samples where samples.sampleID = datafile.sampleID and experiment = " + experimentID;
    List list = s.createSQLQuery(query).addEntity(Datafile.class).list();

	//
	request.setAttribute("test", list); 

	// style
	pageContext.setAttribute("tableclass", "its");

</jsp:scriptlet>

	<h2>
		<a href="techno_techno.jsp">all platforms</a> - 
		
		<![CDATA[<a href=]]>"progress_detail.jsp?id=<jsp:expression>experimentID</jsp:expression>"<![CDATA[>]]>
		experiment <jsp:expression>experimentID</jsp:expression>
		<![CDATA[</a>]]>
	</h2>
	
	<![CDATA[<a href="load?action=20&id=]]><jsp:expression>experimentID</jsp:expression>"<![CDATA[>]]>
	view sample in LIMS
	<![CDATA[</a>]]> -
	<a href="#assign">assign files</a> - <a href="#download">download datafiles</a>
	
	<hr/>
	<jsp:scriptlet>	    
	session.setAttribute("imgpromtid", "" + experimentID);
	</jsp:scriptlet>

	<jsp:include page="progress_techno.jsp"/> 

	<hr/>
	
	
	<jsp:scriptlet>
		String experimentName = "-unknown-";
		try {
			experimentName = new SXQuery().findPromtTitleByPromtID(experimentID);
		} catch (Exception e){
		}
		
	</jsp:scriptlet>
	The following samples are assigned to samples that are part of experiment "<jsp:expression>experimentName</jsp:expression>" (<jsp:expression>experimentID</jsp:expression>).
	<br/>
	
   	<jsp:include page="inc_techno_datafile.jsp"></jsp:include>
   	
   	
   	
	<a id="assign"/>
	<table>
		<tr>
			<th>
				Assign additional files to sample
			</th>
		</tr>
		<tr>
			<td>
				If there are files that you want to add to samples of this <b>experiment <jsp:expression>experimentID</jsp:expression></b>, please use the wizzard.
			</td>
		</tr>
		<tr>
			<td>
				<![CDATA[<a href="techno_msample_massign.jsp?id=]]>
				<jsp:expression>experimentID</jsp:expression>				
				<![CDATA[">]]>
				<img src="pics/go.gif"/>
				<![CDATA[</a>]]>
			</td>
		</tr>
	</table>
	
	
	<a id="download"/>
	<table>
		<tr>
			<th>
				Download data related to samples in this experiment
			</th>
		</tr>
		<tr>
			<td>
				<![CDATA[<a href="techno_download.jsp?t=promt&files=all&id=]]>
				<jsp:expression>experimentID</jsp:expression>				
				<![CDATA[">]]>
				<img src="pics/go.gif"/>
				<![CDATA[</a>]]>
			</td>
		</tr>
	</table>
	
</jsp:root>




