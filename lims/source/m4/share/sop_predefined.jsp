<%@ include file="checklogin.jsp"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.Config"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Select from Custom SOPs</title>
</head>
<body>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customize your sample information in <%=Config.SYSTEM_NAME %>.</title>
</head>
<body>

<%
	String fieldname = "";
	try {
	    fieldname = "" + request.getParameter("name");
	} catch (NullPointerException e){
	    
	}
%>

<!--  head containing logo and description -->
<table align='center' border='0' cellpadding='2' width="70%">
<tr>
		<td align="center" width="2">
			<a href="form.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h3>Standing operating procedures</h3>
			<%
			// if there is a valid field that the select values will be assigned to
 			if (fieldname != null && fieldname.startsWith("field")) {
			%>
			Please select the SOP that you want to use again in your experiment.
			<%}else { %>
			Back to my Experiments <br><a href="main.jsp"><img border="0" src="pics/go.gif"></a>
			<%} %> 

		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			


		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
</tr>
</table>

<table align='center' border='0' cellpadding='2' width="70%">
<%
			// id of related experiment
			long id = 0;
			long targetFields = 0;

			String type = request.getParameter("type");
			

			
			Logger.log(this, fieldname);
			Logger.log(this, type);
			
		
			String query = " select root(sop.uoid) as uoid , sop.question as q1, child.question as q2, child.internalPosition as pos, child.value as value "
                    + "from formobject as sop, formobject as child "
                    + "where sop.discriminator like \"%SOPInputField\" "
                    + "and sop.question = \"" + type + "\" " 
                    + "and child.parent = sop.uoid and child.value != \"\" and child.value not like \"%make a selection%\" and child.value not like \"%unknown%\" and child.value not like \"%-other-%\" and child.value not like \"%undefined%\"  "
                    + "and child.internalPosition > 0 group by child.uoid order by sop.uoid desc";
	
			Logger.log(this, query);


            List list = CoreObject.createSession().createSQLQuery(query)
                    .addScalar("uoid", Hibernate.LONG)
                    .addScalar("pos", Hibernate.LONG)
                    .addScalar("q1", Hibernate.STRING)
                    .addScalar("q2", Hibernate.STRING)
                    .addScalar("value", Hibernate.STRING).list();

            Iterator iterator = list.iterator();

            // valid SOP
            long tmp_expID = 0;
            String title = "";
            String desc = "";
            

            while (iterator.hasNext()) {
                Object[] objects = (Object[]) iterator.next();
                
                // check the access status
                
                try {
                    long pos = Long.parseLong(objects[1] + "");
                    long expID = Long.parseLong(objects[0] + "");
                    PromtUserAccessRight.checkAccess(user.getUOID(), expID, PromtUserAccessRight.READ);
                    if (pos < 1) throw new Exception ("illegal pos");

                    if (tmp_expID != expID){
                        if (tmp_expID > 0){
						%>
						<tr>
	                    	<th><b><%=Util.replace(title, '\n', "<br>")%></b>
	                    	<td width="3">
	                    	<%
	                    		// in case a field is defined itll be a link to modify the exp
	                    		if (fieldname != null && fieldname.startsWith("field")) {
		                    		String url = "";
		                    		%>
		                    		<form action="sop_predefined_mod.jsp" method="post">
		                    			<input type="hidden" name="fieldname" value="<%=fieldname %>">
		                    			<input type="hidden" name="sopLabel" value="<%=title%>">
		                    			<input type="hidden" name="sopDesc" value="<%=desc%>">
		                    			<input type="image" src="pics/go.gif">
		                    		</form>
		                    		<%
	                    		}
	                    	
	                    	%>
	                    <tr>
	                    	<td colspan="2"><%=Util.replace(desc , '\n', "<br>")%>
	                    			<p>
	                    		<font class="small"> SOP was defined in Experiment: [<%=new SXQuery().findPromtTitleByPromtID(expID) %> (<%=expID%>)].</font>
	                    		<br>
	                    <%
                        }
	                    tmp_expID = expID;
	                    title = "";
	                    desc = "";
                    }

                    if (pos == 1){
                        title = "" + (objects[4]) ;
                    } else {
                        desc = "" + (objects[4]) ;
                    }
                    
				} catch (Exception e){
				    // no access
                }
                
            }
%>
</body>
</html>