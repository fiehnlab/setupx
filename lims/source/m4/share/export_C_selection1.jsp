<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Vector"%>

<%

	String value = request.getParameter("add");


	Vector selectedIDs = null;
	try {
	    selectedIDs = (Vector)session.getAttribute("selectedClasses");
	} catch (Exception e){
	    
	}
	if (selectedIDs == null){
	    selectedIDs = new Vector();
	}
	
	int promtID = Integer.parseInt(value);
	Logger.log(this, "promt:" + promtID);
	
	List classIDs = new SXQuery().findClazzIDsByPromtID(promtID);
	Logger.log(this, "classes:" + classIDs.size());
	
	Iterator iterator = classIDs.iterator();
	while(iterator.hasNext()){
	    String classID = iterator.next() + "";
	    
	    // checking if at least ONE sample finished
	    Iterator iter =  new SXQuery().findSampleIDsByClazzID(Integer.parseInt(classID)).iterator();
	    boolean loop = true;
	    while (iter.hasNext() && loop){
	        long sampleID = Long.parseLong("" + iter.next());
	        if (new SXQuery().findScannedPair(sampleID).size() > 0){
	    	    Logger.log(this, "id: " +classID );
	    	    selectedIDs.add(classID);
	    	    loop = false;
	        } 
	    }
	}

	session.setAttribute("selectedClasses", selectedIDs);
%>


<meta http-equiv="refresh" content="0; URL=export_C_classselect.jsp">