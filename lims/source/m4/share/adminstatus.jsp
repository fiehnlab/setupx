<meta http-equiv="refresh" content="60; URL=adminstatus.jsp">
<%@ include file="checklogin.jsp"%>

<%@page import="org.setupx.repository.web.forms.PromtCreator"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<body>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">	
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Status </h2><br>Status of the lab.</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>




<%
java.util.Locale locale = java.util.Locale.US;
org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

%>

<%@page import="org.setupx.repository.server.persistence.PersistenceActionFindException"%>
<%@page import="org.setupx.repository.server.logging.Message"%>




<%@ include file="navi_admin.jsp"%>

<table align='center' border='0' cellpadding='4' width="60%">
<tr>
<th></th>
<th colspan="4">Status</th>
<th></th>
</tr>

<tr>
<td rowspan="201" valign="top"><%@ include file="navi_tree.jsp"%>
<td></td>
<td>lab status<br><font class="small">Progress of samples in the lab
<br>
</font></td>
<td colspan="3" align="right">				<%@ include file="incl_status_processbar.jsp"%>


 more info: <a href="sample_status_overview.jsp?show=true"><img  border='0' src="pics/resize_x_plus.gif"></a></td>
<td></td>
</tr>





<tr>
<td></td>
<td>samples measured<br><font class="small">	List of samples that have been meassuered
<br>
<% try { %>
<br>GC research <%=new Date(new File(Config.DIRECTORY_LOGFILES_MACHINE + File.separator + "sample-log-bart.bin").lastModified()).toLocaleString() %>
<br>GC core <%=new Date(new File(Config.DIRECTORY_LOGFILES_MACHINE + File.separator +  "sample-log-anabelle.bin").lastModified()).toLocaleString() %>
<% } catch (Exception e){} %>

</font></td>
<td colspan="3">
<!--   table containing the last samples that were finished  -->

<table width="100%"> 
<% 
hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();
Iterator iter = hqlSession.createSQLQuery("select label, sampleID  from scanned_samples where sampleID > 1000 and sampleID != \" \" order by uoid desc limit 3 ")
.addScalar("label", org.hibernate.Hibernate.STRING)
.addScalar("sampleID", org.hibernate.Hibernate.STRING)
.setCacheable(org.setupx.repository.Config.QUERY_CACHING_ACTIVE_JSP)
.list()
.iterator();

int i = 0;
while(iter.hasNext()){
Object[] array = (Object[])iter.next();

if (i%20 == 0) {%>
	<tr>
		<td></td>
		<th align="center">name</th>
		<th align="center" colspan="2">sample</th>
		<th align="center" colspan="2">experiment</th>
		<th align="center">time</th>
		<th align="center">label</th>
		<th align="center">details</th>
	</tr>
<%} 
i++;
int promtID = 0;
try {
    promtID = new SXQuery().findPromtIDbySample(Integer.parseInt("" + array[1])); 
}catch (Exception e){
    e.printStackTrace();
}%>
	<tr>
		<td></td>
		<td align="center"><%=array[0]%></td>
		<td align="center"><font class="small"><%=array[1]%></td>
		<td align="center" style="background-color:#FFFFFF"><a href="sample_detail.jsp?id=<%=array[1]%>"><img  border='0' src="pics/details.gif"></a></td>
		<td align="center"><font class="small"><%=promtID %></td>
		<td align="center" style="background-color:#FFFFFF"><a href="load?id=<%=promtID%>&action=20"><img  border='0' src="pics/details.gif"></a></td>
		<td align="center">
				
					<%
					try {
					    
						List list = hqlSession.createCriteria(Message.class).add(Expression.eq("relatedObjectID", new Long("" + array[1]))).addOrder(Order.asc("date")).list();
						Message message = (Message) list.get(list.size()-1); 
						
						Calendar midnight = new GregorianCalendar();
						midnight.set(midnight.get(Calendar.YEAR),midnight.get(Calendar.MONTH), midnight.get(Calendar.DATE),0,1);

						if (message.getDate().after(midnight.getTime())){
							// todays samples
							%>	
								<font class="small">
									<%=DateFormat.getTimeInstance(DateFormat.SHORT, Locale.US).format(message.getDate())%>
								</font>
							<%} else { %>
								<font class="small">
								  <%=java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT, locale).format(message.getDate()) %>
								</font>
							<%} 
					} catch (Exception e){
					
						// occours when messages can not be found
					}
					%>
		</td>
		<td style="background-color:#FFFFFF" align="center">
				<% try {
				    String labelX = new SXQuery().findSampleLabelBySampleID(Integer.parseInt("" + array[1]));
				    if (labelX.compareTo("-your comment-") == 0) labelX = "";
				    %>
					<a href="sample_detail.jsp?id=<%=array[1]%>"><%=labelX %></a>
				<% } catch (PersistenceActionFindException e){%>
					----
				<% }%>
		</td>
		<td align="center" style="background-color:#FFFFFF"><a href="run_detail.jsp?id=<%=array[1]%>"><img  border='0' src="pics/details.gif"></a></td>	
	</tr>
    <%
}
%>
	<tr>
		<td></td>
		<td align="center">...</th>
		<td align="center" colspan="5"> </th>
		<td align="center">more</th>
		<td align="center" style="background-color:#FFFFFF" ><a href="scanned_samples.jsp"><img  border='0' src="pics/resize_x_plus.gif"></a></th>
	</tr>

</table>
</td>
<td></td>
</tr>





<%@ include file="incl_processbars.jsp"%>




</table>

<%@ include file="footer.jsp"%>