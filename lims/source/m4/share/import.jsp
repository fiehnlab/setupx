<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<%@ page language="java" %>

<%@ page import="org.setupx.repository.core.util.hotfix.oldsamples.*" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
</head>


<%@ include file="navi_admin.jsp"%>

<body>
<table align='center' border='0' cellpadding='4' width="60%">
<% org.setupx.repository.core.communication.experimentgeneration.MetadataSource[] metadataSources = org.setupx.repository.core.communication.experimentgeneration.MetadataSourceFinder.newInstances(new java.io.File("/mnt/metabolomics/Scholz/sampleinformation/experiments"));

    for (int i = 0; i < metadataSources.length; i++) { 
    PromtConverter my_source = ((PromtConverter)metadataSources[i]);
    %>
  
  
<!-- entry per metadatasource -->  
	<tr>
	        <th>MetadataSource: <%=org.setupx.repository.core.util.Util.getClassName(my_source)%></th>
	</tr>
	
	<tr>
	        <td>Number of templates: <%=my_source.getClazzTemplates().length %></td>
	</tr>        

<!-- entry per template -->  
<% for (int j = 0; j < my_source.getClazzTemplates().length; j++){ %> 
	<tr>
	        <th colspan="21">Template <%=j%>: <%=org.setupx.repository.core.util.Util.getClassName(my_source.getClazzTemplates()[j])%></th>
	</tr>        
	<tr>
	        <td colspan="21">Number of Samples: <%=my_source.getClazzTemplates()[j].getSamples()%></td>
	</tr>        

<!-- entry per pathes -->  
<% String[] pathes = my_source.getPath();
	for (int p_i = 0; p_i < pathes.length; p_i++) {
	    %>
		<tr>
	        <td><b><%=pathes[p_i]%></b></td><td ><font size="-2"><%=my_source.getClazzTemplates()[j].getValue(pathes[p_i]) %></font></td>
		</tr><%
	} //path
} //template

org.setupx.repository.web.forms.inputfield.multi.Promt prom = metadataSources[i].createPromt();
%>

<tr>
<th colspan="21">Generated new Promt </th>
</tr>        


		<tr>
			<td colspan="21">clazz:</td>
		</tr>        
<%   
org.setupx.repository.web.forms.inputfield.multi.Clazz[] clazzes = prom.getClazzes();
for(int c_i = 0; c_i < clazzes.length; i++){
    %>
		<tr>
			<td>clazz <%=c_i %></td>
			<td>
			<table>
			  <tr>
			    <th></th>
			    <th>value</th>
			  </tr>
				<% org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz[]multiField4Clazzs = clazzes[c_i].getMultiField4Clazzs();
				for (int i_mf = 0 ; i_mf < multiField4Clazzs.length; i_mf ++){
				    
				%> 
			  <tr>
			    <td><%=multiField4Clazzs[i_mf].toStringHTML() %></td>
			    <td><b><%=multiField4Clazzs[i_mf].getValue() %></b></td>
			  </tr>
				<% } %> 
			</table>
			</td>
		</tr>        
    <%   
}
prom.assignClazzTemplates(metadataSources[i]);
%>

<tr>
<th colspan="21">Generated new Promt </th>
</tr>        
  
<%   


}//metadatasource
%>



</table>
</body>
</html>