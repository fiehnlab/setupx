<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBI_Classifier"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIClassifierID"%>
<%
	
	/** higher vertebrates (reptiles, birds and mammals) possessing an  amnion during development */
	final int ANIMAL_INDICATOR = 32524;
	
	/** Embryophyta<br> http://tolweb.org/tree?group=Embryophytes */
	final int PLANT_INDICATOR = 3193;
	
	/** Human<br> */
	final int HUMAN_INDICATOR = 9606;

	new NCBIClassifierID(1,NCBIEntry.MIRCOORGANISM).update(true);
	
	new NCBIClassifierID(9606,NCBIEntry.HUMAN).update(true);
	new NCBIClassifierID(63221,NCBIEntry.HUMAN).update(true);
	
	new NCBIClassifierID(3702,NCBIEntry.PLANT).update(true);
	new NCBIClassifierID(4113,NCBIEntry.PLANT).update(true);
	new NCBIClassifierID(58024,NCBIEntry.PLANT).update(true);
	new NCBIClassifierID(3352, NCBIEntry.PLANT).update(true);

	new NCBIClassifierID(3052, NCBIEntry.MIRCOORGANISM).update(true);
	new NCBIClassifierID(3166, NCBIEntry.MIRCOORGANISM).update(true);
	new NCBIClassifierID(4932,NCBIEntry.MIRCOORGANISM).update(true);
    new NCBIClassifierID(38881,NCBIEntry.MIRCOORGANISM).update(true);
    new NCBIClassifierID(2,NCBIEntry.MIRCOORGANISM).update(true);

    new NCBIClassifierID(HUMAN_INDICATOR, NCBIEntry.HUMAN).update(true);
    new NCBIClassifierID(ANIMAL_INDICATOR, NCBIEntry.ANIMAL).update(true);


    new NCBIClassifierID(7029,NCBIEntry.ANIMAL).update(true);
        
%>



<jsp:forward page="ncbi_classifyed.jsp"/>
