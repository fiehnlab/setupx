<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net" xmlns:c="urn:jsptld:http://java.sun.com/jstl/core">
<jsp:directive.page import="org.setupx.repository.core.util.FilenamePatternFilter"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"/>
<jsp:directive.page import="org.setupx.repository.core.util.logging.Logger"/>
<jsp:directive.page import="java.util.HashSet"/>
<jsp:directive.page import="java.io.File"/>

<jsp:directive.page contentType="text/html; charset=UTF-8" />
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
    <title>SetupX</title>
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>




<jsp:scriptlet>
// path
String path = request.getParameter("path");

long technofiletypeID = Long.parseLong(request.getParameter("technofiletype"));
TechnologyFileType technologyFileTypeParent = (TechnologyFileType)TechnologyFileType.persistence_loadByID(TechnologyFileType.class, technofiletypeID);


// ist all datafiles in this folder
Logger.debug(this, "creating file object \"" + path + "\"");
File folder = new File(path);

File[] files = folder.listFiles(new FilenamePatternFilter(technologyFileTypeParent.getPattern()));
// TODO / missing directories
Logger.debug(this, "number of files in directory \"" + folder + "\":" + files.length);

HashSet fileSet = new HashSet();
HashSet folderSet = new HashSet();
folderSet.add(folder.getParentFile());

for(int i = 0; i != files.length; i++){
    File file = files[i];
    if (file.isDirectory()){
//        Logger.log(this, "adding " + file);
        folderSet.add(file);
    } else {
//        Logger.log(this, "adding " + file);
        fileSet.add(file);
    }
}
</jsp:scriptlet>

	<jsp:include page="techno_header.jsp"></jsp:include>

	<h2>
		<a href="techno_techno.jsp">all platforms</a> - assigning file to <jsp:expression>technologyFileTypeParent.getLabel()</jsp:expression>
	</h2>
	Select the files that you want to assign to a sample.
	 
		



	<br/>
	<h6>directories</h6>
	If the file that you are looking for is not in this folder please check in the sub folders of <jsp:expression>path</jsp:expression>. <br/>
	
	<jsp:scriptlet>
	request.setAttribute("folder", folderSet ); 
	</jsp:scriptlet>

	<form action="techno_datafile_assign.jsp" method="get">	
	<display:table id="row" name="folder" pagesize="20" defaultsort="1" defaultorder="ascending" >
	    <display:column title="folder name" sortable="true" headerClass="sortable">
	     	  <![CDATA[<input type="radio" selected name="path" value=]]>"<jsp:expression>((File)row).getPath()</jsp:expression>"<![CDATA[/>]]><jsp:expression>((File)row).getPath()</jsp:expression>
	    </display:column>
	    <display:footer>
	      <tr>
	        <td colspan="2">	
	        	<![CDATA[<input type="hidden" name="technofiletype" value=]]>"<jsp:expression>technofiletypeID</jsp:expression>"<![CDATA[/>]]>
				<input type="submit" value="change directory"/>
			</td>
	      </tr>
	    </display:footer>
	</display:table>
	</form>

	<form action="techno_datafile_assign_check.jsp" method="get">	
		<![CDATA[<input type="hidden" name="technofiletype" value=]]>"<jsp:expression>request.getParameter("technofiletype")</jsp:expression>"<![CDATA[/>]]>
	<h6>files</h6>

	Please select the file that you want to assign to a sample.
	<jsp:scriptlet>
	request.setAttribute("files", fileSet ); 
	</jsp:scriptlet>


	<display:table name="files" id="row" pagesize="20">
	    <display:column>
	     	  <![CDATA[<input type="radio" name="file" value=]]>"<jsp:expression>((File)row).getPath()</jsp:expression>"<![CDATA[/>]]>
	    </display:column>
	    <display:column title="Name" property="name" sortable="true" headerClass="sortable"/>
	</display:table>
	<input type="submit" value="assign file"/>
	</form>

</jsp:root>


