<%@page import="java.util.Enumeration"%>
<%@page import="java.io.IOException"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFCell"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Vector"%>


<%@include file="incl_xls_header.jsp"%>

<%


/*
uploading a file to the experiment directory
*/

    Logger.debug(null, "upload files");

    MultipartRequest multiRequest;

    try {
      // temporary file - used to have a place 
      File _tmp;

      try {
        _tmp = File.createTempFile("xyz", ".tmp", null);
      } catch (IOException e) {
        throw new IOException("unable to create a temporary file");
      }

      _tmp.delete();

      File directory = _tmp.getParentFile();
      Logger.debug(null, "Saving uploaded files to " + directory.getAbsolutePath());
      multiRequest = new MultipartRequest(request, directory.getAbsolutePath(), 10424162);
    } catch (IOException e1) {
        
        e1.printStackTrace();
        Logger.debug(null, "the request is not a Multipartrequest");
        
      return;
    }

    Enumeration fileNames = multiRequest.getFileNames();
    int i = 0;

    while (fileNames.hasMoreElements()) {
      String filename = (String) fileNames.nextElement();
      i++;

      File file = multiRequest.getFile(filename);
      Logger.debug(null, "uploaded file [" + i + "]: " + file.getName() + "   " + file.getAbsolutePath());

      session.setAttribute("sourcefile", file); 
    }
    Logger.debug(null, "done with uploading the files. number of uploaded files: " + i);

    %>
    
    file has been uploaded. 
    <meta http-equiv="refresh" content="0; URL=xlsimport2.jsp">
    
 <%@include file="incl_xls_footer.jsp"%>
    