<%@page
	import="org.setupx.repository.core.communication.ncbi.NCBIFindException"%>
<%@page import="java.util.Vector"%>
<%@page
	import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.List"%>
<%@page
	import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>

<%@ include file="checknonlogin.jsp"%>
<%@page import="org.setupx.repository.web.forms.PromtCreator"%>
<%@page import="org.setupx.repository.Config"%>


<%

boolean cachedQueries = org.setupx.repository.Config.QUERY_CACHING_ACTIVE_JSP;

try {
	String cache = request.getParameter("cache");
    if (cache.compareTo("false") == 0){
        cachedQueries = false;
    }
} catch (Exception e){
    
}


%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<body>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">	
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Repository</h2><br>Content of the Repository</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>






<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} else { %>
<%@ include file="navi_public.jsp"%>
<%} %>


	<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td align="center">

<%

	// show the chart in 3D or 2D
	boolean IN3D = false;

	// limit the result to experiments that have at least ... experiments.
	int MINNUMBER_OF_EXPERIMENTS = 0;
	
    // query is used for the chart in repository_species
    String __query = "select label.value as value, count(promt.uoid) as c, sum(cache.numberofScannedSamples) as samples "
            + " from formobject as promt "
            + " inner join formobject as page "
            + " on page.parent = promt.uoid "
            + " inner join formobject as species "
            + " on species.parent = page.uoid  "
            + " inner join formobject as ncbispecies "
            + " on ncbispecies.parent = species.uoid  "
            + " inner join formobject as label  "
            + " on label.parent = ncbispecies.uoid  "
            + " inner join cache as cache "
            + " on cache.experimentID = promt.uoid "
            + " inner join formobject as f "
            + " on f.uoid = cache.experimentID "
            + " where ncbispecies.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\"   "
            + " and label.value != \"\"   "
            + " and cache.numberofScannedSamples > 0 "
            + " group by label.value " + " order by label.value ";
            
    Logger.log(this, "query: " + __query);

    List _list = CoreObject.createSession().createSQLQuery(__query)
            .addScalar("c", Hibernate.INTEGER).addScalar("value",
                    Hibernate.STRING).addScalar("samples",
                    Hibernate.INTEGER)
                    .setCacheable(cachedQueries)
                    .list();


    // ids of each kingdom
    int[] speciesType = new int[] { 
            	NCBIEntry.PLANT,
            	NCBIEntry.ANIMAL, 
            	NCBIEntry.MIRCOORGANISM, 
            	NCBIEntry.HUMAN
            	};	

    
    // loop over the kingdoms 
    for (int sp = 0; sp < speciesType.length; sp++) {
        int targetSpecies = speciesType[sp];
        
    	// data and values
    	String url_value_1 = "chd=t:";
    	String url_value_2 = "chd=t:";
    	String url_label_1 = "chl=";
    	String url_label_2 = "chl=";
    	boolean isFirst = true;
    	String speciesTypeName = "";
       		
	    Iterator __iterator = _list.iterator();
        while (__iterator.hasNext()) {
            Object[] objects = (Object[]) __iterator.next();

            String label = objects[1] + "";
            String value = objects[0] + "";
            String nr_samples = objects[2] + "";

             

            // run it throught the ncbi connector
            int ncbiID = NCBIConnector.determineNCBI_Id(label);
            try {
                NCBIEntry entry = NCBIConnector.determineNCBI_Information(ncbiID);
				if (entry.getSpeciesType() == targetSpecies){
	                speciesTypeName = entry.getSpeciesTypeName();
	                int numberOfExp = Integer.parseInt(value);
                    if (numberOfExp > MINNUMBER_OF_EXPERIMENTS
            	    	&&
            	    	label.compareTo("Bos taurus") != 0
            	    ){
            		    if (!isFirst) {
            		        url_value_1 = url_value_1.concat(",");
            		        url_value_2 = url_value_2.concat(",");
            		    }
            		    url_value_1 = url_value_1.concat(value);
            		    url_value_2 = url_value_2.concat((Integer.parseInt(nr_samples)/10	) + ""	 );
            		    
            		    if (!isFirst) {
            		        url_label_1 = url_label_1.concat("|");
            		        url_label_2 = url_label_2.concat("|");
            		    }
            		    
            		    url_label_1 = url_label_1.concat(label + " (" + value +  ")");
            		    url_label_2 = url_label_2.concat(label + " (" + nr_samples +  ")");
            		    
            		    if(isFirst) isFirst = false;
            	    }
                }
		    } catch (NCBIFindException exception) {
                exception.printStackTrace();
            }
        }
	String URL_START = "http://chart.apis.google.com/chart?";
	String CHART_NAME_1 = "chtt=experiments per species (" + speciesTypeName + ")";
	String CHART_NAME_2 = "chtt=samples per species (" + speciesTypeName + ")";
	String CHART_TYPE_PIE = "cht=p&chco=0000ff";
	if (IN3D) CHART_TYPE_PIE = "cht=p3&chco=0000ff";
	String CHART_RESOLUTION = "chs=600x300";
	if (IN3D) CHART_RESOLUTION = "chs=600x200";
	String AND = "&";
	
    // http://chart.apis.google.com/chart?cht=p3&chd=s:Uf9a&chs=200x100&chl=A|B|C|D
	String url1 = URL_START.concat(CHART_TYPE_PIE).concat(AND).concat(CHART_NAME_1).concat(AND).concat(url_value_1).concat(AND).concat(CHART_RESOLUTION).concat(AND).concat(url_label_1);
	String url2 = URL_START.concat(CHART_TYPE_PIE).concat(AND).concat(CHART_NAME_2).concat(AND).concat(url_value_2).concat(AND).concat(CHART_RESOLUTION).concat(AND).concat(url_label_2);
	%>
	
			<img src="<%=url1 %>">
			<br>
			<img src="<%=url2 %>">
			<br>
			<hr>
	<%
} 
%>
