	<%@ include file="checklogin.jsp"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.StringTokenizer"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.communication.file.SampleFile"%>
<%@page import="org.setupx.repository.core.communication.importing.logfile.ScannedPair"%>
<%@page import="org.setupx.repository.core.communication.importing.logfile.LogFileScanner2"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<body>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">	
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Repository</h2><br>Content of the Repository</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>


<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%}  %>


<%
	String sampleIDs = request.getParameter("sampleids");
	String fileIDs = request.getParameter("filenames");

	StringTokenizer sampleIDtokenizer = new StringTokenizer(sampleIDs, "\n");
	StringTokenizer fileIDtokenizer = new StringTokenizer(fileIDs, "\n");

    Logger.debug(this, "sampleIDs: " + sampleIDtokenizer.countTokens() + "  ");
	
	// check number of files vs. number of ids
	if (sampleIDtokenizer.countTokens() != fileIDtokenizer.countTokens()){
	    // err
	}
	

	// header
	%>
	<table align="center">
		<tr>
			<th align="center">
				sample ID				
			<th align="center">
				filename
			<th align="center">
				<%=SampleFile.getExtension(SampleFile.TXT) %>
			<th align="center">
				<%=SampleFile.getExtension(SampleFile.SMP) %>
			<th align="center">
				<%=SampleFile.getExtension(SampleFile.PEG) %>
			<th align="center">
				<%=SampleFile.getExtension(SampleFile.CDF) %>
			<th>
			    note
			<%
	
	// check if files do exist
	while(fileIDtokenizer.hasMoreElements()){
	    String fileID = fileIDtokenizer.nextToken();
	    String sampleID = sampleIDtokenizer.nextToken();
	    
	    fileID = fileID.replaceAll("[^a-zA-Z_0-9_:]", "");
	    sampleID = sampleID.replaceAll("[^a-zA-Z_0-9_:]", "");

	    
	    %>
	<tr>
    	<td align="center">
			<%=sampleID %>
    	<td align="center">
			<%=fileID %>
		<%

	    Logger.debug(this, "checking file: \"" + fileID + "\"");
		
		SampleFile sampleFile = new SampleFile(fileID);
	    
	    boolean exits = false;
	    
	    if (sampleFile.exists(SampleFile.TXT)) {
	        exits = true;
	    } else {
	        sampleFile = new SampleFile(fileID + ":1");
		    if (sampleFile.exists(SampleFile.TXT)) exits = true;
	    }
	    
	    String message;
	    if (exits){
	        // check for the ID
	        new LogFileScanner2().createScannedPair(sampleFile.getACQName(), Integer.parseInt(sampleID));
	        message = "valid and registed now.";
	    }  else {
	        message = "file can not be created cause file " + sampleFile.getFile(sampleFile.TXT).getPath() + " does not exist";
	    }
	    %>
	    	<td align="center">
	        	<%if (sampleFile.exists(SampleFile.TXT)){ %>
	        		<img src="pics/check.gif">
	        	<%} else { %>
	        		<img src="pics/circle.gif">
	        	<%} %>
	    	<td align="center">
	        	<%if (sampleFile.exists(SampleFile.SMP)){ %>
	        		<img src="pics/check.gif">
	        	<%} else { %>
	        		<img src="pics/circle.gif">
	        	<%} %>
	    	<td align="center">
	        	<%if (sampleFile.exists(SampleFile.PEG)){ %>
	        		<img src="pics/check.gif">
	        	<%} else { %>
	        		<img src="pics/circle.gif">
	        	<%} %>
	    	<td align="center">
	        	<%if (sampleFile.exists(SampleFile.CDF)){ %>
	        		<img src="pics/check.gif">
	        	<%} else { %>
	        		<img src="pics/circle.gif">
	        	<%} %>
	        <td>
	        	<font class="small"><%=message %></font>
		<%
	}
%>


</body>
</html>