<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net">
<jsp:directive.page import="org.setupx.repository.core.communication.document.DocumentCategory"/>
<jsp:directive.page import="java.io.File"/>
<jsp:directive.page import="java.util.Locale"/>
<jsp:directive.page import="java.text.DateFormat"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.setupx.repository.core.CoreObject"/>
<jsp:directive.page import="org.hibernate.Session"/>
<jsp:directive.page import="org.setupx.repository.core.communication.document.DocumentObject"/>
<jsp:directive.page import="org.setupx.repository.server.persistence.SXQuery"/>

<jsp:directive.page contentType="text/html; charset=UTF-8" />

<jsp:include page="checklogin.jsp"></jsp:include>

  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
  
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>

  <link rel="stylesheet" href="print.css" type="text/css" media="print" />
  
  </head>

  <body>
	<jsp:include page="sop_header.jsp"></jsp:include>
 
 	<h2>
		<a href="sop_index.jsp"><img src="pics/sop.gif" border="0"/> Categories of SOPs</a>
	</h2>
  

	<jsp:scriptlet>
		Session s = CoreObject.createSession();

		List l = DocumentCategory.persistence_loadAll(DocumentCategory.class, s);

		request.setAttribute( "test" , l); 
		
		DateFormat dFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US);	

		
	</jsp:scriptlet>

	<display:table name="test" id="row">
		<display:column href="sop_index.jsp?" paramId="catid" paramProperty="UOID"  title="Name" property="label" sortable="true"></display:column>
		<display:column title="Descr." property="descr" sortable="true"></display:column>
		
		<display:column title="in Exp. Definition" sortable="true">
	    	<jsp:scriptlet>
	    		String img = "";
	    		if (((DocumentCategory)row).isInSXFrontEnd()){
	    			img = "pics/check.gif";
	    		} else {
	    			img = "pics/circle.gif";
	    		}
	    	</jsp:scriptlet>

			<![CDATA[<a href="sop_category_alter.jsp?type=2&id=]]>
			<jsp:expression>((DocumentCategory)row).getUOID()</jsp:expression>				
			<![CDATA[">]]>
				<![CDATA[<img src=]]>"<jsp:expression>img</jsp:expression>"<![CDATA[/>]]>
			<![CDATA[</a>]]>
		</display:column>
		<display:column title="public" sortable="true">
			
	    	<jsp:scriptlet>
	    		String img = "";
	    		if (((DocumentCategory)row).isPublic()){
	    			img = "pics/check.gif";
	    		} else {
	    			img = "pics/circle.gif";
	    		}
	    	</jsp:scriptlet>

			<![CDATA[<a href="sop_category_alter.jsp?type=1&id=]]>
			<jsp:expression>((DocumentCategory)row).getUOID()</jsp:expression>				
			<![CDATA[">]]>
				<![CDATA[<img src=]]>"<jsp:expression>img</jsp:expression>"<![CDATA[/>]]>
			<![CDATA[</a>]]>


		</display:column>
		<display:column href="sop_category_remove.jsp?" paramId="id" paramProperty="UOID"><img src="pics/trash.gif"/></display:column>
	</display:table>


	<hr/>
	
	
	
create a new SOP Category:


<form action="sop_category_create.jsp">
	<table>
	
	  <tr>
	    <th>Label</th>
	  </tr>

	  <tr>
	    <td><input type="text" name="name"/></td>
	  </tr>

	  <tr>
	    <th>Description</th>
	  </tr>

	  <tr>
	    <td><input width="200" type="text" name="decr"/></td>
	  </tr>

	  <tr>
	    <td><input value="Create Category" type="submit"/></td>
	  </tr>

	</table>
</form>
		
	




</body>




</jsp:root>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
