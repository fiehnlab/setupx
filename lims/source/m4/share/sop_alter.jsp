<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%
	
	// sop_alter.jsp
	
	// attribute to alter
	// 1 - public
	// 2 - in sx frontend
	// 3 - ...
	
	Session s = CoreObject.createSession();
	
	int type = Integer.parseInt(request.getParameter("type"));
	
	// DocumentCategory to alter
	long id = Long.parseLong(request.getParameter("id"));
	DocumentObject object = (DocumentObject) DocumentObject.persistence_loadByID(DocumentObject.class,s, id);

	if (type == 1){
	    boolean value = object.isPubliclyAvailable();
		object.setPubliclyAvailable(!value);
	}

	
	object.update(s);

	// 
%>
	
<%@page import="org.setupx.repository.core.communication.document.DocumentObject"%>
<meta http-equiv="refresh" content="0; URL=sop_index.jsp">
