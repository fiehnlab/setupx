<%@ include file="checklogin.jsp"%>

<%@page import="edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass"%>
<%@page import="edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.core.communication.leco.ColumnInformationNotAvailableException"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.binbase.trigger.ClassSet"%>
<%@page import="org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector"%>
<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.core.communication.leco.ColumnDetector"%>
<%@page import="org.setupx.repository.core.communication.leco.ColumnInformation"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>


<% 	
	// import ?
	boolean importString = ((request.getParameter("import") + "").toLowerCase().compareTo("true") == 0);
	Logger.log(this, "import: " + importString);

	// export ?
	boolean exportString = ((request.getParameter("export") + "").toLowerCase().compareTo("true") == 0);
	Logger.log(this, "export: " + exportString);

	// export ?
	String col = request.getParameter("col");
	Logger.log(this, "col: " + col);
	
	// find the ID 
	long promtID = Long.parseLong(request.getParameter("promt"));
	Logger.log(this, "promt: " + promtID);

	
	ColumnInformation columnInformationForExport = new ColumnInformation(col, col, col); // ColumnDetector.getColumnInformationByID(columnID);
	%>




<%@page import="com.pavelvlasov.jsel.primitives.Int"%>
<form action="export_import.jsp">
	<table align="center" width="60%" cellpadding="5">	


<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width='60%'>
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Export</h2>
			Summary
			<br/>
			<font size="-2"></font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>


<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>







<%
Hashtable classes = new Hashtable();

Enumeration attributNames = request.getSession().getAttributeNames();

while (attributNames.hasMoreElements()){
    String name = attributNames.nextElement().toString();

    String value = request.getSession().getValue(name).toString();
    Logger.log(this, name + ": " + value);

	try {
	    Integer.parseInt("" + value);
	    // creating new empty hashset
	    
	    if (name.compareTo("pos") != 0 && !value.startsWith("-1") && name.compareTo("MAX_NR_GROUPS") != 0){
			if (!classes.containsKey("" + value)){
			    classes.put(""+value, new HashSet());
			    Logger.log(this, "creating group " + value);
			}

			HashSet set = (HashSet)classes.get("" + value);
		    set.add("" + name);
		    Logger.log(this, "adding " + name + " to " + value);
		    
	    }
	} catch (Exception e){
	    
	}
}

//checking - number of classes
int numberOfClasses = classes.size();
/*
if (classes.containsKey("-1")) {
    numberOfClasses = numberOfClasses--;
    Logger.log(this, "removed group with labels " + -1);
}
*/


ExperimentClass[] export_groups = new ExperimentClass[numberOfClasses];
%>


<table align='center' border='0' cellpadding='4' width='60%'>
		<tr>
	    	<td style="background-color: white" rowspan="41">
 	    	<td style="background-color: white" colspan="21" align="center">
 	    		<b>Export in Progress</b>
 	    		<br>
 	    		The Data is sent to BinBase right now. This process takes a while - please be patient and wait until BinBase completed the entire import/export which is the case
 	    		when you get the message "Finished - the data is exported and will be stored as regrouped_yourName.zip.". <br>
 	    		Then the export/import is completed and you can proceed.
 	    	</td>
		<tr>
 	    	<td style="background-color: white" colspan="21" align="center">Assigned Samples 

	    	
			


<% 
	Enumeration enumeration = classes.keys();
 	int classCount = -1;
	while (enumeration.hasMoreElements()){
		int i = Integer.parseInt(enumeration.nextElement() + "");
		Logger.debug(this, "key: " + i);
		classCount++;
		
	
	//for (int i = 0; i < classes.size(); i++){
	    HashSet set = (HashSet)classes.get("" +i);
	    if (set != null){
	    %>
	    <tr>
	    	<td style="background-color: white"><%=i+1 %>: <b><%=set.size() %> Samples</b>
	    	<%
				export_groups[classCount] = new ExperimentClass();
				export_groups[classCount].setId("" + i + "_" + new Date().getTime());
				Logger.debug(this, "group " + i + ": " + export_groups[classCount].getId());

				%>
					<font class="small">
						<br><b>Label:</</b> <%=export_groups[classCount].getId()%>
					</font>
				<%
				 
				ExperimentSample[] samples = new ExperimentSample[set.size()];	
	    		Iterator iter = set.iterator();
	    		int c = -1;
	    		while (iter.hasNext()){
	    		    String v = iter.next().toString() ;
	    		    c++;
	    			samples[c] = new ExperimentSample();
	    			Logger.debug(this, "looking for fileID for " + v);
	    			try {
		    			samples[c].setId("" + new SXQuery().findSampleIDByAcquisitionName(v));
		    			samples[c].setName(v);
	    			} catch (Exception e){
	    			    e.printStackTrace();
		    			samples[c].setId("-unknown-");
		    			samples[c].setName(v);
	    			}
	    			%>
	    		    <td><%=samples[c].getId()%><font class="small"><br><%=samples[c].getName()%></font>
	    		    <%
	    		}
    			
	    		export_groups[classCount].setSamples(samples);
	    	%>
	<%	} else {
	    Logger.debug(this, "we got a class with no content" );
		}
	}
%>

<%
Logger.debug(this, "columnInformation:" + columnInformationForExport);


ClassSet export_classset = new ClassSet(columnInformationForExport);

export_classset.setClasses(export_groups);
String filename = "regrouped_" + user.getUsername() + "_" + new Date().getTime();
export_classset.setId(filename);
export_classset.setIncrease(0);
try {
    export_classset.setEmail(user.getEmailString());
} catch (Exception e){
    e.printStackTrace();
}

// deprecated - new binbase export

// BinBaseServiceConnector binBaseSerivce = new BinBaseServiceConnector(Config.BINBASE_SERVER);
// binBaseSerivce.launchExport((ClassSet)export_classset, importString);


BinBaseServiceConnector binBaseSerivce = new BinBaseServiceConnector();

// start export
//    binBaseSerivce.launchExport((org.setupx.repository.core.communication.binbase.trigger.ClassSet)export_classset);    
try {
    binBaseSerivce.launchExport((org.setupx.repository.core.communication.binbase.trigger.ClassSet)export_classset, importString);
    %>


    		<tr>
     	    	<td style="background-color: white" colspan="21" align="center">

    			Finished - the data is exported and will be stored as <b><%=filename %>.zip</b>.
    			<br><br><br>
    			<code>
    				<%="binBaseSerivce.launchExport((ClassSet)export_classset, " + importString + ");" %>
    				<br>
    				<%=columnInformationForExport.getDescription()%>
    				<br>
    				<%=columnInformationForExport.getLable()%>
    				<br>				
    				Number of classes: <%=export_groups.length %>
    				<br>				
    				email delivery: <%=export_classset.getEmail()%>
    			</code>

    </table>

    <%@ include file="footer.jsp"%>
    <% 
} catch (Throwable e){
    e.printStackTrace();
    %>


	<tr>
	    	<td style="background-color: white" colspan="21" align="center">

		A problem occured.
		<br><br><br>
		<code>
		<%
		boolean loop = true;
		while (loop){
		    %>
			<%=e.getLocalizedMessage() %><font class="small"><%=e.getLocalizedMessage() %></font><br>
		    <%
		    
		    // show the other messages
		    if (e.getCause() != null){
			    e = e.getCause();
		    } else {
		        loop = false;
		    }
		}
		%>
		</code>
</table>

<%@ include file="footer.jsp"%>
<% }
%>
	