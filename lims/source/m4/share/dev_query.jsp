<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net">
  <jsp:directive.page contentType="text/html; charset=UTF-8" />
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.hibernate.Hibernate"/>
<jsp:directive.page import="org.setupx.repository.core.CoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.query.QueryMaster"/>
<jsp:directive.page import="org.setupx.repository.server.persistence.SXQuery"/>
<jsp:directive.page import="java.util.Vector"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
    <title>SetupX</title>
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>

<h2>header</h2>

<jsp:scriptlet>
	List l = CoreObject.createSession().createSQLQuery("select  remoteAddr, requestURI,  count(message.uoid) as c, avg(message) as a" +
	        " from message join WebAccess on message.relatedObjectID  = WebAccess.uoid join user on user.uoid = userID " + 
	        " group by remoteAddr, requestURI  order by count(message.uoid) desc")
	        .addScalar("remoteAddr", Hibernate.STRING)
	        .addScalar("requestURI", Hibernate.STRING)
	        .addScalar("c", Hibernate.STRING)
	        .addScalar("a", Hibernate.STRING)
	        .list();

	List users = UserDO.persistence_loadAll(UserDO.class, CoreObject.createSession());
	
	/*
	List newList = new Vector();
	
	Iterator iter = l.iterator();
	while (iter.hasNext()){
	    Object[] o = (Object[])iter.next();
	    String[] target = new String[o.length];
	    for (int i = 0; i != o.length; i++){
	    	target[i] = o[i] + "";
	    }
	    newList.add(target);
	}
	*/
	request.setAttribute("test", users); 

	// style
	pageContext.setAttribute("tableclass", "its");

	</jsp:scriptlet>


<display:table name="test" defaultsort="1" defaultorder="descending" class="${tableclass}">
  <display:column property="displayName"  sortable="true" headerClass="sortable" />
  <display:column property="organisation" sortable="true" headerClass="sortable" />
  <display:column property="password" sortable="true" headerClass="sortable" />
  <display:column property="phone" />
  <display:column property="username" />
  <display:column property="lastExperimentID" sortable="true" headerClass="sortable" />
  <display:column property="labtechician" />
  <display:column property="admin" />
  <display:column property="emailString" />
</display:table>




<br/>
<br/>
<br/>
<br/>



<display:table name="test" export="true" id="row" class="dataTable" defaultsort="1" defaultorder="ascending"
         pagesize="16" cellspacing="0" decorator="org.displaytag.decorator.TotalTableDecorator">
    <display:column property="displayName" title="Customer" sortable="true" group="1" class="customer"
         headerClass="customer" />
    <display:column property="labtechician" title="Order Number" sortable="true" group="2" class="orderNumber"
         headerClass="orderNumber" />
    <display:column property="lastExperimentID" title="Line Item Total" sortable="true" format="{0,number, currency}" total="true"
         class="lineItemTotal" headerClass="lineItemTotal" />
</display:table>
</jsp:root>

