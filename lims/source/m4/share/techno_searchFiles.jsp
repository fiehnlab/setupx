<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net">
<jsp:directive.page import="java.util.Calendar"/>
<jsp:directive.page import="org.setupx.repository.core.CoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Datafile"/>
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>

<jsp:directive.page import="java.util.Date"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"/>
<jsp:directive.page import="org.setupx.repository.Config"/>

<jsp:directive.page contentType="text/html; charset=UTF-8" />

<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<style type="text/css" media="all">
    @import url("maven-base.css");
    @import url("maven-theme.css");
    @import url("site.css");
    @import url("screen.css");
</style>

<link rel="stylesheet" href="print.css" type="text/css" media="print" />

  <head>
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>

	<jsp:include page="techno_header.jsp"></jsp:include>


	<jsp:scriptlet>
		char qMark = (char)38; // ascii 38 
		
		
		
		// list existing or nonassigned files
		boolean searchExistingDatafiles = true;
		
		try {
			if (request.getParameter("type").compareTo("1") == 0){
			    searchExistingDatafiles = false;
			}
		} catch (Exception e){
		    
		}

		
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -2);
		Date start = calendar.getTime();
		
		Date end = new Date();
		
	</jsp:scriptlet>

		<jsp:scriptlet>
			if (searchExistingDatafiles){
		</jsp:scriptlet>
			<h2><a href="techno_techno.jsp">all platforms</a> - Searchresult for all <a href="?type=1">stored</a> Datafiles</h2>
		for a the period from <jsp:expression>start.toLocaleString()</jsp:expression> until 	<jsp:expression>end.toLocaleString()</jsp:expression>
		<br/>Click <a href="?type=1">here</a> to list all found datafiles for that period.
		<jsp:scriptlet>
			} else {
		</jsp:scriptlet>
			<h2><a href="techno_techno.jsp">all platforms</a> - Searchresult for Datafiles that are <a href="?type=0">NOT</a> stored in the system</h2>
		for a the period from <jsp:expression>start.toLocaleString()</jsp:expression> until 	<jsp:expression>end.toLocaleString()</jsp:expression>
		<br/>Click <a href="?type=0">here</a> to list all datafiles that were not assigned to a sample for that period.
		<jsp:scriptlet>
			}
		</jsp:scriptlet>

	<jsp:scriptlet>


	// List datafiles
	
	
	List datafiles = null;
	
	if (searchExistingDatafiles){
	    datafiles = TechnologyProvider.getDatafiles(start, end);
	} else {
	    datafiles = TechnologyProvider.getPossibleDatafiles(start, end);
	}
	
	request.setAttribute("test", datafiles); 
	</jsp:scriptlet>
	
	
	<hr/>
	<form>
		<table>
			<tr>
				<th>
					Start
				</th>
				<th>
					End
				</th>
				<th>
				</th>
			</tr>
			
			<tr>
				<td width="30">
					<jsp:scriptlet>
						session.setAttribute("_d", start);
					</jsp:scriptlet>
					<jsp:include page="date_select.jsp" />
				</td>
				<td width="30">
					<jsp:scriptlet>
						session.setAttribute("_d", end);
					</jsp:scriptlet>
					<jsp:include page="date_select.jsp" />
				</td>
				<td>
				</td>
			</tr>
		</table>
	</form>

		
	<jsp:scriptlet>
	// style
	pageContext.setAttribute("tableclass", "its");
	</jsp:scriptlet>
	<jsp:include flush="true" page="inc_techno_datafile.jsp" />
	
</jsp:root>