<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.core.communication.mail.MailController"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="java.util.Vector"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Iterator"%>


<%@ include file="checklogin.jsp"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Share your experiment</h2><br>Manage access to experiment <%=request.getParameter("id") %>
		</th>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/connections.gif' height="100px" border="0" valign="middle" align="center">
			</a>
		</td>
	</table>

<%@ include file="navi_admin.jsp"%>

<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
	</tr>
	<tr>
		<td colspan="21">
			The following users have access to the experiment with the id: <%=request.getParameter("id") %>.
		</td>
	</tr>
	<tr>
		<th>
		<th>Name
		<th>eMail
		<th>
		<th>The experiment...

<%
	long expID = Long.parseLong(request.getParameter("id"));
	Vector userIDs = new Vector();
	Session hqlSession = CoreObject.createSession();

            Enumeration e = request.getParameterNames();
            int y = 0;
            
            // boolean if there are users to be created
            boolean newUser = false;

            %>
            <form action="usr_share_grant.jsp" method="post"> 	
            	<input type="hidden" name="id" value="<%=expID %>">
            <%
            
            while (e.hasMoreElements()) {
                y++;
                String parametername = (String) e.nextElement();
                String value = request.getParameter(parametername);
                Logger.info(this, "Name of Param " + y + " : " + parametername + "\t" + value);
                
                if (parametername.length() > 2 && value.length() > 2 && parametername.startsWith("term")){

                    // determine if user is known
                    List l = hqlSession.createSQLQuery("select * from user as u where u.displayName = \""+ value + "\" ").addEntity(UserDO.class).list();
                    UserDO userDO = null;
                    if (l.size() > 0){
                        userDO = (UserDO)l.get(0);
                        userIDs.add("" + userDO.getUOID());

                    } 
                    boolean access = false;
                    if (userDO != null){
                            // checking if user has access to experiment
                            try {
            					PromtUserAccessRight.checkAccess(userDO.getUOID(), expID, PromtUserAccessRight.READ);
            					access = true;
                            } catch (Exception ex) {
                                
                            }
                            
                            
                            // if the user is a master user we have to create it and set access to false
                            // becuase masterusers respond when checkAccess(...) always with true in order to grant them access to all experiments
                            if (userDO.isMasterUser(userDO.getUOID())){
                        	    PromtUserAccessRight.createFullAccess(expID, userDO.getUOID());	
                        	    access = false;
                        	    
                            }
                    }
                    
                    %>
                    <tr>
                    	<td><% if(userDO != null){ %><font style="font-size: xx-small;">SetupX user</font> <%} %>
                    	<td><%=value %>
                    	<td>
                    	<% 
                    	try {
								if(userDO != null) out.print(userDO.getEmailString().substring(0, userDO.getEmailString().indexOf('@')).concat("@xxxxxx.xxx")); 
	                    		else {
	                    			newUser = true;
	                    	%>
								<input type="text" name="email_<%=y%>">
								<input type="hidden" name="name_<%=y%>" value="<%=value%>">
	                    	<%}
                    	} catch (Exception expp){
                    	    expp.printStackTrace();
                    	}
                    	%>
                    	<%if (userDO == null){
                    	    %>
	                       	<td align="center"><img src="pics/user-del.gif"> 
	                       	<td>... is NOT accessible yet. We need to create an account. Please enter the email address.
	                       	<%
                    	} else if (!access){
                    	    
                    	    PromtUserAccessRight.createFullAccess(expID, userDO.getUOID());
            				MailController.inform(userDO.getMailAddress(), "Access granted on Experiment:"  +  new SXQuery().findPromtTitleByPromtID(expID), "\nHello " + userDO.getDisplayName() + ","
            				        + "\n" + user.getDisplay() + "("+ user.getEmailString() + ") granted you access to the experiment: "
            				        + "\n\ntitle:\n" +new SXQuery().findPromtTitleByPromtID(expID) 
            				        + "\n\nabstract:\n" + new SXQuery().findPromtAbstractByPromtID(expID)
            				        + "\n\nIf you have any further questions feel free to contact us." 
            				        + "\n" + Config.SYSTEM_EMAIL);


                    	    %>
	                       	<td align="center"><img src="pics/user-add.gif"> 
	                       	<td>... is now accessible for <%=value%>.
	                       	<%
                    	} else {
                    	    %>
	                       	<td align="center"><img src="pics/user.gif"> 
	                       	<td>... was accessible for <%=value%> already. We did not have to grant access.
	                       	<%
	                   	}%>
                    </tr>
                    <%                    
                }
            }
            
            if (newUser){
                %>
                <tr>
                	<td>
                	<td colspan="4">In order to grant user that do not have an account on <%=Config.SYSTEM_NAME %> access to this experiment, please enter the persons email address and we will email them all information required to login and to access this experiment.
                <tr>
                	<td>
                	<td colspan="4"><input type="submit"> 
                </form> 	
			<%                
            }
            
            Logger.info(this, "Total Parameters : " + y);
	// assigned users
	List assidnedUsers = hqlSession.createSQLQuery("select * from user as u join accessright as a on a.userID = u.uoid and a.experimentID = " + expID + " and a.accessCode > 20").addEntity(UserDO.class).list();


	// only if existing exist
	if (assidnedUsers.size() > 0){
	%>
    <tr>
    	<th colspan="21">
    		Existing users
    <tr>
    	<td colspan="21">
    		The following users have had access to the experiment before already.
    		


	<%

	
	Iterator iter = assidnedUsers.iterator();
	
	while(iter.hasNext()){
	    UserDO userDO = (UserDO)iter.next();
	    if (!userIDs.contains("" + userDO.getUOID()) && (userDO != null) ){
	        
        %>
            <tr>
        	<td><font style="font-size: xx-small;">SetupX user</font>
        	<td><%=userDO.getDisplayName() %>
        	<td><%if(userDO.getEmailString() != null) out.print(userDO.getEmailString().substring(0, userDO.getEmailString().indexOf('@')).concat("@xxxxxx.xxx")); %>
          	<td align="center"><img src="pics/user.gif"> 
	        <td>... was shared with <%=userDO.getDisplayName()%> before already.
	        <td style="background-color: white;">
       		<%
  				if (UserDO.isMasterUser(userDO.getUOID())){

   				} else if (userDO != null && userDO.isLabTechnician()) {

   				} else { 
   					%> <a href="usr_share_remove.jsp?id=<%=expID%>&user=<%=userDO.getUOID() %>"><img border='0' src="pics/trash.gif"></a> <%
  			    } %>
	        </td>
        </tr>
		<%
	    }
	}
	}
%>
    <tr>
    	<th colspan="21">
    		Add users
    <tr>
    	<td colspan="21">
    		You can add additional users by <a href="usr_ajax_query.jsp?id=<%=expID %>">sharing this experiment with them here</a>.

    		
</table>
    		
    		<%MailController.sendNonsendMails(); %>


<%@ include file="footer.jsp"%>





