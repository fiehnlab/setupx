<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.setupx.repository.web.forms.PromtCreator"%>
<%@page import="org.setupx.repository.server.logging.Message"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.forms.InsufficientUserRightException"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>
<%@ include file="checklogin.jsp"%>
<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<% 
	UserDO _user = (UserDO)session.getAttribute(WebConstants.SESS_USER);

	// checking if the user is allowed to delete experiments 
	if (_user.isSuperUser() && _user.isMasterUser(_user.getUOID())){
		// get the experiment ID 
		long id = Long.parseLong("" + request.getParameter("id"));
		boolean delete = new Boolean("" + request.getParameter("total")).booleanValue();
				
		// get the experiment
		Promt _promt = Promt.load(id,true);
		
		// find all promtuseraccessrights
		List access = new SXQuery().findPromtUserAccessRightForPromtID(id);
		Iterator accessiterator = access.iterator();
		while (accessiterator.hasNext()){
		    PromtUserAccessRight accessRight = (PromtUserAccessRight)accessiterator.next();
		    %>
		    Deleting UserAccessRight for user <b><%=accessRight.getUOID()%></b> with access Level <b><%=accessRight.getAccessCode()%></b>.<br>
		    <% 
			new Message(user, user.getDisplay() + " removed access for user "  + accessRight.getUserID() + " for experiment " + accessRight.getExperimentID()).update(true);
		    accessRight.remove();
		}
		
		if (delete){
			// delete it!
			new Message(_user, _user.getDisplay() + " deleted " + _promt.getUOID() + "  ").update(true);
			
			// done.
			_promt.remove();
		}
	} else {
		throw new InsufficientUserRightException("user has isufficient right to access this function of the system.");
	}
	%>

<jsp:forward page="main.jsp"/>
