<%@page import="java.util.List"%>
<%@page import="java.io.FileFilter"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.Iterator"%>


<%@page import="org.setupx.repository.Config"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Vector"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>



<%
	Session hqlSession = CoreObject.createSession();

	long promtID = 0;
	
	try {
	    promtID = Long.parseLong(request.getParameter("id"));
	} catch (Exception e){
	 	   
	}

	File methodSelected = new File(request.getParameter("method"));
	
	File[] allMethod = new File(Config.DIRECTORY_LTQ_METHODS).listFiles();

	List sampleIDs = new SXQuery(hqlSession).findSampleIDsByPromtID(promtID);

	File targetDirectory = new File("/mnt");
	
	session.removeAttribute("exList");
	session.setAttribute("exList", new Vector());
	
	String filerWord = request.getParameter("filter") ;
	if (filerWord.compareTo("null") == 0){
	    filerWord = "";
	}

	
%>



<form action="seq_ltq_confirm2.jsp" method="post">



	
<table align="center" width="600" cellpadding="5">
	<tr>
		<td>
			<table>
				<tr>
					<th>Exit</th>
					<th width="90%">&nbsp;</th>
					<th>Administration</th>
				</tr>
				<tr>
					<td style="background-color:#FFFFFF" align="center"><a href="login.jsp"><img src='pics/go_exit.gif' border="0" valign="middle" align="center"></a></td>
					<td style="background-color:#FFFFFF" align="center"></a></td>
					<td style="background-color:#FFFFFF" align="center"><a href="admin.jsp"><img src='pics/go.gif' border="0" valign="middle" align="center"></a></td>
				</tr>
			</table>					
	<tr>
		<th>
			<h1>Experiment: <%=promtID%>
			<h2><%=new SXQuery(hqlSession).findPromtTitleByPromtID(promtID) %></h2>

	<tr>
		<td>
			<h3>Target folder for datafiles: <%=targetDirectory.getAbsolutePath()%></h3>
			
	<tr>
		<td>
				<h2>Starting position</h2>
				Please enter the offset of the first sample on the tray.
				<input type="text" name="startPos" value="8" />
				
	<tr>
		<td>
				<h2>Randomise</h2>
				Should the samples be randomised:
				<input type="checkbox" name="random" value="true" />
				
	<tr>
		<td>
				<h2>Randomise</h2>
				In case the sampels do not fit on a single tray, should they be randomised all over or only within the tray itself?
				<input type="checkbox" name="randomTotal" value="true" />
				
	<tr>
		<td>
				<h2>Tray</h2>
				Please select the type of tray
				<br>
				<input type="radio" name="tray" checked="checked" value="96" />96 Tray (pos: A1 - H12)<br>
				<input type="radio" name="tray" value="360" />360 Tray (pos: A1 - P24)<br>
			
				<input type="hidden" name="id" value="<%=request.getParameter("id")%>">
			
	<tr>
		<td>
			<input type="submit" value="download sequence">
			
	<tr>
		<td>
			<table border="1">
			  <tr>
			    <th>incl.<br>in seq</th>
			    <th>sample ID
			    <th>sample Label
			    <th>method
			  </tr>
			  
			  
				<% 
					boolean randomise = false;
					try {
						randomise = (request.getParameter("random").compareTo("true") == 0);
					} catch (Exception e){
					    
					}
				
					String selected = "";
					Iterator iterator = sampleIDs.iterator();
					while(iterator.hasNext()){
					    long sampleID = Long.parseLong(iterator.next().toString());	 
					    String label = new SXQuery(hqlSession).findSampleLabelBySampleID(sampleID);
					    String checked = "";

					    if (label.lastIndexOf(filerWord) > -1) {
						    checked = "checked=\"checked\"";
					    }
				%>
				  <tr>
				    <td><input type="checkbox" value="true" <%=checked %> name="active<%=sampleID %>"></td>
				    <td><%=sampleID %></td>
				    <td style="font-size: xx-small;"><%=label %></td>
				    <td>
				   		<select name="method<%=sampleID %>">
				    <%
				    	// method selection
				    %>
				   		<% for (int x = 0; x < allMethod.length; x++){ 
				   			if (allMethod[x].getName().compareTo(methodSelected.getName()) == 0) {
				   			    selected = "selected=\"selected\"";
				   			} else {
				   			    selected = "";
				   			}
				   			%>
							<option value="<%=allMethod[x].getName() %>" <%=selected %>><%=allMethod[x].getName() %></option>
							<%
						} %>
				  </tr>
				<%
					}
				%>
			  </form>
			</table>
</table>

