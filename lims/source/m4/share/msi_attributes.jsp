<%@ include file="checknonlogin.jsp"%>
<%@ include file="incl_header_msi.jsp"%>
<br>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page
	import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Date"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>

<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<table align="center" width="60%" cellpadding="3" >

<tr>
	<th colspan="3" style="background-color: gray;">
		<h3>List of offical MSI Attributes</h3>
		The following attributes have been created officially released by the Metabolomics Standards Initiative (MSI) as of <%=new SimpleDateFormat("MM/dd/yyyy").format(new Date())%>.  
	</td>
</tr>

	<!-- dev_msiattribute.jsp  -->

<%
Session s = CoreObject.createSession();

boolean deleted = false; 
try{
	long removeID = Long.parseLong(request.getParameter("remove"));
	MSIAttribute attribute = ((MSIAttribute)CoreObject.persistence_loadByID(MSIAttribute.class, s, removeID));
	
	%><tr><th colspan="31" style="background-color: red">MSI Attribute <%=attribute.getMsiLabel() %> has been deleted.</th><%
	
	attribute.remove(s);
	deleted = true;
	
} catch (Exception e){
    
}

MSIAttribute attribute = null;
long id = 0;
try {
    id = Long.parseLong(request.getParameter("modify"));
    Logger.debug(this, " found ID " + id + " so just updating");
} catch(Exception e){
    Logger.debug(this, " cant find id - so creating new MSI Attribute");
}
if (id != 0){
    attribute = (MSIAttribute)UserLocal.persistence_loadByID(MSIAttribute.class, s, id);
} else {
    attribute = new MSIAttribute();
}

attribute.setMsiLabel(request.getParameter("label"));
attribute.setQuestion(request.getParameter("question"));
attribute.setDescribtion(request.getParameter("description"));
attribute.setExtension(request.getParameter("extension"));
attribute.setComment(request.getParameter("comment"));
attribute.setUserID(user.getUOID());
attribute.setVersion(new Date());


if (user.isMasterUser(user.getUOID()) ){
	try{
	    attribute.checkConsistency();
	    attribute.update(true);
	} catch (Exception e){
	    
	    if (!deleted) {
	        // show only if nothing was deleted before...
	    	%><tr><th colspan="31" style="background-color: red">MSI Attribute has not been stored: <%=e.getMessage() %></th><%
	    }
	}
} 
%>

	<tr>
		<th>
		<th>
		<th>


	<%
	String msi_Query = "select * from msiattribute order by msiLabel";
	
	List attributes =  CoreObject.createSession().createSQLQuery(msi_Query).addEntity(MSIAttribute.class).list();
	// CoreObject.persistence_loadAll(MSIAttribute.class, s);

Iterator iterator = attributes.iterator();
while (iterator.hasNext()){
    attribute = (MSIAttribute)iterator.next();
    %>
	<tr valign="top">

	    <%@ include file="incl_msiattribute.jsp"%>

		<td valign="top">
			<a name="<%=attribute.getUOID() %>">
			<table width="100%" align="center">	
				<tr align="center">
					<td style="background-color: white;" width="4" align="center">
						<a href="msi_detail_attribute.jsp?id=<%=attribute.getUOID() %>"><img src="pics/details.gif" border="0"  style="background-color: white;"></a> 
					<td>
						details 
						<font style="font-size: x-small"><nobr>(login required)</nobr></font>

				<% if(user.isLabTechnician() || user.isMasterUser(user.getUOID()) ) { %>
				<tr>
					<td style="background-color: white;" width="4" align="center">
						<form action="#mod" method="post">
							<input type="hidden" value="<%=attribute.getUOID() %>" name="modify">
							<input type="hidden" value="<%=attribute.getComment() %>" name="comment">
							<input type="hidden" value="<%=attribute.getExtension() %>" name="extension">
							<input type="hidden" value="<%=attribute.getMsiLabel() %>" name="label">
							<input type="hidden" value="<%=attribute.getDescribtion() %>" name="description">
							<input type="hidden" value="<%=attribute.getQuestion() %>" name="question">
							<input type="image" src="pics/select-grille.gif" border="0"  style="background-color: white;"/>
						</form>
					<td>edit
											<font style="font-size: x-small"><nobr>(login required)</nobr></font>
					
				<tr>
					<td style="background-color: white;" width="4" align="center">
						<a href="?remove=<%=attribute.getUOID() %>"><img src="pics/x.gif" border="0"  style="background-color: white;"></a>
					<td>remove
				<tr>
					<td style="background-color: white;" width="4" align="center">
						<a href="msi_sx_msi_map.jsp"><img src="pics/move_right.gif" border="0"  style="background-color: white;"></a>
					<td>assign <%=org.setupx.repository.Config.SYSTEM_NAME%> attributes
					<%} %>
				<tr>
					<td style="background-color: white;" width="4" align="center">
						<a href="msi_attribute_xml.jsp?id=<%=attribute.getUOID() %>"><img src="pics/xml3.gif" border="0"  style="background-color: white;"></a>
					<td>Attribute as XML
			</table>
	</tr>


	<%
}
%>

	<% if(user.isLabTechnician() || user.isMasterUser(user.getUOID()) ){ %>
	<form action="#<%=request.getParameter("modify") %>" method="post">


	<tr>
		<td colspan="21"><a name="mod">
		<table width="100%">
			<tr><td><b>MSI Label: <td><input type="text" name="label" value="<%=request.getParameter("label") %>" >
			<%
			try {
				%>
				<tr><td>uoid: <td><input type="text" name="modify" value="<%=request.getParameter("modify") %>" readonly="readonly">
				<%
			} catch( Exception e){
				%>
				<tr><td>uoid: <td><input type="text" value="- none -" readonly="readonly">
				<%
			}
			%>
			<tr><td>Question:<td><input type="text" name="question" value="<%=request.getParameter("question") %>" >
			<tr><td>Descr.:<td><textarea cols="90" rows="10" name="description"><%=request.getParameter("description") %></textarea>
			<tr><td>Comment:<td><textarea cols="90" rows="10" name="comment"><%=request.getParameter("comment") %></textarea>
			<tr><td>Extension<td><input type="text" name="extension"  value="<%=request.getParameter("extension") %>" >
			<tr><td>user<td><%=user.getDisplayName() %>
			<tr><td><td><input type="submit">
	</tr>
	</form>
	<% } %>

</table>	
<br>	
<%@ include file="incl_footer_msi.jsp"%>

	