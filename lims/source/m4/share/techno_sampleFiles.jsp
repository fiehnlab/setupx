<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net" xmlns:c="urn:jsptld:http://java.sun.com/jstl/core">
<jsp:directive.page import="java.net.URL"/>
<jsp:directive.page import="org.setupx.repository.server.persistence.SXQuery"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"/>
<jsp:directive.page import="org.hibernate.Session"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Datafile"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Technology"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.hibernate.Hibernate"/>
<jsp:directive.page import="java.util.Vector"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>

  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>


	<jsp:include page="techno_header.jsp"></jsp:include>

	<jsp:scriptlet>
	char qMark = (char)38; // ascii 38 

	long sampleID = Long.parseLong(request.getParameter("id"));

	// get all datafiles for this sample

	Session s = TechnoCoreObject.createSession(); 
	List list = Datafile.findDataFilesBySampleID(sampleID, s);

	//
	request.setAttribute("test", list); 

	// style
	pageContext.setAttribute("tableclass", "its");

</jsp:scriptlet>


	<jsp:scriptlet>
		int classID = 0;
		int promtID = 0;
		String experimentName = "-unknown-";
		try {
			classID = new SXQuery().findClazzIDBySampleID((int)sampleID);
			promtID = new SXQuery().findPromtIDbyClazz(classID);
			experimentName = new SXQuery().findPromtTitleByPromtID(promtID);
		} catch (Exception e){
		}
		
	</jsp:scriptlet>

	<h2>
		<a href="techno_techno.jsp">all platforms</a> - 
		
		<![CDATA[<a href=]]>"techno_sampleFiles_promt.jsp?id=<jsp:expression>promtID</jsp:expression>"<![CDATA[>]]>
		experiment <jsp:expression>promtID</jsp:expression>
		<![CDATA[</a>]]> -
		
		<![CDATA[<a href=]]>"sample_detail.jsp?id=<jsp:expression>sampleID</jsp:expression>"<![CDATA[>]]>
		sample <jsp:expression>sampleID</jsp:expression>
		<![CDATA[</a>]]>
	</h2>
	
	<![CDATA[<a href=]]>"sample_detail.jsp?id=<jsp:expression>sampleID</jsp:expression>"<![CDATA[>]]>
	view sample in LIMS
	<![CDATA[</a>]]> -
	 - <a href="#assign">assign files</a>
	 - <a href="#download">download datafiles</a>
	
	<hr/>
	
	This sample is part of class (<jsp:expression>classID</jsp:expression>) in the experiment 
	
	
	<![CDATA[<a href=]]>"techno_sampleFiles_promt.jsp?id=<jsp:expression>promtID</jsp:expression>"<![CDATA[>]]>
	"<jsp:expression>experimentName</jsp:expression>" (<jsp:expression>promtID</jsp:expression>). <img src="pics/details_small.gif"/>
	<![CDATA[</a>]]>
	
	
	<br/>
	The following files are assigned to the sample <jsp:expression>sampleID</jsp:expression>:
	<br/>
	
   	<jsp:include page="inc_techno_datafile.jsp"></jsp:include>



	<a id="assign"/>
	<table>
		<tr>
			<th>
				Assign additional files to sample
			</th>
		</tr>
		<tr>
			<td>
				If there are files that you want to add to this <b>sample <jsp:expression>sampleID</jsp:expression></b>, please use the wizzard.
			</td>
		</tr>
		<tr>
			<td>
				<![CDATA[<a href="techno_msample_assign_create.jsp?pattern=&sampleID=]]>
				<jsp:expression>sampleID</jsp:expression>				
				<![CDATA[">]]>
				<img src="pics/go.gif"/>
				<![CDATA[</a>]]>
			</td>
		</tr>
	</table>





	<a id="download"/>
	<table>
		<tr>
			<th colspan="21">
				Download datafiles
			</th>
		</tr>
		<tr>
			<td colspan="21">
				All files assigned to this <b>sample <jsp:expression>sampleID</jsp:expression></b> can be downlaoded. 
				<br/>
				Please select which files you want to receive. 
				<br/>
				(If you want to download only a certain datafile please use the download button 
				behind the actual datafile above.
			</td>
		</tr>
		<tr>
			<th>
			</th>
			<th>
			</th>
			<td width="10" align="center">
				<nobr>
					Experiment
					<font style="font-size: xx-small"><br/><jsp:expression>promtID</jsp:expression></font>			
				</nobr>
			</td>
			<td width="10" align="center">
					Class
					<font style="font-size: xx-small"><br/><jsp:expression>classID</jsp:expression></font>			
			</td>
			<td width="10" align="center">
					Sample
					<font style="font-size: xx-small"><br/><jsp:expression>sampleID</jsp:expression></font>			
			</td>
			<td>
			</td>
		</tr>



			<jsp:scriptlet>
				// request: 
			    // t=class ... 		determining which group it is / classes [file, sample, class, prompt ]
			    // id=41223 	ID of the object
			    // zip=true 	zip it or not
			    // files=all	all files [all, 4"ID of the filetype"]

				Iterator technologiesIterator = TechnologyProvider.getTechnologies().iterator();
				while(technologiesIterator.hasNext()){
				    Technology technology = (Technology)technologiesIterator.next();
				    Iterator technoFileTypeIterator = technology.getTechnologyFileTypes().iterator();
				    while (technoFileTypeIterator.hasNext()){
				        TechnologyFileType technologyFileType = (TechnologyFileType) technoFileTypeIterator.next();
					    String files = "files=" + technologyFileType.getUOID();

						if (technologyFileType.getDatafiles(sampleID).size() != 0 ){
						</jsp:scriptlet>
							<tr>
								<td width="10">
									<nobr>
										<jsp:expression>technologyFileType.getTechnology().getLabel()</jsp:expression>
										
										<font style="font-size: xx-small"><br/><jsp:expression>technologyFileType.getTechnology().getDescription()</jsp:expression></font>			
									</nobr>
								</td>
								<td width="10">
									<nobr>
										<jsp:expression>technologyFileType.getLabel()</jsp:expression>				
										<font style="font-size: xx-small"><br/><jsp:expression>technologyFileType.getDescribtion()</jsp:expression></font>			
									</nobr>
								</td>


								
								<jsp:scriptlet>
									String t = "t=promt";
									String id = "id=" + new SXQuery().findPromtIDbySample(sampleID);
			
									String urlParameter = t + qMark + id + qMark + files;
								</jsp:scriptlet>
								<td >
									<![CDATA[<a href="techno_download.jsp?]]>
									<jsp:expression>urlParameter</jsp:expression>				
									<![CDATA[">]]>
									<img src="pics/go_download.gif"/>
									<![CDATA[</a>]]>
								</td>

								<jsp:scriptlet>
									t = "t=class";
									id = "id=" + new SXQuery().findClazzIDBySampleID((int)sampleID);
			
									urlParameter = t + qMark + id + qMark + files;
								</jsp:scriptlet>
								<td >
									<![CDATA[<a href="techno_download.jsp?]]>
									<jsp:expression>urlParameter</jsp:expression>				
									<![CDATA[">]]>
									<img src="pics/go_download.gif"/>
									<![CDATA[</a>]]>
								</td>

								<jsp:scriptlet>
									t = "t=sample";
									id = "id=" + sampleID;
			
									urlParameter = t + qMark + id + qMark + files;
								</jsp:scriptlet>
								<td >
									<![CDATA[<a href="techno_download.jsp?]]>
									<jsp:expression>urlParameter</jsp:expression>				
									<![CDATA[">]]>
									<img src="pics/go_download.gif"/>
									<![CDATA[</a>]]>
								</td>
							</tr>
						<jsp:scriptlet>
						} 
				    }
				} // end of looping over technos and filestypes
				// this is for all 
				String files = "files=all";

				</jsp:scriptlet>


			<tr>
				<td width="10">
					<nobr>
						All
						<font style="font-size: xx-small"><br/>all technologies</font>			
					</nobr>
				</td>
				<td width="10">
					<nobr>
						All				
						<font style="font-size: xx-small"><br/>all filetypes</font>			
					</nobr>
				</td>


								<jsp:scriptlet>
									String t = "t=promt";
									String id = "id=" + new SXQuery().findPromtIDbySample(sampleID);
			
									String urlParameter = t + qMark + id + qMark + files;
								</jsp:scriptlet>
								<td >
									<![CDATA[<a href="techno_download.jsp?]]>
									<jsp:expression>urlParameter</jsp:expression>				
									<![CDATA[">]]>
									<img src="pics/go_download.gif"/>
									<![CDATA[</a>]]>
								</td>

								<jsp:scriptlet>
									t = "t=class";
									id = "id=" + new SXQuery().findClazzIDBySampleID((int)sampleID);
			
									urlParameter = t + qMark + id + qMark + files;
								</jsp:scriptlet>
								<td >
									<![CDATA[<a href="techno_download.jsp?]]>
									<jsp:expression>urlParameter</jsp:expression>				
									<![CDATA[">]]>
									<img src="pics/go_download.gif"/>
									<![CDATA[</a>]]>
								</td>

								<jsp:scriptlet>
									t = "t=sample";
									id = "id=" + sampleID;
			
									urlParameter = t + qMark + id + qMark + files;
								</jsp:scriptlet>
								<td >
									<![CDATA[<a href="techno_download.jsp?]]>
									<jsp:expression>urlParameter</jsp:expression>				
									<![CDATA[">]]>
									<img src="pics/go_download.gif"/>
									<![CDATA[</a>]]>
								</td>
			</tr>
	</table>
</jsp:root>





