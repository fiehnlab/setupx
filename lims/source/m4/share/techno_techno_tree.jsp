<%@page import="java.util.HashSet"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Technology"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<jsp:include page="techno_header.jsp"></jsp:include>

  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>

  <link rel="stylesheet" href="print.css" type="text/css" media="print" />
	
	<%
    Hashtable roots = new Hashtable();
	// requires one hashtable 
    Iterator iterator = TechnologyProvider.getTechnologies().iterator();
	while(iterator.hasNext()){
	    Technology technology = (Technology)iterator.next();
	    Hashtable childs = new Hashtable();
	    Iterator iterator2 = technology.getTechnologyFileTypes().iterator();
		while(iterator2.hasNext()){
		    TechnologyFileType technologyFileType = (TechnologyFileType)iterator2.next();
		    String name = "<a href=\"techno_technologyfiletype.jsp?id=" + technologyFileType.getUOID() + "\"><font style=\"font-size: large\">" + technologyFileType.getLabel() + "</font></a>";
		    //name = name.concat("<br><font style=\"font-size: small\">" + technologyFileType.getDescribtion() + "</font>");
		    name = name.concat("<br><font style=\"font-size: xx-small;\">" + technologyFileType.getDatafiles().size() + " files</font>");
			childs.put(name, new Hashtable());
		}
	    String name = "<a href=\"techno_technology.jsp?id=" + technology.getUOID() + "\"><font style=\"font-size: large\">" + technology.getLabel() + "</font></a>";
	    name = name.concat("<br><font style=\"font-size: small\">" + technology.getDescription() + "</font>");
		roots.put(name, childs);
	}
	%>
	
	<h2>
		<a href="techno_techno.jsp">all platforms</a> - all Technologyfiletypes
	</h2>
	
	<table>	
		<tr>
			<td>
					<%@ include file="incl_orga_chart.jsp"%>
			</td>
		</tr>
	</table>
		