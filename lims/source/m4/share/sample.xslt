<!-- edited with XMLSPY v5 rel. 4 U (http://www.xmlspy.com) by bla (foo) -->
<xsl:stylesheet version="1.0" xmlns:m0="http://www.mpimp-golm.mpg.de/m1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="sample">
		<table border="1" align="center" width="80%">
			<tbody>
				<tr>
					<td colspan="3">
						<h4>Sample</h4>
					</td>
					<xsl:apply-templates select="*|@*"/>
				</tr>
			</tbody>
		</table>
	</xsl:template>

	<xsl:template match="@status|status">
		<tr>
			<td>
				<b>
					<xsl:value-of select="local-name(.)"/>
				</b>
			</td>
			<td>
<!-- old one
					<table border="1">
						<tbody>
							<tr>
							<xsl:choose>
								<xsl:when test=".=0"> <td bgcolor="155">0</td> </xsl:when>
								<xsl:otherwise> <td bgcolor="255">0</td> </xsl:otherwise>
							</xsl:choose>

							<xsl:choose>
								<xsl:when test=".=1"> <td bgcolor="155">0</td> </xsl:when>
								<xsl:otherwise> <td bgcolor="255">0</td> </xsl:otherwise>
							</xsl:choose>

							<xsl:choose>
								<xsl:when test=".=2"> <td bgcolor="155">0</td> </xsl:when>
								<xsl:otherwise> <td bgcolor="255">0</td> </xsl:otherwise>
							</xsl:choose>

							<xsl:choose>
								<xsl:when test=".=3"> <td bgcolor="155">0</td> </xsl:when>
								<xsl:otherwise> <td bgcolor="255">0</td> </xsl:otherwise>
							</xsl:choose>
							</tr>
						</tbody>
					</table> -->
					
				<table border="1" width="60%">
					<tbody>
						<tr>
							<td>not measured</td>
							<td align="center">
								<xsl:choose>
									<xsl:when test=".=0">
										<img src="pics/check.gif"/>
									</xsl:when>
									<xsl:otherwise>
										<img src="pics/o.gif"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<form action="info.jsp">
								<td align="center">
									<input type="image" src="pics/info.gif"/>
								</td>
							</form>
						</tr>
						<tr>
							<td>ok</td>
							<td align="center">
								<xsl:choose>
									<xsl:when test=".=3">
										<img src="pics/check.gif"/>
									</xsl:when>
									<xsl:otherwise>
										<img src="pics/o.gif"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<form action="info.jsp">
								<td align="center">
									<input type="image" src="pics/info.gif"/>
								</td>
							</form>
						</tr>
						<tr>
							<td>failed</td>
							<td align="center">
								<xsl:choose>
									<xsl:when test=".=4">
										<img src="pics/check.gif"/>
									</xsl:when>
									<xsl:otherwise>
										<img src="pics/o.gif"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<form action="info.jsp">
								<td align="center">
									<input type="image" src="pics/info.gif"/>
								</td>
							</form>
						</tr>
						<tr>
							<td>problematic</td>
							<td align="center">
								<xsl:choose>
									<xsl:when test=".=5">
										<img src="pics/check.gif"/>
									</xsl:when>
									<xsl:otherwise>
										<img src="pics/o.gif"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<form action="info.jsp">
								<td align="center">
									<input type="image" src="pics/info.gif"/>
								</td>
							</form>
						</tr>
					</tbody>
				</table>

					
					
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="*">
		<tr>
			<td>
				<b>
					<xsl:value-of select="local-name(.)"/>
				</b>
			</td>
			<td>
				<!-- TODO - add a chooser - if there are other elements do not print there values ... -->
				<xsl:value-of select="."/>
				<table border="1">
					<tbody>
						<xsl:apply-templates select="*|@*"/>
					</tbody>
				</table>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="*|@*">
		<tr>
			<td width="100">
				<b>
					<xsl:value-of select="local-name(.)"/>
				</b>
			</td>
			<td>
				<xsl:value-of select="."/>
				<!-- TODO - add a chooser - if there are other elements do not print there values ... -->
				<table border="1">
					<tbody>
						<xsl:apply-templates select="*|@*"/>
					</tbody>
				</table>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="Operator">
		<tr>
			<td>
				<b>Operator</b> information</td>
			<td>
				<a href="query?queryType=6&amp;docType=2&amp;parameterName=trayname_generic/Operator&amp;parameterValue={.}">
					<xsl:value-of select="."/>
				</a>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="Species">
		<tr>
			<td>
				<b>Species</b> information</td>
			<td>
				<a href="query?queryType=6&amp;docType=2&amp;parameterName=trayname_generic/Species&amp;parameterValue={.}">
					<xsl:value-of select="."/>
				</a>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="a">
	</xsl:template>
	<xsl:template match="class">
		<tr>
			<td>
				<b>Class</b> information</td>
			<td>
				<a href="query?queryType=6&amp;docType=2&amp;parameterName=trayname_generic/class&amp;parameterValue={.}">
					<xsl:value-of select="."/>
				</a>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="trayname_generic">
		<tr>
			<td>Definition added by <b>User</b>
			</td>
			<td>
				<table>
					<tbody>
				</tbody>
				</table>
			</td>
		</tr>
		<xsl:apply-templates select="*"/>
	</xsl:template>
	<!-- template to decribe the meta data -->
	<!--
	<xsl:template match="meta">
		<tr>
			<td>meta-informations</td>
			<td colspan="">
				<center>
				<table>
					<tbody>
		<xsl:apply-templates select="*"/>
					</tbody>
				</table>
				</center>
			</td>
		</tr>
	</xsl:template>-->
</xsl:stylesheet>
