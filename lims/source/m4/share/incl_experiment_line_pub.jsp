<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Hashtable"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<%

/**
requires HQL connection
promtID

long promtID = 0;
UserDO user = null;
Session hqlSession = null;


*/

try {
	// Logger.debug(this, promtID +  " checking access");
	PromtUserAccessRight accessRight = new SXQuery().findPromtUserAccessRightForUserID(user.getUOID(), promtID);
	
	HashSet otherUserHashset = new HashSet();
	
	Iterator useriterator  = new SXQuery().findPromtUserAccessRightForPromtID(accessRight.getExperimentID()).iterator();
	
	while(useriterator.hasNext()){
		PromtUserAccessRight accessRight2 = (PromtUserAccessRight)useriterator.next();
		// Logger.debug(this, promtID +  " checking users 1");
		if (accessRight2.getUserID() != user.getUOID()){
		    try {
		        UserDO _u = (UserDO) hqlSession.createCriteria(UserDO.class).add(Expression.eq("id", new Long(accessRight2.getUserID()))).list().get(0);
		    	// Logger.debug(this, promtID +  " checking user 2");
				//UserDO _u = (UserDO)UserLocal.persistence_loadByID(UserLocal.class, hqlSession, accessRight2.getUserID());
				// filter system users out
				if ( (	!_u.isLabTechnician() 
				        || (_u.isLabTechnician() && _u.isSuperUser()) )
						&& _u.getUOID() != 12 // masteraccount
						&& _u.getUOID() != 29 // gert
						&& _u.getUOID() != 4 // gert
						) {
				    otherUserHashset.add(_u);
				}
		    } catch (Exception e){
		        Logger.warning(this, e.toString());
		    }
		}    
	}
	
	// Logger.debug(this, promtID +  " checking file exist");
	boolean resultFileExists = BBConnector.determineBBResultsArrived(promtID);
	 
	// Logger.debug(this, promtID +  " counting samples");
	int samplesTotal = new SXQuery().findSampleIDsByPromtID(promtID).size();
	
	int classes = new SXQuery().findClazzIDsByPromtID(promtID).size();
	

	boolean found = false;
	int max_numberOfMetadataFields = 0;
	Iterator sampleIDs = new SXQuery().findSampleIDsByPromtID(promtID).iterator();
	/*
	while(!found && sampleIDs.hasNext()){
		String metadataQuery = "select count(formobject.uoid) as s from formobject where formobject.PARENT = " + sampleIDs.next();
		max_numberOfMetadataFields = Integer.parseInt(hqlSession.createSQLQuery(metadataQuery).addScalar("s", Hibernate.STRING).list().get(0).toString());
		if (max_numberOfMetadataFields > 4) found = true;
	}
	*/
	
	
	
	
	%>
	
	
	<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
	<%@page import="org.setupx.repository.core.user.UserDO"%>
	<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
	<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
	<%@page import="org.setupx.repository.core.communication.binbase.BBConnector"%>
	<%@page import="java.util.HashSet"%>
	<%@page import="org.hibernate.criterion.Expression"%>
	<%@page import="org.setupx.repository.core.util.logging.Logger"%>
	<%@page import="org.setupx.repository.server.persistence.PersistenceActionFindException"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIProvider"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIQuery"%>
<%@page import="org.setupx.repository.core.communication.msi.AttributeValuePair"%>


	<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<tr>
		<td style="background-color:#6682B6">
			&nbsp;&nbsp;<br>&nbsp;
		</td>
	</tr> 
	<tr>
		<td colspan="21" style="padding: 0;">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr style="padding: 0;">
					<td width="3" style="padding: 0; background-color:#6682B6" align="left"><img src="pics/corner_white2.gif"></td>
					<td colspan="21" style="background-color: white" align="center">
						<h3><%=new SXQuery().findPromtTitleByPromtID(promtID) %></h3>
				<tr style="padding: 0;">
					<th width="3" style="padding: 0;" align="left"><img src="pics/corner-outside.gif"></td>
					<th width="100%">
						<nobr><%=new SXQuery().findSpeciesbyPromtID(promtID).get(0) %></nobr>
						<br>
						<font style="font-size: x-small;">  <nobr>[ <%@ include file="incl_experiment_structure.jsp"%> ]</nobr></font>
					</th>
			</table>
		</td>
	</tr>
	<tr>
		<th colspan="21" align="left">
			<table>
			    	<th>&nbsp;&nbsp;&nbsp;&nbsp;<a href="load?id=<%=promtID %>&action=<%=PromtUserAccessRight.READ%>"><img  border='0' src="pics/details_small.gif" title="view experiment"></a>
				<% 
					 // icons to remove the whole experiment
					 if (user.isMasterUser(user.getUOID()) && user.isSuperUser()){
				    %>
			    	<th><a href="remove_share_confirm_single.jsp?id=<%=promtID%>"><img  border='0' src="pics/remove-share.gif" title="remove experiment from the list of 'my experiments'"></a>
			    	<th><a href="remove_share_confirm.jsp?id=<%=promtID%>"><img  border='0' src="pics/remove-share-o.gif" title="remove experiment from everybodys list of my experiments"></a>
			    	<th><a href="remove_confirm.jsp?id=<%=promtID%>"><img  border='0' src="pics/remove-all-o.gif" src="pics/remove-share-o.gif" title="DELETE Experiment"></a>
				<% } %>
			    	<th><a href="msi_report_xml.jsp?id=<%=promtID%>"><img src="pics/xml3.gif" border="0"></a>
			    	<%if (found){ %><th><img src="pics/meta3.gif"  border="0"><%} %>
			    	<th><b><font style="color:#6682B6"> ID:<%=promtID%></font></b>
			</table>
		</th>
	</tr>
	<tr>
			<td style="background-color:#6682B6" width="20"></td>
			<td align="justify" colspan="2">
				<font size="-2" style="color:#6682B6">abstract</font><font size="-2"><br>
				<%
					String _abstract = new SXQuery().findPromtAbstractByPromtID(promtID);
					if (_abstract.length() > 400) {
					    _abstract = _abstract.substring(0, 580).concat(" ... [abstract has been shortend].");
					}
				%>
				<%=Util.replace(_abstract, '\n', "<br>") %>
				</font>
							 

				<br><br>
				<font size="-2" style="color:#6682B6">structure:</font><font size="-2"><br>
					<%long id = promtID; %>
					<%@ include file="incl_experiment_structure.jsp"%></font> 
				</font>			 
	

				<%
					 Iterator iterator235 = otherUserHashset.iterator();
					 if (iterator235.hasNext()){
				 %>
				<br><br>
				<font style="color:#6682B6" size="-2">collaborator:</font>
				 <%}
				 while (iterator235.hasNext()){
				     UserDO ___userDO = (UserDO)iterator235.next();
				     %>
							<font size="-2"> 
								<!--  in case the email can not be determined - case for user created before Apr2-2007 -->
								<br><a href="mailto:<%try{%><%=___userDO.getMailAddress().getEmailAddress() %><%}catch (Exception e){ } %>"><img  border='0' src="pics/mail.gif"></a>  <%=___userDO.getDisplayName() %>
							</font>
				     <%
				 }	
				 
				 %>







				
				<table>
				  <tr>
				    <th rowspan="2" valign="top">
				    	Downloads & Reports
				    	<font class="small">
				    		<br>
				    		The following download links allow you to receive the data of this experiment in different formats.
				    		<br>
				    		
				    	</font>
				    </th>
				    <th>Report 
				    	<font class="small"><br/>detailed report</font>
				    </th>
				    <th>Download 
				    	<font class="small"><br/>experimental design</font>
				    </th>
				    <th>Download 
				    	<font class="small"><br>instrument results</font>
				    </th>
				    <th>Download 
				    	<font class="small"><br>annoation result</font>
				    </th>
				    <th>Compounds 
				    	<font class="small"><br>annotated compounds</font>
				    </th>
				  </tr>
				  
				  <tr>
				    <td style="background-color: white" align="center"><a href="pubexperiment.jsp?id=<%=promtID %>"><img  border='0' src="pics/go.gif"></a></td>
				    <td style="background-color: white" align="center"><a href="report_xls.jsp?id=<%=promtID %>&data=true"><img  border='0' src="pics/go_download.gif"></a></td>
				    <td style="background-color: white" align="center"><a href="download_result.jsp?id=<%=promtID %>"><img  border='0' src="pics/go_download.gif"></a></td>
					<% 
						String filename = Config.DIRECTORY_RELATED + File.separator + id + File.separator + id + "_public.zip";
						File file = new File(filename);
						if (file.exists()){
						    %>
							    <td style="background-color: white" align="center"><a href="download?<%=WebConstants.PARAM_FILENAME%>=<%=file.getName() %>&<%=WebConstants.PARAM_PROMTID%>=<%=promtID%>"><img  border='0' src="pics/go_download.gif"></a></td>
						    <%
						} else {
						    %>
						    <td style="background-color: white" align="center"><img  border='0' src="pics/go_blank.gif"></td>
						    <%
						}
					%>
				    <td style="background-color: white" align="center"><a href="compoundPerExp.jsp?id=<%=promtID %>"><img  border='0' src="pics/go_chart.jpg"></a></td>
				  </tr>
				</table>
				
				<%  
				} catch (PersistenceActionFindException persistenceActionFindException){
				
				}
			%>
		<td style="background-color:#6682B6" width="20"></td>