<%@ include file="checklogin.jsp"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="org.setupx.repository.web.HttpSessionPool"%>


<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.communication.leco.Method"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.server.persistence.PersistenceActionFindException"%>
<%@page import="org.setupx.repository.server.persistence.SXQueryCacheObject"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashSet"%><%@page import="org.hibernate.criterion.Expression"%>
<%@page import="org.setupx.repository.server.logging.Message"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Sample"%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<body>




	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">	
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Administration</h2><br>Manage UserAccess, View status of samples, ...</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>


<%

java.util.Locale locale = java.util.Locale.US;

org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

//Integer num_samples = (Integer) hqlSession.createQuery("select count(DISTINCT s.sampleID) from " + org.setupx.repository.web.forms.inputfield.multi.Sample.class.getName() + " as s where s.label != '' and s.sampleID != ' ' and s.sampleID != '1'" ).uniqueResult();

Long num_samples = (Long) hqlSession.createSQLQuery("select count(DISTINCT s.sampleID) as result from scanned_samples as s where s.label != '' and s.sampleID != ' ' and s.sampleID != '1'").addScalar("result", org.hibernate.Hibernate.LONG).uniqueResult();


//        select count(*) from " + org.setupx.repository.web.forms.inputfield.multi.Sample.class.getName()).uniqueResult();
Integer num_experiment = (Integer) hqlSession.createQuery("select count(*) from " + org.setupx.repository.web.forms.inputfield.multi.Promt.class.getName()).uniqueResult();
Integer num_users = (Integer) hqlSession.createQuery("select count(*) from " + org.setupx.repository.core.user.UserDO.class.getName()).uniqueResult();

// (Integer) hqlSession.createQuery("select count(*) from " + org.setupx.repository.core.communication.importing.logfile.ScannedPair.class.getName()).uniqueResult();

// Long num_samples_run = (Long) hqlSession.createSQLQuery("select count(DISTINCT s.label) as result from scanned_samples as s where s.label != '' and s.sampleID != ' ' and s.sampleID != '1'").addScalar("result", org.hibernate.Hibernate.LONG).uniqueResult();

hqlSession.close();
%>



<%@ include file="navi_admin.jsp"%>

<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td width="50%" align="center" valign="top"> 
			<table width="100%" cellpadding="3">
				<tr>
					<th colspan="2">
						<table width="100%">
							<tr>
								<th width="2"><img  border='0' src="pics/limsandrobotics.jpg" height="196px"></th>
								<th width="100%" align="center"><font size="3">Instrument</font></th>
							</tr>
						</table>
					</th>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><img  border='0' src="pics/go_export_old.gif">
					<td>Acquistion<br><font class="small">create a new acsuisition task for the GC</font>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><img    border='0' src="pics/go_setup.gif">
					<td>Machine Setup
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><img    border='0' src="pics/go_setup.gif">
					<td>Methods
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><img    border='0' src="pics/go_blank.gif">
					<td>Print Labels
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><img    border='0' src="pics/go.gif">
					<td>File Handling
			</table>
		</td>


		<td width="50%" align="center" valign="top"> 
			<table width="100%" cellpadding="3">
				<tr>
					<th colspan="2">
						<table width="100%">
							<tr>
								<th width="2"><img  border='0' src="pics/connections.jpg" height="196px"></th>
								<th width="100%" align="center"><font size="3">User Administration</font></th>
							</tr>
						</table>
					</th>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><a href="user.jsp"><img  border='0' src="pics/go_share.gif"></a>
					<td>List of Users<br><font class="small">view all Users registered in the System</font>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><a href="user.jsp#edit"><img  border='0' src="pics/go_user_create.gif"></a>
					<td>New User<br><font class="small">create a new user account for this system.</font>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><a href="accesslog.jsp"><img  border='0' src="pics/go_search.gif"></a>
					<td>Access Log<br><font class="small">interactive map showing where the users are accessing from.</font>
			</table>
		</td>
	<tr>
		<td width="50%" align="center" valign="top"> 
			<table width="100%" cellpadding="3">
				<tr>
					<th colspan="2">
						<table width="100%">
							<tr>
								<th width="2"><img  border='0' src="pics/status.jpg" height="196px"></th>
								<th width="100%" align="center"><font size="3">Setup</font></th>
							</tr>
						</table>
					</th>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><a href="abbreviation.jsp"><img  border='0' src="pics/go.gif"></a>
					<td>Species abbreviation<br><font class="small">List of abbreviation assigned to a valid NCBI name/id.</font>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><a href="m1.jnlp"><img  border='0' src="pics/go_locked.gif"></a>
					<td>GC-TOF Client Application<br><font class="small">Software to be started on the connected GCs.</font>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><a href="services/m1?wsdl"><img  border='0' src="pics/go_locked.gif"></a>
					<td>SOAP Connection<br><font class="small">Connection to the WebService.</font>
			</table>
		</td>
		<td width="50%" align="center" valign="top"> 
			<table width="100%" cellpadding="3">
				<tr>
					<th colspan="2">
						<table width="100%">
							<tr>
								<th width="2"><img  border='0' src="pics/status.jpg" height="196px"></th>
								<th width="100%" align="center"><font size="3">Status Information</font></th>
							</tr>
						</table>
					</th>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><a href=""><img  border='0' src="pics/go_export.gif"></a>
					<td>System status<br><font class="small">List of experiments, finished samples, ...</font>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><a href="messages.jsp?type=4"><img  border='0' src="pics/go.gif"></a>
					<td>Internal Messages<br><font class="small">List of internal messages from the system</font>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><a href="obo_test.jsp?term=seed&ontology=po_anatomy.obo"><img  border='0' src="pics/go_locked.gif"></a>
					<td>Development<br><font class="small">Ontology Test</font>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><a href="fsa.jsp"><img  border='0' src="pics/go_locked.gif"></a>
					<td>BinBaseHelper<br><font class="small">29518 Export (FSA)</font>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><a href="215052.jsp"><img  border='0' src="pics/go_locked.gif"></a>
					<td>BinBaseHelper<br><font class="small">215052 Export</font>
				<tr>
					<td style="background-color:#FFFFFF" valign="top" width="2"><a href="mailto:<%=Config.OPERATOR_ADMIN.toString()%>"><img  border='0' src="pics/help.gif"></a>
					<td>Help<br><font class="small">Request help.</font>
			</table>
		</td>
	</tr>
</table>

















<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<table align='center' border='0' cellpadding='4' width="60%">
<tr>
<th></th>
<th colspan="4">Sample Status</th>
<th></th>
</tr>


<tr>
<td></td>
<td>Number of Experiments</td>
<td><font class="small">Unfiltered List of Experiments stored in the System.</font></td>
<td align="center"><b><%=num_experiment.intValue() %></b></td>
<td width="2"><a href="main_all.jsp"><img  border='0' src="pics/go_search.gif"></a></td>
<td></td>
</tr>

<tr>
<td></td>
<td>Number of Samples</td>
<td><font class="small">List of all Samples stored in the System.</font></td>
<td align="center"><b><%=num_samples.intValue() %></b></td>
<td width="2"><a href="samples.jsp"><img  border='0' src="pics/go_search.gif"></a></td>
<td></td>
</tr>

<tr>
<th></th>
<th colspan="4">LabEquipment Communication</th>
<th></th>
</tr>






<tr>
<td></td>
<td>samples measured<br><font class="small">	List of samples that have been meassuered</font></td>
<td colspan="3">
<!--   table containing the last samples that were finished  -->

<table> 
<% 
hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();
Iterator iter = hqlSession.createSQLQuery("select label, sampleID  from scanned_samples where sampleID != \" \" order by uoid desc limit 3 ")
.addScalar("label", org.hibernate.Hibernate.STRING)
.addScalar("sampleID", org.hibernate.Hibernate.STRING)
.list()
.iterator();

int i = 0;
while(iter.hasNext()){
Object[] array = (Object[])iter.next();

if (i%20 == 0) {%>
	<tr>
		<td></td>
		<th align="center">name</th>
		<th align="center" colspan="2">sample</th>
		<th align="center" colspan="2">experiment</th>
		<th align="center">time</th>
		<th align="center">sample label</th>
		<th align="center">machine details</th>
	</tr>
<%} 
i++;
int promtID = 0;
try {
    promtID = new SXQuery().findPromtIDbySample(Integer.parseInt("" + array[1])); 
}catch (Exception e){
    e.printStackTrace();
}%>
	<tr>
		<td></td>
		<td align="center"><%=array[0]%></td>
		<td align="center"><font class="small"><%=array[1]%></td>
		<td align="center" style="background-color:#FFFFFF"><a href="sample_detail.jsp?id=<%=array[1]%>"><img  border='0' src="pics/details.gif"></a></td>
		<td align="center"><font class="small"><%=promtID %></td>
		<td align="center" style="background-color:#FFFFFF"><a href="load?id=<%=promtID%>&action=20"><img  border='0' src="pics/details.gif"></a></td>
		<td align="center">
				
					<%
					
					
					List list = hqlSession.createCriteria(Message.class).add(Expression.eq("relatedObjectID", new Long("" + array[1]))).addOrder(Order.asc("date")).list();
					Message message = (Message) list.get(list.size()-1); 
					
					Calendar midnight = new GregorianCalendar();
					midnight.set(midnight.get(Calendar.YEAR),midnight.get(Calendar.MONTH), midnight.get(Calendar.DATE),0,1);

					if (message.getDate().after(midnight.getTime())){
						// todays samples
						%>	
							<font class="small">
								<%=DateFormat.getTimeInstance(DateFormat.SHORT, Locale.US).format(message.getDate())%>
							</font>
						<%} else { %>
							<font class="small">
							  <%=java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT, locale).format(message.getDate()) %>
							</font>
						<%} %>
		</td>
		<td style="background-color:#FFFFFF" align="center">
				<% try { %>
					<a href="sample_detail.jsp?id=<%=array[1]%>"><%=new SXQuery().findSampleLabelBySampleID(Integer.parseInt("" + array[1])) %></a>
				<% } catch (PersistenceActionFindException e){%>
					----
				<% }%>
		</td>
		<td align="center" style="background-color:#FFFFFF"><a href="run_detail.jsp?id=<%=array[1]%>"><img  border='0' src="pics/details.gif"></a></td>	
	</tr>
    <%
}
%>
	<tr>
		<td></td>
		<td align="center">...</th>
		<td align="center" colspan="2"></th>
		<td align="center" colspan="2"></th>
		<td align="center"></th>
		<td align="center">more</th>
		<td align="center" style="background-color:#FFFFFF" ><a href="scanned_samples.jsp"><img  border='0' src="pics/resize_x_plus.gif"></a></th>
	</tr>

</table>
</td>
<td></td>
</tr>





<%@ include file="incl_processbars.jsp"%>






<tr>
<td></td>
<td>Create new Acquistion<br></s><font class="small">create a new acsuisition task for the GC</font></td>
<td></td>
<td></td>
<td><a href="aq/query.jsp"><img  border='0' src="pics/go_export_old.gif"></a></td>
<td></td>
</tr>

<tr>
<td></td>
<td>Machines<br><font class="small">List of all machines in the Lab - Define new machines.</font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="machine.jsp"><img  border='0' src="pics/go_setup.gif"></a></td>
<td></td>
</tr>


<tr>
<td></td>
<td>Methods <br><font class="small">Assign methods used for the acquistion </font></td>
<td colspan="2">
<%

hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

int unassigned = hqlSession.createSQLQuery("select {method.*} from machinemethod method where method.machine is null order by method.label").addEntity("method", Method.class).list().size();

if (unassigned > 0){
%>

<font class=error>There are <%=unassigned%> methods that are not assigned to one of the machines.</font>
<%}%>
</td>
<td width="2"><a href="machine_method_assign.jsp"><img  border='0' src="pics/go_setup.gif"></a></td>
<td></td>
</tr>


<tr>
<td></td>
<td>Results<br><font class="small">Results delivered from BinBase</font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="resultfiles.jsp"><img  border='0' src="pics/go_download.gif"></a></td>
<td></td>
</tr>



<tr>
<th></th>
<th colspan="4">User Administration</th>
<th></th>
</tr>

<tr>
<td></td>
<td>Users<br><font class="small">Total number of user useraccounts.</font></td>
<td>create, modify, assign experiments, ...</td>
<td align="center"><b><%=num_users.intValue() %></b></td>
<td width="2"><a href="user.jsp"><img  border='0' src="pics/go_share.gif"></a></td>
<td></td>
</tr>


<tr>
<td></td>
<td>create new User</td>
<td></td>
<td></td>
<td><a href="user.jsp"><img  border='0' src="pics/go_user_create.gif"></a></td>
<td></td>
</tr>



<!-- open sessions -->
<tr>
<th></th>
<th colspan="4">Logged in Users <br><font class="small">all open sessions</th>
<th></th>
</tr>
		<% 
			// list of sessions
			java.util.Hashtable sessions = org.setupx.repository.web.HttpSessionPool.getSessions();
			
			java.util.Enumeration enumeration = sessions.keys();
			while (enumeration.hasMoreElements()){
			    String key = (String)enumeration.nextElement();
				HttpSession httpSession = (javax.servlet.http.HttpSession)sessions.get(key);
				
				try {
				UserDO _user = (UserDO)httpSession.getAttribute(WebConstants.SESS_USER);
				
				
				// checking for sessions that are old
				long diff = new java.util.Date().getTime() - new java.util.Date(httpSession.getLastAccessedTime()).getTime();
				
				if (diff > 1800000) throw new IllegalStateException("outdated");
				%>
			<tr>
			<td></td>
			<td></td>
			<td colspan="3" >
				<table align="right" border="0">
					<tr>
						<td colspan="2"  align="center"><b><%=_user.getDisplayName() %> <a href="mailto:<%=_user.getMailAddress().getEmailAddress()%>"><img  border='0' src="pics/mail.gif"></a></td>
						<td align="center"><b></b><%=_user.getLastExperimentID()%></b> </td>
						
					<tr>
						<td style="background-color:LIGHTGREEN"><font class="small">Login:
						<td style="background-color:LIGHTGREEN"><%=new java.util.Date(httpSession.getCreationTime()).toLocaleString() %></td>
						<td rowspan="2" align="center"><a href="load?id=<%=_user.getLastExperimentID()%>&action=<%=PromtUserAccessRight.READ%>" title="Load the Experiment  <%=new SXQuery().findPromtTitleByPromtID(_user.getLastExperimentID()) %>"><img  border='0' src="pics/go.gif"></a> </td>					
					<tr>
		
						<td style="background-color:RED"><font class="small">Last activity:
						<td style="background-color:RED"><%=new java.util.Date(httpSession.getLastAccessedTime()).toLocaleString()%></td>
					</tr>
				</table>
			</td>
			<td></td>
			</tr>
				<%
			} catch(IllegalStateException exception){
			    HttpSessionPool.getSessions().remove(key);
			    exception.printStackTrace();
			} // catch
			} // while
			%>
<!-- end open sessions -->


<tr>
<th></th>
<th colspan="4">Status</th>
<th></th>
</tr>
<% 
hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();

/* today */
Long messages_today = (Long) hqlSession.createSQLQuery("select count(*) as result from message as m where DATEDIFF(DATE(m.date), CURDATE()) = 0 ").addScalar("result", org.hibernate.Hibernate.LONG).uniqueResult();

/* yesterday */

Long messages_yesterday = (Long) hqlSession.createSQLQuery("select count(*) as result from message as m where DATEDIFF(DATE(m.date), CURDATE()) =1 ").addScalar("result", org.hibernate.Hibernate.LONG).uniqueResult();

hqlSession.close();

%>

<tr>
<td></td>
<td>internal Messages<br><font class="small">See what is / was happening.</font></td>
<td colspan="2" align="center">
<font class="small">Today: <b><%=messages_today.intValue() %></b></font><br>
<font class="small">Yesterday: <b><%=messages_yesterday.intValue() %></b></font><br>
</td>
<td width="2"><a href="messages.jsp?type=4"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>



<tr>
<td></td>
<td>external Connections<br><font class="small">See status of services connected to the LIMS</font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="connections.jsp"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>

<tr>
<th></th>
<th colspan="4">File handling</th>
<th></th>
</tr>

<tr>
<td></td>
<td colspan="4">Use this option to mark samples as "successfully run". Use this option <B>only</B> if you are absolutly sure that this sample has run!<br><font class="small">If you are not 100% sure - contact the admin.</font></td>
<td></td>
</tr>

<tr>
<td></td>
<td>single run <br><font class="small">If you have a single file that you want to assign to a sample.</font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="assign_sample.jsp"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>

<tr>
<td></td>
<td>experiment<br><font class="small">If you have a whole experiment where you want to assign the comment as filename.<br>Adds by default _1 as the run to the comment field.</font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="assign_samples.jsp"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>






<tr>
<th></th>
<th colspan="4">Development!</th>
<th></th>
</tr>

<tr>
<td></td>
<td>Ontology Test<br><font class="small"><a href="http://obo.sourceforge.net/main.html">Open Biomedical Ontologies</a></font></td>
<td></td>
<td align="center"></td>
<td width="2"><a href="obo_test.jsp?term=seed&ontology=po_anatomy.obo"><img  border='0' src="pics/go.gif"></a></td>
<td></td>
</tr>
<tr><td colspan="21">...
</td></tr>


</table>

<%@ include file="footer.jsp"%>