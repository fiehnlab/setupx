<%@page import="java.util.HashSet"%>
<%@page
	import="org.setupx.repository.core.communication.importing.logfile.ScannedPair"%>
<%@page
	import="org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Enumeration"%>
<%@page
	import="org.setupx.repository.core.communication.binbase.trigger.ClassSet"%>
<%@page import="java.util.Vector"%>
<%@page
	import="org.setupx.repository.core.communication.leco.ColumnInformation"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page
	import="edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass"%>
<%@page
	import="edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample"%>

<%@ include file="checklogin.jsp"%>
<%

String colID = request.getParameter("column");

Session hqlSession = CoreObject.createSession();


Vector classesList = (Vector)request.getSession().getAttribute("selectedClasses");

// array with all classes
ExperimentClass[] experimentClasses = new ExperimentClass[classesList.size()];

Enumeration classesEnumeration = classesList.elements();

int classCount = -1;
while (classesEnumeration.hasMoreElements()){
	int classID = Integer.parseInt(classesEnumeration.nextElement() + "");
	Logger.debug(this, "classID: " + classID);
	classCount++;

	// get all samples and make 
	List l = new SXQuery(hqlSession).findSampleIDsByClazzID(classID);

	HashSet samplesHashset = new HashSet();
	
	Iterator sampleIDIterator = l.iterator();
	while (sampleIDIterator.hasNext()){
		try{
		    long sampleID = Long.parseLong("" + sampleIDIterator.next());
		    
		    String label = "" + sampleID;
		    Logger.debug(this, "sampleID: " + sampleID);
	
		    // latest file
		    List scannedPairs = new SXQuery(hqlSession).findScannedPair(sampleID);
		    
			if (scannedPairs.size() < 1) throw new Exception("unable to find ScannedPair for sample " + sampleID);
		    String name = "" + scannedPairs.get(0);
	
		    ExperimentSample experimentSample = new ExperimentSample(label , name);
		    samplesHashset.add(experimentSample);
		    Logger.log(this, "sample: " + experimentSample.getId() + " " + experimentSample.getName());
		}catch(Exception e){
		    e.printStackTrace();
		}
	}
	
	// convert to an array
	ExperimentSample[] samples = new ExperimentSample[samplesHashset.size()];
	Iterator samplesiter = samplesHashset.iterator();
	for (int i = 0; i < samples.length; i++){
	    samples[i] = (ExperimentSample)samplesiter.next();
	}
	        
	// create an ExperimentClass out of it
    ExperimentClass experimentClass = new ExperimentClass(colID, "" + classID, 0, 0, samples);
    Logger.log(this, "class: " + experimentClass.getId() + " " + experimentClass.getSamples().length);

	experimentClasses[classCount] = experimentClass; 
}

// column information
ColumnInformation colInfo = new ColumnInformation(colID, colID, colID);

ClassSet exportClassset = new ClassSet(colInfo);
exportClassset.setId(request.getParameter("name"));
exportClassset.setClasses(experimentClasses);
exportClassset.setIncrease(0);
try {
    exportClassset.setEmail(user.getEmailString());
} catch (Exception e){
    e.printStackTrace();    
}
        
Logger.log(this, "set: " + exportClassset.getId() + " " + exportClassset.getClasses().length);


// actual export
BinBaseServiceConnector binBaseSerivce = new BinBaseServiceConnector();
binBaseSerivce.launchExport((org.setupx.repository.core.communication.binbase.trigger.ClassSet)exportClassset, true);

%>



<%@page import="org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector"%>
<%@page import="org.setupx.repository.web.forms.PromtCreator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

</head>


<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width='90%'>
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Export</h2>
			Summary
			<br/>
			<font size="-2"></font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>


	<table align="center" width="60%" cellpadding="7">	<tr>
	<th  colspan="4">Export Experiment</th>
</tr>
<tr>
	<td colspan="1" align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/download.gif"></td>
	<td colspan="2" width = 90%><h3>The export of your study has been scheduled.</h3><p>
Data have been sent to BinBase for annotation. When the annotation is finished, you will be able to download your results from <%=org.setupx.repository.Config.SYSTEM_NAME%>. 
<br>Additionally, you may consult with the metabolomics laboratory team for quality controls.

		<p>
	</td>
	<td colspan="1"></td>
</tr>
<tr>
	<td colspan="1"></td>
	<td colspan="2"></td>
	<td colspan="1"></td>
</tr>
<tr>
	<td colspan="1" align="right" style="background-color:#FFFFFF"><a href="main.jsp"><img  border='0' src="pics/back.gif"></td>
	<td colspan="2"></td>
	<td colspan="1"></td>
</tr>
</table>



