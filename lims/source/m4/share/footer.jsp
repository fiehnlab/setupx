<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<p align="center">&nbsp;</p>

<hr/>
<p align="center">


<%@ page language="java" %>
<%@ page import="org.setupx.repository.web.forms.*"%>

<%@ page import=" org.setupx.repository.core.user.*"%>
<% UserDO _user = (UserDO)session.getAttribute("user"); 
if (_user == null) { %>
	<p align="center"><b>you are not logged in. <a href="login.jsp">To log.<a></b></p>
<% } else if (_user.isPublic()){  %>
	<p align="center"><b>Public access. To see your personalised <%=org.setupx.repository.Config.SYSTEM_NAME%> <a href="login.jsp">login</a>.</p>
<%} else {%>
	<p align="center"><b><%=_user.getDisplay()%></b>  <%=_user.phone%> <%=_user.getMailAddress().toString()%></p>
<%} %>


<p align="center">

<a href="http://fiehnlab.ucdavis.edu/"><img  border='0' src="pics/logo-fiehnlab.gif"></a>

<a href="http://metabolomics-core.ucdavis.edu/"><img  border='0' src="pics/logo-core.gif"></a>

</p>

<p align="center"><font class="small"><%=org.setupx.repository.Config.SYSTEM_NAME%>-Time: <strong><%=DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.US).format(new Date())%></strong></font> 

<p align="center">created by Martin Scholz - UC Davis - California <br><a href="mailto:mscholz@ucdavis.edu">mscholz@ucdavis.edu</a></p>

<p align="left">&nbsp; </p>
</body>