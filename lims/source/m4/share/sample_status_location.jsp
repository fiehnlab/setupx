<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.communication.status.Status"%>
<%@page import="org.hibernate.criterion.Expression"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.status.SampleArrivedStatus"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="org.setupx.repository.core.communication.status.Timestamped"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="java.util.Hashtable"%>
<%@page import="org.setupx.repository.core.communication.status.DynamicStatus"%>
<%@page import="org.setupx.repository.server.persistence.PersistenceActionFindException"%>
<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 org.setupx.repository.core.communication.binbase.BBConnector,
                 java.util.List,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.util.logging.Logger,
                 org.hibernate.criterion.Expression,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="java.util.HashSet"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>





<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width="60%">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2><%=org.setupx.repository.Config.SYSTEM_NAME%></h2>
			Where are the samples?<br/>
			<font size="-2">Below you can see the content of each of the existing freezers.</font>
		</th>

		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>

		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>

<table align="center" width="60%" cellpadding="5">

<%
	String location = request.getParameter("location");
				
	Calendar midnight = new GregorianCalendar();
	midnight.set(midnight.get(Calendar.YEAR),midnight.get(Calendar.MONTH), midnight.get(Calendar.DATE),0,1);
	
	
	
	// find all stored locations
	org.hibernate.Session hqlSession = org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration.createSessionFactory().openSession();
	Iterator locationIter = null;
	try {
	 	String selction = "select location from status where discriminator = \"" + org.setupx.repository.core.communication.status.SampleArrivedStatus.class.getName() + "\" group by location";

	 	locationIter = hqlSession.createSQLQuery(selction)
	    .addScalar("location", org.hibernate.Hibernate.STRING)
	    .list().iterator();
	
	} catch (Exception e){
	    e.printStackTrace();
	}
%>


<tr>
	<th colspan="21">Choose location that you want to look at.</th>
<tr>
	<td colspan="21">
	<%
	 // other locations
	 
	 while (locationIter.hasNext()){
	     String loc = "" + locationIter.next();
	     if (loc.compareTo(location) != 0){   
	     %>
	     <a href="sample_status_location.jsp?location=<%=loc%>">
		     <img src='pics/arrow-icon.gif' border="0" valign="middle" align="center"><%=loc%>
		 </a>
	     <br>
	     <%
	     }
	 }
	
	
	%>
	 </td>
</tr>

<tr>
	<th colspan="21">Content of <%=location %>.</th>


<tr>
	<th>sampleID
	<th>sample label
	<th>experiment
	<th>stored by
	<th>stored since	
		
		<% // find all samples that are located at a certain location 

			Iterator iterator = hqlSession
							.createCriteria(Status.class)
							.add(Expression.eq("location", location))
							.addOrder(Order.asc("date"))
							.list()
							.iterator();

			while(iterator.hasNext()){
			    SampleArrivedStatus status = (SampleArrivedStatus)iterator.next();
			%>

			<tr>
				<td align="center">
					<a href="sample_detail.jsp?id=<%=status.getSampleID() %>"><img src='pics/arrow-icon.gif' border="0" valign="middle" align="center"><%=status.getSampleID() %></a>
				<td align="center">
					<%=new SXQuery().findSampleLabelBySampleID(status.getSampleID())%>
				<td align="center">
					<% int promtID = new SXQuery().findPromtIDbySample(status.getSampleID()); %>				
					<b><%=promtID %></b><br>
					<%=new SXQuery().findPromtTitleByPromtID(promtID)%><br>
					<font class="small"><%=findCachedColl(promtID)%></font> 
				<td align="center">
					<%try { %>
						<%=findCachedUser(status.getUserID()).getDisplayName() %>
					<%} catch (Exception e){ %>
						unknown
					<%} %>
				<td align="center">

			<%	
			if (status.getDate().after(midnight.getTime())){
				// todays samples
				%>	
				<nobr>
					<font class="small">
						<%=DateFormat.getTimeInstance(DateFormat.SHORT, Locale.US).format(((Timestamped)status).getDate())%>
					</font>
				</nobr>
				<%} else { %>
				<nobr>
					<font class="small">
					  <%=java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT, Locale.US).format(((Timestamped)status).getDate()) %>
					</font>
				</nobr>
				<%}
			}
			%>
		
</table>
		
		

<%! 
		public static Hashtable cacheColl = new Hashtable();
		public static Hashtable cacheUser = new Hashtable();

		public static String findCachedColl(long promtID) throws PersistenceActionFindException{
			if (! cacheColl.containsKey("" + promtID)){
			    Logger.log(null, "no precached object for promtID:" + promtID);
			    cacheColl.put(  "" + promtID, 
						    new SXQuery().findPromtCollaborationHTML(promtID)
		                    );
			} 
		    return cacheColl.get("" + promtID).toString();
		}

		public static UserDO findCachedUser(long userID) throws PersistenceActionFindException{
			if (! cacheUser.containsKey("" + userID)){
			    Logger.log(null,"cachesize: " + cacheUser.size());
			    Logger.log(null,"cachesize: " + cacheUser.toString());

			    Logger.log(null, "no precached object for userID:" + userID);
			    cacheUser.put(  "" + userID, 
		            		(UserDO)UserDO.persistence_loadByID(
		                    UserDO.class, 
		                    DynamicStatus.createSession(), 
		                    userID)
		                    );
			} 
		    return (UserDO)cacheUser.get("" + userID);
		}


		public static String read(HttpServletRequest request, final String parametername, String alternativeValue) {
		 	String value = request.getParameter(parametername);
		  	if (value == null || value.compareTo("null") == 0) return alternativeValue;
	      	return value;
     	}
%>


<%@ include file="footer.jsp"%>
