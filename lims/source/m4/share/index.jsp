
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>


<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration"%>
<%@page import="org.setupx.repository.core.user.UserDO"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.core.communication.document.sop.StandardOperationProcedure"%>
<%@page import="org.setupx.repository.core.util.File"%>
<%@page import="org.setupx.repository.Settings"%>
<%@page import="java.io.IOException"%>
<html>

<script language="JavaScript" src="overlib.js"></script>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%></title>
</head>

<body>




<% // any session will be deleted
session.invalidate();



//Create a new session for the user.
session = request.getSession(true); 



boolean cachedQueries = true; //org.setupx.repository.Config.QUERY_CACHING_ACTIVE_JSP;

try {
	String cache = request.getParameter("cache");
    if (cache.compareTo("true") == 0){
        cachedQueries = true;
    } else if (cache.compareTo("false") == 0){
        cachedQueries = false;
    }

} catch (Exception e){
    
}




%>


<table width="100%" height="100%" cellpadding="10" cellspacing="80" border="1">
	<tr align="center" valign="middle" > 
		<td width="100%" valign="middle">

<table align='center' border='0' cellpadding='4' width="85%">
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<td align="center">
			<font style="font-variant: small-caps; font-size: 40pt;"><b><%=org.setupx.repository.Config.SYSTEM_NAME%></b></font><br>
			<font style="font-variant: small-caps; font-size: 20pt;">study design database for metabolomic projects</font>
		</td>
		<td width="2">
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
</table>
<br>
			<table align="center" width="100%" border="0" >
				<tr>
				
<%
	Session hqlSession = PersistenceConfiguration.createSessionFactory().openSession();

	String pubsamples = "select count(distinct setupXsampleID) as num from pubsample";				
	String numPubSamples = hqlSession.createSQLQuery(pubsamples).addScalar("num", org.hibernate.Hibernate.STRING).setCacheable(cachedQueries).list().get(0).toString();


 	String queryNumbers = " select count(distinct value) as species, sum(c) as experiments, sum(samples) as samples from (     " +
	"select label.value as value, count(promt.uoid) as c, sum(cache.numberofScannedSamples) as samples  " +
    "from formobject as promt  " +
    "inner join formobject as page   " +
    "on page.parent = promt.uoid  " +
    "inner join formobject as species  " +
    "on species.parent = page.uoid   " +
    "inner join formobject as ncbispecies  " +
    "on ncbispecies.parent = species.uoid  " + 
    "inner join formobject as label   " +
    "on label.parent = ncbispecies.uoid   " +
    "inner join cache as cache  " +
    "on cache.experimentID = promt.uoid " + 
    "inner join formobject as f  " +
    "on f.uoid = cache.experimentID " + 
    "where ncbispecies.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\" " +
    "and label.value != \"\"    " +
    "and cache.numberofScannedSamples > 0 " + 
    "group by label.value " + 
	") as a";
	
 	List l = hqlSession.createSQLQuery(queryNumbers).addScalar("species", Hibernate.STRING).addScalar("experiments", Hibernate.STRING).addScalar("samples", Hibernate.STRING).setCacheable(cachedQueries).list();
	Object[] obs = (Object[])l.get(0);
 	
 	//long num_experiment = Long.parseLong("" + hqlSession.createQuery("select count(*) from " + Promt.class.getName()).uniqueResult());
	String num_experiments = "" + obs[1];
	String num_species = "" + obs[0];
	String num_samples = "" + obs[2];
	
	
	long num_users = Long.parseLong("" + hqlSession.createQuery("select count(*) from " + UserDO.class.getName()).setCacheable(cachedQueries).uniqueResult());
%>
				
				
					<!--  left column -->
					<td width="25%" valign="top">
						<table width="100%" >
							<tr>
								<th ><h3>SOP Repository
							<tr>
								<td><strong><font class="small">&nbsp;</font></strong>
							<tr>
								<td style="background-color:#FFFFFF"><font class="small">
									<table>
										<tr>
											<td style="background-color:#FFFFFF">
												<a href="sop_index_tree.jsp"><img src="pics/sop.gif"/></a>
											<td style="background-color:#FFFFFF">
												<font class="small">
													SetupX contains a number of Standard Operation Procedures / Protocols. Some of them are publicly available.
												</font>
											</td>
										</tr>
										<% 
											try {
											    String qX = "select d.* from document as d join documentcategory as dc on dc.uoid = d.category where d.publiclyavailable = true and dc.public = true order by uoid desc limit 1";

											    StandardOperationProcedure standardOperationProcedure = (StandardOperationProcedure)hqlSession.createSQLQuery(qX).addEntity(StandardOperationProcedure.class).list().get(0);
											    
											    %>
										
												<tr>
													<td colspan="2" style="background-color:#FFFFFF">
													Most recent SOP:
												<tr>	
													<td colspan="2" style="background-color:#FFFFFF">
													<font class="small">
														&nbsp;&nbsp;&nbsp;<%=standardOperationProcedure.getLabel() %> [<%=standardOperationProcedure.getCategory().getLabel() %>]
														<br>
														&nbsp;&nbsp;&nbsp;<%=DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, Locale.US).format(standardOperationProcedure.getDateIssued()) %>
													</td>
												</tr>

											    <%
											} catch (Exception e){
										          Logger.warning(this, "can not determine most recent SOP");
											}
										%>
									</table>
								</td>
							<tr>
								<td><strong><font class="small">&nbsp;</font></strong>

							<tr>
								<th ><h3>data content
							<tr>
								<td><strong><font class="small">&nbsp;</font></strong>
							<tr>
								<td style="background-color:#FFFFFF"><a href="advancedquery_dev.jsp?term=&type=1"><img align="bottom" border='0' src="pics/display.gif"></a>&nbsp;Compounds 
							<tr>
								<td style="background-color:#FFFFFF"><font class="small">
									<font class="small">
									<%
									String queryComp = "select label as comp from pubattribute where value != 0 group by label order by count(uoid) desc limit 10";				
									Iterator iteratorComp = hqlSession.createSQLQuery(queryComp).addScalar("comp", org.hibernate.Hibernate.STRING).setCacheable(cachedQueries).list().iterator();
									while (iteratorComp.hasNext()){
										String label = iteratorComp.next().toString();
										%>	
										<nobr><a href="advancedquery_dev.jsp?term=<%=label %>&type=1"><%=label %></a></nobr>
										<% if (iteratorComp.hasNext()) {%> 
											&nbsp;
											<%}
										} %>
										<nobr>&nbsp;...</nobr>
									</font>
								</td>
							<tr>


							<tr>
								<td><strong><font class="small">&nbsp;</font></strong>

							<tr>
								<td style="background-color:#FFFFFF"><a href="content_species.jsp#kingdom"><img align="bottom" border='0' src="pics/display.gif"></a>&nbsp;<%=num_species %> Species in <%=Config.SYSTEM_NAME %> 
							<tr>
								<td style="background-color:#FFFFFF"><font class="small">
									<font class="small">
									<%
									
									String query = "select label.value as ncbilabel, count(promt.uoid) as number "
								        + " from formobject as promt "
								        + " inner join formobject as page "
								        + " on page.parent = promt.uoid "
								        + " inner join formobject as species "
								        + " on species.parent = page.uoid  "
								        + " inner join formobject as ncbispecies "
								        + " on ncbispecies.parent = species.uoid  "
								        + " inner join formobject as label  "
								        + " on label.parent = ncbispecies.uoid  "
								        + " inner join cache as cache "
								        + " on cache.experimentID = promt.uoid "
								        + " inner join formobject as f "
								        + " on f.uoid = cache.experimentID "
								        + " where ncbispecies.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\"   "
								        + " and label.value != \"\"   "
								        + " and cache.numberofScannedSamples > 0 "
								        + " group by label.value " + " order by count(promt.uoid) desc limit 20";
									
									Iterator iterator2 = hqlSession.createSQLQuery(query).addScalar("ncbilabel", org.hibernate.Hibernate.STRING).addScalar("number", org.hibernate.Hibernate.INTEGER).setCacheable(cachedQueries).list().iterator();
									while (iterator2.hasNext()){
									    Object[] ob = (Object[])iterator2.next();
									    String label = ob[0].toString();
										String number = ob[1].toString();
									    if (label.indexOf("-") == -1){
									        NCBIEntry entry = NCBIConnector.determineNCBI_Information(NCBIConnector.determineNCBI_Id(label));
											%>	
											<a href="advancedquery_dev.jsp?term=<%=label %>&type=0" onmouseover="return overlib('<%=label%>: <br><%=number %> Experiments  <br><a href=http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=<%=entry.getTaxID() %>');" onmouseout="return nd();"><%=label%></a>
											
											<% if (iterator2.hasNext()) {%> 
												&nbsp;
											<%}
											} 
										}%>
										&nbsp;<nobr>...</nobr>
									</font>
								</td>
							<tr>
								<td><strong><font class="small">&nbsp;</font></strong>

							<tr>
								<td style="background-color:#FFFFFF"><a href="content_species.jsp#kingdom"><img align="bottom" border='0' src="pics/display.gif"></a>&nbsp;organs / parts
							<tr>
								<td style="background-color:#FFFFFF"><font class="small">
									<font class="small">
										<%@ include file="incl_organs.jsp"%>								
							
				<tr>
				<td colspan="21"></td>
				</tr>
							<tr>
								<th><h3>content</h3></th>
							</tr>
							<tr>
								<td><font class="small">number of samples added to SetupX per month</font>
							<tr>	
								<td>
									<%@ include file="incl_stat_samples.jsp"%>								
								</td>
							</tr>
				</tr>
				<tr>
				<td colspan="21"></td>
				</tr>
							<tr>
								<th><h3>news</h3></th>
							</tr>
							<tr>
								<td><font class="small">from the <a href="http://setupxlims.blogspot.com/">SetupX-DevBlog</a></font>
							<tr>	
								<td>
<script src="http://www.gmodules.com/ig/ifr?url=http://customrss.googlepages.com/customrss.xml&amp;up_rssurl=http%3A%2F%2Fsetupxlims.blogspot.com%2Ffeeds%2Fposts%2Fdefault&amp;up_title=&amp;up_titleurl=&amp;up_num_entries=10&amp;up_linkaction=showdescription&amp;up_background=white&amp;up_border=white&amp;up_round=0&amp;up_fontfamily=Arial&amp;up_fontsize=10pt&amp;up_openfontsize=10pt&amp;up_itempadding=2px&amp;up_bullet=bull&amp;up_custicon=Overrides%20favicon.ico&amp;up_boxicon=0&amp;up_opacity=20&amp;up_itemlinkcolor=black&amp;up_itemlinkweight=Normal&amp;up_itemlinkdecoration=None&amp;up_vlinkcolor=black&amp;up_vlinkweight=Normal&amp;up_vlinkdecoration=None&amp;up_showdate=1&amp;up_datecolor=black&amp;up_tcolor=white&amp;up_thighlight=%231B5289&amp;up_desclinkcolor=black&amp;up_color=black&amp;up_dback=white&amp;up_dborder=black&amp;up_desclinkweight=Normal&amp;up_desclinkdecoration=None&amp;synd=open&amp;w=320&amp;h=220&amp;title=&amp;border=%23ffffff%7C3px%2C1px+solid+%23999999&amp;output=js"></script>								</td>
							</tr>
				</tr>
						</table>




					<!--  center column -->
					<td width="50%" valign="top">
						<table width="100%" bgcolor="white">
							<tr>
								<th colspan="4"><h3>Query
							<tr>
								<td colspan="4" align="center">
									<table width="100%" style="background-color: white" cellspacing="3">
												<% 
													boolean inclImage = false;
												%>
												<%@ include file="incl_q_input.jsp"%>
									</table>
							<tr>
								<th colspan="4"><h3>Welcome to <%=org.setupx.repository.Config.SYSTEM_NAME%>
							<tr>
								<td colspan="4">


							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><img  border='0' src="pics/go_search.gif">
								<td style="background-color:#FFFFFF"><b>What is in <%=org.setupx.repository.Config.SYSTEM_NAME%>?</b>
								<td style="background-color:#FFFFFF">
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">
								
								 <%=org.setupx.repository.Config.SYSTEM_NAME%> is a system that enables investigators to detail and set up a biological experiment. Right now it contains the following samples:
								 <br>
								 
								 <p align="center">
								 <% boolean show = false; %>
								 <a href="content_species.jsp#kingdom">
								 	<%@ include file="incl_topSpecies.jsp"%>
								 </a>	
								 </p>
								 
								<td style="background-color:#FFFFFF">
								<td style="background-color:#FFFFFF">


							<tr>
								<td colspan="4">


							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><img  border='0' src="pics/go_report.gif">
								<td style="background-color:#FFFFFF"><b>Who may submit experiments?</b>
								<td style="background-color:#FFFFFF">	
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">
								
								<b><i>You.</i></b> The front end is intuitive so it should be easy for you to enter your own study design in <%=org.setupx.repository.Config.SYSTEM_NAME%>. For collaborations with the Fiehn laboratory, consent on research strategies and cost-sharing is necessary between the principal investigators. 
								<td style="background-color:#FFFFFF">


							<tr>
								<td colspan="4">


							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><img  border='0' src="pics/go_locked.gif">
								<td style="background-color:#FFFFFF"><b>"I want to keep my data for myself!"</b>
								<td style="background-color:#FFFFFF">
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">Your data are safe and are not automatically available to the public. However, we request that data become publicly available when results are published in peer-reviewed scientific journals. If you consent, data may be shared with other databases, e.g. by request of regulatory or funding agencies.
								<td style="background-color:#FFFFFF">



							<tr>
								<td colspan="4">


							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><img  border='0' src="pics/go_msi.gif">
								<td style="background-color:#FFFFFF"><b>Share your Experiment!</b>
								<td style="background-color:#FFFFFF">
							<tr>
								<td style="background-color:#FFFFFF" colspan="2"><%=org.setupx.repository.Config.SYSTEM_NAME%> is part of ongoing efforts to standardize reporting requirements for metabolomic experiments, developed by the <a href="msi.jsp">MSI</a>. Check out the <a href="main_public.jsp">existing public experiments <img align="bottom" border='0' src="pics/arrow-icon.gif"></a>.
								<td style="background-color:#FFFFFF">

							<tr>
								<td colspan="4">

							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><img  border='0' src="pics/go_export.gif	">
								<td style="background-color:#FFFFFF"><b>Automated Anotation</b>
								<td style="background-color:#FFFFFF">
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">Annotation of GC-TOF mass spectral data is performed by the linked BinBase database. Experiments are scheduled for annotation from the <%=org.setupx.repository.Config.SYSTEM_NAME%> interface, and BinBase exports finished results again to <%=org.setupx.repository.Config.SYSTEM_NAME%> for user downloads.
								<td style="background-color:#FFFFFF">

							<tr>
								<td colspan="4">


							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><img  border='0' src="pics/go_new.gif">
								<td style="background-color:#FFFFFF"><b>Get started!</b>
								<td style="background-color:#FFFFFF">
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">To get started we will provide you with an account. Please enter your user information in the box on the right hand side, and we will get in contact with you as soon as possible.  

								<td style="background-color:#FFFFFF">
							<tr>
								<td colspan="4">


							<tr>
								<td rowspan="2" style="background-color:#FFFFFF" valign="top" align="right"><a href="mailto:<%=org.setupx.repository.Config.SYSTEM_EMAIL %>"><img  border='0' src="pics/help.gif"></a>
								<td style="background-color:#FFFFFF"><b>Support</b>
								<td style="background-color:#FFFFFF">
								<td style="background-color:#FFFFFF">
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">If you have any questions, we are happy to help you. We continuously improve <%=org.setupx.repository.Config.SYSTEM_NAME%> to match study designs and to include query functions. We rely on your feedback! 
								<td style="background-color:#FFFFFF">



						</table>



					<!--  right column -->
					<td width="25%" valign="top">
						<table width="100%">
							<tr>
								<th colspan="2"><h3>Public Data</h3>
							</tr>
							<tr>
								<td style="background-color:#FFFFFF" colspan="2">
									<font style="font-size: x-small;">A subset of <%=numPubSamples %> out of the  <b><%=num_samples %> samples</b> related to <b><%=num_experiments %> experiments</b> is publicly available - including the whole experimental design and the annotated GC-TOF result data. 
									Use the link below to <b>query</b> the whole sytem for the samples that you are interessted in. 
							<tr>
								<td align="right" colspan="2" style="background-color:#FFFFFF" >
									<a href="main_rss.jsp"><img  border='0' src="pics/rss3.gif"></a> 
									<a href="main_public.jsp"><img  border='0' src="pics/go_search.gif"></a> 
								</td>

							</tr>
						</table>
						<br>
						<table width="100%">
						<form action="login" method="POST">
							<%
							// checking if the system is in maintainance mode
							String content = "";
							try {
						        content = File.getFileContent(new java.io.File(Settings.getValue("system.maintain.messagefile", Config.DIRECTORY_DATA + java.io.File.separator + "maintain.txt" )));
						      } catch (IOException e) {
						          Logger.warning(this, "can not determine debug status - message file not found - see settings: system.maintain.messagefile");
						      }

						      String disabled = "";
						      if (content.length() > 0){
						          disabled = "disabled=\"disabled\"";
						      }

						      
							%>

							<tr>
								<th colspan="2"><h3>Existing Users</h3>
							</tr>
							<tr><td style="background-color:#FFFFFF" colspan="2"><font class="small">Please enter your userID & password. In case you no not remember, please contact <a href="mailto:<%=org.setupx.repository.Config.SYSTEM_EMAIL %>">us</a>.
							<%
								if(content.length() > 0){
							%>

							<tr>
								<td style="background-color:white;" colspan="2" align="center">
								<img width="150" height="150" src="pics/maintenanceMan.jpg">
							<tr><td colspan="2">
								<font style="font-style: italic; font: bold;"> The system is currently being maintained. Please check back soon...</font>
								<br>
								<font style="font-size: xx-small;"><%=content %></font>
			
							</td>
							</tr>
							<%
								}
							%>


							<tr><td>login:    </td> 
								<td align="right"><input type="text" <%=disabled %> size="20" value="" name="<%=WebConstants.PARAM_ID%>"/></td>
							</tr>
							<tr><td>password: </td> 
								<td align="right"><input type="password" <%=disabled %>	 size="20" value="" name="<%=WebConstants.PARAM_PASSWORD%>" /></td>
							</tr>
							<tr><td align="right"></td> 
							<td align="right"><input type="image" <%=disabled %> src="pics/go.gif" value="login"/> </td>
						</tr>
						</form>
						</table>
						<br>
						<table width="100%">
						<form action="requestaccount.jsp">
							<tr ><th colspan="2"><h3>New Users*</h3></tr>
							<tr><td style="background-color:#FFFFFF" colspan="2"><font class="small">To create a new user account please enter a few informations about yourself and you will receive your personal account by email.
							<tr><td colspan="1">name
								<td align="right"><input type="text" name="name">
							<tr><td colspan="1">organisation
								<td align="right"><input type="text" name="orga">
							<tr><td colspan="1">eMail
								<td align="right"><input type="text" name="email">
							<tr><td align="right"></td> 
							<td align="right"><input type="image" src="pics/go.gif" value="login"/> </td>
							<tr><td align="left" colspan="2">
								<font class="small">* Accounts are only needed for <b>active collaborators</b> who submit samples for data acquisition to either the UCD metabolomics core laboratory or the UCD Fiehn metabolomics research laboratory.</font>
						</form>
						</table>
						<br>
						<table width="100%">
							<tr>
								<th colspan="2"><h3>Software</h3>
							</tr>
	
							<tr><td colspan="2">Download SetupX</td> 
							<tr><td style="background-color:#FFFFFF" colspan="2"><font class="small">Download SetupX for use in your personal lab enviroment. It is an open source project so feel free to download it and try it out.</font>.
							<tr><td style="background-color:white;" colspan="2" align="center"><a href="readme.jsp"><img border="0" src="pics/go_download.gif"></a></td> 
							

							<tr><td colspan="2">Run SetupX orbiter</td> 
							<tr><td style="background-color:#FFFFFF" colspan="2"><font class="small">Click here to run the SetupX orbiter. A tiny application printing tags with a dymo printer to be placed right on the vials.</font>.
							<tr><td style="background-color:white;" colspan="2" align="center"><a href="m1.jnlp"><img border="0" src="pics/go_download.gif"></a></td> 

							</tr>
						</table>
						<br>
								
						
				</tr>
				<tr>
				<td colspan="21"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>