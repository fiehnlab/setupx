<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.document.sop.StandardOperationProcedure"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
	
<%@ include file="checklogin.jsp"%>


  <%@page import="gov.nih.nlm.nls.lvg.Lib.Category"%>
<%@page import="org.setupx.repository.core.communication.document.DocumentCategory"%>
<style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>


<%


//sop_create.jsp?replaceid=sop.getUOID()

    request.getParameter("replaceid");

	// sop_new.jsp?replaceid=1
	Session s = StandardOperationProcedure.createSession();
	long replaceID = Long.parseLong(request.getParameter("replaceid"));
	        
	// load old sop
	StandardOperationProcedure sopOld = StandardOperationProcedure.load(s, replaceID);
	
	if (sopOld == null){
	    // there is no SOP with that id - which means either a wrong id or it simply is supposed to be a blank new SOP
	    sopOld = new StandardOperationProcedure();
	}

	// trying to increase the last int
	String newVersion = "0.0";
	try {
	    String oldVersion = sopOld.getVersion();
		int posDot = oldVersion.lastIndexOf('.') + 1;
		int subVersion = Integer.parseInt(oldVersion.substring(posDot));
		Logger.log(this, "substring: " + oldVersion + " " + posDot + " " + subVersion);
	    newVersion = oldVersion.substring(0, posDot) + (subVersion + 1);
		Logger.log(this, "newVersion: " + newVersion);
		
	} catch (Exception e){
	    
	}
	// prefill it with values from older one
	%>
	
	
	<jsp:include page="sop_header.jsp"></jsp:include>

 	<h2>
 		Create a new Standard Operation Procedure
	</h2>
	Please define the SOP as accurate as possible. 
	<br>
	First select one of the lab members to be responsible for this document. Only this particular person is in charge of it and can replace / activate or retire this SOP.
	<br>
	Give the SOP a self speaking name.
	<br>
	If you are replacing an older SOP with this document, the default will be that this document is scheduled to replace the older SOP in the future. To activate the 
	new SOP you have to activate the SOP in the detail view which you will see after finishing this form.
	<br>
	Define the SOP here in the big content box or make a note that the information is stored in the File, which you can attatch to this SOP.
	
	
<form action="sop_new.jsp" method="post" enctype="multipart/form-data">
<table align="center">
		<tr>
			<td>
				category
			<td>
			<%
				String disabled="";
				if (sopOld.getCategory() != null){
				    	%>
						<input type="hidden" name="catid" value="<%=sopOld.getCategory().getUOID()%>">
				    	<%
						disabled = "disabled";
				}
			 %>
			
				
				<select name="catid" size="5" <%=disabled %>>
			<% // dropdown showing all lab categories
				List categories = CoreObject.createSession().createSQLQuery("select * from documentcategory " ).addEntity(DocumentCategory.class).list();
				Iterator categoriesIterator = categories.iterator();

				while(categoriesIterator.hasNext()){
				    DocumentCategory category = (DocumentCategory) categoriesIterator.next();
				    String selected = "";
				    if (sopOld.getCategory() != null && category.getUOID() == sopOld.getCategory().getUOID()){
				        selected = "selected";
				    }
				    %>				  
				    	<option value ="<%=category.getUOID() %>" <%=selected %>><%=category.getLabel() %></option>
					    <%
				}
			%>
				</select>

		<tr>
			<td>
				responsible 			
			<td>
				<select name="responsible" size="5">
			<% // dropdown showing all lab people
				List users = CoreObject.createSession().createSQLQuery("select * from user where admin = true or labtechician = true order by displayname" ).addEntity(UserDO.class).list();
				Iterator userIterator = users.iterator();
				while(userIterator.hasNext()){
				    UserDO tmpUser = (UserDO) userIterator.next();
				    String selected = "";
				    if (sopOld.getResponsible() != null && tmpUser.getUOID() == sopOld.getResponsible().getUOID()){
				        selected = "selected";
				    }
				    %>				  
				    	<option value ="<%=tmpUser.getUOID() %>" <%=selected %>><%=tmpUser.getDisplayName() %></option>
					    <%
				}
			%>
				</select>
		<tr>
			<td>
				label
			<td><input type="text" name="label" value="<%=sopOld.getLabel()%>">
		<tr>
			<td>
				area where valid
			<td><input type="text" name="areaValid" value="<%=sopOld.getAreaValid()%>">
		<tr>
			<td>
				comment 			
			<td><input  type="text" name="comment" value="<%=sopOld.getComment()%>">
		<% if(sopOld.getUOID() != 0) {%>
		<tr>
			<td>
				modifications to old version 			
			<td><input  type="text" name="modificationsToLastVersion" value="">
		<% } %>
		<tr>
			<td>
				organisation 			
			<td><input  type="text" name="organisation" value="<%=sopOld.getOrganisation()%>">
		<tr>
			<td>
				version 			
			<td><input  type="text" name="version" value="<%=newVersion%>"> 
			
			<td><font style="font-size: x-small;">The replaced Document was Version: <%=sopOld.getVersion() %></font>
		<tr>
			<td>
				defined by 			
			<td><input  type="text" name="definedBy" value="<%=user.getDisplayName()%>" readonly="readonly">
		<tr>
			<td>
				defined  			
			<td><input  type="text" value="<%=DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US).format(new Date())	%>" readonly="readonly">
		<tr>
			<td>
				replaces Document   
			<td>
				<input  type="text" name="definedBy" value="<%=sopOld.getLabel()%> <%=sopOld.getVersion() %>" readonly="readonly" disabled="disabled">
				<input type="hidden" name="replaceid" value="<%=sopOld.getUOID()%>">
		<tr>
			<td colspan="21">
				content 			
				<br>
				<font style="font-size: x-small;">Please define detailed in this document or upload a PDF containing the definition of the SOP.</font>
		<tr>
			<td colspan="21">
				<textarea name="content" cols=70" rows="25"><%=sopOld.getContent()%></textarea>
		<tr>
			<td colspan="21">
				<input type="file" name="uploadfile"/>	
		<tr>
			<td colspan="21">
				<input type="submit" value="Submit SOP ">
</table>
</form>