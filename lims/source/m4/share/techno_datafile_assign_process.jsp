<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Datafile"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"%>
<%
        long sampleID = Long.parseLong(request.getParameter("id"));
		Logger.debug(this, "id " + sampleID);

		long technoFileTypeID = Long.parseLong(request.getParameter("tid"));
		Logger.debug(this, "tid " + technoFileTypeID);

		String path = request.getParameter("path");
		Logger.debug(this, "path " + path);
		
		Session s = TechnoCoreObject.createSession();
		
		TechnologyFileType technoFileType = (TechnologyFileType)TechnoCoreObject.persistence_loadByID(TechnologyFileType.class, s, technoFileTypeID);

		Datafile datafile = new Datafile(sampleID, new File(path));
		
		technoFileType.addDatafile(datafile);
		
		technoFileType.getTechnology().updatingMyself(s,true);
%>


<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<meta http-equiv="refresh" content="0; URL=techno_technologyfiletype.jsp?id=<%=technoFileTypeID%>">

