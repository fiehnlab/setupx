  <%@page import="java.util.Hashtable"%>
<%@page import="java.util.Enumeration"%>

<%! 
		public int counter = 0;

		public String createRowNr(){
	     	return "data.addRows(" + counter + ");";
	 	}

      	public String createCells(final String parentCellName, final Hashtable hashtable) {
      	    
      	    Logger.log(null, "-----------------------------------------");
      	    Logger.log(null, "parent " + parentCellName + " nr. childs:" + hashtable.size());
    		String result = "";
    		Enumeration keys = hashtable.keys();
    		while (keys.hasMoreElements()){
    		    
    		    // actual node
    		    String key = keys.nextElement().toString();
    		    
    			String childCell  = "data.setCell(" + counter + ", " + 0 + ", '" + key + "');";
    			String parentCell = "data.setCell(" + counter + ", " + 1 + ", '" + parentCellName + "');";
    			
    			Logger.log(null, childCell);
    			Logger.log(null, parentCell);
    			
    			counter++;
    			
    			result = result.concat(childCell + '\n').concat(parentCell + '\n');
    			
    			if (hashtable.get(key) != null && hashtable.get(key) instanceof Hashtable){
    			    result = result.concat(createCells(key, (Hashtable) hashtable.get(key)));
    			}
    		}
    	
    		
		return result;
    
		}

%>



<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<head>
   	<%
  		counter = 0;
  	%>
  
  
  
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["orgchart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Manager');

		<%
		
		String javascCode = createCells("", roots);
		javascCode = createRowNr() + '\n' + javascCode;
		
		// parent
		%><%=javascCode%><%
		%>

        var table = new google.visualization.OrgChart(document.getElementById('chart_div'));
        table.draw(data, {allowHtml:true,	 size: 'small'});
      }
    </script>
  </head>
  <div id="chart_div"></div>
    
