
<%@ include file="checklogin.jsp"%>

<%@page import="java.util.Date"%>
<%@page import="org.setupx.repository.core.communication.document.DocumentObject"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.document.sop.StandardOperationProcedure"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.web.forms.inputfield.multi.Promt"%>
<%@page import="org.setupx.repository.web.WebConstants"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.communication.document.DocumentCategory"%>


<meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>

	<table>
	
	<tr>
		<th colspan="21">
			<h3>Select the Standard Operation Procedures</h3>
			
			<a href="form.jsp"><img src="pics/go.gif"/><br>Click here</a> when you are done selecting all the SOPs that were involved in this experiment.
		</th>
	</tr>
	<tr>
		<th colspan="21">
			<hr/>
		</th>
	</tr>

<%
	Session s = StandardOperationProcedure.createSession();
	String readonly = "";
	
	DateFormat dFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US);	
	// dFormat.format(date));
	
	// the actual experiment received from the session
	Promt promt = (Promt)session.getAttribute(WebConstants.SESS_FORM_PROMT);

	List l = StandardOperationProcedure.createSession().createSQLQuery("select * from document as d order by d.category desc, d.dateValidStart desc").addEntity(StandardOperationProcedure.class).list();
	
	Iterator sopIterator = l.iterator();
	DocumentCategory documentCategory = null;
		
	while (sopIterator.hasNext()){
	    StandardOperationProcedure sop = (StandardOperationProcedure)sopIterator.next();
	    long sopID = sop.getUOID();

	    if (sop.isActive()){
	        
	        if(documentCategory == null || documentCategory.getUOID() != sop.getCategory().getUOID()){
	            documentCategory = sop.getCategory();
	            %>
	<tr>
		<th colspan="3"><h3>
		
			<a href="sop_index.jsp"><img src="pics/sop.gif" border="0"/> Standard Operation Procedures</a> -  
			<% try{ %>
				[category: <%=documentCategory.getLabel() %>]
			<% } catch (Exception e){} %>
		</h3></th>
	</tr>

	            <%
	        }
	        
	    %>

	
	<tr>
		<td width="30" valign="middle">
			<table>
	   			<tr>
	   				<td colspan="3">
	   					<%
	  						// checking if the they are assigned to the promt already 
	  						
	  						boolean assigned = (promt.findSOPMultifield().getSopRefField(sop.getUOID()) != null);
	   					
	   						if (!assigned){
	   					%>
	   					<a href="sop_form_add.jsp?refID=<%=sop.getUOID()%>&action=1">ADD</a>
	   					<%
	   						} else {
	   					%>
	   					<a href="sop_form_add.jsp?refID=<%=sop.getUOID()%>&action=2">REMOVE</a>
	   					<%
	   						}
	   					%>
	   				</td>
	   			</tr>
	   			<tr>
	   				<td colspan="3"><hr></td>
	   			</tr>
			</table>	
		</td>
		<td>
			<table border="0">
			  <tr>
			    <td colspan="2">
			    	<font style="font-size: xx-small;">issued by: Responsible: <nobr><a href="mailto:<%=sop.getResponsible().getEmailString()%>"><%=sop.getResponsible().getDisplayName()%></a></nobr>
			    		&nbsp;&nbsp;Issued: <%=dFormat.format(sop.getDateIssued()) %> 
			    		&nbsp;&nbsp;valid beginning: <%=dFormat.format(sop.getDateValidStart()) %>
			    		&nbsp;&nbsp;valid until: <%=dFormat.format(sop.getDateValidEnd()) %>
					</font>
			    </td>
			   </tr>

			   <tr>
			    <td colspan="2">
			    	<h3><%=sop.getLabel()  %> (Version <%=sop.getVersion() %>)</h3>
		    	</td>
			   </tr>

			   <tr>
		    	<td>
		    		<b>Description</b><br>
		    		<%=sop.getContent().replaceAll("\n", "<br>")%>
		    	</td>
		    	<td rowspan="31">
		    		<table>
		    			<%
						if (sop.getFilePath().length() > 0){
							File relatedFile = new File(sop.getFilePath());
							%>
								<tr>
									<th colspan="3">Related Files</th>
								<tr>
									<td rowspan="2">
										<a href="sop_download.jsp?sopID=<%=sopID %>"><img src="pics/file.jpg" border="0"/></a>
									<td>
										<nobr><%=relatedFile.getName()%></nobr>
									<td width="80%">
										&nbsp;
								<tr>
									<td>
										<nobr><font style="font-size: xx-small;"><%=dFormat.format(new Date(relatedFile.lastModified())) %></font></nobr>
									</td>
								</tr>
							<%
						} else {
							%>
								<tr>
									<th colspan="3">Related Files</th>
								<tr>
									<td colspan="3">
										There is no file to this SOP - all the information is show.
									</td>
								</tr>
							<%
						}
					%>
					</table>
							
							
		    		
		    	</td>
			   </tr>
			   <tr>
		    	<td>
		    		<b>Related File</b><br>
		    		sdfasdfasdfasdf asdf asdf asdf sdf a
		    	</td>
			  </tr>
			</table>
		</td>
		<td width="30"></td>
	</tr>
	    <%	        
	    } // is active
	    

	} // while sopIter 
%>
</table>







