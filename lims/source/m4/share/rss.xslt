<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <title><xsl:value-of select="/rss/channel/title" /></title>
        <link rel="stylesheet" type="text/css" href="http://www.4guysfromrolla.com/css/text.css" />
        <style type="text/css">
          body { font-family: Verdana, Helvetica, sans-serif; font-size: 0.8em; }
          #headerInfo { text-align: center; width:90%; }
          #feedItems { width: 80%; border: 1px solid #333; padding-left: 15px; padding-right: 15px; padding-top: 5px; padding-bottom: 5px; text-align: left; }
          #headerText { text-align: center; padding: 8px; border: 1px dashed #bbb; background-color: #f7f7f7; }
          .rssDescription { padding-left: 25px; }
        </style>

        
        <script type="text/javascript" src="http://aspnet.4guysfromrolla.com/rss/disableOutputEscaping.js" />
      </head>
      
      <body onload="go_decoding();">
        <div id="cometestme" style="display:none;">
          <xsl:text disable-output-escaping="yes">&amp;amp;</xsl:text>
        </div>

        <div align="center">
          <div id="headerInfo">

            <h1><xsl:value-of select="/rss/channel/title" /></h1>
            <p align="center" id="headerText">This page is the syndication feed for <b><xsl:value-of select="/rss/channel/title" /></b>.  You can subscribe
            to this feed using an aggregator program and be kept abreast of the latest 4GuysFromRolla.com articles.  For more information
            on syndicating content check out <a href="http://www.4guysfromrolla.com/webtech/031303-1.shtml">Syndicating Your Website's Content
            with RSS</a>.  For information on displaying another site's syndicated content on your own site, check out 
            <a href="http://aspnet.4guysfromrolla.com/articles/102903-1.aspx">A Custom ASP.NET Server Control for Displaying RSS Feeds</a>.</p>
          </div>
        </div>
        <br />
        
        <div align="center">

          <div id="feedItems">
            <xsl:for-each select="/rss/channel/item">
              <div class="rssItem">
                <h2 class="rssTitle"><a href="{link}"><xsl:value-of select="title" /></a></h2>
                <div name="decodeable" class="rssDescription">
                  <xsl:value-of select="description" disable-output-escaping="yes" />
                </div>
              </div>            
            </xsl:for-each>

          </div>
        </div>
      </body>
    </html>
  </xsl:template>
  
</xsl:stylesheet>