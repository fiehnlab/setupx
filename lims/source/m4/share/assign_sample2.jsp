<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.communication.importing.metadata.*,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="org.setupx.repository.core.communication.importing.logfile.LogFileScanner2"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%@ include file="incl_setaccessright.jsp"%>

	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="./admin.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Util: Assign "virtual" Run (Beta) </h2>
			Tool marks sample that was entered in <%=org.setupx.repository.Config.SYSTEM_NAME%> as finished. <br/>
			<font size="-2"></font>
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>
	
	
	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<%} %>
	
	<table align="center" width="60%" >	
	<tr>
		<td colspan="2" align="center" style="background-color:#FFFFFF"><a href="admin.jsp"><img  border='0' src="pics/back.gif"></a></td>	
		<th colspan="20">ExperimentID</th>
	</tr>
		<tr>
			<td colspan="2"></td>
	      		<%
	      		String label = request.getParameter("filename");
	      		String sampleID = request.getParameter("sampleID");
				new LogFileScanner2().createScannedPair(label, Integer.parseInt(sampleID));
	      		//new org.setupx.repository.core.communication.importing.logfile.ScannedPair(label, sampleID).update(true);%>
			<td colspan="2">
				Combination <b><%=label %></b> and <b><%= sampleID %> </b> has been created.
			</td>
			<td>
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<td colspan="2">
			</td>
			<td>
			</td>
			<td>
			</td>
		</tr>
</table>



<%@ include file="footer.jsp"%>
