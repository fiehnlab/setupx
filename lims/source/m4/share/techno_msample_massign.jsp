
  <%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.Vector"%>
<head>
    <title>SetupX</title>
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>

<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Technology"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Location"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.core.util.FilenamePatternFilter"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Datafile"%>
<%@page import="java.util.Date"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"%>

	<jsp:include page="techno_header.jsp"></jsp:include>


	<h2>
		<a href="techno_techno.jsp">all platforms</a> - search for datafiles
	</h2>
	<p>
	Please enter all sample IDs that you are looking for and all matching filenames. <br>
	The filenames do not nessecerily have to be a full filename - the part that is unique about that specidic file is requiered.
	<br>
	If you ran one sample on different platforms and a unique pattern is used with all the samples the unique pattern is what should be entered.<br>
	Example: "GG24q5-run1.peg", "UV24q5.rqz", "GC24q5-21.txt"  - then the part to enter would be "24q5".
	
<form action="techno_msample_massign_search.jsp"  method="post">
	<table>
		<tr>
			<td>
				Sample IDs
			<td>
				part of the filename
		<tr>
			<td>
				<textarea rows="10" cols="20" name="sampleIDs"><jsp:scriptlet>
					// getting all ids for a class or an experiment
					// try promt
					try {
						List l = null; 
						try {
							l = new SXQuery().findSampleIDsByPromtID(Long.parseLong(request.getParameter("id")));
						} catch (Exception e){
						    l = null;
						}
						
						if (l == null || l.size() == 0){
							try {
								l = new SXQuery().findSampleIDsByClazzID(Integer.parseInt(request.getParameter("id")));
							} catch (Exception e){
							    l = new Vector();
							}
						}
						Iterator i = l.iterator();
						while(i.hasNext()){
						    out.println(i.next().toString());
						}
					} catch (Exception e){
					    
					}
				</jsp:scriptlet></textarea>
			<td>
				<textarea rows="10" cols="20" name="samplePattern"><jsp:scriptlet>
					// getting all ids for a class or an experiment
					// try promt
					try {
						List l = null; 
						try {
							l = new SXQuery().findSampleIDsByPromtID(Long.parseLong(request.getParameter("id")));
						} catch (Exception e){
						    l = null;
						}
						
						if (l == null || l.size() == 0){
							try {
								l = new SXQuery().findSampleIDsByClazzID(Integer.parseInt(request.getParameter("id")));
							} catch (Exception e){
							    l = new Vector();
							}
						}
						Iterator i = l.iterator();
						while(i.hasNext()){
						    out.println(i.next().toString());
						}
					} catch (Exception e){
					    
					}
				</jsp:scriptlet></textarea>
				
		<tr>
			<td>
				<input type="submit">
	</table>
</action>