<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net" xmlns:c="urn:jsptld:http://java.sun.com/jstl/core">
<jsp:directive.page import="org.setupx.repository.server.persistence.SXQuery"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"/>
<jsp:directive.page import="org.hibernate.Session"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Datafile"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Technology"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.hibernate.Hibernate"/>
<jsp:directive.page import="java.util.Vector"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
    <title>SetupX</title>
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>



<jsp:scriptlet>

	long promtID = Long.parseLong(request.getParameter("id"));

	// get all datafiles for this sample

	Session s = TechnoCoreObject.createSession(); 
	List list = s.createSQLQuery("select datafile.* from datafile join samples on samples.sampleID = datafile.sampleid and samples.experiment = " + promtID).addEntity(Datafile.class).list();
	
	//
	request.setAttribute("test", list); 

	// style
	pageContext.setAttribute("tableclass", "its");

</jsp:scriptlet>

	<h2>
		<a href="techno_techno.jsp">all platforms</a> - 
		experiment <jsp:expression>promtID</jsp:expression>
	</h2>
	
	<display:table id="row" name="test">
		<display:column title="Class" sortable="true" headerClass="sortable">
	    	<jsp:expression>
	    		new SXQuery().findClazzIDBySampleID((int)((Datafile)row).getSampleID())
	    	</jsp:expression>
	    </display:column>
		<display:column title="Sample" sortable="true" headerClass="sortable"  href="techno_sampleFiles.jsp?" paramId="id" paramProperty="sampleID" property="sampleID">
	    </display:column>

		<display:column title="Technology Platform" sortable="true" headerClass="sortable"  href="techno_technology.jsp?" paramId="id" paramProperty="technologyFileType.technology.UOID">
		      <c:out value="${row.technologyFileType.technology.label}"/>
	    </display:column>
		<display:column title="FileType" sortable="true" headerClass="sortable"  href="techno_technologyfiletype.jsp?" paramId="id" paramProperty="technologyFileType.UOID">
		      <c:out value="${row.technologyFileType.label}"/>
	    </display:column>
		<display:column title="File name" property="sourceFile.name" sortable="true"></display:column>
		<display:column title="location" property="sourceFile.parent" sortable="true" ></display:column>
	    <display:column title="created on" property="creationDate" sortable="true" headerClass="sortable"/>
	    <display:column title="exists" sortable="true">
	    	<jsp:scriptlet>
	    		String img = "";
	    		if (((Datafile)row).getSourceFile().exists()){
	    			img = "pics/check.gif";
	    		} else {
	    			img = "pics/circle.gif";
	    		}
	    	</jsp:scriptlet>
			<![CDATA[<img src=]]>"<jsp:expression>img</jsp:expression>"<![CDATA[/>]]>
	    </display:column>
		<display:column title="download" sortable="false" headerClass="sortable" href="techno_sampleFiles.jsp?" paramId="id" paramProperty="sampleID">download</display:column>
	</display:table>
</jsp:root>


