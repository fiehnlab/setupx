<%@page import="java.util.Iterator"%>
<%@page import="java.util.Vector"%>

	
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<table style="font-size: xx-small">
	

<%
Vector oldRows = (Vector) session.getAttribute("data");
Vector newRows = new Vector();

Iterator rowIterator = oldRows.iterator();
int rowCounter = -1;
while (rowIterator.hasNext()) {
    rowCounter++;
    Vector row = (Vector) rowIterator.next();
    
	// every of the rows will be placed at a certain point in each row
    for (int i = 0; i < row.size(); i++) {
        Object v = row.get(i);
        
        
        Logger.debug(this, "got: " + rowCounter + " " + i);
        
        // value is at pos (i,rowcounter) and is now placed at (rowcounter, i)
        Vector newRow = null;
        try {
            newRow = ((Vector)newRows.get(i));
            Logger.debug(this, "got newRows: " + i);
        } catch (ArrayIndexOutOfBoundsException e){
            newRow = new Vector();
            newRows.add(i,newRow);
            Logger.debug(this, "created V at newRows: " + i);
        }

        Logger.debug(this, "writing in newRow at: " + rowCounter + " last element: " + newRow.size());
        try {
            newRow.add(rowCounter,v);
		} catch (Exception e){
	        Logger.debug(this, "expending Vector due to illegal pos");
            newRow.add(v);
        }

	}
}

session.setAttribute("data", newRows);

%>

<meta http-equiv="refresh" content="0; URL=pubxlsimport3.jsp">