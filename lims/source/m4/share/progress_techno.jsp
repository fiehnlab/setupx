<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.Technology"%>
<%@page import="org.setupx.repository.server.persistence.SXQuery"%>

<%
try{
	Session hqlSession = CoreObject.createSession();
	SXQuery sxq = new SXQuery(hqlSession);
		
	long promtID =  Long.parseLong("" + session.getAttribute("imgpromtid"));
	
	int totalSamples = sxq.determineNumberOfSamples(promtID);
	if (totalSamples < 1){
	    throw new Exception("there are NO samples in this experiment");
	}
	
    int totalFinished = sxq.determineNumberOfSamplesFinished(promtID);
    
    Collection technologies =  TechnologyProvider.getTechnologies();
    Iterator technoIterator = technologies.iterator();
    Vector names = new Vector();
	Vector perc = new Vector();
    
    int count = 0;
    while (technoIterator.hasNext()){
        Technology technology = (Technology)technoIterator.next();

        int numberFinished  =  sxq.determineNumberOfSamplesFinished(promtID, technology);

        if (numberFinished > 0){
          	perc.add("" + (100 * numberFinished / totalSamples));
            names.add(technology.getLabel());
      	}

        count++;
    }

    if (totalFinished == 0){
	     // no samples have been completed at all
      	perc.add("" + (0));
        names.add("all");
	}
    
	int totalFinishedPerc = 100 * totalFinished / totalSamples;

	
	
	
	
	
	
	
	//// IMG

	String img = "http://chart.apis.google.com/chart?";
	// type
	img = img.concat("cht=bhs");
	
	// size
	img = img.concat("&chs=380x").concat(""+ (30 * (names.size() + 1)));
	
	// data
	img = img.concat("&chd=t:");

	for (int i = 0; i < names.size(); i++){
	    img = img.concat("" + perc.get(i));
	    if (i != names.size() -1) img = img.concat(",");
	}
	
    img = img.concat("|");
	for (int i = 0; i < names.size(); i++){
	    img = img.concat("" + (totalFinishedPerc - Integer.parseInt(perc.get(i).toString()) ));
	    if (i != names.size() -1) img = img.concat(",");
	}
	
    img = img.concat("|");
	for (int i = 0; i < names.size(); i++){
	    img = img.concat("" + (100 - totalFinishedPerc));
	    if (i != names.size() -1) img = img.concat(",");
	}
	
	// colors
	img = img.concat("&chco=00FF00,00FF0033,FF000066");
	
	// width
	img = img.concat("&chbh=20");
	
	// labels
	img = img.concat("&chxt=x,y,r&chxl=0:||25%|50%|75%|complete|1:");
	for (int i = names.size() -1 ; i > -1; i--){
	    img = img.concat("|");
	    img = img.concat(names.get(i).toString());
	}
	
    img = img.concat("|2:");
	for (int i = names.size() -1 ; i > -1; i--){
	    img = img.concat("|");
	    img = img.concat(perc.get(i).toString() + " %");
	}

	// title
	String title = "chtt=Experiment " + promtID + " [" + totalFinishedPerc + "% (" + totalFinished + " samples)]"; 
    img = img.concat("&");
    img = img.concat(title);
		
    hqlSession.close();

%>
<%@page import="java.util.Vector"%>
<a href="progress_detail.jsp?id=<%=promtID %>">
	<img border="0" src="<%=img %>"/>
</a>
<%
} catch (Exception e){
    e.printStackTrace();
}    
%>