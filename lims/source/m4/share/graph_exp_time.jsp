


<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Set"%>
<html>
  <head>
    <script type='text/javascript' src='http://www.google.com/jsapi'></script>
    <script type='text/javascript'>
      google.load('visualization', '1', {'packages':['annotatedtimeline']});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        
        
        data.addColumn('date', 'Date');
        <%
        	Session s = TechnoCoreObject.createSession();
        	List technologyfiletypesList = CoreObject.persistence_loadAll(TechnologyFileType.class, s);
        	
        	Iterator iterator1 = technologyfiletypesList.iterator();
			int cc = 0;
        	while(iterator1.hasNext()){
        	    cc++;
        	    TechnologyFileType ft = (TechnologyFileType)iterator1.next();
        	    %>
		        data.addColumn('number', '<%=ft.getLabel().concat("                              "	).substring(0,8)%>');
		        data.addColumn('string', 'title<%=cc%>');
		        data.addColumn('string', 'text<%=cc%>');
		        <%
        	}
        //DATE_FORMAT(d.dateCreated, '%X, %c, %e, %H, 00, 00' )
        String query1 = " select d.technologyfiletype as TYPE, d.dateCreated  as TIME, count(distinct d.sampleID) AS NUM "
        + " from datafile as d join samples as s on s.experiment = "  +request.getParameter("id") + " and s.sampleID = d.sampleID where d.technologyfiletype < 8  and d.technologyfiletype > 0  "
        + "  group by d.technologyfiletype, SUBSTRING(d.dateCreated, 1, 10) order by SUBSTRING(d.dateCreated, 1, 10) ";
						
		Logger.log(this, query1);

		List l = CoreObject	.createSession().createSQLQuery(query1)
							.addScalar("TYPE",Hibernate.STRING)
							.addScalar("TIME",Hibernate.STRING)
							.addScalar("NUM",Hibernate.INTEGER)
							.list();
		
		Logger.log(this, l.toString());

		int MAX = technologyfiletypesList.size();
		// number of "real" datapoints and a start and end for each of the values
		int numberOfDatapoints = l.size() + ((MAX-1)*2);
		%>

        data.addRows(<%=(numberOfDatapoints)%>);
        <%

		
        int count = 0;
        
        
		Iterator iterator = l.iterator();		

        Hashtable values = new Hashtable();
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        while (iterator.hasNext()){
            Object[] objects = (Object[])iterator.next();

            int value = 0;
            try {
                value = Integer.parseInt(values.get(objects[0]).toString());
            } catch (Exception e){
                // not initalized.
            }
            
            value = value + Integer.parseInt(objects[2].toString());

            values.put(objects[0], value + "");
            Date tmpDate = dfm.parse(objects[1].toString());

            // init values at 0
            if (count == 0){
                for (int tmpC = 1; tmpC < MAX; tmpC++){
                    value = 0;

                    values.put(objects[0], value + "");
                    %>

                    data.setValue(<%=count%>, 0, new Date(<%=(tmpDate.getYear() + 1900) + ", " + (tmpDate.getMonth() ) + ", " + tmpDate.getDate()%>));
                    data.setValue(<%=count%>, <%=(tmpC -1) * 3 + 1%>, <%=value%>);
	              	<%
                    count++;
                }
            }
            
            %>
            data.setValue(<%=count%>, 0, new Date(<%=(tmpDate.getYear() + 1900) + ", " + (tmpDate.getMonth() ) + ", " + tmpDate.getDate()%>));
            data.setValue(<%=count%>, <%=(Integer.parseInt(objects[0].toString()) -1) * 3 + 1%>, <%=value%>);
            
            <%

            count++;
            // values at last datapoint
            if (!iterator.hasNext()){
                for (int tmpC = 1; tmpC < MAX; tmpC++){
                    value = 0;
                    try {
                        value = Integer.parseInt(values.get(tmpC+ "").toString());
                    } catch (Exception e){
                        // not initalized.
                    }
	                %>
	                data.setValue(<%=count%>, 0, new Date(<%=(tmpDate.getYear() + 1900) + ", " + (tmpDate.getMonth() ) + ", " + tmpDate.getDate()%>));
	                data.setValue(<%=count%>, <%=(tmpC -1) * 3 + 1%>, <%=value%>);
	                
	                <%
	                count++;
                }
            }
        }
		%>        
        


        var chart = new google.visualization.AnnotatedTimeLine(document.getElementById('chart_div'));
        <%
	    	GregorianCalendar g = new GregorianCalendar();
	        g.add(GregorianCalendar.MONTH, -3 );
	        Date start = g.getTime();
	    	Date end = new GregorianCalendar().getTime();
        %>
		chart.draw(data, {displayAnnotations: true});      
	}
    </script>
  </head>

  <body>
    <div id='chart_div' style='width: 1100px; height: 400px;'></div>
  </body>
</html>

