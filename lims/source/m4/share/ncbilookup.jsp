<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>

<%
	String speciesString = request.getParameter("species");
	int id = NCBIConnector.determineNCBI_Id(speciesString );
	NCBIEntry entry = NCBIConnector.determineNCBI_Information(id);
%>


<jsp:forward page="ncbi_classifyed.jsp?ncbiid=<%=entry.getTaxID() %>"/>
