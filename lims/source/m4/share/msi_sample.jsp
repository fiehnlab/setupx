<%@ include file="incl_header_msi.jsp"%>



<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.CoreObject"%>


<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.core.communication.msi.MSIAttribute"%>
<%@page import="org.setupx.repository.server.persistence.PersistenceActionFindException"%>
<%
            // query: msi attributes per sample 
            // that exist for the entire experiment
            String sampleID = request.getParameter("id");

            String query1 = "select "
                    + "mf_container.question as question1, "
                    + "mf_label.question  as question2, "
                    + "mf_label.value, "
                    + "mf_container.uoid, "
                    + "clazz.uoid, "
                    + "mf_label.msiattribute as msi1, "
                    + "mf_container.msiattribute as msi2 "
                    + "from      "
                    + "formobject as sample, "
                    + "formobject as multiField4Clazzes,         "
                    + "formobject as mf_label,          "
                    + "formobject as clazz,          "
                    + "formobject as mf_container,          "
                    + "multiField4ClazzesHibernate as x       "
                    + "where clazz.uoid = x.multiField4Clazzs "
                    + "and x.elt = multiField4Clazzes.uoid          "
                    + "and multiField4Clazzes.active = true     "
                    + "and (mf_label.msiattribute is not null or mf_container.msiattribute is not null) "
                    + "and mf_label.PARENT = multiField4Clazzes.uoid          "
                    + "and sample.uoid =  "
                    + sampleID
                    + "   "
                    + "and clazz.uoid = sample.parent "
                    + "and clazz.discriminator like \"%Clazz\" "
                    + "and mf_container.uoid = mf_label.PARENT         "
                    + "and (   select count(*) "
                    + "           from formobject "
                    + "           where parent = multiField4Clazzes.parent "
                    + "   ) = 1        and mf_label.value != \"\" "
                    + "order by clazz.uoid, mf_container.uoid, mf_label.question";

            String query2 = " select mf_container.question  as question1, mf_label.question  as question2, mf_label.value, mf_label.msiattribute as msi1,  mf_container.msiattribute as msi2 from "
                    + " formobject as sample, formobject as multiField4Clazzes, formobject as mf_label, formobject as clazz, formobject as mf_container, multiField4ClazzesHibernate as x "
                    + " where clazz.uoid = x.multiField4Clazzs and x.elt = multiField4Clazzes.uoid and multiField4Clazzes.active = true and (mf_label.msiattribute is not null or mf_container.msiattribute is not null) "
                    + " and mf_label.PARENT = multiField4Clazzes.uoid and sample.uoid = "
                    + sampleID
                    + " and clazz.uoid = sample.parent and clazz.discriminator like \"%Clazz\" and mf_container.uoid = mf_label.PARENT "
                    + " and (   select count(*) from formobject where parent = multiField4Clazzes.parent ) > 1  and mf_label.value != \"\" order by clazz.uoid, mf_container.uoid, mf_label.question ";

            String query3 = " select sample.question as question1, child.question  as question2, child.value, child.msiattribute as msi1, sample.msiattribute as msi2  "
                    + " from formobject as sample, formobject as child where sample.uoid =  "
                    + sampleID
                    + "  "
                    + " and sample.discriminator like \"%Sample\" and child.parent = sample.uoid";

            Session s = CoreObject.createSession();
%>
<table align="center" width="60%" cellpadding="7">
<tr>
	<th colspan="21">
		<a href="msi_sample_xml.jsp?id=<%=sampleID %>"><img src="pics/xml3.gif" border="0"></a>
		<h3>&nbsp;&nbsp;Attributes for sample <%=sampleID%></h3>
	</th> 
</tr>

<%
            String[] queries = new String[] { query1, query2, query3 };
            for (int i = 0; i < queries.length; i++) {
                List result = s.createSQLQuery(queries[i]).addScalar(
                        "question1", Hibernate.STRING).addScalar("question2",
                        Hibernate.STRING).addScalar("value", Hibernate.STRING)
                        .addScalar("msi1", Hibernate.LONG).addScalar("msi2",
                                Hibernate.LONG).list();
                Iterator resultIterator = result.iterator();
                MSIAttribute attribute = null;
                while (resultIterator.hasNext()) {
                    Object[] objects = (Object[]) resultIterator.next();
                    if (objects[2] != null
                            && objects[2].toString().compareTo("null") != 0
                            && objects[2].toString().compareTo("--other--") != 0
                            && objects[2].toString().compareTo("") != 0) {
                        long msiID = 0;
                        try {
                            msiID = Long.parseLong(objects[3].toString());
                        } catch (Exception e) {
                            try {
                                msiID = Long.parseLong(objects[4].toString());
                            } catch (Exception e2) {
                                // no msiid
                            }
                        }
                        if (msiID > 0) {
                            try {
                                attribute = (MSIAttribute) MSIAttribute
                                        .persistence_loadByID(
                                                MSIAttribute.class, s, msiID);
                            } catch (PersistenceActionFindException e) {

                            }
                        }
%>


        <tr>
        <td style="background-color: <%=Util.getColor(i) %>">
	        <%=objects[0]%>
        <td>
        	<%=objects[1]%>
        <td>
    	    <%=objects[2]%> <%
 if (attribute != null) {
 %><%=attribute.getExtension()%> <%
 }
 %>
    	<%
    	if (attribute != null) {
    	%>
    		<td><%=attribute.getMsiLabel()%> 
    	<%
     	} else {
     	%>
    		<td>
    	<%
    	}
    	%>
        <%
                        }
                        }
                    }
        %>
