<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net">
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Technology"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"/>
  <jsp:directive.page contentType="text/html; charset=UTF-8" />
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.hibernate.Hibernate"/>
<jsp:directive.page import="org.setupx.repository.core.CoreObject"/>
<jsp:directive.page import="org.setupx.repository.core.query.QueryMaster"/>
<jsp:directive.page import="org.setupx.repository.server.persistence.SXQuery"/>
<jsp:directive.page import="java.util.Vector"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>


  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="css/print.css" type="text/css" media="print" />

  </head>

	<jsp:scriptlet>
		long id = Long.parseLong(request.getParameter("id"));
		Technology technology = (Technology)TechnoCoreObject.persistence_loadByID(Technology.class, TechnoCoreObject.createSession(), id);
	</jsp:scriptlet>

	<jsp:include page="techno_header.jsp"></jsp:include>
 
 	<h2>
		<a href="techno_techno.jsp">all platforms</a> - 
		
		<![CDATA[<a href="techno_technology.jsp?id=]]>
		<jsp:expression>technology.getUOID()</jsp:expression>
		<![CDATA[">]]>
		<jsp:expression>technology.getLabel()</jsp:expression>
		<![CDATA[</a>]]>	
	</h2>
	<h6><jsp:expression>technology.getDescription()</jsp:expression></h6>
	The following different file types are assigned to this technology "<jsp:expression>technology.getLabel()</jsp:expression>". 


	<jsp:scriptlet>
	request.setAttribute("test", technology.getTechnologyFileTypes()); 

	// style
	pageContext.setAttribute("tableclass", "its");
	</jsp:scriptlet>
	
	<display:table name="test" id="row">
		<display:column property="label" href="techno_technologyfiletype.jsp?" paramId="id" paramProperty="UOID"></display:column>
	    <display:column property="describtion" sortable="true" headerClass="sortable"/>
	    <display:column title="number of files" sortable="true">
	    	<jsp:expression>((TechnologyFileType)row).getDatafiles().size()</jsp:expression>
	    </display:column>
	    <display:column property="pattern" sortable="true" headerClass="sortable"/>
	</display:table>

	<form action="techno_technologyfiletypes_create.jsp">

	<table>
		<tr>
			<th colspan="2">
				Create a new Technology Filetypes
			</th>
		</tr>
		<tr>
			<td colspan="2">
				In order to create a new Filetype please specify the <b>label</b> of the filetype, which can be a word or two and then a pattern that matches <b>all</b> Files
				of this type. A <b>Regular expression</b> like <code>(.*).(.*)</code> would simply say an undefinded number of characters as the filename and an undefined number 
				of characters as the extensions. 
				<br/>
				The expression <code>(.{6})([abz])([a-z]{2})sa(.{1,4})(.*)</code> fits for example the style all datafiles of the GC-TOF are labled. 6 characters defining the 
				day the sample ran and then the character a, b or z for the machine followed by two characters pointing to the operator. The end are <code>sa</code> for sample and 
				then up to for characters pointing to each of the samples for that day.
				<br/>
				<i>The more detailed this expression is written the better and easier the search result for the files will be.</i> 
				<br/>
				The <b>default location</b> is the position where the files are stored. (This information is the path definition from the SetupX server)
				<br/>
			</td>
		</tr>
		<tr>
			<td width="120">Label</td>
			<td><input type="text" name="label"/></td>
		</tr>
		<tr>
			<td>Patterns</td>
			<td><input type="text" name="pattern" size="20" value="(.{6})([abz])([a-z]{2})sa(.{1,4})(:)(.*)"/></td>
		</tr>
		<tr>
			<td>Descr.</td>
			<td>
				<textarea name="desc" rows="2" cols="20">...</textarea>
			</td>
		</tr>
		<tr>
			<td>Default location</td>
			<td><input type="text" name="location"/></td>
		</tr>
		<tr>
			<td>create</td>
			<td>
				<input type="submit" title="create"/>
				<![CDATA[<input type="hidden" name="technoid" value=]]>"<jsp:expression>technology.getUOID()</jsp:expression>"<![CDATA[/>]]>
			</td>
		</tr>
	</table>
	</form>
</jsp:root>


