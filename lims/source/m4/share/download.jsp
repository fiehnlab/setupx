<%@ include file="checknonlogin.jsp"%>

<%@page import="org.setupx.repository.server.persistence.SXQuery"%>

<%
// call: <a href="download.jsp?id=12345&type=txt">
%>



<%
int id = Integer.parseInt(request.getParameter("id"));
String type = request.getParameter("type");


Logger.debug(this, "ID:" + id);
Logger.debug(this, "type:" + type);

String[] endings = new String[]{"peg","smp", "txt", "cdf"};  
String[] paths = new String[]{
        Config.DIRECTORY_SAMPLEFILE_PEG,
        Config.DIRECTORY_SAMPLEFILE_SMP,
        Config.DIRECTORY_SAMPLEFILE_TXT,
        Config.DIRECTORY_SAMPLEFILE_CDF
        }; 

%>


<%=id %>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<%@page import="org.setupx.repository.web.forms.InsufficientUserRightException"%>
<%@page import="java.io.OutputStream"%>
<%@page import="org.setupx.repository.web.DownloadServlet"%>
<%@page import="org.setupx.repository.web.forms.ContentNotAvailableException"%>
<%@page import="java.io.File"%>
<%@page import="org.setupx.repository.core.util.Util"%>
<%@page import="org.setupx.repository.server.persistence.PersistenceActionFindException"%>
<%@page import="java.util.List"%>
<%@page import="java.util.zip.ZipOutputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.util.zip.ZipEntry"%>
<%@page import="java.io.IOException"%>
<%@page import="org.setupx.repository.core.util.logging.Logger"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Vector"%>
<%@page import="org.setupx.repository.Config"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.hibernate.Session"%>
<br>
<%
Session hqlSession = CoreObject.createSession();

// chacking access to this file
try {
    
    int promtID = new SXQuery(hqlSession).findPromtIDbySample(id);
    
    
    // checking access 
    try {
        int access = new SXQuery(hqlSession).findPromtUserAccessRightForUserID(user.getUOID(), promtID).getAccessCode();
        if (access < 20){
    		throw new PersistenceActionFindException("The user has insufficient right not access this information.");
        }
    } catch (Exception exception){
        request.getSession().setAttribute(org.setupx.repository.web.WebConstants.SESS_FORM_MESSAGE, exception.getMessage());
        %>
        <jsp:forward page="error.jsp"/>
        <% 
    }

    
	try {
	    String sampleNameString = new SXQuery().findAcquisitionNameBySampleID(id);
	    sampleNameString = sampleNameString.replace(':','_');
		
	    PromtUserAccessRight.checkAccess(user.getUOID(),promtID, PromtUserAccessRight.READ);
	    
	    OutputStream outputStream = response.getOutputStream();
	    response.setContentType(DownloadServlet.CONTENT_TYPE_TXT);
	
	    String path = "";
	    String ending = "";
	    for (int i = 0; i < endings.length; i++){
	        if (type.compareTo(endings[i]) == 0){
	            ending = endings[i];
	            path = paths[i];
	        }
	    }
	    File file = new File(path + File.separator + sampleNameString + "." + ending); 
	    response.setHeader("Content-Disposition","inline;filename=" + file.getName());
	    DownloadServlet.returnFile(file, outputStream);
	
	} catch(InsufficientUserRightException insufficientUserRightException){
	    %>access NOT granted.
	    	        <br>
	        <% String url = "main.jsp"; %>
	        <meta http-equiv="refresh" content="1; URL=<%=url%>">
	    
	    
	    <%
	}
} catch (PersistenceActionFindException actionFindException){

    
    long promtID = 0;
    // it is not a sample
	List sampleIDs = null;
	try {
		// check if it is a promt
	    sampleIDs = new SXQuery().findSampleIDsByPromtID(id);
		promtID = id;
		Logger.debug(this, "found sampleIDs for promt");
	} catch (Exception e){
		// check if it is a class
		try {
		    sampleIDs = new SXQuery().findSampleIDsByClazzID(id);
		    promtID = new SXQuery().findPromtIDbyClazz(id);
			Logger.debug(this, "found sampleIDs for clazz");
		} catch (Exception e2){
		    %>access NOT granted.
	        <br>
    <% String url = "main.jsp"; %>
    <meta http-equiv="refresh" content="1; URL=<%=url%>">


<%
		    return;
		}
	}
	
	
	
	
	// check access
	try {
		PromtUserAccessRight.checkAccess(user.getUOID(), promtID, PromtUserAccessRight.DOWNLOAD_RESULT);
	} catch (Exception e){
	    %>access NOT granted.
        <br>
<% String url = "main.jsp"; %>
<meta http-equiv="refresh" content="1; URL=<%=url%>">


<%
	    
	}
	
	if (sampleIDs != null && sampleIDs.size() > 0){
	    // take each sample and each related sampletype and put it in a zipfile.

	    // These are the files to include in the ZIP file
	    List filenames = new Vector();
	    
	    Iterator sampleIterator = sampleIDs.iterator();
	    while(sampleIterator.hasNext()){
	        int sampleID = Integer.parseInt(sampleIterator.next() + "" );
            String _name = "";
            try {
                _name = new SXQuery(hqlSession).findAcquisitionNameBySampleID(sampleID);
                _name = _name.replace(':','_');
	            Logger.debug(this, "found " + _name + " for " + sampleID);
            } catch (Exception e) {
                _name = "";
            }

            for(int a = 0; a< endings.length; a++){
                String extension = endings[a];
                String path = paths[a];

                String filename = path + File.separator + _name + '.' + extension;
    	        
    	        File file = new File(filename);
    	        if ((file.exists() && extension.compareTo(type) == 0) || (file.exists() && "all".compareTo(type) == 0)){
    		        filenames.add(filename);
    	            Logger.debug(this, "found " + filename);
    	        }
            }
	    }
	    
	    // Create a buffer for reading the files
	    byte[] buf = new byte[1024];
	    
	    try {
	        // Create the ZIP file
	        ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream());
		    response.setHeader("Content-Disposition","inline;filename=" + id + ".zip");
		    response.setContentType(DownloadServlet.CONTENT_TYPE_ZIP);

	    
	        // Compress the files
	        Iterator filenamesIterator = filenames.iterator();
	        //for (int i=0; i < filenames.length; i++) {
            Logger.debug(this, "zipping " + filenames.size() + " files.");
			while (filenamesIterator.hasNext()){
			    String filename = "" + filenamesIterator.next();
	            FileInputStream in = new FileInputStream(filename);
	    
	            Logger.debug(this, "zipping " + filename);
	            
	            // Add ZIP entry to output stream.
	            // zipOutputStream.setMethod(ZipOutputStream.STORED); 
	            zipOutputStream.setLevel(0); 
	            zipOutputStream.putNextEntry(new ZipEntry(filename));
	            
	    
	            // Transfer bytes from the file to the ZIP file
	            int len;
	            while ((len = in.read(buf)) > 0) {
	                zipOutputStream.write(buf, 0, len);
	            }
	    
	            // Complete the entry
	            zipOutputStream.closeEntry();
	            in.close();
	        }
	    
	        // Complete the ZIP file
	        zipOutputStream.close();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
}%>



