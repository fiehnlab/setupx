<%@ include file="checknonlogin.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%> & Metabolomics Standards Initiative (MSI)</title>
</head>
<body>



<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>	
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

<%@ include file="incl_header_msi.jsp"%>
<br>

<table align="center" width="60%" cellpadding="5">
	<tr><th><h2>List of Publications related to the metabolomics standards initiative (MSI)</h2>
	<tr><td><a href="http://www.springerlink.com/openurl.asp?genre=article&volume=3&issue=3&spage=175&issn=1573-3882">The metabolomics standards initiative (MSI)</a>
	<tr><td><a href="http://www.springerlink.com/openurl.asp?genre=article&volume=3&issue=3&spage=179&issn=1573-3882">Standard reporting requirements for biological samples in metabolomics experiments: mammalian/in vivo experiments</a>
	<tr><td><a href="http://www.springerlink.com/openurl.asp?genre=article&volume=3&issue=3&spage=189&issn=1573-3882">Standard reporting requirements for biological samples in metabolomics experiments: microbial and in vitro biology experiments</a>
	<tr><td><a href="http://www.springerlink.com/openurl.asp?genre=article&volume=3&issue=3&spage=195&issn=1573-3882">Minimum reporting standards for plant biology context information in metabolomic studies</a>
	<tr><td><a href="http://www.springerlink.com/openurl.asp?genre=article&volume=3&issue=3&spage=203&issn=1573-3882">Standard reporting requirements for biological samples in metabolomics experiments: environmental context</a>
	<tr><td><a href="http://www.springerlink.com/openurl.asp?genre=article&volume=3&issue=3&spage=211&issn=1573-3882">Proposed minimum reporting standards for chemical analysis</a>
	<tr><td><a href="http://www.springerlink.com/openurl.asp?genre=article&volume=3&issue=3&spage=223&issn=1573-3882">Proposed reporting requirements for the description of NMR-based metabolomics experiments</a>
	<tr><td><a href="http://www.springerlink.com/openurl.asp?genre=article&volume=3&issue=3&spage=231&issn=1573-3882">Proposed minimum reporting standards for data analysis in metabolomics</a>
	<tr><td><a href="http://www.springerlink.com/openurl.asp?genre=article&volume=3&issue=3&spage=243&issn=1573-3882">A roadmap for the establishment of standard data exchange structures for metabolomics</a>
	<tr><td><a href="http://www.springerlink.com/openurl.asp?genre=article&volume=3&issue=3&spage=249&issn=1573-3882">Metabolomics standards initiative: ontology working group work in progress</a>
	</table>


<br>	
<%@ include file="incl_footer_msi.jsp"%>

