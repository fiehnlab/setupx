<%@ include file="checklogin.jsp"%>

<%@ page import="java.util.*,
                 org.setupx.repository.core.communication.importing.metadata.*,
                 org.setupx.repository.server.persistence.*"%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=org.setupx.repository.web.forms.PromtCreator.version%> </title>

<%@ page language="java" %>

<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<% int id = Integer.parseInt(request.getParameter("id")); %>

<body>
	<table align='center' border='0' cellpadding='4' width="60%">
		<td align="center" width="2">
			<a href="main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
		
			<h2>Detailed information about the sample <%=id %></h2> <br/>
		</th>
		<td align="center" width="2">
			<a href="www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</table>


	<% if (user.isLabTechnician()){ %>
	<%@ include file="navi_admin.jsp"%>
	<% 
	String acqname = new SXQuery().findAcquisitionNameBySampleID(id);
	
	// cut the extension at the end of the filename of
	acqname = acqname.substring(0, acqname.indexOf(":"));
	
	Hashtable ht = org.setupx.repository.core.communication.leco.LecoACQFile.getValuesPerACQname(acqname); %>
	
	<table align='center' border='0' cellpadding='4' width="60%">
		<tr>
			<th colspan="1"></th>
			<th colspan="1"></th>
			<th colspan="1"></th>
			<th colspan="1"></th>
		</tr>
		
		<% 
		Enumeration enumeration = ht.keys();
		while(enumeration.hasMoreElements()) { 
			String key = (String)enumeration.nextElement();
			String value = (String)ht.get(key);		
			%>
			<tr>
				<td></td>
				<td><b><%=key%><b/></td>
				<td><%=value%></td>
				<td></td>
			</tr>
		<% } %>
		<tr>
			<td colspan="1" align="right" style="background-color:#FFFFFF"><a href="sample_detail.jsp?id=<%=id%>"><img  border='0' src="pics/back.gif"></td>
			<td colspan="2"></td>
			<td colspan="1"></td>
		</tr>
	</table>
		
	
	
	
	<%} else { %>
	
	<table align='center' border='0' cellpadding='4' width="60%">
		<tr>
			<td colspan="1" align="center" style="background-color:#FFFFFF"><img  border='0' src="pics/error.jpg"></td>
			<td colspan="2" width = 90%>This is information is only available for lab personal.</td>
			<td colspan="1"></td>
		</tr>
		<tr>
			<td colspan="1"></td>
			<td colspan="2"></td>
			<td colspan="1"></td>
		</tr>
		<tr>
			<td colspan="1" align="right" style="background-color:#FFFFFF"><a href="sample_detail.jsp?id=<%=id%>"><img  border='0' src="pics/back.gif"></td>
			<td colspan="2"></td>
			<td colspan="1"></td>
		</tr>
	</table>
	
	<% } %>
</body>
</html>