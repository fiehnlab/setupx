

<%@page import="org.setupx.repository.core.communication.ncbi.local.NCBIEntry"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBIConnector"%>
<%@page import="org.setupx.repository.core.communication.ncbi.NCBI_Tree"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.setupx.repository.Config"%>
<%@ include file="checknonlogin.jsp"%>

<%@ include file="robot.jsp"%>

<%
	int ncbiID = Integer.parseInt(request.getParameter("ncbiID"));
	NCBIEntry entry = NCBIConnector.determineNCBI_Information(ncbiID);
	String encrypted = "************************************************************************************************************************************************************************************************************************************************************************************";
	
	boolean cachedQueries = org.setupx.repository.Config.QUERY_CACHING_ACTIVE_JSP;

	try {
		String cache = request.getParameter("cache");
	    if (cache.compareTo("false") == 0){
	        cachedQueries = false;
	    }
	} catch (Exception e){
	    
	}


%>


<%@page import="org.setupx.repository.server.persistence.SXQuery"%>
<%@page import="java.util.List"%>
<%@page import="org.setupx.repository.core.user.access.PromtUserAccessRight"%>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>



<table align='center' border='0' cellpadding='0' cellspacing="0" width="60%">
	<tr>
		<td colspan="21" style="background-color:#6682B6;" align="right"><font class="small"><%=new Date().toLocaleString() %>&nbsp;</font>

	<tr>
		<td width="1" style="background-color:#6682B6;" align="left" valign="top">
			<img  border='0' src="pics/corner_white2.gif" >
		<td colspan="21" style="background-color:white;" align="center">
			<font style="text-align: center; font-size:20">
				<b>species: <i><%=entry.getName()%></i></b>
			</font>
	<tr>
		<td style="background-color: white" align="center" colspan="21">
			<font class="small">The following information was found in the repository.<br>&nbsp;
			</font>

	<tr>
		<td colspan="7">
		<table align="center" width="100%" border=0 style="background-color:white; ">
			<tr>
							<form action="advancedquery_dev.jsp">
								<td style="background-color:white;" align="right" valign="top"><a href="q_main_ajax.jsp?type=0"><img  border='0' src="pics/back.gif"></td>
								<td style="background-color:white;" align="right" valign="top" width="400"></td>
								<td align="right" style="background-color: white"><input type="text" type="text" id="testinput" name="term" size="20" style="text-align: center; font-size:20" value="<%=entry.getName()	%>"> 
								<td style="background-color:white;" align="left"><input type="image" src="pics/go_search.gif" />
								<input type="hidden" name="type" value="0"> 
							</form>
		</table>
	</tr>


  	<tr>
	 	<th rowspan="821" style="background-color:white;">&nbsp;</th>
	 	<th rowspan="821" ">&nbsp;&nbsp;</th>
	    <th>&nbsp;</th>
	    <th>&nbsp;</th>
	    <th>&nbsp;</th>
	 	<th rowspan="821" ">&nbsp;&nbsp;</th>
	 	<th rowspan="821" style="background-color:white;">&nbsp;</th>
	</tr>
	
	<tr>
		<td align="center" colspan="3">
		<table width="100%">
			<tr>
				<td align="center">rank
				<td><%=entry.getRank()%>
			<tr>
				<td align="center">type
				<td><%=entry.getSpeciesTypeName()%>
			<tr>
				<td align="center">id
				<td><%=entry.getTaxID()%> <font class="small">(ncbi id)</font></td>
			<tr>
				<td align="center">group
				<td>
				<%
					NCBIEntry group = NCBI_Tree.findGroup(entry.getName());
				%>
				<a href="?ncbiID=<%=group.getTaxID() %>"><%=group.getName() %>
			<tr>
				<td align="center">parent
				<td>
						<a href="?ncbiID=<%=entry.getParent().getTaxID()%>">
						<%=entry.getParent().getName()%>
				<%
					Iterator childs = entry.getChilds().iterator();
					if (childs.hasNext()){
				%>
			<tr>
				<td align="center" valign="top">childs
				<td>
					<table>
			<%	
					while(childs.hasNext()){
					    NCBIEntry entry2 = (NCBIEntry)childs.next();
					    %>
					<tr>
						<td>
							<%if (entry2.isSplitpoint()){ %>
								<b>
						    	<img src='pics/c_3.gif' border="0" valign="middle" align="center">
							<%} else if (entry2.getChilds().size() > 0){%>
						    	<img src='pics/c_2.gif' border="0" valign="middle" align="center">
							<%} else {%>
						    	<img src='pics/c_1.gif' border="0" valign="middle" align="center">
							<%} %>
						    	
		
						    	<a href="?ncbiID=<%=entry2.getTaxID() %>"><%=entry2.getName()%> 
					    	<%if (NCBI_Tree.taxIDs.containsKey(entry2.getTaxID())){ %>[<%=NCBI_Tree.findExperiments(entry2.getTaxIdInt())%>]<%} %> 
					    <% 
				}
				%>
					</table>
				
				<%}
				
					
				List expList = new SXQuery().findPromtIDbySpecies(entry.getTaxIdInt());
				if (expList.size() > 0){
					int numberOfUnknownExp = 0;
				    %>




			<tr>
				<th colspan="2">Organs from this species in the repository
			<tr>
				<td colspan="2" align="center"><font class="small"></font>
			<tr>
				<td colspan="2">
					<table border="0" width="100%">



			<tr>
				<th colspan="2">Experiments with this species
			<tr>
				<td colspan="2" align="center"><font class="small">In case you have no permission to access the experiment the information will be hidden from you.</font>
			<tr>
				<td colspan="2">
					<table border="0" width="100%">
					<%
					Session my_ses = CoreObject.createSession();
					Iterator experimentIDs = expList.iterator();
					int alternate = -1;
					while (experimentIDs.hasNext()){
					    long promtID = Long.parseLong(experimentIDs.next().toString());
					    
					    int numberOfRunSamples = 0;
					    try {
					        Logger.log(this, "looking for samples:" );
					        String queryString = "select numberofScannedSamples from cache where experimentID = " + promtID;
					        Logger.log(this, "looking for samples:" + queryString);
					        
					        String num = my_ses.createSQLQuery(queryString).addScalar("numberofScannedSamples",Hibernate.INTEGER).setCacheable(cachedQueries).list().get(0) + "";
					        Logger.log(this, num);
						    numberOfRunSamples = Integer.parseInt(num);
					    } catch (Exception e){
					        numberOfRunSamples = 0;
					        e.printStackTrace();
					    }

					    if (numberOfRunSamples > 0){
						    // check access 
						    try {
						        PromtUserAccessRight.checkAccess(user.getUOID(), promtID, PromtUserAccessRight.READ);
							    %>
								<tr>
									<td 
									<% 
								    alternate = alternate * -1;
									if(alternate > 0){
										    %>
										    style="background-color: white"
										    <%
										}
									%>
									><%=new SXQuery().findPromtTitleByPromtID(promtID) %> 
										<font class="small">
											<br>
											&nbsp;&nbsp;&nbsp;&nbsp; <%=numberOfRunSamples%> Samples
										</font>
										<td><font class="small"><%=promtID %></font>
										<td><a href="pubexperiment.jsp?id=<%=promtID%>&action=20"><img src='pics/resize_x_plus.gif' border="0" valign="middle" align="center"></a> 
								<%
						    } catch (Exception e){
						        numberOfUnknownExp++;
						        
						        if (numberOfUnknownExp < 8){
							    %>
								<tr>
									<td><%=encrypted.substring(0,new SXQuery().findPromtTitleByPromtID(promtID).length()) %>
									<td><font class="small"><%=encrypted.substring(0,("" + promtID).length()) %></font>
									<td><img src='pics/locked.jpg' border="0" valign="middle" align="center">
								<% }
						    }
					    }
					}
					if (numberOfUnknownExp > 8){
					    %>
						<tr>
							<td>Additional <%=numberOfUnknownExp-8 %> experiments.
							<td><font class="small">------</font>
							<td><img src='pics/locked.jpg' border="0" valign="middle" align="center">
						<% }
				%>
					<tr>
				
				</table>
			    <%
					}
				%>
				</table>
				
			<tr>
				<th colspan="2">Compounds
			<tr>
				<td colspan="2" align="center">
					<font class="small">The following compounds have been found in samples that were taken from this species.</font>
			<tr>
				<td align="center" valign="top">
				<td colspan="1">
					<% int speciesID = entry.getTaxIdInt(); %>
					<!--  start -->
				 	<%@ include file="incl_compound_by_species.jsp"%>
					<!--  end -->

				
				
				
		<tr>
			<th colspan="2">&nbsp;
	<tr>
		<td colspan="21" style="background-color:white;" align="center">
</table>

					
<%@ include file="footer.jsp"%>
