<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net" xmlns:c="urn:jsptld:http://java.sun.com/jstl/core">
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Datafile"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Technology"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"/>
  <jsp:directive.page contentType="text/html; charset=UTF-8" />
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.hibernate.Hibernate"/>
<jsp:directive.page import="java.util.Vector"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
  
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>
  <head>
  
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />
  
  </head>




	<jsp:scriptlet>
		long sampleID = Long.parseLong(request.getParameter("sampleid"));
		List files = TechnologyProvider.getPossibleDatafiles(sampleID);
		request.setAttribute("test", files); 
	</jsp:scriptlet>

	<display:table id="row" name="test" defaultsort="1" defaultorder="descending" pagesize="20">
		<display:column title="Technology" sortable="true" headerClass="sortable"  href="techno_technology.jsp?" paramId="id" paramProperty="technologyFileType.technology.UOID">
		      <c:out value="${row.technologyFileType.technology.label}"/>
	    </display:column>
		<display:column title="FileType" sortable="true" headerClass="sortable"  href="techno_technologyfiletype.jsp?" paramId="id" paramProperty="technologyFileType.UOID">
		      <c:out value="${row.technologyFileType.label}"/>
	    </display:column>
		<display:column title="File name" property="sourceFile.name" sortable="true"></display:column>
		<display:column title="location" property="sourceFile.parent" sortable="true" ></display:column>
	    <display:column title="created on" property="creationDate" sortable="true" headerClass="sortable"/>
	    <display:column title="exists" sortable="true">
	    	<jsp:scriptlet>
	    		String img = "";
	    		if (((Datafile)row).getSourceFile().exists()){
	    			img = "pics/check.gif";
	    		} else {
	    			img = "pics/circle.gif";
	    		}
	    	</jsp:scriptlet>
			<![CDATA[<img src=]]>"<jsp:expression>img</jsp:expression>"<![CDATA[/>]]>
	    </display:column>
	</display:table>
	
</jsp:root>
	