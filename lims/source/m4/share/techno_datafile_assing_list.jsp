<jsp:root version="1.2" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:display="urn:jsptld:http://displaytag.sf.net" xmlns:c="urn:jsptld:http://java.sun.com/jstl/core">
<jsp:directive.page import="org.setupx.repository.core.util.logging.Logger"/>
<jsp:directive.page import="java.util.Date"/>
<jsp:directive.page import="java.io.File"/>
<jsp:directive.page import="java.util.Set"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"/>
<jsp:directive.page import="org.hibernate.Session"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Datafile"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyFileType"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.Technology"/>
<jsp:directive.page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"/>
  <jsp:directive.page contentType="text/html; charset=UTF-8" />
<jsp:directive.page import="org.setupx.repository.core.user.UserDO"/>
<jsp:directive.page import="java.util.Iterator"/>
<jsp:directive.page import="java.util.List"/>
<jsp:directive.page import="org.hibernate.Hibernate"/>
<jsp:directive.page import="java.util.Vector"/>
<jsp:directive.page contentType="text/html; charset=UTF-8" />
  <jsp:text>
    <![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">]]>
  </jsp:text>

  <jsp:text>
    <![CDATA[<html xmlns="http://www.w3.org/1999/xhtml" lang="en">]]>
  </jsp:text>

  <head>
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css" media="all">
      @import url("maven-base.css");
      @import url("maven-theme.css");
      @import url("site.css");
      @import url("screen.css");
  </style>
  <link rel="stylesheet" href="print.css" type="text/css" media="print" />

  </head>



<jsp:scriptlet>

	Set fileSet = (Set)session.getValue("files");

	Logger.debug(this, "adding list of files to session: " + fileSet);
	Logger.debug(this, "adding list of files to session (length): " + fileSet.size());
	
	
	//
	request.setAttribute("test", fileSet); 

	// style
	pageContext.setAttribute("tableclass", "its");

	long techofiletypeID = Long.parseLong(request.getParameter("tid"));
	
	Session s = TechnoCoreObject.createSession();
	TechnologyFileType technologyFileType = (TechnologyFileType)TechnologyFileType.persistence_loadByID((TechnologyFileType.class), s, techofiletypeID);
</jsp:scriptlet>


	<jsp:include page="techno_header.jsp"></jsp:include>


	<h2>
		<a href="techno_techno.jsp">all platforms</a> - 
		
		<![CDATA[<a href="techno_technology.jsp?id=]]>
		<jsp:expression>technologyFileType.getTechnology().getUOID()</jsp:expression>
		<![CDATA[">]]>
		<jsp:expression>technologyFileType.getTechnology().getLabel()</jsp:expression>
		<![CDATA[</a>]]>	
		- 
		<![CDATA[<a href="techno_technologyfiletype.jsp?id=]]>
		<jsp:expression>technologyFileType.getUOID()</jsp:expression>
		<![CDATA[">]]>
		<jsp:expression>technologyFileType.getLabel()</jsp:expression>
		<![CDATA[</a>]]>	
		- search result
	</h2>
	<display:table id="row" name="test">
		<jsp:scriptlet>
		// checking if the file is assigned to the actual technology yet
		System.out.println(">" + row);
		String path = "";
		if (row != null){
			File f = ((File)row);
		}
		String query1 = "select * from datafile where datafile.technologyfiletype = " + techofiletypeID + " and source = \"" + path + "\"";
		List listSameTechno = s.createSQLQuery(query1).addEntity(Datafile.class).list();
		
		String query2 = "select * from datafile where datafile.technologyfiletype != " + techofiletypeID + " and source = \"" + path + "\"";
		List listOthers = s.createSQLQuery(query2).addEntity(Datafile.class).list();
		
		// or any other technologies
		</jsp:scriptlet>
	
		<display:column title="File name" property="name" sortable="true"></display:column>
		<display:column title="location" property="parentFile" sortable="true" ></display:column>
		<display:column title="created" sortable="true" headerClass="sortable">
		      <jsp:expression>new Date(((File)row).lastModified()).toLocaleString()</jsp:expression>
	    </display:column>
	    
	    <display:column title="exists" sortable="true">
	    	<jsp:scriptlet>
	    		String img = "";
	    		if (row == null) {
	    		    img = "pics/error.gif";
	    		} else if (((File)row).exists()){
	    			img = "pics/check.gif";
	    		} else {
	    			img = "pics/circle.gif";
	    		}
	    	</jsp:scriptlet>
			<![CDATA[<img src=]]>"<jsp:expression>img</jsp:expression>"<![CDATA[/>]]>
	    </display:column>
	    <display:column title="assigned to this filetype" sortable="true">
	    	<jsp:expression>listSameTechno.size()</jsp:expression>
	    </display:column>
	    <display:column title="assigned to other filetypes" sortable="true">
	    	<jsp:expression>listOthers.size()</jsp:expression>
	    </display:column>
	    <display:column title="assigned at all" sortable="true">
	    	<jsp:expression>listOthers.size() + listSameTechno.size()</jsp:expression>
	    </display:column>
	    <display:column>
				<jsp:scriptlet>if (((File)row).isDirectory()){</jsp:scriptlet>
					- directory - 
				<jsp:scriptlet>} else {</jsp:scriptlet>
					<![CDATA[<a href="techno_datafile_assign_check.jsp?technofiletype=]]><jsp:expression>request.getParameter("tid")</jsp:expression><![CDATA[&]]><![CDATA[file=]]><jsp:expression>((File)row).getPath()</jsp:expression>"<![CDATA[>]]>
					assign to sample
					<![CDATA[</a>]]>
				<jsp:scriptlet>}</jsp:scriptlet>
	    </display:column>
	</display:table>
</jsp:root>


