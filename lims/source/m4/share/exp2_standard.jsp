<%@ include file="checklogin.jsp"%>

<%@ page import="java.io.InputStream,
                 java.io.IOException,
                 java.util.*,
                 org.setupx.repository.core.communication.importing.logfile.*,
				 org.setupx.repository.core.user.access.PromtUserAccessRight,
                 org.setupx.repository.web.forms.inputfield.multi.*,
                 org.setupx.repository.core.communication.leco.*,
                 org.setupx.repository.core.util.*,
                 org.setupx.repository.server.persistence.*"%>


<%@page import="org.setupx.repository.core.communication.file.SampleFile"%>
<%@page import="org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnoCoreObject"%>
<%@page import="org.hibernate.Hibernate"%>
<%@page import="org.setupx.repository.core.CoreObject"%>
<%@page import="org.setupx.repository.core.communication.technology.platform.TechnologyProvider"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><%=org.setupx.repository.Config.SYSTEM_NAME%>     Version:<%=PromtCreator.version%> </title>

<%@ page language="java" %>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<title>M1</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>

<%
int promtID = Integer.parseInt(request.getParameter("id"));
%>
<body>



<!--  head containing logo and description --> 
<table align='center' border='0' cellpadding='4' width='60%'>
	<tr>
		<td align="center" width="2">
			<a href="./main.jsp">
			<img src='pics/logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
		<th>
			<h2>Export</h2>
			Samples & Column
			<br/>
			<font size="-2">Below you find all samples including every available run for each sample related to the selected experiment.<br>Choose all samples that you want to include in the result.</font>
		</th>
		<td width="2" align="center" style="background-color:#FFFFFF">
			<%@ include file="incl_navi.jsp"%>
		</td>
		<td align="center" width="2">
			<a href="http://www.ucdavis.edu">
			<img src='pics/uc_logo.gif' border="0" valign="middle" align="center">
			</a>
		</td>
	</tr>
	
	
	
</table>

<% if (user.isLabTechnician()){ %>
<%@ include file="navi_admin.jsp"%>
<%} %>



<!--  column information -->
	<table align="center" width="60%" cellpadding="2">




	<table align="center" width="60%" cellpadding="2">	
	
	<tr>
		<th colspan="10">Select Samples for Export <%=promtID %></th>
	</tr>

	<%
	String[] colums =new BinBaseServiceConnector().determineColumns();
	%>

	<tr>
		<td colspan="10">
		<p align="center"><img src="pics/error2.gif" border="0">
		</p>
		<p>
		Please select carefully the samples and the actual runs that you want to export. By default the last run of a sample is selected. In order to change the
		selection, simply uncheck the box for the samples that you do not want to export and instead select the samples that you want.
		<p>
		In case you some of your samples have not been run the system will ignore these meaning that even an incomplete experiment can be exported.
		</td>
	</tr>
	<tr>
		<th colspan="10">Change Pre selection of samples</th>
	</tr>
	<tr>
		<td colspan="10">
		SetupX selects a certain number of samples and runs for you by default. Selected are all samples that completed and out of these only the most recent run is selected.
		If you want to change this default selection you can either do it one by one or select all the samples by defining what the filenames (or parts of it) are. <br>
		You can name as many parts of the filenames as you like - the file will be preselected if it matches any of them.
		<br>
		<form action="">
			<input type="hidden" name="id" value=<%=promtID %>>
			
			<%
				String az = request.getParameter("q");
				try {
					if (az.compareTo("null") == 0) {
					    az = "sa123 sa44";
					}
				} catch (Exception e){
				    az = "sa123 sa44";
				}
				    
			
			%>
			<input type="text" name="q" value="<%=az %>">
			<br>
			<input type="submit" title="select">
		</form>
	</tr>
<form method="post" action="exp2_summary.jsp">
	<tr>
		<td colspan="10">
		Before you export the data set please make sure that the correct column is selected below. This information is critical and a wrong selection will
		result in wrong result and even more important will create inconsistency in the selected database. <b>If you are not 100% sure which of the columns
		is the correct one for your data set get in contact with the lab manager.</b>
		<p>
		Please select the column that the samples were run on:
		<select name="column">
			<%
			for(int colC = 0; colC < colums.length; colC++){
			    %>
				<option value="<%=colums[colC] %>"  
				<%if (colums[colC].indexOf("rtx") > -1){ %> SELECTED <%}%> ><%=colums[colC] %>
				</option>
			    <%
			}
		%>
		</select>
	</tr>
	


<%
Session _Session = CoreObject.createSession();

String technoIDs = "";

String techQ_part = "";

techQ_part = "select uoid from technologyfiletype where " ;

for (int i = 0; i < SampleFile.TYPES.length; i++){
    techQ_part = techQ_part.concat("pattern like \"%" + SampleFile.getExtension(SampleFile.TYPES[i]) + "%\" ");

    // in between them
    if (i != SampleFile.TYPES.length -1){
        techQ_part = techQ_part.concat(" OR ");
    }
}


Iterator xxIter = _Session.createSQLQuery(techQ_part).list().iterator(); 
while(xxIter.hasNext()){
    technoIDs = technoIDs.concat(xxIter.next().toString());  
    if (xxIter.hasNext()){
        technoIDs = technoIDs.concat(", ");            
    }
}



List classList = new SXQuery(_Session).findClazzIDsByPromtID(promtID);
Iterator iterator = classList.iterator();

// each class
int staticCount = -1;
while (iterator.hasNext()) {
    staticCount++;
    int classID = Integer.parseInt("" + iterator.next());

    // class labels
    java.util.Iterator iterator1 = new SXQuery(_Session).findLabelsforClazz(classID).iterator();
    %>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<%
		int group = staticCount;
		String linkOtherClasses = "";
		try {
		    try {
			    group = Integer.parseInt(request.getParameter("group" + classID));    
		    } catch (NumberFormatException e){
		        //group stays staticcount
		    }
		    
		    // create a string containing ALL OTHER classes with there grouping
		    java.util.Iterator allClasses = new SXQuery(_Session).findClazzIDsByPromtID(promtID).iterator();
		    int initialID = -1;
		    while (allClasses.hasNext()){
		        initialID++;
		        long _id = Long.parseLong("" + allClasses.next());
		        if (_id != classID){

		            int _tmpGroup = initialID;
					try {
					    // in case the ids are not initaliesed.
			            _tmpGroup = Integer.parseInt(request.getParameter("group" + _id) );
					} catch (Exception e){
					    org.setupx.repository.core.util.logging.Logger.debug(this, e.getMessage());
					}
		            linkOtherClasses = linkOtherClasses + "&group" + _id + "=" + _tmpGroup;    
					
		            org.setupx.repository.core.util.logging.Logger.debug(this, "LINK OTHERS: " + linkOtherClasses);
		        }
		    }
		    
		} catch (Exception e){
			e.printStackTrace();
		}
		
		%>
		<th></th>
		<th colspan="3" align="center" style="background-color:<%=Util.getColor(classID*2)%>">Class <b><%=classID %></b></th>

		<th colspan="6">
			
			<table align='center' border='0' cellpadding='4'>
			  <tr>
			  
				<% 
				//deactivated
				for (int cl = 0; cl < 0; cl++){ //classList.size(); cl++){ %>
				    <!--  missing is the ids of all other classes in the href -->
					<th>
						<!--  deactivated  -->
						<!--  <a href="export_selection.jsp?id=<%=promtID%>&group<%=classID%>=<%=cl%><%=linkOtherClasses %>"> -->
							<img src='<%if (cl==group){ %>pics/checked.jpg<%} else {%>pics/blank.jpg<%}%>' border="0" valign="middle" align="center">
						<!-- </a> -->
						<%if (cl==group){ %><input type="hidden" name="group<%=classID%>" value="<%=group%>"><%} %>


					</th>
				    <th valign="middle" align="center" style="background-color:<%=Util.getColor(cl*2)%>" >
				    	<b><%=cl%></b>
				    </th>
			    <%} %>
			  </tr>
			</table>
		</th>
		<th></th>
	</tr>
	<tr>
	<td colspan="1"></td>
	<td style="background-color:<%=org.setupx.repository.core.util.Util.getColor(classID*2)%>"  colspan="1"></td>
	<td colspan="2">
	<td colspan="6">
		<table>
    <% 
    while (iterator1.hasNext()) {
    Object[] element = (Object[]) iterator1.next();
      %>
			<tr>
					<td><%=element[0].toString()%>:</td>
					<td><b><%=element[1].toString()%></b></td>
			</tr>
      <%
    }
 	%>
			</table>
	    </td>
	  <td></td>
	</tr>
	
	
	<%
	Session hqlSession = TechnoCoreObject.createSession();

	// samples in that class
    Iterator sampleIDs = new SXQuery(_Session).findSampleIDsByClazzID(classID).iterator();
    while (sampleIDs.hasNext()){
        int sampleID = Integer.parseInt("" +  sampleIDs.next());
        //String label = new SXQuery().findSampleLabelBySampleID(sampleID);
        


		String query = 
            " select " +
            " SUBSTRING_INDEX(SUBSTRING_INDEX(d.source, \".\", 1), \"/\", -1) as XX " +
            " from datafile as d " +
//            " join technologyfiletype as t " +
//            " on t.uoid = d.technologyfiletype " +
//            " and (" + techQ_part + ") " +
            " where d.sampleID = " + sampleID + " " +
            " and d.technologyfiletype in (" + technoIDs + ")"+
            " group by SUBSTRING_INDEX(SUBSTRING_INDEX(d.source, \".\", 1), \"/\", -1) " +
            " order by SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(d.source, \".\", 1), \"/\", -1),\"_\",-1) desc";

        Logger.log(this, "query:" + query);
        
        List datafilesAsRun = hqlSession.createSQLQuery(query).addScalar("XX", Hibernate.STRING).list();

        
        Iterator datafileNamesIter = datafilesAsRun.iterator();
        boolean checked = true;
        %>

		<% if(datafilesAsRun.size() == 0){%>
        <tr>
        <td ></td>        
        <td style="background-color:<%=org.setupx.repository.core.util.Util.getColor(classID*2)%>" align="center"></td>        
        <td align="center" style="background-color:#FFFFFF"></td>        
        <td align="center">Sample <b><%=sampleID%></b></td>        
        <td align="center"><%=new SXQuery(_Session).findSampleLabelBySampleID(sampleID) %></td>        
        <td align="center" colspan="3"><b>Sample has not been run yet.</b></td>        
		<td align="center"><img src='pics/hprio_tsk.gif' border="0" valign="middle" align="center"></td>        
        <td align="center" style="background-color:#FFFFFF"><input type="checkbox" disabled="disabled"></td>        
        <td></td>        
        </tr>
		<%} %>        
        
        
        <%
        HashSet hash = new HashSet();
        boolean first= true;
        while (datafileNamesIter.hasNext()) {
			String acqname = (String) datafileNamesIter.next();
			if (!hash.contains(acqname)){
	            hash.add(acqname);
				boolean exists = SampleFile.exists(acqname, SampleFile.TXT);  
	            %>
	        <tr>
	        <td></td>        
			<td style="background-color:<%=Util.getColor(classID*2)%>" align="center"></td>
			<td width="2" align="center" style="background-color:#FFFFFF"><% if (first){ %><a href="sample_detail.jsp?id=<%=sampleID%>"><img src='pics/details.gif' border="0" valign="middle" align="center"></a><%}%></td>        
	        <td align="center"><% if (first){ %>Sample <b><%=sampleID%> <%}%></b></td>        
	        <td align="center"><% if (first){ %><%=new SXQuery(_Session).findSampleLabelBySampleID(sampleID) %> <%}%></td>        

	        <!--  hotfix -->
	        <td align="center" <%if (!exists){%> style="background-color:#FF545A" <%} %> colspan="3"><%=acqname%></td>


	        <%
	        // added: 
	        // another parameter allows to define what elements have to be contained in the filename
	        String q = "";
	        try {
	        	// 	
	        	q = request.getParameter("q");
	        	StringTokenizer tokenizer = new StringTokenizer(q," ");
	            boolean loop = true;
	            while (tokenizer.hasMoreTokens()){
	                String token = "";
	                token = tokenizer.nextToken();
	                if (loop && acqname.indexOf(token) > -1){
	                    // the token is inside the acqname 
	                    // checked = true;
	                    loop = false;
	                }
	            }
	            if (loop && checked){
	                checked = false;
	            }
	        } catch (Exception e){
	            
	        }
	        %>



			<%if (exists){ %>
	        <td align="center" valign="middle" width="2">
				<a href="run_detail_acq.jsp?acq=<%=acqname%>">	
		    	    <img src='pics/details.gif' border="0" valign="middle" align="center">
		        </a>
	        </td>        
	        <td align="center" style="background-color:#FFFFFF"><input type="checkbox" name="<%=acqname%>" value="<%=sampleID%>" <%if (checked){ %>checked="checked" <%} %>></td>        
	        <%} else { %>
			<td align="center" valign="middle">
				<img src='pics/warning.gif' border="0" valign="middle" align="center">
				<font class="error">Sample ran successfully but file was not converted.</font>
			</td>
	        <td align="center" style="background-color:#FFFFFF"><input type="checkbox" disabled="true" name="<%=acqname%>" value="<%=sampleID%>" <%if (checked){ %>checked="checked" <%} %>></td>        
	        <%}%>
	        <td>
	        	<table>
	        		<tr>
	        			<%if (SampleFile.exists(acqname, SampleFile.TXT)){ %>
	        				<td style="background-color: green"><font class="small">TXT</font></td>
	        			<%} else { %>
	        				<td style="background-color: red"><font class="small">TXT</font></td>
	        			<%} %>
	        			<%if (SampleFile.exists(acqname, SampleFile.PEG)){ %>
	        				<td style="background-color: green"><font class="small">PEG</font></td>
	        			<%} else { %>
	        				<td style="background-color: red"><font class="small">PEG</font></td>
	        			<%} %>
	        			<%if (SampleFile.exists(acqname, SampleFile.CDF)){ %>
	        				<td style="background-color: green"><font class="small">CDF</font></td>
	        			<%} else if (SampleFile.exists(acqname, SampleFile.xCDF)){ %>
	        				<td style="background-color: green"><font class="small">xCDF</font></td>
	        			<%} else { %>
	        				<td style="background-color: red"><font class="small">CDF</font></td>
	        			<%} %>
	        			<%if (SampleFile.exists(acqname, SampleFile.SMP)){ %>
	        				<td style="background-color: green"><font class="small">SMP</font></td>
	        			<%} else { %>
	        				<td style="background-color: red"><font class="small">SMP</font></td>
	        			<%} %>
        			</tr>
       			</table>
	        </td>        
	        </tr>
			<%
            if (checked){   
                checked = false;
            }
        	}
			first = false;
        }
    }
}
%>
	  		<tr>
  					<td><input type="hidden" name="promt" value="<%=promtID%>"></td>
  					<td colspan="6" align="right">Send selection to Export.  </td>
  					<td colspan="2" align="left"><input src='pics/go_export.gif' type="image" value="connect"/></td>
  					<td></td>
	  		</tr>
	<tr>
		<td colspan="1"></td>
		<td colspan="8">
		<td colspan="1"></td>
	</tr>
	<%
	%>
</table>
</form>
</body>

<%@ include file="footer.jsp"%>
