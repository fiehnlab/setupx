package org.setupx.util;

import java.security.Security;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.SXMailAuthenticator;
import org.setupx.repository.core.util.logging.Logger;

import com.sun.mail.smtp.SMTPMessage;

public class SXMail extends CoreObject{

    private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

    public static void main(String args[]) throws Exception {
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        String emailMsgTxt = "Test Message Contents";
        String emailSubjectTxt = "A test from gmail";
        String[] sendTo = { "mscholz@ucdavis.edu" };
        new SXMail().sendSSLMessage(sendTo, emailSubjectTxt, emailMsgTxt, Config.OPERATOR_ADMIN.getEmailAddress());
    }

    public void sendSSLMessage(String recipients[], String subject, String message, String from)
            throws MessagingException {
        /*
         * Properties p = new Properties(); p.put("mail.smtp.user", Config.MAIL_SERVER_USERNAME);
         * p.put("mail.smtp.host", Config.MAIL_SERVER_NAME); p.put("mail.smtp.port", Config.MAIL_SERVER_PORT);
         * p.put("mail.smtp.starttls.enable", "true"); p.put("mail.smtp.auth", "true"); p.put("mail.smtp.debug",
         * "true"); p.put("mail.smtp.socketFactory.port", Config.MAIL_SERVER_PORT);
         * p.put("mail.smtp.socketFactory.class", SSL_FACTORY); p.put("mail.smtp.socketFactory.fallback", "false");
         */

        Properties p = new Properties();
        p.put("mail.smtp.user", "setupx.mail@gmail.com");
        p.put("mail.smtp.host", Config.MAIL_SERVER_NAME);
        p.put("mail.smtp.port", "465");
        p.put("mail.smtp.starttls.enable", "true");
        p.put("mail.smtp.auth", "true");
        p.put("mail.smtp.debug", "false");
        p.put("mail.smtp.socketFactory.port", "465");
        p.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        p.put("mail.smtp.socketFactory.fallback", "false");

        SecurityManager security = System.getSecurityManager();
        log("Security Manager: " + security);

        try {
            Authenticator auth = new SXMailAuthenticator(Config.MAIL_SERVER_USERNAME, Config.MAIL_SERVER_PASSWORD);
            Session session = Session.getInstance(p, auth);

            Logger.info(this, p.toString());

            session.setDebug(true);

            Message msg = new SMTPMessage(session);
            InternetAddress addressFrom = new InternetAddress(from);
            msg.setFrom(addressFrom);

            InternetAddress[] addressTo = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressTo[i] = new InternetAddress(recipients[i]);
            }
            
            // tmp deactivated - AS LONG AS IT IS NOT ABSOLUTLY FINAL ALL EMAIL GO TO ME !!
            // msg.setRecipients(Message.RecipientType.TO, addressTo);
            msg.setRecipients(Message.RecipientType.TO, new Address[]{new InternetAddress("martin.scholz.usa@gmail.com")});

            // Setting the Subject and Content Type
            msg.setSubject(subject);
            msg.setContent(message, "text/plain");
            Transport.send(msg);
        } catch (Exception mex) { // Prints all nested (chained) exceptions as well
            mex.printStackTrace();

        }
    }

}
