package org.setupx.util.pubchem.pug;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/*import org.apache.xpath.XPathAPI;*/
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
<h2>Submit complex queries to the Power User Gateway.</h2>

<h3>how ?</h3>
 * <ol>
 * <li>creating a request
 * <li>receiving an PCT-Waiting_reqid
 * <li>check the {@link #responseDocument} for the requiered download {@link URL}  
 * <li>{@link #refresh()} the request until the response contains a download {@link URL}
 * <li>{@link #store(URL, File)} the result in a local file.
 * </ol>


<h3>Code</h3>
The simple way to use the this class:<p>
<code>
int[] pctIDs = new int[]{1,2,3,4};<br>
{@link PowerUserGatewayRequest} request = new {@link PowerUserGatewayRequest}(pctIDs);<br>
<b>request.submitInitalRequest();</b><br>
<br>
while(!request.getStatus().isFinished()){<br>
&nbsp;&nbsp;&nbsp;System.out.println("waiting for " + request.getRequestID());
&nbsp;&nbsp;&nbsp;request.refresh();<br>
}
<br>
// download it to your directory<br>
{@link PowerUserGatewayRequest}.store(request.getResponseURL(), targetFile);<br>
<br><br>
</code>


<h2>Update:  Java 1.4 & Java5</h2>
 added support for java 1.4 and java 5:<br>
 {@link #xpath(String, Document)} method is now redirecting to {@link #xpathJDK1_4(String, Document)} or {@link #xpathJDK5(String, Document)}.<br>
 Seperation was needed to support java5 without additional libraries.<br>

 <b>In case you are running it on java1.4 </b> you need to import <code>org.apache.xpath.XPathAPI</code> and modify the mehtod {@link #xpath(String, Document)}:
 <br><br>
 <code>
public static Node <b>xpath(String pathString, final Document doc)</b> throws Exception {<br>
</code><b>depending on your JDK use one of the methods.<code>
<font color="red"><br>
<i>&nbsp;&nbsp;//Node node = xpathJDK1_4(pathString, doc);</i><br>
&nbsp;&nbsp;Node node = xpathJDK5(pathString, doc);<br> 
</font>
&nbsp;&nbsp;return node;<br>
}
</code>
<p>

<hr>
<a rel="license" href="http://creativecommons.org/licenses/by/3.0/us/">
<img alt="Creative Commons License" style="border-width:0" src="http://creativecommons.org/images/public/somerights20.png" />
</a>
<br />This work is licensed under a 
<a rel="license" href="http://creativecommons.org/licenses/by/3.0/us/">Creative Commons Attribution 3.0 United States License</a>.
<hr>
 *
 * @see http://pubchem.ncbi.nlm.nih.gov/ 
 * @see http://depth-first.com/articles/2007/06/04/hacking-pubchem-power-user-gateway
 * @see http://depth-first.com/articles/2007/06/11/hacking-pubchem-learning-to-speak-pug
 * 
 * @author <a href=mailto:mscholz@ucdavis.edu>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.21 $
 *
 */
public class PowerUserGatewayRequest {
    public static final String COMPRESSION_NONE = "none";
    public static final String COMPRESSION_GZIP = "gzip";
    public static final String COMPRESSION_BZIP2 = "bzip2";


    /** the PubChem ids that will be requested from the Power User Gateway*/ 
    private int[] pctIDs;

    private Document requestDocument;
    private Document responseDocument;

    private String requestid;

    private String downloadFormat = "sdf";

    private String compression = COMPRESSION_GZIP;
    
    private static final String url = "http://pubchem.ncbi.nlm.nih.gov/pug/pug.cgi";
    private static final String ID_xpath = "//*/PCT-Waiting_reqid";


    /**
     * creata a new instance of the request. <pre>The request is <b>not</b> sent.
     * @param pctIDs the pubchem IDs 
     */
    public PowerUserGatewayRequest (final int[] pctIDs) {
        debug("creating request");
        this.pctIDs = pctIDs;
    }


    /**
     * 
     * @throws Exception 
     * @since JAVA5
     * 
     */
    private static Node xpathJDK5(String pathString, Document doc) throws Exception {
        XPath xpath = XPathFactory.newInstance().newXPath();
        try {
            Node node = ((Node)xpath.evaluate(pathString, doc, XPathConstants.NODE));
            return node;
        } catch (DOMException e) {
            throw new Exception(e.toString());
        } catch (XPathExpressionException e) {
            throw new Exception(e.toString());
        }
    }

    /**
     * reuqieres import {@link org.apache.xpath.XPathAPI}
     */
    /*
    private static Node xpathJDK1_4(String pathString, Document doc) throws DOMException, TransformerException {
        return XPathAPI.selectNodeList(
                doc.getDocumentElement(), 
                pathString
               ).item(0);
    }
     */

    /**
     * creating a new request and checking for an update. The {@link #responseDocument} will be updated and overwirtten by the new {@link Document} received 
     * after the {@link #refresh()}. <p>
     * Before the actual refresh is committed / submitted there is a 3000 pause. 
     */
    public final void refresh() throws ParserConfigurationException, TransformerConfigurationException, IOException, SAXException, FactoryConfigurationError, TransformerFactoryConfigurationError, TransformerException {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

        this.initRefreshRequestDocument();

        // submit the actual request.
        this.request();
    }


    public final void submitInitalRequest() throws ParserConfigurationException, IOException, SAXException, FactoryConfigurationError, TransformerConfigurationException, TransformerFactoryConfigurationError, TransformerException {
        // create the request
        this.initInitalRequestDocument();

        // submit it
        this.request();
    }


    /**
     * submit the {@link #requestDocument} ({@link Document}). 
     */
    private final void request() throws IOException, SAXException, ParserConfigurationException, FactoryConfigurationError, DOMException, TransformerException {
        debug("submit request");
        URL server = new URL(url);
        HttpURLConnection connection = (HttpURLConnection)server.openConnection();
        //connection.connect();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "text/xml; charset=\"utf-8\"");
        connection.setDoOutput(true);
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF8");
        out.write(getXMLString(this.requestDocument));
        out.close(); 

        this.responseDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(connection.getInputStream());

        try {
            // finding the request ID. - the request ID is only received in the inital request and not in the updates.
            this.requestid = xpath(ID_xpath,responseDocument).getFirstChild().getNodeValue();
        } catch (Exception e) {
            // ignore
            // debug("can not determine new ID");
        }
    }


    private final void initRefreshRequestDocument() throws ParserConfigurationException {
        // build a new request
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        org.w3c.dom.Document doc = builder.newDocument();

        String[] tagNames = new String[]{
                "PCT-Data_input", 
                "PCT-InputData", 
                "PCT-InputData_request", 
                "PCT-Request"
        };


        // create a root object
        doc.appendChild(doc.createElement("PCT-Data"));
        Element element = doc.getDocumentElement();

        for (int i = 0; i < tagNames.length; i++) {
            String tagname = tagNames[i];
            Element child = doc.createElement(tagname);

            element.appendChild(child);
            element = child;
        }



        Element reqid = doc.createElement("PCT-Request_reqid");
        reqid.appendChild(doc.createTextNode(this.getRequestID()));

        Element type = doc.createElement("PCT-Request_type");
        type.setAttribute("value","status");

        element.appendChild(reqid);
        element.appendChild(type);


        this.requestDocument = doc;
    }


    /** 
     * init the first request. Build the document submitted to the PUG. <pre>in case you want to make modifications to the Request (add additional parameters, 
     */
    protected void initInitalRequestDocument() throws ParserConfigurationException {
        debug("init request");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        org.w3c.dom.Document doc = builder.newDocument();

        String[] tagNames = new String[]{
                "PCT-Data_input", 
                "PCT-InputData", 
                "PCT-InputData_download", 
                "PCT-Download", 
                "PCT-Download_uids", 
                "PCT-QueryUids", 
                "PCT-QueryUids_ids", 
                "PCT-ID-List"
        };


        // create a root object
        doc.appendChild(doc.createElement("PCT-Data"));
        Element element = doc.getDocumentElement();

        for (int i = 0; i < tagNames.length; i++) {
            String tagname = tagNames[i];
            Element child = doc.createElement(tagname);

            element.appendChild(child);
            element = child;
        }

        Element pccompound = doc.createElement("PCT-ID-List_db");
        pccompound.appendChild(doc.createTextNode("pccompound"));

        Element pctidlistuids = doc.createElement("PCT-ID-List_uids");
        // loop over id s 
        for (int i = 0; i < pctIDs.length; i++) {
            int pctID = pctIDs[i];
            Element pctIDNode = doc.createElement("PCT-ID-List_uids_E");
            pctIDNode.appendChild(doc.createTextNode(pctID + "" ));
            pctidlistuids.appendChild(pctIDNode);
        }


        try {
            Node tmp = xpath("//*/PCT-Download", doc);


            Element format = doc.createElement("PCT-Download_format");
            format.setAttribute("value",this.getDownloadFormat());
            tmp.appendChild(format);

            Element compress = doc.createElement("PCT-Download_compression");
            compress.setAttribute("value",this.getCompression());
            tmp.appendChild(compress);

        } catch (Exception e1) {
            e1.printStackTrace();
        }

        element.appendChild(pccompound);
        element.appendChild(pctidlistuids);

        this.requestDocument = doc;
    }


    /**
     * reuqieres import {@link org.apache.xpath.XPathAPI}
     */
    /*
    private static Node xpathJDK1_4(String pathString, Document doc) throws DOMException, TransformerException {
        return XPathAPI.selectNodeList(
                doc.getDocumentElement(), 
                pathString
               ).item(0);
    }
     */

    /** 
     * @return status of my request. <code>{@link Status#isFinished()}</code> tells you if the result is ready or not.
     */
    public final Status getStatus() {
        try {
            this.getResponseURL();
            return new Status(true, null);
        } catch (URLNotAvailableException e) {
            return new Status(false, e);
        }
    }


    /**
     * @return the id for the request received from the PowerUserGateway.<pre>Until the request is submitted the id is invalid.</pre>
     */
    public final String getRequestID() {
        return this.requestid;
    }


    /**
     * @return url on the ncbi servers pointing to the resultfile.
     * @throws URLNotAvailableException
     */
    public final URL getResponseURL() throws URLNotAvailableException{
        try {
            return new URL(xpath("//*/PCT-Download-URL_url", this.responseDocument).getFirstChild().getNodeValue());
        } catch (Exception e){
            throw new URLNotAvailableException(e);
        }
    }


    public final String getCompression() {
        return compression;
    }


    public final String getDownloadFormat() {
        return downloadFormat;
    }


    /**
     * convert an XML {@link Document} into a {@link String}.
     * @return String containg the document.
     */
    private static String getXMLString(Document requestDocument){
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            DOMSource source = new DOMSource(requestDocument);
            StringWriter stringWriter =new StringWriter();
            StreamResult streamResult = new StreamResult(stringWriter);
            transformer.transform(source, streamResult);
            return stringWriter.toString();
        } catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }    

    private static void debug(String string) {
        System.out.println(string);
    }


    class URLNotAvailableException extends Exception {
        public URLNotAvailableException(Exception e) {
            super(e);
        }
    }

    /**
     * Status.
     * @author scholz
     */
    class Status {

        private boolean status;
        private String msg;

        Status(boolean b, Object object) {
            this.status = b;
            if (msg != null){
                this.msg = object.toString();
            } else {
                this.msg = "--";
            }
        }

        public boolean isFinished() {
            return status;
        }

        public String getMessage(){
            return msg;
        }
    }


    public final void setCompression(String compression) {
        this.compression = compression;
    }


    public final void setDownloadFormat(String downloadFormat) {
        this.downloadFormat = downloadFormat;
    }


    public static Node xpath(String pathString, final Document doc) throws Exception {
        Node node = xpathJDK5(pathString, doc);
        //Node node = xpathJDK1_4(pathString, doc);
        return node;
    }


    /**
     * Receive a binary file and store it in a local file.
     * @param url URL the file will be requested from.
     * @param targetFile file the data will be stored at.
     */
    public static void store(URL url, File targetFile) throws IOException {
        URLConnection uc = url.openConnection();
        String contentType = uc.getContentType();
        int contentLength = uc.getContentLength();
        if (contentType.startsWith("text/") || contentLength == -1) {
            throw new IOException("This is not a binary file.");
        }
        InputStream raw = uc.getInputStream();
        InputStream in = new BufferedInputStream(raw);
        byte[] data = new byte[contentLength];
        int bytesRead = 0;
        int offset = 0;
        while (offset < contentLength) {
            bytesRead = in.read(data, offset, data.length - offset);
            if (bytesRead == -1)
                break;
            offset += bytesRead;
        }
        in.close();
    
        if (offset != contentLength) {
            throw new IOException("Only read " + offset + " bytes; Expected " + contentLength + " bytes");
        }
    
        FileOutputStream out = new FileOutputStream(targetFile);
        out.write(data);
        out.flush();
        out.close();
        System.out.println("file has been stored at " + targetFile.toString());
    }
}
