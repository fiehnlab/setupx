package org.setupx.util;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Date;

public class FileAgeFilter implements FilenameFilter {

    private Date end;
    private Date start;
    private Date d;

    public FileAgeFilter(Date start, Date end) {
        this.start = start;
        this.end = end;
    }

    /**
     * accepts files of a certain time period
     */
    public boolean accept(File dir, String name) {
        this.d = new Date(new File(dir, name).lastModified());
        return (start.before(d) && end.after(d));
    }
}
