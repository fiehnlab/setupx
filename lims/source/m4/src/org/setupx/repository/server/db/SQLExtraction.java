package org.setupx.repository.server.db;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class SQLExtraction {

    private String content;
    private File sourceFile;

    public SQLExtraction(File file) {
        this.sourceFile = file;

        try {
            this.content = org.setupx.repository.core.util.File.getFileContent(this.sourceFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void process() {
        boolean found = true;
        int offset = 0;
        // find position of the
        while (found) {

            System.out.println(this.sourceFile.getName() + " offset:" + offset);

            int selectPos = this.content.indexOf("select ", offset);
            System.out.println(this.sourceFile.getName() + " found select at pos: " + selectPos);

            if (selectPos < 0) {
                found = false;
                return;
            }

            int closePos = 0;
            int openPos = 0;
            try {
                // find the opening bracket
                try {
                    openPos = this.content.substring(0, selectPos).lastIndexOf('"');
                } catch (Exception e) {
                    openPos = offset - 1;
                }
                if (openPos < offset) {
                    throw new UnsupportedTermException("unable to find opening");
                }
                System.out.println(this.sourceFile.getName() + " got open pos: " + openPos);

                // find the end
                // it is the closing with a ;
                // ERR this is not the END
                closePos = this.content.substring(selectPos).indexOf(';') + 1;
                System.out.println(this.sourceFile.getName() + " got close pos: " + closePos);

                System.out.println(this.sourceFile.getName() + " creating extraction " + openPos + " to "
                        + (closePos + openPos));
                String extract = this.content.substring(openPos, closePos + openPos);

                if (extract.indexOf("from") < 0) {
                    // System.out.println(extract);
                    throw new UnsupportedTermException("does not include a from term");
                }

                if (extract.indexOf("SQLStatementPool") > 0) {
                    // System.out.println(extract);
                    throw new UnsupportedTermException("was processed already - it includes (SQLStatementPool)");
                }

                SQLExtractionTerm extractionTerm = new SQLExtractionTerm(extract);

                // putting it back together
                String tmpcontent = this.content.substring(0, openPos);

                String label = "default";

                // SQLStatementPool.getStatement(this, "isactive", "select uoid from formobject where active = 'true'
                // and uoid = " + SQLStatement.TOKENNAME ).setSingleParameterValue(id);
                // String term = "SQLStatementPool.getStatement(this, \"" + label + "\" , \"" + extractionTerm.getTerm()
                // + "\" + SQLStatement.TOKENNAME).setSingleParameterValue(" + extractionTerm.getPlaceholder() + ");";
                
                if (extractionTerm.getPlaceholder().size() == 1){
                    String term = "SQLStatementPool.getStatement(\"" + this.sourceFile.getName() + "\", \"" + label + "\")" +
                    ".setSingleParameterValue("
                    + ((Placeholder)extractionTerm.getPlaceholder().get(0)).getVariableName() + ")";

                    SQLStatementPool.initStatement(this.sourceFile.getName(), label, extractionTerm.getTerm());

                    tmpcontent = tmpcontent.concat(term);
                }
                

                tmpcontent = tmpcontent.concat(this.content.substring((closePos + openPos), this.content.length()));

                offset = closePos;
                this.content = tmpcontent;

                // store it ..
                try {
                    org.setupx.repository.core.util.File.storeData2File(new File("/Users/scholz/Desktop/modified/"
                            + this.sourceFile.getName()), this.content);
                    org.setupx.repository.core.util.File.storeData2File(new File("/Users/scholz/Desktop/modified/"
                            + this.sourceFile.getName() + "orig"), org.setupx.repository.core.util.File
                            .getFileContent(this.sourceFile));
                    
                    
                    
                    
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } catch (UnsupportedTermException e) {

                // e.printStackTrace();
                offset = selectPos + 1;// (closePos+openPos) ;
                System.out.println(this.sourceFile.getName() + " setting offset to " + offset + " because: "
                        + e.getMessage());

            } catch (Exception e) {
                e.printStackTrace();
                found = false;
            }
        }
    }
}
