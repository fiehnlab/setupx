/**
 * ============================================================================ File:    SXQuery.java Package: org.setupx.repository.server.persistence cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.server.persistence;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.exporting.AcquisitionParameter;
import org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample;
import org.setupx.repository.core.communication.file.SampleFile;
import org.setupx.repository.core.communication.ldap.AcquisitionFileException;
import org.setupx.repository.core.communication.leco.LecoACQFile;
import org.setupx.repository.core.communication.msi.AttributeValuePair;
import org.setupx.repository.core.communication.ncbi.NCBIConnector;
import org.setupx.repository.core.communication.ncbi.NCBIException;
import org.setupx.repository.core.communication.technology.platform.Datafile;
import org.setupx.repository.core.communication.technology.platform.Technology;
import org.setupx.repository.core.communication.technology.platform.TechnologyProvider;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.UserLocal;
import org.setupx.repository.core.user.access.PromtUserAccessRight;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.event.Event;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;
import org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.Sample;
import org.setupx.repository.web.forms.page.Page;


/**
 * All relevant Queries. Including Session handling.
 * 
 * <p>
 * <b> usage: </b>
 * </p>
 * 
 * <p>
 * <code> new SXQuery().xxx(); </code>
 * </p>
 * 
 * <p>
 * <b>If you want to extend the functionality:</b>
 * </p>
 * 
 * <p>
 * add session handling at the start and end of the new query method. Like:
 * </p>
 * 
 * <p>
 * <code>public int findXXXX(long i) throws PersistenceActionFindException {<br><b>this.startTransacion();</b><br> String basic = "...";<br> Query query = this.session.createQuery(basic);<br> List result = query.setCacheable(this.isCacheActive()).list();<br><b>this.finishTransaction();</b><br> return (...);<br>}<br>}<br></code>
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class SXQuery extends CoreObject {
    //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private org.hibernate.Session session;
    private Transaction transaction;

    //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private static final long serialVersionUID = -2555032216515467879L;
    private static SessionFactory sessionFactory;
    
    
    // activates the caching 
    private boolean cacheActive = Config.QUERY_CACHING_ACTIVE_SXQUERY;

    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new SXQuery object.
     */
    public SXQuery() {
    }

    /**
     * Creates a new SXQuery object.
     */
    public SXQuery(boolean caching) {
        this.setCacheActive(caching);
    }

    /**
     * Creates a new SXQuery object.
     *
     * @param _session 
     */
    public SXQuery(org.hibernate.Session _session) {
        this.session = _session;
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * checks if a formobject is active or not 
     *
     * @param id uoid of the formobject
     *
     * @return active or not
     *
     */
    public boolean isActive(int id) throws PersistenceActionFindException {
        this.startTransacion();

        String basic = "select uoid from formobject where active = 'true' and uoid = " + id; 

        //Logger.debug(this, basic);
        try {
            List list = this.session.createSQLQuery(basic).addScalar("uoid", org.hibernate.Hibernate.LONG).setCacheable(this.isCacheActive()).list();
            this.finishTransaction();

            return list.size() > 0;
        } catch (Exception e) {
            warning(new PersistenceActionFindException("System problem: unable to find by " + id, e));

            return false;
        }
    }

    /**
     * is a promt finished?. Checkes if each of the samples related to this experiment has been run.
     * updated to use {@link Datafile}s
     *
     * @param promt_id the promt id
     *
     * @return true if finished
     */
    public boolean determinePromtFinished(long promt_id) {
        return (this.determineNumberOfSamples(promt_id) == this.determineNumberOfSamplesFinished(promt_id));
    }

    /**
     * checkes if a promt with a specific id has been started yet
     * updated to use {@link Datafile}s
     */
    public boolean determinePromtRunStarted(long promt_id) {
        return (this.determineNumberOfSamplesFinished(promt_id) > 0);
    }

    /**
     * check if a sample has been started yet. Checks the scanned table, if there is one or more entries that indicate that the sample has been run
     * updated to use {@link Datafile}s
     * 
     * @param sampleID
     *
     * @return true if it has been run.
     */
    public boolean determineSampleRun(long sampleID) {
        startTransacion();
        
        String query = "select count(distinct datafile.uoid) from datafile where sampleID = " + sampleID + " ";
        try {
            return Integer.parseInt(this.session.createSQLQuery(query).list().get(0).toString()) > 0;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }

    /**
     * is a promt finished?. Checkes if each of the samples related to this experiment has been run.
     * <p>
     * added cache
     * </p>
     * <p>
     * updated to use {@link Datafile}s
     * </p>
     * 
     *
     * @param promt_id the promt id
     *
     * @return number of samples finished
     *
     * @todo replace it with ONE query
     */
    public int determineSamplesFinishedByPromtID(long promt_id) {
        return this.determineNumberOfSamplesFinished(promt_id);
    }

    /**
     * find the acqname for a sample
     *
     * @param sampleID the sampleID
     *
     * @return the acqnmae
     * 
     * @deprecated system not using ACQ names anymore
     *
     * @throws PersistenceActionFindException unable to find the acqname
     */
    public String findAcquisitionNameBySampleID(long sampleID)
    throws PersistenceActionFindException {
        // check on samples, that have been run 
        try {
            List list = this.findScannedPair(sampleID);

            if (list.size() > 0) {
                return ((String) list.get(0));
            }
        } catch (Exception e) {
            e.printStackTrace();
            // dont do anything
            debug("sample is not been scanned yet.");
        }

        this.startTransacion();

        String basic = "select parameter.value from " + AcquisitionParameter.class.getName() + " as parameter, " + Sample.class.getName() + " as sample, " + AcquisitionSample.class.getName() + " as acqsample, " + Event.class.getName() + " as event " + "WHERE parameter.label = 'name'  " +
        "AND acqsample = event.acquisitionsample " + "AND sample.id = '" + sampleID + "'" + "AND acqsample = parameter.acqsample " + "AND sample = event.sample ";

        List result = null;

        try {
            Query query = this.session.createQuery(basic);
            result = query.setCacheable(this.isCacheActive()).list();
            this.finishTransaction();
        } catch (Exception e) {
            //e.printStackTrace();
            this.finishTransaction();
            throw new PersistenceActionFindException("System problem: unable to find acqname for name " + sampleID);
        }

        if (result.size() != 1) {
            this.finishTransaction();
            throw new PersistenceActionFindException("unable to find acqname for name " + sampleID);
        } else {
            this.finishTransaction();
            return ((result.get(0) + ""));
        }
    }

    /**
     * TODO: 
     *
     * @param parentID TODO
     *
     * @return TODO
     */
    public List findChildActiveIDs(long parentID) {
        this.startTransacion();

        String basic = "select child.id from " + FormObject.class.getName() + " as parent, " + FormObject.class.getName() + " as child " + "where child.parent = parent " + " and child.active = 'true' and parent = '" + parentID + "'";

        Query query = this.session.createQuery(basic);
        List result = query.setCacheable(this.isCacheActive()).list();
        this.finishTransaction();

        return result;
    }

    /**
     * find all id of the child objects that are directly related to the parent object
     *
     * @param parentID the id th
     *
     * @return a list containing all childs related to a specific element
     */
    public List findChildsIDs(long parentID) {
        this.startTransacion();

        String basic = "select child.id from " + FormObject.class.getName() + " as parent, " + FormObject.class.getName() + " as child " + "where child.parent = parent " + "and parent = '" + parentID + "'";

        Query query = this.session.createQuery(basic);
        List result = query.setCacheable(this.isCacheActive()).list();
        this.finishTransaction();

        return result;
    }

    /**
     * TODO: 
     *
     * @param classIDlong TODO
     *
     * @return TODO
     */
    public String findClassInformationString(long classIDlong) {
        java.util.Iterator iterator1 = this.findLabelsforClazz(classIDlong).iterator();

        StringBuffer stringBuffer = new StringBuffer();

        while (iterator1.hasNext()) {
            Object[] element = (Object[]) iterator1.next();

            if (element[1].toString().compareTo("- unknown -") != 0) {
                stringBuffer.append("" + element[0].toString());
                stringBuffer.append(" " + element[1].toString());

                if (iterator1.hasNext()) {
                    stringBuffer.append("\n");
                }
            }
        }

        return stringBuffer.toString();
    }

    /**
     * find the clazz id for a sample
     *
     * @param sampleID the sampleid
     *
     * @return the clazz id
     *
     * @throws PersistenceActionFindException unable to find the class id
     */
    public int findClazzIDBySampleID(int sampleID) throws PersistenceActionFindException {
        return findParent(sampleID);
    }

    /**
     * find the clazz ids for a promt
     *
     * @param promt_ID the promt id
     *
     * @return the list of clazz ids
     */
    public List findClazzIDsByPromtID(final long promt_id) {
        this.startTransacion();

        String basic = "select clazz.id from " + Promt.class.getName() + " as promt, " + Page.class.getName() + " as page, " + Clazz.class.getName() + " as clazz, " + FormObject.class.getName() + " as child " + "where page.parent = promt and child.parent = page and clazz.parent = child and promt = '" +
        promt_id + "' and clazz.active = true  ";

        Query query = this.session.createQuery(basic);
        List result = query.setCacheable(this.isCacheActive()).list();
        this.finishTransaction();

        return result;
    }

    /**
     * find all clazzids that are related to the same promt
     *
     * @param clazzID
     *
     * @return
     *
     * @throws PersistenceActionFindException
     */
    public List findClazzesIDbyClazz(int clazzID) throws PersistenceActionFindException {
        int id = this.findParent(clazzID);

        return findChildsIDs(id);
    }

    /**
     * TODO: 
     *
     * @param clazzID TODO
     *
     * @return TODO
     *
     * @throws PersistenceActionFindException TODO
     */
    public List findDimensionIDsByClassID(long clazzID) throws PersistenceActionFindException {
        this.startTransacion();

        String basic = "select multiField4Clazzes.uoid as dimension " + "     from  " + "      formobject as multiField4Clazzes, " + "      formobject as clazz, " + "      formobject as unknown, " + "      multiField4ClazzesHibernate as x " + "      where clazz.uoid = x.multiField4Clazzs " +
        "      and x.elt = multiField4Clazzes.uoid " + "      and multiField4Clazzes.active = true " + "      and unknown.uoid = clazz.parent " + "      and clazz.uoid = " + clazzID + "      and 1 < (select count(*) from formobject where parent = multiField4Clazzes.parent ) ";

        try {
            List list = this.session.createSQLQuery(basic).addScalar("dimension", org.hibernate.Hibernate.LONG).setCacheable(this.isCacheActive()).list();
            this.finishTransaction();

            return list;
        } catch (Exception e) {
            throw new PersistenceActionFindException("System problem: unable to find by " + clazzID, e);
        }
    }

    /**
     * find labels for a specific clazz - these labels are taken from multifields4clazzes that were used to create this clazz.
     *
     * @return a list of Object[]s -  element[0] contains label - element[1] the value - both as String
     *
     * @throws Exception
     */
    public List findLabelsforClazz(long clazzID) {
        this.startTransacion();

        /*
       String basic = "select mf_label.question, mf_label.value " + " from formobject as label, " + " formobject as multiField4Clazzes, " + " formobject as mf_label, " + " formobject as sample, " +
         " formobject as clazz," + " multiField4ClazzesHibernate as x " + " where sample.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.Sample\"" + " and sample.parent = clazz.uoid " +
         " and label.PARENT = sample.uoid " + " and label.question = \"label\" " + " and clazz.uoid = x.multiField4Clazzs " + " and x.elt = multiField4Clazzes.uoid " +
         " and multiField4Clazzes.active = true " + " and mf_label.PARENT = multiField4Clazzes.uoid " + " and clazz.uoid = \"" + clazzID +
         "\" and mf_label.value != \"\" group by question order by mf_label.uoid";
         */
        String basic = "select mf_label.question, mf_label.value " +
        " from  formobject as multiField4Clazzes,     formobject as mf_label,      formobject as clazz,      formobject as mf_container,      multiField4ClazzesHibernate as x      where clazz.uoid = x.multiField4Clazzs      and x.elt = multiField4Clazzes.uoid      and multiField4Clazzes.active = true      and mf_label.PARENT = multiField4Clazzes.uoid      and clazz.uoid = \"" +
        clazzID + "\" and mf_container.uoid = mf_label.PARENT     and (select count(*) from formobject where parent = multiField4Clazzes.parent) > 1     and mf_label.value != \"\" order by clazz.uoid, mf_label.parent, mf_label.question"; //mf_label.value != \"--other--\" 

        List result = this.session.createSQLQuery(basic).addScalar("mf_label.question", org.hibernate.Hibernate.STRING).addScalar("mf_label.value", org.hibernate.Hibernate.STRING).setCacheable(this.isCacheActive()).list();

        this.finishTransaction();

        return result;
    }

    /**
     * TODO: 
     *
     * @param myID TODO
     *
     * @return TODO
     */
    public List findMultiField4ClazzIDsByPromtID(long myID) {
        List result = new Vector();

        // take all childobjects
        List list = new SXQuery().findChildsIDs(myID);
        Logger.debug(this, "childs: " + list.size());

        // check for type
        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            int id = (Integer.parseInt(iterator.next() + ""));

            try {
                FormObject.persistence_loadByID(MultiField4Clazz.class, id);

                if (new SXQuery().isActive(id)) {
                    Logger.debug(this, "found " + id + " is active.");
                    result.add("" + id);
                } else {
                    Logger.debug(this, "found " + id + " but not active.");
                }
            } catch (PersistenceActionFindException e) {
                // is not a MultiField4Clazz
                // Logger.debug(this, id + " is not a MultiField4Clazz");
                result.addAll(findMultiField4ClazzIDsByPromtID(id));
            }
        }

        Logger.debug(this, " total found " + result.size());

        return result;
    }

    /**
     * find the parentid for a formobject
     *
     * @param sampleID the object that the parent is looked for
     *
     * @return the parent id
     *
     * @throws PersistenceActionFindException unable to find the parent
     */
    public int findParent(int sampleID) throws PersistenceActionFindException {
        return findParent(new Long(sampleID).longValue());
    }

    /**
     * find the parentid for a formobject
     *
     * @param sampleID the object that the parent is looked for
     *
     * @return the parent id
     *
     * @throws PersistenceActionFindException unable to find the parent
     */
    public int findParent(long childID) throws PersistenceActionFindException {
        this.startTransacion();

        String basic = "select parent.id from " + FormObject.class.getName() + " as parent, " + FormObject.class.getName() + " as child " + "where child.parent = parent " + "and child = '" + childID + "'";

        Query query = this.session.createQuery(basic);
        List result = query.setCacheable(this.isCacheActive()).setCacheable(this.isCacheActive()).list();
        this.finishTransaction();

        if (result.size() != 1) {
            throw new PersistenceActionFindException("unable to find parent id for " + childID + " - number of results: " + result.size());
        } else {
            return (Integer.parseInt(result.get(0) + ""));
        }
    }

    /**
     * find the title for a promt
     *
     * @param promtID
     *
     * @return description
     *
     * @throws PersistenceActionFindException
     */
    public String findPromtAbstractByPromtID(long promtID)
    throws PersistenceActionFindException {
        try {
            return findInputfieldFirstLevel(promtID, "abstract");
        } catch (PersistenceActionFindException e) {
            return "-unknown-";
        }
    }

    /**
     * TODO: 
     *
     * @param promtID TODO
     *
     * @return TODO
     */
    public String findPromtClassesString(long promtID) {
        Iterator iterator = this.findClazzIDsByPromtID(promtID).iterator();
        StringBuffer buffer = new StringBuffer();

        while (iterator.hasNext()) {
            long element = Long.parseLong("" + iterator.next());
            buffer.append("Class " + element + ":\n" + findClassInformationString(element));

            if (iterator.hasNext()) {
                buffer.append("\n");
            }
        }

        return buffer.toString();
    }

    /**
     * TODO: 
     *
     * @param promtID TODO
     *
     * @return TODO
     *
     * @throws PersistenceActionFindException TODO
     */
    public HashSet findPromtCollaboration(long promtID) throws PersistenceActionFindException {
        
        /*
         * deactivated
        this.startTransacion();

        HashSet otherUserHashset = this.findPromtCollaboration(promtID,this.session);

        this.finishTransaction();
        
        return otherUserHashset;

         * deactivated
         */
        if (false) throw new PersistenceActionFindException("");
        
        return new HashSet();

    }


    /**
     * TODO: 
     *
     * @param promtID TODO
     *
     * @return TODO
     *
     * @throws PersistenceActionFindException TODO
     */
    public HashSet findPromtCollaboration(long promtID, Session sess) throws PersistenceActionFindException {
        this.session = sess;
        
        this.startTransacion();

        HashSet otherUserHashset = new HashSet();
        Iterator useriterator = new SXQuery(this.session).findPromtUserAccessRightForPromtID(promtID).iterator();

        while (useriterator.hasNext()) {
            PromtUserAccessRight accessRight2 = (PromtUserAccessRight) useriterator.next();

            try {
                UserDO _u = (UserDO) UserLocal.persistence_loadByID(UserLocal.class, this.session, accessRight2.getUserID());

                // filter system users out
                if (!_u.isLabTechnician() && !_u.isMasterUser(_u.getUOID()) && !_u.isPublic()) {
                    otherUserHashset.add(_u);
                }
            } catch (org.hibernate.type.SerializationException e) {
                warning(e);
            }
        }

        return otherUserHashset;
    }

    
    
    
    /**
     * TODO: 
     *
     * @param promtID TODO
     *
     * @return TODO
     *
     * @throws PersistenceActionFindException TODO
     */
    public String findPromtCollaborationHTML(final long promtID)
    throws PersistenceActionFindException {
        
        HashSet hashSet = this.findPromtCollaboration(promtID);
        Iterator iterator = hashSet.iterator();
        StringBuffer buffer = new StringBuffer();

        while (iterator.hasNext()) {
            UserDO element = (UserDO) iterator.next();
            buffer.append("<a href='mailto:" + element.getMailAddress().getEmailAddress() + "'>" + element.getDisplayName() + "</a>");

            if (iterator.hasNext()) {
                buffer.append("<br>");
            }
        }

        return buffer.toString();
    }

    /**
     * TODO: 
     *
     * @param promtID TODO
     *
     * @return TODO
     *
     * @throws PersistenceActionFindException TODO
     */
    public String findPromtCollaborationString(final long promtID)
    throws PersistenceActionFindException {
        this.startTransacion();

        HashSet hashSet = this.findPromtCollaboration(promtID);
        Iterator iterator = hashSet.iterator();
        StringBuffer buffer = new StringBuffer();

        while (iterator.hasNext()) {
            UserDO element = (UserDO) iterator.next();
            buffer.append(element.getDisplayName() + "\n");
        }

        return buffer.toString();
    }

    /**
     * TODO: 
     *
     * @param promtID TODO
     *
     * @return TODO
     */
    public String findPromtComment(long promtID) {
        startTransacion();

        String comment = (String) session.createSQLQuery("select element3.value as comment from formobject as promt, formobject as page, " + "formobject as element3 where promt.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.Promt\" " +
                "and page.parent = promt.uoid and element3.parent = page.uoid and element3.question = \"comment\" and " + "element3.discriminator =\"org.setupx.repository.web.forms.inputfield.BigStringInputField\" " + "and promt.uoid = \"" + promtID + "\"").addScalar("comment", org.hibernate.Hibernate.STRING)
                .setCacheable(this.isCacheActive()).list().get(0);

        //comment = Util.replace(comment,'\n',"<br>");
        return comment;        
    }

    /**
     * Find the PromtID for a clazz ID
     *
     * @param clazzID the clazzid
     *
     * @return the promt ID
     *
     * @throws PersistenceActionFindException unable to find the experiment
     */
    public int findPromtIDbyClazz(int clazzID) throws PersistenceActionFindException {
        this.startTransacion();

        String basic = "select promt.id from " + Promt.class.getName() + " as promt, " + FormObject.class.getName() + " as child3, " + FormObject.class.getName() + " as child2, " + FormObject.class.getName() + " as child1  " + "where child1.parent = promt " + "and child2.parent = child1 " +
        "and child3.parent = child2 " + "and child3 = '" + clazzID + "'";

        Query query = this.session.createQuery(basic);
        List result = query.setCacheable(this.isCacheActive()).setCacheable(this.isCacheActive()).list();
        this.finishTransaction();

        if (result.size() != 1) {
            throw new PersistenceActionFindException("unable to find promt id for " + clazzID);
        } else {
            return (Integer.parseInt(result.get(0) + ""));
        }
    }

    /**
     * Find the PromtID for a sample ID
     *
     * @param i the sample ID
     *
     * @return the promt ID
     *
     * @throws PersistenceActionFindException unable to find the experiment
     */
    public int findPromtIDbySample(int i) throws PersistenceActionFindException {
        return findPromtIDbySample(new Long(i).longValue());
    }

    /**
     * Find the PromtID for a sample ID
     *
     * @param i the sample ID
     *
     * @return the promt ID
     *
     * @throws PersistenceActionFindException unable to find the experiment
     */
    public int findPromtIDbySample(long i) throws PersistenceActionFindException {
        try {
            return (int)root(i + "");
        } catch (Exception e) {
            throw new PersistenceActionFindException("unable to find promt id for " + i, e);
        }
    }

    /**
     * find all promtIDs for a species.
     * 
     * <p>
     * checks all promts if they contain an species with the given speciesID
     * </p>
     *
     * @param ncbiSpeciesID
     *
     * @return
     */
    public List findPromtIDbySpecies(String speciesName) {
        String query = "select promt.uoid as uoid from formobject as promt, formobject as page, formobject as species, formobject as ncbispecies, formobject as label where " + "label.value = \"" + speciesName +
        "\" and label.parent = ncbispecies.uoid and ncbispecies.parent = species.uoid and species.parent = page.uoid and page.parent = promt.uoid";

        startTransacion();

        List list = session.createSQLQuery(query).addScalar("uoid", org.hibernate.Hibernate.LONG).setCacheable(this.isCacheActive()).setCacheable(this.isCacheActive()).list();

        this.finishTransaction();

        return list;
    }

    /**
     * uses the ncbiconnector to get the real name
     *
     * @param ncbiSpeciesID
     *
     * @return
     *
     * @throws NCBIException
     *
     * @see #findPromtIDbySpecies(int)
     */
    public List findPromtIDbySpecies(int ncbiSpeciesID) throws NCBIException {
        String ncbiSpeciesName;

        try {
            ncbiSpeciesName = NCBIConnector.determineNCBI_Information(ncbiSpeciesID).getName();
        } catch (NCBIException e) {
            e.printStackTrace();
            throw e;
        }

        return findPromtIDbySpecies(ncbiSpeciesName);
    }

    /**
     * search all promts in this system
     *
     * @return a list with all promts
     */
    public List findPromtIDs() {
        this.startTransacion();

        String basic = "select id from " + Promt.class.getName() + " order by uoid desc";

        Query query = this.session.createQuery(basic);
        List result = query.setCacheable(this.isCacheActive()).setCacheable(this.isCacheActive()).list();
        this.finishTransaction();

        return result;
    }

    /**
     * TODO: 
     *
     * @param promtID TODO
     *
     * @return TODO
     */
    public String findPromtStructure(final long promtID) {
        if (promtID > 0) {
            String query = "select mf_label.question as l1, mf_container.question as l2, mf_label.value as l3" + " from  formobject as multiField4Clazzes, " + " formobject as promt, formobject as page, formobject  as clazz, formobject  as child, formobject as mf_label,  " +
            " formobject as mf_container,      multiField4ClazzesHibernate as x      where clazz.uoid = x.multiField4Clazzs      and x.elt = multiField4Clazzes.uoid      and multiField4Clazzes.active = true      and mf_label.PARENT = multiField4Clazzes.uoid      and mf_container.uoid = mf_label.PARENT     and (select count(*) from formobject where parent = multiField4Clazzes.parent) > 1  " +
            "  and mf_label.value != \"\"  " + " and mf_label.value != \"--other--\"  " + " and page.parent = promt.uoid and child.parent = page.uoid and clazz.parent = child.uoid and promt.uoid = " + promtID + " GROUP by mf_container.uoid order by mf_container.uoid";

            startTransacion();

            List list = session.createSQLQuery(query).addScalar("l1", Hibernate.STRING).addScalar("l2", Hibernate.STRING).addScalar("l3", Hibernate.STRING).setCacheable(this.isCacheActive()).setCacheable(this.isCacheActive()).list();

            Hashtable mainValues = new Hashtable();
            Iterator listIterator = list.iterator();

            while (listIterator.hasNext()) {
                Object[] strings = (Object[]) listIterator.next();

                String l1 = "" + strings[0];
                String l2 = "" + strings[1];
                String value = "" + strings[2];

                if (mainValues.containsKey(l2)) {
                    int count = Integer.parseInt(mainValues.get(l2) + "");
                    count++;
                    mainValues.put(l2, count + "");
                } else {
                    mainValues.put(l2, 1 + "");
                }
            }

            // create a string contain RELEVANT information
            StringBuffer buffer = new StringBuffer();
            Enumeration keyEnumeration = mainValues.keys();

            while (keyEnumeration.hasMoreElements()) {
                String __keyValue = "" + keyEnumeration.nextElement();
                String __countValue = "" + mainValues.get(__keyValue);

                buffer.append(__countValue + " different " + __keyValue);

                if (keyEnumeration.hasMoreElements()) {
                    buffer.append(" and ");
                }
            }

            return buffer.toString();
        } else {
            return "";
        }
    }

    /**
     * find the abstract for a promt
     *
     * @param promtID
     *
     * @return description
     *
     * @throws PersistenceActionFindException
     */
    public String findPromtTitleByPromtID(long promtID) throws PersistenceActionFindException {
        try {
            return findInputfieldFirstLevel(promtID, "Title");
        } catch (PersistenceActionFindException e) {
            return "-unknown-";
        }
    }

    /**
     * TODO: 
     *
     * @param promtID TODO
     *
     * @return TODO
     */
    public List findPromtUserAccessRightForPromtID(long promtID) {
        // list of accessrights that the are related to the user
        this.startTransacion();

        List list = session.createCriteria(PromtUserAccessRight.class).add(Expression.eq("experimentID", new Long(promtID))).addOrder(Order.desc("accessCode")).addOrder(Order.desc("experimentID")).setCacheable(this.isCacheActive()).setCacheable(this.isCacheActive()).list();

        this.finishTransaction();

        return list;
    }

    /**
     * find all Promt IDs that the user has right to
     *
     * @param userID the userid that the PromtUserAccessRights are related to
     *
     * @return a list of PromtUserAccessRights
     */
    public List findPromtUserAccessRightForUserID(long userID) {
        // list of accessrights that the are related to the user
        this.startTransacion();

        List list = session.createCriteria(PromtUserAccessRight.class).add(Expression.eq("userID", new Long(userID))).addOrder(Order.desc("accessCode")).addOrder(Order.desc("experimentID")).setCacheable(this.isCacheActive()).setCacheable(this.isCacheActive()).list();

        this.finishTransaction();

        return list;
    }

    /**
     * find the accessright for a user for a specific user and experiment combination.
     *
     * @param userID the users id
     * @param promtID the experiment id
     *
     * @return the accessright for this specific combination
     *
     * @throws PersistenceActionFindException
     */
    public PromtUserAccessRight findPromtUserAccessRightForUserID(long userID, long promtID)
    throws PersistenceActionFindException {
        // list of accessrights that the are related to the user
        this.startTransacion();

        Criteria criteria = session.createCriteria(PromtUserAccessRight.class);
        criteria.add(Expression.eq("userID", new Long(userID)));
        criteria.add(Expression.eq("experimentID", new Long(promtID)));

        List list = criteria.setCacheable(this.isCacheActive()).setCacheable(this.isCacheActive()).list();

        this.finishTransaction();

        if (list.size() == 0) {
            throw new PersistenceActionFindException("no access rights for user: " + userID + " on experiment: " + promtID);
        }

        return (PromtUserAccessRight) list.get(0);
    }

    /**
     * TODO: 
     *
     * @param status TODO
     *
     * @return TODO
     *
     * @throws PersistenceActionFindException TODO
     *
     * @deprecated
     */
    public List findSample(int status) throws PersistenceActionFindException {
        this.startTransacion();

        List result = null;

        switch (status) {
        case Sample.STATUS_NOT_RUN:
            // looking for samples that have not been run
            //List list = org.setupx.repository.web.forms.Sample.
            warning(this, "QUERY HAS TO BE OPTIMIZED");
            result = session.createCriteria(Sample.class).setCacheable(this.isCacheActive()).setCacheable(this.isCacheActive()).list();

            break;

        default:
            throw new PersistenceActionFindException("unable to find samples - status " + status + " is unknown.");
        }

        this.finishTransaction();

        return result;
    }

    /**
     * Find the Sample ID for an acquisition name. Checking the existing acqustionssamples for a specific acquistion name.   In case the acquistion is not found the acquistion name is looked up by using the filebased version of this method.
     *
     * @param name the acquistionname
     *
     * @return the sample id
     *
     * @throws PersistenceActionFindException TODO
     *
     * @see SXQuery#findSampleIDByAcquisitionNameFILEBASED(String)
     */
    public final int findSampleIDByAcquisitionName(String name)
    throws PersistenceActionFindException {
        try {
            List list = new SXQuery().findScannedPair(name);

            if (list.size() > 0) {
                return (Integer.parseInt("" + list.get(0)));
            }
        } catch (Exception e) {
            // dont do anything
        }

        try {
            warning(this, "unable to find sampleID (findSampleIDByAcquisitionName) by name " + name + " . Going for filebased aproach now.");
            return findSampleIDByAcquisitionNameFILEBASED(name);
        } catch (AcquisitionFileException e) {
            throw new PersistenceActionFindException(e);
        }
    }

    /**
     * TODO: 
     *
     * @param label TODO
     *
     * @return TODO
     *
     * @throws AcquisitionFileException TODO
     */
    public int findSampleIDByAcquisitionNameFILEBASED(String label)
    throws AcquisitionFileException {
        return LecoACQFile.determineID(label);
    }

    /**
     * TODO: 
     *
     * @param clazzID TODO
     *
     * @return TODO
     */
    public List findSampleIDsByClazzID(int clazzID) {
        return findChildsIDs(clazzID);
    }

    /**
     * find sample ids for a promt
     */
    public List findSampleIDsByPromtID(final long promt_id) {
        this.startTransacion();

        String basic = "select sample.id from " + Promt.class.getName() + " as promt, " + Page.class.getName() + " as page, " + Clazz.class.getName() + " as clazz, " + Sample.class.getName() + " as sample, " + FormObject.class.getName() + " as child " + "where page.parent = promt " +
        "and child.parent = page " + "and clazz.parent = child " + "and sample.parent = clazz " + "and promt = '" + promt_id + "' and sample.active = true and clazz.active = true";

        Query query = this.session.createQuery(basic);
        List result = query.setCacheable(this.isCacheActive()).setCacheable(this.isCacheActive()).list();
        this.finishTransaction();

        return result;
    }

    /**
     * find the sample Label for a specific sample
     *
     * @param sampleID the samples uoid
     *
     * @return the label
     *
     * @throws PersistenceActionFindException TODO
     */
    public String findSampleLabelBySampleID(long sampleID)
    throws PersistenceActionFindException {
        this.startTransacion();

        String basic = "select child.value from " + FormObject.class.getName() + " as child, " + Sample.class.getName() + " as sample " + "where child.question = 'comment' " + "and child.parent = sample " + "and sample = '" + sampleID + "'  ";

        Query query = this.session.createQuery(basic);
        List result = query.setCacheable(this.isCacheActive()).list();
        this.finishTransaction();

        if (result.size() != 1) {
            throw new PersistenceActionFindException("unable to find the label for sample " + sampleID);
        }

        return (String) result.get(0);
    }

    /**
     * select all acqnames that were successfully run for a certain sample id
     *
     * @param sampleID the sample id
     * @deprecated
     * @return a list of acqnames - type: String
     */
    public List findScannedPair(long sampleID) {
        this.startTransacion();
 
        Logger.err(this,"findScannedPair(long sampleID) is not supported anymore - this is only to support Export functionality on GC");
        String techQ_part = "";
/*
        for (int i = 0; i < SampleFile.TYPES.length; i++){
            techQ_part = techQ_part.concat("t.pattern like \"%" + SampleFile.getExtension(SampleFile.TYPES[i]) + "%\" ");

            // in between them
            if (i != SampleFile.TYPES.length -1){
                techQ_part = techQ_part.concat(" OR ");
            }
        }
*/        
        String query = 
            " select " +
            " SUBSTRING_INDEX(SUBSTRING_INDEX(d.source, \".\", 1), \"/\", -1) as XX " +
            " from datafile as d " +
//            " join technologyfiletype as t " +
//            " on t.uoid = d.technologyfiletype " +
//            " and (" + techQ_part + ") " +
            " where d.sampleID = " + sampleID + " " +
            " group by SUBSTRING_INDEX(SUBSTRING_INDEX(d.source, \".\", 1), \"/\", -1) " +
            " order by SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(d.source, \".\", 1), \"/\", -1),\"_\",-1) desc";

        log(this, "query:" + query);
        
        try {
            List l = session.createSQLQuery(query).addScalar("XX", Hibernate.STRING).list();
            return l;
    
        }catch (Exception e) {
            Logger.err(this, "unable to determine any runs for "  + sampleID);
            return new Vector();
        }
        
        

        

        /*
        this.startTransacion();

        // check the table containing all scanned samples
        // select * from scanned_samples where sampleID = "6240"
        // modified - that alway the sample with the highest last digest is beeing used.
        String basic = "select pair.label from " + ScannedPair.class.getName() + " as pair where pair.sampleID = '" + sampleID + "' order by pair.label DESC";
        Query query = this.session.createQuery(basic);
        List list = query.setCacheable(this.isCacheActive()).list();

        this.finishTransaction();

        return list;
        */
    }

    
    
    public List findDatafilesNames(long sampleID) throws PersistenceActionFindException {
        List datafiles = this.findDatafiles(sampleID);
        List set = new Vector();
        Iterator iterator = datafiles.iterator();
        while (iterator.hasNext()) {
            Datafile datafile = (Datafile) iterator.next();
            set.add(datafile.getSourceFile().getName());
        }
        return set;
    }

    public List findDatafiles(long sampleID) throws PersistenceActionFindException {
        return TechnologyProvider.getDatafiles(sampleID);
    }

    /**
     * select all ids that were successfully run for a certain sample names
     *
     * @param sampleID the sample id
     *
     * @return a list of acqnames - type: String
     * @deprecated
     */
    public List findScannedPair(String sampleName) {

        return findSamplesIDByFilename(sampleName);
        
        /*
        this.startTransacion();

        // check the table containing all scanned samples
        // select * from scanned_samples where sampleID = "6240"
        // modified - that alway the sample with the highest last digest is beeing used.
        String basic = "select pair.sampleID  from " + ScannedPair.class.getName() + " as pair where pair.label like '" + sampleName + "%' and pair.sampleID != '' order by pair.label DESC";
        Query query = this.session.createQuery(basic);
        List list = query.setCacheable(this.isCacheActive()).list();

        this.finishTransaction();

        return list;
        */

    }

    public List findSamplesIDByFilename(String sampleName) {
        return TechnologyProvider.determineSampleIDs(sampleName);
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public List findSpecies() {
        String query = "select label.value as value from formobject as promt, formobject as page, formobject as species, formobject as ncbispecies, formobject as label " + "where label.parent = ncbispecies.uoid " +
        "and ncbispecies.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\"  " + "and ncbispecies.parent = species.uoid and species.parent = page.uoid and page.parent = promt.uoid and label.value != \"\"  ";

        startTransacion();

        List list = session.createSQLQuery(query).addScalar("value", org.hibernate.Hibernate.STRING).setCacheable(this.isCacheActive()).list();

        this.finishTransaction();

        return list;
    }

    /**
     * TODO: 
     *
     * @param promtID TODO
     *
     * @return TODO
     * 
     * 
     * ERROR !!!! CHECK IT !!! CHECK IT !!! CHECK IT !!! CHECK IT !!! CHECK IT !!! CHECK IT 
     */
    public List findSpeciesbyPromtID(long promtID) {
        String query = "select label.value as value from formobject as promt, formobject as page, formobject as species, formobject as ncbispecies, formobject as label " + "where label.parent = ncbispecies.uoid " +
        "and ncbispecies.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfield\"  " + "and ncbispecies.parent = species.uoid and species.parent = page.uoid and page.parent = promt.uoid and label.value != \"\"  " + "and promt.uoid = " + promtID;

        startTransacion();

        List list = session.createSQLQuery(query).addScalar("value", org.hibernate.Hibernate.STRING).setCacheable(this.isCacheActive()).list();

        this.finishTransaction();

        return list;
    }

    /**
     * TODO: 
     *
     * @param clazzID TODO
     *
     * @return TODO
     *
     * @throws PersistenceActionFindException TODO
     */
    public List findVariationsIDsByClassID(long clazzID)
    throws PersistenceActionFindException {
        this.startTransacion();

        String basic = " select  mf_label.uoid as variation, multiField4Clazzes.uoid as dimension " + " from " + " formobject as multiField4Clazzes, " + " formobject as mf_label, " + " formobject as clazz, " + " formobject as unknown, " + " multiField4ClazzesHibernate as x " +
        " where clazz.uoid = x.multiField4Clazzs " + " and x.elt = multiField4Clazzes.uoid " + " and multiField4Clazzes.active = true " + " and mf_label.PARENT = multiField4Clazzes.uoid " + " and unknown.uoid = clazz.parent " + " and mf_label.value != \"\" " + " and clazz.uoid = " + clazzID +
        " and 1 < (select count(*) from formobject where parent = multiField4Clazzes.parent ) " + " order by mf_label.uoid ";

        try {
            List list = this.session.createSQLQuery(basic).addScalar("variation", org.hibernate.Hibernate.LONG).addScalar("dimension", org.hibernate.Hibernate.LONG).setCacheable(this.isCacheActive()).list();
            this.finishTransaction();

            return list;
        } catch (Exception e) {
            throw new PersistenceActionFindException("System problem: unable to find by " + clazzID, e);
        }

    }

    /**
     * TODO: 
     *
     * @param promtID TODO
     * @param additionlComment TODO
     */
    public void updateComment(long promtID, final String additionlComment) {
        this.startTransacion();

        // get the original comment
        final String orig_comment = this.findPromtComment(promtID);
        final String new_comment = orig_comment.concat("\n").concat(additionlComment);

        // set the value
        long uoid = Long.parseLong("" +
                session.createSQLQuery("select element3.uoid as id from formobject as promt, formobject as page, " + "formobject as element3 where promt.discriminator = \"org.setupx.repository.web.forms.inputfield.multi.Promt\" " +
                        "and page.parent = promt.uoid and element3.parent = page.uoid and element3.question = \"comment\" and " + "element3.discriminator =\"org.setupx.repository.web.forms.inputfield.BigStringInputField\" " + "and promt.uoid = \"" + promtID + "\"").addScalar("id", org.hibernate.Hibernate.LONG)
                        .setCacheable(this.isCacheActive()).list().get(0));

        this.updateField(uoid, new_comment);

        this.finishTransaction();
    }

    /**
     * TODO: 
     *
     * @param promtID TODO
     * @param additionlComment TODO
     * @param userDO TODO
     */
    public void updateComment(long promtID, final String additionlComment, UserDO userDO) {
        this.updateComment(promtID, userDO.getDisplayName() + ": " + additionlComment + " " + java.text.DateFormat.getDateInstance(java.text.DateFormat.MEDIUM, java.util.Locale.US).format(new java.util.Date()));
    }

    /**
     * TODO: 
     *
     * @param uoid TODO
     * @param new_value TODO
     */
    public void updateField(final long uoid, final String new_value) {
        this.startTransacion();

        String hqlUpdate = "update BigStringInputField c set c.value = :value where c.id = :uoid";

        int updatedEntities = session.createQuery(hqlUpdate).setString("value", new_value).setString("uoid", "" + uoid).executeUpdate();
    }

    /**
     * TODO: 
     *
     * @param promtID TODO
     * @param question TODO
     *
     * @return TODO
     *
     * @throws PersistenceActionFindException TODO
     */
    public String findInputfieldFirstLevel(long promtID, String question)
    throws PersistenceActionFindException {
        this.startTransacion();

        String basic = "select element.value from " + Promt.class.getName() + " as promt, " + FormObject.class.getName() + " as page, " + FormObject.class.getName() + " as element " + " where page.parent = promt " + " and element.parent = page " + " and element.question = '" + question + "'" +
        " and promt = '" + promtID + "'";

        Query query = this.session.createQuery(basic);
        List result = query.setCacheable(this.isCacheActive()).list();
        this.finishTransaction();

        if (result.size() != 1) {
            throw new PersistenceActionFindException("unable to find the title");
        }

        return (String) result.get(0);
    }


    private List findElementsByQuestion(String question) {
        this.startTransacion();
        String basic = "select uoid from formobject where question like \"" + question + "\" order by uoid desc";
        List list = this.session.createSQLQuery(basic).addScalar("uoid", org.hibernate.Hibernate.LONG).setCacheable(this.isCacheActive()).list();
        return list;
    }

    private String findValueByID(long elementID) {
        this.startTransacion();
        String basic = "select value from formobject where uoid = " + elementID;
        List list = this.session.createSQLQuery(basic).addScalar("value", org.hibernate.Hibernate.STRING).setCacheable(this.isCacheActive()).list();
        return "" + list.get(0);

    }

    /**@deprecated replaced by SQL root()*/
    private long findRoot(long elementID) {
        return root(elementID + "");
    }

    /**
     * close the session.
     */
    private final void finishTransaction() {
        if (transaction != null) {
            this.transaction.commit();
        }

        this.session.close();
    }

    /**
     * check Session and in case it is closed or not existing, create a clean open session
     */
    private final void startTransacion() {
        log("-----------------------------------");
        log("starting transaction.");
        
        log("status:  session: "  + session);
        log("status:  sessionfact: "  + sessionFactory);
        
        if (session == null || (!session.isOpen()) ) {
            log("creating new  session");
            sessionFactory = PersistenceConfiguration.createSessionFactory();
            this.session = sessionFactory.openSession();
        } else if ((sessionFactory == null) || sessionFactory.isClosed()) {
            log("creating new sessionfactory and session");
            sessionFactory = PersistenceConfiguration.createSessionFactory();
            this.session = sessionFactory.openSession();
        }
        
        if (this.log){
            try {
                log("session factory: " + this.sessionFactory.toString());
                log("session factory is closed: " + this.sessionFactory.isClosed());
                log("session factory created: " + new Date(this.sessionFactory.getStatistics().getStartTime()).toLocaleString());

                log("session : " + this.session);
                log("session is open: " + this.session.isOpen());
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
        
        this.transaction = this.session.beginTransaction();
        log("transaction started.");
        log("-----------------------------------");
    }


    /**
     * finds values for a specific question within an promt. The position in the promt is irrelevant.
     * 
     * @param question
     * @param promtID
     * @return
     */
    public List findQuestionValue(String question, final long promtID){

        this.startTransacion();

        List result = new ArrayList();
        for (int i = 1; i < 10; i++){

            StringBuffer stringBuffer = new StringBuffer();

            // find the id of the attribute            
            // creating the sqlstring
            stringBuffer.append("select f" + i +".uoid as id from ");
            for (int j = 0; j < i; j++){
                stringBuffer.append("formobject as f" + j + ", ");
            }

            stringBuffer.append("formobject as f" + i + " ");

            stringBuffer.append("where ");

            stringBuffer.append(" f" + i + ".question = \"" + question + "\" ");
            stringBuffer.append("and f0.uoid = " + promtID);

            for (int j = 1; j <= i; j++){
                stringBuffer.append(" and " );
                stringBuffer.append(" f" + (j-1) + ".uoid = f" + (j) + ".parent ");
            }

            List tmp = this.session.createSQLQuery(stringBuffer.toString()).addScalar("id", Hibernate.LONG).setCacheable(this.isCacheActive()).list();


            debug("number of elements at depth: " + tmp.size());


            // in case we got something - get the value and check child elements for values
            if (tmp.size() > 0){
                Iterator idIterator = tmp.iterator();
                while (idIterator.hasNext()) {

                    // thats the id that was found
                    long id = Long.parseLong("" + idIterator.next());

                    // get the values for itself 
                    result.add(this.session.createSQLQuery("select value from formobject where uoid = " + id).addScalar("value", Hibernate.STRING).setCacheable(this.isCacheActive()).list());


                    // get childvalues
                    result.add(this.session.createSQLQuery("select fieldchild.value as value from formobject as field, "
                            + " formobject as fieldchild where fieldchild.parent = field.uoid "
                            + " and field.active = true and field.uoid = " + id
                            + " order by fieldchild.uoid desc").addScalar("value", Hibernate.STRING).setCacheable(this.isCacheActive()).list());

                }
            }
        }
        return result;
    }



    public String findQuestionValueString(String questions, long promtID){ 
        List list = new SXQuery().findQuestionValue(questions, promtID);
        Iterator values = list.iterator();
        if (list.size() > 0){
            StringBuffer buffer = new StringBuffer();

            while(values.hasNext()){
                //String value = "" + values.next();
                Iterator valuesIterator = ((List)values.next()).iterator();
                if (valuesIterator.hasNext()){
                    String v = "" + valuesIterator.next();
                    if (v.length() > 0){
                        buffer.append(v + " ");
                    }
                }
            }
            return buffer.toString();
        }
        return "";
    }


    /**
     * find root element of any formobject - meaning the the parent, or parent-parent, or ... node that has null as a parent value.
     * @param anyFormobjectID
     * @return
     */ 
    public long root(String anyFormobjectID) {
        this.startTransacion();
        // change:
        // using MySQL internal root(int) function
        String basic = "select root(uoid) as root from formobject where uoid = " + anyFormobjectID;
        List result = this.session.createSQLQuery(basic).addScalar("root", Hibernate.LONG).setCacheable(this.isCacheActive()).list();

        //this.finishTransaction();
        return Long.parseLong(result.get(0).toString());
    }


    /**
     * todo - needs to be changed to a real datatype {@link AttributeValuePair} is missuesd.
     * @param sampleID
     * @param attributeNames
     * @return
     */
    public AttributeValuePair[] findPubdataBySampleID(long sampleID, List attributeNames) {
        Vector v = new Vector();
        this.startTransacion();
        // NEW WAY


        String valueQuery = "select att.value as v, att.label as l from pubattribute as att , pubsample as s where s.setupXsampleID = " + sampleID + " and s.uoid = att.parent";
        List values = session.createSQLQuery(valueQuery).addScalar("v", Hibernate.STRING).addScalar("l", Hibernate.STRING).setCacheable(this.isCacheActive()).list();

        // sort the values
        Iterator xaf = attributeNames.iterator();
        while(xaf.hasNext()){
            AttributeValuePair attributeValuePair = new AttributeValuePair();

            String attName = xaf.next().toString();
            attributeValuePair.setLabel1(attName);

            // loop and get the correct value
            Iterator valueIterator = values.iterator();
            while(valueIterator.hasNext()){
                Object[] objects = (Object[])valueIterator.next();
                if (objects[1].toString().compareTo(attName) == 0){ 
                    attributeValuePair.setValue("" + objects[0]);
                }
            }
            v.add(attributeValuePair);
        }

        AttributeValuePair[] results = new AttributeValuePair[v.size()];
        Iterator vIter = v.iterator();
        int pos = 0;
        while (vIter.hasNext()) {
            AttributeValuePair elem = (AttributeValuePair) vIter.next();
            if (elem != null){
                results[pos] = elem;
                pos++;
            }
        }

        this.finishTransaction();
        return results;
    }
    
    
    
    
    public List findComoundNamesBySpecies(int speciesID){
        
        Iterator ids;
        try {
            ids = new SXQuery().findPromtIDbySpecies(speciesID).iterator();
        } catch (NCBIException e) {
            Logger.err(this, "unable to find ids");
            return null;
            
        }

        StringBuffer query = new StringBuffer("select a.label as label from pubsample as sample, pubdata as d, pubattribute as a ");
        query.append("where d.uoid = sample.parent and a.parent = sample.uoid ");
        boolean idsAvaialable = false;
        if (ids.hasNext()) {
            idsAvaialable = true; 
            query.append("and (");
        }

        if (! ids.hasNext()) {
            query.append("and d.relatedExperimentID = \"0\" " );
        }

        while(ids.hasNext()){
            query.append("d.relatedExperimentID = " + ids.next() + " " );
            if (ids.hasNext()) query.append(" or ");    
        }

        if (idsAvaialable){
            query.append(") ");
        }
        query.append(" group by a.label order by a.label");

        Logger.debug(this, "query: " + query.toString());

        Session ses = CoreObject.createSession();
        List lX=  ses.createSQLQuery(query.toString()).addScalar("label", Hibernate.STRING).setCacheable(this.isCacheActive()).list();
        
        return lX;
    }

    public boolean isCacheActive() {
        return cacheActive;
    }

    public void setCacheActive(boolean cacheActive) {
        this.cacheActive = cacheActive;
    }
    
    
    
    public int determineNumberOfSamples(long promtID){        
        String query = "select countsamples(" + promtID + ")";
        try {
            return Integer.parseInt(this.session.createSQLQuery(query).list().get(0).toString());
        } catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * number of {@link Sample} that finished on ANY {@link Technology}
     * 
     * @param promtID
     * @return
     */
    public int determineNumberOfSamplesFinished(long promtID) {
        this.startTransacion();
        // checking the cache
        try {
            return SXQueryCache.getNumberOfFinishedSamplesByExperiment(promtID);
        } catch (SXQueryNonCachedException e) {
            Logger.debug(this, "no cached object");
        }

        try {
            String queryXX = "select countsamplesfinished(" + promtID + ")";
            int num = Integer.parseInt(this.session.createSQLQuery(queryXX).list().get(0) + "");
            new SXQueryCacheObject(promtID, num);
            return num;
        } catch (Exception e) {
            Logger.log(this, "unable to determine number of samples");
            // e.printStackTrace();
            return 0;
        }
    }
    
    /**
     * number of {@link Sample} of a specific {@link Promt} that finished on this specific {@link Technology}
     * 
     * @param promtID
     * @param technology
     * @return
     */
    public int determineNumberOfSamplesFinished(long promtID, Technology technology){
        this.startTransacion();
        String query = "select countsamplesfinishedTechnology(" + promtID + "," + technology.getUOID() + ")";
        try {
            return Integer.parseInt(this.session.createSQLQuery(query).list().get(0).toString());
        } catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }
}
