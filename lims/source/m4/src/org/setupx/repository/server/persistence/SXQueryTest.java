package org.setupx.repository.server.persistence;

import java.util.Iterator;
import java.util.List;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.technology.platform.Technology;
import org.setupx.repository.core.communication.technology.platform.TechnologyProvider;

import junit.framework.TestCase;

public class SXQueryTest extends TestCase {
    
    private List promtIDs;

    public SXQueryTest() {
    }
    
    protected void setUp() throws Exception {
        super.setUp();
        String basic = "select uoid from formobject where formobject.discriminator like \"%Promt\"";
        this.promtIDs = CoreObject.createSession().createSQLQuery(basic).addScalar("uoid", org.hibernate.Hibernate.LONG).setCacheable(false).list();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testIsActive() throws PersistenceActionFindException {
        Iterator iterator = this.promtIDs.iterator();
        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().isActive((int)promtID);
        }
    }

    public void testDeterminePromtFinished() {
        Iterator iterator = this.promtIDs.iterator();
        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().determinePromtFinished(promtID);
        }
    }

    public void testDeterminePromtRunStarted() {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().determinePromtRunStarted(promtID);
        }
    }

    public void testDetermineSampleRun() {
        fail("Not yet implemented");
    }

    public void testDetermineSamplesFinishedByPromtID() {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().determineSamplesFinishedByPromtID(promtID);
        }
    }

    public void testFindAcquisitionNameBySampleID() {
        fail("Not yet implemented");
    }

    public void testFindChildActiveIDs() {
        fail("Not yet implemented");
    }

    public void testFindChildsIDs() {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            assertTrue(new SXQuery().findChildActiveIDs(promtID).size() > 0);
        }
    }

    public void testFindClassInformationString() {
        fail("Not yet implemented");
    }

    public void testFindClazzIDBySampleID() {
        fail("Not yet implemented");
    }

    public void testFindClazzIDsByPromtID() {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().findClazzIDsByPromtID(promtID);
        }
    }

    public void testFindClazzesIDbyClazz() {
        fail("Not yet implemented");
    }

    public void testFindDimensionIDsByClassID() {
        fail("Not yet implemented");
    }

    public void testFindLabelsforClazz() {
        fail("Not yet implemented");
    }

    public void testFindMultiField4ClazzIDsByPromtID() {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().findMultiField4ClazzIDsByPromtID(promtID);
        }
    }

    public void testFindParentInt() {
        fail("Not yet implemented");
    }

    public void testFindParentLong() {
        fail("Not yet implemented");
    }

    public void testFindPromtAbstractByPromtID() throws PersistenceActionFindException {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().findPromtAbstractByPromtID(promtID);
        }
    }

    public void testFindPromtClassesString() {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().findPromtClassesString(promtID);
        }
    }

    public void testFindPromtCollaboration() throws PersistenceActionFindException {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().findPromtCollaboration(promtID);
        }
    }

    public void testFindPromtCollaborationHTML() throws PersistenceActionFindException {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().findPromtCollaborationHTML(promtID);
        }
    }

    public void testFindPromtCollaborationString() throws PersistenceActionFindException {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().findPromtCollaborationString(promtID);
        }
    }

    public void testFindPromtComment() {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            assertTrue(new SXQuery().findPromtComment(promtID).length() > 0);
        }
    }

    public void testFindPromtIDbyClazz() {
        fail("Not yet implemented");
    }

    public void testFindPromtIDbySampleInt() {
        fail("Not yet implemented");
    }

    public void testFindPromtIDbySampleLong() {
        fail("Not yet implemented");
    }

    public void testFindPromtIDbySpeciesString() {
        fail("Not yet implemented");
    }

    public void testFindPromtIDbySpeciesInt() {
        fail("Not yet implemented");
    }

    public void testFindPromtIDs() {
        assertTrue(this.promtIDs.size() == new SXQuery().findPromtIDs().size());
    }

    public void testFindPromtStructure() {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().findPromtStructure(promtID);
        }
    }

    public void testFindPromtTitleByPromtID() throws PersistenceActionFindException {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().findPromtTitleByPromtID(promtID);
        }
    }

    public void testFindPromtUserAccessRightForPromtID() {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            assertTrue(new SXQuery().findPromtUserAccessRightForPromtID(promtID).size() > 0);
        }
    }

    public void testFindPromtUserAccessRightForUserIDLong() {
        fail("Not yet implemented");
    }

    public void testFindPromtUserAccessRightForUserIDLongLong() {
        fail("Not yet implemented");
    }

    public void testFindSample() {
        fail("Not yet implemented");
    }

    public void testFindSampleIDByAcquisitionName() {
        fail("Not yet implemented");
    }

    public void testFindSampleIDByAcquisitionNameFILEBASED() {
        fail("Not yet implemented");
    }

    public void testFindSampleIDsByClazzID() {
        fail("Not yet implemented");
    }

    public void testFindSampleIDsByPromtID() {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            assertTrue(new SXQuery().findSampleIDsByPromtID(promtID).size() > 0);
        }
    }

    public void testFindSampleLabelBySampleID() {
        fail("Not yet implemented");
    }

    public void testFindScannedPairLong() {
        fail("Not yet implemented");
    }

    public void testFindScannedPairString() {
        fail("Not yet implemented");
    }

    public void testFindSpecies() {
        fail("Not yet implemented");
    }

    public void testFindSpeciesbyPromtID() {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().findSpeciesbyPromtID(promtID);
        }
    }

    public void testFindVariationsIDsByClassID() {
        fail("Not yet implemented");
    }

    public void testUpdateCommentLongString() {
        fail("Not yet implemented");
    }

    public void testUpdateCommentLongStringUserDO() {
        fail("Not yet implemented");
    }

    public void testUpdateField() {
        fail("Not yet implemented");
    }

    public void testFindQuestionValue() {
        fail("Not yet implemented");
    }

    public void testFindQuestionValueString() {
        fail("Not yet implemented");
    }

    public void testRoot() {
        fail("Not yet implemented");
    }

    public void testFindPubdataBySampleID() {
        fail("Not yet implemented");
    }

    public void testFindComoundNamesBySpecies() {
        fail("Not yet implemented");
    }

    public void testIsCacheActive() {
        
    }

    public void testSetCacheActive() {
        fail("Not yet implemented");
    }

    public void testDetermineNumberOfSamples() {
        Iterator iterator = this.promtIDs.iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            new SXQuery().determineNumberOfSamples(promtID);
        }
    }


    public void testDetermineNumberOfSamplesFinishedLongTechnology() throws PersistenceActionFindException {
        Iterator technolgies = TechnologyProvider.getTechnologies().iterator();

        while (technolgies.hasNext()){
            Technology technology = (Technology)technolgies.next();
            Iterator iterator = this.promtIDs.iterator();

            while (iterator.hasNext()){
                long promtID = Long.parseLong("" + iterator.next());
                new SXQuery().determineNumberOfSamplesFinished(promtID,technology);
            }

        }
    }

    public void testDetermineNumberOfSamplesFinishedLong() throws PersistenceActionFindException {
        Iterator technolgies = TechnologyProvider.getTechnologies().iterator();

        while (technolgies.hasNext()){
            Technology technology = (Technology)technolgies.next();
            Iterator iterator = this.promtIDs.iterator();

            while (iterator.hasNext()){
                long promtID = Long.parseLong("" + iterator.next());
                new SXQuery().determineNumberOfSamplesFinished(promtID);
            }

        }
    }
}
