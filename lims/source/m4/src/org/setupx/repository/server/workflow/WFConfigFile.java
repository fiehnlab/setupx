package org.setupx.repository.server.workflow;

import org.setupx.repository.core.InitException;
import org.setupx.repository.core.util.xml.XMLFile;
import org.setupx.repository.core.util.xml.XPath;
import org.setupx.repository.core.util.xml.XPathException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

import java.io.File;
import java.io.UTFDataFormatException;


/**
 * a file containing all information for configurating a workflow.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class WFConfigFile extends XMLFile {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new WFConfigFile object.
   *
   * @param i_SourceFile a file containg the information for the workflow
   *
   * @throws SAXException
   * @throws UTFDataFormatException
   * @throws SAXException 
   * @throws UTFDataFormatException 
   */
  public WFConfigFile(File i_SourceFile) throws UTFDataFormatException, SAXException {
    super(i_SourceFile);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * returns an array of ids that point as id to the configurationid of an processable element
   *
   * @return an array of ids that point as id to the configurationid of an processable element
   *
   * @throws InitException probs whie init the file
   */
  public WorkFlowConfiguration getWorkFlowConfiguration()
    throws InitException {
    WorkFlowConfiguration configuration = new WorkFlowConfiguration();

    NodeList list;

    try {
      configuration.id = XPath.pathValue(this.getDocument(), "/workflow/@id");
      configuration.comment = XPath.pathValue(this.getDocument(), "/workflow/@comment");

      list = XPath.path(this.getDocument(), "/workflow/id");
    } catch (XPathException e) {
      throw new InitException("unable to find the ids in the file ", e);
    }

    configuration.wfElementIds = new int[list.getLength()];

    for (int i = 0; i < list.getLength(); i++) {
      Node j = list.item(i);
      configuration.wfElementIds[i] = Integer.parseInt(j.getFirstChild().getNodeValue());
    }

    return configuration;
  }
}
