package org.setupx.repository.server.persistence;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.Util;



public class PersistenceActionUpdateException extends PersistenceActionException {

    public PersistenceActionUpdateException(Object coreObjectThatCanNotBeUpdated,  Exception e) {
        super("unable to update: " + Util.getClassName(coreObjectThatCanNotBeUpdated), e);
    }


    public PersistenceActionUpdateException(String string) {
        super(string);
    }

    public PersistenceActionUpdateException(String string, PersistenceActionFindException e) {
        super(string, e);
    }
}
