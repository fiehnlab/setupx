package org.setupx.repository.server.workflow;

import org.setupx.repository.core.InitException;
import org.setupx.repository.core.util.File;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.UtilException;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.workflow.elements.Configuration;
import org.setupx.repository.server.workflow.elements.ProcessingException;

import java.net.URL;

import java.util.Vector;


/**
 * abstract super class for WFElements containing implementations for getConfiguration
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 *
 * @see org.setupx.repository.server.workflow.WFElement#getConfiguration()
 */
public abstract class WFElement {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @link aggregationByValue
   * @undirected
   * @labelDirection forward
   * @label dircribed by
   */
  protected Configuration configuration = null;

  // the workflow where this element is attached to ...
  private Workflow workflow = null;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final int DEFAULT_ID_DECISION = 35;
  public static final int DEFAULT_ID_PROCESS = 68;

  {
    Logger.log(this, "new instance");
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new WFElementImpl object.
   *
   * @throws InitException 
   * @throws WorkFlowException 
   */
  public WFElement() throws InitException, WorkFlowException {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * sets the configuration
   *
   * @param configuration the configuration
   */
  public final void setConfiguration(Configuration configuration) {
    this.configuration = configuration;
  }

  /**
   * return the ConfigutationObject declaring this object
   *
   * @return the ConfigutationObject declaring this object
   */
  public final Configuration getConfiguration() {
    return configuration;
  }

  /**
   * @return
   */
  public Workflow getWorkflow() {
    return this.workflow;
  }

  /**
   * the action method, that must be implemented by every process
   *
   * @throws ProcessingException
   */
  public abstract void action() throws ProcessingException;

  /**
   * @see Object#toString();
   */
  public final String toString() {
    return Util.getClassName(this) + ": " + this.getConfiguration().toString();
  }

  /**
   * Findes all Derivates for a WFElement in a defined package.
   *
   * @param name The directory containing the Classes that will be checked
   *
   * @throws UtilException unable to create instances of the classes
   */
  public static Class[] findImplementations(String name)
    throws UtilException {
    Logger.debug(null, "looking for instances of " + WFElement.class.getName() + " in package: " + name);

    // vector containing all classes that match
    Vector classes = new Vector();
    String packageName = name;

    if (!name.startsWith("/")) {
      name = "/" + name;
    }

    name = name.replace('.', '/');

    // Get a File object for the package
    URL url = File.class.getResource(name);

    if (url == null) {
      throw new UtilException("unable to create an url for Packagename: " + name);
    }

    java.io.File directory = new java.io.File(url.getFile());
    Logger.debug(null, directory.getAbsolutePath());

    if (directory.exists()) {
      // Get the list of the files contained in the package
      String[] files = directory.list();

      for (int i = 0; i < files.length; i++) {
        // we are only interested in .class files
        if (files[i].endsWith(".class")) {
          // removes the .class extension
          String classname = files[i].substring(0, files[i].length() - 6);

          try {
            Class myClass = Class.forName(packageName + "." + classname);
            Logger.debug(null, "working on " + myClass.getName());

            // Try to create an instance of the object
            Object o = myClass.newInstance();

            if (o instanceof WFElement) {
              classes.add(myClass);
            }
          } catch (ClassNotFoundException cnfex) {
            System.err.println(cnfex);
            throw new UtilException("problems creating an object for : " + name + "." + classname);
          } catch (InstantiationException iex) {
            // We try to instanciate an interface
            // or an object that does not have a
            // default constructor
            System.err.println(iex);
          } catch (IllegalAccessException iaex) {
            // The class is not public
            System.err.println(iaex);
          }
        }
      }
    }

    // vector to WFElement[]
    Class[] result = new Class[classes.size()];

    for (int i = 0; i < classes.size(); i++) {
      Class clazz = (Class) classes.get(i);
      result[i] = clazz;
    }

    return result;
  }

  /**
   * TODO: 
   *
   * @param workflow2 TODO
   */
  public void setWorkflow(Workflow workflow2) {
    this.workflow = workflow2;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public static String[] none() {
    return new String[] {  };
  }

  /**
   * @return all keys that link to a element in the memory that are READ by this element
   */
  abstract protected String[] getDataInKeys();

  /**
   * @return all keys that link to a element in the memory that are STORED by this element
   */
  protected abstract String[] getDataOutKeys();

  /**
   * @throws InitException
   */
  protected void init() throws InitException {
    // checking mem
    if (this.workflow == null) {
      throw new InitException();
    }

    if (this.configuration == null) {
      throw new InitException();
    }

    // check if all fileds that this element requieres are present in the mem
    String[] inKeys = this.getDataInKeys();

    for (int i = 0; i < inKeys.length; i++) {
      if (!this.workflow.workflowMemory.containsKey(inKeys[i])) {
        throw new WorkflowMemoryException("the key: " + inKeys[i] + " is requiered by " + Util.getClassName(this) + " but not in the mem - check the former elements in the wf");
      }
    }

    String[] outKeys = this.getDataOutKeys();

    // adding all fields, that this element stores in the wf to the mem.
    for (int i = 0; i < outKeys.length; i++) {
      String key = outKeys[i];
      this.workflow.workflowMemory.push(new MemoryObject4Init(key));
    }
  }

  /**
   * register this instance at the handler
   *
   * @throws InitException
   * @throws WFMException
   *
   * @deprecated all the elements are beeing registrated by the manager
   */
  protected void register() throws InitException, WorkFlowException {
    throw new UnsupportedOperationException();

    /*
       Logger.debug(this, "registrating this");
                          if (lnkWorkFlowManager == null) {
                              //lnkWorkFlowManager = WorkFlowManagerFactory.getInstance();
                              throw new InitException(
                                      "unable to init register this object, cause no workflowmanager initatlized ...");
                          }
                          lnkWorkFlowManager.register(this, false);*/
  }
}
