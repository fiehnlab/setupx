package org.setupx.repository.server.db;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

public class SQLStatementPoolInit {

    private Vector detected_files = new Vector();

    public SQLStatementPoolInit() {
    }

    public static void main(String[] args) {
        SQLStatementPoolInit init = new SQLStatementPoolInit();
        
          //init.searchQueries("select", new File("/Users/scholz/Documents/2008/dev/workspace/setupx_gcode/lims/source/m4/src/org/setupx/repository/core"));
        
//        init.searchQueries("select", new File("/Users/scholz/setupx_gcode/lims/source/m4/src/org/setupx"));
        init.searchQueries("select", new File("/Users/scholz/setupx_gcode/lims/source/m4/share/"));

        init.extract();
    }

    private void extract() {
        // take each class
        
        for (Iterator iterator = detected_files.iterator(); iterator.hasNext();) {
            File file = (File) iterator.next();
            SQLExtraction extraction = new SQLExtraction(file);
            extraction.process();
        }
    }

    /**
     * read each directory and every file and look for the term
     * 
     * @param string
     */
    private void searchQueries(final String token, File directory) {

        File[] files = directory.listFiles();

        for (int i = 0; files != null && i < files.length; i++) {
            File file = files[i];

            if (!file.isDirectory()) {
                if (findToken(file, token)) {
                    System.out.println("sql in " + file.getName());
                    this.detected_files.add(file);
                } 
            } else {
                //System.out.println(" searching in " + file.getName());
                this.searchQueries(token, file);
            }
        }

    }

    private static boolean findToken(File file, String token) {
        // scanning ....
        try {
            String content = org.setupx.repository.core.util.File.getFileContent(file);
            int pos = content.toLowerCase().indexOf(token.toLowerCase());
            return pos > 0;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
