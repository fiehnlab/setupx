package org.setupx.repository.server.db;

/**
 * @deprecated not used anymore
 */
public class QueryException extends Exception {
  /**
   *
   */
  public QueryException() {
    super();
  }

  /**
   */
  public QueryException(String arg0) {
    super(arg0);
  }

  /**
   */
  public QueryException(Throwable arg0) {
    super(arg0);
  }

  /**
   */
  public QueryException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }
}
