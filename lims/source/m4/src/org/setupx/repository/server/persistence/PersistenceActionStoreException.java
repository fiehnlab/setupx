package org.setupx.repository.server.persistence;

public class PersistenceActionStoreException extends PersistenceActionException {

    public PersistenceActionStoreException(Exception e) {
        super("unable to store: ", e);
    }

    public PersistenceActionStoreException(String string, Exception e) {
        super(string, e);
    }
}
