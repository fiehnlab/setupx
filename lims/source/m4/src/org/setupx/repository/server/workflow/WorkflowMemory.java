package org.setupx.repository.server.workflow;

import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;

import java.util.Enumeration;
import java.util.Hashtable;


/**
 * Memory for one Workflow - every process element in the workflow can access this element and can excange data by pushing and pulling data into this element.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 */
public class WorkflowMemory {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * an array containing WFElement objects. the position in the vector is the id.
   *
   * @associates org.setupx.repository.server.workflow.MemoryObject
   * @directed
   * @link aggregationByValue
   * @clientCardinality 1
   * @supplierCardinality 0..
   * @undirected
   */
  private Hashtable mem = null;
  private MemoryObject tempMemeoryObject;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new WorkflowMemory object.
   */
  public WorkflowMemory() {
    Logger.log(this, "new instance");
    this.mem = new Hashtable();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param string
   */
  public boolean containsKey(String string) {
    return this.mem.containsKey(string);
  }

  /**
   * returns an array with all keys of the objects stored in the mem
   *
   * @return an array with all keys of the objects stored in the mem
   */
  public String[] keys() {
    String[] result = new String[mem.size()];
    Enumeration enumeration = this.mem.keys();
    int i = 0;

    while (enumeration.hasMoreElements()) {
      result[i] = (String) enumeration.nextElement();
      i++;
    }

    return result;
  }

  /**
   * pulling an object out of this mem - the object will stay inside this memory
   *
   * @param key the key of the object that is looked for
   *
   * @return the Memoryobject that was found
   *
   * @throws MemoryObjectNotFoundException the object was not found
   */
  public MemoryObject pull(String key) throws MemoryObjectNotFoundException {
    Logger.debug(this, "fetching " + key + " out of me.");
    tempMemeoryObject = (MemoryObject) mem.get(key);

    if (tempMemeoryObject == null) {
      throw new MemoryObjectNotFoundException("the object (" + key + ") was not found in the WorkflowMemory.");
    }

    return tempMemeoryObject;
  }

  /**
   * adds a MemoryObject to this memory
   *
   * @param object the object that will be stored
   */
  public void push(MemoryObject object) {
    Logger.debug(this, "storing " + object.key + " inside me.");
    mem.put(object.key, object);
  }

  /**
   * removes a MemoryObject from the memory
   *
   * @param key the key of the object, that will be deleted
   *
   * @throws MemoryObjectNotFoundException the element with that key was not found
   */
  public void remove(String key) throws MemoryObjectNotFoundException {
    MemoryObject memoryObject = this.pull(key);
    Logger.debug(this, "deleting " + key + " out of me.");
    mem.remove(memoryObject.key);
  }

  /**
   * number of elements inside this mem
   *
   * @return number of elements inside this mem
   */
  public int size() {
    return mem.size();
  }

  /**
   * toString ...
   */
  public String toString() {
    StringBuffer buffer = new StringBuffer();

    buffer.append(Util.getClassName(this)).append(":").append(this.size());

    Enumeration enumeration = mem.keys();

    while (enumeration.hasMoreElements()) {
      MemoryObject element = (MemoryObject) mem.get(enumeration.nextElement());
      buffer.append(element.toString());
    }

    return buffer.toString();
  }
}
