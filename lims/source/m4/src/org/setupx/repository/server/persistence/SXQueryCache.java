/**
 * ============================================================================ File:    SXQueryCache.java Package: org.setupx.repository.server.persistence cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.server.persistence;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class SXQueryCache {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param promt_id TODO
   * @param numberOfScannedSamples TODO
   */
  public static void setNumberOfFinishedSamplesByExperiment(long promt_id, int numberOfScannedSamples) {
    new SXQueryCacheObject(promt_id, numberOfScannedSamples);
  }

  /**
   * TODO: 
   *
   * @param promtID TODO
   *
   * @return TODO
   *
   * @throws SXQueryNonCachedException TODO
   */
  public static int getNumberOfFinishedSamplesByExperiment(long promtID)
    throws SXQueryNonCachedException {
    return SXQueryCacheObject.findByExperimentID(promtID).getNumberofScannedSamples();
  }
}
