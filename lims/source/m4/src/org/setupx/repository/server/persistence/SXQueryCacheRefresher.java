package org.setupx.repository.server.persistence;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.BackgroundThread;
import org.setupx.repository.core.communication.notification.NotificationCentral;
import org.setupx.repository.core.communication.notification.SystemNotification;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;

public class SXQueryCacheRefresher extends BackgroundThread {

    private long pause;
    public Date lastScanDate;

    public SXQueryCacheRefresher(long statusRefresh) {
        this.pause = statusRefresh;
    }
    

    /*
     *  (non-Javadoc)
     * @see java.lang.Thread#run();
     */
    public final void run() {
        NotificationCentral.notify(new SystemNotification("starting " + Util.getClassName(this)));

        for (; true;) {
            try {
                Logger.log(this, "sleeping for " + pause + ".");
                this.lastScanDate = new Date();
                this.sleep(pause);
            } catch (InterruptedException e) {
                Logger.warning(this, "err while scanning: " ,e);
            }

            Logger.info(this, "refreshing cache now- " + Util.getDateString());

            try {
                refreshSXQueryCache();
            } catch (Exception e1) {
                Logger.warning(this, "err while refreshing: " ,e1);
            }
        }
    }

    public static void init() {
        Logger.info(SXQueryCacheRefresher.class, "creating new " + SXQueryCacheRefresher.class.getName());
        Thread thread = new SXQueryCacheRefresher(Config.QUERY_CACHING_REFRESH);
        thread.setDaemon(true);
        thread.start();
    }

    private void refreshSXQueryCache() throws InterruptedException {
        Session s = CoreObject.createSession();
        
        // load all experiment IDs
        List promtIDs = new SXQuery(s).findPromtIDs();
        
        
        Iterator promtIDIterator = promtIDs.iterator();
        while (promtIDIterator.hasNext()) {
            long promtID = Long.parseLong(promtIDIterator.next().toString());
            
            int cachedOLD = 0;
            // current
            try {
                cachedOLD = SXQueryCache.getNumberOfFinishedSamplesByExperiment(promtID);
            } catch (SXQueryNonCachedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            // delete all of the cache
            try {
                    SXQueryCacheObject.findByExperimentID(promtID).remove();
            } catch (Exception e){
                e.printStackTrace();
            }
            
            
            int numberOfScannedSamples = new SXQuery(s).determineNumberOfSamplesFinished(promtID);
            
            if (numberOfScannedSamples != cachedOLD){
                Logger.info(this, "cache for " + promtID + " updated. Current number of samples now: " + numberOfScannedSamples + " (was + " + cachedOLD + ")");
            }
            
            // each experiment gets a refresh
            new SXQueryCacheObject(promtID, numberOfScannedSamples);
            
            // pause after each value to keep the load down
            this.sleep(5000);
            }
    }

}
