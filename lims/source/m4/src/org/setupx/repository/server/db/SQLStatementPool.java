package org.setupx.repository.server.db;

import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;


public class SQLStatementPool {

    private Object relatedObject;

    public SQLStatementPool(Object object) {
        Logger.debug(null, "class: " + object.getClass());
        this.relatedObject = object;
    }

    
    /**
     * return the statement with that specif number
     * @param i
     * @return
     */
    public SQLStatement getStatement(String descr) {
        return this.getStatement(descr, "unknown");    
    }

    
    public SQLStatement getStatement(String descr, String defaultTerm) {
        String classname = Util.getClassName(this.relatedObject);

        String propName = classname.concat(".java.").concat(descr);
        
        Logger.debug(this, "searching for statement \"" + propName + "\"");
        
        return SQLSettings.getValue(propName, defaultTerm);
    }


    public static SQLStatement getStatement(Object object, String desc ) {
        return new SQLStatementPool(object).getStatement(desc);
    }


    public static SQLStatement getStatement(Object object, String desc, String defaultString) {
        return new SQLStatementPool(object).getStatement(desc, defaultString);
    }


    public static void initStatement(String classname, String descr, String term) {
        String propName = classname.concat(".").concat(descr);
        SQLSettings.getValue(propName, term);
    }
}