package org.setupx.repository.server.db;

import org.setupx.repository.core.util.Util;


public class SQLStatement {
    public static final String TOKENNAME = "$TOKEN$";
    private String sqlString;

    public SQLStatement(String value) {
        this.sqlString = value;
    }

    /**
     * replaces the value inside the string by the given value
     * @param id
     * @return new String with replaced value(s)
     */
    public String setParameterValue(String tokenName, String replacement) {
        return Util.replace(this.sqlString, tokenName, replacement);
    }

    public String setParameterValue(String tokenName, int replacement) {
        return this.setParameterValue(tokenName, "" + replacement);
    }

    public String setParameterValue(String tokenName, long replacement) {
        return this.setParameterValue(tokenName, "" + replacement);
    }

    

    
    public String setSingleParameterValue(int replacement) {
        return this.setSingleParameterValue("" + replacement);
    }

    public String setSingleParameterValue(long replacement) {
        return this.setSingleParameterValue("" + replacement);
    }

    public String setSingleParameterValue(String replacement) {
        return Util.replace(this.sqlString, TOKENNAME, replacement);
    }
    
    public String toString(){
        return this.sqlString;
    }
}
