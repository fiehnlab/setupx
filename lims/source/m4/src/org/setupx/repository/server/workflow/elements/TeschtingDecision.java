package org.setupx.repository.server.workflow.elements;

import org.setupx.repository.core.InitException;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.workflow.WorkFlowException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.2 $
 */
public class TeschtingDecision extends Decision {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new TeschtingDecision object.
   *
   * @param configuration 
   *
   * @throws InitException 
   * @throws WorkFlowException 
   */
  public TeschtingDecision(DecisionConfiguration configuration)
    throws InitException, WorkFlowException {
    super(configuration);
  }

  /**
   * Creates a new TeschtingDecision object.
   *
   * @throws InitException 
   * @throws WorkFlowException 
   */
  public TeschtingDecision() throws InitException, WorkFlowException {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @throws ProcessingException TODO
   */
  public void action() throws ProcessingException {
    Logger.log(this, "action is called for process : " + this.getConfiguration().getId() + "[" + this.getConfiguration().getLabel() + "]");
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.WFElement#getDataInKeys()
   */
  protected String[] getDataInKeys() {
    // TODO Auto-generated method stub
    return null;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.server.workflow.WFElement#getDataOutKeys()
   */
  protected String[] getDataOutKeys() {
    // TODO Auto-generated method stub
    return null;
  }
}
