package org.setupx.repository.server.persistence.hibernate;

import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.PromtCreateException;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.SampleScheduledException;

import java.util.Hashtable;


/**
 * 
 *
 * @author $author$
 * @version $Revision$
 */
public class PerstistenceCheck {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Hashtable hashtableLoadSet = new Hashtable();
  private Hashtable hashtableOriginalSet = new Hashtable();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new PerstistenceCheck object.
   *
   * @throws PersistenceActionException 
   * @throws PromtCreateException 
   * @throws SampleScheduledException 
   */
  public PerstistenceCheck() throws PersistenceActionException, PromtCreateException, SampleScheduledException {
    // create a new Promt
    Promt promt = Promt.load(0);

    // expand it a little
    // save it
    promt.save();

    int numElements = countElements(promt);

    // iterate through each of the elements and store the id in an hashtable
    collectIDs(promt, hashtableOriginalSet);

    // load the same promt
    long promtuoid = promt.getUOID();
    promt = null;
    promt = Promt.load(promtuoid);

    // iterate through each of the elements and check if they are in the hashtable
    collectIDs(promt, hashtableLoadSet);

    // if not msg it
    compare(hashtableLoadSet, hashtableOriginalSet, numElements);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param formObject TODO
   * @param ht TODO
   */
  private static void add(FormObject formObject, Hashtable ht) {
    Logger.log(formObject, "adding " + formObject.getUOID());

    if (formObject.getUOID() == 0) {
      throw new RuntimeException("Object is not persistent.");
    }

    ht.put("" + formObject.getUOID(), formObject);
  }

  /**
   * TODO: 
   *
   * @param formObject TODO
   * @param ht TODO
   */
  private static void collectIDs(FormObject formObject, Hashtable ht) {
    add(formObject, ht);

    FormObject[] fields = formObject.getFields();

    for (int i = 0; i < fields.length; i++) {
      collectIDs(fields[i], ht);
    }
  }

  /**
   * TODO: 
   *
   * @param hashtableLoadSet2 TODO
   * @param hashtableOriginalSet2 TODO
   * @param numElements TODO
   */
  private static void compare(Hashtable hashtableLoadSet2, Hashtable hashtableOriginalSet2, int numElements) {
    if (hashtableLoadSet2.size() != hashtableOriginalSet2.size()) {
      throw new RuntimeException("not matching sizes.");
    }

    if (hashtableLoadSet2.size() != numElements) {
      throw new RuntimeException("not the inital size. orig:" + numElements + "  load:" + hashtableLoadSet2.size());
    }
  }

  /**
   * TODO: 
   *
   * @param formObject TODO
   *
   * @return TODO
   */
  private static int countElements(FormObject formObject) {
    int count = 0;

    // for myself
    count = count + 1;
    Logger.log(formObject, "element nr. " + count);

    // each of my childs
    FormObject[] formObjects = formObject.getFields();

    for (int i = 0; i < formObjects.length; i++) {
      count = count + countElements(formObjects[i]);
    }

    Logger.log(formObject, "element nr. " + count);

    return count;
  }
}
