package org.setupx.repository.server.workflow.elements;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public interface Configuration {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param description TODO
   */
  public void setDescription(String description);

  /**
   * @return
   */
  public String getDescription();

  /**
   * @param id The id to set.
   */
  public void setId(int id);

  /**
   * @return Returns the id.
   */
  public int getId();

  /**
   * @param label The label to set.
   */
  public void setLabel(String label);

  /**
   * @return Returns the label.
   */
  public String getLabel();

  /**
   * return a summary what the process is doing and parts of the configuration
   *
   * @return a summary what the process is doing and parts of the configuration
   */
  public String toString();
}
