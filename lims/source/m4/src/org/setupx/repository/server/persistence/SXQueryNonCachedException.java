package org.setupx.repository.server.persistence;

public class SXQueryNonCachedException extends Exception {

    public SXQueryNonCachedException(Exception e) {
        super(e);
}

    public SXQueryNonCachedException(String string) {super(string);
    }

}
