package org.setupx.repository.server.workflow;

import org.setupx.repository.core.InitException;
import org.setupx.repository.core.util.logging.Logger;


/**
 * Factory for creating a WorkflowManager, that does exist just as a single instance in the system!
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.2 $
 */
public class WorkFlowManagerFactory {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static WorkFlowManager instance = null;
  private static WorkFlowManagerFactory thiZ = new WorkFlowManagerFactory();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private WorkFlowManagerFactory() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * return an instance of the WorkFlowManager
   *
   * @return an instance of the WorkFlowManager
   *
   * @throws WorkFlowException
   * @throws InitException
   */
  public static WorkFlowManager getInstance() throws InitException, WorkFlowException {
    // Logger.debug(thiZ, "requesting WorkFlowManager");
    if (instance == null) {
      Logger.err(thiZ, "there is no existing one - so a new instance of WorkFlowManager is created ");
      instance = new org.setupx.repository.server.workflow.WorkFlowManager();

      instance.initWorkflows();
    }

    return instance;
  }
}
