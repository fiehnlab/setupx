package org.setupx.repository.server.workflow.elements;

import org.setupx.repository.core.communication.vocabulary.VocabularyCheckException;
import org.setupx.repository.server.workflow.MemoryObjectNotFoundException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class ProcessingException extends Exception {
  /**
   * Creates a new ProcessingException object.
   *
   * @param e 
   */
  public ProcessingException(MemoryObjectNotFoundException e) {
    super(e);
  }

  /**
   * Creates a new ProcessingException object.
   *
   * @param e1 
   */
  public ProcessingException(VocabularyCheckException e1) {
    super(e1);
  }

  /**
   * @param e1
   */
  public ProcessingException(Exception e1) {
    // TODO Auto-generated constructor stub
  }
}
