package org.setupx.repository.server.workflow.elements;

import org.setupx.repository.core.InitException;
import org.setupx.repository.server.workflow.WFElement;
import org.setupx.repository.server.workflow.WorkFlowException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public abstract class Decision extends WFElement {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Decision object.
   *
   * @param configuration 
   *
   * @throws InitException 
   * @throws WorkFlowException 
   */
  public Decision(DecisionConfiguration configuration)
    throws InitException, WorkFlowException {
    this.configuration = configuration;
  }

  /**
   * Creates a new Decision object.
   *
   * @throws InitException 
   * @throws WorkFlowException 
   */
  public Decision() throws InitException, WorkFlowException {
    this.configuration = new StandardConfiguration(WFElement.DEFAULT_ID_DECISION);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @throws ProcessingException TODO
   */
  public abstract void action() throws ProcessingException;
}
