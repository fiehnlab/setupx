/**
 * ============================================================================ File:    PersistenceConfiguration.java Package: org.setupx.repository.server.persistence.hibernate cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.server.persistence.hibernate;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;

public class PersistenceConfiguration extends CoreObject {

    private DummyObject obj = new DummyObject();
    private SessionFactory factory;

    private PersistenceConfiguration(){
        debug(this, "[init()]creating session factory ");
        if (this.factory == null) {
            String key;
            String value;

            debug(this, "[init()]setting up: SessionFactory");

            // reading properties
            Properties properties = new Properties();
            debug(this, "[init()]start reading properties.");
            try {
                // check if file is ok
                File fileConfiguration = new File(Config.CONF_HIBERNATE_FILE);
                debug(this, "[init()]reading config: " + fileConfiguration.toString());
                if (!fileConfiguration.exists() ||  !fileConfiguration.canRead()) throw new RuntimeException("can not init Persistence: ConfigurationFile " + Config.CONF_HIBERNATE_FILE + " can not be read.");
                properties.load(new FileInputStream(fileConfiguration));
                debug(this, "[init()]read config: " + Config.CONF_HIBERNATE_FILE);
            } catch (Exception e) {
                throw new RuntimeException();
            }

            debug(this, "[init()]creating new Configuration");
            Configuration cfg = new Configuration();
            Enumeration enumeration = properties.keys();

            while (enumeration.hasMoreElements()) {
                key = (String) enumeration.nextElement();
                value = (String) properties.get(key);
                addConfigParameter(key, value,cfg);
            }

            debug(this, "[init()]adding mapping directory: " + Config.DIRECTORY_CONF_HIBERNATE + "mapping");
            cfg.addDirectory(new File(Config.DIRECTORY_CONF_HIBERNATE + "mapping"));

            addConfigParameter("hibernate.show_sql", "false", cfg);
            addConfigParameter(Environment.HBM2DDL_AUTO, "update", cfg); //'create', 'create-drop' and 'update'.
                    
            // activate caching
            addConfigParameter("hibernate.cache.provider_class", "org.hibernate.cache.HashtableCacheProvider", cfg);
            addConfigParameter("hibernate.cache.use_query_cache", "true", cfg);
            addConfigParameter("hibernate.cache.use_second_level_cache", "true", cfg);

            debug(this, "[init()]building SessionFactory");
            this.factory = cfg.buildSessionFactory();

            debug(this, "[init()]sessionfactory: " + this.factory);
            debug(this, "[init()]done setting up: SessionFactory");
        }
    }

    private static void addConfigParameter(String key, String value, Configuration cfg) {
        debug(null, "[init()]setting config parameter: " + key + ": " + value);
        cfg.setProperty(key, value);
    }

    private static class PersistenceConfigurationHolder{
        private static PersistenceConfiguration persistenceConfiguration = new PersistenceConfiguration();
    }


    public static PersistenceConfiguration getInstance(){
        return PersistenceConfigurationHolder.persistenceConfiguration;
    }

    public static SessionFactory createSessionFactory() {
        return getInstance().factory;
    }
}
