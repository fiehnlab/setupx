package org.setupx.repository.server.db;

/**
 * 
 *
 * @author <a href="mailto:scholz@zeypher.com?subject=m1" >&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class DataSourceFactoryException extends Exception {
  /**
   *
   */
  public DataSourceFactoryException() {
    super();

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public DataSourceFactoryException(String arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public DataSourceFactoryException(Throwable arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   */
  public DataSourceFactoryException(String arg0, Throwable arg1) {
    super(arg0, arg1);

    // TODO Auto-generated constructor stub
  }
}
