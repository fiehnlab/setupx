package org.setupx.repository.server.persistence;

public abstract class PersistenceActionException extends Exception{

    public PersistenceActionException(Throwable  e) {
        super(e);    }

    public PersistenceActionException(String string, Throwable  e) {
        super(string, e);
    }

    public PersistenceActionException(String string) {
        super(string);
    }
}
