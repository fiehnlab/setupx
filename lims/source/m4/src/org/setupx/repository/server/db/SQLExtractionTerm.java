package org.setupx.repository.server.db;

import java.util.Iterator;
import java.util.Vector;


public class SQLExtractionTerm {
    int lastnumberOfToken = 0;
    private Vector placeholder = new Vector();
    private String term;

    public SQLExtractionTerm(String extract) throws UnsupportedTermException {
        //extract = extract.replaceAll("\\s\\s+|\\n|\\r", " ");
        
        extract = extract.replaceAll("\\s\\s+|\\n|\\r", " ");
        extract = replace(extract, "\" + \"", " ");

//        System.out.println(extract);
        
        extract = replaceParameter(extract, "\\\"" , "\"\\\"");
        extract = replaceParameter(extract, " \" + " , " + \"");


        extract = replace(extract,"\"\\\"\\\"", "\"\"");
        extract = replace(extract, "\" select", "\"select");

        // remove leading and last "
        if (extract.charAt(0) == '\"' &&  extract.charAt(extract.length() -1 ) == '\"'){
            extract = extract.substring(1,extract.length()-1);
        } 
        
        if (extract.indexOf("select") != 0){
            System.err.println("illegal (" + extract.indexOf("select") + "): " + extract);
            throw new UnsupportedTermException();
        } else {
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            System.out.println("starting extraction from: " + extract);
            for (Iterator iterator = this.placeholder.iterator(); iterator.hasNext();) {
                Placeholder placeholder = (Placeholder) iterator.next();
                System.out.println("token: " + placeholder.getPlaceholderToken() + "    \tsource: " + placeholder.getVariableName());
            }
            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        }
        
        this.term = extract;
    }

    private String replaceParameter(String extract, String startToken, String endToken) {
        try {
            // cut out the entire term incl \"" ..
            // looking for something like    label = \"" + label + "\"
            
            int posStart = extract.indexOf(startToken);
            if (posStart < 0) return extract;
            
            int posEnd = extract.indexOf(endToken, posStart);
            if (posEnd < 0) return extract;
            
            String a  = extract.substring(0,posStart);
            String c  = extract.substring(posEnd + endToken.length(), extract.length());
            
            System.out.println("extract: " + extract);
            System.out.println("start: " + posStart);
            System.out.println("end  : " + posEnd);

            
            String variableName = extract.substring(posStart + startToken.length(), posEnd + 1);
            
            // paste a placeholder
            String placeholderToken = SQLStatement.TOKENNAME + lastnumberOfToken;
            lastnumberOfToken ++;
            
            System.out.println("variableName  : " + variableName);
            System.out.println("placeholderToken  : " + placeholderToken);
            // add a Placehodlerobject to the extractionterm
            this.addPlaceholder(variableName, placeholderToken);
            //System.out.println("variableName  : " + variableName);
            //System.out.println("placeholderToken  : " + placeholderToken);


            //System.out.println("a: " + a);
            //System.out.println("b: " + placeholderToken);
            //System.out.println("c: " + c);
            
            extract = a.concat(placeholderToken).concat(c);
            
            
            System.out.println("result: " + extract);
            return extract;
        } catch (Exception e) {
            e.printStackTrace();
            return extract;
        }
    }


    
    
    private void addPlaceholder(String variableName, String placeholderToken) {
        this.placeholder.add(new Placeholder(variableName, placeholderToken));
    }

    public static String replace(String source, String searchstring, String replacestring) {
        int start = source.indexOf(searchstring);

        if (start == -1) {
            return source;
        }

        String firstpart = source.substring(0, start);
        String endpart = source.substring(start + searchstring.length(), source.length());

        String result = firstpart + replacestring + endpart;

        if (result.indexOf(searchstring) > 0) {
            result = replace(result, searchstring, replacestring);
        }

        return result;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public int getLastnumberOfToken() {
        return lastnumberOfToken;
    }

    public Vector getPlaceholder() {
        return placeholder;
    }

    public void setLastnumberOfToken(int lastnumberOfToken) {
        this.lastnumberOfToken = lastnumberOfToken;
    }

    public void setPlaceholder(Vector placeholder) {
        this.placeholder = placeholder;
    }

}