package org.setupx.repository.server.db;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class WriterException extends Exception {
  /**
   * Creates a new WriterException object.
   */
  public WriterException() {
    super();
  }

  /**
   * Creates a new WriterException object.
   */
  public WriterException(String arg0) {
    super(arg0);
  }

  /**
   * Creates a new WriterException object.
   */
  public WriterException(Throwable arg0) {
    super(arg0);
  }

  /**
   * Creates a new WriterException object.
   */
  public WriterException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }
}
