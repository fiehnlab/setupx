package org.setupx.repository.server.db;

public class UnsupportedTermException extends Exception {

    public UnsupportedTermException() {
        // TODO Auto-generated constructor stub
    }

    public UnsupportedTermException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public UnsupportedTermException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public UnsupportedTermException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
