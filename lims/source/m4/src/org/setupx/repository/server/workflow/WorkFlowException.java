package org.setupx.repository.server.workflow;

import org.setupx.repository.core.InitException;


/**
 * workflowmanagerException!
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class WorkFlowException extends Exception {
  /**
   * @param string
   */
  public WorkFlowException(String string) {
    super(string);
  }

  /**
   * @param string
   * @param e
   */
  public WorkFlowException(String string, ArrayIndexOutOfBoundsException e) {
    super(string, e);
  }

  /**
   * @param e1
   */
  public WorkFlowException(InitException e1) {
    super(e1);
  }
}
