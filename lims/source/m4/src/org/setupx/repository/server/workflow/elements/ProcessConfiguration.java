package org.setupx.repository.server.workflow.elements;

/**
 * Configuration for a Single Process in the Workflow
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.1 $
 */
public class ProcessConfiguration extends DefaultConfiguration {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ProcessConfiguration object.
   *
   * @param label 
   * @param id 
   */
  public ProcessConfiguration(String label, int id) {
    super(label, id, "NA");
  }

  /**
   * Creates a new ProcessConfiguration object.
   *
   * @param label 
   * @param id 
   * @param description 
   */
  public ProcessConfiguration(String label, int id, String description) {
    super(label, id, description);
  }
}
