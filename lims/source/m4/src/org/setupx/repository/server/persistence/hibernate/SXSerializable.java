/**
 * ============================================================================ File:    SXSerializable.java Package: org.setupx.repository.server.persistence.hibernate cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.server.persistence.hibernate;

import java.io.Serializable;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public interface SXSerializable extends Serializable {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param lId TODO
   */
  public void setUOID(long lId);

  /**
   * TODO: 
   *
   * @return TODO
   */
  public long getUOID();
}
