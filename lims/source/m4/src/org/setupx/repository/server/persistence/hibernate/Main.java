/**
 * ============================================================================ File: Main.java Package:
 * org.setupx.repository.server.persistence.hibernate cvs: $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21
 * scholz Exp $ ============================================================================ Martin Scholz Copyright (C)
 * 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/
 * ============================================================================
 */
package org.setupx.repository.server.persistence.hibernate;

import org.setupx.repository.Config;

/**
 * Util Init Class
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class Main {
    // ~ Instance fields
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public static void main(String[] args) throws Exception {
        Config.initCoreUsers();
    }
}
