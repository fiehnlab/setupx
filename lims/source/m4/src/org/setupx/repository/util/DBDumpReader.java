package org.setupx.repository.util;

import java.io.File;
import java.net.URL;
import java.util.StringTokenizer;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.Zip;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.util.dump.DBvirtualTableEntry;

/**
 * @deprecated old version - do not use ! reades a set of database dump files
 *             and creates an XML file out of them
 */
public class DBDumpReader extends CoreObject {
    // ~ Instance fields
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    java.util.Vector names = new java.util.Vector(10);
    java.util.Vector nodes = new java.util.Vector(10);

    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new DBDumpReader object.
     * 
     * @throws Exception
     *             
     */
    public DBDumpReader() throws Exception {
        File directory = new File("C:\\dev\\data\\taxonomy");

        String path = "ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdmp.zip";
        this.readFiles(new URL(path), directory);

        // open taxonomy
        StringTokenizer stringTokenizer = new StringTokenizer(
                org.setupx.repository.core.util.File.getFileContent(new File(
                        directory.getAbsolutePath() + File.separator
                                + "nodes.dmp")), "\n");

        while (stringTokenizer.hasMoreTokens()) {
            String line = stringTokenizer.nextToken();
            DBvirtualTableEntry node = new DBvirtualTableEntry(line);
            this.nodes.add(node);
        }

        // open taxonomy
        stringTokenizer = new StringTokenizer(
                org.setupx.repository.core.util.File.getFileContent(new File(
                        directory.getAbsolutePath() + File.separator
                                + "names.dmp")), "\n");

        while (stringTokenizer.hasMoreTokens()) {
            String line = stringTokenizer.nextToken();
            DBvirtualTableEntry node = new DBvirtualTableEntry(line);
            this.names.add(node);
        }

        for (int i = 0; i < this.names.size(); i++) {
            DBvirtualTableEntry name = (DBvirtualTableEntry) this.names.get(i);
            this.log(name);
        }
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * TODO: 
     * 
     * @param args
     *            TODO
     * 
     * @throws Exception
     *             TODO
     */
    public static void main(String[] args) throws Exception {
        new DBDumpReader();
    }

    /**
     * TODO: 
     * 
     * @param content
     *            TODO
     * 
     * @return TODO
     */
    public static String map(String content) {
        Logger.log(null, "start on content: " + content.length());

        StringTokenizer stringTokenizerLINE = new StringTokenizer(content,
                "\t|\n");

        // lines
        while (stringTokenizerLINE.hasMoreElements()) {
            String line = stringTokenizerLINE.nextToken();
            Logger.log(null, line);
        }

        return null;
    }

    /**
     * TODO: 
     * 
     * @param url
     *            TODO
     * @param directory
     *            TODO
     */
    public void readFiles(URL url, File directory) {
        /*
         * try { url = new URL(path); debug("url created ");
         * org.setupx.repository.core.util.Util.wget(url); debug("downloaded"); }
         * catch (MalformedURLException e1) { throw new CommunicationException("the
         * URL is not valid ", e1); } catch (ConnectException e) { throw new
         * CommunicationException("error ", e); } try { // check URL
         * Util.checkURL(url); } catch (ConnectException e) { throw new
         * CommunicationException("the URL " + url.toExternalForm() + " - the server
         * can not be reached.", e); }
         */
        /*
         * File target = File.createTempFile("",""); try {
         * org.setupx.repository.core.util.File.download(url,target); } catch
         * (IOException e) { throw new CommunicationException("unable to download " +
         * url.toExternalForm() + " ", e); } try { file = new java.io.File(new
         * URI(url.toExternalForm())); } catch (URISyntaxException e2) { throw new
         * CommunicationException("the URL " + url.toExternalForm() + " is
         * invalid.", e2); }
         */
        File file = new File("C:\\taxdmp.zip");

        try {
            // open and unzip zip file
            Zip.unzipFile(file, directory);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
