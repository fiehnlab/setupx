package org.setupx.repository.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.setupx.repository.core.util.logging.Logger;

/**
 * util to cut large files into smaller ones - cuts the biginning of a single
 * large file and stores it in a defined directory. it starts with the first
 * 10MB, then stores the first 20MB, 30MB, ...
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 */
public class BigXMLReader {
    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //
    // Reading and writing an XML document stored in a file.
    //
    public static void main(String[] args) {
        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream(new File("C:\\tob.xml"));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        BufferedInputStream bufferedInputStream = new BufferedInputStream(
                inputStream);

        try {
            Logger.log(null, "" + bufferedInputStream.read());
            Logger.log(null, "size: " + bufferedInputStream.available());
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        int ch = 0;

        StringBuffer buffer = new StringBuffer();

        int count = 0;

        try {
            while ((ch = bufferedInputStream.read()) > -1) {
                count++;

                StringBuffer buf = new StringBuffer();
                buf.append((char) ch);
                // System.out.print(buf.toString());
                buffer.append(buf);

                if ((count % 10000000) == 0) {
                    Logger.log(null, "count: " + count);
                    org.setupx.repository.core.util.File.storeData2File(
                            new File("Z:\\Scholz\\4Tobias\\data"
                                    + (count / 10000) + ".txt"), buffer
                                    .toString());
                }
            }
        } catch (IOException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
    }
}
