package org.setupx.repository.util;

public class Link {

    private String img = "pics/vide.gif";
    private Class  relatedObjectClass;
    private String relatedObjectID;
    private String relatedObjectLink;
    private String label;

    public Link(String label, String relatedObjectLink) {
	this.label = label;
	this.relatedObjectLink = relatedObjectLink;
    }

    public Link(String label, String relatedObjectLink, String img) {
	this.label = label;
	this.relatedObjectLink = relatedObjectLink;
	this.img = img;
    }

    public Link(String label, Class relatedObjectClass, String relatedObjectID, String relatedObjectLink) {
	this.label = label;
	this.relatedObjectClass = relatedObjectClass;
	this.relatedObjectID = relatedObjectID;
	this.relatedObjectLink = relatedObjectLink;
    }

    public Class getRelatedObjectClass() {
	return this.relatedObjectClass;
    }

    public String getRelatedObjectID() {
	return this.relatedObjectID;
    }

    public String getRelatedObjectLink() {
	return this.relatedObjectLink;
    }

    public String getLabel() {
	return this.label;
    }

    public String getImg() {
	return this.img;
    }

    public void setImg(String img) {
	this.img = img;
    }

    public void setLabel(String label) {
	this.label = label;
    }

    public void setRelatedObjectClass(Class relatedObjectClass) {
	this.relatedObjectClass = relatedObjectClass;
    }

    public void setRelatedObjectID(String relatedObjectID) {
	this.relatedObjectID = relatedObjectID;
    }

    public void setRelatedObjectLink(String relatedObjectLink) {
	this.relatedObjectLink = relatedObjectLink;
    }

    public String toHTMLLink() {
	return "<a href=\"" + this.getRelatedObjectLink() + "\"><img  border='0' src='" + this.getImg() + "'></a>"
		+ this.getLabel();
    }

}
