package org.setupx.repository.util.dump;

import org.setupx.repository.core.CoreObject;

import java.util.StringTokenizer;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class DBvirtualTableEntry extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  String[] arguments = null;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new DBvirtualTableEntry object.
   *
   * @param line 
   */
  public DBvirtualTableEntry(String line) {
    StringTokenizer stringTokenizer2 = new StringTokenizer(line, "\t|\t");
    int size = stringTokenizer2.countTokens();

    arguments = new String[size];

    int i = 0;

    while (stringTokenizer2.hasMoreElements()) {
      arguments[i] = stringTokenizer2.nextToken();
      //System.out.print( " - " + arguments[i]);
      i++;
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public int getID() {
    return Integer.parseInt(this.arguments[0]);
  }

  /**
   * TODO: 
   *
   * @param args TODO
   */
  public static void main(String[] args) {
    new DBvirtualTableEntry("35	|	32	|	species	|	MM	|	0	|	1	|	11	|	1	|	0	|	1	|	1	|	0	|		|");
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String toString() {
    StringBuffer buffer = new StringBuffer();

    for (int i = 0; i < arguments.length; i++) {
      buffer.append(arguments[i] + " ");
    }

    return buffer.toString();
  }
}
