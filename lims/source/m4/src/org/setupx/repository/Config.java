
package org.setupx.repository;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

import org.setupx.repository.core.communication.importing.logfile.LogFileScanner2;
import org.setupx.repository.core.communication.mail.EMailAddress;
import org.setupx.repository.core.communication.mail.MailController;
import org.setupx.repository.core.communication.notification.NotificationCentral;
import org.setupx.repository.core.communication.notification.SystemNotification;
import org.setupx.repository.core.communication.status.StatusTrackerCache;
import org.setupx.repository.core.communication.technology.scanner.ScannerFactory;
import org.setupx.repository.core.query.QueryMaster;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.UserLocal;
import org.setupx.repository.core.user.access.PromtUserAccessRight;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.server.persistence.SXQueryCacheRefresher;

/**
 * Configuration for the whole system
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.25 $
 */
public final class Config {

    // --------------------------------- debug mode for promt creating --------------------------------
    public static final int PROMTCREATE_DEBUG_MODE = 325;

    // --------------------------------- system label .... --------------------------------
    public static final String SYSTEM_NAME = Settings.getValue("system.name");
    public static final String SYSTEM_EMAIL = Settings.getValue("system.email", "mscholz@ucdavis.edu");

    // ---------------------------------- directories
    public static final String DIRECTORY_MAIN = Settings.getValue("system.dir.main", File.separator + "lims"
            + File.separator);

    public static final String DIRECTORY_DOCUMENTOBJECTS = Settings.getValue("system.dir.documents", File.separator + "mnt"
            + File.separator + "gctof" + File.separator + "setupx" + File.separator + "documents");
    
    
    
    public static final String DIRECTORY_CONF = Settings.getValue("system.dir.conf", DIRECTORY_MAIN + "conf"
            + File.separator);
    public static final String DIRECTORY_CONF_HIBERNATE = Settings.getValue("hibernate.dir", DIRECTORY_CONF
            + "hibernate" + File.separator);
    public static final String CONF_HIBERNATE_FILE = Settings.getValue("hibernate.conffile", DIRECTORY_CONF_HIBERNATE
            + "hibernate.properties");
    public static final String DIRECTORY_EXPORT = Settings.getValue("system.dir.export", File.separator + "mnt"
            + File.separator + "gctof" + File.separator + "setupx");

    /** directory containing folders for each experiment */
    public static final String DIRECTORY_RELATED = Settings.getValue("system.dir.relatedfile", DIRECTORY_EXPORT
            + File.separator + "documents");
    public static final String DIRECTORY_BB_RESULTS = Settings.getValue("system.dir.resultfile",
            Config.DIRECTORY_EXPORT + java.io.File.separator + "result" + java.io.File.separator);

    /** directory containing all workflows */
    public static final String DIRECTORY_WORKFLOWS = DIRECTORY_CONF + "workflow";
    public static final String DIRECTORY_DATA = DIRECTORY_MAIN + File.separator + "data" + File.separator;

    /** directory for storing files */
    public static final String DIRECTORY_PERSISTENCE = DIRECTORY_DATA + File.separator + "persistence" + File.separator;

    /** directory containing the configuration files for the importer */
    public static final String configDirectory = DIRECTORY_DATA + File.separator + "converter" + File.separator
            + "mapping_machine" + File.separator;

    /** directory containing the acqtasks */
    public static final String DIRECTORY_ACQFILES = Settings.getValue("system.dir.acq", DIRECTORY_EXPORT);
    
    /** directory containing lcq sequences */
    public static final String DIRECTORY_LTQ_SEQ = Settings.getValue("system.dir.ltq.seq", DIRECTORY_EXPORT);

    /** directory containing lcq methods */
    public static final String DIRECTORY_LTQ_METHODS = Settings.getValue("system.dir.ltq.method", DIRECTORY_EXPORT);

    /** location of different datafiles for each sample */
    public static final String DIRECTORY_SAMPLEFILE_PEG = Settings.getValue("system.dir.sample.netpeg",
            "/mnt/gctof/ucdavis/data");
    public static final String DIRECTORY_SAMPLEFILE_SMP = Settings.getValue("system.dir.sample.netsmp",
            "/mnt/gctof/ucdavis/data");
    public static final String DIRECTORY_SAMPLEFILE_TXT = Settings.getValue("system.dir.sample.nettxt",
            "/mnt/binbase/data");
    public static final String DIRECTORY_SAMPLEFILE_CDF = Settings.getValue("system.dir.sample.netcdf", "/mnt/netcdf");
    
    public static final String DIRECTORY_SAMPLEFILE_XCDF = Settings.getValue("system.dir.sample.netcdf", DIRECTORY_SAMPLEFILE_CDF);

    public static final String SAMPLEFILE_EXTENSION_PEG = Settings.getValue("system.sample.extenion.peg", "peg");
    public static final String SAMPLEFILE_EXTENSION_TXT = Settings.getValue("system.sample.extenion.txt", "txt");
    public static final String SAMPLEFILE_EXTENSION_SMP = Settings.getValue("system.sample.extenion.smp", "smp");
    public static final String SAMPLEFILE_EXTENSION_CDF = Settings.getValue("system.sample.extenion.cdf", "cdf");
    public static final String SAMPLEFILE_EXTENSION_xCDF = Settings.getValue("system.sample.extenion.xcdf", "cdf.gz");

    // --------------------------------- dictionary .... --------------------------------

    /** directory containing all library (words) */
    public static final String DIRECTORY_LIBRARY = DIRECTORY_DATA + File.separator + "library";

    /** requiered similarity between two words to accept it - range is 0 to 256 */
    public static final int LIBRARY_SIMILARITY = 190;

    /** number of values that must be found - if it is decalred as known */
    public static final int GOOGLE_LIMIT = 7000;
    public static final String NCBI_DIRECTORY = Settings.getValue("system.dir.ncbi", DIRECTORY_DATA + File.separator
            + "taxonomy" + File.separator);
    public static final String GOOGLE_LICENSEKEY = Settings.getValue("system.key.google",
            "2eY+o71QFHIg0tgVo0ksYSV7hXNob9fb");

    // --------------------------------- mailserver .... --------------------------------
    public static boolean MAIL_ACTIVE = true;

    /* @deprecated */
    public static final String MAIL_SERVER = Settings.getValue("system.mail.smtpserver.name","smtp.gmail.com");
    
    public static final String MAIL_SERVER_NAME = Settings.getValue("system.mail.smtpserver.name","smtp.gmail.com");
    public static final String MAIL_SERVER_USERNAME = Settings.getValue("system.mail.smtpserver.username","setupx.mail@gmail.com");
    public static final String MAIL_SERVER_PASSWORD = Settings.getValue("system.mail.smtpserver.password","password");
    public static final int MAIL_SERVER_PORT = Settings.getValue("system.mail.smtpserver.port",465);

    
    // --------------------------------- webserver .... --------------------------------
    public static final String PROTOCOL_HTTP = "http://";

    /** ip of the webserver */
    public static final String WEB_SERVER_SERVER_IP = "128.120.136.209";
    public static final String WEB_SERVER_PORT = "80";
    public static final String PAGE_START = PROTOCOL_HTTP + WEB_SERVER_SERVER_IP + ":" + WEB_SERVER_PORT
            + "/m1/start.html";

    // "http://localhost:90/";
    // PROTOCOL_HTTP + WEB_SERVER_SERVER_IP + ":" + WEB_SERVER_PORT +
    // "/m1/index.jsp";
    public static final String IMG_START = PROTOCOL_HTTP + WEB_SERVER_SERVER_IP + ":" + WEB_SERVER_PORT
            + "/m1/pics/m1.jpg";
    public static final int WEB_SERVICE_TIMEOUT_STANDARD = 6000;

    // --------------------------------- databaseserver and databases --------------------------------

    /** ip od the databaseserver */
    public static final String DATABASE_SERVER_IP = "1.1.1.1";
    public static final String DATABASE_SERVER_PORT = "80";
    public static final String URLPATH_TAMINO = "/tamino/";

    /** directory that is beeing scanned and which contains the logfiles */
    public static final String DIRECTORY_LOGFILES_MACHINE = Settings.getValue("system.dir.gctof.logfiles","/mnt/gctof/ucdavis/samplelog");

    /** the final definition where the database is runnning */
    public static final String DATABASE_SERVER = DATABASE_SERVER_IP + ":" + DATABASE_SERVER_PORT;

    // Collections for the different databases
    public final static String TAMINO_SAMPLES_COLLECTION = "ino:etc";
    public final static String TAMINO_EXPERIMENT_COLLECTION = "ino:etc";
    public final static String TAMINO_SCHEMA_COLLECTION = "ino:etc";
    public static final boolean NEW_DESIGN = true;
    public final static String TAMINO_COLLECTION_M2_test = "ino:etc";
    public static final String EXP_DATABASE = "m3_exp";
    public static final String SCHEMA_DATABASE = "m3_schema";
    public static final String SAMPLES_DATABASE = "m3_samples";

    // --------------------------------- authentification - authorisation --------------
    public static final String KERBEROS_REALM = "UCDAVIS.EDU";
    public static final String KERBEROS_KDC = "AUTH2.UCDAVIS.EDU";
    public static final String DIRECTORY_PERSISTENCE_SX = DIRECTORY_PERSISTENCE + File.separator + "SetupX" + File.separator;
    public static final String FILE_LOGIN = "\\dev\\workspace\\m4\\src\\de\\mpg\\mpimp\\golm\\m1\\core\\user\\kerberos\\jaas.conf";

    // --------------------------------- external data .... --------------------------------
    public static final String FILE_TISSUE_DB = DIRECTORY_DATA + File.separator + "tissuedb" + File.separator
            + "tissue.html";

    /**
     * defines if the converted file will be stored in the db
     * 
     * @deprecated since splitting converter and importer
     */
    public static final boolean INCLUDE_STORING = false;

    /**
     * activate the coonverting in the importer tool - false means the existing, converted file is beeing stored in the
     * database
     * 
     * @deprecated since splitting converter and importer
     */
    public static final boolean INCLUDE_CONVERTING = true;

    /** activate testcases for m1 service - set false for debugging without a running server(offline) */
    public static final boolean TESTCASES_M1_ACTIVE = true;

    /** activate performacetests !!it removes all entries from all databases!! */
    public static final boolean TESTCASES_PERFORMANCE_ACTIVE = false;

    /**
     * defines the maximum number of WFElements
     * 
     * @see org.setupx.repository.server.workflow.WorkFlowManager#initProcessables()
     */
    public static final int DEFAULT_NUM_OF_WFELEMENTS = 100;

    // --------------------------------- logging .... --------------------------------
    public static final String UNKNOW_CLASS = " - - - - - ";

    /** activate the logging */
    public static final boolean LOGGING_ACITVE = Settings.getValue("logging.standard", true);
    public static final boolean LOGGING_ERROR = Settings.getValue("logging.err", true);
    public static final boolean LOGGING_INFO_ACITVE = Settings.getValue("logging.info", true);



    /** activate logging into a file */
    public static final boolean LOGGING2FILE_ACITVE = Settings.getValue("logging.standard.tofile", false);

    /** activate logging errors into a file */
    public static final boolean LOGGING_ERROR2FILE_ACITVE = Settings.getValue("logging.file.errors", false);
    public static final String FILE_LOG_ACCESS = Settings.getValue("logging.standard.file", DIRECTORY_DATA
            + File.separator + "access.log");
    public static final boolean LOGGING_SYSTEM_ACITVE = Settings.getValue("logging.system", false);
    public static final boolean LOGGING_SHOW_CONSTUCTOR_WARNINGS = Settings.getValue(
            "logging.form.constructor.warning", false);
    public static final boolean LOGGING_WARNING_MISSING_ROOT = Settings.getValue("logging.form.missingroot", false);
    public static final boolean LOGGING_SHOW_MISSING_ROOT = Settings.getValue("logging.form.missingroot", false);
    public static final boolean LOGGING_SHOW_MISSING_FIELDS = Settings.getValue("logging.form.missingfields", false);
    public static final boolean LOGGING_DB_REPORT = Settings.getValue("logging.form.dbreport", false);

    // --------------------------------- generel .... --------------------------------
    public static final boolean URLCHECK_ACTIVE = false;

    // --------------------------------- all files .... --------------------------------

    /**
     * directory containing files for import - and where all the files will be saved, that have been converted from ??
     * to m1s, m1e, m1m
     */
    public static final String FILES_IMPORT_DIRECTORY = DIRECTORY_MAIN + "import" + File.separator;

    /** directory containing all files in subfolders that have been imported */
    public static final String FILES_BACKUP_DIRECTORY = "\\m2\\importedFiles_backup";
    public static final String DIRECTORY_OBO = DIRECTORY_DATA + "obo" + File.separator;

    /** file containing the plant ontology */
    public static final String FILE_OBO_ANATOMY = DIRECTORY_OBO + File.separator + "po_anatomy.obo";

    /** filename for converted file */
    public static final String FILE_RESULT = DIRECTORY_MAIN + "test-result.xml";

    /** filename containing the metainformations that will be imported */
    public static final String sourceFile = DIRECTORY_MAIN + "log.csv";

    /** filename for error-converting-file */
    public static final String errorFile = DIRECTORY_MAIN + "test_err.csv";

    /** filename for userfile, containing all users */
    public static final String IMPORT_FILENAME_USERS = DIRECTORY_CONF + "users.xml";

    /** tag if the file with users will be read or not */
    public static final boolean FILE_USERS_ACTIVE = false;

    /** filename for error-log-file */
    public static final String IMPORT_FILENAME_ERROR = DIRECTORY_MAIN + "error.log";

    /** filename for access-log-file */
    public static final String IMPORT_FILENAME_ACCESS = DIRECTORY_MAIN + "access.log";

    /** filename for logging-files */
    public static final String IMPORT_FILENAME_LOG = Settings.getValue("logging.file.import", DIRECTORY_MAIN + "log.log");

    /** jdbc driver for the SAP-db */
    public final static String SAP_JDBC_Drivername = "com.sap.dbtec.jdbc.driver.sapdb";

    /** username for using the sap database */
    public final static String SAP_user = "dba";

    /** password for using the sap database */
    public final static String SAP_password = "dba";

    /**
     * @label operator doing the preperatio
     */
    public static EMailAddress OPERATOR_PREPERATION = new EMailAddress(Settings.getValue("system.operator.prep.email",
            "mscholz@ucdavis.edu"));
    public static EMailAddress OPERATOR_MANAGER = new EMailAddress(Settings.getValue("system.operator.manager.email",
            "mscholz@ucdavis.edu"));
    public static EMailAddress OPERATOR_MEASUREMENT = new EMailAddress(Settings.getValue("system.operator.lab.email",
            "mscholz@ucdavis.edu"));
    public static EMailAddress OPERATOR_ADMIN = new EMailAddress(Settings.getValue("system.operator.admin.email",
            "mscholz@ucdavis.edu"));
    private static final String OPERATOR_ADMIN_PASSWORD = Settings.getValue("system.operator.admin.email", "northstar");

    // --- NEW VERSION

    /** file containing the messages shown in webgui and client */
    public static final String FILE_MESSAGES = DIRECTORY_CONF + "messages.xml";

    /** file containing the users shown in webgui and client */
    public static final String FILE_USERS = DIRECTORY_CONF + "users.xml";

    /** default domain for LDAP access */
    public static String DEFAULT_DOMAIN = "mpimp-golm.mpg.de";
    public static URL TAMINO_DEFAULT_DATABASENAME_M2_SAMPLES = null;

    // public static URL TAMINO_DEFAULT_DATABASENAME_M2_SCHEMA = null;
    public static URL TAMINO_DEFAULT_DATABASENAME_M2_EXP = null;
    private static Object thiZ = new Config();

    public static final boolean SAMPLE_FIELD_AUTOCORRECTION = true;
    public static final String FTP_SYSTEM_USERNAME = Settings.getValue("system.binbase.ftp.user", "setupx");
    public static final String FTP_SYSTEM_PASSWORD = Settings.getValue("system.binbase.ftp.pass", "northstar");

    public static final String WEB_SERVICE_ACCESSCODE = "987$$%97667234";

    /** time how often the status has to be refreshed */
    public static final long STATUS_REFRESH = Settings.getValue("system.status.autoupdate", 120000);

    /** defines the last possible id of an experiment that can be loaded / modify if deprecated experiments are stored in the system.*/
    public static final long PROMT_OUTDATED_ID = Settings.getValue("system.promt.outdated.id", 318159);

    public static final String WEB_SERVICE_VALID_IP1 = Settings.getValue("system.webservice.ip1", "127.0.0.1");
    public static final String WEB_SERVICE_VALID_IP2 = Settings.getValue("system.webservice.ip2", "127.0.0.1");
    public static final String WEB_SERVICE_VALID_IP3 = Settings.getValue("system.webservice.ip3", "127.0.0.1");

    public static final String BINBASE_SERVER_PASSKEY = Settings.getValue("system.binbase.passkey", "11ab7969acb11ab7969acb");

    public static final boolean QUERY_CACHING_ACTIVE_JSP = Settings.getValue("system.query.caching.jsp", false);
    public static final boolean QUERY_CACHING_ACTIVE_SXQUERY = Settings.getValue("system.query.caching.sxquery", false);
    public static final long QUERY_CACHING_REFRESH = Settings.getValue("system.query.caching.refreshing", 20000);

    public static final long FILESCANNER_AUTOASSIGNED_MIN_SAMPLEID = Settings.getValue("system.scanner.minsampleid", 20);

    public static final boolean FILESCANNER_GCSCANNER_ACTIVE = Settings.getValue("system.scanner.gcfilescanner.active", true);
    public static final boolean FILESCANNER_TECHNOSCANNER_ACTIVE = Settings.getValue("system.scanner.technofilescanner.active", true);

    // pause in ms
    public static final long FILESCANNER_PAUSE =  Settings.getValue("system.scanner.technofilescanner.pause", 240000);

    public static final boolean FILESCANNER_AUTOASSIGNED_DEFAULT = Settings.getValue("system.scanner.technofilescanner.automaticassignment.default", true);



    /**
     * init the constants
     */
    static {
        System.out.println("start Config.java");

        try {
            init();
        } catch (Throwable e) {
            e.printStackTrace();
            // NotificationCentral.notify(new SystemNotification("startup problem", e));
        }

    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a HTML representation. The representation is a <code>table</code> that can be pasted in any HTML file.
     */
    public static String getHTMLConfig() throws IllegalArgumentException, IllegalAccessException {
        StringBuffer buffer = new StringBuffer("<table><tr>");
        Config config = new Config();
        Field[] fields = Config.class.getDeclaredFields();

        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];

            buffer.append("<tr><td><b>" + field.getName() + "</b></td><td>").append(field.get(config) + "       </td></tr>");
        }

        buffer.append("</table>");

        return buffer.toString();
    }

    private static URL initBBURL() {
        try {
            return new URL(Settings.getValue("system.binbase.ws.url"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;    
    }

    /**
     * doing nothing - used to start the static constructor
     */
    public static void callTheStaticConstructor() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#finalize()
     */
    public void finalize() {
        NotificationCentral.notify(new SystemNotification("shutting down"));
        // Mail.postMail(new String[] { Config.OPERATOR_ADMIN.getEmailAddress() }, "[" + SYSTEM_NAME + "] shutting
        // down", "shutting down ", SYSTEM_EMAIL);
    }

    /**
     * TODO: 
     */
    public static void initCoreUsers() {
        Logger.log(thiZ, "init admin user");

        try {
            UserDO userDO = null;

            String[] ids = new String[] { "public", Config.OPERATOR_ADMIN.toString() };
            String[] pws = new String[] { "public", Config.OPERATOR_ADMIN_PASSWORD };
            String[] ems = new String[] { Config.SYSTEM_EMAIL, Config.OPERATOR_ADMIN.toString() };
            String[] orga = new String[] { "public user", "Administrator" };
            boolean[] adm = new boolean[] { false, true };
            boolean[] lab = new boolean[] { false, true };

            for (int i = 0; i < ids.length; i++) {
                try {
                    userDO = UserDO.load(ids[i], pws[i]);
                } catch (PersistenceActionFindException e1) {
                    // e1.printStackTrace();
                    // in case user was not found create a new one with the defined parameters
                    userDO = new UserLocal(ids[i], pws[i], OPERATOR_ADMIN);
                    userDO.setMailAddress(new EMailAddress(ems[i]));
                    userDO.setOrganisation(orga[i]);
                    userDO.setAdmin(adm[i]);
                    userDO.setLabtechician(lab[i]);

                    userDO.update(true);

                    Logger.debug(thiZ, "created user labperson: + " + userDO.isLabtechician());
                    Logger.debug(thiZ, "created user admin    : + " + userDO.isAdmin());
                    Logger.debug(thiZ, "created user email    : + " + userDO.getEmailString());

                    Logger.log(thiZ, "created user: + " + userDO.toString());
                }
            }
        } catch (PersistenceActionUpdateException e) {
            throw new RuntimeException(
                    "unable to start the system: not able to create admin account - check database and mapping.");
        }

        // init access rights to sx user.
        // makeing sure that admin user has full access to all experiments
        try {
            java.util.List list = new SXQuery().findPromtIDs();
            Iterator iterator = list.iterator();
            long sxUserID = UserDO.load(Config.OPERATOR_ADMIN.toString(), Config.OPERATOR_ADMIN_PASSWORD).getUOID();

            while (iterator.hasNext()) {
                long promptID = Long.parseLong("" + iterator.next());
                PromtUserAccessRight.createFullAccess(promptID, sxUserID);
                Logger.log(thiZ, "created new PromtUserAccessRight: " + promptID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @throws ConfigurationException
     *                 TODO
     */
    private static void init() {

        try {
            // init an xml parser - checking that there is one!
            Util.initXMLParser();
        } catch (Exception e) {
            Logger.warning(thiZ, "err init xmlparser ", e);
        }

        try {
            // init the quermaster
            QueryMaster.reinitCache();
        } catch (Exception e) {
            Logger.warning(thiZ, "err init xmlparser ", e);
        }

        try {
            // init the tracking cache
            StatusTrackerCache.init();
        } catch (Exception e) {
            Logger.warning(thiZ, "err init xmlparser ", e);
        }

        try {
            // init the tracking cache
            SXQueryCacheRefresher.init();
        } catch (Exception e) {
            Logger.warning(thiZ, "err init xmlparser ", e);
        }


        try {
            // start the logfilescanner
            LogFileScanner2.init();
        } catch (Exception e) {
            Logger.warning(thiZ, "err init xmlparser ", e);
        }

        try {
            // new techo based logfilescanner 
            ScannerFactory.init();
        } catch (Exception e) {
            Logger.warning(thiZ, "err init xmlparser ", e);
        }

        try {
            // new techo based logfilescanner 
            MailController.init();
        } catch (Exception e) {
            Logger.warning(thiZ, "err init xmlparser ", e);
        }
        

        Logger.info(thiZ, "registrating SOAP webservices ");
        try {
            Runtime.getRuntime().exec("ant -f " + DIRECTORY_MAIN + File.separator + "source/m4/build.xml register")
            .getInputStream();
        } catch (IOException e) {
            Logger.warning(thiZ, " -------------------------------");
            Logger.warning(thiZ, " -- CAN NOT START WEBSERVICES --");
            Logger.warning(thiZ, " -------------------------------");
            Logger.warning(thiZ, " err: WEBSERVICES " , e);
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        System.out.println("--> " + SYSTEM_NAME);
        System.out.println("--> " + Settings.allStoredSettings());

        System.out.println("-- public contants:" + getConstants());
    }

    /**
     * creating a message containing all contants. <br>
     * done by reflection.
     * 
     * @return
     */
    private static String getConstants() {
        StringBuffer buffer = new StringBuffer();

        // get all constants for this.
        Class class1 = thiZ.getClass();
        Field[] fields = class1.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            buffer.append("\n").append(field.getName());
        }
        return buffer.toString();
    }
}
