/**
 * ============================================================================ File:    NCICBOrganRestriction.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.core.communication.ncbi.NCICBException;
import org.setupx.repository.core.communication.ncbi.NCICB_Result;
import org.setupx.repository.core.communication.ncbi.NCICB_connector;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.ValidationAnswer;
import org.setupx.repository.web.forms.validation.ValidationException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @hibernate.subclass
 */
public class NCICBOrganRestriction extends Restriction {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new NCICBOrganRestriction object.
   */
  public NCICBOrganRestriction() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#cloneRestriction()
   */
  public Restriction cloneRestriction() {
    return new NCICBOrganRestriction();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#validateObject()
   */
  public ValidationAnswer validateObject(InputField field)
    throws ValidationException {
    NCICB_Result result = null;

    try {
      result = NCICB_connector.query("" + field.getValue());
    } catch (NCICBException e) {
      e.printStackTrace();
    }

    if ((result == null) || (result.getName() == null) || (result.getId() == null)) {
      return new ValidationAnswer(field, "unable to find organ on NCICB");
    }

    return null;
  }
}
