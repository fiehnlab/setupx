package org.setupx.repository.web.forms.inputfield;

import java.io.File;

import org.setupx.repository.core.InitException;
import org.setupx.repository.core.communication.ontology.obo.OBOConnector;
import org.setupx.repository.core.communication.ontology.obo.OBOFinderException;
import org.setupx.repository.core.communication.ontology.obo.OBOTerm;
import org.setupx.repository.web.forms.restriction.OBORestriction;
import org.setupx.repository.web.forms.restriction.Restriction;

/**
 * {@link InputField} which is a regular {@link StringInputfield} with the Extension of showing content from the related OBO-Ontology.
 * 
 * @see OBOConnector#determineTerm(String)
 * 
 * @hibernate.subclass
 */
public class OBOStringInputfield extends StringInputfield {

    /**
     * default constructor requiered for hibernate persistence
     */
    public OBOStringInputfield(){
        super();
    }

    /** 
     * @see StringInputfield
     */
    public OBOStringInputfield(String question, String desctiption){
        super(question, desctiption);
    }

    /** 
     * @see StringInputfield
     */
    public OBOStringInputfield(String question, String descr, boolean remove ){
        super(question, descr,remove );
    }

    /** 
     * @see StringInputfield
     */
    public OBOStringInputfield(String question, String descr, String answer, boolean remove ){
        super(question, descr, answer, remove );
    }

    /**
     * Adds obo information.
     * {@link OBORestriction}
     * @see OBORestriction
     * @see org.setupx.repository.web.forms.inputfield.StringInputfield#createHTML(int)
     * @see OBORestriction
     */
    public String createHTML(int col) {
        // get the restriction which should be an OBORestriction.
        Restriction restriction = this.getRestriction();

        // in case it is an oboRestriction
        if (restriction instanceof OBORestriction) {
            OBORestriction oboRestriction = (OBORestriction) restriction;

            // take the sourcefile of the restriction 
            String oboSourceString = oboRestriction.getSource();

            OBOTerm oboTerm;
            try {
                OBOConnector oboConnector = OBOConnector.newInstance(new File(oboSourceString));
                oboTerm = oboConnector.determineTerm("" + this.getValue());

                StringBuffer buffer = new StringBuffer();

                buffer.append(super.createHTML(col));
                String termDef = oboTerm.getDefinition();

                if (termDef != null && termDef.length() > 2){
                    buffer.append("<td " + createBackgroudcolor() + ">Details");
                    buffer.append("<br><font size=\"-3\">&nbsp &nbsp &nbspInformation from the <a href=\"http://obofoundry.org/\"\">OBOntology</a></font></span></td>");
                    buffer.append("\n<td " + createBackgroudcolor() + " colspan=\"" + (col - 1) + "\">");

                    //buffer.append("<font class=\"small\">" +  oboTerm.getTerm() + "<br>") ;
                    buffer.append(termDef);
                    //buffer.append(oboTerm.getId()+ "<br>");
                    buffer.append("</td></tr>\n");
                }
                return buffer.toString();

            } catch (OBOFinderException e) {
                e.printStackTrace();
                return super.createHTML(col);
            } catch (InitException e) {
                e.printStackTrace();
                return super.createHTML(col);
            }
        } else {
            return super.createHTML(col);
        }
    }


    /**
     * overwriting the helpbutton in order to provide a navigation button for the ontology.
     */
    protected String createHelpButton() {
        Restriction restriction = this.getRestriction();

        // in case it is an oboRestriction
        if (restriction instanceof OBORestriction) {
            OBORestriction oboRestriction = (OBORestriction) restriction;

            // take the sourcefile of the restriction 
            String oboSourceString = oboRestriction.getSource();
            return "<a href=\"" + this.createNavLink(oboSourceString) + "\">" + "<img  border='0' src=\"pics/obo_lookup.png\"/>" + "</a>  ";
        } else { 
            return super.createHelpButton();
        }
    }



    /**
     * create a link that links to a navigation through the ontologie
     * @param oboConnector
     * @return a link
     */
    private String createNavLink(String oboFile) {
        // cutting the path away
        oboFile = oboFile.substring(oboFile.lastIndexOf('/') +1 , oboFile.length());
        return "oboNav.jsp?obo=" + oboFile + "&field=" + this.getName() + "&term=" + this.getValue();
    }

}
