/**
 * ============================================================================ File:    FilesMultiField.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import java.io.File;
import java.util.Date;

import org.hibernate.Session;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.web.DownloadServlet;
import org.setupx.repository.web.forms.FormRootNotAvailableException;
import org.setupx.repository.web.forms.InActiveException;


/**
 * Implementation of Formcontainer.
 * 
 * <p>
 * It does not have an own value.
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.23 $
 *
 * @hibernate.subclass
 */
public class FilesMultiField extends MultiField {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new FilesMultiField object.
   */
  public FilesMultiField() {
    super();
  }

  /**
   * Creates a new FilesMultiField object.
   *
   * @param question 
   * @param description 
   */
  public FilesMultiField(String question, String description) {
    super(question, description);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTMLChilds(int)
   */
  public String createHTMLChilds(int numberOfColums) {
    try {
      return createHTMLChilds(this.root().getUOID(), numberOfColums);
    } catch (FormRootNotAvailableException e) {
      e.printStackTrace();

      return "";
    }
  }

  /**
   * TODO: 
   *
   * @param id TODO
   * @param numberOfColums TODO
   * @param l
   *
   * @return TODO
   */
  public static String createHTMLChilds(long id, int numberOfColums) {
    // determine all related files 
    File[] files = Promt.determineRelatedFields(id);

    StringBuffer buffer = new StringBuffer();

    for (int i = 0; files != null && i < files.length; i++) {
      File file = files[i];
      buffer.append(toHTML(file, (numberOfColums - 1), id));
    }

    return buffer.toString();
  }

  /**
   * TODO: 
   *
   * @param file TODO
   * @param cols TODO
   * @param promtID
   *
   * @return TODO
   */
  public static final String toHTML(File file, int cols, long promtID) {
    String filename = file.getName();
    long size = file.length();
    Date date = new Date(file.lastModified());

    StringBuffer buffer = new StringBuffer();
    buffer.append("\n<!-- file: " + filename + " -->\n<tr>\n");
    buffer.append("<td>" + filename + "</td>");
    buffer.append("<td><font size=\"-3\">&nbsp &nbsp &nbsp Size:" + size + "</font></span></td>");
    buffer.append("<td><font size=\"-3\">&nbsp &nbsp &nbsp last modified:" + date.toLocaleString() + "</font></span></td>");

    buffer.append("\n<td>");
    buffer.append("<a href=\"" + DownloadServlet.createDownloadLink(file.getName(), "" + promtID) + "\"><img src='" + org.setupx.repository.core.util.File.determineFileIMG(file) + "' border=\"0\" valign=\"middle\" align=\"center\"></a> ");
    buffer.append("\n</td>");

    buffer.append("\n<td colspan=\"" + (cols - 3) + "\">");
    buffer.append("\n</td>");
    buffer.append("</tr>");

    return buffer.toString();
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.CoreObject#persistenceChilds(org.hibernate.Session, boolean)
   */
  public void persistenceChilds(Session session, boolean createIt)
    throws PersistenceActionException {
  }
  
  
  public Node createXMLNode(Document doc) throws InActiveException {
      warning(this, "not xml exported.");
        return null;
  }

}
