/**
 * ============================================================================ File:    Summary.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import java.util.Hashtable;

import org.w3c.dom.Document;
import org.w3c.dom.Node;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 *
 * @hibernate.subclass
 */
public class Summary extends FormObject {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   */
  public Summary() {
    super("", "");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#getAttributes()
   */
  public Hashtable getAttributes() {
    return null;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTML(int)
   */
  public String createHTML(int numberOfTD) {
    try {
      // creating summary of the whole experiment
      return this.root().toStringTree();
    } catch (FormRootNotAvailableException e) {
      err("unable to create HTML - root element can not be detected.", e);

      return "";
    }
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#toStringHTML()
   */
  public String toStringHTML() {
    warning(new UnsupportedOperationException("toStringHTML - not supported !"));

    return "";
  }
  
  public Node createXMLNode(Document doc) throws InActiveException {
      warning(this, "not xml exported.");
        return null;
  }

}
