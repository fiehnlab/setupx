// Copyright (C) 1998-2001 by Jason Hunter <jhunter_AT_acm_DOT_org>.
// All rights reserved.  Use of this class is limited.
// Please see the LICENSE for more information.
package org.setupx.repository.web;

import java.io.File;


// A class to hold information about an uploaded file.
//
class UploadedFile {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String dir;
  private String filename;
  private String original;
  private String type;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  UploadedFile(String dir, String filename, String original, String type) {
    this.dir = dir;
    this.filename = filename;
    this.original = original;
    this.type = type;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getContentType() {
    return type;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public File getFile() {
    if ((dir == null) || (filename == null)) {
      return null;
    } else {
      return new File(dir + File.separator + filename);
    }
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getFilesystemName() {
    return filename;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getOriginalFileName() {
    return original;
  }
}
