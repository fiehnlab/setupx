/**
 * ============================================================================ File:    MachineRunEvent.java Package: org.setupx.repository.web.forms.event cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.event;

import org.setupx.repository.core.communication.exporting.Machine;

import java.util.Date;


/**
 * @hibernate.subclass
 */
public class MachineRunEvent extends Event {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Date date;
  private Machine machine;
  private String folder;
  private String name;
  private String status;
  private String type;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new MachineRunEvent object.
   */
  public MachineRunEvent() {
    super();
  }

  /**
   * Creates a new MachineRunEvent object.
   *
   * @param i_machine 
   * @param i_name 
   * @param i_date 
   * @param i_status 
   * @param i_folder 
   * @param i_type 
   */
  public MachineRunEvent(Machine i_machine, String i_name, Date i_date, String i_status, String i_folder, String i_type) {
    super("run on " + i_date.toLocaleString());
    setName(i_name);
    setMachine(i_machine);
    setFolder(i_folder);
    setStatus(i_status);
    setType(i_type);
    setDate(i_date);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param date TODO
   */
  public void setDate(Date date) {
    this.date = date;
  }

  /**
   * @hibernate.property
   */
  public Date getDate() {
    return date;
  }

  /**
   * TODO: 
   *
   * @param folder TODO
   */
  public void setFolder(String folder) {
    this.folder = folder;
  }

  /**
   * @hibernate.property
   */
  public String getFolder() {
    return folder;
  }

  /**
   * TODO: 
   *
   * @param machine TODO
   */
  public void setMachine(Machine machine) {
    this.machine = machine;
  }

  /**
   * hibernate.many-to-one column="MACHINE" class = "org.setupx.repository.core.communication.exporting.Machine"
   */
  public Machine getMachine() {
    return machine;
  }

  /**
   * TODO: 
   *
   * @param name TODO
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @hibernate.property
   */
  public String getName() {
    return name;
  }

  /**
   * TODO: 
   *
   * @param status TODO
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @hibernate.property
   */
  public String getStatus() {
    return status;
  }

  /**
   * TODO: 
   *
   * @param type TODO
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @hibernate.property
   */
  public String getType() {
    return type;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.event.Event#toString()
   */
  public String toString() {
    try {
      return super.toString() + "sample ran on machine: " + this.getMachine().getMachineLetter() + "  on " + this.getDate().toGMTString();
    } catch (NullPointerException e) {
      warning(e);

      return super.toString();
    }
  }
}
