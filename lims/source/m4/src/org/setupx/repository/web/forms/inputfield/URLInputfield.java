/**
 * ============================================================================ File:    URLInputfield.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import org.setupx.repository.web.forms.restriction.URLRestriction;


/**
 * Inputfield for URLs
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 *
 * @hibernate.subclass
 */
public class URLInputfield extends StringInputfield {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new URLInputfield object.
   */
  public URLInputfield() {
    this("...", "...");
  }

  /**
   * Creates a new URLInputfield object.
   *
   * @param question 
   * @param description 
   */
  public URLInputfield(String question, String description) {
    super(question, description);
    this.setRestriction(new URLRestriction());
    this.setHelptext("Please define a URL.<br>A valid URL would be <code>http://fiehnlab.ucdavis.edu</code>");
    this.setValue("http://fiehnlab.ucdavis.edu");
    this.setHelptext(
      "Uniform Resource Locator. A way of specifying the location of something on the Internet, eg, \"http://www.photo.net/wtr/thebook/glossary.html\" is the URL for this glossary. The part before the colon specifies the protocol (HTTP). Legal alternatives include encrypted protocols such as HTTPS and legacy protocols such as FTP, news, gopher, etc. The part after the \"//\" is the server hostname (\"photo.net\"). The part after the next \"/\" is the name of the file on the remote server.");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.StringInputfield#cloneField()
   */
  public InputField cloneField() {
    URLInputfield clone = new URLInputfield(this.getQuestion(), this.getDescription());
    clone.setValue(this.getValue());

    return clone;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHelpButton()
   */
  protected String createHelpButton() {
    String superButton = super.createHelpButton();
    String linkbutton = "";

    if ((this.validate() != null) && (this.validate().length() > 1)) {
      linkbutton = "";
    } else {
      String link = (String) this.getValue();
      linkbutton = "<a href=\"" + link + "\"	target=\"extern\" alt=\"" + link + "\" \">" + linkIMG + "</a>  ";
    }

    return linkbutton + superButton;
  }
}
