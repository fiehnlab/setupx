/**
 * ============================================================================ File:    DropDown.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.web.forms.FormContainer;
import org.setupx.repository.web.forms.relation.Relation;
import org.setupx.repository.web.forms.restriction.DDRestriction;
import org.setupx.repository.web.forms.restriction.Restriction4StringLength;

import org.hibernate.Session;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * Simple Drop down
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.21 $
 *
 * @hibernate.subclass
 */
public class DropDown extends InputField {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Set keyValuePairs = new HashSet();
  private String submitOnChange = "";
  private int size = 1;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String STRING_OTHER = "--other--";
  private static final String DEFAULT_MAKE_SELECTION = "-- make a selection --";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new DropDown object.
   *
   * @param question 
   * @param description 
   * @param values 
   * @param submitIfChanged 
   */
  public DropDown(String question, String description, String[] values, boolean submitIfChanged) {
    this(question, description, values, values, submitIfChanged);
  }

  /**
   * Creates a new DropDown object.
   *
   * @param question 
   * @param description 
   * @param keys 
   * @param values 
   * @param submitIfChanged 
   */
  public DropDown(String question, String description, String[] keys, String[] values, boolean submitIfChanged) {
    super(question, description);

    this.setValue(DEFAULT_MAKE_SELECTION);

    if (submitIfChanged) {
      this.submitOnChange = "onChange=\"document.forms[0].submit()\"";
    }

    this.keyValuePairs = new HashSet();

    for (int i = 0; i < keys.length; i++) {
      debug("adding new pair: " + keys[i] + " : " + values[i]);
      this.keyValuePairs.add(new KeyValuePair(keys[i], values[i]));
    }

    // adding a default Restriction - prevents it from staying in default selection
    this.setRestriction(new DDRestriction(DEFAULT_MAKE_SELECTION));
  }

  /**
   * @deprecated just for peristencelayer
   */
  public DropDown() {
    this("", "", new String[] {  }, true);
  }

  /**
   * @param string
   * @param string2
   * @param hashtable
   * @param b
   */

  /*public DropDown(String string, String string2, Hashtable hashtable, boolean b) {
     this(string, string2, keys(hashtable), values(hashtable), b);
     }*/
  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.AbstractDropDown#setKeys(java.lang.String[])
   */

  /**
   * @param question
   * @param description
   * @param set
   * @param submitOnChanged
   */
  public DropDown(String question, String description, Set set, boolean in_submitOnChanged) {
    super(question, description);

    this.setValue(DEFAULT_MAKE_SELECTION);

    if (in_submitOnChanged) {
      this.submitOnChange = "onChange=\"document.forms[0].submit()\"";
    }

    this.setValuesHibernate(set);

    // adding a default Restriction - prevents it from staying in default selection
    this.setRestriction(new DDRestriction(DEFAULT_MAKE_SELECTION));
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param size TODO
   */
  public void setSize(int size) {
    this.size = size;
  }

  /**
   * @hibernate.property
   */
  public int getSize() {
    return size;
  }

  /**
   * TODO: 
   *
   * @param submitOnChange TODO
   */
  public void setSubmitOnChangeDD(String submitOnChange) {
    this.submitOnChange = submitOnChange;
  }

  /**
   * @hibernate.property
   */
  public String getSubmitOnChangeDD() {
    return submitOnChange;
  }

  /**
   * creates and <b>appends</b> a dropdownfield to the Field
   *
   * @return a reference to the generated field that is shown in case none of the values was made
   */
  public static BigStringInputField create(FormContainer con, String question, String description, String helptext, String[] values) {
    // add "other" to values
    values = org.setupx.repository.core.util.Util.expand(values, DropDown.STRING_OTHER);

    // create and add the DD
    DropDown down = new DropDown(question, description, values, true);
    con.addField(down);
    down.setHelptext(helptext);

    // create the big comment field
    BigStringInputField bigStringInputField = new BigStringInputField("detailed", "please specify here");
    con.addField(bigStringInputField);

    bigStringInputField.setRestriction(new Restriction4StringLength(20));

    Relation relation = new Relation(new String[] { DropDown.STRING_OTHER }, bigStringInputField);

    down.addRelation(relation);

    return bigStringInputField;
  }

  /**
   * differnece - return value is the dd field creates and <b>appends</b> a dropdownfield to the Field
   *
   * @return a reference to the generated field that is shown in case none of the values was made
   */
  public static DropDown createDown(FormContainer con, String question, String description, String helptext, String[] values) {
    // add "other" to values
    values = org.setupx.repository.core.util.Util.expand(values, DropDown.STRING_OTHER);

    // create and add the DD
    DropDown down = new DropDown(question, description, values, true);
    con.addField(down);
    down.setHelptext(helptext);

    // create the big comment field
    BigStringInputField bigStringInputField = new BigStringInputField("detailed", "please specify here");
    con.addField(bigStringInputField);

    bigStringInputField.setRestriction(new Restriction4StringLength(20));

    Relation relation = new Relation(new String[] { DropDown.STRING_OTHER }, bigStringInputField);

    down.addRelation(relation);

    return down;
  }

  /**
   * TODO: 
   *
   * @param keyValues TODO
   */
  public void setValuesHibernate(Set keyValues) {
    this.keyValuePairs = keyValues;

    /*Iterator iterator = keyValues.iterator();
       int i = 0;
       this.keys = new String[keyValues.size()];
       this.values = new String[keyValues.size()];
    
       while (iterator.hasNext()) {
         KeyValuePair pair = (KeyValuePair) iterator.next();
         this.keys[i] = pair.getInternKey();
         this.values[i] = pair.getInternValue();
         i++;
       }
     */
  }

  /**
   * @hibernate.set
   * @hibernate.collection-key column="key_values"
   * @hibernate.collection-one-to-many  class="org.setupx.repository.web.forms.inputfield.KeyValuePair"
   */
  public Set getValuesHibernate() {
    return this.keyValuePairs;
  }

  /**
   * @param valuesHibernate
   *
   * @return
   */
  public final static Set clone(Set valuesHibernate) {
    Set set = new HashSet();
    Iterator iterator = valuesHibernate.iterator();

    while (iterator.hasNext()) {
      KeyValuePair pair = (KeyValuePair) iterator.next();
      KeyValuePair pair_clone = pair.cloneSet();
      set.add(pair_clone);
    }

    return set;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public InputField cloneField() {
    DropDown dropDown = this.newInstance();
    dropDown.setRestriction(this.getRestriction().cloneRestriction());
    dropDown.setSize(this.size);

    return dropDown;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTML(int)
   */
  public String createHTML(int col) {
    StringBuffer buffer = new StringBuffer();

    buffer.append("<!-- " + Util.getClassName(this) + " -->\n<tr>\n" + "<td>" + this.getQuestion() + this.createRequieredNote() + " <BR/><font size=\"-3\">&nbsp &nbsp &nbsp " + this.getDescription() + "</font>");
    buffer.append("</td>\n" + "<td colspan=" + (col - 1) + ">");
    buffer.append(this.createAnchor());
    buffer.append("<select name=\"" + this.getName() + "\" size=" + getSize() + " " + this.submitOnChange + "  >\n");

    String tempValue = (String) this.getValue();

    // the default selection on top
    if (tempValue.compareTo(DEFAULT_MAKE_SELECTION) == 0) {
      buffer.append("<option selected>");
      buffer.append(DEFAULT_MAKE_SELECTION);
      buffer.append("</option> /n");
    }

    String selection = "";
    Iterator iterator = this.getValuesHibernate().iterator();

    while (iterator.hasNext()) {
      KeyValuePair pair = (KeyValuePair) iterator.next();

      // checking if this optionstring is selected
      if (tempValue.compareTo(pair.getInternValue()) == 0) {
        selection = "selected";
      } else {
        selection = "";
      }

      buffer.append("<option " + selection + " value=\"" + pair.getInternValue() + "\">");
      buffer.append(pair.getInternKey());
      buffer.append("</option> /n");
    }

    buffer.append("</select>\n\n");
    buffer.append(this.createHelpButton());
    buffer.append(this.createErrorMSG());
    buffer.append("</td></tr>\n");

    return buffer.toString();
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public DropDown newInstance() {
    return new DropDown(this.getQuestion(), this.getDescription(), clone(this.getValuesHibernate()), getSubmitOnChanged());
  }

  /**
   * TODO: 
   *
   * @param map TODO
   *
   * @return TODO
   */

  /*
     private static String[] keys(Set set) {
       Iterator iterator = set.iterator();
       String[] strings = new String[set.size()];
       int i = 0;
       while (iterator.hasNext()) {
         String element = ((KeyValuePair) iterator.next()).getInternKey();
         strings[i] = element;
         i++;
       }
       return strings;
     }*/
  /* (non-Javadoc)
   * @see org.setupx.repository.core.CoreObject#persistenceChilds(org.hibernate.Session, boolean)
   */
  public void persistenceChilds(Session session, boolean createIt)
    throws PersistenceActionException {
    Iterator iterator = this.getValuesHibernate().iterator();
    debug("saving KeyValuePairs");

    while (iterator.hasNext()) {
      if (CoreObject.SHOW_PERSISTENCE_LOG){
          debug("saving child");
      }
      ((KeyValuePair) iterator.next()).update(session, createIt);
    }

    super.persistenceChilds(session, createIt);
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  protected boolean getSubmitOnChanged() {
    return (this.submitOnChange.compareTo("") != 0);
  }
}
