/**
 * ============================================================================ File:    ParentIDLabel.java Package: org.setupx.repository.web.forms.label cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.label;

import org.setupx.repository.web.forms.FormObject;


/**
 * Label showing the Parent ID. The Label determines the parent object and shows the parent objects ID
 *
 * @hibernate.subclass
 */
public class ParentIDLabel extends DynamicLabel {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @deprecated only for persistencelayer
   */
  public ParentIDLabel() {
    super();
  }

  /**
   * Creates a new ParentIDLabel object.
   *
   * @param question 
   * @param description 
   */
  public ParentIDLabel(String question, String description) {
    super(question, description);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#getValue()
   */
  public Object getDynamicValue() {
    FormObject formObject = this.getParent();

    return "" + formObject.getUOID();
  }
}
