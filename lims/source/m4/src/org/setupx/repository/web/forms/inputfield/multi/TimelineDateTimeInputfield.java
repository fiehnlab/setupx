/**
 * ============================================================================ File:    TimelineDateTimeInputfield.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.core.communication.experimentgeneration.template.ClazzTemplate;
import org.setupx.repository.core.util.hotfix.oldsamples.OldSampleMapping;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.inputfield.DateInputfield;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.TimeInputfield;


/**
 * @hibernate.subclass
 */
public class TimelineDateTimeInputfield extends MultiField4Clazz {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new TimelineDateTimeInputfield object.
   */
  public TimelineDateTimeInputfield() {
    super();
  }

  /**
   * Creates a new TimelineDateTimeInputfield object.
   *
   * @param string 
   * @param string2 
   */
  public TimelineDateTimeInputfield(String string, String string2) {
    super(string, string2);

    FormObject date = new DateInputfield();
    FormObject time = new TimeInputfield(TimeInputfield.DEFAULT_TIME);

    this.addField(date);
    this.addField(time);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws UnsupportedClazzMappingException TODO
   */
  public String getPath() throws UnsupportedClazzMappingException {
    throw new UnsupportedClazzMappingException();
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws UnsupportedClazzMappingException TODO
   */
  public FormObject getSingleValueField() throws UnsupportedClazzMappingException {
    throw new UnsupportedClazzMappingException();
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.InputField#cloneField()
   */
  public InputField cloneField() {
    return new TimelineDateTimeInputfield(this.getQuestion(), this.getDescription());
  }

  /**
   * TODO: 
   *
   * @param col TODO
   *
   * @return TODO
   */
  public String createHTML(int col) {
    StringBuffer response = new StringBuffer();

    response.append("<!-- " + this.getName() + " (" + this.getDescription() + ") -->");

    // add a remove button
    response.append("<tr><td colspan=\"" + col + "\">" + this.createRemoveButton() + " " + this.getQuestion() + "<br><span class=small>" + this.getDescription() + "</span></td></tr>");
    response.append("<tr><td></td><td colspan=\"" + col + "\"><table>" + this.createHTMLChilds(4) + "</table></td></tr>");

    // anchor
    response.append(this.createAnchor());

    // add a help button
    response.append(this.createHelpButton());

    return response.toString();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.clazzes.ClazzInformation#createShortLabel()
   */
  public String createShortLabel() {
    return this.getQuestion() + ": " + ((StringInputfield) this.getField(0)).getValue() + " " + ((StringInputfield) this.getField(1)).getValue();
  }
}
