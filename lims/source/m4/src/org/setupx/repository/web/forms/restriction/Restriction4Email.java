/**
 * ============================================================================ File:    Restriction4Email.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.Link;
import org.setupx.repository.web.forms.validation.ValidationAnswer;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 *
 * @hibernate.subclass
 */
public class Restriction4Email extends StringRestriction {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String REGEX = "\\b[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\\.[a-zA-Z]{2,4}\\b";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Restriction4Email object.
   */
  public Restriction4Email() {
    super(REGEX, false);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public ValidationAnswer validateObject(InputField field) {
    ValidationAnswer answer = super.validateObject(field);

    if (answer != null) {
      return new ValidationAnswer(new Link(field.getValue() + " is not a valid emailaddress."));
    } else {
      return null;
    }
  }
}
