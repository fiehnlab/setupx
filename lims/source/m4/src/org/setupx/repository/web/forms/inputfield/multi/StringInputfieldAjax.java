package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.web.forms.inputfield.StringInputfield;

/**
 * Input field containing a simple string plus ajax support
 * 
 * @hibernate.subclass
 */
public class StringInputfieldAjax extends StringInputfield {
    int ajaxType = 0;

    public StringInputfieldAjax(String question, String description, final int ajaxType) {

        super(question, description);
        this.ajaxType = ajaxType;
    }

    /**
     * @deprecated just for peristencelayer
     */
    public StringInputfieldAjax() {
        super();
    }

    public String createField() {
        StringBuffer buffer = new StringBuffer();
        String submit = "";

        if (this.getAutosubmit()) {
            submit = "onBlur=\"document.forms[0].submit()\"";
        }

        buffer.append("<a name=\"" + this.getName() + "\"/><input type=\"text\" id=\"testinput" + this.getName()
                + "\" name=\"" + this.getName() + "\"  size=\"" + this.getSize_x() + "\"  " + submit + " value=\""
                + (String) this.getValue() + "\"/> " + this.getExtension());

        buffer.append("\n<script type=\"text/javascript\">");
        buffer.append("\nvar options = {");
        buffer.append("\n        script:\"ajax_central.jsp?json=true&type=" + this.ajaxType + "&\", ");
        buffer.append("\n        json:true, ");
        buffer.append("\n        varname:\"input\", ");
        buffer.append("\n        shownoresults:false, ");
        buffer.append("\n        callback: function (obj) { document.getElementById('testid').value = obj.id; } ");
        buffer.append("\n        }; ");
        buffer.append("\n        var as_json = new bsn.AutoSuggest('testinput" + this.getName() + "', options); ");
        buffer.append("\n</script> ");

        buffer.append(this.createErrorMSG());

        return buffer.toString();
    }
}
