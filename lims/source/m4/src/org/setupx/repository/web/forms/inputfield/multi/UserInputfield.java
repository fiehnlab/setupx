/**
 * ============================================================================ File:    UserInputfield.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.inputfield.BigStringInputField;
import org.setupx.repository.web.forms.inputfield.BooleanInputField;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.label.Label;
import org.setupx.repository.web.forms.relation.Relation;
import org.setupx.repository.web.forms.restriction.Restriction4Email;
import org.setupx.repository.web.forms.restriction.UserExistsRestriction;


/**
 * Inputfield for userinformation.
 * 
 * <p>
 * Includes a validation if the user exists (optional).
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.5 $
 *
 * @hibernate.subclass
 */
public class UserInputfield extends MultiField {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public StringInputfield email;
  private BigStringInputField info;
  private BooleanInputField modify;
  private Label nameRemote;
  private StringInputfield nameLocal;
  private StringInputfield remote_login;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param string
   * @param string2
   */
  public UserInputfield(String question, String desc) {
    super(question, desc);

    // names for non ldap
    StringInputfield name = new StringInputfield("Firstname and Lastname", "enter the name of the person.");
    this.nameLocal = name;

    remote_login = new StringInputfield("login-id", "enter the login that this user uses to login to this system");
    remote_login.setAutosubmit(true);
    remote_login.setHelptext("The campus of the University of California has got a campus wide conmputer system. " + "This system has all users on campus registered. This field asks for the login ID. (also called Kerberos ID). " +
      "<p>These account are created by the <a href=\"http://computingaccounts.ucdavis.edu/\">IT Express Computing Service.</a>");

    // fields are mofified by the ldap (userexistsrestiction)
    Label remote_username = new Label("Name", "the users name", "");
    remote_username.setHelptext("This is the username that is stored in the remote LDAP directory.");
    this.nameRemote = remote_username;

    InputField remote_info = new BigStringInputField("user information", "this is additional information about the user taken from the campus ldap-system", false);
    remote_info.setRestriction(null);
    email = new StringInputfield("eMail-Address", "The users eMail-Address.", false);
    email.setRestriction(new Restriction4Email());

    // restriction, that fills all information by taking it from the remote user object
    remote_login.setRestriction(new UserExistsRestriction(remote_login, remote_username, remote_info, email));

    // additonal information about the user
    info = new BigStringInputField("additional information", "enter information: like position in this project ...");
    info.setRows(info.getRows() / 2);

    modify = new BooleanInputField("Campus Member", "does this person have an account on the campus security system.", true);
    modify.setHelptext("Campus Member - The University of California Davis has got a campus wide system, which contains all user. So in case this user is a member of the UC Davis please select YES.");

    // adding all fields !! a few of them are alway hidden - (non active) !!
    this.addField(modify);
    this.addField(name);
    this.addField(remote_login);
    this.addField(remote_username);
    this.addField(email);
    this.addField(remote_info);
    this.addField(info);

    modify.addRelation(new Relation(new String[] { "" + true }, remote_login));
    modify.addRelation(new Relation(new String[] { "" + true }, remote_info));
    modify.addRelation(new Relation(new String[] { "" + true }, remote_username));
    modify.addRelation(new Relation(new String[] { "" + false }, name));
    modify.setValue("" + true);
  }

  /**
   * Creates a new UserInputfield object.
   */
  public UserInputfield() {
    super("User", "enter all the available information for this user.");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param active TODO
   */
  public void setActiveBigInforField(boolean active) {
    this.info.setActive(active);
  }

  /**
   * TODO: 
   *
   * @param object TODO
   */
  public final void setLoginNameValue(Object object) {
    remote_login.setValue(object);
  }

  /**
   * TODO: 
   *
   * @param email TODO
   */
  public void setEmail(StringInputfield email) {
    this.email = email;
  }

  /* @hibernate.many-to-one class = "org.setupx.repository.web.forms.FormObject"*/
  public StringInputfield getEmail() {
    return email;
  }

  /**
   * TODO: 
   *
   * @param info TODO
   */
  public void setInfo(BigStringInputField info) {
    this.info = info;
  }

  /* @hibernate.many-to-one class = "org.setupx.repository.web.forms.FormObject"*/
  public BigStringInputField getInfo() {
    return info;
  }

  /**
   * TODO: 
   *
   * @param modify TODO
   */
  public void setModify(BooleanInputField modify) {
    this.modify = modify;
  }

  /* @hibernate.many-to-one class = "org.setupx.repository.web.forms.FormObject"*/
  public BooleanInputField getModify() {
    return modify;
  }

  /**
   * TODO: 
   *
   * @param nameLocal TODO
   */
  public void setNameLocal(StringInputfield nameLocal) {
    this.nameLocal = nameLocal;
  }

  /* @hibernate.many-to-one class = "org.setupx.repository.web.forms.FormObject"*/
  public StringInputfield getNameLocal() {
    return nameLocal;
  }

  /**
   * TODO: 
   *
   * @param nameRemote TODO
   */
  public void setNameRemote(Label nameRemote) {
    this.nameRemote = nameRemote;
  }

  /* @hibernate.many-to-one class = "org.setupx.repository.web.forms.FormObject"*/
  public Label getNameRemote() {
    return nameRemote;
  }

  /**
   * TODO: 
   *
   * @param id TODO
   */
  public void setRemoteID(String id) {
    remote_login.setValue(id);
  }

  /**
   * FIXME - not tested - seems not to work
   */
  public void setRemoteUser(boolean b) {
    modify.setValue("" + b);
    modify.setActive(b);
    nameRemote.setActive(b);
    nameLocal.setActive(!b);
  }

  /**
   * TODO: 
   *
   * @param remote_login TODO
   */
  public void setRemote_login(StringInputfield remote_login) {
    this.remote_login = remote_login;
  }

  /* @hibernate.many-to-one class = "org.setupx.repository.web.forms.FormObject"*/
  public StringInputfield getRemote_login() {
    return remote_login;
  }

  /**
   * creates a inputfiled for user information. <b>User must be in one of the connected user systems.</b>
   */

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() throws CloneException {
    warning(this, "add cloning function");

    return new UserInputfield(this.getQuestion(), this.getDescription());
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#toStringHTML()
   */
  public String toStringHTML() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("<b>" + this.getQuestion() + "</b> ");

    if ((nameLocal != null) && nameLocal.isActive()) {
      buffer.append(nameLocal.getValue());
    }

    if ((nameRemote != null) && nameRemote.isActive()) {
      buffer.append(nameRemote.getValue());
    }

    /*for (int i = 0; i < this.size(); i++) {
       if (this.getField(i).isActive()){
           buffer.append(((InputField) this.getField(i)).toStringHTML());
           buffer.append("<br>");
       }
       }
     */
    return buffer.toString();
  }
  
  public String getXMLChildname() {
    return "user";
}
}
