package org.setupx.repository.web.forms.inputfield.multi;

public class SampleScheduledException extends Exception {

    public SampleScheduledException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public SampleScheduledException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public SampleScheduledException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public SampleScheduledException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
