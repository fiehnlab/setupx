package org.setupx.repository.web.forms;

public class InsufficientUserRightException extends Exception {

    public InsufficientUserRightException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public InsufficientUserRightException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public InsufficientUserRightException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public InsufficientUserRightException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
