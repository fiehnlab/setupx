package org.setupx.repository.web.forms;

import java.util.Date;

import org.setupx.repository.Config;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.web.forms.clazzes.ClazzDimension;
import org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.SampleScheduledException;

public class PromtUtil {
    
    public static void checkPersistence(){
        
        Config.initCoreUsers();

        try {
            // new PromtUtil().internalTest();
            new PromtUtil().checkTimeForSave();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * checking how long it takes to load and save an avereage Promt
     * @throws SampleScheduledException 
     * @throws PromtCreateException 
     */
    private void checkTimeForSave() throws PersistenceActionException, PromtCreateException, SampleScheduledException{
        // load a promt 
        Promt promt = Promt.load(336062);
        promt.update(true);
        
        Promt promt2 = Promt.load(336062);
        
        long timeStart = new Date().getTime();
        promt2.update(true);
        long timeEnd = new Date().getTime();
        
        Logger.log(this, "time it took to update: " + (timeEnd - timeStart));
    }

    private Promt promt;
    private int numClasses;
    private int numSamples;
    private long promtID;
    private int numClassDim;
    private int step = 1;
    private String speciesName;
    private String genotype;
    private boolean genoTypeActive;

    private void internalTest() throws PersistenceActionException, PromtCreateException, SampleScheduledException, IllegalPromtException, ComparisonsException{
        // PersistenceConfiguration.createSessionFactory();
        
        
        // create a promt
        this.promt = Promt.load(0);
        this.promt.findNBCISpeciesInputfield().setValue("Citrus sinensis");
        
        // init the number of classes and samples
        initSampleAndClasses();
        
        // before I save - check if the geno field is on
        if (! this.promt.findGenotypeInputfield().isActive()) throw new RuntimeException("GenotypeInputfield().isActive()");
        
        
        // save it
        this.promt.update(true);
        this.promtID = this.promt.getUOID();
        checkSampleAndClasses("promt has been saved");

        if (! this.promt.findGenotypeInputfield().isActive()) throw new RuntimeException("GenotypeInputfield().isActive()");

        /*
        this.promt.findNBCISpeciesInputfield().setValue("Citrus karna");
        */
        checkSampleAndClasses("species set again.");
        Promt.compare(this.promt, Promt.load(promtID));
        
        promt.update(true);
        Promt.compare(this.promt, Promt.load(promtID));
        
        this.promt = null;
        
        this.promt = Promt.load(promtID);
        checkSampleAndClasses("promt loaded");
        
        /*
        for (int i = 0; i < this.promt.determineClazzDimensions().length ; i++ ) {

            try {
                MultiFieldExpanding multiFieldExpanding = ((MultiFieldExpanding)this.promt.determineClazzDimensions()[i].getField(0).getParent());
                multiFieldExpanding.expand(1);
                Logger.debug(this, "expanding Multifield nr." + i + ": " + multiFieldExpanding.getQuestion() + " " + Util.getClassName(multiFieldExpanding));
                
                initSampleAndClasses();
                checkSampleAndClasses("promt expanded");
                
                this.promt.update(true);
                checkSampleAndClasses("promt expanded and updated");
                
                
                this.promt = Promt.load(this.promtID);
                checkSampleAndClasses("promt loaded again");
                Promt.compare(this.promt, Promt.load(promtID));

            } catch (ClassCastException e) {
                Logger.debug(this, this.promt.determineClazzDimensions()[i].getField(0).getParent().getClass().toString() + " is not a MultiFieldExpanding.");
            } catch (ArrayExpandException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
         */
    }

    private void initSampleAndClasses() {
        this.numClassDim = this.promt.getClazzDimensionDetector().getClazzDimensions().length;
        this.numClasses = this.promt.getClazzes().length;
        this.numSamples = this.promt.getSampleIDs().length;

        this.speciesName = this.promt.findNBCISpeciesInputfield().getSpeciesName();
        this.genotype = this.promt.findGenotypeInputfield().getField(0).toString();
        this.genoTypeActive = this.promt.findGenotypeInputfield().isActive();
    }

    private void checkSampleAndClasses(String string) throws IllegalPromtException {

        /*
        if (this.speciesName.compareTo(this.promt.findNBCISpeciesInputfield().getSpeciesName()) != 0) throw new IllegalPromtException("species " + this.speciesName + " vs. " +this.promt.findNBCISpeciesInputfield().getSpeciesName() , this.promt);
        if (this.genotype.compareTo(this.promt.findGenotypeInputfield().getField(0).toString()) != 0) throw new IllegalPromtException("genotype " + this.genotype + " vs. " + this.promt.findGenotypeInputfield().getField(0).toString(), this.promt);
        if (this.genoTypeActive  ==  this.promt.findGenotypeInputfield().isActive()) throw new IllegalPromtException("species " + this.genoTypeActive + " vs. " +this.genoTypeActive  );
        */
        
        
        ClazzDimension[] clazzDimensions = this.promt.determineClazzDimensions();
        
        Logger.log(this,"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        Logger.log(this,"          " + step + "          " + string + "          " + step + "          " + string + "    ") ;
        Logger.log(this,"          ClazzDimension: "  + clazzDimensions.length);
        for (int i = 0; i < clazzDimensions.length; i++) {
            ClazzDimension dimension = clazzDimensions[i];
            Logger.log(this,"          ClazzDim#" + i + ")");
            MultiField4Clazz[] multiField4Clazzs =  dimension.getFields();
            for (int j = 0; j < multiField4Clazzs.length; j++) {
                MultiField4Clazz multiField4Clazz = multiField4Clazzs[j];
                Logger.log(this,"               multiField4Clazz#" + j + ": [" + multiField4Clazz.getUOID() + "] " + multiField4Clazz.isActive() + " " + Util.getClassName(multiField4Clazz));
            }
        }
        Logger.log(this,"          genotype         should be: " + genotype + "\t");
        Logger.log(this,"          is:" + this.promt.findGenotypeInputfield().getField(0).toString());
        Logger.log(this,"          genoytpe active: should be: " + genoTypeActive + "\t is:" + this.promt.findGenotypeInputfield().isActive());
        Logger.log(this,"          species:         should be: " + speciesName + "\t is:" + this.promt.findNBCISpeciesInputfield().getSpeciesName()) ;
        Logger.log(this,"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

        
        if (this.numClassDim != this.promt.determineClazzDimensions().length){
            throw new IllegalPromtException("incorrect number of classdimensions: " + this.numClassDim + "[old] vs. " + this.promt.determineClazzDimensions().length + "[new]");
        }
        
        if (this.numClasses != this.promt.getClazzes().length){
            throw new IllegalPromtException("incorrect number of classes: " + this.numClasses + " vs. " + this.promt.getClazzes().length);
        }

        if (this.numSamples != this.promt.getSampleIDs().length){
            throw new IllegalPromtException("incorrect number of samples: " + this.numSamples + " vs. " + this.promt.getSampleIDs().length);
        }
        

        
        step ++;
    }
}
