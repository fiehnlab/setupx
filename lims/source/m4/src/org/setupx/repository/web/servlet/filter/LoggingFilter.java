package org.setupx.repository.web.servlet.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.omg.CORBA.Request;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.logging.Message;
import org.setupx.repository.web.WebAccess;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.ip.BlockedIP;

/**
 * Logging the requests.
 * 
 * @author scholz
 */
public class LoggingFilter implements Filter {

    private FilterConfig config = null;

    public void init(FilterConfig config) throws ServletException {
        this.config = config;
    }

    public void destroy() {
        config = null;
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
    throws IOException, ServletException {
        // filter the search engines
        if (BlockedIP.isblocked(servletRequest.getRemoteHost())){
            Logger.warning(this, " ip " + servletRequest.getRemoteHost() + " is blocked");
            throw new ServletException("blocked ip " + servletRequest.getRemoteHost() + "");
        } else {


            try {
                //servletRequest.getRemoteHost

                // time before start
                long before = System.currentTimeMillis();

                // do the actual job ...
                filterChain.doFilter(servletRequest, servletResponse);

                // time after
                long after = System.currentTimeMillis();

                // name
                String name = "";
                if (servletRequest instanceof HttpServletRequest) {
                    name = ((HttpServletRequest) servletRequest).getRequestURI();
                }

                Message message = null;
                WebAccess access = new WebAccess(servletRequest);
                access.update(true);


                message = new Message(this, (after - before) + "");


                // adding userinformation
                UserDO user = null;
                try{
                    if (servletRequest instanceof HttpServletRequest){
                        user = (UserDO) ((HttpServletRequest)servletRequest).getSession().getAttribute("user"); 
                        if (user != null){
                            message.setUserID(user.getUOID());
                        }
                    }
                } catch (Exception e) {
                    // dont worry / no user found
                }

                if (message != null){
                    // saving a reference to the webaccess in the message
                    message.setRelatedObjectID(access.getUOID());
                    message.setClassName(access.getClass().getName());
                    message.update(true);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
