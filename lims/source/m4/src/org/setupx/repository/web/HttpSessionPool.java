/**
 * ============================================================================ File:    HttpSessionPool.java Package: org.setupx.repository.web cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web;

import org.setupx.repository.core.CoreObject;

import java.util.Hashtable;

import javax.servlet.http.HttpSession;

public class HttpSessionPool extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  static Object thiz = new HttpSessionPool();
  static Hashtable sessions = new Hashtable();

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  public static HttpSession getHttpSession(final int session_id) {
    try {
      return (HttpSession) sessions.get("" + session_id);
    } catch (Exception e) {
      warning(thiz, "unable to find session: " + session_id);

      return null;
    }
  }


  public static Hashtable getSessions() {
    return sessions;
  }

  public static void add(HttpSession session) {
    log(thiz, "adding session " + session.getId());
    sessions.put(session.getId(), session);
  }
}
