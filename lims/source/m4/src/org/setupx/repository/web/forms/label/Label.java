/**
 * ============================================================================ File:    Label.java Package: org.setupx.repository.web.forms.label cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.label;

import org.setupx.repository.core.util.Util;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;


/**
 * Label for displaying information only. The Value can be changed from external but not by the user.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 *
 * @hibernate.subclass
 */
public class Label extends StringInputfield {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Label object.
   *
   * @param question 
   * @param description 
   * @param value 
   *
   * @hibernate.class
   */
  public Label(String question, String description, String value) {
    this(question, description);
    this.setValue(value);
  }

  /**
   * Creates a new Label object.
   *
   * @deprecated only for persistencelayer
   */
  public Label() {
    this("...", "...");
  }

  /**
   * @param question
   * @param description
   */
  public Label(String question, String description) {
    super(question, description, false);
    this.setRestriction(null);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public Object getDynamicValue() {
    return getValue();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() {
    return new Label(this.getQuestion(), this.getDescription(), "" + this.getValue());
  }

  /**
   * change from an inputfield to a string representation <br><b>the method is NOT called for the generation in the Clazz!! </b>
   *
   * @see org.setupx.repository.web.forms.inputfield.multi.Clazz#createHTML(int)
   * @see org.setupx.repository.web.forms.FormObject#getHTML()
   */
  public String createHTML(int col) {
    StringBuffer buffer = new StringBuffer();
    buffer.append("\n<!-- " + Util.getClassName(this) + " -->\n<tr>\n<td colspan=\"" + (col - 1) + "\">" + this.getQuestion() + this.createRequieredNote() + "<BR/><font size=\"-3\">" + this.getDescription() + "</font>");

    debug("--> " + Util.getClassName(this) + " is instance of DynamicLabel: " + (this instanceof DynamicLabel));

    if (this instanceof DynamicLabel) {
      buffer.append("\n<td><a name=\"" + this.getName() + "\"/>" + ((DynamicLabel) this).getDynamicValue());
    } else {
      buffer.append("\n<td><a name=\"" + this.getName() + "\"/>" + (String) this.getDynamicValue());
    }

    buffer.append(this.createHelpButton());
    buffer.append("</td></tr>\n");

    return buffer.toString();
  }
}
