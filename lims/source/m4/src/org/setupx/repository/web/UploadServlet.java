/**
 * ============================================================================ File:    UploadServlet.java Package: org.setupx.repository.web cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web;

import com.oreilly.servlet.MultipartRequest;

import org.setupx.repository.core.util.logging.Logger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.Enumeration;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Converts a Gutenberg-indexfile to an XML-File.  Gutenbergfiles look like GUTINDEX.03 which describe the content of a Gutenberg folder containing the  material for one year - This file has no real structure. So it is extreamly difficult to use in any  kind of application. For those who need the
 * structure as an XML-File, this converter has been developed.   To convert the index-files, the file must be uploaded in a multipartrequest. The answer, that will be send back from this servlet, will be the xml document with all entries of that file.
 *
 * @author Martin Scholz
 * @version $Revision: 1.5 $
 */
public class UploadServlet extends HttpServlet {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  protected void doPost(HttpServletRequest req, HttpServletResponse res)
    throws IOException {
    File _tmp = File.createTempFile("xyz", ".tmp", null);
    Logger.log(this, "Saving uploaded files to " + _tmp.getParentFile().getAbsolutePath());

    MultipartRequest multiRequest = new MultipartRequest(req, _tmp.getParentFile().getAbsolutePath());
    _tmp.delete();

    Enumeration fileNames = multiRequest.getFileNames();

    while (fileNames.hasMoreElements()) {
      String filename = (String) fileNames.nextElement();
      File file = multiRequest.getFile(filename);

      if (file == null) {
        error(res, new IOException("file in request is null."));

        return;
      }

      Logger.log(this, "uploaded file: " + file.getName() + "   " + file.getAbsolutePath());

      PrintWriter out = res.getWriter();
      File xmlFile;

      try {
        xmlFile = null; //_converter.getXMLFile();
      } catch (Exception e) {
        e.printStackTrace();
        throw new IOException(e.getMessage());
      }

      res.setContentType("application/xml");

      int _sizeFile = (int) xmlFile.length();
      int _char_read = 0;
      java.io.FileReader inFile = new java.io.FileReader(xmlFile);
      char[] readData = new char[_sizeFile];

      while (inFile.ready()) {
        _char_read += inFile.read(readData, _char_read, _sizeFile - _char_read);
      }

      inFile.close();

      String _text_read = new String(readData, 0, _char_read);
      out.print(_text_read);
      out.close();
    }
  }

  /**
   * 
   *
   * @param res 
   * @param ex 
   *
   * @throws IOException 
   */
  private void error(HttpServletResponse res, Exception ex)
    throws IOException {
    PrintWriter writer = res.getWriter();
    res.setContentType("text/html");
    writer.println(ex.toString() + "  ");
    writer.close();
    this.destroy();
  }
}
