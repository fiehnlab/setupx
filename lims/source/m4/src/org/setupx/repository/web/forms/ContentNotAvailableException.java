package org.setupx.repository.web.forms;

public class ContentNotAvailableException extends Exception {

    public ContentNotAvailableException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public ContentNotAvailableException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ContentNotAvailableException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public ContentNotAvailableException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
