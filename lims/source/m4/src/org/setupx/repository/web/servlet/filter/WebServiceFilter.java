package org.setupx.repository.web.servlet.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.setupx.repository.Config;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.logging.Message;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.web.WebAccess;

/**
 * Securing the webservices
 * 
 * @author scholz
 */
public class WebServiceFilter implements Filter {
    private FilterConfig config = null;

    public void init(FilterConfig config) throws ServletException {
        this.config = config;
    }

    public void destroy() {
        this.config = null;
    }

    /**
     * check if requesting IP is a valid registered IP
     */
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        // checking IP
        WebServiceFilter.checkIP(servletRequest);

        // proceed with regular action
        filterChain.doFilter(servletRequest, servletResponse);
    }

    /**
     * compare the ip to 3 defined IPs
     * @param servletRequest
     * @throws ServletException 
     */
    private static void checkIP(ServletRequest servletRequest) throws ServletException {
        String ip = servletRequest.getRemoteAddr();
        boolean valid = false;
        if (    ip.compareTo(Config.WEB_SERVICE_VALID_IP1) == 0
            || ip.compareTo(Config.WEB_SERVICE_VALID_IP2) == 0
            || ip.compareTo(Config.WEB_SERVICE_VALID_IP3) == 0){
                valid = true;
            }
        if (valid){
            return;
        } else {
            // illegal access
            throw new ServletException("you ip address can not access this service / contact admin and register the address " + ip);
        }
    }
}
