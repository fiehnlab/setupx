/**
 * ============================================================================ File: PromtCreator.java Package:
 * org.setupx.repository.web.forms cvs: $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz Copyright (C) 2005 Martin
 * Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/
 * ============================================================================
 */
package org.setupx.repository.web.forms;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.document.sop.StandardOperationProcedure;
import org.setupx.repository.core.communication.experimentgeneration.MetadataSource;
import org.setupx.repository.core.communication.experimentgeneration.template.ClazzTemplate;
import org.setupx.repository.core.communication.experimentgeneration.template.SampleTemplate;
import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.core.communication.importing.logfile.ScannedPair;
import org.setupx.repository.core.communication.ncbi.local.NCBIEntry;
import org.setupx.repository.core.util.hotfix.oldsamples.OldSampleMapping;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.web.forms.clazzes.ClassTable;
import org.setupx.repository.web.forms.inputfield.BigStringInputField;
import org.setupx.repository.web.forms.inputfield.BooleanInputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.SubmitButton;
import org.setupx.repository.web.forms.inputfield.multi.AnimalOriginInputfield;
import org.setupx.repository.web.forms.inputfield.multi.FilesMultiField;
import org.setupx.repository.web.forms.inputfield.multi.GrowthConditionInputfield;
import org.setupx.repository.web.forms.inputfield.multi.HumanOriginInputfield;
import org.setupx.repository.web.forms.inputfield.multi.MultiField;
import org.setupx.repository.web.forms.inputfield.multi.MultiFieldClazzes;
import org.setupx.repository.web.forms.inputfield.multi.MultiFieldExpanding;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.SpeciesInputfield3;
import org.setupx.repository.web.forms.inputfield.multi.TimelineInputfield;
import org.setupx.repository.web.forms.inputfield.multi.TreatmentInputfield;
import org.setupx.repository.web.forms.inputfield.multi.UserInputfield;
import org.setupx.repository.web.forms.label.Label;
import org.setupx.repository.web.forms.label.PromtIDLabel;
import org.setupx.repository.web.forms.page.AdminPage;
import org.setupx.repository.web.forms.page.Page;
import org.setupx.repository.web.forms.page.SamplePage;
import org.setupx.repository.web.forms.relation.Relation;
import org.setupx.repository.web.forms.relation.RelationKingdom;
import org.setupx.repository.web.forms.restriction.Restriction4Int;

/**
 * creates the inital promt
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.38 $
 */
public class PromtCreator extends CoreObject {
    // ~ Static fields/initializers
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private static final long serialVersionUID = -6419987878135445485L;
    public static String version = "$Revision: 1.38 $";
    private static Object thiZ = PromtCreator.class;
    public static final int CHEMVERSION = 10;
    public static final int DEFAULTVERSION = 20;
    public static final int DEVEDITION = 30;

    static {
        log(thiZ, "starting");
    }

    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * not used
     */
    private PromtCreator() {
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public static final Promt create(int type) throws PromtCreateException, MappingError {
        // for debugging purposes
        if (type == DEVEDITION) {
            return createInternalDevEdition(new NullMetaDatasource());
        }
        return createInternal(new NullMetaDatasource());
    }

    private static Promt createInternalDevEdition(NullMetaDatasource nullMetaDatasource) throws MappingError, PromtCreateException {
        return PromtCreator_Dev.createInternalDevEdition(nullMetaDatasource);
    }

    /**
     * create a Promt by using the information from the source
     */
    public static final Promt create(MetadataSource source) throws PromtCreateException, MappingError {
        return createInternal(source);
    }



    /**
     * create a Promt by using the information from the source
     * <p>
     * after the promt is actually created the source is taken again and the includes samples are mapped to the new
     * created samples
     * </p>
     * 
     * @throws MappingError
     * @throws
     * @see Promt#assignClazzTemplates(MetadataSource)
     */
    private static final Promt createInternal(final MetadataSource source) throws PromtCreateException, MappingError {
        Logger.log(PromtCreator.class, "creating promt");

        if (source != null) {
            log(thiZ, "using a soure: " + org.setupx.repository.core.util.Util.getClassName(source));
        }

        Promt promt = new Promt(
                "Experiment Definition",
                "For running the experiment we need information from you about the samples, the structure of the experiment, ...<br>"
                        + "To make it as easy as possible for you to enter this information the questions are splitted into different <b>Pages</b>."
                        + "<p>Each of the pages contains different questions. We would please you to enter all the information as detailed as possible.<br>",
                "Next to each page header there is a small <b>icon</b>, which shows you if the all questions on this page have been answered sufficient.<br>"
                        + "When you are done with the desciption of your experiment, you can submit the data to the central "
                        + Config.SYSTEM_NAME + "Server.");

        // -------------------------------------------------------------------------------------------------------------------------

        /* Biological Source Genotype, provenance and identification data for items of biological material */
        Page page_biosource = new Page("Biological Source", "Genotype, provenance and identification",
                "Genotype, provenance and identification data for items of biological material");
        page_biosource.dividerHR = true;

        SpeciesInputfield3 speciesForm = new SpeciesInputfield3("rat", source);

        // the Array of different samples
        page_biosource.addField(speciesForm);

        promt.addField(page_biosource);

        // -------------------------------------------------------------------------------------------------------------------------

        /* Growth Description of the environments in which the biological material developed */
        Page page_growth = new Page("Growth", "Description of the environments",
                "Description of the environments in which the biological material developed");
        promt.addField(page_growth);

        // might be redundant
        MultiFieldExpanding growth = new MultiFieldExpanding("Growth (Plant)", "Definition of the Growth Condition",
                "Please define each growthcondition in which the material was developed.",
                new GrowthConditionInputfield());

        // add to the page
        page_growth.addField(growth);
        // remove the whole inputfield, when the species is not a plant or any of the specified types.
        speciesForm.species().addRelation(
                new RelationKingdom(new int[] { NCBIEntry.PLANT, NCBIEntry.MIRCOORGANISM }, growth));

        MultiFieldExpanding originHuman = new MultiFieldExpanding("Origin (Human)",
                "Location where the samples are from.", "Please define where the samples are from.",
                new HumanOriginInputfield());

        // add to the page
        page_growth.addField(originHuman);
        speciesForm.species().addRelation(new RelationKingdom(NCBIEntry.HUMAN, originHuman));

        MultiFieldExpanding originANIMAL = new MultiFieldExpanding("Origin (Animal)",
                "Location where the animal samples are from.", "Please define where the samples are from.",
                new AnimalOriginInputfield());

        // add to the page
        page_growth.addField(originANIMAL);
        speciesForm.species().addRelation(new RelationKingdom(NCBIEntry.ANIMAL, originANIMAL));

        // -------------------------------------------------------------------------------------------------------------------------
        Page page_treatment = new Page("Treatment", "information how the sample was treated",
                "Please define each single treatment.");

        if (!Config.NEW_DESIGN) {
            page_treatment.dividerHR = true;
        }

        // add an overview of the different treatments
        MultiField treatmentTriggers = new MultiField("Treatments",
                "please select the types of treatments that are involved in this experiment.");

        // timeline inputfiel
        BooleanInputField booleanInputField = new BooleanInputField("Timeline",
                "were the samples take in a specific timeline", true);
        treatmentTriggers.addField(booleanInputField);

        BooleanInputField booleanInputFieldDiet = new BooleanInputField("Diet",
                "was the biological soure under a diet", true);
        treatmentTriggers.addField(booleanInputFieldDiet);

        BooleanInputField booleanInputField2 = new BooleanInputField("other treatments",
                "were the samples treated in any other way", true);
        treatmentTriggers.addField(booleanInputField2);

        page_treatment.addField(treatmentTriggers);

        TimelineInputfield timelineInputfield = new TimelineInputfield("Timeline", "");

        page_treatment.addField(timelineInputfield);
        booleanInputField.addRelation(new Relation("true", timelineInputfield));

        // diet input field becomes a single field and a updated version of the dietinputfield
        /*
         * // diet inputfiel DietInputfield dietInputfieldSingle = new DietInputfield("Diet", ""); MultiFieldExpanding
         * dietMulti = new MultiFieldExpanding("Diets", "discribe the different diets", "", dietInputfieldSingle);
         * page_treatment.addField(dietMulti); booleanInputFieldDiet.addRelation(new Relation("true", dietMulti));
         */
        // --> new version
        DietInputfield2 dietIn = new DietInputfield2("Diet", "");
        page_treatment.addField(dietIn);
        booleanInputFieldDiet.addRelation(new Relation("true", dietIn));
        // <-- new version

        // add more specific treatments
        MultiField treatGroup = new MultiField("Treatment", "please define how the samples were treated");

        treatGroup.addField(new StringInputfield("name", "label the treatment"));
        treatGroup.addField(new BigStringInputField("description", "..."));

        MultiFieldExpanding s = new MultiFieldExpanding("specific treatments",
                "please define the different variations of this treatment", "", new TreatmentInputfield());

        try {
            s.expand(1);
        } catch (ArrayExpandException e1) {
            throw new PromtCreateException(e1);
        }

        treatGroup.addField(s);

        MultiFieldExpanding multiField_treatment = new MultiFieldExpanding(
                "other Treatments",
                "Define treatment conditions.",
                "We are asking this information to collect information how the samples were differently treated. For example in an experiment setup there is one type of plant that was treated with different amounts of fertilizer (20 mg/day, 10 mg/day and no fertilizer). So the label could be �fertilizer treatment� and any description. Under variations on the bottom of this page each of the different amounts would be specified � each of the different amount as a separate variation.",
                treatGroup);
        booleanInputField2.addRelation(new Relation("true", multiField_treatment));
        page_treatment.addField(multiField_treatment);
        promt.addField(page_treatment);

        /* Sample Preparation - Protocols for preparing samples for presentation to analytical instruments */
        Page page_prep = new Page("Sample Preparation.", "Protocols for preparing samples ",
                "Protocols for preparing samples for presentation to analytical instruments");
        page_prep.dividerHR = false;

        if (Config.NEW_DESIGN) {
            page_prep.dividerHR = true;
        }

        // -----------------
        /*
         * MultiFieldExpanding multiFieldExpandingExtraction = new MultiFieldExpanding("SOP (Extraction)", "standard
         * operation protocols used for the extraction.", "Protocols for preparing samples", new
         * SOPInputField("Extraction", SOP.determineSOPs(SOP.PREPERATION), true)); MultiFieldExpanding
         * multiFieldExpandingSplit = new MultiFieldExpanding("SOP (Splitratio)", "standard operation protocols for
         * GC-Settings.", "Protocols used for the Settings on the GC-TOF", new SOPInputField("Splitratio",
         * SOP.determineSOPs(SOP.SPLITRATIO), true)); page_prep.addField(multiFieldExpandingExtraction);
         * page_prep.addField(multiFieldExpandingSplit);
         */

        // new Version receives the possible SOP as DocumentObjects
        page_prep.addField(StandardOperationProcedure.createInputfield());

        // -----------------
        StringInputfield inDry = new StringInputfield("weight", "approx weight");
        inDry.setRestriction(new Restriction4Int());
        inDry.setSize_x(4);
        inDry.setExtension("mg");
        page_prep.addField(inDry);

        BooleanInputField inDryOrFresh = new BooleanInputField("Dry weight",
                "Are the samples already dried or is it the freshweight.");
        page_prep.addField(inDryOrFresh);

        /*
         * // preperation done by whom UserInputfield field_Preperation = new UserInputfield("Preparation", "Person who
         * did the preperation. We like to have this information,<BR> so that we can get in contact with that person in
         * case there<br> are questions related to the preparation of the samples.");
         * field_Preperation.setActiveBigInforField(false); field_Preperation.setRemoteUser(false);
         * page_prep.addField(field_Preperation);
         */

        promt.addField(page_prep);

        // -------------------------------------------------------------------------------------------------------------------------

        /*
         * Analysis Specific Sample Preparation - Protocols specific to particular analytical technologies Analysis
         * Specific Sample Preparation - Protocols specific to particular analytical technologies - Instrumental
         * Analysis - Process description of the chemical analysis of samples, including descriptions of analytical
         * instruments and their operational parameters, quality control protocols and references to archive copies of
         * raw results Metabolome Estimate - The output from the analytical instruments after it has been processed from
         * raw data to produce a metabolome description and metadata about the processing
         */
        /* Samples on a single page */
        Page page_samples = new SamplePage("Samples / Classes", "samples for this experiement",
                "This is the summary of all classes. The Classes are a collections of different samples.");
        page_samples.setSummaryActive(true);

        page_samples.addField(new ClassTable());

        // navigation
        MultiField field_classes = new MultiFieldClazzes();
        page_samples.addField(field_classes);
        promt.addField(page_samples);

        // -------------------------------------------------------------------------------------------------------------------------

        /* Admin Experiment description and contact details */
        Page page_admin = new AdminPage(
                "Administration",
                "Contact details",
                "Please define all Members that are involved in this experiment. This helps you and us to keep track of who is involved in this experiment.");
        page_admin.setSummaryActive(true);

        UserInputfield field_pi = new UserInputfield("User", "enter all the available information for this user.");

        field_pi.setQuestion("Principal Investigator");
        field_pi.setDescription("The principal investigator of this experiment.");
        // field_pi.setRemoteUser(false);
        field_pi.setActiveBigInforField(false);
        page_admin.addField(field_pi);

        UserInputfield userin = new UserInputfield("User", "enter all the available information for this user.");
        MultiFieldExpanding users = new MultiFieldExpanding("involved Users", "Users involved in this experiment.",
                "Please define every single user in this experiment.", userin);

        users.keyLabel = "user";
        page_admin.addField(users);

        promt.addField(page_admin);

        // -------------------------------------------------------------------------------------------------------------------------

        /* introduction ... */
        Page page_abstract = new Page("Abstract", "General Information regarding the whole experiment", "");
        page_abstract
                .setLongDescription("This Page asks for a few <b>general information</b> about this experiment.<p>"
                        + "Please give your experiment an <b>Title</b>. This information helps you keep track of your different experiments. For "
                        + "example \"Influence of Lightstress on the Mouse Liver.\"<p>"
                        + "The <b>experiment ID</b> is generated by this system and is unique - it can not be changed.<p>"
                        + "At the Bottom you can add references to informations, that you want to share with this experiment.");
        page_abstract.setSummaryActive(true);

        // title
        page_abstract.addField(new StringInputfield("Title", "please enter the title of the experiment"));

        // sample id
        Label expID = new PromtIDLabel("Exp.ID",
                "The experiment ID is generated by this system and is unique - it can not be changed.");
        expID.setSize_x(expID.getSize_x() / 2);
        expID
                .setHelptext("This is a unique ID that allowes us to identify this experiment. In case the experiment is not beeing stored yet, the Id will stay 0. As soon as the experiment is saved, you ll see the actual experiment id.");
        page_abstract.addField(expID);

        // abstract
        BigStringInputField inAbstract = new BigStringInputField("abstract", "short description of the experiment",
                "-please type here your own abstract-");
        inAbstract.setCols(80);
        inAbstract.setRows(10);
        page_abstract.addField(inAbstract);

        // big comment
        BigStringInputField inComment = new BigStringInputField("comment",
                "information and comments that you want to add to this experiment.");
        inComment.setRestriction(null);
        inComment.setCols(80);
        inComment.setRows(13);
        page_abstract.addField(inComment);

        // files that are related to this document
        MultiField relatedFiles = new FilesMultiField("Files",
                "Documents, Files, ... that are related to this document");
        page_abstract.addField(relatedFiles);

        SubmitButton submitButton = new SubmitButton();
        page_abstract.addField(submitButton);

        promt.addField(page_abstract);

        speciesForm.species().setValue("Bos taurus");

        promt.testParentField();

        promt.showIt(1);

        promt.updateClazzDimDetector();

        try {
            // assigning classtemplates
            promt.assignClazzTemplates(source);
        } catch (MappingError e) {
            e.printStackTrace();
        }

        if (source.isFinishedRun()) {
            try {
                promt.update(true);

                // in case all the samples have been run already, create scannedpair with them
                ClazzTemplate[] clazzTemplates = source.getClazzTemplates();

                for (int i = 0; i < clazzTemplates.length; i++) {
                    SampleTemplate[] sampleTemplates = clazzTemplates[i].getSamples();

                    for (int j = 0; j < sampleTemplates.length; j++) {
                        SampleTemplate template = sampleTemplates[j];

                        // create a new scanned pair
                        try {
                            String sampleName = template.getValue(OldSampleMapping.SAMPLENAME);
                            String sampleID = template.getAssignedSample().getUOID() + "";

                            new ScannedPair(sampleName, sampleID).update(true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                promt.update(true);
            } catch (PersistenceActionUpdateException e1) {
                e1.printStackTrace();
            }
        }

        return promt;
    }

}
