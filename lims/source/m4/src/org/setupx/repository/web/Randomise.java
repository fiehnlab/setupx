/**
 * ============================================================================ File:    Randomise.java Package: org.setupx.repository.web cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web;

import org.setupx.repository.core.util.web.UiHttpServlet;
import org.setupx.repository.web.forms.inputfield.multi.Sample;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Randomise extends UiHttpServlet {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.core.util.web.UiHttpServlet#process(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  public void process(HttpServletRequest request, HttpServletResponse response)
    throws Throwable {
    Sample[] samples = null;

    for (int i = 0; i < samples.length; i++) {
      Sample sample = samples[i];
      Hashtable hashtable = sample.getAttributes();
      Enumeration enumeration = hashtable.keys();

      while (enumeration.hasMoreElements()) {
        // key of the attribute
        String key = (String) enumeration.nextElement();

        // value of attribute
        String value = (String) hashtable.get(key);
      }
    }
  }
}
