package org.setupx.repository.web.forms;
public class WebFormException extends Exception {
    public WebFormException(Exception e) {
        super(e);
    }

    
    public WebFormException(String string, Exception e3) {
        super(string, e3);
    }
}
