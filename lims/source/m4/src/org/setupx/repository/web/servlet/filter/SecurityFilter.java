package org.setupx.repository.web.servlet.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.setupx.repository.core.util.logging.Logger;

/**
 * securing the requests.
 * 
 * @author scholz
 */
public class SecurityFilter implements Filter {
    
    private FilterConfig config = null;
    
    public void init(FilterConfig config) throws ServletException {
      this.config = config;
    }

    public void destroy() {
      config = null;
    }

    
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        // actual filtering
        // Logger.debug(this, "securing application");
        
        // do the actual job ...
        filterChain.doFilter(servletRequest, servletResponse);

    }
}
