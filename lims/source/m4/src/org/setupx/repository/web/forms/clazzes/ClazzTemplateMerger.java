/**
 * ============================================================================ File:    ClazzTemplateMerger.java Package: org.setupx.repository.web.forms.clazzes cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.clazzes;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.experimentgeneration.template.ClazzTemplate;
import org.setupx.repository.core.communication.experimentgeneration.template.SampleTemplate;
import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.core.util.hotfix.oldsamples.OldSampleMapping;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;
import org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz;
import org.setupx.repository.web.forms.inputfield.multi.UnsupportedClazzMappingException;

import java.util.HashSet;


/**
 * Merger for Clazzes and Templates.
 * 
 * <p>
 * Merges a set of clazzes and a set of tempates. Each template is compared to the clazzes. When the matching clazz is beeing found, the samples for the template are taken from the template and mapped onto the real clazz.
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class ClazzTemplateMerger extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Clazz[] clazzs;
  private ClazzTemplate[] templates;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private ClazzTemplateMerger(Clazz[] clazzs, ClazzTemplate[] clazzTemplates) {
    this.clazzs = clazzs;
    this.templates = clazzTemplates;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * merges a set of clazzes and a set of templates.
   *
   * @param clazzs the samples of these clazzes are beeing replaced by the sampletempletes of the matching clazztemplate
   * @param clazzTemplates the templates that are mapped onto the real clazz
   *
   * @throws MappingError it is not possible to find a matching clazz for all of the templates.
   */
  public static void merge(Clazz[] clazzs, ClazzTemplate[] clazzTemplates)
    throws MappingError {
    new ClazzTemplateMerger(clazzs, clazzTemplates).merge();
  }

  /**
   * compare a value against the value stored in a MultiField4Clazz
   *
   * @return true if the same.
   *
   * @throws UnsupportedClazzMappingException
   */
  private boolean compare(MultiField4Clazz multiField4Clazz, String valueInTemplate)
    throws UnsupportedClazzMappingException {
    //sdebug("comparing \"" + valueInTemplate + "\" and \"" +multiField4Clazz.getSingleValue() + "\" " );
    String valueInMultiF4Clazz = (String) multiField4Clazz.getSingleValue();

    return (valueInTemplate.compareTo(valueInMultiF4Clazz) == 0);
  }

  /**
   * TODO: 
   *
   * @throws MappingError
   */
  private void merge() throws MappingError {
    HashSet exceptions = new HashSet();

    for (int i = 0; i < templates.length; i++) {
      ClazzTemplate template = templates[i];
      boolean loop = true;

      for (int j = 0; (j < clazzs.length) && loop; j++) {
        Clazz clazz = clazzs[j];
        debug("merge clazz " + j + "/" + clazzs.length + "  template " + i + "/" + templates.length);
        debug("number of exception: " + exceptions.size());

        try {
          merge(clazz, template);
          debug("ok: merge for template " + i + " successful.");
          loop = false;
        } catch (MatchingError e) {
          // thats ok - we keep on searching
        }
      }

      if (loop) {
        debug("merge for template " + i + " unsuccessful.");
        exceptions.add(new MappingError("unable to find a matching clazz for template " + i + "/" + templates.length));
      }
    }

    if (exceptions.size() != 0) {
      throw new MappingError("unable to map: " + exceptions.size() + " exception.", exceptions);
    }
  }

  /**
   * merge the information in a clazz with the information of the template.
   * 
   * <p>
   * done by checking if all pathes needed for the mapping have a matching field in the class.
   * </p>
   * 
   * <p>
   * after that the  each Mutlifield4class out of the clazz is taken and compared to
   * </p>
   *
   * @param clazz TODO
   * @param template TODO
   *
   * @throws MatchingError
   */
  private void merge(final Clazz clazz, ClazzTemplate template)
    throws MatchingError {
    MultiField4Clazz[] field4Clazzs = clazz.getMultiField4Clazzs();
    String valueInTemplate = "";

    // changes: iterate over all the values in the template and look for a matching 
    // clazz then.
    // values that have to be in the mapping
    String[] pathes = OldSampleMapping.getPath();

    for (int i = 0; i < pathes.length; i++) {
      String path = pathes[i];

      // get the Mutlifield4clazz with the same path
      boolean loop = true;

      for (int j = 0; (j < field4Clazzs.length) && loop; j++) {
        try {
          // in case it is the same path
          if (field4Clazzs[j].getPath().compareTo(path) == 0) {
            //debug("found matching for path \"" + path + "\"- now checking value");
            valueInTemplate = template.getValue(field4Clazzs[j].getPath());

            // and in case it is the same value
            if (compare(field4Clazzs[j], valueInTemplate)) {
              //debug("found matching for values too");
              loop = false;
            }
          }
        } catch (UnsupportedClazzMappingException e) {
        }
      }

      // didt find anything
      if (loop) {
        throw new MatchingError("the clazz and the template do not match. Missing match for: " + path);
      }
    }

    debug("ok - mergin samples now");

    /*
       @deprecated
       // take each Mutlifield4class and
       for (int i = 0; i < field4Clazzs.length; i++) {
         field4Clazz = field4Clazzs[i];
         try {
           valueInTemplate = template.getValue(field4Clazz.getPath());
           debug("  looking in " + Util.getClassName(field4Clazz) + " with value: " + valueInTemplate);
           if (compare(field4Clazz, valueInTemplate)) {
             // ok - matching
             debug("  match for \"" + valueInTemplate + "\" found in [" + Util.getClassName(field4Clazz) + "] ");
           } else {
             debug("  no matching for \"" + valueInTemplate + "\" found in [" + Util.getClassName(field4Clazz) + "]");
             throw new MatchingError("unable to match clazz and template, cause:   no matching for \"" + valueInTemplate + "\" found in [" + Util.getClassName(field4Clazz) + "]");
           }
         } catch (UnsupportedOperationException e) {
           throw new MatchingError("unable to get a path from " + Util.getClassName(field4Clazz) + " ", e);
         } catch (UnsupportedClazzMappingException e) {
           throw new MatchingError("unable to get a path from " + Util.getClassName(field4Clazz) + " ", e);
         }
       }
     */
    // ok that is the correct one
    mergeSamples(clazz, template.getSamples());
  }

  /**
   * merge a set of sampletemplates onto a real clazz
   *
   * @param clazz the clazz the samples will be mapped on.
   * @param sampleTemplates the samples that will be mapped.
   */
  private void mergeSamples(Clazz clazz, SampleTemplate[] sampleTemplates) {
    new SampleTemplateMerger(clazz, sampleTemplates).merge();
  }
}
