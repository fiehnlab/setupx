package org.setupx.repository.web.ajax;


public class AjaxContants {
    
    public static final int QUERY_USER = 1;
    public static final int QUERY_ORGAN = 2;
    public static final int QUERY_SPECIES = 3;
    
}
