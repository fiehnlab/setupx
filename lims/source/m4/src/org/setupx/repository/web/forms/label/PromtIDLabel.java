/**
 * ============================================================================ File:    PromtIDLabel.java Package: org.setupx.repository.web.forms.label cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.label;

import org.setupx.repository.web.forms.FormRootNotAvailableException;


/**
 * @hibernate.subclass
 */
public class PromtIDLabel extends DynamicLabel {
    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @deprecated only for persistencelayer
     */
    public PromtIDLabel() {
        super();
    }

    /**
     * @see DynamicLabel#DynamicLabel(String, String)
     */
    public PromtIDLabel(String question, String description) {
        super(question, description);
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* (non-Javadoc)
     * @see org.setupx.repository.web.forms.DynamicLabel#getValue()
     */
    public Object getDynamicValue() {
        try {
            return "" + root().getUOID();
        } catch (FormRootNotAvailableException e) {
            warning(this, "unable to find root");

            return UNDEFINED_VALUE;
        }
    }
}
