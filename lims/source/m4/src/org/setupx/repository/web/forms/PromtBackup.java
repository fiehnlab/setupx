/**
 * ============================================================================ File:    PromtBackup.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.web.forms.inputfield.multi.Promt;

import java.io.File;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class PromtBackup extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public File file;
  public Promt promt;
  public PromtQuery query;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new PromtBackup object.
   *
   * @param promt 
   * @param file 
   */
  public PromtBackup(Promt promt, File file) {
    this.promt = promt;
    this.file = file;
    this.query = new PromtQuery(promt);
  }
}
