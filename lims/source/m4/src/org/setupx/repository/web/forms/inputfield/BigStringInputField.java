/**
 * ============================================================================ File:    BigStringInputField.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import org.setupx.repository.core.util.Util;
import org.setupx.repository.web.forms.restriction.MultiLexiRestriction;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.21 $
 *
 * @hibernate.subclass
 */
public class BigStringInputField extends StringInputfield {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private boolean editable = true;

  /** default values for the size of the inputfiled */
  private int cols = DEFAULT_COLS;

  /** default values for the size of the inputfiled */
  private int rows = DEFAULT_ROWS;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static int DEFAULT_COLS = 60;
  public static int DEFAULT_ROWS = 10;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new BigStringInputField object.
   */
  public BigStringInputField() {
    this("...", "...");
  }

  /**
   * Creates a new BigStringInputField object.
   *
   * @param question 
   * @param description 
   * @param restriction 
   */
  public BigStringInputField(String question, String description) {
    super(question, description, false);
  }

  /**
   * Creates a new BigStringInputField object.
   *
   * @param question 
   * @param description 
   * @param answer 
   * @param restriction 
   */
  public BigStringInputField(String question, String description, String answer) {
    super(question, description, answer, false);
  }

  /**
   * Creates a new BigStringInputField object.
   *
   * @param question 
   * @param description 
   * @param answer 
   * @param rows 
   * @param cols 
   */
  public BigStringInputField(String question, String description, String answer, int rows, int cols, boolean removable) {
    super(question, description, answer, removable);
    this.setRestriction(new MultiLexiRestriction());
    setCols(cols);
    setRows(rows);
  }

  /**
   * @param string
   * @param string2
   * @param b
   */
  public BigStringInputField(String question, String description, boolean b) {
    super(question, description, "", false);
    this.setEditable(b);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param cols TODO
   */
  public void setCols(int cols) {
    this.cols = cols;
  }

  /**
   * @hibernate.property
   */
  public int getCols() {
    return cols;
  }

  /**
   * @hibernate.property
   */
  public boolean isEditable() {
    return editable;
  }

  /**
   * TODO: 
   *
   * @param rows TODO
   */
  public void setRows(int rows) {
    this.rows = rows;
  }

  /**
   * @hibernate.property
   */
  public int getRows() {
    return rows;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() {
    return new BigStringInputField(this.getQuestion(), this.getDescription(), (String) this.getValue());
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.StringInputfield#createHTML()
   */
  public String createHTML(int col) {
    StringBuffer buffer = new StringBuffer();
    buffer.append("<!-- " + Util.getClassName(this) + " -->\n<tr>\n<td " + createBackgroudcolor() + " colspan=\"" + (col) + "\" >" + this.getQuestion() + "<BR/><span class=\"small\" >" + this.getDescription() + " </span>");
    // new addition - button for history completion
    buffer.append(this.createHistoryButton());

    buffer.append("<BR>");
    buffer.append("<textarea name=\"" + this.getName() + "\" rows=\"" + this.getRows() + "\" cols=\"" + this.getCols() + "\"  " + this.editableString() + "  >" + (String) this.getValue() + "</textarea>");

    buffer.append(createErrorMSG());

    // button to remove this
    buffer.append(this.createRemoveButton());

    /*
    // button for resizing
    buffer.append("<tr><td colspan=\"" + (col) + "\" align=\"right\">" + createResizeButtonsX() + createResizeButtonsY() + "</td></tr>\n");
    buffer.append("</td></tr>\n");
    buffer.append(createHiddenResizeFiled());
    */

    return buffer.toString();
  }

  /**
   * TODO: 
   */
  public void sizeColMinus() {
    setCols(this.getCols() - 1);
  }

  /**
   * TODO: 
   */
  public void sizeColPlus() {
    setCols(this.getCols() + 1);
  }

  /**
   * TODO: 
   */
  public void sizeRowMinus() {
    setRows(this.getRows() - 1);
  }

  /**
   * TODO: 
   */
  public void sizeRowPlus() {
    setRows(this.getCols() + 1);
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  protected String editableString() {
    if (this.isEditable()) {
      return "";
    } else {
      return "readonly";
    }
  }

  /**
   * @param b
   */
  private void setEditable(boolean b) {
    this.editable = b;
  }

  /**
   * @return
   */
  private String createHiddenResizeFiled() {
    return ("<input name=\"resize\" type=\"hidden\" value=\"foo\"/>");
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  private String createResizeButtonsX() {
    // one button for x plus and one button for x minus //
    return "<input type=\"image\" src=\"" + resizeXminus + "\" ONFOCUS=\"document.forms[0].resize.value = " + this.getName() + "Xplus\"; document.forms[0].submit()\">" + "<input type=\"image\" src=\"" + resizeXplus + "\" ONFOCUS=\"document.forms[0].resize.value = " + this.getName() +
    "Xminus\"; document.forms[0].submit()\">";
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  private String createResizeButtonsY() {
    return "<input type=\"image\" src=\"" + resizeYminus + "\" ONFOCUS=\"document.forms[0].resize.value = " + this.getName() + "Yplus\"; document.forms[0].submit()\">" + "<input type=\"image\" src=\"" + resizeYplus + "\" ONFOCUS=\"document.forms[0].resize.value = " + this.getName() +
    "Yminus\"; document.forms[0].submit()\">";
  }
}
