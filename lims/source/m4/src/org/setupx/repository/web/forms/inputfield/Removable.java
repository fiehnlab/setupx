/**
 * ============================================================================ File:    Removable.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.Controller;


/**
 * object has the ability, that it can be removed from a bigger set of dynamical growing array.
 *
 * @see org.setupx.repository.web.forms.MultiFieldExpanding
 */
public interface Removable {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * decide if the object can be removed or not
   *
   * @param i_removable if the object can be removed or not
   */
  public void setRemovable(boolean i_removable);

  /**
   * return if the object can be removed or not
   *
   * @return if the object can be removed or not
   */
  public boolean getRemovable();

  /**
   * creates a clone of the object itself
   *
   * @return new instance of this Field
   *
   * @throws CloneException TODO
   */
  public InputField cloneField() throws CloneException;

  /**
   * Creates a button to remove this field. The value in the inputfiled will be read by the controller-servlet, which is removing this field from the fieldarray.
   * <pre> it is neccesarry to send a name-value-pair so that the controller is able to find the page on which the field was placed, an to show this again.</pre>
   * <pre><b>FIXME find a way to create a button to remove without the border</b> - done - check  border='0' around the img object</pre>
   *
   * @return a String representing a button to remove this.
   *
   * @see Controller
   */
  public String createRemoveButton();

  /**
   * TODO: 
   *
   * @param msg TODO
   *
   * @return TODO
   */
  public String createRemoveButton(String msg);

  /**
   * TODO: 
   *
   * @param msg TODO
   * @param img TODO
   *
   * @return TODO
   */
  public String createRemoveButton(String msg, String img);
}
