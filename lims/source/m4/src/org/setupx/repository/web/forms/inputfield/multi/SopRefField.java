package org.setupx.repository.web.forms.inputfield.multi;

import org.hibernate.Session;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.document.DocumentCoreObject;
import org.setupx.repository.core.communication.document.sop.StandardOperationProcedure;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.web.forms.inputfield.StringInputfield;

/**
 * @hibernate.subclass
 */
public class SopRefField extends StringInputfield{

    // only for persistence layer
    public SopRefField(){
        super();
    }
    
    public SopRefField(long sopUOID) {
        super("QUE","DESC");
        // referencing an sop by using the value as a ling to SOPs
        this.setRestriction(null);
        this.setValue("" + sopUOID);
    }
    
    
    /**
     * receive the SOP that is related to this inputfiled
     * @return
     * @throws PersistenceActionFindException 
     */
    public StandardOperationProcedure getStandardOperationProcedure() throws PersistenceActionFindException{
        long sopUOID = 0;
        
        // trying to determine SOP ID
        try {
            sopUOID = Long.parseLong("" + this.getValue());
        } catch (Exception e) {
            throw new PersistenceActionFindException(e);
        }
        
        // load sop
        StandardOperationProcedure standardOperationProcedure = null;
        try {
            Session s = CoreObject.createSession();
            standardOperationProcedure= (StandardOperationProcedure)DocumentCoreObject.persistence_loadByID(StandardOperationProcedure.class,s, sopUOID);
        } catch (Exception e) {
            throw new PersistenceActionFindException(e);
        }
        
        if (standardOperationProcedure == null) throw new PersistenceActionFindException("object is null");

        return standardOperationProcedure;
    }
    
    public String createHTML(int col){
        try {
            StringBuffer buffer = new StringBuffer();
            buffer.append("\n<!-- " + Util.getClassName(this) + " -->\n<tr><td>\n");
            
            buffer.append("<table border=\"0\">");
            buffer.append("<tr>");
            buffer.append("<td colspan=\"2\"><hr>");
            buffer.append("</tr>");
            buffer.append("<tr>");
            buffer.append("<td colspan=\"2\">");
            buffer.append("<font style=\"font-size: xx-small;\">issued by: Responsible: <nobr><a href=\"mailto:" + this.getStandardOperationProcedure().getResponsible().getEmailString() + "\">" + this.getStandardOperationProcedure().getResponsible().getDisplayName() + "</a></nobr>");
            buffer.append("</font>");
            buffer.append("</td></tr>");
            buffer.append("<tr><td colspan=\"2\">");
            buffer.append("<h3> " + this.getStandardOperationProcedure().getLabel() + " (Version " + this.getStandardOperationProcedure().getVersion() + ")</h3>");
            buffer.append("</td></tr><tr><td>");
            buffer.append("<b>Description</b><br>");
            buffer.append(this.getStandardOperationProcedure().getContent().replaceAll("\n", "<br>"));
            buffer.append("</td>");
            buffer.append("</td> </tr>  </table>");

            buffer.append("\n</td></tr>\n");

            return buffer.toString();
        } catch (Exception e) {
            return "";
        }
    }
}
    