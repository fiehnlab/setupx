/**
 * ============================================================================ File:    RestrictionOBO_human_anatomy.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.Config;
import org.setupx.repository.Settings;
import org.setupx.repository.core.communication.ontology.obo.SourceDefinition;


/**
 * @hibernate.subclass
 */
public class RestrictionOBO_human_anatomy extends OBORestriction {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** the source file containin the ontology in valid OBO Format. */
  String source = Settings.getValue("data.obo.humananatomy", Config.DIRECTORY_OBO + "human-dev-anat-staged.obo");

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#setSource()
   */
  public void setSource(String _source) {
    this.source = _source;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#getSource()
   */
  public String getSource() {
    return this.source;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#cloneRestriction()
   */
  public Restriction cloneRestriction() {
    return new RestrictionOBO_human_anatomy();
  }

  public SourceDefinition getSourceDefinition() {
      return new SourceDefinition("Human Anatomy Ontology", "http://www.obofoundry.org/cgi-bin/detail.cgi?id=human-dev-anat-staged");
  }
}
