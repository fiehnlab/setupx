/**
 * ============================================================================ File:    TimeInputfield.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import org.setupx.repository.web.forms.restriction.Restriction4Time;


/**
 * Inputfield for any time information
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.21 $
 *
 * @hibernate.subclass
 */
public class TimeInputfield extends StringInputfield {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String DEFAULT_TIME = "03:00 pm";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new TimeInputfield object.
   */
  public TimeInputfield() {
    super("time", "hh:mm am");
    //this.setValue(org.setupx.repository.core.util.Util.getTime());
    this.setValue("03:00 pm");
    this.setRestriction(new Restriction4Time());
    this.setSize_x(10);
    this.setHelptext("This field requieres the input of a specific time. Please write the time in the following way: <p>At first the two digets that represent the hour and aftern a the seperator \": \" the two digets representing the minutes. <br>Like 23:12 or 02:09");
  }

  /**
   * @param time
   */
  public TimeInputfield(Object time) {
    this();
    this.setValue(time);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() {
    return new TimeInputfield(this.getValue());
  }
}
