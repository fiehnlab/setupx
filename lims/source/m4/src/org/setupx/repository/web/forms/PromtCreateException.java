package org.setupx.repository.web.forms;
public class PromtCreateException extends Exception {
    public PromtCreateException(Exception e1) {
        super(e1);
    }

    public PromtCreateException(String string, Throwable exception) {
        super(string, exception);
    }

}
