/**
 * ============================================================================ File:    ImageCreater.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import org.setupx.repository.web.forms.inputfield.multi.Promt;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 */
public class ImageCreater {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  static Graphics g = null;
  static Frame frame = null;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private ImageCreater() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param promt TODO
   *
   * @return TODO
   */
  public static Image createSummary(Promt promt) {
    // invisable frame
    frame = new Frame();
    frame.addNotify();

    // create image
    Image image = frame.createImage(860, 300);
    g = image.getGraphics();

    // draw hello off screen
    Graphics2D g2 = (Graphics2D) g;

    // mapped servlet name
    g2.rotate(java.lang.Math.toRadians(90));
    g.setColor(new Color(0, 0, 200));
    g2.drawString("a�ldsjfa�lkdsf", 10, -10);

    g2.rotate(java.lang.Math.toRadians(-80));
    g2.setColor(new Color(200, 200, 100));
    g2.setFont(new Font("Comic Sans MS", Font.BOLD, 40));
    g2.drawString("a�ldsjfa�lkdsf", 70, 100);
    g2.drawLine(30, 170, 370, 170);

    return image;
  }

  /**
   * TODO: 
   *
   * @param args TODO
   */
  public static void main(String[] args) {
  }
}
