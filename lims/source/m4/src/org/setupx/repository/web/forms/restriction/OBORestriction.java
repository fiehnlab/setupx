/**
 * ============================================================================ File:    OBORestriction.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.core.InitException;
import org.setupx.repository.core.communication.ontology.obo.OBOConnector;
import org.setupx.repository.core.communication.ontology.obo.OBOFinderException;
import org.setupx.repository.core.communication.ontology.obo.OBOTerm;
import org.setupx.repository.core.communication.ontology.obo.SourceDefinition;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.ValidationAnswer;
import org.setupx.repository.web.forms.validation.ValidationException;


/**
 * Restriction validating an Object against the defined Ontology.
 * 
 * <p>
 * The ontology is defined in the derivates of this class.
 * </p>
 * 
 * <p>
 * Validation looks up if the actual term in the ontology.
 * </p>
 * 
 * <p>
 * If the term does exist as an actual  term the anser is null. In case the term is not found the validation process looks for synonyms in the ontology.
 * </p>
 * 
 * <p>
 * If found the <code>Validationanswer</code> will contain a <code>HTML</code>  link to set the real value in this field.
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @hibernate.subclass
 *
 * @see http://obo.sourceforge.net/
 */
public abstract class OBORestriction extends Restriction {
    //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    OBOConnector connector = null;

    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new OBORestriction object.
     */
    public OBORestriction() {
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * set the source of the restriction.
     * 
     * <p>
     * The _source points to the file containing the ontology.
     * </p>
     * 
     * <p>
     * The source has to be a valid OBO file.
     * </p>
     *
     * @param _source OBO ontology
     *
     * @see http://obo.sourceforge.net/
     */
    public abstract void setSource(String _source);

    /**
     * @return file containing the OBO ontology
     */
    public abstract String getSource();

    /*
     *  (non-Javadoc)
     * @see org.setupx.repository.web.forms.restriction.Restriction#validateObject(org.setupx.repository.web.forms.inputfield.InputField)
     */
    public ValidationAnswer validateObject(InputField field2)
    throws ValidationException {
        ValidationAnswer answer = null;
        OBOTerm term = null;

        if (this.connector == null) {
            try {
                this.connector = OBOConnector.newInstance(new java.io.File(this.getSource()));
            } catch (InitException e) {
                warning(this, "unable to init " + Util.getClassName(this));
                e.printStackTrace();
            }
        }

        Logger.debug(this, "checking : " + field2.getValue());

        try {
            term = connector.determineTerm("" + field2.getValue());
            Logger.debug(this, "(1) : " + term.toString());

            return null;
        } catch (OBOFinderException e) {
            //e.printStackTrace();
            answer = new ValidationAnswer(field2);

            // adding a link to the source website / should be replaced at some point by a browser through the different ontologies. 
            // Right now it is a static link to a website that has shows the ontology.
            if (this.getSourceDefinition() != null){
                answer.setMsg("Please specify this term by using a term from the " + this.getSourceDefinition().getOntologyName() + ". See details at their website: <a href=\"" +  this.getSourceDefinition().getOntologyLink() + "\" target=\"new\">LINK</a>");
            } else {
                answer.setMsg("The term \"" + field2.getValue() + "\" is not defined in the ontology (" + this.getSource() + ").");
            }
        }


        return answer;
    }

    
    /**
     * defines the source of an ontology in order to allow links to the related obo.
     * @return
     */
    public abstract SourceDefinition getSourceDefinition();
}
