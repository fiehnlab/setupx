package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.web.forms.FormObject;

/**
 * container with a variable number of fields representing SOPs
 * @author scholz
 * @hibernate.subclass
 */
public class SOPMultiField extends MultiField {
    static Object thiz = new SOPMultiField();
    
    public SOPMultiField() {
        // in the beginning it is entirely empty
        super("Standard Operation Procedures", "List of all the SOPs that are related to / were used in this experiment.");
    }


    public String createHTML(int col) {
        StringBuffer response = new StringBuffer();
        response.append(super.createHTML(col));
        
        response.append("<tr><td colspan=\"" + (col) + "\" align=\"center\"><a href=\"sop_form_select.jsp\">ADD / REMOVE additional SOPs.</a></td></tr>");

        return response.toString();
    }
    
    
    public SopRefField getSopRefField(long refID){
        FormObject[] formObjects = this.getFields();
        for (int i = 0; i < formObjects.length; i++) {
            SopRefField sopRefField = (SopRefField)formObjects[i];
            try {
                if(sopRefField.getStandardOperationProcedure().getUOID() == refID){
                    return sopRefField;
                }
            } catch (PersistenceActionFindException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
