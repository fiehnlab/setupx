/**
 * ============================================================================ File:    DEVOranganInputfield.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.core.InitException;
import org.setupx.repository.core.communication.ontology.obo.OBOConnector;
import org.setupx.repository.core.communication.ontology.obo.OBOFinderException;
import org.setupx.repository.core.communication.ontology.obo.OBOTerm;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class DEVOranganInputfield extends StringInputfield {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private OBOTerm oboTerm = new OBOTerm();

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static OBOConnector connector;

  static {
    try {
      connector = new OBOConnector();
    } catch (InitException e) {
      e.printStackTrace();
    }
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new DEVOranganInputfield object.
   */
  public DEVOranganInputfield() {
    super();

    //this.setRestriction(new DEVOrganRestriction());
    this.setQuestion("dev - organ");
    this.setDescription("dev - descr");
    this.setAutosubmit(true);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.InputField#setValue(java.lang.Object)
   */
  public void setValue(Object object) {
    // adding the functionality that it determines what PO thinks about the value 
    String stringValue = (String) object;

    try {
      this.oboTerm = connector.determineTerm(stringValue);
      stringValue = this.oboTerm.getTerm();
    } catch (OBOFinderException e) {
      e.printStackTrace();
    }

    super.setValue(stringValue);
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public InputField cloneField() {
    return super.cloneField();
  }

  /**
   * TODO: 
   *
   * @param col TODO
   *
   * @return TODO
   */
  public String createHTML(int col) {
    String result = super.createHTML(col);
    result = result.concat(this.oboTerm.createHTML(col));

    return result;
  }
}
