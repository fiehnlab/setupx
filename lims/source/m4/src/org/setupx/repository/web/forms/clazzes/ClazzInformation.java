/**
 * ============================================================================ File:    ClazzInformation.java Package: org.setupx.repository.web.forms.clazzes cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.clazzes;

import org.setupx.repository.web.forms.FormContainer;


/**
 * Object contains information relevant for the generation of Clazzes.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 */
public interface ClazzInformation extends FormContainer {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * used to be displayed in exportfiles, summaries, ...
   *
   * @return
   */
  public abstract String createShortLabel();

  /**
   * TODO: 
   *
   * @return TODO
   */
  public abstract String toStringHTML();
}
