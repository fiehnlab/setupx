/**
 * ============================================================================ File:    SubmitPage.java Package: org.setupx.repository.web.forms.page cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.page;

import org.setupx.repository.core.util.Util;


/**
 * page used to submit results to the server.
 * <pre>does not have any inputfileds</pre>
 */
public class SubmitPage extends Page {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new SubmitPage object.
   *
   * @param name 
   * @param description 
   */
  public SubmitPage(String name, String description) {
    super(name, description, "");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTML()
   */
  public String createHTML(int col) {
    return "<!-- " + Util.getClassName(this) + " -->\n\n<tr><td colspan=\"" + (col - 1) + "\">" + this.getName() + "<br/><span class=small>" + this.getDescription() + "</span></td><td>Ok, it is done. Thank you.</td></tr>";
  }
}
