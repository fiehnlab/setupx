/**
 * ============================================================================ File:    ClassTable.java Package: org.setupx.repository.web.forms.clazzes cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.clazzes;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.setupx.repository.core.util.Util;
import org.setupx.repository.web.forms.FormRootNotAvailableException;
import org.setupx.repository.web.forms.InActiveException;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;


/**
 * A table containing all Classes generated for this promt.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.3 $
 *
 * @hibernate.subclass
 */
public class ClassTable extends StringInputfield {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ClassTable object.
   */
  public ClassTable() {
    super("Classes", "These are the classes currently calculated based on your information.");
    this.setRequiered(false);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() {
    return new ClassTable();
  }

  /**
   * TODO: 
   *
   * @param col TODO
   *
   * @return TODO
   */
  public String createHTML(int col) {
    StringBuffer buffer = new StringBuffer();
    buffer.append("\n<!-- " + Util.getClassName(this) + " -->\n" + "<tr>\n");

    buffer.append("\n<tr>" + "<td colspan=" + col + " align=\"center\">" + "<table width=\"700\" border=0>" +
      "<tr>        <td>        <b>Classes / Samples</b>        <p>Below you see a representaion of your experiment that you entered in the past pages. All of the factors (Genotype, Treatment, ... define the most" +
      "        important vectors that span the dimensions of a metabolomic study. They are called classes. The actual samples are assigned to one of these        classes. Each class is supposed to contain at a number of " + Clazz.DEFAULT_NUMBER_SAMPLES +
      " samples - all samples in one class are identical - same" +
      "        genotype - same growth conditions,... they are replicates.        <p>        Please assign your samples to the form below. You can replace the labels of the samples by your own lables - it is just important that the information        that is written on the vails, samplecontainer is <b>identical</b> with this information so that the operators in the laboratory are able to match" +
      "        your samples to the actual information in the system.        <p><b>Number of Samples per Class.</b>        <p>The default number of samples is <b>" + Clazz.DEFAULT_NUMBER_SAMPLES + "</b>.We require this number because of staistical reasons." +
      "<ul>        <li>If you want to add additional samples to a class just type in the number of samples that you want to add and to that specific class.        <li>In case that there are more samples listed in this class than you actually have for your experiment, you might remove them by pressing the <img src=\"pics/trash.gif\" border=\"0\">-Button next to the sample." +
      "<li>In case you have no samples for this class you can deactivate/remove the whole class by pressing the <img src=\"pics/active_no.gif\" border=\"0\"> button either by the class itself or in the matrix on top of the page.." +
      "<li>If you want to add samples for this class after you deactivated this class, you can easily activate the class again by pressing the corresponding icon (<img src=\"pics/active_yes.gif\" border=\"0\"> ) in the table on top of this page." +
      "<li>If the number of samples per class does not match the number of samples in all classes you can define the correct number by setting the number of samples for <b>all</b> classes below<br>" +
      "<img src=\"pics/warning.gif\"><b>Attention:</b> the number of samples in <b>all</b> classes will be changed. If you are sure please check the box and type the" + "number of samples that you request.<br>");

    buffer.append(createClassResizeButtons());

    buffer.append("</ul></td></tr></table></td></tr><tr><td colspan=" + col + "><hr></td></tr>");

    buffer.append("<tr><td colspan=" + col + " align=\"center\">" + this.createSampleFieldLink() + "</td></tr>");
    
    buffer.append("<tr><td colspan=" + col + "><hr></td></tr>");
    
    try {
      buffer.append("<td align=\"center\" colspan=\"" + (col) + "\">" + this.root().createSummary().createHTMLTable() + "	<br>" + createFormula());
    } catch (FormRootNotAvailableException e) {
      err("unable to create formula - root element can not be detected.", e);
    }

    buffer.append("</td></tr>\n");

    return buffer.toString();
  }

  
  /**
   * provide a link to a page creating 
   * @return
   */
  private String createSampleFieldLink() {
      StringBuffer buffer = new StringBuffer();
      
      buffer.append("In case you do have additional attributes that you want to assign to each of the samples feeld free to add them here.");
      
      buffer.append("<br><a href=\"sample_sutom_meta_pre.jsp\"><img src=\"pics/go_export_old.gif\"></a>");     
      return buffer.toString();
  }

/**
   * TODO: 
   *
   * @return TODO
   */
  private String createClassResizeButtons() {
    return "<input name='classesresize' value='true' type='checkbox' size=2>" + "<input name='number' type='text' size ='2' value='8'>" + "<input type='submit'>";
  }

  /**
   * @return
   */
  private String createFormula() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("<table><tr>");

    ClazzDimensionDetector detector = null;

    try {
      detector = this.root().getClazzDimensionDetector();
    } catch (FormRootNotAvailableException e) {
      err("unable to create formula - root element can not be detected.", e);

      return "";
    }

    ClazzDimension[] clazzDimensions = detector.getClazzDimensions();

    for (int i = 0; i < clazzDimensions.length; i++) {
      ClazzDimension dimension = clazzDimensions[i];
      
      // hide all entries that have 1 entry!
      if (dimension.size() > 1){
          buffer.append("<td align=\"center\" valign=\"middle\">");
          buffer.append("<b>" + dimension.getMainQuestion() + "</b>");
          buffer.append("<br>");
          buffer.append(dimension.size());
          buffer.append("<br>");
          buffer.append(dimension.getField(0).createLinkedIMG());
          buffer.append("</td>");

          // check if last entry
          if ((i + 1) != clazzDimensions.length) {
            buffer.append("<td align=\"center\" valign=\"middle\">");
            buffer.append(" x ");
            buffer.append("</td>");
          }
      }
    }

    buffer.append("<td align=\"center\" valign=\"middle\">");
    buffer.append(" = ");
    buffer.append("</td>");

    buffer.append("<td align=\"center\" valign=\"middle\">");
    buffer.append("<b>classes</b>");
    buffer.append("<br>");

    try {
      buffer.append(root().numberClasses());
    } catch (FormRootNotAvailableException e1) {
      err("unable to create formula - root element can not be detected.", e1);
    }

    buffer.append("</td>");

    buffer.append("</tr></table>");

    return buffer.toString();
  }
  
  public Node createXMLNode(Document doc) throws InActiveException {
      warning(this, "not xml exported.");
        return null;
  }

}
