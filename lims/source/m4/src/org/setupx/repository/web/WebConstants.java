/**
 * ============================================================================ File:    WebConstants.java Package: org.setupx.repository.web cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web;

import org.setupx.repository.Config;
import org.setupx.repository.core.InitException;
import org.setupx.repository.core.user.access.PromtUserAccessRight;
import org.setupx.repository.core.util.xml.XMLFile;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

import java.io.File;
import java.io.UTFDataFormatException;

import java.util.Hashtable;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.7 $
 */
public class WebConstants {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String PARAM_FILENAME = "file";
  public static final int QUERY_BY_NAME = 1;
  public static final int QUERY_BY_ID = 2;
  public static final int QUERY_PARENT_EXPERIMENT = 3;
  public static final int QUERY_BY_STATUS = 4;
  public static final int QUERY_PARAMETERCOMBINATION = 6;
  public static final String PARAM_ALEX = "alex";
  public static final String PARAM_VAILOFFSET = "vailoffset";
  public static final String PARAM_BLANKS = "blanks";
  public static final String PARAM_NAME = "name";
  public static final String PARAM_GENERATE_TYPE = "generateType";
  public static final String PARAM_IMG_WIDTH = "imgWidth";
  public static final String PARAM_CLASS = "class";
  public static final String PARAM_QUERY_TYPE = "queryType";
  public static final String PARAM_SEARCH_NAME = "parameterName";
  public static final String PARAM_SEARCH_VALUE = "parameterValue";
  public static final String SESS_SAMPLES_REMOVEID = "removeID";
  public static final String PARAM_STATUS = "status";
  public static final String PARAM_ID = "id";
  public static final String PARAM_PASSWORD = "password";
  public static final String PARAM_DOC_TYPE = "docType";
  public static final String SESS_DOCUMENT_ARRAY = "documents";
  public static final String SESS_USER = "user";
  public static final String SESS_ACQUISTION = "acquistion";
  private static Hashtable messages = new Hashtable();
  private static String TAGNAME_MESSAGE = "message";
  private static String ATTRIBUTENAME_ID = "id";
  public static final int GERNERATE_TYPE_ACQUISITION = 20;
  public static final String PARAM_RANDOM = "random";
  public static final String PARAM_MACHINE_ID = "machineID";
  public static final String PARAM_SOP_ID = "sopID";
  public static final String SESS_FORM_PROMT = "form_promt";
  public static final String PARAM_FORM_LASTPAGE = "form_lastpage";
  public static final String PARAM_FORM_TARGET = "form_target";
  public static final String SESS_FORM_POS = "pos";
  public static final String PARAM_HELP = "help";
  public static final String SESS_FORM_MESSAGE = "msg";
  public static final String SESS_FORM_HEADER = "header";
  public static final String BORDER = " border=\"0\"";
  public static final String SESS_MULTIFIELD = "multifield";
  public static final String PARAM_DETAIL = "detail";
  public static final String PARAM_ACTIVATE = "activate";
  public static final String PARAM_AUTOFILL = "autofill";
  public static final String SESS_CLAZZ = "clazz";
  public static final String SESS_SAMPLES = "samples";
  public static final String SESS_SAMPLES_SELECTED = "samples_selected";
  public static final String SESS_LINK_DOWNLOAD = "downloadlink";
  public static final String QUERYSERVLET_DATE1 = "date1";
  public static final String QUERYSERVLET_DATE2 = "date2";
  public static final String QUERYSERVLET_OWNER_NAME = "owner";
  public static final String QUERYSERVLET_EXPERIMENT_ID = "experiment";
  public static final String QUERYSERVLET_CLAZZ_ID = "clazz";
  public static final String QUERYSERVLET_SAMPLE_ID = "sample";
  public static final String QUERYSERVLET_QUERYTYPE = "type";

  /**
   * @deprecated
   */
  public static final int PARAM_LOAD_ACTION_CLONE = PromtUserAccessRight.CLONE;

  /**
   * @deprecated
   */
  public static final int PARAM_LOAD_ACTION_EDIT = PromtUserAccessRight.WRITE;

  /**
   * @deprecated
   */
  public static final int PARAM_LOAD_ACTION_REPORT = PromtUserAccessRight.READ;

  /**
   * @deprecated
   */
  public static final int PARAM_LOAD_ACTION_CREATE = PromtUserAccessRight.CREATE;

  /**
   * @deprecated
   */
  public static final int PARAM_LOAD_ACTION_EXPORT = PromtUserAccessRight.EXPORT;
  public static final String PARAM_LOAD_ACTION = "action";

  /** report in the session */
  public static final String SESS_REPORT = "report";
  public static final String PARAM_PROMTID = "pID";
  public static final String SESS_MACHINE_INFO = "machineInfo";

  static {
    XMLFile file = null;

    try {
      file = new XMLFile(new File(Config.FILE_MESSAGES));
    } catch (UTFDataFormatException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    } catch (SAXException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }

    try {
      fillHashTable(file);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * findes the message to an msgCode
   *
   * @param msgCode the msgCode
   *
   * @return the message
   *
   * @throws WebException if the msgCode is null or if no msg is found for the msgCode
   */
  public static String getMessage(String msgCode) throws WebException {
    if ((msgCode == null) || !messages.containsKey(msgCode)) {
      throw new WebException("no Message defined for messageCode " + msgCode);
    }

    return (String) messages.get(msgCode);
  }

  /**
   * fills the internal hashtable with all msg_s from a configurationFile
   *
   * @param file the config file
   *
   * @throws InitException TODO
   */
  private static void fillHashTable(XMLFile file) throws InitException {
    // hashtable containing all messages as Value and the code as Key
    // is reset everytime this method is called - the old values are removed !!! 
    messages = new Hashtable();

    NodeList nodeList = file.getDocument().getElementsByTagName(WebConstants.TAGNAME_MESSAGE);

    for (int i = 0; i < nodeList.getLength(); i++) {
      Element element = (Element) nodeList.item(i);

      // <message id="23">this is the content.</message>
      String id = element.getAttribute(WebConstants.ATTRIBUTENAME_ID);
      String msg = element.getFirstChild().getNodeValue();

      // checking the values
      if ((id == null) || (msg == null)) {
        throw new InitException("found null for a value in the configfile: " + file.getSourceFile().getAbsolutePath());
      }

      if (msg.length() < 1) {
        new InitException("found illegal value for a attribute in the configfile: " + file.getSourceFile().getAbsolutePath());
      }

      if (id.length() < 1) {
        new InitException("found illegal id in the configfile: " + file.getSourceFile().getAbsolutePath());
      }

      messages.put(id, msg);
    }
  }
}
