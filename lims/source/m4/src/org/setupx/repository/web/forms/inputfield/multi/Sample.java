/**
 * ============================================================================ File:    Sample.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import org.setupx.repository.Config;
import org.setupx.repository.core.communication.experimentgeneration.template.SampleTemplate;
import org.setupx.repository.core.communication.exporting.AcquisitionParameter;
import org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample;
import org.setupx.repository.core.communication.leco.AcquisitionFileQueryException;
import org.setupx.repository.core.util.hotfix.oldsamples.OldSampleMapping;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;
import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.FormRootNotAvailableException;
import org.setupx.repository.web.forms.InActiveException;
import org.setupx.repository.web.forms.event.Event;
import org.setupx.repository.web.forms.event.Event4Acquisition;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.label.Label;
import org.setupx.repository.web.forms.label.ParentIDLabel;
import org.setupx.repository.web.forms.restriction.Restriction4StringLength;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;


/**
 * A single Sample. The Sample itself does not have a value - it is just a collection of differnt inputfields, that do contain the information of this Sample. The Sample does have a set of events - which contain all events that happend to this sample (Creation, Acquistion, ...)
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision:     1.21 $
 *
 * @hibernate.subclass
 */
public class Sample extends MultiField {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Set events = new HashSet();
  private String link;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static String[] tableLabels = null;
  protected static int numberOfAttributes = 0;

  /** the label that will be printed on the sample itself */

  //private Clazz parentClazz;
  private static int lastID;
  private static final String SAMPLE_LABEL_PRE = "sx";
  public static final int STATUS_NOT_RUN = 91;
  public static final int STATUS_RUN = 92;
  private static Object thiz = new Sample();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Sample object.
   */
  public Sample() {
    super("sampleLabel", "");
    this.initFields();
    // not this way - it is not ensured, that the
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public static String createHTMLheader() {
      if (tableLabels == null){
          return "";
      }
    
    StringBuffer response = new StringBuffer();

    for (int i = 0; i < tableLabels.length; i++) {
      response.append("<th>" + tableLabels[i] + "</th>");
    }

    return response.toString();
  }

  /**
   * @return the calue of the comment field.
   */
  public String getComment() {
    return ((InputField) this.getField(2)).getValue().toString();
  }

  /**
   * @param events the events for this sample
   */
  public void setEvents(Set events) {
    this.events = events;
  }

  /**
   * @hibernate.set
   * @hibernate.collection-key column="SAMPLE"
   * @hibernate.collection-one-to-many  class="org.setupx.repository.web.forms.event.Event"
   */
  public Set getEvents() {
    return this.events;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#getField(int)
   */
  public FormObject getField(int i) {
    if (this.getFields().length <= i) {
      if (Config.LOGGING_SHOW_MISSING_FIELDS) {
        throw new ArrayIndexOutOfBoundsException("the position " + i + " is not available. The size of the array is : " + this.getFields().length);
      }

      if (Config.SAMPLE_FIELD_AUTOCORRECTION) {
        this.initFields();
      }
    }

    return this.getFields()[i];
  }

  /**
   * @deprecated
   */
  public String getID() {
    return ""; //+ this.inputfield_id.getValue();
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getLabel() {
    /*try {
       return ((InputField)this.getField(1)).getValue() + " " + ((InputField)this.getField(2)).getValue();
       }catch (Exception e) {*/
    return "-NA-";
    //}
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#setValue(java.lang.Object)
                                       public void setValue(Object object) {
                                         super.setValue(object);
                                         int pos = 2;
                                         // HOTFIX
                                         try {
                                           ((InputField) this.getField(pos)).setValue(object);
                                         } catch (ArrayIndexOutOfBoundsException e) {
                                           err("it is not possible to change value of object on position " + pos + " cause the array does only have a sice of " + this.size(), e);
                                         } catch (NullPointerException e) {
                                           err("it is not possible to change value of object on position " + pos + " cause the array does only have a sice of " + this.size(), e);
                                         }
                                       }
   */

  /**
   * adds an Event to this sample
   *
   * @param event the event that will be added
   */
  public void addEvent(Event event) {
    if ((this.events == null) && Config.SAMPLE_FIELD_AUTOCORRECTION) {
      this.initEvents();
    }

    this.events.add(event);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() throws CloneException {
    Sample newInstance = new Sample();

    return newInstance;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTML()
   */
  public String createHTML(int col) {
    StringBuffer response = new StringBuffer();

    // create a table containing 
    response.append("\n<!-- " + this.getName() + " (" + this.getDescription() + ") -->");
    response.append("\n<tr>");

    for (int i = 0; i < this.size(); i++) {
      StringInputfield object = (StringInputfield) this.getField(i);
      response.append("\n<td align=\"center\">");

      if (object instanceof Label) {
        response.append(((Label) object).getDynamicValue());
      } else {
        // just the inputfield
        response.append(object.createField());
      }

      response.append("</td>");
    }

    response.append("\n<td>");

    // in case there is a remove button - add it - otherwise add the deactivate button
    if (this.createRemoveButton().length() > 2) {
      response.append(this.createRemoveButton());
    } else {
      // deactivate the whole clazz
      response.append(((Clazz) this.getParent()).createActivateButton());
    }

    response.append("</td>");

    response.append("\n</tr>\n");

    return response.toString();
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.InputField#createLinkedIMG()
   */
  public final String createLinkedIMG() {
    return this.link;
  }

  /**
   * load the samples based on if they have been send into acquistion or not
   *
   * @param b true - load all samples that have been sheduled - <br>false - all samples that have <b>NOT</b> been sheduled
   *
   * @return List of samples mathing.    SELECT parameter.value , sample.UOID as sampleID, event.uoid as eventID, acqsample.uoid as acqSample from event as event, formobject as sample , acquisitionsample as acqsample , acquisitionparameter as parameter  where sample.discriminator =
   *         'org.setupx.repository.web.forms.Sample'   AND   event.discriminator  = 'org.setupx.repository.web.forms.event.Event4Acquisition'   AND sample.uoid = event.fk_sample AND event.uoid = acquisition_sample_id AND acqsample.uoid = event.fk_sample  AND parameter.fk_acqsample = acqsample.uoid
   *         AND parameter.label = 'Name'
   *
   * @throws PersistenceActionFindException
   */
  public static Sample findByAcquisitionName(String name)
    throws PersistenceActionFindException {
    log(thiz, "findByAcquisitionName");

    log(thiz, "create session");

    Session session = PersistenceConfiguration.createSessionFactory().openSession();

    log(thiz, "create transaction");

    Transaction transaction = session.beginTransaction();

    log(thiz, "create query");

    Query query = session.createQuery("select sample from " + Sample.class.getName() + " as sample, " + AcquisitionSample.class.getName() + " as acqsample, " + AcquisitionParameter.class.getName() + " as parameter, " + Event.class.getName() + " as event " + "WHERE event.SXUOID = event.timestamp " +
        "AND sample.uoid = event.fk_acqsample " + "AND acqsample.uoid = event.acquisition_sample_id  " + "AND parameter.fk_acqsample = acqsample.uoid  " + "AND parameter.value = '" + name + "'");

    /*
       + "WHERE parameter.value = '" + name + "'"
               + "AND parameter.label = 'Name'");
     */
    log(thiz, "list");

    List list = query.list();

    log(thiz, "found " + list.size() + " objects");
    session.close();

    if (list.size() == 0) {
      throw new PersistenceActionFindException("unable to find " + name);
    } else if (list.size() > 1) {
      throw new PersistenceActionFindException("ACHTUNG: unable to find " + name + " found more than one entry for " + name);
    }

    return (Sample) list.get(0);
  }

  /**
   * load the samples based on if they have been send into acquistion or not
   *
   * @param b true - load all samples that have been sheduled - <br>false - all samples that have <b>NOT</b> been sheduled
   *
   * @return List of samples mathing.
   */
  public static List findByAcquistion(boolean b) {
    return null;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public static List findByEvents() {
    return null;
  }

  /**
   * load the samples based on if they have been measured or not
   *
   * @param b true - load all samples that have been measured - <br>false - all samples that have <b>NOT</b> been measured
   *
   * @return List of samples mathing.
   */
  public static List findByMeasured(boolean b) {
    return null;
  }

  /**
   * load the samples based on when they were defined in the system
   *
   * @param period_start
   * @param period_end
   */
  public static List findByTime(Date period_start, Date period_end) {
    return null;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getACQInfo() {
    StringBuffer buffer = new StringBuffer();

    for (Iterator iter = this.getEvents().iterator(); iter.hasNext();) {
      Event event = (Event) iter.next();

      if (event instanceof Event4Acquisition) {
        Event4Acquisition event4Acquisition = (Event4Acquisition) event;

        try {
          buffer.append(" " + event4Acquisition.getAcquisitionSample().getParamter(AcquisitionParameter.NAME).getValue());
        } catch (Exception e) {
          warning(this, e.toString());
        }
      }
    }

    return buffer.toString();
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getCreationDate() {
    Iterator iterator = this.getEvents().iterator();

    while (iterator.hasNext()) {
      Event element = (Event) iterator.next();

      if (element instanceof Event) {
        return element.getTimestamp().toLocaleString();
      }
    }

    return " -- ";
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getPromtID() {
    if (this.uoid == 0) {
      try {
        if (this.root() != null) {
          return "" + this.root().getUOID();
        }
      } catch (FormRootNotAvailableException e1) {
      }
    }

    try {
      return "" + (new SXQuery().findPromtIDbySample(this.getUOID()));
      //}
    } catch (PersistenceActionFindException e) {
      e.printStackTrace();

      return "-NA-";
    }
  }

  /**
   * @return TODO
   */
  public final String determineClazzID() {
    // in case that there is no chached id
    if (this.getParent() == null) {
      try {
        return "" + (new SXQuery().findParent(this.getUOID()));
      } catch (PersistenceActionFindException e) {
        e.printStackTrace();

        return "-NA-";
      }
    } else {
      return "" + this.getParent().getUOID();
    }
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.CoreObject#persistenceChilds(org.hibernate.Session, boolean)
   */
  public void persistenceChilds(Session session, boolean createIt)
    throws PersistenceActionException {
    Iterator iterator = this.getEvents().iterator();

    while (iterator.hasNext()) {
      ((Event) iterator.next()).update(session, createIt);
    }

    super.persistenceChilds(session, createIt);
  }

  //public static Sample findByAcquisitionName(String name) throws PersistenceActionFindException {

  /**
   * select formobject.uoid as FormObject, formobject.parent_id as Class , acquisitionparameter.value as AcquistionLabel  from acquisitionparameter, formobject , event, acquisitionsample  where formobject.discriminator = 'org.setupx.repository.web.forms.Sample' AND event.eventtype =
   * 'org.setupx.repository.web.forms.event.Event4Acquisition'  AND event.sample_event = formobject.uoid  AND acquisitionsample.id = event.acquisition_sample_id  AND acquisitionsample.id = acquisitionparameter.acq_sample_id  AND acquisitionparameter.value = '021005akbsa13' AND
   * acquisitionparameter.label = 'Name'
   */

  //Session session = PersistenceConfiguration.createSessionFactory().openSession();

  /*
     SQLQuery query = session.createSQLQuery("from formobject.* " +
                     " from acquisitionparameter, formobject , event, acquisitionsample" +
                     " where formobject.discriminator = 'org.setupx.repository.web.forms.Sample'" +
                     " AND event.eventtype = 'org.setupx.repository.web.forms.event.Event4Acquisition'" +
                     " AND event.sample_event = formobject.uoid" +
                     " AND acquisitionsample.id = event.acquisition_sample_id" +
                     " AND acquisitionsample.id = acquisitionparameter.acq_sample_id" +
                     " AND acquisitionparameter.value = '" + name + "'" +
                     " AND acquisitionparameter.label = 'Name'");
         SQLQuery query = session.createSQLQuery("select formobject.* " +
                     " from acquisitionparameter, formobject , event, acquisitionsample" +
                     " where formobject.discriminator = 'org.setupx.repository.web.forms.Sample'" +
                     " AND event.eventtype = 'org.setupx.repository.web.forms.event.Event4Acquisition'" +
                     " AND event.sample_event = formobject.uoid" +
                     " AND acquisitionsample.id = event.acquisition_sample_id" +
                     " AND acquisitionsample.id = acquisitionparameter.acq_sample_id" +
                     " AND acquisitionparameter.value = '" + name + "'" +
                     " AND acquisitionparameter.label = 'Name'");
   */
  /*
     debug(thiz, "creating a sqlquery");
     SQLQuery query = session.createSQLQuery("select form.* from formobject form");
     debug(thiz, "adding entitiy");
     query.addEntity("form", FormObject.class);
  
     query.setMaxResults(10);
  
     debug(thiz, "listing");
     List list = query.list();
     log(thiz, "found " + list.size()  + " objects");
     if (list.size() == 0) throw new PersistenceActionFindException("unable to find a sample that is sheduled with the label " + name);
     else return (Sample)list.get(0);
      /*
      List list = Sample.persistence_loadAll(Sample.class, session);
      warning(null, "has to be redone !!   Sample findByAcquisitionName(String name) {");
      for (int i = 0; i < list.size(); i++) {
        Sample sample = (Sample) list.get(i);
        Set events = sample.getEvents();
        Iterator iter = events.iterator();
        while (iter.hasNext()) {
          Event event = (Event) iter.next();
          debug(thiz, "got an object - id:" + event.getId() + " comment:" + event.getComment() + " " + Util.getClassName(event));
          if (event instanceof Event4Acquisition) {
            Event4Acquisition event4Acquisition = (Event4Acquisition) event;
            String acquisitionsample_name = "";
            try {
              acqSample = event4Acquisition.getAcquisitionSample();
              debug(thiz, acqSample);
              parameter = acqSample.getParamter(AcquisitionParameter.NAME);
              debug(thiz, parameter);
              acquisitionsample_name = parameter.getValue();
              debug(thiz, acquisitionsample_name);
              debug(thiz, "comparing " + acquisitionsample_name + " and  " + name);
              if (acquisitionsample_name.compareTo(name) == 0) {
                log(thiz, "GOT IT");
                session.close();
                return sample;
              }
            } catch (ParameterException e) {
              err(thiz, e);
            }
          }
        }
      }
  
      session.close();
      throw new PersistenceActionFindException("unable to find a sample that is sheduled with the label " + name);
   */

  //}

  /**
   * converts
   *
   * @param list
   *
   * @return
   */
  public static final Sample[] toArray(List list) {
    Sample[] samples = new Sample[list.size()];

    for (int i = 0; i < samples.length; i++) {
      samples[i] = (Sample) list.get(i);
    }

    return samples;
  }

  /**
   * TODO: 
   *
   * @param set TODO
   *
   * @return TODO
   */
  public static Sample[] mapToSampleArray(HashSet set) {
    Sample[] samples = new Sample[set.size()];
    Iterator iterator = set.iterator();
    int i = 0;

    while (iterator.hasNext()) {
      samples[i] = (Sample) iterator.next();
      i++;
    }

    return samples;
  }

  /**
   * @param element
   */
  public void setComment(String element) {
    ((InputField) this.getField(2)).setValue(element);
  }

  /**
   * @return
   *
   * @throws PersistenceActionFindException
   */
  public static Sample loadByID(long id) throws PersistenceActionFindException {
    Session session = PersistenceConfiguration.createSessionFactory().openSession();

    return (Sample) Sample.persistence_loadByID(Sample.class, session, id);
  }

  /**
   * TODO: 
   *
   * @param sampleName TODO
   */
  public void setSampleName(String sampleName) {
    setComment(sampleName);
  }

  /**
   * TODO: 
   *
   * @param sampleTemplate TODO
   *
   * @return TODO
   */
  public static Sample map(SampleTemplate sampleTemplate) {
    Sample newSample = new Sample();

    // mapping values
    newSample.setComment(sampleTemplate.getValue(OldSampleMapping.OPERATOR_COMMENT));
    //newSample.setLabel("MPI-oldImport...");
    sampleTemplate.setAssignedSample(newSample);

    return newSample;
  }

  /**
   * converts to an ExperimentSample for BinBase use
   *
   * @return an ExperimentSample for BinBase
   *
   * @see ExperimentSample
   * @see org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector
   */
  public ExperimentSample toExperimentSample() {
    //new SXQuery().findAcquisitionNameBySampleID(this.getUOID());
    return new ExperimentSample("" + this.getUOID(), "");
  }

  /**
   * generates a unique id. FIXME - database find last ID  protected static String generateID() { lastID++; return SAMPLE_ID_PRE + lastID + "_" + new Date().getTime(); }
   */
  /**
   * generates a label.
   *
   * @return
   */
  private static String generateLabel() {
    lastID++;

    return SAMPLE_LABEL_PRE + lastID + "";
  }

  /**
   *
   */
  private void initEvents() {
    debug("initEvents()");
    this.events = new HashSet();
  }

  /**
   * TODO: 
   */
  private void initFields() {
    // in case that the fields are not initalized
    if (this.getFields().length != 0) {
      debug("initFields() - fields do exist - so terminating initFields()");

      return;
    }

    this.addEvent(new Event("Sample created in the System."));

    //debug("initFields() - number of existing fields: " + this.getFields().length);
    this.setFields(new FormObject[0]);

    //this.parentClazz = clazz;
    InputField inputfield_id = new ParentIDLabel("Sample ID", "uniqe ID for this sample");
    this.addField(inputfield_id);

    this.link = inputfield_id.createLinkedIMG();

    StringInputfield inputfield_label = new StringInputfield("label", "label which can be found on the sample vails", Sample.generateLabel(), false);
    inputfield_label.setSize_x(6);
    this.addField(inputfield_label);

    StringInputfield inputfield_comment = new StringInputfield("comment", "any information you want to store", "-your comment-", false);
    inputfield_comment.setSize_x(40);
    inputfield_comment.setRestriction(new Restriction4StringLength(3));
    //inputfield_comment.setRestriction(null);
    this.addField(inputfield_comment);
    
    this.setRemovable(true);

    if (this.tableLabels == null) {
      initTableLabels();
    }
  }

  public void initTableLabels() {
      // new: in case the number of attributes for this sample is more than the existing ones - then add some of them
      debug("init: already had " + numberOfAttributes + " labels for the tables. " );

      if (this.size() > numberOfAttributes){
          numberOfAttributes = this.size();

          debug("init: now creating " + numberOfAttributes + " labels for the tables.");
          tableLabels = new String[numberOfAttributes];

          try {
            for (int i = 0; i < numberOfAttributes; i++) {
              //debug("init: tablelabels: count i: " + i);
              InputField field = (InputField) this.getField(i);
              debug("init: tablelabels: field: " + field);

              // FIXME 
              // returnvalue is not correct - is always 0 - not the position in the array. int pos = this.getFieldPos(inputfield_id.getName());
              int pos = i; // HOTFIX

              tableLabels[pos] = field.getQuestion() + "<br><span class=\"small\">" + field.getDescription() + "</span>";
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
      }
  }

public void resetHTMLheader() {
    tableLabels = null;
    numberOfAttributes = 0;
    this.initTableLabels();
}

public final String getXMLChildname() {
   return "sample";
}


public Node createXMLNode(Document doc) throws InActiveException {
    Node node = super.createXMLNode(doc);
    
    Hashtable hashtable = super.getExportElements();
    
    // add each run
    List scannedList = new SXQuery().findScannedPair(this.uoid);
    Iterator iterator = scannedList.iterator();
    
    int i = 0;
    while (iterator.hasNext()) {
        i++;
        String label = "" + iterator.next();
        
        Element runElement = doc.createElement("run");
        runElement.setAttribute("repetition", "" + i);
        
        
        // todo - add all related files
        // for each file ...
        Element fileElement = doc.createElement("file");
        fileElement.setAttribute("source", label);
        runElement.appendChild(fileElement);
        // end for each file
        
        // run attributes
        Element machineSetupElement = doc.createElement("setup");
        // add all the attributes
        Hashtable ht;
        try {
            try {   
                label = label.substring(0, label.indexOf(":"));
            } catch (Exception e) {
                label = label.substring(0, label.indexOf("_"));
            }

            ht = org.setupx.repository.core.communication.leco.LecoACQFile.getValuesPerACQname(label);
            Enumeration enumeration = ht.keys();
            while(enumeration.hasMoreElements()) { 
                String key = (String)enumeration.nextElement();
                String value = (String)ht.get(key);                
                machineSetupElement.setAttribute(key, value);
            }
            runElement.appendChild(machineSetupElement);
        } catch (AcquisitionFileQueryException e) {
            e.printStackTrace();
        } 
        node.appendChild(runElement);
    }    
    return node;
}
}
