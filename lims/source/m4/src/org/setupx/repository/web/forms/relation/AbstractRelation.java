/**
 * ============================================================================ File:    AbstractRelation.java Package: org.setupx.repository.web.forms.relation cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.relation;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.server.persistence.hibernate.SXSerializable;
import org.setupx.repository.web.forms.ComparisonsException;


/**
 * @hibernate.class table = "relation"
 * @hibernate.discriminator column = "discriminator"
 *
 * @see org.setupx.repository.web.forms.relation.Relation
 */
public abstract class AbstractRelation extends CoreObject implements SXSerializable {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param strings TODO
   */
  public abstract void setConditions(String[] strings);

  /**
   * TODO: 
   *
   * @return TODO
   */
  public abstract String[] getConditions();

  /**
   * updates a relation
   * 
   * <p>
   * <b>this is not <code>peristence</code> - it is the function to update the relation and its relation targets.!</b>
   * </p>
   *
   * @param string the value that will be set and compared to the requiered values.
   */
  public abstract void update(String string);

  /**
   * TODO: 
   *
   * @param condiSet TODO
   */
  public void setConditionsHibernate(Set condiSet) {
    this.setConditions(mapStrings(condiSet));
  }

  /**
   * @hibernate.property type = "serializable"
   */
  public Set getConditionsHibernate() {
    return map(getConditions());
  }

  /**
   * The setter method for this Customer's identifier.
   */
  public void setUOID(long lId) {
    this.uoid = lId;
  }

  /**
   * @hibernate.id column = "uoid" generator-class="native"
   */
  public long getUOID() {
    return (uoid);
  }

  static Set map(String[] conditions2) {
    HashSet hashSet = new HashSet();

    for (int i = 0; i < conditions2.length; i++) {
      hashSet.add(conditions2[i]);
    }

    return hashSet;
  }

  /**
   * @param condiSet
   *
   * @return
   */
  private static String[] mapStrings(Set condiSet) {
    if (condiSet == null) {
      return new String[0];
    }

    String[] array = new String[condiSet.size()];
    Iterator iterator = condiSet.iterator();
    int i = 0;

    while (iterator.hasNext()) {
      array[i] = (String) iterator.next();
      i++;
    }

    return array;
  }

public void compare(AbstractRelation relation2) throws ComparisonsException{
    if (this.getUOID() != relation2.getUOID())throw new ComparisonsException(this, relation2, "UOID");
    if (this.getConditions().length != relation2.getConditions().length)throw new ComparisonsException(this, relation2, "number of conditions");
}
}
