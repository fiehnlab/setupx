/**
 * ============================================================================ File:    DateInputfield.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import org.setupx.repository.web.forms.restriction.Restriction4Date;

import java.text.DateFormat;

import java.util.Date;
import java.util.Locale;


/**
 * Inputfield for a Datevalue
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.21 $
 *
 * @hibernate.subclass
 */
public class DateInputfield extends StringInputfield {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new DateInputfield object.
   */
  public DateInputfield() {
    super("date", "");
    this.setValue(new Date());
  }

  /**
   * Creates a new DateInputfield object.
   *
   * @param date 
   */
  public DateInputfield(Object date) {
    this("date", "");
    this.setValue(date);
  }

  /**
   * @param string
   * @param string2
   */
  public DateInputfield(String question, String descruption) {
    this(question, descruption, new Date());
  }

  /**
   * @param string
   * @param string2
   * @param seedingDate
   */
  public DateInputfield(String string, String string2, Object date) {
    super(string, string2);
    this.setSize_x(20);
    //this.setValue(org.setupx.repository.core.util.Util.getDate());
    this.setValue(date);
    this.setRestriction(new Restriction4Date(new Date()));
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.InputField#setValue(java.lang.Object)
   */
  public void setValue(Object value) {
    if (value instanceof Date) {
      Date castedDate = (Date) value;
      super.setValue(DateFormat.getDateInstance(DateFormat.SHORT, Locale.US).format(castedDate));
    } else {
      super.setValue(value);
    }
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() {
    return new DateInputfield(this.getQuestion(), this.getDescription(), this.getValue());
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.StringInputfield#createField()
   */
  public String createField() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("<input type=\"Text\" name=\"" + this.getName() + "\" value=\"" + this.getValue() + "\">" + "<a href=\"javascript:cal" + this.getName() + ".popup();\"><img src=\"pics/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Click Here to Pick up the date\"></a><br> " +
      "<script language=\"JavaScript\"> <!-- \n" + "var cal" + this.getName() + " = new calendar1(document.forms['default'].elements['" + this.getName() + "']); " + "cal" + this.getName() + ".year_scroll = false; " + "cal" + this.getName() + ".time_comp = false;  //--></script>");

    //"<a name=\"" + this.getName() + "\"/><input type=\"text\" name=\"" + this.getName() + "\"  size=\"" + this.getSize_x() + "\"  " + submit + " value=\"" + (String) this.getValue() +
    //"\"/> " + this.getExtension());
    buffer.append(this.createErrorMSG());

    return buffer.toString();
  }
}
