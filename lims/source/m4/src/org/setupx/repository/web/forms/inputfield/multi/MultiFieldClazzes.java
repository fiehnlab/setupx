/**
 * ============================================================================ File:    MultiFieldClazzes.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.core.util.Util;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.PersistenceActionStoreException;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.FormRootNotAvailableException;
import org.setupx.repository.web.forms.clazzes.ClazzDimensionDetector;

import org.hibernate.Session;

import java.util.HashSet;


/**
 * Inputfield that contains all the differen clazzes and the samples related to that clazz.
 * 
 * <p>
 * Do not add Fields to this field - they will not be checked - cause the createHTML method  is overwritten.
 * </p>
 * 
 * <p>
 * <code>4oct2006:</code>
 * 
 * <ul>
 * <li>
 * adding button to change the total number of samples in <b>all</b> clazzes.
 * </li>
 * <li>
 * major changing on the text
 * </li>
 * </ul>
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.36 $
 *
 * @hibernate.subclass
 */
public class MultiFieldClazzes extends MultiField {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @link the only instance in this promt
   */
  private ClazzDimensionDetector clazzDimensionDetector = null;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new MultiFieldClazzes object.
   */
  public MultiFieldClazzes() {
    super("Classes", "All classes in this experiment");
    this.dividerHR = true;

    // if this is saved already - check the childs
    if (this.uoid != 0) {
      try {
        validateChildUOIDs();
      } catch (PersistenceActionStoreException e) {
        throw new RuntimeException(e);
      }
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


  /**
   * returns the single instance of the Clazzdimensiondetector
   *
   * @return single instance of the Clazzdimensiondetector
   *
   * @throws FormRootNotAvailableException
   */
  public ClazzDimensionDetector getClazzDimensionDetector()
    throws FormRootNotAvailableException {
    if (this.clazzDimensionDetector == null) {
      // convert this.fileds 
      Clazz[] clazzes = Clazz.convertToClazz(this.getFields());

      if ((this.getFields() == null) || (this.getFields().length == 0)) {
        clazzes = null;
      }

      this.clazzDimensionDetector = new ClazzDimensionDetector(this.root(), clazzes);
    }

    this.setFields(this.clazzDimensionDetector.getClazzes());

    return this.clazzDimensionDetector;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTML()
   */
  public String createHTML(int col) {
    Clazz[] clazzs = null;

    try {
      // just update the number of clazzes and then add them as fields to this - after that the super method for the html creation
      this.getClazzDimensionDetector().update();
      clazzs = this.getClazzDimensionDetector().getClazzes();
    } catch (FormRootNotAvailableException e) {
      e.printStackTrace();
    }

    this.setFields(clazzs);

    String buttons = this.createButtons();

    String childs = this.createHTMLChilds(col);

    return buttons.concat(childs);
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.CoreObject#persistenceChilds(org.hibernate.Session, boolean)
   */
  public void persistenceChilds(Session session, boolean createIt)
    throws PersistenceActionException {
    super.persistenceChilds(session, createIt);

    debug("making sure that all classes and samples were correctly stored.");

    validateChildUOIDs();
  }

  /**
   * create all buttons that are relevant for <b>all</b> classes. They will be shown on top of all classes.
   *
   * @return HTML code containing the buttons.
   */
  private String createButtons() {
    String response = "";

    response = response.concat("");

    return response;
  }

  /**
   * @throws PersistenceActionStoreException
   */
  private void validateChildUOIDs() throws PersistenceActionStoreException {
    try {
      debug("number of clazzes from ClazzDimensionDetector: " + this.getClazzDimensionDetector().getClazzes().length);
      debug("number of clazzes in " + Util.getClassName(this) + ": " + this.getFields().length);
    } catch (FormRootNotAvailableException e) {
      throw new PersistenceActionStoreException(e);
    }

    HashSet names = new HashSet();

    for (int i = 0; i < this.size(); i++) {
      Clazz clazz = (Clazz) this.getField(i);

      if (clazz.getUOID() == 0) {
        throw new PersistenceActionStoreException("there is a class that has not been stored.", new NullPointerException());
      }

      // check that there is no identical name used.
      if (names.contains(clazz.getUOID() + "")) {
        throw new PersistenceActionStoreException("there are classes with the same name.", new NullPointerException());
      }

      names.add(clazz.getUOID() + "");

      FormObject[] formObjects = clazz.getFields();

      debug("number of samples in clazz " + clazz.uoid + ": " + clazz.getFields().length);

      for (int j = 0; j < formObjects.length; j++) {
        Sample sample = (Sample) formObjects[j];

        if (sample.getUOID() == 0) {
          throw new PersistenceActionStoreException("there is a sample that has not been stored.", new NullPointerException());
        }
      }
    }
  }
}
