/**
 * ============================================================================ File:    SPromtCreator.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import org.setupx.repository.web.forms.inputfield.BigStringInputField;
import org.setupx.repository.web.forms.inputfield.DropDown;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.page.Page;
import org.setupx.repository.web.forms.restriction.Restriction4Email;
import org.setupx.repository.web.forms.restriction.Restriction4StringLength;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class SPromtCreator {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public static Promt create() {
    Promt promt = new Promt();

    // inputpage for classinformation, ...
    Page pageIn = new Page("Sampleinformation", "Information about the samples", "");

    StringInputfield inputfieldName = new StringInputfield("name", "First- and Lastname");
    inputfieldName.setRestriction(new Restriction4StringLength(5));

    StringInputfield inputfieldEmail = new StringInputfield("email", "Your email address");
    inputfieldEmail.setRestriction(new Restriction4Email());

    BigStringInputField comment = new BigStringInputField("comment", "please describe the samples");
    comment.setRestriction(null);
    comment.setRows(3);
    comment.setCols(20);

    BigStringInputField classesText = new BigStringInputField("Samples", "Please copy and past ALL your samples in this field. Each of the lines should represent a singel sample.");
    classesText.setRestriction(new Restriction4StringLength(20));

    // TODO - what is a sample
    DropDown down = new DropDown("number of classes", "How many classes do you have in this set of samples?", new String[] { "1", "2", "3", "4", "5", "6" }, false);

    pageIn.addField(inputfieldName);
    pageIn.addField(inputfieldEmail);
    pageIn.addField(comment);
    pageIn.addField(classesText);

    // page for assigning each sample to the classes
    promt.addField(pageIn);

    return promt;
  }
}
