/**
 * ============================================================================ File:    ClazzID.java Package: org.setupx.repository.web cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class ClazzID extends ID {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ClazzID object.
   *
   * @param id 
   */
  public ClazzID(long id) {
    super(id);
  }

  /**
   * Creates a new ClazzID object.
   *
   * @param id 
   */
  public ClazzID(String id) {
    super(id);
  }
}
