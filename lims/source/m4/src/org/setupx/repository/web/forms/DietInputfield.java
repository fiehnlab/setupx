/**
 * ============================================================================ File:    DietInputfield.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import org.setupx.repository.web.forms.clazzes.Treatment;
import org.setupx.repository.web.forms.inputfield.BigStringInputField;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.Removable;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.multi.MultiField;
import org.setupx.repository.web.forms.inputfield.multi.MultiFieldExpanding;
import org.setupx.repository.web.forms.restriction.Restriction4StringLength;


/**
 * @hibernate.subclass
 */
public class DietInputfield extends MultiField implements Treatment {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new DietInputfield object.
   */
  public DietInputfield() {
    super();
  }

  /**
   * Creates a new DietInputfield object.
   *
   * @param question 
   * @param description 
   */
  public DietInputfield(String question, String description) {
    super(question, description);

    InputField field1 = new StringInputfield("Label", "discription of the diet", "diet", false);
    field1.setRestriction(new Restriction4StringLength(8));

    BigStringInputField field2 = new BigStringInputField("comment", "please disribe the diet", false);
    field2.setRestriction(null);

    this.addField(field1);
    this.addField(field2);

    Removable removable = new DietStringInputfield("diet");
    MultiFieldExpanding fieldExpanding = new MultiFieldExpanding("Doses", "define every variation of this diet", "", removable);

    try {
      fieldExpanding.expand(3);
    } catch (ArrayExpandException e) {
      e.printStackTrace();
    }

    this.addField(fieldExpanding);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField#cloneField()
   */
  public InputField cloneField() throws CloneException {
    return new DietInputfield(this.getQuestion(), this.getDescription());
  }
}
