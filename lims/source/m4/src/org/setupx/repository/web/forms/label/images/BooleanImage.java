/**
 * ============================================================================ File:    BooleanImage.java Package: org.setupx.repository.web.forms.label.images cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.label.images;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.setupx.repository.web.forms.InActiveException;
import org.setupx.repository.web.forms.inputfield.StringInputfield;


/**
 * Shows an image depending on the value
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.5 $
 *
 * @hibernate.subclass
 */
public class BooleanImage extends StringInputfield {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String trueIMG = "<img  border='0' src=\"pics/point_2.gif\"/>";
  public static final String falseIMG = "<img  border='0' src=\"pics/point.gif\"/>";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new BooleanImage object.
   */
  public BooleanImage() {
    super("...", "...");
  }

  /**
   * Creates a new BooleanImage object.
   *
   * @param question 
   * @param description 
   * @param value 
   */
  public BooleanImage(String question, String description, boolean value) {
    super(question, description);
    this.setValue("" + value);
    this.setRestriction(null);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String createField() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("<a name=\"" + this.getName() + " \"/>");

    //<input type=\"text\" name=\"" + this.getName() + "\"  size=\"" + this.getSize_x() + "\"  value=\"" + (String) this.getValue() + "\"/>");
    if (Boolean.valueOf((String) this.getValue()).booleanValue()) {
      buffer.append(trueIMG);
    } else {
      buffer.append(falseIMG);
    }

    // error message
    if ((this.validate() != null) && (this.validate().length() > 1)) {
      buffer.append("<!--  error message -->");
      buffer.append("\n<span class=\"error\">" + this.validate() + "</span>");
    }

    return buffer.toString();
  }


  public Node createXMLNode(Document doc) throws InActiveException {
      warning(this, "not xml exported.");
        return null;
  }
}
