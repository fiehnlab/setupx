/**
 * ============================================================================ File:    TreatmentInputfield.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.core.communication.experimentgeneration.template.ClazzTemplate;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.clazzes.AlignX;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.restriction.Restriction4StringLength;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @hibernate.subclass
 */
public class TreatmentInputfield extends MultiField4Clazz implements AlignX {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new TreatmentInputfield object.
   */
  public TreatmentInputfield() {
    super("Treatment", "please describe the way the samples were treatet:");

    StringInputfield inputfield = new StringInputfield("variation", "specific variation of this treatment");
    inputfield.setRestriction(new Restriction4StringLength(4));
    inputfield.setRemovable(true);
    this.addField(inputfield);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getPath() {
    return ClazzTemplate.FUNCTION;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public FormObject getSingleValueField() {
    return this.getField(0);
  }

  /*
   * little variation - list without a header - cause there is just one item inside
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTML(int)
   */
  public String createHTML(int col) {
    StringBuffer buffer = new StringBuffer();

    for (int i = 0; i < this.getFields().length; i++) {
      buffer.append(((InputField) this.getField(i)).createHTML(col));
    }

    return buffer.toString();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.clazzes.ClazzInformation#createShortLabel()
   */
  public String createShortLabel() {
    return toStringHTML();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.MultiField4Clazz#toStringHTML()
   */
  public String toStringHTML() {
    return this.getQuestion() + " " + ((InputField) this.getField(0)).getValue();
  }
}
