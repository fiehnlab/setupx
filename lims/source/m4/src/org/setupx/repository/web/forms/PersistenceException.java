package org.setupx.repository.web.forms;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 * @deprecated
 */

public class PersistenceException extends Exception {
  /**
   * Creates a new PersistenceException object.
   */
  public PersistenceException() {
    super();
  }

  /**
   * Creates a new PersistenceException object.
   *
   * @param message 
   */
  public PersistenceException(String message) {
    super(message);
  }

  /**
   * Creates a new PersistenceException object.
   *
   * @param cause 
   */
  public PersistenceException(Throwable cause) {
    super(cause);
  }

  /**
   * Creates a new PersistenceException object.
   *
   * @param message 
   * @param cause 
   */
  public PersistenceException(String message, Throwable cause) {
    super(message, cause);
  }
}
