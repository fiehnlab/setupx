/**
 * ============================================================================ File:    SubmitButton.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.setupx.repository.web.forms.InActiveException;
import org.setupx.repository.web.forms.restriction.SubmitButtonRestriction;


/**
 * @hibernate.subclass
 */
public class SubmitButton extends StringInputfield {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new SubmitButton object.
   */
  public SubmitButton() {
    super("Final Submit", "" + "When you are finally done with entering the information, just check the box and submit the information to the central server.");
    this.setRemovable(false);
    this.setActive(true);
    this.setRestriction(new SubmitButtonRestriction());
    this.setValue("" + false);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() {
    warning(this, "trying to clone a Submitbutton.");

    return null;
  }

  /**
   * creates just the inputfield - excludung any table formatting
   */
  public String createField() {
    String noSelect = "";
    String yesSelect = "";
    StringBuffer buffer = new StringBuffer();

    // check if the value is selected - otherwise do not select it
    boolean selected = false;

    if (((String) this.getValue()).compareTo("" + true) == 0) {
      debug(this, "SELECETED value: " + this.getValue());
      selected = true;
    } else {
      debug(this, "NOT SELECETED value: " + this.getValue());
    }

    if (selected) {
      yesSelect = "checked=\"true\"";
    } else {
      noSelect = "checked=\"false\"";
    }

    buffer.append("" + "<input type=\"radio\" name=\"" + this.getName() + "\"  value=\"true\" " + yesSelect + " /> OK  " + "<input type=\"radio\" name=\"" + this.getName() + "\"   value=\"false\" " + noSelect + " /> not ok " + this.createHelpButton() + "");
    buffer.append("<br> <input type=\"submit\" value=\"Send Data to SetupX-Server\"/>");

    return buffer.toString();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.InputField#validate()
   */
  public String validate() {
    debug("my value: " + this.getValue());
    debug(this.getValue());

    return super.validate();
  }
  
  
  public Node createXMLNode(Document doc) throws InActiveException {
      warning(this, "not xml exported.");
        return null;
  }

}
