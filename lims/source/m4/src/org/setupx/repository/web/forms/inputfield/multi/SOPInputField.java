/**
 * ============================================================================ File:    SOPInputField.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.core.communication.sop.SOP;
import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.inputfield.BigStringInputField;
import org.setupx.repository.web.forms.inputfield.DropDown;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.KeyValuePair;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.relation.Relation;

import java.util.Iterator;
import java.util.Set;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 *
 * @hibernate.subclass
 */
public class SOPInputField extends MultiField4Clazz {
    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new SOPInputField object.
     *
     * @deprecated replaced by sop - with different SOPs
     */
    public SOPInputField() {
        super("SOP", "Standard Operation Protocol");

        InputField field_sop = new DropDown("Preparation SOP", "Please select the SOP that was used for the Preparation.", new String[] { "Fiehnlab-SOP-xxx1", "Fiehnlab-SOP-xxx2", "Fiehnlab-SOP-xxx3", "Fiehnlab-SOP-xxx4", "Fiehnlab-SOP-xxx5", "Fiehnlab-SOP-xxx6", DropDown.STRING_OTHER }, true);
        field_sop.setHelptext("The SOPs are predefined Protocols for preparing the samples. In case you followed the instructions of the SOPS while repering the samples, please select the SOP-ID.<br>Otherwise please select " + DropDown.STRING_OTHER + " and describe the preparation in the field below.");

        InputField field_sopDescription = new BigStringInputField("Preparation", "in case the Preparation was not done based on an SOP - please specify it here.");
        InputField field_sopShortDescriptin = new StringInputfield("Name", "Short label for the description - how would you label your SOP.");
        Relation relation = new Relation(new String[] { DropDown.STRING_OTHER }, new FormObject[] { field_sopDescription, field_sopShortDescriptin });
        field_sop.addRelation(relation);

        this.addField(field_sop);
        this.addField(field_sopShortDescriptin);
        this.addField(field_sopDescription);
    }

    /**
     * Creates a new SOPInputField object.
     *
     * @param label 
     * @param sops 
     * @param inklOther 
     */
    public SOPInputField(String label, SOP[] sops, boolean inklOther) {
        this(label, convert(sops, inklOther), inklOther);
    }

    /**
     * Creates a new SOPInputField object.
     *
     * @param label 
     * @param sops 
     * @param inklOther 
     */
    public SOPInputField(String label, String[] sopLabels, boolean inklOther) {
        super(label, "Standard Operation Protocol");

        InputField field_sop = new DropDown(label, "Please select the SOP that was used.", sopLabels, true);

        field_sop.setHelptext("The SOPs are predefined Protocols. In case you followed the instructions of the SOPS while repering the samples, please select the SOP-ID.<br>Otherwise please select " + DropDown.STRING_OTHER + " and describe in the field below.");

        this.addField(field_sop);

        if (inklOther) {
            InputField field_sopDescription = new BigStringInputField("Preparation", "in case you used your own Protocol - please specify it here.");
            InputField field_sopShortDescriptin = new StringInputfield("Name", "Short label for the description - how would you label your SOP.");
            Relation relation = new Relation(new String[] { DropDown.STRING_OTHER }, new FormObject[] { field_sopDescription, field_sopShortDescriptin });
            field_sop.addRelation(relation);
            this.addField(field_sopShortDescriptin);
            this.addField(field_sopDescription);
        }
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /*
     *  (non-Javadoc)
     * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getPath()
     */
    public String getPath() throws UnsupportedClazzMappingException {
        throw new UnsupportedClazzMappingException();
    }

    /*
     *  (non-Javadoc)
     * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getSingleValueField()
     */
    public FormObject getSingleValueField() throws UnsupportedClazzMappingException {
        throw new UnsupportedClazzMappingException();
    }

    /* (non-Javadoc)
     * @see org.setupx.repository.web.forms.MultiField4Clazz#cloneField()
     */
    public InputField cloneField() throws CloneException {
        SOPInputField field = new SOPInputField(this.getQuestion(), determineValues(), true);

        return field;
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public String createShortLabel() {
        return this.getField(0).toStringHTML();
    }

    /**
     * TODO: 
     *
     * @param sops TODO
     * @param inklOther TODO
     *
     * @return TODO
     */
    private static String[] convert(SOP[] sops, boolean inklOther) {
        int end = sops.length;
        int pos = 0;

        String[] sopLabels = new String[end];

        if (inklOther) {
            end++;
            sopLabels = new String[end];
            sopLabels[0] = DropDown.STRING_OTHER;
        }

        for (int sopCounter = 0; sopCounter < sops.length; sopCounter++) {
            pos++;
            debug(null, "pos: " + pos + "  sopCounter: " + sopCounter + " end: " + end + " sops.length" + sops.length);
            sopLabels[pos] = sops[sopCounter].getName();
        }

        return sopLabels;
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    private String[] determineValues() {
        Set set = ((DropDown) this.getField(0)).getValuesHibernate();
        String[] values = new String[set.size()];
        Iterator iterator = set.iterator();
        int i = 0;

        while (iterator.hasNext()) {
            KeyValuePair element = (KeyValuePair) iterator.next();
            values[i] = element.getInternValue();
            i++;
        }

        return values;
    }


    /**
     * create a link for older entries 
     */
    public String getAdditionalInfo() {
        // example: sop_predefined.jsp?type=Extraction&name=field344
        StringBuffer link = new StringBuffer();
        link.append("sop_predefined.jsp?");
        link.append("type=");
        link.append(this.getQuestion());
        link.append("&name=");
        link.append(this.getName());
        
        String txt = "If you defined these SOP informations already you can use these from the former experiments.";
        
        return txt + " <a href=\"" + link + "\"> <img src=\"pics/go_report.gif\" border=\"0\"></a><hr>"; 
    }
}
