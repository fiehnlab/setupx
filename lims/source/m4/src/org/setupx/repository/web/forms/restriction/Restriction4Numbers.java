/**
 * ============================================================================ File:    Restriction4Numbers.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.ValidationAnswer;


/**
 * Limits an inputfield to numbers. In addition the min and max can be set.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.3 $
 *
 * @hibernate.subclass
 * @deprecated use the subclasses - class can not be abstract - because of existing objects
 */
public class Restriction4Numbers extends StringRestriction {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public boolean includePatternTest = true;
  int endRange = 999999;
  int startRange = 999999;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String REGEX = "[0-9]{1,}+";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Restriction4Numbers object.
   */
  public Restriction4Numbers() {
    super(REGEX, false);
  }

  /**
   * Creates a new Restriction4Numbers object.
   *
   * @param startRange starting of the valid range
   * @param endRange endpoint of the valid range
   */
  public Restriction4Numbers(int startRange, int endRange) {
    super(REGEX, false);
    setStartRange(startRange);
    setEndRange(endRange);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   */
  public void setEndRange(int endRange) {
    this.endRange = endRange;
  }

  /**
   * @hibernate.property
   */
  public int getEndRange() {
    return endRange;
  }

  /**
   * @return a type specific type of errormessage
   */
  public String getErrorMessage() {
    return "illegal Value";
  }

  /**
   */
  public void setStartRange(int startRange) {
    this.startRange = startRange;
  }

  /**
   * @hibernate.property
   */
  public int getStartRange() {
    return startRange;
  }

  /**
   * parse the value - it is abstract that each subtype can have its only parsing method
   */
  public double parse(String s) {
    return Double.parseDouble(s);
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#validateObject()
   */
  public ValidationAnswer validateObject(InputField field) {
    String s = (String) field.getValue();

    if (s.length() == 0) {
      return new ValidationAnswer(field, "Please enter the value.");
    }

    try {
      double _value = this.parse(s);

      // check max and min
      if ((startRange != 999999) && (startRange > _value)) {
        return new ValidationAnswer(field, "the value must be more than " + startRange);
      }

      if ((endRange != 999999) && (endRange < _value)) {
        return new ValidationAnswer(field, "the value must be less than " + endRange);
      }
    } catch (Exception e) {
      err(this, e);

      if ((endRange != 999999) && (startRange != 999999)) {
        return new ValidationAnswer(field, "the value must be more than " + startRange + " and less than " + endRange);
      } else {
        return new ValidationAnswer(field, this.getErrorMessage());
      }
    }

    if (includePatternTest) {
      return super.validateObject(field);
    }

    return null;
  }
}
