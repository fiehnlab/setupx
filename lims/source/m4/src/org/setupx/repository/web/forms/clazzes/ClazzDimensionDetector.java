/**
 * ============================================================================ File:    ClazzDimensionDetector.java Package: org.setupx.repository.web.forms.clazzes cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.clazzes;

import java.util.Iterator;
import java.util.Vector;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.web.forms.ClazzNotFoundException;
import org.setupx.repository.web.forms.FormContainer;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.IncreaseException;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;
import org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz;
import org.setupx.repository.web.forms.inputfield.multi.NBCISpeciesInputfieldException;
import org.setupx.repository.web.forms.inputfield.multi.Promt;


/**
 * Tool, which analyses a <code>promt</code> and generates based on this the clazzes.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.20 $
 */
public class ClazzDimensionDetector extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @label the promt the clazzes will be looked for
   * @link aggregationByValue
   */
  Promt promt = null;

  /**
   * @label all the clazzes <b>possible </b>in this configuration
   */
  private Clazz[] clazzes = new Clazz[0];

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** Array containing all ClazzAxis found in a promt */
  private ClazzDimension[] clazzDimensions = new ClazzDimension[0];
  private static Object thiz = new ClazzDimensionDetector();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ClazzDimensionDetector object.
   *
   * @param promt 
   * @param existing_clazzes 
   */
  public ClazzDimensionDetector(Promt promt, Clazz[] existing_clazzes) {
    this.promt = promt;
    debug("creating a new instance - ClazzDimensionDetector(Promt promt, Clazz[] existing_clazzes)");

    if (existing_clazzes != null) {
      debug("creating a new instance - ClazzDimensionDetector( " + promt.getUOID() +  " , Clazz[] " + existing_clazzes.length + ")");
      
      this.clazzes = existing_clazzes;
      //show(existing_clazzes);
      for (int i = 0; i < existing_clazzes.length; i++) {
          Clazz clazz = existing_clazzes[i];
          debug("existing clazzes: nr." + i + " uoid : " + clazz.getUOID() + "  samples: " + clazz.getSamples().size());
      }
    } else {
      warning(this, "existing classes are NULL - NO existing_clazzes ??");
    }

    debug("number of classes before update : " + clazzes.length);
    debug("number of dimensions before update : " + this.clazzDimensions.length);
   
    this.update();

    debug("number of classes after update : " + clazzes.length);
    debug("number of dimensions after update : " + this.clazzDimensions.length);
  }

  /**
   * <b>use the constructor with the promt</b>
   */
  private ClazzDimensionDetector() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * looks for a clazz that is related to all givven multiField4Clazzs
   *
   * @param multiField4Clazzs the multiField4Clazzs that the clazz must be related to
   *
   * @throws ClazzNotFoundException
   */
  public Clazz getClazz(MultiField4Clazz[] multiField4Clazzs)
    throws ClazzNotFoundException {
      
      
    //debug("getClazz() - looking for Clazz with a relation to " + multiField4Clazzs.length + " multiField4Clazzes");
    for (int i = 0; i < clazzes.length; i++) {
      Clazz clazz = clazzes[i];
      if (clazz.getMultiField4Clazzs().length != multiField4Clazzs.length){
          warning(this, "clazz has " + clazz.getMultiField4Clazzs().length  + " related MultiField4Clazzs - while the call was for " + multiField4Clazzs.length + " MultiField4Clazzs.");
      }

      debug("getClazz() - checking clazz: " + clazz.getUOID() + " " + i + "/" + clazzes.length);
      if (clazz.relatedTo(multiField4Clazzs)) {
        debug("getClazz() - found a clazz: " + clazz.getUOID() + " " + i + "/" + clazzes.length);
        return clazz;
      }
    }

    Logger.warning(this, "unable to getClazz(): did not find a clazz for " + multiField4Clazzs);

    throw new ClazzNotFoundException(multiField4Clazzs);
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public ClazzDimension[] getClazzDimensions() {
    return clazzDimensions;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public Clazz[] getClazzes() {
    return this.clazzes;
  }

  /**
   * @param inputFields
   *
   * @return null if not found - otherwise the clazz for this combination of clazzdimensions
   */
  public Clazz determineClass(MultiField4Clazz[] inputFields) {
    Logger.log(this, "determineClass: current number of clazzes:  " + this.clazzes.length);

    for (int i = 0; i < this.clazzes.length; i++) {
      Clazz clazz = this.clazzes[i];

      //debug("checking clazz: " + clazz.toString());
      if (clazz.relatedTo(inputFields)) {
        return clazz;
      }
    }

    return null;
  }

  /**
   * finds all clazzes that are related to this Field
   *
   * @param multiField4Clazz
   *
   * @return
   */
  public Iterator findClazzes(MultiField4Clazz multiField4Clazz) {
    Vector v = new Vector();

    //debug(this, "findClazzes ------------" + multiField4Clazz);
    for (int i = 0; i < this.clazzes.length; i++) {
      //debug(this, "checking " + i + this.clazzes[i]);
      if (this.clazzes[i].relatedTo(multiField4Clazz)) {
        v.add(multiField4Clazz);
      }
    }

    return v.iterator();
  }

  /**
   * updates the clazzes
   */
  public void update() {
    debug("update() ~~~~~~~~~~~~~~~~~~~~~~ START UPDATE Clazzdimensions clazzDimensions:" + getClazzDimensions().length + " clazzes:" + this.getClazzes().length + "~~~~~~~~~~~~~~~~~~~~~~");
    this.promt.updateRelations();  
    debug("update() - status after updated relations: [this.promt.updateRelations]");
    this.showInfo();
    
    
    try {
        debug("forcing update on species");
        Object species = this.promt.findNBCISpeciesInputfield().getSpeciesInputfield().getValue();
    
        debug("value before forced update: " + species );
        this.promt.findNBCISpeciesInputfield().getSpeciesInputfield().setValue(species);

    
        species = null;
        species = this.promt.findNBCISpeciesInputfield().getSpeciesInputfield().getValue();
        debug("value after forcing update: " + species );

    } catch (NBCISpeciesInputfieldException e) {
        e.printStackTrace();
    }

    this.showInfo();
    
    
    // ok until here
    debug("update() - determineClazzDimensions() now. Number of clazzDimensions before determineClazzDimensions(): " + this.getClazzDimensions().length);
    clazzDimensions = null;
    
    try{
        throw new Exception();
    } catch (Exception e) {
        e.printStackTrace();
    }

    
    
    
    this.determineClazzDimensions();
    
    
    debug("update() - number of clazzDimensions after determineClazzDimensions(): " + this.getClazzDimensions().length);
    
    this.clazzes = createClazzes(clazzDimensions, clazzes, Clazz.DEFAULT_NUMBER_SAMPLES);
    debug("~~~~~~~~~~~~~~~~~~~~~~ END UPDATE Clazzdimensions clazzDimensions:" + getClazzDimensions().length + " clazzes:" + this.getClazzes().length + "~~~~~~~~~~~~~~~~~~~~~~");
    this.showInfo();
  }

  private void showInfo() {
      for (int i = 0; i < this.getClazzDimensions().length; i++) {
          ClazzDimension clazzDimension = this.getClazzDimensions()[i];
          debug("ClazzDimension " + (i+1) + "/" + this.getClazzDimensions().length + ": " + Util.getClassName(clazzDimension) + "  "  );
          
          for (int j = 0; j < clazzDimension.getFields().length; j++) {
            MultiField4Clazz multiField4Clazz = clazzDimension.getFields()[j];
            debug("     MultiField4Clazz " + (j+1) + "/" + clazzDimension.getFields().length + ": " + Util.getClassName(multiField4Clazz) + "  " + multiField4Clazz.getUOID() + " " + multiField4Clazz.isActive());
          }
      }
  }

/**
   * TODO: 
   *
   * @param origArray TODO
   * @param dimensions TODO
   *
   * @return TODO
   */
  private void add2clazzDimensions(ClazzDimension[] dimensions) {
    for (int i = 0; i < dimensions.length; i++) {
      //origArray = 
      add2clazzDimensions(dimensions[i]);
    }
  }

  /**
   * adds a dimension to the array of dimensions
   *
   * @param axis
   *
   * @return
   */
  private void add2clazzDimensions(ClazzDimension dimension) {
    //debug(thiz, "adding one classdimension :  " + dimension + " to array of axis-size: " + clazzDimensions.length);
    // in case it is allready inside ...
    for (int i = 0; i < clazzDimensions.length; i++) {
      if (clazzDimensions[i].hashCode() == dimension.hashCode()) {
        Logger.warning(thiz, "dublicate: " + dimension.hashCode());

        return; //origDimensions;
      }
    }

    if (dimension == null) {
      return; //origDimensions;
    }

    ClazzDimension[] newArray = new ClazzDimension[clazzDimensions.length + 1];

    for (int i = 0; i < clazzDimensions.length; i++) {
      newArray[i] = clazzDimensions[i];
    }

    //debug(thiz, "adding " + dimension);
    newArray[newArray.length - 1] = dimension;
    clazzDimensions = newArray;

    //debug(thiz, "to array of result: "  +origAxisArray.length);
    //debug(thiz, "added dimension:  " + dimension + " to array of axis-size: " + clazzDimensions.length);
    //return newArray;
  }

  /**
   */
  /*private static ClazzDimension[] lookInContainer4ClazzAxis(ClazzDimension[] origArray, FormContainer container) {
     FormObject[] formObjects = container.getFields();
     return determineClazzDimension(origArray, formObjects);
     }*/

  /**
   * calculates the number of classes bases on the number of dimensions
   *
   * @param dimension the dimensions
   *
   * @return the number of classes
   */
  private static int calculate(ClazzDimension[] dimension) {
    int numOfClassesCalcuated = 1;
    Logger.debug(thiz, "calculate(ClazzDimension[] dimension) dimension size:" + dimension.length);

    for (int i = 0; i < dimension.length; i++) {
      Logger.debug(thiz, "calculate(ClazzDimension[] dimension)   multiply by size of dimension[i] (" + dimension[i].size() + "): " + dimension[i]);
      numOfClassesCalcuated = numOfClassesCalcuated * dimension[i].size();
    }

    //Logger.log(thiz, buffer.toString() + "= " + numOfClassesCalcuated);
    Logger.debug(thiz, "calculate(ClazzDimension[] dimension) number of classes calculated:" + dimension.length);
    return numOfClassesCalcuated;
  }

  /*for (positions.length)
     int pos = calculatePos(positions, dimensions) - 1;
     //debug(thiz, "pos: " + pos);
     // collecting all the fields and create a new clazz out of them
     //debug(thiz, "creating clazz " + (pos + 1) + "/" + clazzs.length);
     MultiField4Clazz[] multiField4Clazzs = new MultiField4Clazz[dimensions.length];
     for (int i = 0; i < dimensions.length; i++) {
       log(thiz, "requesting from dimension " + (i + 1) + "   filed: " + positions[i] + " of " + dimensions[i].size());
       multiField4Clazzs[i] = dimensions[i].getField(positions[i]);
     }
     clazzs[pos] = new Clazz(multiField4Clazzs);
     count++;
     // in case that the end is reached - leave !! works only if the classes are created sequently !!
     if ((pos + 1) == clazzs.length) {
       debug(thiz, "done");
       return clazzs;
     }
     // increase position of pos in the dimension
     positions[posInArray]++;
     //posInArray = 0;
     looping = false;
     //debug(thiz, " pos of dimension " + posInArray + " to " + positions[posInArray]);
     if (positions[posInArray] >= (dimensions[posInArray].size())) {
       debug(thiz, "max(" + dimensions[posInArray].size() + ") for dimenson nr." + (posInArray + 1) + "/" + dimensions.length + " reached.");
       positions[posInArray] = 0;
       posInArray++;
       if (positions[posInArray] != (dimensions[posInArray].size())) {
         positions[posInArray]++;
         posInArray--;
       } else {
         posInArray++;
       }
       // final end
       if (posInArray >= dimensions.length) {
         looping = false;
         loop = true;
         //debug(thiz, "final end reached " + posInArray);
         return clazzs;
       }
     } else {
       // increase position of pos in the dimension
       positions[posInArray]++;
       //posInArray = 0;
       looping = false;
       //debug(thiz, " pos of dimension " + posInArray + " to " + positions[posInArray]);
     }
     }
   */

  /**
   * calculates the position <code>(counter)</code> for a combination of numbers and dimensions
   */
  private static int calculatePos(int[] positions, ClazzDimension[] dimensions) {
    int factor = 1;
    int value = 0;

    for (int i = 0; i < positions.length; i++) {
      //debug(thiz, "counter:" + i + " factor:" + factor + " pos:" + (positions[i]+1) + "/" + dimensions[i].size() );
      value = value + (factor * positions[i]);
      factor = factor * dimensions[i].size();
    }

    return value + 1;
  }

  /**
   * creates a clazzAxis by taking all elemtens of type Multifield4Clazz
   *
   * @param object
   *
   * @return
   */
  private static ClazzDimension createClazzDimension(FormContainer array) {
    ClazzDimension tempAxis = new ClazzDimension();
    FormObject[] formObjects = array.getFields();

    debug(thiz, "createClazzDimension(): creating ClazzDimension from array of " + array.size() + " elements");
    for (int i = 0; i < formObjects.length; i++) {
      debug(thiz, "createClazzDimension(): " + Util.getClassName(formObjects[i]));
      if (formObjects[i] instanceof MultiField4Clazz) {
        tempAxis.add((MultiField4Clazz) formObjects[i]);
      }
    }

    if (tempAxis.size() != array.size()) {
      Logger.warning(thiz, "createClazzDimension():  the number of elements in the vector " + array.size() + " and in the clazzdimension " + tempAxis.size() + " is not equal");
    }

    return tempAxis;
  }

  /**
   * creates the classes for a defined set of dimensions
   *
   * @param dimensions the dimensions
   * @param clazzes
   * @param numberOfSamples number of samples per clazz
   *
   * @return an array of clazzes
   *
   * @throws ArrayIndexOutOfBoundsException in case that there is just a single ClazzDimension. Make sure that there are at least two.
   */
  private Clazz[] createClazzes(ClazzDimension[] dimensions, Clazz[] clazzes, int numberOfSamples) {
    debug(thiz, "createClazzes: --- creating clazzes for " + dimensions.length + " dimensions.");

    // in case that there is no clazzdimension I cant create any clazzes
    if (dimensions.length == 0) {
      return new Clazz[] {  };
    }
    
    debug(thiz, "number of dimensions: " + dimensions.length);
    debug(thiz, "number of classes: " + calculate(dimensions));

    // blank Array
    Clazz[] clazzs = new Clazz[calculate(dimensions)];
    boolean loop = true;

    int[] positions = new int[dimensions.length];

    while (loop) {
      int clazzNumber = calculatePos(positions, dimensions) - 1;

      MultiField4Clazz[] multiField4Clazzs = new MultiField4Clazz[dimensions.length];

      for (int i = 0; i < dimensions.length; i++) {
        //debug(thiz, "createClazz: requesting from dimension " + (i + 1) + "   field: " + (positions[i] + 1) + " of " + dimensions[i].size());
        multiField4Clazzs[i] = dimensions[i].getField(positions[i]);
      }

      try {
        // checking if clazz exists 
        clazzs[clazzNumber] = getClazz(multiField4Clazzs);
      } catch (ClazzNotFoundException e1) {
        warning(thiz, "createClazz: A clazz with the combination " + multiField4Clazzs +  " does not exist - creating clazzs nr.: " + clazzNumber);
        clazzs[clazzNumber] = new Clazz(multiField4Clazzs, numberOfSamples);
      }

      try {
        positions = increase(positions, dimensions);
      } catch (IncreaseException e) {
        //debug(thiz, "createClazz: in case the last pos is at max - it is done - end is reached.");
        return clazzs;
      }

      // in case the last pos is at max - it is done - end is reached.
      //debug(thiz, "last pos: " + (positions[(positions.length - 1)] + 1) + "  ");
      //debug(thiz, "last dim length: " + dimensions[(positions.length - 1)].size() + " ");
      if (clazzNumber == clazzs.length) {
        //if ((positions[(positions.length)]) >= dimensions[(positions.length)].size()) {
        //debug(thiz, "createClazz: in case the last pos is at max - it is done - end is reached.");
        return clazzs;
      }
    }

    throw new RuntimeException();
  }

  /**
   */
  private void determineClazzDimension(FormObject[] formObjects) {
    for (int i = 0; i < formObjects.length; i++) {
      FormObject object = formObjects[i];

      debug(thiz, ".................................................................");
      debug(thiz, "current number of ClazzDimensions " + clazzDimensions.length);
      debug(thiz, "checking object (" + i + "/" + formObjects.length + ") : " + Util.getClassName(object) +  " " + object);
      if ((object != null) && object.isActive() && look4MultiField4ClazzInside(object)) {
        // if formObject contains MultiField4Clazz create a dimension ...
        debug(thiz, "found MultiField4Clazz inside " + Util.getClassName(object));
        // ... and add it
        //origArray = 
        add2clazzDimensions(createClazzDimension((FormContainer) object));
      }
      // if it is a formcontainer - go inside and check it
      else if (object instanceof FormContainer) {
        //debug(thiz, "found FormContainer: " + Util.getClassName(object));
        FormObject[] formObjects2 = ((FormContainer) object).getFields();
        determineClazzDimension(formObjects2);
        //add2clazzdetermineClazzDimension(formObjects2));
      }
    }
  }

  /**
   * finds all ClazzMultifield4Clazz form and inside the fields. and creates an array of ClazzAxis.
   *
   * @return
   *
   * @see ClazzDimension
   */
  private void determineClazzDimensions() {
    // resetting
    clazzDimensions = new ClazzDimension[0];

    FormObject[] objects = this.promt.getFields();

    // call recursive method
    determineClazzDimension(objects);
    
    debug("determineClazzDimensions()   clazzDimensions.length: " + this.clazzDimensions.length);
}

  /**
   * @param dimensions
   * @param positions
   *
   * @return
   *
   * @throws IncreaseException
   */
  private static int[] increase(int[] positions, ClazzDimension[] dimensions)
    throws IncreaseException {
    for (int i = 0; i < positions.length; i++) {
      //debug(thiz, (i) + "/" + (positions.length));
      if (positions[i] > dimensions[i].size()) {
        Logger.warning(thiz, "it ist not possible to access a pos: " + positions[i] + " in array with size: " + dimensions[i].size());
      }

      /*
         debug(thiz, ".increase():   pos: " + (positions[i]));
         debug(thiz, " dimension vector at pos: " + dimensions[positions[i]]);
         debug(thiz, " size of dimension vec at pos:  " + dimensions[positions[i]].size());
       */
      int pos = positions[i];

      //log(thiz, "pos: " + pos);
      //log (thiz, "number of clazzdimensions:" + dimensions.length);
      int p1 = positions[i];
      int p2 = dimensions[i].size() - 1; //dimension.size()-1;

      if (p1 == p2) {
        //warning(thiz, "end of dimension " + i + " reached.");
        positions[i] = 0;
      } else {
        positions[i]++;

        //debug(thiz, "positions[i] " + positions[i]);
        //debug(thiz, "dimensions[i] " + dimensions[i].size());
        //debug(thiz, showValues(positions, dimensions));
        return positions;
      }

      if (positions[i] >= dimensions[i].size()) {
        throw new IncreaseException("end reached.");
      }
    }

    throw new IncreaseException("the loop was left without a requiered return.");

    //debug(thiz,showValues(positions, dimensions));
    //return positions;
  }

  /**
   * check if object is a formContainer but not a MultiField4Clazz
   *
   * @param object
   *
   * @return
   */
  private static boolean look4FormContainer(FormObject object) {
    // if (fields[i] instanceof FormContainer && !(fields[i] instanceof MultiField4Clazz)){
    return (object instanceof FormContainer && !(object instanceof MultiField4Clazz));
  }

  /**
   * checks on the first child level if one of the childs is a MultiField4Clazz
   *
   * @param object container of the fileds - <code>if NOT(object instanceof FormContainer)</code> returnvalue is false
   *
   * @return if one of the childs is a MultiField4Clazz
   */
  private static boolean look4MultiField4ClazzInside(FormObject object) {
    if (!(object instanceof FormContainer)) {
      //debug(thiz, "no Formcontainer: " + Util.getClassName(object));
      return false;
    }

    FormObject[] formObjects = object.getFields();

    for (int i = 0; i < formObjects.length; i++) {
      FormObject object2 = formObjects[i];

      //debug(thiz, i + "/" + formObjects.length + " checking: " + object2);
      if (object2 instanceof MultiField4Clazz) {
        return true;
      }

      /*else if (object2 instanceof FormContainer) {
         boolean inside = look4FormContainer(object2);
         if (inside) {
           return inside;
         }
         }*/
    }

    return false;
  }

  /**
   * TODO: 
   *
   * @param positions TODO
   * @param dimensions TODO
   *
   * @return TODO
   */
  private static String showValues(int[] positions, ClazzDimension[] dimensions) {
    StringBuffer buffer = new StringBuffer();
    Logger.debug(thiz, " dimensions: ");

    for (int i = 0; i < dimensions.length; i++) {
      ClazzDimension dimension = dimensions[i];
      buffer.append("\n dimension: " + i + "|");

      for (int j = 0; j < dimension.size(); j++) {
        if (dimension.get(j) == null) {
          buffer.append("\tNULL");
        } else {
          buffer.append("\tok(" + j + ")");
        }
      }
    }

    Logger.debug(thiz, " postions: ");

    for (int i = 0; i < positions.length; i++) {
      buffer.append("\n + " + i + "(" + positions[i] + ")");
    }

    return buffer.toString();
  }
}
