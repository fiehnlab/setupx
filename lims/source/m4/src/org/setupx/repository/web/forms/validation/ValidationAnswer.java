/**
 * ============================================================================ File:    ValidationAnswer.java Package: org.setupx.repository.web.forms.validation cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.validation;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.web.forms.inputfield.InputField;

import java.util.HashSet;
import java.util.Iterator;


/**
 * object containing the answer after a validation. 
 * supports generated link.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.15 $
 */
public class ValidationAnswer extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private HashSet set = new HashSet();

  /**
   * @label the inputfield that will be validated
   */
  private InputField field = null;

  /** Link created automaticly, which links back to the form servlet with the new value */
  private Link link = null;

  /** msg containing the message that will be shown */
  private String msg = "";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ValidationAnswer object.
   *
   * @param message 
   */
  public ValidationAnswer(InputField field, String message) {
    this.msg = message;
    this.field = field;
  }

  /**
   * Creates a new ValidationAnswer object.
   */
  public ValidationAnswer(InputField field) {
    this.field = field;
  }

  /**
   * Creates a new ValidationAnswer object.
   *
   * @param field 
   */
  public ValidationAnswer(Link link) {
    this.field = null;
    this.link = link;
  }

  /**
   * Creates a new ValidationAnswer object.
   *
   * @param field 
   * @param message 
   * @param linkedContent 
   */
  public ValidationAnswer(InputField field, String message, String linkedContent) {
    this.field = field;
    this.msg = message;
    this.add(linkedContent);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param link TODO
   */
  public void setLink(Link link) {
    this.link = link;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public Link getLink() {
    if (this.link == null) {
      createALink();
    }

    return link;
  }

  /**
   * TODO: 
   *
   * @param msg TODO
   */
  public void setMsg(String msg) {
    this.msg = msg;
  }

  /**
   * TODO: 
   *
   * @param string TODO
   */
  public void add(String string) {
    this.set.add(string);
  }

  /**
   * creates a link - linking back to the form_servlet
   *
   * @param name
   * @param taxID
   * @param name2
   *
   * @return
   */
  public static String toLink(String name, String value, String label) {
    return toLink(name, value, label, "");
  }

  /**
   * TODO: 
   *
   * @param name TODO
   * @param value TODO
   * @param label TODO
   * @param alt_comment TODO
   *
   * @return TODO
   */
  public static String toLink(String name, String value, String label, String alt_comment) {
    //"form?form_lastpage=0&field0=scholz&field1=University+of+California+Davis%2C+Genome+Center+Davis&field2=yourmail%40mail.com&field3=42&field4=synechococus&field5=synechococus&field6=Braunschweig&field7=18.11.2004+19%3A41%3A41&field8=false&=
    StringBuffer buffer = new StringBuffer();
    buffer.append("form?");
    buffer.append(name);
    buffer.append("=");
    buffer.append(value);
    // #anchor
    buffer.append("#");
    buffer.append(name);

    // <a href="form?form_target=1&form_lastpage=0">next</a>
    return "<a href=\"" + buffer.toString() + "\"  title=\"" + alt_comment + "\">" + label + "</a>\n";
  }

  /**
   * creates a link including key and value - as post in case that the restriciton makes sugenstions - it will be formated as a link
   */
  public static String toLink(String name, String value) {
    return toLink(name, value, value);
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String toString() {
    return this.getClass() + "  " + this.getLink();
  }

  /**
   * converts the value of this response into a linked content.  The content can be clicked then to correct the misspelling,... inside the value.
   *
   * @return the link
   */
  private void createALink() {
    //debug("creating a response to show  [" + this.set.size() + "]   msg:" + msg);
    // merging link and msg
    StringBuffer buffer = new StringBuffer();

    buffer.append(msg);

    if ((set != null) && (set.size() != 0)) {
      Iterator iterator = set.iterator();

      while (iterator.hasNext()) {
        buffer.append("  try this:  ");

        String suggestion = (String) iterator.next();
        String suggestionAsLink = toLink(field.getName(), suggestion);
        buffer.append(suggestionAsLink);
      }
    }

    this.link = new Link(buffer.toString());
    //debug("created a new link: " + this.link.toString());
  }
}
