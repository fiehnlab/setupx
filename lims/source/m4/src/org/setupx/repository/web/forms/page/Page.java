/**
 * ============================================================================ File:    Page.java Package: org.setupx.repository.web.forms.page cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.page;

import org.setupx.repository.web.forms.ArrayShrinkException;
import org.setupx.repository.web.forms.FormContainer;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.FormsException;
import org.setupx.repository.web.forms.inputfield.InputField;

import java.util.Enumeration;
import java.util.Hashtable;


/**
 * A Page on the web.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.23 $
 *
 * @hibernate.subclass
 */
public class Page extends FormObject implements FormContainer {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private boolean summaryActive;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Page object.
   */
  public Page() {
    super();
  }

  /**
   * Creates a new Page object.
   *
   * @param name 
   * @param description 
   * @param longDescription 
   * @param inputFields 
   */
  public Page(String name, String description, String longDescription, InputField[] inputFields) {
    this(name, description, longDescription);
    checkInputFiledNames(inputFields);
    this.setFields(inputFields);
  }

  /**
   * Creates a new Page object.
   *
   * @param name 
   * @param description 
   * @param longDescription 
   */
  public Page(String name, String description, String longDescription) {
    super(name, description, longDescription);
    // every page is deactivated from the beginning on - until the first content is added
    this.setActive(false);

    this.setSummaryActive(false);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#getAttributes()
   */
  public Hashtable getAttributes() {
    return null;
  }

  /**
   * TODO: 
   *
   * @param summaryActive TODO
   */
  public void setSummaryActive(boolean summaryActive) {
    this.summaryActive = summaryActive;
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @hibernate.property
   */
  public boolean isSummaryActive() {
    return summaryActive;
  }

  /*
   * (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormContainer#addField(org.setupx.repository.web.forms.FormObject)
   */
  public void addField(FormObject formObject) {
    warning(this, "TODO - per page there should be only one Multifield4Clazz");
    this.addInternalField(formObject);
  }

  /**
   * @deprecated look at jsps
   */
  public String createHTML(int i) {
    throw new UnsupportedOperationException("this method is not supported anymore.");
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormContainer#remove(org.setupx.repository.web.forms.FormObject)
   */
  public boolean remove(FormObject formObject) throws ArrayShrinkException {
    return this.removeInternalField(formObject.getName());
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormContainer#remove(java.lang.String)
   */
  public boolean remove(String key) throws ArrayShrinkException {
    FormObject formObject = this.getField(key);

    return this.remove(formObject);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormContainer#size()
   */
  public int size() {
    return this.getFields().length;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#toStringHTML()
   */
  public String toStringHTML() {
    return this.getName() + " " + this.getDescription();
  }

  /**
   * @param values
   *
   * @throws FormsException
   * @throws FormsException
   */
  public void updateFields(Hashtable values) throws FormsException {
    debug(this, "--> updating values in " + values.size() + " fields");

    Enumeration enumeration = values.keys();

    while (enumeration.hasMoreElements()) {
      String key = (String) enumeration.nextElement();
      InputField field = (InputField) this.getField(key);
      debug(this, "--> updating value: \"" +  key + "\" in " + field);

      if (field == null) {
          throw new FormsException("unable to find the inputfield called " + key);
      }

      field.setValue(values.get(key));
    }

    debug(this, "<-- updating values in " + values.size() + " fields");
  }


  /**
   * checking if all inputfiled names occur just once...
   */
  private static void checkInputFiledNames(InputField[] inputFields2) {
    if (inputFields2 == null) {
      throw new NullPointerException("inputfields are null");
    }

    Hashtable hashtable = new Hashtable();

    for (int i = 0; i < inputFields2.length; i++) {
      hashtable.put(inputFields2[i].getName(), "null");
    }
  }
  
  public String getXMLChildname() {
    return "group";
  }
}
