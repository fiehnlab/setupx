/**
 * ============================================================================ File:    Controller.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import com.oreilly.servlet.MultipartRequest;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.ServiceMediator;
import org.setupx.repository.core.communication.export.FileExport;
import org.setupx.repository.core.communication.export.FileExportException;
import org.setupx.repository.core.communication.export.xls.XLSFileExport;
import org.setupx.repository.core.communication.ncbi.local.LocalDatabaseInstance;
import org.setupx.repository.core.communication.notification.ExperimentSubmittedNotification;
import org.setupx.repository.core.communication.notification.NotificationCentral;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.access.PromtUserAccessRight;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.core.util.web.UiHttpServlet;
import org.setupx.repository.core.util.web.WebForwardException;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.web.WebAccess;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.forms.inputfield.BigStringInputField;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.SubmitButton;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;
import org.setupx.repository.web.forms.inputfield.multi.ExpandingField;
import org.setupx.repository.web.forms.inputfield.multi.MultiField;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.SampleScheduledException;
import org.setupx.repository.web.forms.page.Page;

import java.util.Date;

import java.io.File;
import java.io.IOException;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.parser.TagElement;



/**
 * Central Controlling servlet for the input forms.   Every request is beeing send to <b>THIS</b> servlet.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.22 $
 */
public class Controller extends UiHttpServlet {
    //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public static final String FIELDSTART = "field";
    private static Object thiZ = new Controller();

    // defining if pages jump automaticly to the next
    private static final boolean AUTOJUMP = false;

    // max age of promt in milliseconds until it ll be autosaved again.  
    private static final long AUTOSAVE_PROMT_MAXAGE = 1000 * 60;

    static {
        System.out.println("starting");
        Logger.log(thiZ, "starting");
    }

    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new Controller object.
     */
    public Controller() {
        super();
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    public final void destroy() {
        super.destroy();
        LocalDatabaseInstance.setToNull();
    }

    /* (non-Javadoc)
     * @see org.setupx.repository.core.util.web.UiHttpServlet#process(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public final void process(HttpServletRequest request, HttpServletResponse response)
    throws WebForwardException, WebFormException {
        // logging
        // webaccess logging was moved into a filter
        // new WebAccess(request);

        // finding the userobject
        UserDO userDO = determineUser(request);

        // determine the promt form the sesseion
        Promt promt = null;
        stat(userDO, promt, -1);

        try {
            promt = determinePromt(request, userDO);
        } catch (PromtCreateException e2) {
            throw new WebFormException(e2);
        } catch (PersistenceActionException e2) {
            throw new WebFormException(e2);
        }
        stat(userDO, promt, -1);

        // Logout button - 
        if (logout(request, userDO, promt)) {   
            Logger.log(thiZ, "logout");
            forward("/", request, response);

            return;
        }

        // find the position of the last visited Page	
        int last_pos = determineLastVisitedPage(request);
        stat(userDO, promt, -1);

        // check if there is a linked target
        int target_pos = determineTarget(request);
        //stat(userDO, promt, target_pos);

        // jumping between the pages...
        Hashtable values = getInputValues(request);
        //stat(userDO, promt, target_pos); 

        if (last_pos == 9999) {
            Logger.debug(thiZ, "the page information is not inside the request, so the pos will be looked for");
            last_pos = promt.findPageContaingKey(values);

            if (target_pos == 9999) {
                target_pos = last_pos;
            }
        }
        //stat(userDO, promt, target_pos);

        if (target_pos == 9999) {
            target_pos = last_pos;
        }

        try {
            // adding the values in the related field
            try {
                Logger.debug(thiZ, "updating values in page " + last_pos);

                FormObject object = promt.getField(last_pos);
                Page _page = (Page) object;
                Logger.debug(thiZ, "updating values in page " + _page.getDescription());

                _page.updateFields(values);
                //stat(userDO, promt, target_pos);
            } catch (ClassCastException e) {
                Logger.err(thiZ, "trying to access an unknown field: " + e.toString());
            } catch (ArrayIndexOutOfBoundsException e) {
                Logger.warning(thiZ, "trying to access an unknown field: " + e.toString());
            } catch (FormsException e) {
                throw new WebFormException(e);
            }
        }  
        catch (Exception e){
            e.printStackTrace();
        }

        // the following methods check for specail actions - in case that a helppage,... is requested, there
        // will be a forwarding. 
        // checking if there are fields inside the page that request help information
        if (helpFields(promt, request, response)) {
            return;
        }

        classesResize(promt, request, response);
        //stat(userDO, promt, target_pos);

        if (autoFillField(promt, request, response)) {
            //stat(userDO, promt, target_pos);
            return;
        }

        // checking if there are fields inside the page that want to be expanded
        expandFields(promt, request);
        //stat(userDO, promt, target_pos);

        // checking if there are fields that want to be resized
        resizeFields(promt, request);
        //stat(userDO, promt, target_pos);

        // checking if there are fields inside the page that will be removed
        removeFields(promt, request);
        //stat(userDO, promt, target_pos);

        // upload Files
        uploadFiles(promt, request);
        //stat(userDO, promt, target_pos);

        // detail information for each page
        if (detailPages(promt, request, response)) {
            //stat(userDO, promt, target_pos);
            return;
        }

        // finding the fields for activation
        activateFields(promt, request);
        //stat(userDO, promt, target_pos);

        // check the page - and determine which page to show next
        if (AUTOJUMP) {
            try {
                if (((Page) promt.getField(last_pos)).isValid()) {
                    // page is valid - go on to next one
                    // trying to check if there is a page after this one
                    promt.getField(last_pos + 1);
                    target_pos = last_pos + 1;
                } else {
                    // page is not valid - to check add the errormessage and show page again
                    String checkError = promt.getField(last_pos).validate();
                    Logger.debug(thiZ, "unvalid page :" + checkError);
                    target_pos = last_pos;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                Logger.debug(thiZ, "end of pages reached");

                // end of pages reached
                // show summary and send then
                Logger.debug(thiZ, "looking for submitpage");
            }
        } else {
            Logger.debug(thiZ, "no autojump");
        } // end autojump
        

        //check the nextpage - in case it is unknown, set it to first page
        if (target_pos == 9999) {
            // firstpage
            target_pos = 0;
        }

        // set id for next page
        request.getSession().setAttribute(WebConstants.SESS_FORM_POS, new Integer(target_pos));
        //stat(userDO, promt, target_pos);

        // storing the promt
        // in case the promt is complete - store it ---------------------
        if (promt.isComplete()) {
            int notifyID = ServiceMediator.ACTION_STORE;

            if (promt.getUOID() != 0) {
                notifyID = ServiceMediator.ACTION_UPDATE;
            }

            try {
                // deactivate the submit button, so that it will not be autosaved next time
                Vector vector = new Vector();
                promt.getField(SubmitButton.class, vector);

                for (int i = 0; i < vector.size(); i++) {
                    ((SubmitButton) vector.get(i)).setValue("" + false);
                }

                promt.update(true);

                userDO.setLastExperimentID(promt.getUOID());
                userDO.update(true);
                //stat(userDO, promt, target_pos);

                PromtUserAccessRight.createFullAccess(promt.getUOID(), userDO.getUOID());

                ServiceMediator.notifyUser(promt, notifyID);
                NotificationCentral.notify(new ExperimentSubmittedNotification(promt));

                // create an xls exportfile
                XLSFileExport fileExport = null;

                try {
                    fileExport = new XLSFileExport(FileExport.createFileName(promt.getUOID(), "xls"));
                    fileExport.export(promt);
                    fileExport.createExportFile();

                    String link = fileExport.createDownloadLink();

                    // add the export to the session
                    // add the export link to the session
                    request.getSession().setAttribute(WebConstants.SESS_LINK_DOWNLOAD, link);
                } catch (FileExportException e1) {
                    CoreObject.err(this, e1);
                }

                forward("/summary.jsp", request, response);

                return;
            } catch (PersistenceActionUpdateException e3) {
                throw new WebFormException(e3);
            }
        }

        // Autosave
        // in case the promt has not been saved for the maxPeriod time - save it.
        Date now = new Date();
        long timeLastSaved = 0L;
        try {
            // get the time when the promt was saved the last time.
            timeLastSaved = Long.parseLong("" + request.getSession().getAttribute("TIME"));    
            //stat(userDO, promt, target_pos);
        } catch (Exception e) {
            Logger.warning(this, "Unable to determine when the promt has been saved.");
        }

        if (now.getTime() -  timeLastSaved > AUTOSAVE_PROMT_MAXAGE){
            Logger.log(this, "autosaving Promt.");
            try {
                //promt.update(true);
                Logger.debug(this, "saving promt " + promt.getUOID()) ;
                promt.save(userDO);
                //stat(userDO, promt, target_pos);

                Logger.debug(this, "setting last position on session: " + target_pos) ;
                request.getSession().putValue(WebConstants.SESS_FORM_POS, new Integer(target_pos));
                //stat(userDO, promt, target_pos);
                
                request.getSession().putValue(WebConstants.SESS_FORM_PROMT, promt);
                //stat(userDO, promt, target_pos);

                Logger.debug(this, "setting new lastsaved time in session: " + last_pos) ;
                request.getSession().setAttribute("TIME", "" + now.getTime());
                //stat(userDO, promt, target_pos);
                forward("/dev_form.jsp", request, response);

                return;
            } catch (org.setupx.repository.server.persistence.PersistenceActionUpdateException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        Logger.debug(thiZ, "process -- DONE - forwarding - TARGET: " + target_pos);
        //stat(userDO, promt, target_pos);
        forward("/dev_form.jsp", request, response);
    }


    private void stat(UserDO userDO, Promt promt, int target) {
        Logger.debug(thiZ, ">>>>>STAT>>>>>>>>>>>>>>");
        
        Logger.debug(thiZ, "user " + userDO.getDisplayName() + "   ");

        Logger.debug(thiZ, "target " + target + "   ");

        if (promt == null){
            Logger.debug(thiZ, "promt is NULL ");
        } else {
            Logger.debug(thiZ, "promt valid: " + promt.isValid());
    
            FormObject[] pages = promt.getFields();
            for (int i = 0; i < pages.length; i++) {
                FormObject page = pages[i];
                Logger.debug(thiZ, i + " " + page.isValid() + " " + page.validate());
            }
        }
        
        Logger.debug(thiZ, "<<<<<STAT end <<<<<<<<<<<<<<");
    }

    /**
     * adds a clean promt to the session
     *
     * @param request the request
     *
     * @throws PersistenceException it is not possible to restore the clean promt from the xmlfile
     * @throws PromtCreateException
     * @throws PersistenceException
     *
     * @deprecated - not in use anymore - use load(null)
     */
    protected static Promt addCleanPromt(HttpServletRequest request)
    throws PersistenceActionException, PromtCreateException, PersistenceException {
        // create a blank promt
        Promt promt;

        try {
            promt = Promt.load(0L);
        } catch (PersistenceActionFindException e) {
            throw new PromtCreateException(e);
        } catch (SampleScheduledException e) {
            throw new PromtCreateException(e);
        }

        Logger.debug(thiZ, "adding a new promt to the session");
        request.getSession().setAttribute(WebConstants.SESS_FORM_PROMT, promt);

        Logger.debug(thiZ, "saving the new promt. Promt id: " + promt.getUOID());

        return promt;
    }

    /**
     * determines a hashtable of all inputfileds specific for these forms  in the request, by filtering them by name.  <B>The name of all valid fields is generated, when they are created.</B>
     *
     * @param request the request
     *
     * @return a hashtable of all inputfileds specific for these forms  in the request
     *
     * @see InputField#createUName()
     */
    private final Hashtable getInputValues(HttpServletRequest request) {
        Hashtable hashtable = new Hashtable();
        Enumeration enumeration = request.getParameterNames();

        while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();

            try {
                // make sure that only the fileds get in there
                if (key.substring(0, FIELDSTART.length()).compareTo(FIELDSTART) == 0) {
                    String value = request.getParameter(key);
                    hashtable.put(key, value);
                } else {
                    Logger.debug(thiZ, "no InputField: " + key);
                }
            } catch (StringIndexOutOfBoundsException e) {
                Logger.debug(thiZ, "string to short");
            }
        }

        return hashtable;
    }

    /**
     * checks if there are fields inside the page that will be deactivated or activated
     *
     * @param promt the promt, that contains all the fields
     */
    private final static void activateFields(Promt promt, HttpServletRequest request) {
        Logger.debug(thiZ, "activateFields()");
        try {
            String value = getRequestParameter(request, WebConstants.PARAM_ACTIVATE);

            if ((value == null) || (value.length() < 2)) {
                Logger.debug(thiZ, "activateFields() - no activation");
                return;
            }

            // get the related field
            FormObject formObject = promt.getField(value);

            if (formObject == null) {
                Logger.err(thiZ, "activateFields() - unable to find field " + value);
                return;
            }

            Logger.log(thiZ, "activateFields() - found the object - old status: " + formObject.isActive());

            boolean activation;

            // change activation
            if (formObject.isActive()) {
                activation = false;
            } else {
                activation = true;
            }

            boolean activeBefore = formObject.isActive();
            formObject.setActive(activation);
            boolean activeAfter = formObject.isActive();
            
            if (activeAfter == activeBefore) throw new NullPointerException("activation change for object " + formObject.getUOID() + " not possible.");
            Logger.log(thiZ, "activateFields() - changed status: " + formObject.isActive());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * forwards a field to a autofillpage
     */
    private boolean autoFillField(Promt promt, HttpServletRequest request, HttpServletResponse response) {
        Logger.debug(thiZ, "autoFillField()");

        String fieldname = getRequestParameter(request, WebConstants.PARAM_AUTOFILL);

        if ((fieldname == null) || (fieldname.compareTo("") == 0)) {
            return false;
        }

        Logger.debug(thiZ, "AUTOFILL requested for page " + fieldname);

        String page = "/autofill.jsp";

        try {
            request.getSession().setAttribute(WebConstants.SESS_CLAZZ, (InputField) promt.getField(fieldname));
            forward(page, request, response);

            return true;
        } catch (Exception e) {
            Logger.warning(thiZ, "AUTOFILL: unable to forward to " + page);

            return false;
        }
    }

    /**
     * resizes existing classes to a defined size.
     */
    private boolean classesResize(Promt promt, HttpServletRequest request, HttpServletResponse response) {
        Logger.debug(thiZ, "classesResize()");

        String activeString = getRequestParameter(request, "classesresize");
        Logger.debug(thiZ, "classesResize() " + activeString);

        boolean active = new Boolean(activeString).booleanValue();
        Logger.debug(thiZ, "classesResize() " + active);

        if (!active) {
            return false;
        }

        // number of samples that each class should have 
        int size = getRequestParameterInt(request, "number");
        Logger.debug(thiZ, "resizing classes to " + size);

        if (size > 40) {
            size = 40;
        }

        Clazz clazz = null;
        Clazz[] clazzes = promt.getClazzes();

        for (int i = 0; i < clazzes.length; i++) {
            clazz = clazzes[i];
            Logger.debug(thiZ, "resizing class " + clazz.getUOID());
            clazz.setFields(clazz.createChilds(size));
        }

        return true;
    }

    /**
     * determine the last visisted page by getting a parameter from the session
     *
     * @param request the request
     *
     * @return the number of the page
     *
     * @see WebConstants#PARAM_FORM_LASTPAGE
     */
    private final static int determineLastVisitedPage(HttpServletRequest request) {
        Logger.debug(thiZ, "determineLastVisitedPage()");
        
        int page = getRequestParameterInt(request, WebConstants.PARAM_FORM_LASTPAGE);

        Logger.debug(thiZ, "determineLastVisitedPage(): last page: " + page);
        return page;
    }

    /**
     * finds the Promt from the session
     *
     * @param request the request, that contains the promt
     * @param userDO
     *
     * @return the promt that is inside the reuqest
     *
     * @throws PromtCreateException
     * @throws PersistenceException
     * @throws PromtCreateException unable to create a clean promt
     * @throws PersistenceActionUpdateException
     */
    private final static Promt determinePromt(HttpServletRequest request, UserDO userDO)
    throws PromtCreateException, PersistenceActionException {
        Object object = request.getSession().getAttribute(WebConstants.SESS_FORM_PROMT);
        Logger.debug(thiZ, "determinePromt() user: " + userDO + "  ");
        Logger.debug(thiZ, "determinePromt() promt: " + object + "  ");

        Promt promt = null;

        // in case the user is null OR the promt is not found in the session
        if ((object == null) || (userDO == null)) {
            Logger.warning(thiZ, "there was no promt in the session - add a clean new one");
            Logger.debug(thiZ, "checking if user has been here before: " + userDO + " ");

            try {
                promt = Promt.load(userDO);
            } catch (PersistenceActionFindException e) {
                e.printStackTrace();
            } catch (SampleScheduledException e) {
                e.printStackTrace();
            }

            if (promt == null) {
                throw new PromtCreateException("unable to create a new Promt", new NullPointerException("promt is still null"));
            }

            // deprecated - is done above in the load(..) method
            //promt.getClazzDimensionDetector().update();
            // if it is still null - load create a new one
            //if (promt == null) {
            //    promt = addCleanPromt(request);
            //}
            // add the promt to the session
            request.getSession().setAttribute(WebConstants.SESS_FORM_PROMT, promt);
        }
        // a promt was found and is casted now
        else {
            Logger.log(thiZ, "got an object in the session" + Util.getClassName(object));
            promt = (Promt) object;
        }

        //promt.update(false);
        return promt;
    }

    /**
     * determine if there are special Multifields that "requested" a detailed view.
     *
     * @return true if there is an internal forward in this method. otherwise false
     *
     * @see detail.jsp
     */
    private final boolean detailPages(Promt promt, HttpServletRequest request, HttpServletResponse response) {
        String fieldname = getRequestParameter(request, WebConstants.PARAM_DETAIL);
        Logger.debug(thiZ, "detailsPages");

        if ((fieldname == null) || (fieldname.compareTo("") == 0)) {
            return false;
        }

        Logger.debug(thiZ, "DetailView requested for page " + fieldname);

        MultiField field;

        try {
            field = (MultiField) promt.getField(fieldname);
        } catch (Exception e) {
            Logger.err(thiZ, "There was a request for a detailed view of a field that is not instance of a Multifield.");

            return false;
        }

        String page = "/detail.jsp";

        try {
            request.getSession().setAttribute(WebConstants.SESS_MULTIFIELD, field);
            forward(page, request, response);

            return true;
        } catch (Exception e) {
            Logger.warning(thiZ, "HELP_function: unable to forward to " + page);

            return false;
        }
    }

    /**
     * determine the int of the target page the last user wanted to link to
     *
     * @param request the request
     *
     * @return the pos of the page found in the session - in case nothing is found it is 9999
     */
    private static final int determineTarget(HttpServletRequest request) {
        Logger.debug(thiZ, "determineTarget");

        int target = getRequestParameterInt(request, WebConstants.PARAM_FORM_TARGET);
        
        Logger.debug(thiZ, "determineTarget: target: "+ target);
        
        return target;
        
    }

    /**
     * find the user in the session
     *
     * @param request the request where the session is taken from in which the user has to be.
     *
     * @return the user if the user is found the object of the user - otherwise null
     */
    private static final UserDO determineUser(HttpServletRequest request) {
        Logger.debug(thiZ, "determineUser()");

        Object object = request.getSession().getAttribute(WebConstants.SESS_USER);

        if (object == null) {
            return null;
        }

        if (!(object instanceof UserDO)) {
            Logger.err(thiZ, "the object found in the session was not a userDO");

            return null;
        }

        return (UserDO) object;
    }

    /**
     * checks if there fields inside the request, that want to be expanded
     *
     * @param promt
     * @param request
     *
     * @throws WebFormException
     * @throws FieldexpandException
     */
    private final static void expandFields(Promt promt, HttpServletRequest request)
    throws WebFormException {
        Logger.debug(thiZ, "expanding field");

        // fields are labeld like number_field1  --> number_ + the label of the field that will be expanded.
        String startString = "number_";

        // getting all parameter names
        Enumeration enumeration = request.getParameterNames();

        // checking if one of them matches to the name givven abouve
        while (enumeration.hasMoreElements()) {
            String element = (String) enumeration.nextElement();

            int sizeStartString = startString.length();
            int sizeElement = element.length();

            //Logger.debug(thiZ,"Startsting " + sizeStartString + " element " + sizeElement);
            // if found - expand the field
            if ((sizeElement > sizeStartString) && (element.substring(0, startString.length()).compareTo(startString) == 0)) {
                String fieldLabel = element.substring(startString.length(), element.length());
                int numberOfNewFields = getRequestParameterInt(request, element);

                try {
                    if (numberOfNewFields > 0){
                        Logger.debug(thiZ, "found expanding field: " + fieldLabel);
                        ExpandingField field = ((ExpandingField) promt.getField(fieldLabel));
                    
                        Logger.debug(thiZ, "expanding field: " + field + "  to expand by " + numberOfNewFields);
                        
                        if (field == null) {
                            throw new WebFormException(new FieldexpandException("didnt find the field: " + fieldLabel));
                        }

                        field.expand(numberOfNewFields);
                    }
                } catch (ArrayExpandException e) {
                    throw new WebFormException(e);
                }
            }
        }
    }

    /**
     * TODO _ change: instead of adding the txt and ... to the session - add the whole object . then modify jsp
     *
     * @param promt
     * @param request
     *
     * @return true if there is an internal forward in this method. otherwise false
     *
     * @see FormObject#createHelpButton()
     */
    private final boolean helpFields(Promt promt, HttpServletRequest request, HttpServletResponse response) {
        Logger.debug(thiZ, "helpFields()");

        String fieldname = getRequestParameter(request, WebConstants.PARAM_HELP);

        if ((fieldname == null) || (fieldname.compareTo("") == 0)) {
            return false;
        }

        Logger.debug(thiZ, "HELP requested for page " + fieldname);

        InputField field = (InputField) promt.getField(fieldname);
        String txt = "";
        String page = "/popup.jsp";

        try {
            request.getSession().setAttribute(WebConstants.SESS_FORM_HEADER, "Information for " + field.getQuestion());
            txt = field.getDescription() + "<BR>" + field.getLongDescription() + "<BR>" + field.getHelptext();
            request.getSession().setAttribute(WebConstants.SESS_FORM_MESSAGE, txt);

            forward(page, request, response);

            return true;
        } catch (Exception e) {
            Logger.warning(thiZ, "HELP_function: unable to forward to " + page);

            return false;
        }
    }

    /**
     * logs a user out of the system, stores the current experiment and destroys the session so that it is invalid.
     *
     * @param promt the esperiment that will be stored.
     * @param userDO User that "ownes" the session and the experiment
     * @param request the userrequest
     */
    private boolean logout(HttpServletRequest request, UserDO userDO, Promt promt) {
        String value = getRequestParameter(request, "logout");
        debug("Logout-Value: " + value);

        if (value != null) {
            Logger.log(thiZ, "- logging out - ");

            // saving user and promt
            userDO.setLastExperimentID(promt.getUOID());

            try {
                promt.save(userDO);

                /*
           promt.update(true);
           userDO.setLastExperimentID(promt.getUOID());
           userDO.update(true);
                 */
            } catch (Exception e) {
                e.printStackTrace();
            }

            request.getSession().invalidate();
            request.getSession(true);

            return true;
        }

        return false;
    }

    /**
     * checks if there are fields inside the page that will be removed.
     *
     * @param promt the promt, that contains all the fields
     * @param request the request, inside which the information is covered which fields will be deleted
     *
     * @throws WebFormException
     * @throws ArrayShrinkException unable to shrink the array of fields while deleting the field
     */
    private final static void removeFields(Promt promt, HttpServletRequest request)
    throws WebFormException {
        Logger.debug(thiZ, "remove fields");

        String startString = "remove";

        // getting all parameter names
        Enumeration enumeration = request.getParameterNames();

        // checking if one of them matches to the name givven abouve
        while (enumeration.hasMoreElements()) {
            String element = (String) enumeration.nextElement();

            int sizeStartString = startString.length();
            int sizeElement = element.length();

            // if found - expand the field
            if (startString.compareTo(element) == 0) {
                Logger.debug(thiZ, "remove() - compare: " + startString + " " + sizeStartString + " element " + element + " " + sizeElement);

                // get the name of the form that will be deleted
                String key = getRequestParameter(request, element);

                FormObject formObject = promt.getField(key);

                if (formObject == null) {
                    Logger.err(thiZ, "REMOVE: unable to find: " + key);
                }

                // the removing
                boolean result = false;

                try {
                    result = formObject.getParent().removeInternalField(key);
                } catch (ArrayShrinkException e) {
                    throw new WebFormException(e);
                }

                if (!result) {
                    Logger.err(thiZ, "REMOVE: unable to delete: " + key + " from: " + formObject.getParent());
                }
            }

            // update the Clazzdimdetector
            promt.updateClazzDimDetector();
        }
    }

    /**
     * checks the request for fields that "want" to be resized.
     * 
     * <p>
     * then it finds them and changes their size.
     * </p>
     *
     * @param promt
     * @param request
     *
     * @see BigStringInputField#createResizeButtonsX()
     */
    private final static void resizeFields(Promt promt, HttpServletRequest request) {
        Logger.debug(thiZ, "resize fields");

        //this.printRequestParameters(request);
        // TODO: dem hidden field wird nicht der korrekte wert zugeweisen - @see BigStringInputField#createResizeButtonsX()
        String value = getRequestParameter(request, "resize");

        // if there is an existing parameter for the rezise select which dimension
        if (value != null) {
            Logger.debug(thiZ, "got a field for resize: " + value);
        }
    }

    /**
     * upload files
     *
     * @param promt
     * @param request
     *
     * @throws IOException
     */
    private final static void uploadFiles(final Promt promt, final HttpServletRequest request) {
        Logger.debug(thiZ, "upload files");

        MultipartRequest multiRequest;

        try {
            // temporary file - used to have a place 
            File _tmp;

            try {
                _tmp = File.createTempFile("xyz", ".tmp", null);
            } catch (IOException e) {
                throw new IOException("unable to create a temporary file");
            }

            _tmp.delete();

            File directory = _tmp.getParentFile();
            Logger.debug(thiZ, "Saving uploaded files to " + directory.getAbsolutePath());

            multiRequest = new MultipartRequest(request, directory.getAbsolutePath());
        } catch (IOException e1) {
            // the request is not a Multipartrequest
            return;
        }

        Enumeration fileNames = multiRequest.getFileNames();
        int i = 0;

        while (fileNames.hasMoreElements()) {
            String filename = (String) fileNames.nextElement();
            i++;

            File file = multiRequest.getFile(filename);
            Logger.debug(thiZ, "uploaded file [" + i + "]: " + file.getName() + "   " + file.getAbsolutePath());
        }

        Logger.debug(thiZ, "done with uploading the files. number of uploaded files: " + i);
    }
}
