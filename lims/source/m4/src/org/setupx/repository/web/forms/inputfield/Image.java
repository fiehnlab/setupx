/**
 * ============================================================================ File:    Image.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.setupx.repository.core.util.Util;
import org.setupx.repository.web.forms.InActiveException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 *
 * @hibernate.subclass
 */
public class Image extends StringInputfield {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String img = "";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Image object.
   */
  public Image() {
    super("...", "...");
  }

  /**
   * @param question
   * @param description
   */
  public Image(String question, String img_file) {
    super(question, "");
    this.img = img_file;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() {
    return new Image(this.getQuestion(), this.img);
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String createHTML(int col) {
    StringBuffer buffer = new StringBuffer();
    buffer.append("\n<!-- " + Util.getClassName(this) + " -->\n<tr>\n" + "<td>" + this.getQuestion() + this.createRequieredNote() + "<BR/>" + "<font class=\"small\">" + this.getDescription() + "</font>");
    buffer.append("<BR/><B><font class=\"error\">" + this.validate() + "</font></B></td>");
    buffer.append("\n<td><a name=\"" + this.getName() + " \"/><input type=\"text\" name=\"" + this.getName() + "\"  value=\"" + (String) this.getValue() + "\"/>");

    buffer.append("\n<td>" + img);
    buffer.append(this.createHelpButton());

    buffer.append("</td></tr>\n");

    return buffer.toString();
  }
  
  public Node createXMLNode(Document doc) throws InActiveException {
      warning(this, "not xml exported.");
        return null;
  }

}
