package org.setupx.repository.web.pub.data.cache;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;

import org.setupx.repository.Settings;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.db.SQLSettings;
import org.setupx.repository.server.db.SQLStatementPool;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;

/**
 * @hibernate.subclass
 */
public class VariationsCache extends CacheObject {
    private String type = "";
    private String question = "";
    private String value = "";
    private long classid = 0;
    
    // id of the noncached object
    private long realObjectUOID = 0;
    
    public VariationsCache(long realObjectUOID, String type, String question, String value, long classid) {
        super();
        this.type = type;
        this.question = question;
        this.value = value;
        this.classid = classid;
        this.realObjectUOID = realObjectUOID;
        Logger.log(this, "created new instance " + this.toString());
    }

    public VariationsCache() {
        super();
    }

    public static void refresh(){
        Logger.debug(VariationFinder.class,".refresh() - START");
        
        String query = "select " +
                        "`formbig`.`uoid` AS `id`, " +
                        "`formbig`.`discriminator` AS `type`, " +
                        "`formbig`.`question` AS `question` " +
                        ",`formvalue`.`value` AS `value` " +
                        ",`clazz`.`uoid` AS `classid` " +
                        "from (((`setupx_pro`.`multiField4ClazzesHibernate` `multi` join `setupx_pro`.`formobject` `formbig`) join `setupx_pro`.`formobject` `formvalue`) join `setupx_pro`.`formobject` `clazz`) where ((`formbig`.`uoid` = `multi`.`elt`) and (`clazz`.`uoid` = `multi`.`multiField4Clazzs`) and (`formvalue`.`PARENT` = `formbig`.`uoid`) and `formvalue`.`active` and (`formvalue`.`value` <> _latin1'') and (`formvalue`.`value` <> _latin1'-- make a selection --')) order by `formbig`.`question`";

        Logger.debug(VariationsCache.class, "sql: " + query);
        Session s = createSession();
        
        List list = s.createSQLQuery(query)
            .addScalar("id", org.hibernate.Hibernate.LONG)
            .addScalar("type", org.hibernate.Hibernate.STRING)
            .addScalar("question", org.hibernate.Hibernate.STRING)
            .addScalar("value", org.hibernate.Hibernate.STRING)
            .addScalar("classid", org.hibernate.Hibernate.LONG)
            .list();
        
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            Object[] objects = (Object[]) iterator.next();

            
            VariationsCache variationsCache = 
                new VariationsCache(
                        Long.parseLong("" + objects[0].toString()),
                        objects[1].toString(),
                        objects[2].toString(),
                        objects[3].toString(),
                        Long.parseLong("" + objects[4].toString())
                );
            try {
                variationsCache.update(true);
            } catch (PersistenceActionUpdateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        Logger.debug(VariationFinder.class,".refresh() - END");
    }

    /**
     * @hibernate.property
     */
    public long getClassid() {
        return classid;
    }

    /**
     * @hibernate.property
     */
    public String getQuestion() {
        return question;
    }

    /**
     * @hibernate.property
     */
    public long getRealObjectUOID() {
        return realObjectUOID;
    }

    /**
     * @hibernate.property
     */
    public String getType() {
        return type;
    }

    /**
     * @hibernate.property
     */
    public String getValue() {
        return value;
    }

    public void setClassid(long classid) {
        this.classid = classid;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setRealObjectUOID(long realObjectUOID) {
        this.realObjectUOID = realObjectUOID;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }

}



