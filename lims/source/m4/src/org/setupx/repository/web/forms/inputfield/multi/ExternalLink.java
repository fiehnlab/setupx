/**
 * ============================================================================ File:    ExternalLink.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.web.forms.inputfield.Removable;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.URLInputfield;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 *
 * @hibernate.subclass
 */
public class ExternalLink extends MultiField implements Removable {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @deprecated just for peristencelayer
   */
  public ExternalLink() {
    this("...", "...");
  }

  /**
   * Creates a new ExternalLink object.
   *
   * @param question 
   * @param description 
   */
  public ExternalLink(String question, String description) {
    super(question, description);
    this.addField(new URLInputfield("External URL", "type the link to the external resource. (like: http://ucdavis.edu)"));
    this.addField(new StringInputfield("Description", "comment about the content that is linked."));
  }
}
