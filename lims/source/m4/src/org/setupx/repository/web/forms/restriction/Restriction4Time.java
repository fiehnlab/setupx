/**
 * ============================================================================ File:    Restriction4Time.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.ValidationAnswer;
import org.setupx.repository.web.forms.validation.ValidationException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.5 $
 *
 * @hibernate.subclass
 */
public class Restriction4Time extends Restriction {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Restriction4Time object.
   */
  public Restriction4Time() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#cloneRestriction()
   */
  public Restriction cloneRestriction() {
    return new Restriction4Time();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#validateObject()
   */
  public ValidationAnswer validateObject(InputField field)
    throws ValidationException {
    // parse the value - and check if it is a time
    try {
      parse((String) field.getValue());
    } catch (ValidationException e) {
      return new ValidationAnswer(field, e.getMessage());
    }

    return null;
  }

  /**
   * @param string
   *
   * @throws ValidationException
   */
  private void parse(String string) throws ValidationException {
    try {
      int hours = Integer.parseInt(string.substring(0, string.indexOf(':')));
      int min = Integer.parseInt(string.substring(string.indexOf(':') + 1, string.indexOf(':') + 3));

      String amPm = string.substring(string.indexOf(':') + 4, string.indexOf(':') + 6);

      if (amPm.toLowerCase().compareTo("am") != 0) {
        if (amPm.toLowerCase().compareTo("pm") != 0) {
          throw new ValidationException("Please define the time by giving am or pm.");
        }
      }

      if (hours > 12) {
        throw new ValidationException("The maximum for the hours is 12.");
      }

      if (min > 59) {
        throw new ValidationException("The maximum of minutes is 59.");
      }
    } catch (ValidationException e) {
      throw e;
    } catch (Exception e) {
      throw new ValidationException("\"" + string + "\" is not a valid Time.");
    }
  }
}
