/**
 * ============================================================================ File:    SampleComparator.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import org.setupx.repository.web.forms.inputfield.multi.Sample;

import java.util.HashSet;
import java.util.Iterator;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class SampleComparator {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private SampleComparator() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param samplesSelected TODO
   * @param sample TODO
   *
   * @return TODO
   */
  public static boolean contains(HashSet samplesSelected, Sample sample) {
    Iterator iterator = samplesSelected.iterator();

    while (iterator.hasNext()) {
      Sample element = (Sample) iterator.next();

      if (element.getUOID() == sample.getUOID()) {
        return true;
      }
    }

    return false;
  }
}
