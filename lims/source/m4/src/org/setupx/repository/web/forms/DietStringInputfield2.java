package org.setupx.repository.web.forms;

import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.Removable;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.multi.MultiField4ClazzSimple;
import org.setupx.repository.web.forms.inputfield.multi.UnsupportedClazzMappingException;

/**
 * @hibernate.subclass
 */
public class DietStringInputfield2 extends MultiField4ClazzSimple implements Removable {

    public DietStringInputfield2() {
        super();
    }

    public DietStringInputfield2(String string) { 
        super(string, "Variation");

        FormObject stringField = new StringInputfield("Variation", "please define the variation");
        this.addField(stringField);
    }

    /*
     * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#cloneField()
     /* (non-Javadoc)
     */
    public InputField cloneField() throws CloneException {
        return new DietStringInputfield2(this.getQuestion());
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /*
     *  (non-Javadoc)
     * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getPath()
     */
    public String getPath() throws UnsupportedClazzMappingException {
        throw new UnsupportedClazzMappingException();
    }

    /*
     *  (non-Javadoc)
     * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getSingleValueField()
     */
    public FormObject getSingleValueField() throws UnsupportedClazzMappingException {
        throw new UnsupportedClazzMappingException();
    }


    /* (non-Javadoc)
     * @see org.setupx.repository.web.forms.clazzes.ClazzInformation#createShortLabel()
     */
    public String createShortLabel() {
        return this.toStringHTML();
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public String toStringHTML() {
        return this.getQuestion() + " " + ((InputField) this.getField(0)).getValue();
    }

}
