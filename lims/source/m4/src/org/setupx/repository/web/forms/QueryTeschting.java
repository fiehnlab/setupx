/**
 * ============================================================================ File:    QueryTeschting.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionStoreException;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;
import org.setupx.repository.web.forms.inputfield.multi.Sample;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import org.hibernate.criterion.Expression;

import java.util.List;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class QueryTeschting {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  static Object thiz = new QueryTeschting();

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param args TODO
   *
   * @throws PersistenceActionFindException TODO
   */
  public static void main(String[] args) throws PersistenceActionFindException {
    log(thiz, "creating a sample");

    Sample sample = new Sample();

    try {
      sample.save();
    } catch (PersistenceActionStoreException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    log(thiz, "------ NOW criteria -----");

    Session session = PersistenceConfiguration.createSessionFactory().openSession();
    Transaction transaction = session.beginTransaction();

    log(thiz, "creating a sqlquery");

    Criteria crit = session.createCriteria(Sample.class);
    crit.add(Expression.eq("name", "field25"));
    crit.setMaxResults(10);

    log(thiz, "listing");

    List samples = crit.list();

    log(thiz, "found " + samples.size() + " objects");
    session.close();

    log(thiz, "------ NOW HQL -----");

    session = PersistenceConfiguration.createSessionFactory().openSession();
    transaction = session.beginTransaction();

    log(thiz, "creating a sqlquery");

    Query query = session.createQuery("from " + Sample.class.getName());

    log(thiz, "listing");

    List list = query.list();

    log(thiz, "found " + list.size() + " objects");
    session.close();


  }

  /**
   * TODO: 
   *
   * @param thiz2 TODO
   * @param string TODO
   */
  private static void log(Object thiz2, String string) {
    System.out.print(org.setupx.repository.core.util.Util.getClassName(thiz2) + ":  " + string + " \n");
  }
}
