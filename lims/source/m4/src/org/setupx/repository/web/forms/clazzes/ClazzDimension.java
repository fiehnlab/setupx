/**
 * ============================================================================ File:    ClazzDimension.java Package: org.setupx.repository.web.forms.clazzes cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.clazzes;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz;


/**
 * collection of all MultiField4Clazzes, that are on one dimension inside a promt.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 */
public class ClazzDimension extends Vector {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static ClazzDimension thiz = new ClazzDimension();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected ClazzDimension() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param i TODO
   *
   * @return TODO
   */
  public MultiField4Clazz getField(int i) {
    return (MultiField4Clazz) super.get(i);
  }

  /**
   * @return
   */
  public MultiField4Clazz[] getFields() {
    MultiField4Clazz[] fields = new MultiField4Clazz[this.size()];
    Iterator iterator = this.iterator();

    for (int i = 0; i < fields.length; i++) {
      fields[i] = (MultiField4Clazz) iterator.next();
    }

    //CoreObject.debug(this, "converting from vector " + this.size() + " to Array size: " + fields.length);

    return fields;
  }

  /**
   * TODO: 
   *
   * @param formObject TODO
   */
  public void add(MultiField4Clazz formObject) {
    //CoreObject.debug(this, "adding " + formObject);
    super.add(formObject);
  }

  /**
   * @param formObject
   *
   * @deprecated overrides the add function of the vector - to make it type save
   */
  public synchronized boolean add(Object o) {
    //if (o instanceof MultiField4Clazz) return super.add(o);
    throw new UnsupportedOperationException("use add(FormObject) or cast your " + Util.getClassName(o));
  }

  /**
   * find the Clazzdimension with the most elements
   *
   * @param dimensions TODO
   *
   * @return TODO
   */
  public static ClazzDimension determineLargestDimension(ClazzDimension[] dimensions) {
    int size = 0;
    ClazzDimension longestClazzDimension = null;

    for (int i = 0; i < dimensions.length; i++) {
      ClazzDimension dimension = dimensions[i];
      CoreObject.debug(thiz, "checking dimension size -  elements in dimension: " + dimension.size() + "  " + dimension.toString());

      if (dimension.size() > size) {
        longestClazzDimension = dimension;
        size = dimension.size();
      }
    }

    return longestClazzDimension;
  }

  /**
   * returns the <b>FIRST</b> dimension that implements AlignX.
   *
   * @param dimensions array that will be searched for the first AlignX
   *
   * @return the <b>FIRST</b> dimension that implements AlignX.
   *
   * @see AlignX
   */
  public static ClazzDimension determineXDimension(ClazzDimension[] dimensions) {
    for (int i = 0; i < dimensions.length; i++) {
      if (dimensions[i].getField(0) instanceof AlignX) {
        return dimensions[i];
      }
    }

    Logger.warning(thiz, "no instance of ALIGNX found - using the longest dimension.");

    return determineLargestDimension(dimensions);
  }

  /**
   * TODO: 
   *
   * @param i TODO
   * @param asHTML TODO
   * @param includeButton TODO
   *
   * @return TODO
   */
  public String getDescription(int i, boolean asHTML, boolean includeButton) {
    MultiField4Clazz multiField4Clazz = this.getField(i);

    // in case the multiField4Clazz is not even active 
    if (!multiField4Clazz.getParent().isActive()) {
      if (asHTML) {
        return "";
      } else {
        return null;
      }
    }
    
    StringBuffer result = new StringBuffer();

    // add specific color
    if (asHTML) {
      result.append("\n<td width=\"120\" style=background-color:" + Util.getColor(i + 1) + ">");

      // take the firsts element 
      try {
        result.append("\n<span class=\"smallwhite\">" + getMainQuestion() + " No: " + (i+1) + " </b>");
        result.append("\n<br>");
      } catch (Exception e) {
        Logger.err(thiz, e.getMessage());
      }
    }

    //   F I L T E R I N G
    // take only the name and the information that seperated it from the others as a string;
    MultiField4Clazz temp = null;
    HashSet hashSet = new HashSet();

    for (int count = 0; count < this.size(); count++) {
      if (count != i) {
        temp = (MultiField4Clazz) this.get(count);

        for (int fieldCount = 0; fieldCount < temp.size(); fieldCount++) {
          InputField field = ((InputField) multiField4Clazz.getField(fieldCount));

          //Logger.debug(thiz, "field: " + field);
          String value1 = (String) field.getValue();
          String value2 = (String) ((InputField) temp.getField(fieldCount)).getValue();

          //Logger.debug(thiz, value1 + " vs. " + value2);
          if (value1.compareTo(value2) != 0) {
            // only acive fields
            if (!hashSet.contains("" + fieldCount) && field.isActive()) {
              result.append(field.getQuestion() + ": " + field.getValue() + "<br>");

              hashSet.add("" + fieldCount);
            }
          }
        }
      }
    }

    if (includeButton && asHTML) {
      result.append(this.getField(0).createLinkedIMG());
    }

    if (asHTML) {
      result.append("</span></td>");
    }

    return result.toString();
  }

  /**
   * @return
   */
  public String getMainQuestion() {
    try {
      return ((MultiField4Clazz) this.get(0)).getQuestion();
    } catch (Exception e) {
      Logger.warning(thiz, "unable to find element nr. 0 ");

      return "";
    }
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String toString() {
    StringBuffer buffer = new StringBuffer();
    buffer.append(Util.getClassName(this));
    buffer.append(" " + this.size() + " elements:[");

    for (int i = 0; i < this.size(); i++) {
      FormObject formObject = this.getField(i);
      buffer.append(formObject.toString());
    }

    buffer.append("]");

    return buffer.toString();
  }
}
