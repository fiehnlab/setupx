/**
 * ============================================================================ File:    LocationDropDown.java Package: org.setupx.repository.web.forms.inputfield cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield;

import java.util.Set;
import java.util.Vector;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 *
 * @deprecated not in use
 */
public class LocationDropDown extends DropDown {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String[] keys = new String[] { "" };

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static Vector _values = new Vector();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new LocationDropDown object.
   *
   * @param question 
   * @param description 
   * @param values 
   * @param submitIfChanged 
   */
  public LocationDropDown(String question, String description, String[] values, boolean submitIfChanged) {
    super(question, description, values, values, submitIfChanged);
  }

  /**
   * @param question
   * @param description
   * @param set
   * @param submitOnChanged
   */
  public LocationDropDown(String question, String description, Set set, boolean submitOnChanged) {
    super(question, description, set, submitOnChanged);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.AbstractDropDown#newInstance()
   */
  public DropDown newInstance() {
    return new LocationDropDown(this.getQuestion(), this.getDescription(), super.clone(this.getValuesHibernate()), this.getSubmitOnChanged());
  }

  /**
   * TODO: 
   *
   * @param values TODO
   */
  private void addValues(String[] values) {
    for (int i = 0; i < values.length; i++) {
      _values.add(values[i]);
    }
  }
}
