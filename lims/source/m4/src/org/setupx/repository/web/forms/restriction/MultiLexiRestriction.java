/**
 * ============================================================================ File:    MultiLexiRestriction.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.core.communication.vocabulary.AbtractLibrary;
import org.setupx.repository.core.communication.vocabulary.VocabularyChecker;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.ValidationAnswer;
import org.setupx.repository.web.forms.validation.ValidationException;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.StringTokenizer;


/**
 * Restriction allowing more than one word.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.8 $
 *
 * @hibernate.subclass
 */
public class MultiLexiRestriction extends LexiRestriction {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final String SPACE = " ";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new MultiLexiRestriction object.
   */
  public MultiLexiRestriction() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws ValidationException TODO
   */
  public ValidationAnswer validateObject(InputField field)
    throws ValidationException {
    if (field.getValue() == null) {
      return null;
    }

    debug(this, "validating " + field.getValue() + "[" + Util.getClassName(field.getValue()) + "]");

    if (!(field.getValue() instanceof String)) {
      throw new ValidationException("the value is not of type String");
    }

    // cut the value into smaller values(words) and check each of them
    Enumeration words = new StringTokenizer((String) field.getValue());
    StringBuffer buffer = new StringBuffer();

    while (words.hasMoreElements()) {
      String word = (String) words.nextElement();

      // checking the word
      String[] response = VocabularyChecker.check4suggestion(word);

      // create a suggestion 
      buffer.append(findBestWord(word, response) + SPACE);
    }

    buffer = new StringBuffer(buffer.toString().replace('\n', ' '));

    // remove the last space
    int posi = buffer.length() - 1;

    try {
      if (buffer.charAt(posi) == ' ') {
        buffer = buffer.deleteCharAt(posi);
      }
    } catch (StringIndexOutOfBoundsException e) {
      // not found
    }

    // in case that both are the same - validation is null
    String a = ((String) field.getValue()).toLowerCase();
    String b = buffer.toString().toLowerCase().replace('\n', ' ');
    int compare = a.compareTo(b);
    debug(" \"" + a + "\" - \"" + b + "\"    result: " + compare);

    if (compare == 0) {
      return null;
    } else {
      return new ValidationAnswer(field, "did you mean : ", buffer.toString());
    }
  }

  /**
   * @param suggestions
   *
   * @return
   */
  private HashSet convert(String[] suggestions) {
    HashSet hashSet = new HashSet();

    for (int i = 0; i < suggestions.length; i++) {
      hashSet.add(suggestions[i]);
    }

    return hashSet;
  }

  /**
   * returns the best word - the orig one or the best suggestion
   *
   * @param origWord the original word
   * @param suggestions suggestion for misspelled word
   */
  private String findBestWord(String origWord, String[] suggestions) {
    debug(this, "trying to find best word for:\"" + origWord + "\"");

    for (int i = 0; i < suggestions.length; i++) {
      debug(this, "  suggestion " + i + ": " + suggestions[i]);
    }

    // in case there were no suggestions
    if (suggestions == null) {
      // the orig is ok
      return origWord;
    }

    // in case the orig is inside the suggestions
    for (int i = 0; i < suggestions.length; i++) {
      if (origWord.toLowerCase().compareTo(suggestions[i].toLowerCase()) == 0) {
        debug(origWord.toLowerCase() + "  " + suggestions[i].toLowerCase());

        return origWord;
      }
    }

    String mostSimilarWord = AbtractLibrary.compare(origWord, convert(suggestions));

    /*
       int similarity = 0;
       for (int i = 0; i < suggestions.length; i++) {
           int tempSimilarity  = Util.stringCompare(origWord , suggestions[i]);
           debug(this, "checking " + origWord + " against " + suggestions[i] + " similarity " + tempSimilarity + "-" + similarity);
           if (tempSimilarity > similarity) {
               mostSimilarWord = suggestions[i];
               similarity = tempSimilarity;
           }
       }
     */
    if ((mostSimilarWord == null) || (mostSimilarWord.compareTo("null") == 0)) {
      mostSimilarWord = origWord;
    }

    debug(this, "best word is:\"" + mostSimilarWord + "\"");

    return mostSimilarWord;
  }
}
