/**
 * ============================================================================ File:    SController.java Package: org.setupx.repository.web.forms cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms;

import org.setupx.repository.core.ServiceMediator;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.UserFactoryException;
import org.setupx.repository.core.user.UserNotFoundException;
import org.setupx.repository.core.user.WrongPasswordException;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.forms.inputfield.multi.Promt;

import javax.servlet.http.HttpServletRequest;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class SController extends Controller {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static Object thiZ = new SController();

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param request TODO
   *
   * @return TODO
   *
   * @throws PersistenceException TODO
   */
  protected static Promt addCleanPromt(HttpServletRequest request)
    throws PersistenceException {
    Promt promt = SPromtCreator.create();

    Logger.debug(thiZ, "adding a new promt to the session");
    request.getSession().setAttribute(WebConstants.SESS_FORM_PROMT, promt);

    // find the default user
    UserDO userDO = null;

    try {
      userDO = ServiceMediator.findUser("local", "local");
    } catch (UserNotFoundException e) {
      e.printStackTrace();
    } catch (WrongPasswordException e) {
      e.printStackTrace();
    } catch (UserFactoryException e) {
      e.printStackTrace();
    }

    // add userObject to session
    Logger.debug(thiZ, "adding userobject to the session");
    request.getSession().setAttribute(WebConstants.SESS_USER, userDO);

    return promt;
  }
}
