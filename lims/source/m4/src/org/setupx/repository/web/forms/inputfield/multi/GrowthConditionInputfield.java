/**
 * ============================================================================ File:    GrowthConditionInputfield.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.clazzes.AlignX;
import org.setupx.repository.web.forms.inputfield.BigStringInputField;
import org.setupx.repository.web.forms.inputfield.DateInputfield;
import org.setupx.repository.web.forms.inputfield.DropDown;
import org.setupx.repository.web.forms.inputfield.HumidityInputfield;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.inputfield.TemperatureInputfield;
import org.setupx.repository.web.forms.relation.Relation;
import org.setupx.repository.web.forms.restriction.Restriction4StringLength;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 *
 * @hibernate.subclass
 */
public class GrowthConditionInputfield extends MultiField4Clazz implements AlignX {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new GrowthConditionInputfield object.
   */
  public GrowthConditionInputfield() {
    super("Growth condition", "Conditions under which the Sample grew up.");

    DropDown down = new DropDown("Greenhouse", "define the location", new String[] { "UC Davis - greenhouse", "MPIMP Golm - greenhouse", DropDown.STRING_OTHER }, true);
    down.setHelptext("This field requieres the location where the sample is from.");
    this.addField(down);

    BigStringInputField bigStringInputField = new BigStringInputField("detailed Location", "when the location is not givven in the List above, please specify it here");
    this.addField(bigStringInputField);

    bigStringInputField.setRestriction(new Restriction4StringLength(20));

    Relation relation = new Relation(new String[] { DropDown.STRING_OTHER }, bigStringInputField);
    down.addRelation(relation);

    this.addField(new StringInputfield("growing place / media", "Media the plant grew on."));

    this.addField(new DateInputfield("seeding date", "Date when the sample was planted."));
    this.addField(new DateInputfield("transplanting date", "Date when the sample was transplanted."));
    this.addField(new DateTimeInputfield("harvest date", "Date and Time when the sample was harvested."));

    this.addField(new HumidityInputfield("night humidity", "humidity in Percent at night"));
    this.addField(new HumidityInputfield("day humidity", "humidity in Percent at day"));

    this.addField(new TemperatureInputfield("night temp.", "temperature in Celsius at night"));
    this.addField(new TemperatureInputfield("day temp.", "temperature in Celsius at day"));

    // LIGHT 
    //[�mol/m� s]
    // light - day length
    StringInputfield inputfield = new StringInputfield("light: daily period", "the hours of light per day ");
    inputfield.setRequiered(false);
    inputfield.setHelptext("also called \"light exposure time\"");
    inputfield.setRestriction(new Restriction4Double(0, 24));
    inputfield.setSize_x(10);
    inputfield.setExtension(" h/day");
    this.addField(inputfield);

    String unit = "&#181mol/m&#178&#183s";
    StringInputfield light = new StringInputfield("light: fluence", "light intensity at daytime");
    light.setRestriction(new Restriction4Double());
    light.setExtension(unit);
    light.setRequiered(false);
    light.setSize_x(4);
    light.setHelptext("Please define the light fluence that was used every day. ");
    this.addField(light);

    // special add method
    BigStringInputField field = DropDown.create(this, "light source", "type of lamp mounted over the sample", "", new String[] { "sun", "UV-light", "cool white light", "HPIT", });
    field.setQuestion("lamp: detailed");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.MultiField4Clazz#getInstance()
   */
  public MultiField getInstance() {
    return new GrowthConditionInputfield();
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getPath()
   */
  public String getPath() throws UnsupportedClazzMappingException {
    throw new UnsupportedClazzMappingException();
    //return OldClassTemplate.OPERATOR_COMMENT;
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz#getSingleValueField()
   */
  public FormObject getSingleValueField() throws UnsupportedClazzMappingException {
    throw new UnsupportedClazzMappingException();
    //return this.getField(1);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.clazzes.ClazzInformation#createShortLabel()
   */
  public String createShortLabel() {
    return this.toStringHTML();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.MultiField4Clazz#toStringHTML()
   */
  public String toStringHTML() {
    return this.getQuestion() + " " + ((InputField) this.getField(0)).getValue() + "  " + ((InputField) this.getField(2)).getValue();
  }
}
