/**
 * ============================================================================ File:    MultiFieldExpanding.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import java.util.StringTokenizer;

import org.setupx.repository.Config;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.forms.ArrayExpandException;
import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.Removable;
import org.setupx.repository.web.forms.inputfield.StringInputfield;


/**
 * A set of fields, that is expanding, if the user wants to add fields of the same type.
 * 
 * <p>
 * Inside this array there can be every type of inputfield.
 * </p>
 * 
 * <p>
 * Based on MultiField.
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 *
 * @hibernate.subclass
 *
 * @see org.setupx.repository.web.forms.MultiField
 */
public class MultiFieldExpanding extends MultiField implements ExpandingField {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** label shown on the expand button */
  public String keyLabel = "add field";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @deprecated just for peristencelayer
   */
  public MultiFieldExpanding() {
    this("", "", "", new StringInputfield("", ""));
  }

  /**
   * Creates a new MultiFieldExpanding object.
   *
   * @param question 
   * @param description 
   * @param longDescription 
   * @param firstInputfield the object that will be used as the first element
   */
  public MultiFieldExpanding(String question, String description, String longDescription, Removable firstInputfield) {
    super(question, description);
    this.setLongDescription(longDescription);

    try {
      firstInputfield.setRemovable(true);
      this.addField((FormObject) firstInputfield);
    } catch (Exception e) {
      if (this instanceof Clazz) {
        debug("firstInputfield is null");
      } else {
        warning(this, "firstInputfield is null");
        e.printStackTrace();
      }
    }
  }

  /**
   * @param string
   * @param string2
   * @param string3
   * @param drug1
   * @param drug2
   *
   * @deprecated does not work - FIXME
   */
  private MultiFieldExpanding(String question, String description, String longDescription, Removable firstInputfield1, Removable firstInputfield2) {
    this(question, description, longDescription, firstInputfield1);
    firstInputfield2.setRemovable(true);
    this.addField((FormObject) firstInputfield2);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Set the values inside the elements that are inside this elements.
   * 
   * <p>
   * Multifieldexpanding do not have actual values - so in case the setter is called the  object is checked, if it contains a number of lines - and the number of existing  objects inside this is expanded to the number of lines and the value in each line is
   * pasted:<br><code>((InputField)this.getField(i)).<strong>setValue(value_in_line)</strong>;</code>.
   * </p>
   *
   * @see org.setupx.repository.web.forms.inputfield.InputField#setValue(java.lang.Object)
   */
  public final void setValue(Object object) {
    //super.setValue(object);
    String _inText = (String) object;

    //debug("size of autofill string : " + _inText.length());
    if (_inText.length() < 1) {
      return;
    }

    // autofill
    debug("setting value - by autofill");

    StringTokenizer stringTokenizer = new StringTokenizer(_inText, "\n");
    int numberOfLines = stringTokenizer.countTokens();

    if (numberOfLines > this.size()) {
      log(this, "content for autoset is too long - expanding to new size");

      warning(this, "XXXXXXXXXXXXXXXXXXXXXXXXXXXX replace the setValue in Clazz - to be tested. XXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

      //this.setFields(this.createChilds(numberOfLines));
      // new way is just cloning
      try {
        this.expand(numberOfLines);
      } catch (ArrayExpandException e) {
        e.printStackTrace();
      }
    }

    // take each line and add set it to the sample
    int i = 0;

    while (stringTokenizer.hasMoreTokens()) {
      String element = stringTokenizer.nextToken();

      // hotfix
      if (this.getField(i) instanceof Sample) {
        ((Sample) this.getField(i)).setComment(element);
      } else {
        ((InputField) this.getField(i)).setValue(element);
      }

      i++;
    }
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.ExpandingField#createExpandButton()
   */
  public final String createExpandButton() {
    return "<!-- button -->\n" + "<tr><td colspan=\"2\"><font size=\"-2\">add<input type=\"text\" size=\"2\" name=\"number_" + this.getName() + "\" value=\"0\"></input> additional </font><input type=\"submit\" label=\"add\" value=\"" + keyLabel + "\"/></td></tr>";
  }

  /**
   * TODO: functionality in Controller is missing
   *
   * @param targetPage TODO
   *
   * @return TODO
   */
  public final String createExpandButtonADD(int targetPage) {
    String msg = "add an other item";
    StringBuffer buffer = new StringBuffer();
    buffer.append("form?");
    buffer.append("number_" + this.getName());
    buffer.append("=");
    buffer.append("1");

    if (targetPage != 99) {
      buffer.append("&");
      buffer.append(WebConstants.PARAM_FORM_TARGET);
      buffer.append("=");
      buffer.append(targetPage);
    }

    return "<a href=\"" + buffer.toString() + "\"	title=\"" + msg + "\">" + IMG_PLUS + "</a>  ";
  }

  /**
   * TODO: functionality in Controller is missing
   *
   * @return TODO
   */
  public String createExpandButtonREDUCE() {
    int target_page = this.parentPage().getInternalPosition();

    return super.createRemoveButton("", "", target_page);
  }

  /*public final String createExpandButtonREDUCE() {
     return ((Removable) this.lastItem()).createRemoveButton("remove item", Sample.IMG_MINUS);
     }*/
  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.FormObject#createHTML()
   */
  public String createHTML(int col) {
    StringBuffer response = new StringBuffer();

    // create a table contaitaining 
    response.append("<!-- " + this.getName() + " (" + this.getDescription() + ") -->");

    // anchor
    response.append(this.createAnchor());

    // setting my own number of cols
    int colInternal = 4;

    if (Config.NEW_DESIGN) {
      response.append("<!-- " + this.getName() + " (" + this.getDescription() + ") -->");
      response.append("<tr><td colspan=\"" + (col) + "\" align=\"center\">");
      response.append("<fieldset>");
      response.append("<legend>" + this.getQuestion() + "</legend>");
      response.append("<table " + WebConstants.BORDER + " width=\"100%\">\n");

      // description
      response.append("<tr><td  colspan=\"" + colInternal + "\">");
      response.append("<i>" + this.getDescription() + "</i>");
      response.append("<br>");
      response.append("<font class=\"small\">" + this.getLongDescription() + "</font>");
      response.append("</td></tr> \n");

      response.append("<tr><td colspan=\"" + colInternal + "\">");
      response.append("<i>Currently there are/is " + this.size() + " " + ((InputField) this.getField(0)).getQuestion() + "s. ");
      this.keyLabel = this.getQuestion();
      response.append(this.createExpandButton());
      response.append("</td></tr>");

      response.append("<tr><td colspan=\"" + colInternal + "\" align=\"center\">\n");
    } else {
      response.append(htmlCreateHeader(col));

      response.append("<tr><td colspan=\"" + col + "\"><span class=\"small\"> Currently there are/is " + this.size() + " " + ((InputField) this.getField(0)).getQuestion() + "-Fields. If you need more inputfields, just type in the number of needed fields.</span>" + this.createExpandButton() +
        "</td></tr>");

      response.append("<tr><td colspan=\"" + col + "\" align=\"center\">");
      response.append("\n<table " + WebConstants.BORDER + " width=\"90%\">\n");
    }

    // anchor
    response.append(this.createAnchor());

    // put every inputfield in there
    response.append(this.createHTMLChilds(colInternal));

    // add an expand button
    // - moved up - 
    // response.append(this.createExpandButton());
    // add a help button
    response.append(this.createHelpButton());

    //  close the table
    response.append("</table>");

    if (Config.NEW_DESIGN) {
      response.append("</fieldset>");
    } else {
      response.append("</td></tr>");
    }

    return response.toString();
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.web.forms.ExpandingField#expand(int)
   */
  public final void expand(int i) throws ArrayExpandException {
    // object that will be cloned
    Removable lastObject;

    // limit the number of elemts to 10
    if (i < 1) {
      return;
    } else if (i > 100) {
      i = 100;
    }

    // take the last object inside the array and clone it.
    try {
      lastObject = (Removable) this.getField(getFields().length - 1);
    } catch (NullPointerException e) {
      throw new ArrayExpandException("unable to expand the array");
    }

    if (lastObject == null) {
      throw new ArrayExpandException("unable to expand array cause last object is null.");
    }

    // create a number of clones based out of the existing one.
    for (int j = 0; j < i; j++) {
      Removable clone = null;

      try {
        clone = (Removable) lastObject.cloneField();
        clone.setRemovable(true);
      } catch (CloneException e1) {
        throw new ArrayExpandException(e1);
      }

      // add the new clone to the set of existing clones.
      addField((FormObject) clone);
    }
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  protected String createAutofillButton() {
    StringBuffer buffer = new StringBuffer();

    //MultiFieldExpanding sample = (MultiFieldExpanding) this.getField(0);

    /*for (int i = 0; i < sample.size(); i++) {
       FormObject inputField = sample.getField(i);
       if (inputField.isActive() && inputField instanceof StringInputfield && !(inputField instanceof Label) && !(inputField instanceof DetailImage)) {
     */

    // autofill
    StringBuffer buffer1 = new StringBuffer();
    buffer1.append("form?");
    buffer1.append(this.getName());
    buffer1.append("=");
    buffer1.append("");
    buffer1.append("&");
    buffer1.append(WebConstants.PARAM_AUTOFILL);
    buffer1.append("=");
    buffer1.append(this.getName());

    // onclick=\"window.open(this.href,this.target,'width=400,height=500,menubar=no,status=no'); return false;\" 
    String link = "<tr><td colspan=\"2\"><font size=\"-2\">Use copy&paste to fill the values: <a href=\"" + buffer1.toString() + "\"   >" + IMG_AUTOFILL + "</a></td></tr>";
    buffer.append(link);

    /*} else {
       buffer.append("<td></td>");
       }
       }*/

    // TODO Auto-generated method stub
    return buffer.toString();
  }

}
