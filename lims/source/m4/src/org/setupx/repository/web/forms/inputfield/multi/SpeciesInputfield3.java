/**
 * ============================================================================ File:    SpeciesInputfield3.java Package: org.setupx.repository.web.forms.inputfield.multi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.core.communication.experimentgeneration.MetadataSource;
import org.setupx.repository.core.communication.experimentgeneration.template.ClazzTemplate;
import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.core.communication.ncbi.local.NCBIEntry;
import org.setupx.repository.core.util.hotfix.oldsamples.mapping.MappingException;
import org.setupx.repository.web.forms.ArrayExpandException;
import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.NullMetaDatasource;
import org.setupx.repository.web.forms.clazzes.AlignX;
import org.setupx.repository.web.forms.inputfield.BigStringInputField;
import org.setupx.repository.web.forms.inputfield.DateInputfield;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.StringInputfield;
import org.setupx.repository.web.forms.relation.RelationKingdom;
import org.setupx.repository.web.forms.restriction.Restriction4Int;
import org.setupx.repository.web.forms.restriction.Restriction4StringLength;


/**
 * New Version.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.5 $
 *
 * @hibernate.subclass
 */
public class SpeciesInputfield3 extends MultiField implements AlignX {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @throws MappingError 
 * @deprecated just for peristencelayer
   */
  public SpeciesInputfield3() throws MappingError {
    this(" ");
  }

  /**
   * Creates a new SpeciesInputfield3 object.
   *
   * @param string
   *
   * @throws MappingException
   * @throws MappingError
   * @throws
   */
  public SpeciesInputfield3(String speciesName) throws MappingError {
    this(speciesName, new NullMetaDatasource());
  }

  /**
   * Creates a new SpeciesInputfield3 object.
   *
   * @param speciesName 
   * @param source 
   *
   * @throws MappingError
   * @throws MappingException
   * @throws MappingException 
   */
  public SpeciesInputfield3(String speciesName, final MetadataSource source)
    throws MappingError {
    super("Biological Source ", "define the sample and the organ as detailed as possible.");

    NBCISpeciesInputfield species = null;

    try {
      // micro
      try {
        species = new NBCISpeciesInputfield(speciesName);
        species.getSpeciesInputfield().setValue(speciesName);

        //        source.map(species, OldClassTemplate.SPECIES);
      } catch (ArrayExpandException e) {
        throw e;
      } catch (Exception e) {
        e.printStackTrace();
      }

      this.addField(species);

      // sex 
      StringInputfield inSex = new StringInputfield("sex", "please define the gender");
      inSex.setValue("male");
      //source.map(inSex, ClazzTemplate.SEX);
      inSex.setRestriction(new Restriction4StringLength(4));
      inSex.setSize_x(7);
      this.addField(inSex);
      species.addRelation(new RelationKingdom(new int[] { NCBIEntry.ANIMAL, NCBIEntry.HUMAN }, new InputField[] { inSex }));

      // birth - human
      DateInputfield inHdob = new DateInputfield();
      inHdob.setQuestion("DOB");
      inHdob.setRequiered(false);
      inHdob.setDescription("date of birth");
      //source.map(inHdob, ClazzTemplate.DOB);
      addField(inHdob);

      // age - only animals
      StringInputfield inAAge = new StringInputfield("age", "the animals age");
      inAAge.setSize_x(4);
      inAAge.setRequiered(false);
      inAAge.setRestriction(new Restriction4Double(0, 98));
      inAAge.setExtension("years");

      this.addField(inAAge);
      species.addRelation(new RelationKingdom(new int[] { NCBIEntry.ANIMAL }, new InputField[] { inAAge }));

      // dev stage
      StringInputfield inPDevStage = new StringInputfield("Development Stage", "");

      //inPDevStage.setHelptext("TODO");
      //inPDevStage.setRestriction(new OBORestriction(new java.io.File(Config.DIRECTORY_OBO + java.io.File.separator + "po_temporal.obo" )));
      source.map(inPDevStage, ClazzTemplate.DEV_STAGE);
      this.addField(inPDevStage);
      species.addRelation(new RelationKingdom(new int[] { NCBIEntry.PLANT }, new InputField[] { inPDevStage }));

      // BMI
      StringInputfield inHBMI = new StringInputfield("BMI", "body mass index");
      inHBMI.setHelptext("A popular method used to gauge whether or not a person is overweight. <br>BMI is calculated by dividing a person's weight (in kilograms) by his or her height (in meters, squared).");
      inHBMI.setRestriction(new Restriction4Int());
      inHBMI.setSize_x(4);
      inHBMI.setRequiered(false);
      this.addField(inHBMI);

      // human diagnosis
      StringInputfield inDiagn = new BigStringInputField("Diagnosis", "diagnosis");
      inDiagn.setRestriction(null);
      this.addField(inDiagn);
      species.addRelation(new RelationKingdom(NCBIEntry.HUMAN, new InputField[] { inHBMI, inHdob, inDiagn }));

      // backgorund line
      StringInputfield inPBackgroudLine = new StringInputfield("Parental Line", "the background line");
      inPBackgroudLine.setRequiered(false);
      source.map(inPBackgroudLine, ClazzTemplate.BACKGOUND);
      this.addField(inPBackgroudLine);
      species.addRelation(new RelationKingdom(new int[] { NCBIEntry.PLANT }, new InputField[] { inPDevStage }));

      /* doppelt - ist in organ input field abgelegt.
         // organs HUMAN
         OrganInputfield inputOrganString = new OrganInputfield("organ", "define the organ (human)", species);
         InputField inOrganHUMAN = new MultiFieldExpandingSimple("Organs", "define all organs ", "define all organs that samples were taken from.", inputOrganString);
         warning(this, "NCICBOrganRestriction deactivated.");
         species.addRelation(new RelationKingdom(NCBIEntry.HUMAN, inOrganHUMAN));
         this.addField(inOrganHUMAN);
       */

      // organs other
      OrganInputfield organInputfield = new OrganInputfield("organ", "define the organ", species);
      InputField inOrganGeneral = new MultiFieldExpandingSimple("Organs", "define all organs ", "define all organs that samples were taken from.", organInputfield);
      this.addField(inOrganGeneral);
      //species.addRelation(new RelationKingdom(new int[] { NCBIEntry.HUMAN, NCBIEntry.ANIMAL, NCBIEntry.PLANT, NCBIEntry.MIRCOORGANISM }, inOrganGeneral));
      // MAPPING
      source.map(organInputfield);

      // line			
      GenotypeInputfield removable = new GenotypeInputfield("Line", "line, strain, or genotype home");
      InputField inPLine = new MultiFieldExpandingSimple("Genotype", "line, strain, or genotype home", "", removable);
      inPLine.setHelptext("TODO");
      inPLine.setRequiered(false);
      this.addField(inPLine);

      source.map(removable);


      /*
       */
      species.addRelation(new RelationKingdom(new int[] { NCBIEntry.PLANT, NCBIEntry.ANIMAL }, new InputField[] { inPLine, inPBackgroudLine }));
    } catch (ArrayExpandException e) {
      e.printStackTrace();
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.MultiField4Clazz#cloneField()
   */
  public InputField cloneField() throws CloneException {
    //      SpeciesInputfield3 instance = new SpeciesInputfield3();
    //      instance.create("--unknown--");
    //      return instance;
    SpeciesInputfield3 spClone;

    try {
      spClone = new SpeciesInputfield3("" + this.species().getSpeciesName());
    } catch (MappingError e) {
      throw new CloneException(e);
    }

    // take each value and take the value
    if (spClone.getFields().length != this.getFields().length) {
      throw new CloneException("unable to clone " + org.setupx.repository.core.util.Util.getClassName(this) + " cause the number of Fields in the clone is not identical.");
    }

    Object o;

    for (int i = 0; i < this.getFields().length; i++) {
      o = ((InputField) this.getField(i)).getValue();
      ((InputField) spClone.getField(i)).setValue(o);
    }

    spClone.species().setValue(species().getValue());

    return spClone;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.clazzes.ClazzInformation#createShortLabel()
   */
  public String createShortLabel() {
    return determineMainValue();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.MultiField4Clazz#determineMainValue()
   */
  public String determineMainValue() {
    try {
      NBCISpeciesInputfield speciesInputfield = this.findNBCISpeciesInputfield();

      return "Species: " + speciesInputfield.entry.getName();
    } catch (Exception e) {
      return "Species";
    }
  }

  /**
   * @return
   */
  public NBCISpeciesInputfield species() {
    return this.findNBCISpeciesInputfield();
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.MultiField4Clazz#toStringHTML()
   */
  public String toStringHTML() {
    return determineMainValue();
  }
  
  public String createHTML(int col) {
      this.species().updateRelations(this.species().entry);
      return super.createHTML(col);
  }
}
