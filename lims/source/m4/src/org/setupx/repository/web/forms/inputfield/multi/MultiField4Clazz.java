package org.setupx.repository.web.forms.inputfield.multi;

import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.web.forms.ArrayExpandException;
import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.FormObjectComparator;
import org.setupx.repository.web.forms.clazzes.ClazzInformation;
import org.setupx.repository.web.forms.inputfield.InputField;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;


/**
 * Special Inputfield, which contains several inputfileds like a Multifiel.  <br><b>These Objects are used to create the clazzes.</b>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 *
 * @hibernate.subclass
 */
public abstract class MultiField4Clazz extends MultiField implements ClazzInformation {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new MultiField4Clazz object.
   *
   * @param question 
   * @param description 
   */
  public MultiField4Clazz(String question, String description) {
    super(question, description);
  }

  /**
   * @deprecated just for peristencelayer
   */
  public MultiField4Clazz() {
    this("...", "...");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.InputField#cloneField()
   */
  public InputField cloneField() throws CloneException {
    MultiField4Clazz instance = null;

    try {
      instance = (MultiField4Clazz) this.getClass().newInstance(); // this.cloneField();
    } catch (InstantiationException e) {
      throw new CloneException(e);
    } catch (IllegalAccessException e) {
      throw new CloneException(e);
    }

    if (instance.getFields().length != this.getFields().length) {
      Logger.logAnatomize(instance);
      Logger.logAnatomize(this);
      throw new CloneException("unable to clone " + Util.getClassName(this) + " cause the number of Fields in the clone is not identical. Should be " + this.size() + " but is " + instance.getFields().length);
    }

    Object o;

    // take each value and set the the value in the clone
    for (int i = 0; i < this.getFields().length; i++) {
      o = ((InputField) this.getField(i)).getValue();
      ((InputField) instance.getField(i)).setValue(o);

      // active or not
      instance.getField(i).setActive(this.getField(i).isActive());
    }

    return instance;
  }

  /**
   * TODO: 
   *
   * @param multiField4ClazzsActive TODO
   *
   * @return TODO
   */
  public static MultiField4Clazz[] toArray(HashSet multiField4ClazzsActive) {
    MultiField4Clazz[] multiField4Clazzs = new MultiField4Clazz[multiField4ClazzsActive.size()];
    int i = 0;

    for (Iterator iter = multiField4ClazzsActive.iterator(); iter.hasNext(); i++) {
      multiField4Clazzs[i] = (MultiField4Clazz) iter.next();
    }

    Arrays.sort(multiField4Clazzs, FormObjectComparator.byID());

    return multiField4Clazzs;
  }

  /**
   * Returns tha path where the value would be found in an imported xml file. The Path of each field makes it possible for external tools to map information onto the existing objects.
   *
   * @return information where the mapped information would be in an imported xml file
   *
   * @throws UnsupportedClazzMappingException
   *
   * @see org.setupx.repository.core.communication.experimentgeneration.MetadataSource#map(InputField, String)
   */
  public abstract String getPath() throws UnsupportedClazzMappingException;

  /**
   * mapping n-Values onto this object and if nessessary clone of this.
   * <h3>
   * 
   * <ul>
   * <li>
   * <b>case one:</b> the vlaues hashset contains exactly ONE object -  what happens is that that specific value is mapped onto this object using - the singleValue Field.
   * </li>
   * <li>
   * <b>case two:</b> the hashset contains more then one value -  what happens is that clones of this object are created. The number clones plus this is the number of elements in the hashset, so that every value can be mapped onto a singe object.
   * </li>
   * </ul>
   * 
   *
   * @param values
   *
   * @throws UnsupportedClazzMappingException
   * @throws CloneException unable to create a clone
   * @throws MappingError illegal set of values
   */
  public final void setSingleValue(HashSet values) throws UnsupportedClazzMappingException, CloneException, MappingError {
    if ((values == null) || (values.size() == 0)) {
      throw new MappingError("illegal set of values.");
    }

    //HashSet fieldsToMapOn = new HashSet();
    //fieldsToMapOn.add(this);
    // checking if the number of values matches the number of objects to map on.
    try {
      debug("requiered size: " + values.size());
      ((MultiFieldExpanding) this.getParent()).expand(values.size() - 1);
    } catch (ArrayExpandException e) {
      throw new CloneException(e);
    }

    /*
       for (; fieldsToMapOn.size() != values.size(); ) {
           debug("creating clone for matching size: " + fieldsToMapOn.size() + " vs. " + values.size());
           fieldsToMapOn.add(this.cloneField());
           // add it to the parent
           ((MultiFieldExpanding)this.getParent()).addField(cloneField());
       }
     */

    // when done - map each value on one of the clones
    Iterator valuesIter = values.iterator();
    MultiFieldExpanding parentFormObject = (MultiFieldExpanding) this.getParent();
    FormObject[] formObject = parentFormObject.getFields();

    for (int i = 0; i < formObject.length; i++) {
      MultiField4Clazz multiField4Clazz = (MultiField4Clazz) formObject[i];
      String value = valuesIter.next() + "";
      multiField4Clazz.setSingleValue(value);
    }

    /*
       for (Iterator fields = fieldsToMapOn.iterator(); fields.hasNext();) {
           MultiField4Clazz multiField4Clazz = ((MultiField4Clazz) fields.next());
           multiField4Clazz.setSingleValue(value);
       }
     */
  }

  /**
   * TODO: 
   *
   * @param value TODO
   *
   * @throws UnsupportedClazzMappingException TODO
   */
  public final void setSingleValue(String value) throws UnsupportedClazzMappingException {
    InputField inputField = (InputField) this.getSingleValueField();
    debug("set value \"" + value + "\" inside of: " + Util.getClassName(inputField));
    inputField.setValue(value);
  }

  /*{
     throw new UnsupportedOperationException("mapping for " + Util.getClassName(this) + " not supported.");
     }*/

  /**
   * return a value that is representing the uniqe value of this object.
   * 
   * <p></p>

   * <p>
   * the value returned should represent the main value.
   * </p>
   * 
   * <p>
   * Implemented mainly for mapping tools (Old experiments onto a promt).
   * </p>
   *
   * @return value represent the main value of the field
   *
   * @throws UnsupportedClazzMappingException
   *
   * @see OrganInputfield
   * @see #getSingleValueField()
   */
  public final String getSingleValue() throws UnsupportedClazzMappingException {
    InputField inputField = (InputField) this.getSingleValueField();

    //debug("information \"" + inputField.getValue() + "\" was requested from: " + Util.getClassName(inputField));
    return "" + inputField.getValue();
  }

  /**
   * returns the filed that contains the "single" information.
   * <h3>
   *
   * @throws UnsupportedClazzMappingException
   *
   * @see #getSingleValue()
   * @see #setSingleValue()
   */
  public abstract FormObject getSingleValueField() throws UnsupportedClazzMappingException;
  
  /*
   * (non-Javadoc)
   *
   * @hibernate.property
   *
   * @see org.setupx.repository.web.forms.Removable#getRemovable()
   */
  public boolean getRemovable() {
    Logger.debug(this, "  NEEDS check:  overwritten getRemovable() ");
    return true;
  }

}
