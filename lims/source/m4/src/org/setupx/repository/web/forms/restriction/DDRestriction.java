/**
 * ============================================================================ File:    DDRestriction.java Package: org.setupx.repository.web.forms.restriction cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.web.forms.restriction;

import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.ValidationAnswer;


/**
 * Drop Down Restriction
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.5 $
 *
 * @hibernate.subclass
 */
public class DDRestriction extends Restriction {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String illegalValue;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new DDRestriction object.
   *
   * @param name 
   * @param illegalValue 
   */
  public DDRestriction(String illegalValue) {
    this.illegalValue = illegalValue;
  }

  /**
   * Creates a new DDRestriction object.
   */
  public DDRestriction() {
    this("");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param illegalValue TODO
   */
  public void setIllegalValue(String illegalValue) {
    this.illegalValue = illegalValue;
  }

  /**
   * @hibernate.property
   */
  public String getIllegalValue() {
    return illegalValue;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#cloneRestriction()
   */
  public Restriction cloneRestriction() {
    return new DDRestriction(this.illegalValue);
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.web.forms.restriction.Restriction#validateObject()
   */
  public ValidationAnswer validateObject(InputField field2) {
    if (this.illegalValue.compareTo((String) field2.getValue()) == 0) {
      return new ValidationAnswer(field2, "Please make a selection.");
    } else {
      return null;
    }
  }
}
