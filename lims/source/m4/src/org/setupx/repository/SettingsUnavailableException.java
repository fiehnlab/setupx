package org.setupx.repository;

public class SettingsUnavailableException extends Exception {

    public SettingsUnavailableException() {
        // TODO Auto-generated constructor stub
    }

    public SettingsUnavailableException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public SettingsUnavailableException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public SettingsUnavailableException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
