package org.setupx.repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;

public class Settings {
    
    
    private static File file = new File("/usr/share/setupx/lims/conf/system/system.properties");
    //private static File file = new File("/Users/scholz/setupx_gcode/lims/conf/system/system.properties");
    private static Properties p = new Properties();
    public static Date age = new Date();

    static {
        try {
            load();
        } catch (SettingsUnavailableException e) {
            e.printStackTrace();
        }
    }

    public static String getValue(String key) {
        return getValue(key, "unknown");
    }

    private static void load() throws SettingsUnavailableException {
        try {
            p.load(new FileInputStream(file));
            age = new Date();
        } catch (FileNotFoundException e) {
            throw new SettingsUnavailableException(e);
        } catch (IOException e) {
            throw new SettingsUnavailableException(e);
        }
    }

    private static String storeProperties(String key) throws SettingsUnavailableException {
        // value is not known
        try {
            p.store(new FileOutputStream(file), "settings for \"" + key + "\" were not found or modified.");

            // after storing them - open the file again and sort the entries
            org.setupx.repository.core.util.File.sortEntries(file);

        } catch (FileNotFoundException e) {
            throw new SettingsUnavailableException(e);
        } catch (IOException e) {
            throw new SettingsUnavailableException(e);
        }

        return p.getProperty(key);
    }

    public static void refresh() throws SettingsUnavailableException {
        load();
    }

    /**
     * read a parameter from the config file - if value can not be found the default value will be added to the
     * properties file.
     * 
     * @param key
     * @param defaultValue
     * @return
     */
    public static String getValue(String key, String defaultValue) {
        String value = p.getProperty(key);
        if (value == null || value.compareTo("null") == 0) {
            p.setProperty(key, defaultValue);
            // storing the properties file and adding the missing value
            try {
                return storeProperties(key);
            } catch (SettingsUnavailableException e) {
                e.printStackTrace();
                return defaultValue;
            }
        } else {
            return value;
        }

    }
    
    
    /**
     * keys of all properties
     */
    public static Enumeration keys(){
        return p.keys();
    }

    /**
     * @return a list of all settings stored in the properties file.
     */
    public static String allStoredSettings() {
        StringBuffer result = new StringBuffer();

        result.append("settings stored in " + file.getName() + "[" + file.getAbsolutePath() + "] at "
                + age.toLocaleString() + ": ");

        Enumeration keys = p.keys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement() + "";
            String value = p.getProperty(key);

            result.append("\n" + key + ":\t" + value);
        }
        return result.toString();
    }

    public static boolean getValue(String key, boolean b) {
        return Boolean.valueOf(getValue(key, "" + b)).booleanValue();
    }

    public static int getValue(String key, int defaultvalue) {
        return Integer.parseInt(getValue(key, "" + defaultvalue));
    }

    public static File getFile() {
        return file;
    }

    public static void setFile(File file) {
        Settings.file = file;
    }
    
    public static void setValue(String key, String value) throws SettingsUnavailableException{
        p.setProperty(key, value);
        storeProperties(key);
    }
}
