/**
 * ============================================================================ File:    MDCore.java Package: org.setupx.repository.core.communication.importing.metadata cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.importing.metadata;

import org.setupx.repository.core.CoreObject;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class MDCore extends CoreObject {
  /*    public static void log(Object object, String string) {
     System.out.print(string);
     }
     public static void debug(Object object, String string) {
         System.out.print(string);
     }
     public static void warning(Object object, String string) {
         System.err.print(string);
     }
  
     public void debug(String string) {
         debug(null, string);
     }
   */
}
