/**
 * ============================================================================ File:    Randomisation.java Package: org.setupx.repository.core.communication.exporting cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.exporting;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample;
import org.setupx.repository.core.communication.exporting.sampletypes.Blank;
import org.setupx.repository.core.communication.exporting.sampletypes.MethodBlank;
import org.setupx.repository.core.communication.exporting.sampletypes.QC;
import org.setupx.repository.core.communication.exporting.sampletypes.RegionBlank;
import org.setupx.repository.core.util.logging.Logger;

import java.util.Random;
import java.util.Vector;


/**
 * randomises a set of samples defined by an SOP.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
class Randomisation extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Machine machine;
  private String sopID;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Randomisation() {
    throw new UnsupportedOperationException();
  }

  private Randomisation(Machine i_machine, String i_sopID) {
    this.sopID = i_sopID;
    this.machine = i_machine;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * creates a new randomisation
   *
   * @param machine the machine the samples will be run on
   *
   * @return the new randomisation-object
   */
  public static Randomisation newInstance(Machine machine, String sopID) {
    return new Randomisation(machine, sopID);
  }

  /**
   * TODO: 
   *
   * @param samples TODO
   * @param information TODO
   *
   * @return TODO
   */
  public AcquisitionSample[] randomise(AcquisitionSample[] samples, JobInformation information) {
    debug("starting randomising " + samples.length + " samples for machine: " + machine.getUOID());

    // randomisation
    samples = randomise(samples);

    // in case blanks are requiered before the sample
    if (information.isSurroundingBlanks()) {
      log("adding blanks");

      // find the blanks
      Blank blank = null;

      for (int i = 0; i < samples.length; i++) {
        AcquisitionSample sample = samples[i];

        if (sample instanceof Blank) {
          blank = (Blank) sample;
        }
      }

      if (blank == null) {
        warning(this, "There are NO blanks in this Acqusition. Blank runs can only be added when the blank solvent is somewhere on the tray.");

        return samples;
      }

      // create a copy to one of the blanks
      Blank clone = null;

      try {
        clone = new Blank(information); //blank.cloneBlank();
        clone.addParameter(new AcquisitionParameter(AcquisitionParameter.VIAL, blank.getParamter(AcquisitionParameter.VIAL).getValue()));
      } catch (MappingError e) {
        e.printStackTrace();
      } catch (ParameterException e) {
        e.printStackTrace();
      }

      Vector samplesVec = Acquisition.convert(samples);

      for (int i = 0; i < samplesVec.size(); i++) {
        AcquisitionSample acquisitionSample = (AcquisitionSample) samplesVec.get(i);

        // in case it is a real sample
        if (acquisitionSample.typeShortcut.compareTo(AcquisitionSample.AcquisitionLabel) == 0) {
          // paste it before the sample
          samplesVec.add(i, clone);
          i = i + 1;
        }
      }

      return Acquisition.convert(samplesVec);
    }

    return samples;
  }

  /**
   * TODO: 
   *
   * @param unrandomised TODO
   *
   * @return TODO
   */
  private AcquisitionSample[] randomise(AcquisitionSample[] unrandomised) {
    Random Generator = new Random();

    AcquisitionSample[] randomised = new AcquisitionSample[unrandomised.length];

    // before starting - check that only the samples are beeing randomised
    for (int i = 0; i < unrandomised.length; i++) {
      AcquisitionSample sample = unrandomised[i];

      if ((sample instanceof Blank) || (sample instanceof MethodBlank) || (sample instanceof QC) || (sample instanceof RegionBlank)) {
        debug(sample.getClass().getName() + " is not beeing randoized - placing at pos: " + i);
        randomised[i] = sample;
      }
    }

    // take sample by sample and put it on a new position that is random
    for (int i = 0; i < unrandomised.length;) {
      AcquisitionSample sample = unrandomised[i];
      int randomPosition = Generator.nextInt(unrandomised.length);

      if ((sample instanceof Blank) || (sample instanceof MethodBlank) || (sample instanceof QC) || (sample instanceof RegionBlank)) {
        // skip it
        i++;
      } else if (randomised[randomPosition] == null) {
        Logger.log(this, "Sample " + i + " was moved to " + randomPosition);
        randomised[randomPosition] = sample;
        i++;
      } else {
        Logger.log(this, "position " + randomPosition + " is used already.");
      }
    }

    return randomised;
  }
}
