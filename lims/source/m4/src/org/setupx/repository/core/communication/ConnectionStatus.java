/**
 * ============================================================================ File:    ConnectionStatus.java Package: org.setupx.repository.core.communication cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication;

import org.setupx.repository.core.Connectable;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.Util;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.9 $
 */
public class ConnectionStatus extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Connectable con;
  private String serviceName;
  private Throwable error;
  private boolean available;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ConnectionStatus object.
   *
   * @param connectable 
   */
  public ConnectionStatus(Connectable connectable) {
    this.con = connectable;
    this.serviceName = Util.getClassName(connectable);

    try {
      connectable.checkAvailability();
      this.available = true;
    } catch (ServiceNotAvailableException e) {
      this.available = false;
      this.error = e.getCause();
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public boolean isAvailable() {
    return available;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public Throwable getError() {
    return error;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getServiceName() {
    return serviceName;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String toString() {
    if (isAvailable()) {
      return this.getServiceName() + " " + this.isAvailable();
    } else {
      return this.getServiceName() + " " + this.isAvailable() + " cause: " + error;
    }
  }
}
