/**
 * ============================================================================ File:    XMLFile.java Package: org.setupx.repository.core.util.xml cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.xml;

import org.setupx.repository.core.CoreObject;

import org.apache.xerces.parsers.DOMParser;

import org.jdom.JDOMException;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UTFDataFormatException;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


/**
 * 
 *
 * @author <a href="mailto:scholz@zeypher.com?subject=m1" >&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.11 $
 */
public class XMLFile extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected File sourceFile = null;
  private Document doc = null;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param document
   */
  public XMLFile(Document document) {
    doc = document;
  }

  /**
   * Creates a new XMLFile object.
   *
   * @param documentJDOM 
   */
  public XMLFile(org.jdom.Document documentJDOM) {
    try {
      doc = DocumentConverter.convert(documentJDOM);
    } catch (JDOMException e) {
      e.printStackTrace();
    }
  }

  /**
   * Creates a new XMLFile object.
   *
   * @param i_SourceFile 
   *
   * @throws SAXException
   * @throws CreateException
   */
  public XMLFile(File i_SourceFile) throws SAXException, UTFDataFormatException {
    if (i_SourceFile != null) {
      this.sourceFile = i_SourceFile;
    } else {
      throw new SAXException(new NullPointerException("sourcefile " + i_SourceFile + " is null"));
    }

    if (!i_SourceFile.exists()) {
      throw new SAXException("file " + i_SourceFile.toString() + " does not exist.");
    }

    if (!i_SourceFile.canRead()) {
      throw new SAXException("file " + i_SourceFile.toString() + " can not be read.");
    }

    DOMParser parser = new DOMParser();
    String _path = i_SourceFile.getAbsolutePath();

    try {
      parser.parse(_path);
    } catch (UTFDataFormatException e) {
      warning(this, e.toString());
      throw e;
    } catch (IOException e) {
      warning(this, e.toString());
      throw new SAXException(e);
    }

    doc = parser.getDocument();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public Document getDocument() {
    return this.doc;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public File getSourceFile() {
    return sourceFile;
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws TransformerConfigurationException TODO
   * @throws TransformerFactoryConfigurationError TODO
   * @throws FileNotFoundException TODO
   * @throws TransformerException TODO
   * @throws IOException TODO
   */
  public String content() throws TransformerConfigurationException, TransformerFactoryConfigurationError, FileNotFoundException, TransformerException, IOException {
    Transformer transformer = TransformerFactory.newInstance().newTransformer();
    DOMSource source = new DOMSource(doc);

    File tempFile = File.createTempFile("XXX", "TMP");
    OutputStream outputStream = new FileOutputStream(tempFile);
    StreamResult result = new StreamResult(outputStream);

    transformer.transform(source, result);
    outputStream.close();

    String content = org.setupx.repository.core.util.File.getFileContent(tempFile);
    
    tempFile.delete();
    
    return content;
  }

  /**
   * TODO: 
   *
   * @param xPath TODO
   *
   * @return TODO
   *
   * @throws XPathException TODO
   */
  public String pathValue(String xPath) throws XPathException {
    return XPath.path(this.getDocument(), xPath).item(0).getFirstChild().getNodeValue();
  }

  /**
   * TODO: 
   *
   * @param file TODO
   *
   * @throws TransformerConfigurationException TODO
   * @throws TransformerFactoryConfigurationError TODO
   * @throws FileNotFoundException TODO
   * @throws TransformerException TODO
   * @throws IOException TODO
   */
  public void storeDocument(File file) throws TransformerConfigurationException, TransformerFactoryConfigurationError, FileNotFoundException, TransformerException, IOException {
    if (file.exists()) {
      log(this, "replacing file  >>" + file.getName() + "<<");
    }

    Transformer transformer = TransformerFactory.newInstance().newTransformer();

    DOMSource source = new DOMSource(doc);

/*    if (!file.getParentFile().exists()) {
      file.getParentFile().mkdir();
      debug(this, "created a new directory called: " + file.getParentFile().getAbsolutePath());
    }
*/  
    FileOutputStream os = new FileOutputStream(file);
    StreamResult result = new StreamResult(os);

    transformer.transform(source, result);
    os.close();

    //log ( this, "status nachdem gespeichert wurde : " + doc.getFirstChild().getAttributes().getNamedItem("status"));
    //log ( this, org.setupx.repository.core.util.File.getFileContent(file));
    //log ( this, file.getAbsolutePath());
  }

  /**
   * @return
   */
  public StreamSource toStream() {
    File _file;

    try {
      _file = File.createTempFile("XXX", "XXX");
      storeDocument(_file);

      return new StreamSource(_file);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
