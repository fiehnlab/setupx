/**
 * ============================================================================ File:    SplitratioSOP.java Package: org.setupx.repository.core.communication.sop cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.sop;

import org.setupx.repository.core.communication.exporting.MachinePool;
import org.setupx.repository.core.communication.leco.GCTOF;
import org.setupx.repository.core.communication.leco.Method;
import org.setupx.repository.server.persistence.PersistenceActionFindException;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class SplitratioSOP extends SOP {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  GCTOF instrument = new GCTOF();

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String[][] getValues() {
    java.util.Iterator machines = null;

    try {
      machines = MachinePool.getAvailableMachines().iterator();
    } catch (PersistenceActionFindException e) {
      return new String[0][0];
    }

    Set methods = new HashSet();

    while (machines.hasNext()) {
      GCTOF machine = (GCTOF) machines.next();
      methods.addAll(machine.getMethods("(GC)"));
    }

    return map(methods);
  }

  /**
   * TODO: 
   *
   * @param set TODO
   *
   * @return TODO
   */
  private static String[][] map(Set set) {
    String[][] result = new String[set.size()][2];

    Iterator iterator = set.iterator();
    int i = 0;

    while (iterator.hasNext()) {
      Method method = (Method) iterator.next();
      result[i] = new String[] { method.getLabel(), null };
      i++;
    }

    return result;
  }
}
