package org.setupx.repository.core.util;

public class SXMailAuthenticator extends javax.mail.Authenticator {
    private javax.mail.PasswordAuthentication authentication;

    public SXMailAuthenticator(String username, String password) {
        authentication = new javax.mail.PasswordAuthentication(username, password);
    }

    protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
        return authentication;
    }
}