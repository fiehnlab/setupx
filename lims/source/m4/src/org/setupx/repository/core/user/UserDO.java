/**
 * ============================================================================ File:    UserDO.java Package: org.setupx.repository.core.user cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.user;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.mail.EMailAddress;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;
import org.setupx.repository.server.persistence.hibernate.SXSerializable;


/**
 * abstract class that is representing a user in the system. 
 * the user can be stored in the local configuration file or can be determined from an external ldapserver - look at the UserFactory
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.9 $
 *
 * @hibernate.class  table = "user"
 * @hibernate.discriminator column = "discriminator"
 */
public abstract class UserDO extends CoreObject implements SXSerializable {
    //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //public EMailAddress mailAddress = new EMailAddress("unknown@perc.de");
    public Hashtable userinformation = new Hashtable();
    public String displayName = "--";
    public String organisation = "--";
    public String password = "--";
    public String phone = "--";
    public String username = "--";
    private long lastExperimentID;

    private boolean labtechician = false;
    private boolean admin = false;

    private String emailString = "";

    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new UserDO object.
     *
     * @param username2 
     * @param password2 
     * @param email 
     */
    public UserDO(String username2, String password2, EMailAddress email) {
        this.username = username2;
        this.setPassword(password2);
        this.setMailAddress(email);
    }

    /**
     * Creates a new UserDO object.
     */
    public UserDO() {
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * TODO: 
     *
     * @return TODO
     */
    public String getDisplay() {
        if (this.organisation.compareTo("") != 0) {
            return this.displayName + " (" + this.organisation + ")";
        }

        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @hibernate.property
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param lastExperimentID TODO
     */
    public void setLastExperimentID(long lastExperimentID) {
        this.lastExperimentID = lastExperimentID;
    }

    /**
     * @hibernate.property
     */
    public long getLastExperimentID() {
        return this.lastExperimentID;
    }

    /**
     * @deprecated
     * @param mailAddress TODO
     */
    public void setMailAddress(EMailAddress mailAddress) {
        this.emailString = mailAddress.getEmailAddress();
        /*
        if (this.mailAddress == null){
            this.mailAddress = new EMailAddress("unknown@unknown.de");
            warning(this, "unable to restore email address.");
        }
        this.mailAddress = mailAddress;
         */
    }

    /**
     * not a hibernate property anymore !!
     * @deprecated
     */
    public EMailAddress getMailAddress() {
        return new EMailAddress(this.getEmailString());
    }

    /**
     * TODO: 
     *
     * @param organisation TODO
     */
    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    /**
     * @hibernate.property
     */
    public String getOrganisation() {
        return organisation;
    }

    /**
     * TODO: 
     *
     * @param password TODO
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @hibernate.property
     */
    public String getPassword() {
        return password;
    }

    /**
     * TODO: 
     *
     * @param phone TODO
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @hibernate.property
     */
    public String getPhone() {
        return phone;
    }

    /**
     * The setter method for this Customer's identifier.
     */
    public void setUOID(long lId) {
        this.uoid = lId;
    }

    /**
     * @hibernate.id column = "uoid" generator-class="native"
     */
    public long getUOID() {
        return this.uoid;
    }

    /**
     * TODO: 
     *
     * @param userinformation TODO
     */
    public void setUserinformation(Hashtable userinformation) {
        this.userinformation = userinformation;
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public Hashtable getUserinformation() {
        return userinformation;
    }

    /**
     * TODO: 
     *
     * @param username TODO
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @hibernate.property
     */
    public String getUsername() {
        return username;
    }

    /**
     * TODO: 
     *
     * @param username2 TODO
     * @param password2 TODO
     *
     * @return TODO
     *
     * @throws PersistenceActionFindException TODO
     */
    public static UserDO load(String username2, String password2)
    throws PersistenceActionFindException {
        UserDO result = null;

        Session session = PersistenceConfiguration.createSessionFactory().openSession();

        List list = session.createCriteria(UserDO.class).add(Expression.eq("password", password2)).add(Expression.eq("username", username2)).list();

        if (list.size() < 1) {
            throw new PersistenceActionFindException("unable to find user " + username2);
        }

        if (list.size() > 1) {
            throw new PersistenceActionFindException("found more than one user " + username2);
        }

        result = (UserDO) list.get(0);

        return result;
    }


    public static UserDO load(String username)
    throws PersistenceActionFindException {
        UserDO result = null;

        Session session = PersistenceConfiguration.createSessionFactory().openSession();

        List list = session.createCriteria(UserDO.class).add(Expression.eq("username", username)).list();

        if (list.size() < 1) {
            throw new PersistenceActionFindException("unable to find user " + username);
        }

        if (list.size() > 1) {
            throw new PersistenceActionFindException("found more than one user " + username);
        }

        result = (UserDO) list.get(0);

        return result;
    }




    /**
     * @return
     */
    public String getAbbreviation() {
        //return this.getUsername().
        return this.getUsername().substring(0, 2).toLowerCase();
    }

    public boolean isLabTechnician() {
        return this.isLabtechician();
    }

    /**
     * @see #isAdmin()
     * @deprecated - user isAdmin instead
     */
    public static final boolean isMasterUser(long userID) {
        try {
            return ((UserDO)UserDO.persistence_loadByID(UserDO.class, CoreObject.createSession() , userID)).isAdmin();
        } catch (Exception e) {
            return false;
        }
    }




    /**
     * @deprecated
     */
    public boolean isSuperUser() {
        return isMasterUser(this.getUOID());
    }

    /*
     * (non-Javadoc)
     * @see org.setupx.repository.core.CoreObject#toString()
     */
    public String toString() {
        return toString("\n");
    }

    public String toStringHTML() {
        return toString("<br>");
    }

    private String toString(String string) {
        StringBuffer buffer = new StringBuffer();
        buffer.append(username + "  ");
        buffer.append("(" + getMailAddress().getEmailAddress() + ") ");
        buffer.append(string);

        Enumeration keys = userinformation.keys();

        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            buffer.append(key + ": " + userinformation.get(key) + "  ");
            buffer.append(string);
        }

        return buffer.toString();
    }

    /**
     * @hibernate.property
     */public boolean isLabtechician() {
         return labtechician;
     }

     /**
      * @hibernate.property
      */
     public boolean isAdmin() {
         return admin;
     }
     /**
      * @hibernate.property
      */
     public String getEmailString() {
         return emailString;
     }

     public void setLabtechician(boolean labtechician) {
         this.labtechician = labtechician;
     }

     public void setAdmin(boolean admin) {
         this.admin = admin;
     }

     public void setEmailString(String emailString) {
         this.emailString = emailString;
     }
     
     
     public boolean isPublic(){
         return (this.getUsername().compareTo("public") == 0);
     }
}
