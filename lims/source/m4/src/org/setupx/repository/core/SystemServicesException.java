package org.setupx.repository.core;

/**
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class SystemServicesException extends Exception {
    /**
     * Creates a new SystemServicesException object.
     */
    public SystemServicesException() {
        super();
    }

    /**
     * Creates a new SystemServicesException object.
     * 
     * @param message
     */
    public SystemServicesException(String message) {
        super(message);
    }

    /**
     * Creates a new SystemServicesException object.
     * 
     * @param cause
     */
    public SystemServicesException(Throwable cause) {
        super(cause);
    }

    /**
     * Creates a new SystemServicesException object.
     * 
     * @param message
     * @param cause
     */
    public SystemServicesException(String message, Throwable cause) {
        super(message, cause);
    }
}
