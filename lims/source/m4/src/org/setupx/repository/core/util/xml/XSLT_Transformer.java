/**
 * ============================================================================ File:    XSLT_Transformer.java Package: org.setupx.repository.core.util.xml cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class XSLT_Transformer {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private StreamSource XSLT_Source = null;

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @return an instance of the XSLT-Transformer
   */
  public static XSLT_Transformer getInstance() {
    XSLT_Transformer _xtrans = new XSLT_Transformer();
    _xtrans.XSLT_Source = getXSLTDefault();

    return _xtrans;
  }

  /**
   * converts an incomming stream stream. the result is written to the outputstream. the xslt style sheet for the transformation can be set using setXSLTSource
   *
   * @param i_inStream the source that will be transformed
   * @param i_ostream the result where the transformed stream will be written to
   *
   * @throws TransformerException 
   */
  public void convert(InputStream i_inStream, OutputStream i_ostream)
    throws TransformerException {
    StreamResult _resultStream = new StreamResult(i_ostream);
    StreamSource _sourceStream = new StreamSource(i_inStream);

    TransformerFactory _factory = TransformerFactory.newInstance();
    Transformer _transformer;

    _transformer = _factory.newTransformer(XSLT_Source);
    _transformer.transform(_sourceStream, _resultStream);
  }

  /**
   * converts a XML file unsing a XSLTstylesheetfile.
   *
   * @param i_xmlFile the file, the data will be read from, to convert
   * @param i_xsltFile the stylesheet used by the transformer
   * @param i_result the file where the result will be written to
   *
   * @throws TransformerException Transformerexception TODO
   * @throws IOException TODO
   */
  public static void convert(File i_xmlFile, File i_xsltFile, File i_result)
    throws TransformerException, IOException {
    FileInputStream _fileInputStream = new FileInputStream(i_xmlFile);
    InputStream _istream = _fileInputStream;
    FileOutputStream _ostream = new FileOutputStream(i_xmlFile);
    XSLT_Transformer _xtrans = XSLT_Transformer.getInstance();
    _xtrans.setXSLTSource(i_xsltFile);
    _xtrans.convert(_istream, _ostream);

    _ostream.close();
  }

  /**
   * TODO: 
   *
   * @param i_xmlFile TODO
   * @param i_xsltFile TODO
   * @param io_stream TODO
   *
   * @throws FileNotFoundException TODO
   * @throws TransformerException TODO
   */
  public static void convert(File i_xmlFile, File i_xsltFile, OutputStream io_stream)
    throws FileNotFoundException, TransformerException {
    FileInputStream _fileInputStream = new FileInputStream(i_xmlFile);
    InputStream _istream = _fileInputStream;
    FileOutputStream _ostream = new FileOutputStream(i_xmlFile);
    XSLT_Transformer _xtrans = XSLT_Transformer.getInstance();
    _xtrans.setXSLTSource(i_xsltFile);
    _xtrans.convert(_istream, _ostream);
  }

  /**
   * Sets the XSLT-Stylesheet
   *
   * @param i_xslt the Stylesheet as a String, that will be set as the XSLT-Stylesheet
   */
  public void setXSLTSource(String i_xslt) {
    this.XSLT_Source = new StreamSource(i_xslt);
  }

  /**
   * Sets the XSLT-Stylesheet
   *
   * @param i_xsltSource the File that will be set as the XSLT-Stylesheet
   *
   * @throws FileNotFoundException if the file can not be found
   */
  public void setXSLTSource(File i_xsltSource) throws FileNotFoundException {
    InputStream _istream = new FileInputStream(i_xsltSource);
    this.XSLT_Source = new StreamSource(_istream);
  }

  /**
   * testing TODO
   *
   * @param args 
   *
   * @throws IOException 
   * @throws TransformerException 
   */
  public static void main(String[] args) throws Exception {
    String SYNTAX = XSLT_Transformer.class.getName() + " (file to convert)  (resultfile) (stylesheet) ";

    try {
      String _inFileName = args[0];
      String _outFile = args[1];
      String _syleSheet = args[2];

      if (args.length != 3) {
        throw new Exception(" wring number of args" + SYNTAX);
      }

      java.io.File _inFile = new File(_inFileName);
      FileInputStream _fileInputStream = new FileInputStream(_inFile);
      InputStream _istream = _fileInputStream;
      FileOutputStream _ostream = new FileOutputStream(_outFile);

      // !!!!
      XSLT_Transformer x = XSLT_Transformer.getInstance();
      x.setXSLTSource(new File(_syleSheet));
      x.convert(_istream, _ostream);

      // !!!!
      _ostream.close();
    } catch (java.lang.ArrayIndexOutOfBoundsException arrayEx) {
      throw new Exception("unable to convert: \n use: " + SYNTAX + "\n\n", arrayEx);
    } catch (Exception ex) {
      throw new Exception("unable to convert:  ", ex);
    }
  }

  /**
   * @return a default XSLT Stylesheet for testing
   */
  private static StreamSource getXSLTDefault() {
    String _XSLT_String = ("" + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>" + "<xsl:template match='/'>" + "<xsl:copy-of select='.' />" + "</xsl:template>" + "</xsl:stylesheet>");

    return new StreamSource(new java.io.StringReader(_XSLT_String));
  }
}
