/**
 * ============================================================================ File:    AcquisitionFile.java Package: org.setupx.repository.core.communication.exporting cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.exporting;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import org.setupx.repository.Config;
import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample;


/**
 * A file, that can be stored and loaded into the leco-software.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class AcquisitionFile extends ExportFile {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  String content = "";
  String label;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String VALUE_SEPERATOR = "\u0009";
  public static final String LINE_SEPERATOR = "\n";
  public static final String EXTENSION = "seq";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new AcquisitionFile object.
   *
   * @param acquisition 
   * @param machine 
   *
   * @throws UnsupportedMachine 
   */
  protected AcquisitionFile(Acquisition acquisition, Machine machine)
    throws UnsupportedMachine {
    // create an ID
    label = machine.getMachineLetter() + "_mach__" + acquisition.samples.length + "_samples__operator_" + acquisition.getJobInformation().getOperator() + "_" + new Date().getTime();

    // seperator 
    // find the correct order of the attributes
    ACQMachineConfigurationIterator iterator = ACQMachineConfigurationIterator.iterator(machine);
    log(this, "machineID: " + machine.getUOID());

    // very first line - ID for filetype
    content = content.concat("Acquisition Sequence Table Version 2");
    content = content.concat(LINE_SEPERATOR);

    // first line - add a line with the labels
    while (iterator.hasNext()) {
      content = content.concat((String) iterator.next());

      if (iterator.hasNext()) {
        content = content.concat(VALUE_SEPERATOR);
      } else {
        content = content.concat(LINE_SEPERATOR);
      }
    }

    // each of samples
    for (int i = 0; i < acquisition.samples.length; i++) {
      // open each sample and read all the labels defined in the iterator 
      AcquisitionSample sample = acquisition.samples[i];
      iterator.reset();

      String label;
      String value;

      //Logger.err(this, "adding sample to content - sampleNo: " + i + " num of AcquisitionSamples: " + acquisition.samples.length + "  iterator sais: " + iterator.hasNext());
      while (iterator.hasNext()) {
        label = (String) iterator.next();
        value = "";

        AcquisitionParameter parameter = null;

        try {
          parameter = sample.getParamter(label);
          value = parameter.getValue();
        } catch (ParameterException e) {
          // if there is nothing for this label 
          value = "-NA-";
        }

        content = content.concat(value);

        // if the sample is finish add a line seperator else a valueseperator
        if (iterator.hasNext()) {
          content = content.concat(VALUE_SEPERATOR);
        } else {
          content = content.concat(LINE_SEPERATOR);
        }

        //log(this, "label: " + label + " value: " + value + "\n    content: " + content);
      }
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * returns a String representation of the content
   *
   * @return a String representation of the content
   */
  public String getContent() {
    return content;
  }

  /**
   * TODO: 
   *
   * @throws FileNotFoundException TODO
   * @throws IOException TODO
   */
  public void store() throws FileNotFoundException, IOException {
    this.saveTo(Config.DIRECTORY_EXPORT + java.io.File.separator + this.label + ".seq");
  }
}
