package org.setupx.repository.core;

import junit.framework.TestCase;

import org.setupx.repository.server.persistence.PersistenceActionStoreException;
import org.setupx.repository.web.forms.inputfield.StringInputfield;

public class CoreObjectTest extends TestCase {
    
    CoreObject coreObject = null;
    
    protected void setUp() throws Exception {
        CoreObject coreObject = new StringInputfield("","","",false);
        this.coreObject = coreObject;
    }

    public CoreObjectTest(String name) {
        super(name);
    }

    public void testRemoveSession() throws PersistenceActionStoreException, PersistenceActionRemoveException {
        coreObject.save();
        coreObject.remove(CoreObject.createSession());
    }

    public void testSave() throws PersistenceActionStoreException {
        coreObject.save();
    }

    public void testSetIDInt() {
        try {
            coreObject.setID(23);
            throw new AssertionError();
        } catch (UnsupportedOperationException e) {
        }
    }

    public void testSetIDLong() {
        try {
            coreObject.setId(23);
            throw new AssertionError();
        } catch (UnsupportedOperationException e) {
        }
    }

    public void testSetId() {
        try {
            coreObject.setId(23);
            throw new AssertionError();
        } catch (UnsupportedOperationException e) {
        }
    }

    public void testCreateSession() {
        assertTrue(coreObject.createSession() != null);
    }

    public void testCheckString() {
        try {
            coreObject.checkString("null");
            throw new AssertionError();
        } catch (Exception e) {
        }

        try {
            coreObject.checkString(null);
            throw new AssertionError();
        } catch (Exception e) {
        }

        try {
            coreObject.checkString("glas");
        } catch (Exception e) {
            throw new AssertionError();
        }
}

    public void testSetCaching() {
        coreObject.setCaching(false);
        assertTrue(!coreObject.isCaching());

        coreObject.setCaching(true);
        assertTrue(coreObject.isCaching());
    }
}
