/**
 * ============================================================================ File:    Login.java Package: org.setupx.repository.core.communication.kerberos cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.kerberos;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.CommunicationException;
import org.setupx.repository.core.user.UserNotFoundException;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;


/**
 * Login on the kerberos server.
 * 
 * <p>
 * the two different constructors offer a check by using a  swingGUI to type in the password or the other way is that both, the password and the username are stored in the constructor - so that there will be no further questions for the password - for example for use in serverconfiguration.
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
class Login extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static Subject mySubject;
  private static Object thiz = new Login();

  static {
    debug(thiz, "init Login");
    System.setProperty("java.security.auth.login.config", Config.FILE_LOGIN);
    System.setProperty("java.security.krb5.kdc", Config.KERBEROS_KDC);
    System.setProperty("java.security.krb5.realm", Config.KERBEROS_REALM);
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Login by using a Swing interface. The password is not set so the GUI will pop up and ask for it.
   *
   * @param username the username that will be used for the login
   *
   * @throws UnvalidUserException
   * @throws CommunicationException
   * @throws UserNotFoundException userid is unknown
   */
  public Login(String username) throws UnvalidUserException, CommunicationException {
    this.validateSwingDialog(username);
  }

  /**
   * Checks if a user exists on the kerberos server
   *
   * @param username the username that will be used for the login
   * @param password password
   *
   * @throws UnvalidUserException
   * @throws CommunicationException
   * @throws UserNotFoundException 
   */
  public Login(String username, String password) throws CommunicationException {
    //debug("logging on " + username);
    this.validateNonInterface(username, password);
  }

  private Login() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * checks a user.
   *
   * @param username the username that will be used for the login
   * @param password the password for the user
   *
   * @throws UnvalidUserException
   * @throws CommunicationException
   * @throws UserNotFoundException user was not found
   */
  protected void validateNonInterface(String username, String password)
    throws CommunicationException {
    validate(new PlainCallbackHandler(username, password));
  }

  /**
   * checks a user by opening a swingdialog and asking for the password FIXME there is a problem that the swing window does not destroy itself
   *
   * @param username the username that will be used for the login
   *
   * @throws UnvalidUserException
   * @throws CommunicationException
   * @throws UserNotFoundException user was not found
   *
   * @see SwingDialogCallbackHandler#handle(Callback[])
   */
  protected void validateSwingDialog(String username) throws UnvalidUserException, CommunicationException {
    validate(new SwingDialogCallbackHandler(username));
  }

  /**
   * check a user using different handlers.
   *
   * @param handler the handler used to validate the information
   *
   * @throws CommunicationException
   * @throws UserNotFoundException user was not found
   *
   * @see CallbackHandler
   */
  private void validate(CallbackHandler handler) throws CommunicationException {
    LoginContext lc = null;

    try {
      lc = new LoginContext("JaasSample", handler);
    } catch (LoginException le) {
      throw new CommunicationException(le);
    } catch (SecurityException se) {
      throw new CommunicationException(se);
    }

    debug("created login context.");

    try {
      // attempt authentication
      lc.login();
      debug("logged in.");
      lc.logout();
      handler = null;
      lc = null;
    } catch (LoginException le) {
      CommunicationException ex = new CommunicationException("unable to connect to authentification server", le);
      throw ex;
    }
  }
}
