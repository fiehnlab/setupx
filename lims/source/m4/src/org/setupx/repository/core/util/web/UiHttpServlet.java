/**
 * ============================================================================ File:    UiHttpServlet.java Package: org.setupx.repository.core.util.web cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.web;

import org.setupx.repository.core.ServiceMediator;
import org.setupx.repository.core.communication.mail.MailController;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.web.WebConstants;

import java.io.IOException;

import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * abstract class used as superclass of all servlets in this system. it adds methods  for simple forwarding and others - <B>use it</B>.
 *
 * @author <a href="mailto:scholz@zeypher.com">Martin Scholz</a>
 */
public abstract class UiHttpServlet extends HttpServlet {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public String pageToForwardTo = "";

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static ServiceMediator serviceMediator;

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * method that is processed when the instance of this servlet is called
   *
   * @param request the request
   * @param response the resonse
   *
   * @throws Throwable exceptions while processing
   */
  public abstract void process(HttpServletRequest request, HttpServletResponse response)
    throws Throwable;

  /**
   * received a parameter from the the <B>request</B>
   *
   * @param request the request
   * @param name the name of the parameter
   *
   * @return a value attached to that Parameter
   */
  public static String getRequestParameter(HttpServletRequest request, String name) {
    String result = request.getParameter(name);
    

    return result;
  }

  /**
   * @see UiHttpServlet#getRequestParameter(HttpServletRequest, String)
   */
  public static int getRequestParameterInt(HttpServletRequest request, String name) {
    int result = 9999;

    try {
      result = Integer.parseInt(getRequestParameter(request, name));
    } catch (NumberFormatException e) {
      Logger.warning(null, "  error parsing the value for " + name);
    }

    return result;
  }

  /**
   * @see Logger#debug(Object, String)
   */
  public final void debug(String string) {
    Logger.debug(this, string);
  }

  /**
   * @param request
   * @param queryservlet_experiment_id
   *
   * @return
   */
  public static long getRequestParameterLong(HttpServletRequest request, String name) {
    return new Long(getRequestParameterInt(request, name)).longValue();
  }

  /*
   *  (non-Javadoc)
   * @see javax.servlet.Servlet#destroy()
   */
  public void destroy() {
    Logger.warning(this, "destroying " + this.getClass().getName());
  }

  /*
   *  (non-Javadoc)
   * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  public void doGet(HttpServletRequest _request, HttpServletResponse i_response)
    throws ServletException, IOException {
    Logger.debug(this, ".doGet() start");

    printRequestParameters(_request);
    printSessionAttributes(_request);

    try {
      Logger.debug(this, ".process() start");
      process(_request, i_response);
      Logger.debug(this, ".process() end");
    } catch (Throwable ex) {
      ex.printStackTrace();

      String message = ex.getMessage() + "  (" + ex.getLocalizedMessage() + ")";
      _request.getSession().putValue(WebConstants.SESS_FORM_MESSAGE, message);

      try {
        forward("/error.jsp", _request, i_response);
      } catch (WebForwardException e) {
        throw new ServletException("unable to forward", e);
      }
    }
  }

  /*
   *  (non-Javadoc)
   * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  public void doPost(HttpServletRequest _request, HttpServletResponse i_response)
    throws ServletException, IOException {
    Logger.debug(this, "forwarding to do get");
    doGet(_request, i_response);
  }

  /**
   * forwarding to a specific address
   *
   * @param i_addressToGoTo the address
   * @param request the request
   * @param response the response
   *
   * @throws WebForwardException
   * @throws Exception unablte to create a dispatcher
   */
  public void forward(String i_addressToGoTo, HttpServletRequest request, HttpServletResponse response)
    throws WebForwardException {
    RequestDispatcher dispatcher;
    dispatcher = getServletContext().getRequestDispatcher(i_addressToGoTo);

    if (dispatcher == null) {
      throw new WebForwardException("unable to create Dispatcher for forwarding");
    }

    Logger.debug(this, "forwarding to: " + i_addressToGoTo);

    try {
      dispatcher.forward(request, response);
    } catch (Exception e) {
      Logger.debug(this, "unable to forward to " + i_addressToGoTo + ":  " + e);
      throw new WebForwardException(e);
    }
  }

  /*
   *  (non-Javadoc)
   * @see javax.servlet.GenericServlet#init()
   */
  public void init() throws ServletException {
    Logger.log(this, "init " + this.getClass().getName());
  }

  /**
   * forwarding to errorpage
   */

  /*public void forwardToErrorPage(Throwable i_exc, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
     HttpSession _session = request.getSession();
     String _errorMessage = "<B><font color='red'>" + i_exc.getMessage() + "</b><br>Grund:<BR> " + i_exc.toString() +  "</font><br>";
                  i_exc.printStackTrace();
                  Throwable _temp_exc = i_exc;
                  while (_temp_exc.getCause() != null){
                    _temp_exc = _temp_exc.getCause();
                    _errorMessage += "\n<br>" + _temp_exc.getMessage();
                  }
                  _session.setAttribute(UiConstants.SESSION_ERROR,_errorMessage);
                  logERR(" Leite an Errorpage weiter:   " + _errorMessage);
                  //Exception selbst an Session haengen
                  _session.setAttribute(UiConstants.SESSION_EXCEPTION,i_exc);
                  forwardToPage(UiConstants.PAGE_DEFAULT_ERROR,request,response);
                }
   */
  protected void finalize() throws Throwable {
    MailController.sendNonsendMails();
    super.finalize();
  }

  /**
   * print all attributes from the request
   *
   * @param request the request
   */
  protected void printRequestParameters(HttpServletRequest request) {
    if (true) {
      Enumeration enu = request.getParameterNames();
      Logger.debug(this, "--> request-parameter:");

      while (enu.hasMoreElements()) {
        String tempName = enu.nextElement().toString();
        String tempValue = request.getParameter(tempName).toString();
        Logger.debug(this, "   " + tempName + "\t\t:    " + tempValue);
      }

      Logger.debug(this, "<-- request-parameter:");
    }
  }

  /**
   * print all attributes in the session
   *
   * @param request the request
   */
  protected void printSessionAttributes(HttpServletRequest request) {
    if (true) {
      HttpSession session = request.getSession();
      Logger.debug(this, "--> session-parameter:");

      Enumeration enum_app = session.getAttributeNames();

      while (enum_app.hasMoreElements()) {
        String attribute = enum_app.nextElement().toString();
        Logger.debug(this, "   " + attribute + "\t\t:    " + session.getAttribute(attribute));
      }

      Logger.debug(this, "<--  session-parameter");
    }
  }
}
