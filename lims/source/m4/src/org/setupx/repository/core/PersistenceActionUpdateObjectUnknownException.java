/*
 * Created on 18.11.2005 TODO To change the template for this generated file go to Window - Preferences - Java - Code
 * Style - Code Templates
 */
package org.setupx.repository.core;

import org.setupx.repository.server.persistence.PersistenceActionException;

/**
 * @author scholz TODO To change the template for this generated type comment go to Window - Preferences - Java - Code
 *         Style - Code Templates
 */
public class PersistenceActionUpdateObjectUnknownException extends PersistenceActionException {

    /**
     * @param e
     */
    public PersistenceActionUpdateObjectUnknownException(Throwable e) {
        super(e);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param string
     * @param e
     */
    public PersistenceActionUpdateObjectUnknownException(String string, Throwable e) {
        super(string, e);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param string
     */
    public PersistenceActionUpdateObjectUnknownException(String string) {
        super(string);
        // TODO Auto-generated constructor stub
    }

}
