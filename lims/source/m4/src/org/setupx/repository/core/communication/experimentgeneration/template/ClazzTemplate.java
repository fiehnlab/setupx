/**
 * ============================================================================ File:    ClazzTemplate.java Package: org.setupx.repository.core.communication.experimentgeneration.template cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.experimentgeneration.template;

/**
 * template containing all information relevant for a class and the multifiled4clazzes.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @see org.setupx.repository.web.forms.inputfield.multi.Clazz
 * @see org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz
 */
public interface ClazzTemplate extends Template {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  //public static final String SEX = null;
  //public static final String DOB = null;
  public static final String LINE = "/sample/trayname_generic/line";
  public static final String DEV_STAGE = "/sample/trayname_generic/developmental_stage";
  public static final String BACKGOUND = "/sample/trayname_generic/linebackground_line";
  public static final String SPECIES = "/sample/trayname_generic/Species";
  public static final String ORGAN = "/sample/trayname_generic/organ";
  public static final String ORGAN_DETAIL = "/sample/trayname_generic/organ_specification";
  public static final String FUNCTION = "/sample/trayname_generic/functional_description";
  public static final String OPERATOR_COMMENT = "/sample/trayname_generic/Operator_comments";

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * pathes where the values are located at.
   *
   * @return an array containing all pathes.
   */
  public String[] getPathes();

  /**
   * TODO: 
   *
   * @param sampleTemplates TODO
   */
  public void setSamples(SampleTemplate[] sampleTemplates);

  /**
   * templates containing relevent information for the sample
   *
   * @return templates containing relevent information for the sample
   */
  public SampleTemplate[] getSamples();

  /**
   * value for a specific path
   *
   * @param path the path
   *
   * @return the value
   */
  public String getValue(String path);
}
