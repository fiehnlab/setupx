/**
 * ============================================================================ File:    LocalStaticLibrary.java Package: org.setupx.repository.core.communication.vocabulary cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.vocabulary;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.StringTokenizer;

import org.setupx.repository.Config;
import org.setupx.repository.core.util.logging.Logger;


/**
 * library containing all words contained in a directory
 * 
 * <p>
 * the libraries that will be loaded must all be stored in a directory
 * </p>
 * 
 * <p>
 * <B>TODO add a URL-loader to load the files remote - that they can change</B>
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.12 $
 *
 * @see org.setupx.repository.Config#DIRECTORY_LIBRARY
 */
public class LocalStaticLibrary extends AbtractLibrary {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public File directory = new File(Config.DIRECTORY_LIBRARY);
  private HashSet libraryHashSet;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new LocalStaticLibrary object.
   */
  public LocalStaticLibrary() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @return new instance
   */
  public static Library newInstance() {
    LocalStaticLibrary library = new LocalStaticLibrary();
    library.fillLibrary();

    return library;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.communication.vocabulary.AbtractLibrary#receiveValues()
   */
  public HashSet receiveValues() {
    return this.libraryHashSet;
  }

  /**
   * fills the hashtable whith all words
   */
  private void fillLibrary() {
    debug(this, "init");
    this.libraryHashSet = new HashSet();

    //load each file
    File[] files = directory.listFiles();

    if (files == null) {
      Logger.warning(this, "no libraries found");

      return;
    }

    debug(this, "found " + files.length + " text libraries at " + directory.getAbsolutePath());

    for (int i = 0; i < files.length; i++) {
      File file = files[i];
      debug(this, "adding wordlibrary " + file);

      String filecontent = "";

      try {
        filecontent = org.setupx.repository.core.util.File.getFileContent(file);
      } catch (IOException e) {
        Logger.warning(this, "unable to load dictionary " + file);
      }

      StringTokenizer tokenizer = new StringTokenizer(filecontent, "\n");

      int z = 0;

      while (tokenizer.hasMoreElements()) {
        z++;

        String word = (String) tokenizer.nextElement();

        //add each word to the library
        //log(this,"adding: " + word);
        this.libraryHashSet.add(word);
      }

      debug(this, "\t " + z + " words: " + file.getName());
    }

    log(this, "words for dict-check: " + this.libraryHashSet.size());
  }
}
