package org.setupx.repository.core.communication.technology.scanner;


import java.util.Hashtable;

import org.setupx.repository.core.CoreObject;


public class ScanningProcessMonitor extends CoreObject{

  static Object thiz = new ScanningProcessMonitor();
  static Hashtable processes = new Hashtable();

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  public static ScanningProcess getScanningProcess(final int id) {
    try {
      return (ScanningProcess) processes.get("" + id);
    } catch (Exception e) {
      warning(thiz, "unable to find session: " + id);

      return null;
    }
  }


  public static Hashtable getSessions() {
    return processes;
  }

  public static void add(ScanningProcess scanningProcess) {
    log(thiz, "adding process " + scanningProcess.getName());
    processes.put("" + scanningProcess.getName(), scanningProcess);
  }

}
