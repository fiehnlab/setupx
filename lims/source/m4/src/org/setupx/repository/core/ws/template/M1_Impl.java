/**
 * ============================================================================ File:    M1_Impl.java Package: org.setupx.repository.core.ws.template cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.ws.template;

/**
 * Implementation of the WebService-Interface. The methods do not have to return any real values. This class is just beeing used by the JAVA2WSDL generator.
 *
 * @author <a href="mailto:scholz@zeypher.com?subject=m1" >&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class M1_Impl implements M1 {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param experimentID TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String[] getClassIDsByExperiment(String experimentID)
    throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param classID TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String getClassInfos(String classID) throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param sampleID TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String getClazzIDbySampleID(String sampleID) throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param clazzID TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String getExperimentIDbyClazz(String clazzID)
    throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param sampleID TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String getExperimentIDbySample(String sampleID)
    throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String getExperimentsSheduled() throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param acquistitionName TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String getSampleIDbyACQLabel(String acquistitionName)
    throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param clazzID TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String[] getSamplesByClassID(String clazzID) throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param userID TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String[] getSamplesForUser(long userID) throws WebServiceException {
    return null;
  }

  /**
   * TODO: 
   *
   * @param serviceCode TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String activateService(String serviceCode) throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param sampleID TODO
   * @param message TODO
   *
   * @throws WebServiceException TODO
   */
  public void addEvent(String sampleID, String message)
    throws WebServiceException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param username TODO
   * @param password TODO
   * @param authorizationCode TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public long authenticateUser(String username, String password, String authorizationCode)
    throws WebServiceException {
    return 0;
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String[][] availableColumns() throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param label TODO
   * @param message TODO
   *
   * @throws WebServiceException TODO
   */
  public void createMessage(String label, String message)
    throws WebServiceException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param acqname TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String determineColumnIDbyACQname(String acqname)
    throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param speciesname TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String determineSpeciesType(String speciesname)
    throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param speciesname TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String determineSpeciesTypeName(String speciesname)
    throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * TODO: 
   *
   * @param experimentID TODO
   *
   * @throws WebServiceException TODO
   */
  public void exportByExperimentID(String experimentID)
    throws WebServiceException {
    // TODO Auto-generated method stub
  }

  /**
   * TODO: 
   *
   * @param clazzSetID TODO
   * @param resultfile_url TODO
   *
   * @throws WebServiceException TODO
   */
  public void uploadResultFile(String clazzSetID, String resultfile_url)
    throws WebServiceException {
    // TODO Auto-generated method stub
  }

public String determineMetaDataByClass(String classID, String key) throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
}

public String determineMetaDataByExperiment(String experimentID, String key) throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
}

public String determineMetaDataBySample(String sampleID, String key) throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
}

public String determineMetaDataBySampleMax(String sampleID, String key) throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
}

public String determineNCBIBySampleID(String sampleID) throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
}

public String determineSpeciesNameByNCBI(String ncbiID) throws WebServiceException {
    // TODO Auto-generated method stub
    return null;
}

public boolean isMasterUser(String username, String key) throws WebServiceException {
    // TODO Auto-generated method stub
    return false;
}

public boolean isUser(String username, String key) throws WebServiceException {
    return false;
}

public String[] getExperimentsByUser(String username, String password)
        throws WebServiceException {
    return null;

}

public String getSampleTimestamp(String filename) throws WebServiceException {
return "";
}
}