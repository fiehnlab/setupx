/**
 * ============================================================================ File:    MailCreator.java Package: org.setupx.repository.core.communication.mail cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.mail;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.ServiceMediator;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.web.forms.inputfield.multi.Promt;


class MailCreator extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static Object thiz = new MailCreator();
  private static MailContent mail = null;
  private static String subject = "";
  private static String msg = "";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private MailCreator() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  static MailContent create(UserDO userDO, Promt promt, int notifyID) {
    mail = null;
    debug(thiz, "creating mail for " + userDO.getMailAddress().toString());

    switch (notifyID) {
    case ServiceMediator.ACTION_STORE:
      subject = "NEW experiment submitted.";
      msg = "" + userDO.getDisplayName() + ", \n\n" + "Thank you very much for submitting your experiment. \n" + "We stored your experiment under the id [" + promt.getUOID() + "] in our system.\n" + "Please keep this ID for further communication with us. \n\n" +
        "In case you want to make any modifications to your experiment or simply look up the status of your samples \n" + "you can log back into " + Config.SYSTEM_NAME + " by using the following ids:\n\n" + "Username: " + userDO.getUsername() + "\n" + "Password: " + userDO.getPassword() + "\n\n" +
        "We will get in contact with you soon, to discuss details about the experiment.\n" + "Thank you, \n\n" + " The Lab-Team";

      break;

    /* CAREFULL with this - dont want to send to much
       case ServiceMediator.ACTION_UPDATE:
           subject = "your experiment has been updated.";
               msg = "" + userDO.getDisplayName() + ", \n\n" +
               "Thank you very much for submitting your experiment. \n"+
               "We stored your experiment under the id [" + promt.getUOID() + "] in our system.\n"+
               "Please keep this ID for further communication with us. \n\n" +
               "In case you want to make any modifications to your experiment or simply look up the status of your samples \n" +
               "you can log back into " + Config.SYSTEM_NAME + " by using the following ids:\n\n" +
               "Username: " + userDO.getUsername() + "\n" +
               "Password: " + userDO.getPassword() + "\n\n" +
               "We will get in contact with you soon, to discuss details about the experiment.\n" +
               "Thank you, \n\n"+
               " The Lab-Team";
           break;
     */
    default:
      warning(thiz, "unknown mail notify ID ");
      subject = null;
      msg = null;

      break;
    }

    if ((subject != null) && (msg != null)) {
      mail = new MailContent(userDO.getMailAddress(), subject, msg);
      //Event event = new MailSendEvent(mail, promt);
    }

    return mail;
  }
}
