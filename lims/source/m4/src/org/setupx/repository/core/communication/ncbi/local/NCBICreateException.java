/*
 * Created on 15.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.setupx.repository.core.communication.ncbi.local;

import org.setupx.repository.core.communication.ncbi.NCBIFindException;

/**
 * @author scholz
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class NCBICreateException extends Exception {

    /**
     * @param string
     */
    public NCBICreateException(String string) {
        super (string);
    }

    /**
     * @param e
     */
    public NCBICreateException(NCBIFindException e) {
super(e);    }

}
