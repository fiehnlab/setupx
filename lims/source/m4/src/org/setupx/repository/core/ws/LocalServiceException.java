package org.setupx.repository.core.ws;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class LocalServiceException extends Exception {
  /**
   * Creates a new LocalServiceException object.
   */
  public LocalServiceException() {
    super();
  }

  /**
   * Creates a new LocalServiceException object.
   *
   * @param arg0 
   */
  public LocalServiceException(String arg0) {
    super(arg0);
  }

  /**
   * Creates a new LocalServiceException object.
   *
   * @param arg0 
   */
  public LocalServiceException(Throwable arg0) {
    super(arg0);
  }

  /**
   * Creates a new LocalServiceException object.
   *
   * @param arg0 
   * @param arg1 
   */
  public LocalServiceException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }
}
