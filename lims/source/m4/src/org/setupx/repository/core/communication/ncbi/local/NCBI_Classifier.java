/**
 * ============================================================================ File: NCBI_Classifier.java Package:
 * org.setupx.repository.core.communication.ncbi.local cvs: $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21
 * scholz Exp $ ============================================================================ Martin Scholz Copyright (C)
 * 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/
 * ============================================================================
 */
package org.setupx.repository.core.communication.ncbi.local;

import java.util.List;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.web.forms.restriction.NCBIVocRestriction;

/**
 * determine if the sample is an animal, human or plant by checking the ncbi - code
 */
public class NCBI_Classifier extends CoreObject {
    // ~ Static fields/initializers
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    static Object thiz = new NCBI_Classifier();

    /** higher vertebrates (reptiles, birds and mammals) possessing an amnion during development */
    final static int ANIMAL_INDICATOR = 32524;

    /**
     * Embryophyta<br>
     * http://tolweb.org/tree?group=Embryophytes
     */
    final static int PLANT_INDICATOR = 3193;

    /** Human<br> */
    final static int HUMAN_INDICATOR = 9606;
    public static final String UNKNOWN_SPECIES = "-unknown-";

    /** ncbi code for a mouse */
    public static final String[] UNKNOWN = new String[] { "Mus sp.", "Mus musculus", UNKNOWN_SPECIES };
    private static NCBIEntry entry;

    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private NCBI_Classifier() {
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @return
     */
    public static int classify(NCBIEntry i_entry) {
        debug(thiz, "classify " + i_entry.getName() + " taxid: " + i_entry.getTaxID());
        entry = i_entry;

        if ((entry == null) || !i_entry.isSpecies()
                || (i_entry.getTaxIdInt() == NCBIVocRestriction.UNKNOWN_NCBI_ORGANISM_ID)) {
            return NCBIEntry.UNKNOWN;
        }

        // loop as long as there is not the root - and try to find one of the indicators
        int id = NCBIEntry.UNKNOWN;

        try {
            do {
                // check if there are classifications in the database
                try {
                    id = NCBIClassifierID.getKingdomByNCBI(entry.getTaxIdInt());        
                } catch (Exception e) {
                    Logger.log(null, "" + e);
                }

                debug(thiz, "taxid: " + entry.getTaxIdInt() + "  classification: " + id + "     entry.getTaxIdInt()"
                        + entry.getTaxIdInt());

                // take the parent and check if it is one of the indicators
                entry = entry.getParent();

            } while ((id == NCBIEntry.UNKNOWN) && (entry.getTaxIdInt() != NCBIVocRestriction.ROOT));
        } catch (Exception e) {
            err(null, e);
            id = NCBIEntry.UNKNOWN;
        }

        return id;
    }

    /**
     * returns an array of NCBI ids that act as classifiers for the defined kingdom
     */
    public static final List getClassifierID(final int kingdomID) {
        return NCBIClassifierID.getNCBIIDsPerKingdom(kingdomID);
    }
}
