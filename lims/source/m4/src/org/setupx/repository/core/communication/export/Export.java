/**
 * ============================================================================ File:    Export.java Package: org.setupx.repository.core.communication.export cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.export;

import java.util.Arrays;
import java.util.HashSet;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.exporting.ExportException;
import org.setupx.repository.core.communication.msi.MSIAttribute;
import org.setupx.repository.core.communication.msi.MSIQuery;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.FormObjectComparator;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.Sample;
import org.setupx.repository.web.pub.data.PubAttribute;

public class Export extends CoreObject {
    //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    protected HashSet exportParts = new HashSet();
    private HashSet exportSamples = new HashSet();
    protected Promt experiment;
    private Clazz[] clazzes;

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * TODO: 
     *
     * @return TODO
     */
    public final HashSet getExportParts() {
        return exportParts;
    }



    /**
     * TODO: 
     *
     * @return TODO
     */
    public final HashSet getExportSamples() {
        return exportSamples;
    }

    /**
     * export an experiment
     *
     * @param promt the experiment to export
     */
    public final void export(Promt promt) {
        debug("exporting promt" + promt.getUOID());
        this.experiment = promt;

        // take every sample
        Clazz[] clazzes = promt.getClazzes();
        this.clazzes = clazzes;

        for (int i = 0; i < clazzes.length; i++) {
            Clazz clazz = clazzes[i];
            this.export(clazz);
        }
    }

    /**
     * export a clazz
     *
     * @param clazz the clazz to export
     */
    public final void export(Clazz clazz) {
        debug("exporting clazz " + clazz.getUOID());

        FormObject[] samples = clazz.getFields();

        Arrays.sort(samples, FormObjectComparator.byID());

        for (int i = 0; i < samples.length; i++) {
            Sample sample = (Sample) samples[i];
            this.export(sample);
        }
    }

    /**
     * export a sample
     *
     * @param sample the sample to be exported
     */
    public final void export(Sample sample) {
        debug("exporting sample " + sample.getUOID());

        ExportSample exportSample = new ExportSample(sample);
        this.exportSamples.add(exportSample);
    }

    /**
     * TODO: 
     */
    protected final void gatherExportInformation() {
        debug("gatherExportInformation()");
        
        // gathering information for header
        this.add(createExperimentParts());

        // gathering general information about experiment
        this.add(createClazzesParts());

        // samples
        this.add(createSampleParts());

        try {
            // msi attributes
            this.add(createMSIAttributeParts());
        } catch (PersistenceActionFindException e){
            e.printStackTrace();
        }

        /*
        // all msi attributes
        try {
            this.add(createMSIDescr());
        } catch (PersistenceActionFindException e){
            e.printStackTrace();
        }
        */
        

        // bbdata
        try {
            this.add(createPubAttribute());
        } catch (PersistenceActionFindException e){
            e.printStackTrace();
        }
        
        debug("finished: gatherExportInformation()");
    }


    private ExportPart createPubAttribute() throws PersistenceActionFindException {
        Logger.log(this, "creating export part createPubAttribute");
        long id = this.experiment.getUOID();
        return new ExportPart(PubAttribute.class, id);
    }


    /*
    private ExportPart createMSIDescr() throws PersistenceActionFindException {
        Logger.log(this, "creating definition of all the MSI Attributes");
        
        // TODO

        return new ExportPart(MSIAttribute.class);
    }
    */


    private ExportPart createMSIAttributeParts() throws PersistenceActionFindException {
        Logger.log(this, "creating export part createMSIAttributeParts");
        // find msi Attributes
        long id = this.experiment.getUOID();
        return new ExportPart(MSIAttribute.class, id);
    }



    private void add(ExportPart part) {
        if (part != null) {
            this.exportParts.add(part);
        }
    }

    private ExportPart createClazzesParts() {
        Logger.log(this, "creating export part createClazzesParts");
        if (this.clazzes != null) {
            return new ExportPart(this.clazzes);
        }

        return null;
    }

    private ExportPart createExperimentParts() {
        Logger.log(this, "creating export part createExperimentParts");
        if (this.experiment != null) {
            return new ExportPart(this.experiment);
        }

        return null;
    }


    private ExportPart createSampleParts() {
        Logger.log(this, "creating export part createSampleParts");
        return new ExportPart(this.exportSamples);
    }


    /**
     * loads a formobject and creates an export based on the type.
     * @param id ID of the formobject that will be exported. It has to be a Formobject
     * @return
     * @throws ExportException 
     */
    public static Export export(String id) throws ExportException {
        // load the formobject
        FormObject formObject;
        try {
            formObject = FormObject.persistence_loadByID(FormObject.class, Long.parseLong(id));
        } catch (NumberFormatException e) {
            throw new ExportException("invalid ID for a Formobject", e);
        } catch (PersistenceActionFindException e) {
            throw new ExportException("unable to load object ", e);
        }

        // start the export based on the type
        Export export = new XMLExport();
        export.export(formObject);
        export.createParts();
        return export;
    }

    
    
    /**
     * @deprecated
     */
    private void createParts() {
        this.add(this.createClazzesParts());
        this.add(this.createExperimentParts());
        this.add(this.createSampleParts());
    }



    /**
     * export any formobject. The {@link FormObject} has to be a {@link Sample}, {@link Clazz} or a {@link Promt} otherwise a {@link ExportException} will be thrown.
     * @author scholz
     * @param formObject object to be exported.
     * @throws ExportException in case it is not a
     */
    public void export(FormObject formObject) throws ExportException {
        if (formObject instanceof Promt) {
            this.export((Promt) formObject);
        } else if (formObject instanceof Sample) {
            this.export((Sample) formObject);
        } else if (formObject instanceof Clazz) {
            this.export((Clazz) formObject);
        } else {
            throw new ExportException("unable to export because the type to be exported is not supported. (type: " + Util.getClassName(formObject) + ")");
        }
    }
}
