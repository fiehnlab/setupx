package org.setupx.repository.core.communication.status;

import org.setupx.repository.core.user.UserDO;

/**
 * @hibernate.subclass
 */

public abstract class PostprocessEventStatus extends PersistentStatus implements Timestamped{

    public PostprocessEventStatus(long sampleID, UserDO userDO, String message) throws StatusPersistenceException {
        super(sampleID,userDO,message);
    }

    public PostprocessEventStatus() {
        super();
    }
}
