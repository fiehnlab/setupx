/**
 * ============================================================================ File:    LibraryLocalVocabularyCheck.java Package: org.setupx.repository.core.communication.vocabulary cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.vocabulary;

import org.setupx.repository.core.CoreObject;


/**
 * a local librarycheck
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.9 $
 *
 * @see org.setupx.repository.core.communication.vocabulary.LocalLibrary
 */
class LibraryLocalVocabularyCheck extends CoreObject implements VocabularyCheck {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // get a persistent object of the libary mapped by XML	
  private Library library;

  {
    if (ACTIVE) {
      library = LocalLibrary.instance();
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.communication.vocabulary.VocabularyCheck#getSuggestion(java.lang.String)
   */
  public String getSuggestion(final String word) throws VocabularyCheckException {
    if (ACTIVE) {
      return (library.createSuggestion(word));
    } else {
      return "";
    }
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.communication.vocabulary.VocabularyCheck#check(java.lang.String)
   */
  public boolean check(final String word) throws VocabularyCheckException {
    if (ACTIVE) {
      return (library.contains(word));
    } else {
      return false;
    }
  }
}
