package org.setupx.repository.core.util.hotfix;

import org.setupx.repository.core.communication.importing.logfile.ScannedPair;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;

import org.hibernate.Session;

import org.hibernate.criterion.Expression;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;


/**
 * 
 *
 * @author $author$
 * @version $Revision$
 */
public class ScannedSamplesRedundancy {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static Session hqlSession = null;
  private static HashSet checkedIDs = null;

  static {
    hqlSession = PersistenceConfiguration.createSessionFactory().openSession();
    checkedIDs = new HashSet();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @throws PersistenceActionFindException TODO
   */
  public static final void cleanScannedSamlples() throws PersistenceActionFindException {
    // read all scanned samples
    final List allScannedSamples = ScannedPair.persistence_loadAll(ScannedPair.class, hqlSession);
    final int size = allScannedSamples.size();

    // take first element and look for redundencies
    Iterator allScannedSamplesIter = allScannedSamples.iterator();

    while (allScannedSamplesIter.hasNext()) {
      ScannedPair scannedPair = (ScannedPair) allScannedSamplesIter.next();
      String label = scannedPair.getLabel();
      String sampleID = scannedPair.getSampleID();
      long uoid = scannedPair.getUOID();

      if (!checkedIDs.contains("" + label)) {
        List redunancies = hqlSession.createCriteria(ScannedPair.class).add(Expression.eq("label", label)).add(Expression.eq("sampleID", sampleID)).list();

        // kill them
        remove(redunancies, uoid);
      } else {
        Logger.log(null, scannedPair + " is done already.");
      }
    }
  }

  /**
   * TODO: 
   *
   * @param redunancies TODO
   * @param excludeUOID TODO
   */
  private static void remove(List redunancies, long excludeUOID) {
    Iterator iterator = redunancies.iterator();

    while (iterator.hasNext()) {
      ScannedPair element = (ScannedPair) iterator.next();
      Logger.log(null, "remove: " + element.getUOID() + "  " + excludeUOID + "   " + (element.getUOID() != excludeUOID));

      if (element.getUOID() != excludeUOID) {
        Logger.debug(null, "removing " + element.getUOID() + "  " + element);
        element.remove();
        checkedIDs.add("" + element.getLabel());
      }
    }
  }
}
