/**
 * ============================================================================ File:    PromtUserAccessRight.java Package: org.setupx.repository.core.user.access cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.user.access;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.web.WebConstants;
import org.setupx.repository.web.forms.InsufficientUserRightException;


/**
 * Access right of one user one a certain experiment.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @hibernate.class table = "accessright"
 * @hibernate.discriminator column = "discriminator"
 * @hibernate.cache usage="read-write"  
 */
public class PromtUserAccessRight extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private int accessCode = 0;
  private long experimentID = 0;
  private long userID = 0;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final long serialVersionUID = 1L;

  //public static final int NOACCESS = 0;
  public static final int CREATE = 1;
  public static final int READ = 20;
  public static final int CLONE = 30;
  public static final int WRITE = 40;
  public static final int DOWNLOAD_RESULT = 45;
  public static final int DELETE = 80;
  public static final int EXPORT = 85;
  public static final int UNLIMITED = 100;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ExperimentUserAccessRight object.
   */
  public PromtUserAccessRight() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Define the level of the access. 
   *
   * @param accessCode TODO
   */
  public void setAccessCode(int accessCode) {
    this.accessCode = accessCode;
  }

  /**
   * @hibernate.property
   */
  public int getAccessCode() {
    return accessCode;
  }

  /**
   * @param experimentID experiment ID 
   */
  public void setExperimentID(long experimentID) {
    this.experimentID = experimentID;
  }

  /**
   * @hibernate.property
   */
  public long getExperimentID() {
    return experimentID;
  }

  /**
   * TODO: 
   *
   * @param userID TODO
   */
  public void setUser(UserDO userID) {
    this.userID = userID.getUOID();
  }

  /**
   * TODO: 
   *
   * @param userID TODO
   */
  public void setUserID(long userID) {
    this.userID = userID;
  }

  /**
   * @hibernate.property
   */
  public long getUserID() {
    return userID;
  }

  /**
   * check the access to a certain experiment
   *
   * @param userID TODO
   * @param promtID TODO
   * @param action TODO
   *
   * @throws InsufficientUserRightException TODO
   */
  public final static void checkAccess(long userID, long promtID, int action)
    throws InsufficientUserRightException {
    if (UserDO.isMasterUser(userID)) {
      return;
    }

    PromtUserAccessRight userAccessRight;

    debug(PromtUserAccessRight.class, "checking access right for user: " + userID + " on exp: " + promtID);
    debug(PromtUserAccessRight.class, "level of accessright requested : " + action);

    /* in case it is a new Experiment - no user rights requiered */
    if (action == WebConstants.PARAM_LOAD_ACTION_CREATE) {
      debug(PromtUserAccessRight.class, "level of accessright for creating new Experiment is not limited");

      return;
    }

    try {
      userAccessRight = new SXQuery().findPromtUserAccessRightForUserID(userID, promtID);
      debug(PromtUserAccessRight.class, "level of accessright for user : " + userAccessRight.getAccessCode());
    } catch (PersistenceActionFindException e) {
      throw new InsufficientUserRightException("The user has insufficient right not access this information.", e);
    }

    // check if there is a combination of this pair of userrights registered
    if (userAccessRight.getAccessCode() < READ) {
      throw new InsufficientUserRightException("The user has insufficient right to access this information.");
    }

    // check if the requiered action is possible with the current accescode
    if (userAccessRight.getAccessCode() < action) {
      throw new InsufficientUserRightException("The user has insufficient right not start the requiered action on this information. Requested level: " + action + " - access limited to " + userAccessRight.getAccessCode() + ".");
    }

    debug(PromtUserAccessRight.class, "level of accessright granted : " + userAccessRight.getAccessCode());
  }

  /**
   * The setter method for Unique Object identifier.
   */
  public void setUOID(long lId) {
    uoid = lId;
  }

  /**
   * The getter method for Unique Object identifier.
   * 
   * <p>
   * The id is generated by the persistence layer.
   * </p>
   *
   * @return unique id for this object
   *
   * @hibernate.id column = "uoid" generator-class="native"
   */
  public long getUOID() {
    return (uoid);
  }

  /**
   * creates unrestricted access for a user on a specific promt
   *
   * @param promptID experiment id
   * @param sxUserID user id
   */
  public static void createFullAccess(long promptID, long sxUserID) {
    if (promptID == 0) {
      warning(PromtUserAccessRight.class, "unable to create Accessrights for user:" + sxUserID + " cause ExperimentID is : " + promptID);

      return;
    }

    PromtUserAccessRight accessRight = null;

    try {
      accessRight = new SXQuery().findPromtUserAccessRightForUserID(sxUserID, promptID);
    } catch (PersistenceActionFindException e) {
      // there is no existing access right
      accessRight = new PromtUserAccessRight();
    }

    accessRight.setUserID(sxUserID);
    accessRight.setAccessCode(PromtUserAccessRight.UNLIMITED);
    accessRight.setExperimentID(promptID);

    try {
      accessRight.update(true);
    } catch (PersistenceActionException e1) {
      e1.printStackTrace();
    }
  }
}
