package org.setupx.repository.core.communication.ldap;

public class AcquisitionFileException extends Exception {

    public AcquisitionFileException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public AcquisitionFileException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public AcquisitionFileException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public AcquisitionFileException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
