    package org.setupx.repository.core.ws;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.setupx.repository.Config;
import org.setupx.repository.core.ServiceMediator;
import org.setupx.repository.core.communication.binbase.BBConnector;
import org.setupx.repository.core.communication.binbase.trigger.BinBaseServiceConnector;
import org.setupx.repository.core.communication.export.ReportEngine;
import org.setupx.repository.core.communication.exporting.ExportException;
import org.setupx.repository.core.communication.leco.ColumnDetector;
import org.setupx.repository.core.communication.leco.ColumnInformation;
import org.setupx.repository.core.communication.leco.ColumnInformationNotAvailableException;
import org.setupx.repository.core.communication.leco.LecoACQFile;
import org.setupx.repository.core.communication.ncbi.NCBIConnector;
import org.setupx.repository.core.communication.ncbi.NCBIException;
import org.setupx.repository.core.communication.ncbi.local.NCBIEntry;
import org.setupx.repository.core.communication.status.ResultReceivedStatus;
import org.setupx.repository.core.communication.status.StatusPersistenceException;
import org.setupx.repository.core.communication.technology.platform.Datafile;
import org.setupx.repository.core.communication.technology.platform.TechnologyProvider;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.UserFactoryException;
import org.setupx.repository.core.user.UserNotFoundException;
import org.setupx.repository.core.user.WrongPasswordException;
import org.setupx.repository.core.user.access.PromtUserAccessRight;
import org.setupx.repository.core.util.ConnectException;
import org.setupx.repository.core.util.hotfix.HotFix;
import org.setupx.repository.core.util.hotfix.HotFixException;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.web.forms.inputfield.multi.Promt;

import com.pavelvlasov.persistence.PersistenceException;


/**
 * WebServiceImplemetation for the M1_java. This Service is the Implemetation for the methods, that are  spezified in the M1 service.
 *
 * @author <a href="mailto:scholz@zeypher.com?subject=m1" >&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 */
public class M1_BindingImpl extends Service implements M1_PortType {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param experimentID TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws WebServiceException TODO
   */
  public String[] getClassIDsByExperiment(String experimentID)
    throws RemoteException, WebServiceException {
    List list = new SXQuery().findClazzIDsByPromtID(Long.parseLong(experimentID));
    Iterator iterator = list.iterator();
    String[] result = new String[list.size()];
    int pos = 0;

    while (iterator.hasNext()) {
      result[pos] = "" + iterator.next();
      pos++;
    }

    return result;
  }

  /**
   * TODO: 
   *
   * @param classID TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws WebServiceException TODO
   */
  public String getClassInfos(String classID) throws RemoteException, WebServiceException {
    // convert to long
    long classIDlong = Long.parseLong(classID);

    java.util.Iterator iterator1 = new SXQuery().findLabelsforClazz(classIDlong).iterator();

    StringBuffer stringBuffer = new StringBuffer();

    while (iterator1.hasNext()) {
      Object[] element = (Object[]) iterator1.next();

      stringBuffer.append("" + element[0].toString());
      stringBuffer.append("" + element[1].toString());
    }

    return stringBuffer.toString();
  }

  /**
   * TODO: 
   *
   * @param sampleID TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws WebServiceException TODO
   */
  public String getClazzIDbySampleID(String sampleID) throws RemoteException, WebServiceException {
    try {
      return new SXQuery().findParent(Long.parseLong(sampleID)) + "";
    } catch (PersistenceActionFindException e) {
      throw instanceWebSerivceException(e);
    }
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.ws.M1_PortType#getExperimentIDbyClazz(int)
   */
  public String getExperimentIDbyClazz(String clazzID)
    throws WebServiceException {
    try {
      return new SXQuery().findPromtIDbyClazz(Integer.parseInt(clazzID)) + "";
    } catch (PersistenceActionFindException e) {
      throw instanceWebSerivceException(e);
    }
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.ws.M1_PortType#getExperimentIDbySample(int)
   */
  public String getExperimentIDbySample(String sampleID)
    throws WebServiceException {
    try {
      return new SXQuery().findPromtIDbySample(Long.parseLong(sampleID)) + "";
    } catch (PersistenceActionFindException e) {
      throw instanceWebSerivceException(e);
    }
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.ws.M1_PortType#getExperimentsSheduled()
   */
  public String getExperimentsSheduled() throws RemoteException, WebServiceException {
    throw instanceWebSerivceException(new UnsupportedOperationException(""));
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.ws.M1_PortType#getSampleIDbyACQLabel(java.lang.String)
   */
  public String getSampleIDbyACQLabel(String acquistitionName)
    throws RemoteException, WebServiceException {
    try {
      return new SXQuery().findSampleIDByAcquisitionName(acquistitionName) + "";
    } catch (PersistenceActionFindException e) {
      throw instanceWebSerivceException(e);
    }
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.ws.M1_PortType#getSamples(int)
   */
  public String[] getSamplesByClassID(String clazzID) throws WebServiceException {
    List list = new SXQuery().findChildsIDs(new Long(clazzID).intValue());
    Iterator iterator = list.iterator();
    int i = 0;
    String[] results = new String[list.size()];

    while (iterator.hasNext()) {
      results[i] = iterator.next() + "";
      i++;
    }

    return results;
  }

  /**
   * find all sampleIDs for one user. 
   *
   * @param userID userID of the user the samples are requested for.
   * @return an array of sampleIDs 
   * @throws WebServiceException 
   */
  public String[] getSamplesForUser(long userID) throws WebServiceException {
    int accessright_limit = PromtUserAccessRight.READ;
    HashSet sampleIDs = new HashSet();

    // get all his experiments
    List list = new SXQuery().findPromtUserAccessRightForUserID(userID);
    Iterator iterator = list.iterator();

    while (iterator.hasNext()) {
      PromtUserAccessRight element = (PromtUserAccessRight) iterator.next();

      if (element.getAccessCode() >= accessright_limit) {
        sampleIDs.addAll(new SXQuery().findSampleIDsByPromtID(element.getExperimentID()));
      }
    }

    Logger.debug(this, "number of samples for user " + sampleIDs.size());

    Iterator sampIterator = sampleIDs.iterator();

    String[] ids = new String[sampleIDs.size()];
    int i = 0;

    while (sampIterator.hasNext()) {
      ids[i] = sampIterator.next() + "";
      i++;
    }

    return ids;
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.ws.M1_PortType#activateService(int)
   */
  public String activateService(String serviceCode) throws WebServiceException {
    try {
      new HotFix(Integer.parseInt(serviceCode));
    } catch (HotFixException e) {
      throw instanceWebSerivceException(e);
    }

    return "";
  }

  /**
   */
  public void addEvent(String sampleID, String message)
    throws WebServiceException {
  }

  /**
   * the service requires a webservice accesscode - if this code is not provided the system will pause for four seconds.
   */
  public long authenticateUser(String username, String password, String authorizationCode)
    throws WebServiceException {
    UserDO userDO;

    // wait 6 seconds and then start doing stuff
    try {
        if (authorizationCode.compareTo(Config.WEB_SERVICE_ACCESSCODE) != 0){
            Thread.sleep(4000);
        }

      userDO = ServiceMediator.findUser(username, password);
    } catch (InterruptedException e) {
      e.printStackTrace();
      throw instanceWebSerivceException(new ConnectException("not available right now. Error(23-1)"));
    } catch (UserNotFoundException e) {
      throw instanceWebSerivceException(new ConnectException("user not found. Error(23-2)"));
    } catch (WrongPasswordException e) {
      throw instanceWebSerivceException(new ConnectException("user not found. Error(23-3)"));
    } catch (UserFactoryException e) {
      throw instanceWebSerivceException(new ConnectException("not available right now. Error(23-4)"));
    }

    return userDO.getUOID();
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws WebServiceException TODO
   */
  public String[][] availableColumns() throws RemoteException, WebServiceException {
    return ColumnDetector.getAllColumnConfigurationsArray();
  }

  /**
   * TODO: 
   *
   * @param label TODO
   * @param message TODO
   *
   * @throws RemoteException TODO
   * @throws WebServiceException TODO
   */
  public void createMessage(String label, String message)
    throws RemoteException, WebServiceException {
    org.setupx.repository.server.logging.Message message2 = new org.setupx.repository.server.logging.Message();
    message2.setLabel(label);
    message2.setMessage(message);

    try {
      message2.update(true);
    } catch (PersistenceActionUpdateException e) {
      e.printStackTrace();
    }
  }

  /**
   * TODO: 
   *
   * @param acqname TODO
   *
   * @return TODO
   *
   * @throws RemoteException TODO
   * @throws WebServiceException TODO
   */
  public String determineColumnIDbyACQname(String acqname)
    throws RemoteException, WebServiceException {
    ColumnInformation columnInformation;

    try {
      columnInformation = LecoACQFile.getColumnBySampleACQname(acqname);
    } catch (ColumnInformationNotAvailableException e) {
      throw instanceWebSerivceException(e);
    }

    Logger.debug(this, "for " + acqname + " found: " + columnInformation);

    return columnInformation.getBbid();
  }

  /**
   * find the type of species
   *
   * @param speciesname TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   *
   * @see NCBIEntry#ANIMAL
   * @see NCBIEntry#HUMAN
   * @see NCBIEntry#PLANT
   * @see NCBIEntry#UNKNOWN
   */
  public String determineSpeciesType(String speciesname)
    throws WebServiceException {
    try {
      int id = NCBIConnector.determineNCBI_Id(speciesname);

      return NCBIConnector.determineNCBI_Information(id).getSpeciesType() + "";
    } catch (Exception e) {
      throw instanceWebSerivceException(e);
    }
  }

  /**
   * find a species name
   *
   * @param speciesname TODO
   *
   * @return TODO
   *
   * @throws WebServiceException TODO
   */
  public String determineSpeciesTypeName(String speciesname)
    throws WebServiceException {
    try {
      int id = NCBIConnector.determineNCBI_Id(speciesname);

      return NCBIConnector.determineNCBI_Information(id).getSpeciesTypeName();
    } catch (Exception e) {
      throw instanceWebSerivceException(e);
    }
  }

  /**
   * TODO: 
   *
   * @param experimentID TODO
   *
   * @throws RemoteException TODO
   * @throws WebServiceException TODO
   */
  public void exportByExperimentID(String experimentID)
    throws RemoteException, WebServiceException {
    try {
      Promt.export(Long.parseLong(experimentID), ColumnInformation.RTX5);
    } catch (Exception e) {
      org.setupx.repository.server.logging.Message message = new org.setupx.repository.server.logging.Message();
      message.setLabel("Webservice error.");
      message.setMessage(e.toString() + "    " + e.getMessage());

      try {
        message.update(true);
      } catch (PersistenceActionUpdateException e1) {
        e1.printStackTrace();
      }

      throw instanceWebSerivceException(e);
    }
  }

  /* (non-Javadoc)
   * @see org.setupx.repository.core.ws.M1_PortType#uploadResultFile(java.lang.String, byte[])
   */
  public void uploadResultFile(String clazzSetID, String resultfile_url)
  throws WebServiceException {
      Logger.log(this, "---------- receiving file for clazzSet " + clazzSetID + " at: " + resultfile_url);
      ServiceMediator.notifyUser(Config.OPERATOR_ADMIN, "received file from binbase: " + clazzSetID, "URL: " + resultfile_url);

      // store uploaded resultset in directory
      URL url;

      try {
              url = new URL(resultfile_url);
      } catch (MalformedURLException e) {
          // try if the server address is simply missing the s serveraddress
          if(resultfile_url.indexOf("http://") < 0){
              try {
                  // adding the serveraddress and try again
                  String serveraddress = new BinBaseServiceConnector().getBinBaseServer();
                  resultfile_url = serveraddress + resultfile_url;
                  Logger.debug(this, "retrying to connect to server with url: " + resultfile_url);
                url = new URL(resultfile_url);
            } catch (MalformedURLException e1) {
                throw instanceWebSerivceException("illegal URL", e);
            }
          } else {
              throw instanceWebSerivceException("illegal URL", e);
          }
      }

      // file where the data will be stored at
      java.io.File target = BBConnector.getBBResultFile(clazzSetID);

      try {

          URLConnection urlConnection = (URLConnection) new URL(resultfile_url).openConnection();

          DataInputStream in = new DataInputStream(urlConnection.getInputStream());

          OutputStream os = new FileOutputStream(target);
          BufferedOutputStream bos = new BufferedOutputStream(os);

          byte[] buffer = new byte[1024];
          int readCount;

          while ((readCount = in.read(buffer)) > 0) {
            bos.write(buffer, 0, readCount);
          }

          bos.close();

          
          
          ServiceMediator.notifyUser(Config.OPERATOR_ADMIN, "received file from binbase: " + clazzSetID, "The file " + target + " contains the resultdate from BinBase.");
          try {
              ResultReceivedStatus.createInstancesForPromt(clazzSetID, UserDO.load("sx"));
          } catch (StatusPersistenceException e) {
              e.printStackTrace();
          } catch (PersistenceActionFindException e) {
              e.printStackTrace();
          } catch (PersistenceActionUpdateException e) {
            e.printStackTrace();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
      } catch (IOException e) {
          throw instanceWebSerivceException("illegal URL", e);
      }


      // adding file merge of binbasedate and setupx data
      //File.addToZipFile();

      // open zip file

      // add metadatafile

      // add general comment file

  }

  /**
   * creates a new WebServiceException
   * @author Martin Scholz
   */
  private static final WebServiceException instanceWebSerivceException(String string, Exception e) {
    e.printStackTrace();

    return instanceWebSerivceException(new Exception("SetupX: Error: " + string));
  }

  /**
   * creates a new WebServiceException
   * @author Martin Scholz
   */
  private static final WebServiceException instanceWebSerivceException(Exception exception) {
    Logger.log(M1_BindingImpl.class, exception);

    Logger.log(exception, "   --- exception is thrown - exceptions is casted to " + WebServiceException.class);

    WebServiceException ex = new WebServiceException();
    ex.setFaultString(exception.toString() + " -- " + exception.getLocalizedMessage());

    //Logger.log(ex,ex);
    exception.printStackTrace();

    return ex;
  }

public String determineMetaDataByClass(String classID, String key) throws RemoteException, WebServiceException {
    return metadata(classID);
}

public String determineMetaDataByExperiment(String experimentID, String key) throws RemoteException, WebServiceException {
    return metadata(experimentID);
}

public String determineMetaDataBySample(String sampleID, String key) throws RemoteException, WebServiceException {
    return metadata(sampleID);
}

public String determineMetaDataBySampleMax(String sampleID, String key) throws RemoteException, WebServiceException {
    try {
        return ReportEngine.determineMetaDataBySampleMax(sampleID).toString();
    } catch (ExportException e) {
        throw instanceWebSerivceException(e);
    }
}

public String determineNCBIBySampleID(String sampleID) throws RemoteException, WebServiceException {
    long promtID;
    // find promtID by sampleID
    try {
        promtID = new SXQuery().findPromtIDbySample(Integer.parseInt(sampleID));
    } catch (NumberFormatException e) {
        e.printStackTrace();
        throw instanceWebSerivceException("unable to determine promtID for sampleID " + sampleID,  e);
    } catch (PersistenceActionFindException e) {
        e.printStackTrace();
        throw instanceWebSerivceException("unable to determine promtID for sampleID " + sampleID + " (persistence layer)",  e);
    }
    
    
    // find species by promtID  
    List speciesList = new SXQuery().findSpeciesbyPromtID(promtID);
    if (speciesList.size() < 1){
        throw instanceWebSerivceException("unable to determine species for " + sampleID + " - the promt contains no species ",  new NullPointerException());
    }

    // convert it to ncbi code
    try {
        return NCBIConnector.determineNCBI_Id("" + speciesList.get(0))+"";
    } catch (Exception e) {
        throw instanceWebSerivceException("unable to find NCBI code for species: " + speciesList.get(0), e);
    }
}

public String determineSpeciesNameByNCBI(String ncbiID) throws RemoteException, WebServiceException {
    try {
        return NCBIConnector.determineNCBI_Information(Integer.parseInt(ncbiID)).getName();
    } catch (NumberFormatException e) {
        throw instanceWebSerivceException(e);
    } catch (NCBIException e) {
        throw instanceWebSerivceException(e);
    }
}


/**
 * determine if a user is assigned to the masterusergroup
 */
public boolean isMasterUser(String username, String key) throws RemoteException, WebServiceException {
    
    UserDO userDO;
    try {
        userDO = UserDO.load(username);
    } catch (PersistenceActionFindException e) {
        throw instanceWebSerivceException("unable to find user: " + username, e);
    }
    return UserDO.isMasterUser(userDO.getUOID());
}


public boolean isUser(String username, String key) throws RemoteException, WebServiceException {
    try {
        UserDO.load(username);
        return true;
    } catch (PersistenceActionFindException e) {
        return false;
    }
}

private String metadata(String experimentID) throws WebServiceException {
    try {
        return ReportEngine.determineMetaData(experimentID).toString();
    } catch (ExportException e) {
        throw instanceWebSerivceException("unable to export metadata for " + experimentID, e);
    }
}


    public String[] getExperimentsByUser(String username, String password) throws WebServiceException{
        
        UserDO userDO;
        try {
            userDO = ServiceMediator.findUser(username, password);
        } catch (Exception e) {
            throw instanceWebSerivceException(e);
        } 
        
        Vector ids = new Vector();
        List list = new SXQuery().findPromtUserAccessRightForUserID(userDO.getUOID());
        
        Iterator iter = list.iterator();
        while(iter.hasNext()){
            PromtUserAccessRight promtUserAccessRight = (PromtUserAccessRight)iter.next();
            if (promtUserAccessRight.getAccessCode() > PromtUserAccessRight.READ){
                ids.add("" + promtUserAccessRight.getExperimentID());
            }
        }

        // convert it to an array of strings
        String[] result = new String[ids.size()];
        
        Iterator iterator = ids.iterator();
        int pos = 0;
        while (iterator.hasNext()){
            result[pos] = iterator.next().toString();
            pos++;
        }
        return result;
    }

    /**
     * checking for the date when a spcific txt file was created
     */
    public String getSampleTimestamp(String filename) throws RemoteException, WebServiceException {

        // check the filename
        if (filename.length() > 5 || filename.indexOf('_') > 1){
            
        } else {
            throw instanceWebSerivceException(new Exception("illegal filename " + filename));
        }

        // find sampleID
        long sampleID = 0;
        try {
            sampleID = new SXQuery().findSampleIDByAcquisitionName(filename);
        } catch (PersistenceActionFindException e) {
            instanceWebSerivceException("unable to determine sampleID for " + filename, e);
        }
        
        // find the datafiles
        List l = null;
        try {
            l = TechnologyProvider.getDatafiles(sampleID);
        } catch (PersistenceActionFindException e) {
            instanceWebSerivceException("unable to find any datafiles for sampleID " + sampleID, e);
        }
        
        // find a TXT type
        Iterator iter = l.iterator();
        while (iter.hasNext()){
            Datafile datafile = (Datafile)iter.next();
//            if (datafile.getTechnologyFileType().getTechnology().getLabel().indexOf("GC") > 0){
            if (datafile.getSource().indexOf(filename) > -1){
                return datafile.getDateCreated().toString();
            }
        }
        instanceWebSerivceException("unable to find any datafiles for GC for sampleID " + sampleID, new Exception("unable to find any files for technology called \"GC\""));
        // can not be reached
        return "";
    }
}
