package org.setupx.repository.core.communication.document.sop;

import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.inputfield.multi.MultiField;
import org.setupx.repository.web.forms.inputfield.multi.SOPMultiField;

public class StandardOperationProcedureFormObjectFactory {

    public static FormObject createInputfields() {
        // create a formobject that acts as a container for all sops
        MultiField container = new SOPMultiField();
        return container;
    }
}
