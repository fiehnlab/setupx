package org.setupx.repository.core.query;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.access.PromtUserAccessRight;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;

class QueryFilter extends CoreObject {

    private static final Class thiz = QueryFilter.class;
    private UserDO user;
    private static List filters;

    private QueryFilter(UserDO user) {
        this.user = user;
    }
    
    
    /**
     * check if the accesslevel for a certain user can be granted or not.
     * <p> accesslevel is {@link PromtUserAccessRight#CREATE}, {@link PromtUserAccessRight#UNLIMITED}, ...
     * @param promtID
     * @param accessLevel
     * @return
     */
    final boolean checkAccess(final long promtID, final int accessLevel){
        try {
            return new SXQuery().findPromtUserAccessRightForUserID(user.getUOID(), promtID).getAccessCode() >= accessLevel;
        } catch (PersistenceActionFindException e) {
            warning(thiz, "can not access the promt accessrigths");
            warning(e);
            return false;
        }
    }



    /**
     * creating an instance of the filter. In case a filter has been created earlier, the existing filter will be reused.
     * 
     * @param user user for whom the filter will be created.
     * @return a new filter instance
     */
    public static QueryFilter newInstance(UserDO user) {
        if (user == null){
            throw new InstantiationError("user can not be null.");
        }
        
        QueryFilter filter = null;
        
        // get a filter from the exising set of filters
        try {
            filter = (QueryFilter)filters.get(Integer.parseInt("" + user.getUOID() ));
            if (filter == null || filter.getUser() == null) {
                throw new Exception("no valid filter exists.");
            }
        } catch (Exception e) {
            // no filter esits
            debug(thiz, "creating new queryfilter for " + user.getDisplayName());
            filter = new QueryFilter(user);
        }
        return filter;
    }


    
    public List filter(List resultList) {
        log("--- starting to filter set of " + resultList.size() + " elements. ---");
        List filteredList = new Vector();
        
        Iterator iterator = resultList.iterator();
        while (iterator.hasNext()) {
            QueryMasterAnswer answer = (QueryMasterAnswer) iterator.next();
            
            if (isAccessible(answer)){
                debug(thiz, "adding answer " + answer.getLabel() + "   " + filteredList.size() + "  "  + resultList.size());
                filteredList.add(answer);
            } else {
                debug(thiz, "discarding answer " + answer.getLabel()  + "   " + filteredList.size() + "  "  + resultList.size());
            }
        }
        log("--- finished filtering: remaining " + filteredList.size() + " of " + resultList.size() + " elements. ---");
        return filteredList;
    }


    private boolean isAccessible(QueryMasterAnswer answer) {
        if (! answer.isAccessRestricted()) {
            return true;
        } else {
            return answer.checkAccessRestriction(user);
        }
    }


    public UserDO getUser() {
        return user;
    }


    public void setUser(UserDO user) {
        this.user = user;
    }
}
