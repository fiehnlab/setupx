/**
 * ============================================================================ File:    CustomerMetaDataRetrieval.java Package: org.setupx.repository.core.communication.importing.metadata cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.importing.metadata;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;

import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.web.forms.inputfield.multi.Promt;


/**
 * finds data that is related to a specific label in the related documents of a promt
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class CustomerMetaDataRetrieval extends MDCore {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private HashSet sourceFiles = new HashSet();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new CustomerMetaDataRetrieval object.
   *
   * @param promtID the id of the experiment that the sample is related to
   *
   * @throws IOException unable to oben the related files
   */
  public CustomerMetaDataRetrieval(long promtID) throws IOException {
    // get files from folder for this specific experiment
      File[] files = this.getFiles(promtID);


    for (int i = 0; i < files.length; i++) {
      // check if these files might be interesting 
      File file = files[i];

      if (FileQualification.check(file)) {
        this.sourceFiles.add(new MetaDataFile(file));
      }
    }

    debug(this, "using " + this.sourceFiles.size() + " files for information retrieval.");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public File[] getFiles(long promtID) {
      return Promt.determineRelatedFields(promtID);
  }

/**
   * query for a specific label
   *
   * @param queryString the label
   *
   * @return data that was found
   */
  public final Data query(String queryString) {
    debug(this, "query for " + queryString);

    Data data = new Data();

    Iterator iterator = this.sourceFiles.iterator();

    while (iterator.hasNext()) {
      MetaDataFile metadatafile = (MetaDataFile) iterator.next();
      data.add(metadatafile.query(queryString));
    }

    return data;
  }

  /**
   * TODO: 
   *
   * @param sampleID TODO
   *
   * @return TODO
   *
   * @throws PersistenceActionFindException TODO
   * @throws IOException TODO
   */
  public static final Data querySample(int sampleID) throws PersistenceActionFindException, IOException {
    final long promtID = new SXQuery().findPromtIDbySample(sampleID);
    final String label = new SXQuery().findSampleLabelBySampleID(sampleID);

    return new CustomerMetaDataRetrieval(promtID).query(label);
  }
}
