/**
 * ============================================================================ File:    Jack.java Package: org.setupx.repository.core.util cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 *
 * @jmx:mbean
 */
public class Jack {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @jmx.managed-attribute name="xxxxx" description="descr" type="java.lang.String"
   */
  public String box = "bla";
}
