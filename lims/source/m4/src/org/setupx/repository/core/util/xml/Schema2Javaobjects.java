/**
 * ============================================================================ File:    Schema2Javaobjects.java Package: org.setupx.repository.core.util.xml cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.xml;

import java.io.FileNotFoundException;


/**
 * Converter XML Schema to Java-Source-Code
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 *
 * @deprecated 19oct2004 - castor api changed
 */
public class Schema2Javaobjects {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Schema2Javaobjects object.
   */
  public Schema2Javaobjects() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param XML -SchemaFile packagename targetDirectory
   *
   * @throws FileNotFoundException the xml-schema-file was not found
   * @throws WrongNumberArgsException if more or less than 3 attributes where used
   */
  public static void main(String[] args) throws FileNotFoundException, WrongNumberArgsException {
    if (args.length != 3) {
      throw new WrongNumberArgsException("usage: \n first Attribute = XML-SchemaFile   \n second Attribute = packagename \n third Attribute = targetDirectory \n" + args.length);
    }

    String filename = args[0];
    String packagename = args[1];
    String target = args[2];

    System.out.println("generating source from XMLSchema-File (" + filename + ") to package " + packagename + " - stored in " + target);
    generate(filename, packagename, target);

    System.out.println("DONE - bye");
  }

  /**
   * generates javasources for a xml schema file
   *
   * @param filename the xml-schema-file
   * @param packagename the packagename for the generated files
   * @param target the folder for the new generated files
   *
   * @throws FileNotFoundException xml-schema-file was not found
   */
  private static void generate(String filename, String packagename, String target)
    throws FileNotFoundException { /*
       SourceGenerator generator = new SourceGenerator();
       generator.setDestDir(target);
       generator.generateSource(filename, packagename); //"C:\\schema.xsd", "de.packagename.test");*/
  }
}
