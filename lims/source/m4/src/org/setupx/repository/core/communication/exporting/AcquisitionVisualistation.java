/**
 * ============================================================================ File: AcquisitionVisualistation.java
 * Package: org.setupx.repository.core.communication.exporting cvs: $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06
 * 21:54:21 scholz Exp $ ============================================================================ Martin Scholz
 * Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/
 * ============================================================================
 */
package org.setupx.repository.core.communication.exporting;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample;
import org.setupx.repository.core.communication.exporting.sampletypes.Blank;

/**
 * A visualisation of the whole experiment - a graphic one and a text one.
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class AcquisitionVisualistation extends CoreObject {
    // ~ Instance fields
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public AcquisitionVisualistation.Graphic graphicVisualisation = null;
    public AcquisitionVisualistation.Text textVisualisation = null;

    /** Table containing all samples as class:AcquisitionSample as value and the pos as key (String) */
    protected Hashtable samplesTable = new Hashtable();

    // ~ Static fields/initializers
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private AcquisitionVisualistation(Acquisition i_acquisition) throws AcquisitionVisualistationException {
        debug(this, "creating a new instance");

        // creating an array of all samples incl. the positions
        AcquisitionSample sample = null;

        for (int i = 0; i < i_acquisition.samples.length; i++) {
            sample = i_acquisition.samples[i];

            // get position
            int pos = 0;

            try {
                pos = Integer.parseInt(sample.getParamter(AcquisitionParameter.VIAL).getValue().replaceAll(" ", ""));

                // chekcing if pos is used already
                if (this.samplesTable.containsKey(pos + "")) {
                    // ignore dublicated blanks
                    if (sample instanceof Blank) {
                    } else {
                        i_acquisition.showAcquisition();
                        throw new AcquisitionVisualistationException("there is more then one sample at the position "
                                + pos);
                    }
                }

                // log(this, "pos: " + pos + " " + AcquisitionParameter.NAME + ": " +
                // sample.getParamter(AcquisitionParameter.NAME).getValue());
                this.samplesTable.put(pos + "", sample);
            } catch (ParameterException e) {
                e.printStackTrace();
            }
        }

        // creating a text visualisation
        this.textVisualisation = new Text(this);

        // creating a graphic visualisation
        this.graphicVisualisation = new Graphic(this);
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @return iterator containing all AcquisitionSample -- copy: iterator of --> Table containing all samples as
     *         class:AcquisitionSample as value and the pos as key (String)
     */
    public Iterator valuesIterator() {
        this.samplesTable.values();

        Vector vector = new Vector();

        for (int i = 0; i < this.samplesTable.size(); i++) {
            String pos = "pos" + i;

            if (this.samplesTable.contains(pos)) {
                vector.add(this.samplesTable.get(pos));
            }
        }

        return vector.iterator();
    }

    /**
     * TODO: 
     * 
     * @param i_acquisition
     *                TODO
     * @return TODO
     * @throws AcquisitionVisualistationException
     */
    protected static AcquisitionVisualistation newInstance(Acquisition i_acquisition)
            throws AcquisitionVisualistationException {
        AcquisitionVisualistation result = new AcquisitionVisualistation(i_acquisition);

        return result;
    }

    /**
     * cuts everything away from start to last occourance of the character
     */
    private static String cut(char c, String tempValue) {
        int pos = tempValue.lastIndexOf(c);

        return tempValue.substring(0, pos);
    }

    // ~ Inner Classes
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public class Graphic extends CoreObject {
        public Graphic(AcquisitionVisualistation visualistation) {
            this.log("new instance  - size: " + AcquisitionVisualistation.this.samplesTable.size());
            this.log("TODO - image generation");
        }
    }

    public class Text extends CoreObject {
        public String htmlVisualistation = null;
        private AcquisitionVisualistation visualistation;

        public Text(AcquisitionVisualistation _visualistation) {
            this.visualistation = _visualistation;
            log(this, "new instance  - size of samples: " + this.visualistation.samplesTable.size());

            // creating a HTML_table of all samples...
            try {
                this.htmlVisualistation = this.createHTMLCode();
            } catch (AcquisitionVisualistationException e) {
                e.printStackTrace();
            }
        }

        /**
         * @return
         * @throws AcquisitionVisualistationException
         */
        private String createHTMLCode() throws AcquisitionVisualistationException {
            String result = "<table>";
            Iterator iterator = this.visualistation.valuesIterator();

            // header
            Iterator labels = AcquisitionParameter.getLabelIterator();

            while (labels.hasNext()) {
                result = result.concat("<th>" + labels.next() + "</th>");
            }

            result = result.concat("</tr>");

            // iterate over all samples
            while (iterator.hasNext()) {
                AcquisitionSample acquisitionSample = (AcquisitionSample) iterator.next();

                result = result.concat("<tr>");
                labels = AcquisitionParameter.getLabelIterator();

                while (labels.hasNext()) {
                    result = result.concat("<td>");

                    try {
                        String tempValue = acquisitionSample.getParamter(labels.next() + "").getValue();
                        tempValue = cut('\\', tempValue);

                        result = result.concat(tempValue);
                    } catch (ParameterException e) {
                        // throw new AcquisitionVisualistationException(e);
                    }

                    result = result.concat("</td>");
                }

                result = result.concat("</tr>");
            }

            result = result.concat("</table>");

            return result;
        }
    }
}
