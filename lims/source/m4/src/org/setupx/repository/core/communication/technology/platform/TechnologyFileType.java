package org.setupx.repository.core.communication.technology.platform;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.util.FileAgeFilter;


/**      
 * @hibernate.class table = "technologyfiletype"
 * @hibernate.discriminator column = "discriminator"
 */
public class TechnologyFileType extends TechnoCoreObject {
    
    String describtion = "";
    Set defaultLocations = new HashSet();
    
    // pattern of the files
    String pattern = "";
    
    Set datafiles = new HashSet();
    private String label;
    private Technology technology;
    private List sampleIDs;
    
    private boolean automaticAssignment = Config.FILESCANNER_AUTOASSIGNED_DEFAULT;
    
    /**does this type of {@link TechnologyFileType} automaticly get scanned and files that are found 
     * do they get automaticly assigned?
     * @hibernate.property
     */
    public boolean isAutomaticAssignment() {
        return automaticAssignment;
    }

    public void setAutomaticAssignment(boolean automaticAssignment) {
        this.automaticAssignment = automaticAssignment;
    }

    /**
     * @deprecated only for persistence
     */
    public TechnologyFileType() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param describtion
     * @param location
     * @param pattern
     */
    public TechnologyFileType(String label, String describtion, Location defaultlocation, String pattern) {
        super();
        this.label = label;
        this.describtion = describtion;
        this.defaultLocations.add(defaultlocation);
        this.pattern = pattern;
        debug("created");

    }


    public void addDatafile(Datafile datafile) {
        datafiles.add(datafile);
    }

    /**
     * @hibernate.property
     */
    public String getDescribtion() {
        return describtion;
    }
    
    /**
     * @hibernate.set lazy="true"
     * @hibernate.collection-key column="technologyfiletype"
     * @hibernate.collection-one-to-many  class="org.setupx.repository.core.communication.technology.platform.Location"
     */
    public Set getDefaultLocations() {
        return defaultLocations;
    }

    public void setDefaultLocations(Set defaultLocations) {
        this.defaultLocations = defaultLocations;
    }


    /**
     * @hibernate.property
     */
    public String getPattern() {
        return pattern;
    }

    /**
     * @hibernate.set lazy="true"
     * @hibernate.collection-key column="technologyfiletype"
     * @hibernate.collection-one-to-many  class="org.setupx.repository.core.communication.technology.platform.Datafile"
     */
    public Set getDatafiles() {
        return datafiles;
    }

    /**
     * @hibernate.property
     */
    public String getLabel() {
        return label;
    }

    public void setDescribtion(String describtion) {
        this.describtion = describtion;
    }

    public void setDefaultlocation(Set locations) {
        this.defaultLocations = locations;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public void setDatafiles(Set datafiles) {
        this.datafiles = datafiles;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Set getDatafiles(long l) {
        Set set = new HashSet();

        // filter data files
        Iterator iterator = this.datafiles.iterator();
        while(iterator.hasNext()){
            Datafile datafile = (Datafile)iterator.next();
            if (datafile.getSampleID() == l){
                set.add(datafile);
            }
        }
        return set;
    }


/**
 * 
 * @param minsampleID minimum SX ID of the sample that will be checked for.
 * @return
 */
    public Collection getPossibleDatafiles(long minsampleID ) {
        Set datafiles = new HashSet();
        
        // check each location
        Iterator defaultIterator = this.getDefaultLocations().iterator();

        while (defaultIterator.hasNext()) {
            
            // open each possible location 
            Location location = (Location) defaultIterator.next();
            File directory = new File(location.getPath());
            debug("scanning: " + directory.getPath());
            
            // check for pattern // using this as filter
            TechnologyFileTypeFileFilter technologyFileTypeFileFilter = new TechnologyFileTypeFileFilter(this, minsampleID);
            File[] files = directory.listFiles(technologyFileTypeFileFilter);
            
            // return all files that do
            for (int i = 0; i < files.length; i++) {
                
                // find the sampleID
                long sampleID = technologyFileTypeFileFilter.checkForSampleID(files[i].getName());
                
                // check if the datafile does not exist yet
                String query = "select uoid from datafile where sampleID = " + sampleID + " and source = \"" + files[i].getPath() + "\" and technologyfiletype = " + this.getUOID();
                
                List elements = CoreObject.createSession().createSQLQuery(query).addScalar("uoid", Hibernate.LONG).list();
                // debug("Exists alread? Query " + query + ":     " + elements.size());
                
                boolean found = elements.size() > 0; 
                
                if (!found) {
                    debug("got match: " + files[i].getName());
                    // creating new datafile
                    // where does the techplatorm come from?
                    Datafile datafile;
                    try {
                        datafile = Datafile.create(sampleID, files[i]);
                        datafile.setTechnologyFileType(this);
                        datafiles.add(datafile);
                        
                    } catch (DatafileException e) {
                        e.printStackTrace();
                    }
                } else {
                    // debug("match exists already : " + files[i].getName());
                }
            }
        }
        return datafiles;
    }

    
    
    public Collection getPossibleDatafiles(Date start, Date end) {
        Set datafiles = new HashSet();
        
        // check each location
        Iterator defaultIterator = this.getDefaultLocations().iterator();

        while (defaultIterator.hasNext()) {
            
            // open each possible location 
            Location location = (Location) defaultIterator.next();
            File directory = new File(location.getPath());
            debug("scanning: " + directory.getPath());
            
            // check for pattern // using this as filter
            FilenameFilter filter = new FileAgeFilter(start, end);
            File[] files = directory.listFiles(filter);
            
            // return all files that do
            for (int i = 0; i < files.length; i++) {
                
                // check if the datafile does not exist yet
                String query = "select uoid from datafile where source = \"" + files[i].getPath() + "\" and technologyfiletype = " + this.getUOID();
                
                List elements = CoreObject.createSession().createSQLQuery(query).addScalar("uoid", Hibernate.LONG).list();
                // debug("Exists alread? Query " + query + ":     " + elements.size());
                
                boolean found = elements.size() > 0; 
                
                if (!found) {
                    debug("got match: " + files[i].getName());
                    // creating new datafile
                    // where does the techplatorm come from?
                    Datafile datafile;
                    try {
                        datafile = Datafile.create(0, files[i]);
                        datafile.setTechnologyFileType(this);
                        datafiles.add(datafile);
                        
                    } catch (DatafileException e) {
                        e.printStackTrace();
                    }
                } else {
                    // debug("match exists already : " + files[i].getName());
                }
            }
        }
        return datafiles;
    }
    
    
    /**
     * parent item
     *
     * @hibernate.many-to-one column="technology" class = "org.setupx.repository.core.communication.technology.platform.Technology"
     */
    public Technology getTechnology() {
        return this.technology;
    }
 
    public void setTechnology(Technology technology){
        this.technology = technology;
    }
    
    /**
     * The setter method for Unique Object identifier.
     */
    public void setUOID(long lId) {
        uoid = lId;
    }

    /**
     * The getter method for Unique Object identifier.
     * 
     * <p>
     * The id is generated by the persistence layer.
     * </p>
     *
     * @return unique id for this object
     *
     * @hibernate.id column = "uoid" generator-class="native"
     */
    public long getUOID() {
        return (uoid);
    }


    public void myUpdateMeAndChilds(Session hqlSession, boolean createIt) {
        Logger.debug(this, "contains " + this.getDatafiles().size() + " datafiles.");
        Iterator iterator = this.getDatafiles().iterator();
        while(iterator.hasNext()){
            TechnoCoreObject coreObject  = (TechnoCoreObject)iterator.next();
            coreObject.myUpdateMeAndChilds(hqlSession, createIt);
        }
        
        
        Logger.debug(this, "contains " + this.getDefaultLocations().size() + " locations.");
        iterator = this.getDefaultLocations().iterator();
        while(iterator.hasNext()){
            TechnoCoreObject coreObject  = (TechnoCoreObject)iterator.next();
            coreObject.myUpdateMeAndChilds(hqlSession, createIt);
        }
        
        super.myUpdateMeAndChilds(hqlSession, createIt);
    }
    
    public void addDefaultlocation(Location location){
        this.defaultLocations.add(location);
    }

    public Collection getDatafiles(Date start, Date end) {
        Collection datafiles = this.getDatafiles();
        // filter them
        Collection subset = new HashSet();
        Iterator iterator = datafiles.iterator();
        while (iterator.hasNext()) {
            Datafile datafile = (Datafile) iterator.next();
            if (datafile.getDateCreated().after(start) && datafile.getDateCreated().before(end)){
                subset.add(datafile);
            }
        }
        return subset;
    }
}
