package org.setupx.repository.core.communication.technology.platform.debugging;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;


/**
 * @hibernate.class table = "debug_a"
 * @hibernate.discriminator column = "discriminator"
 */
public class A extends MyObject{
    private long uoid;
    private Set b = new HashSet();
    private C c = null;
    private String comment;

    /**
     * @hibernate.property
     */
    public String getComment() {
        return comment;
    }
    
    public void setComment(String comment){
        this.comment = comment;
    }
    
    /**
     * The setter method for Unique Object identifier.
     */
    public void setUOID(long lId) {
        uoid = lId;
    }

    /**
     * The getter method for Unique Object identifier.
     * 
     * <p>
     * The id is generated by the persistence layer.
     * </p>
     *
     * @return unique id for this object
     *
     * @hibernate.id column = "uoid" generator-class="native"
     */
    public long getUOID() {
        return (uoid);
    }
        
    /**
     * @hibernate.set lazy="true"
     * @hibernate.collection-key column="parent"
     * @hibernate.collection-one-to-many  class="org.setupx.repository.core.communication.technology.platform.debugging.B"
     */    
    public Set getB() {
        return b;
    }

    /** @hibernate.many-to-one column="c" class="org.setupx.repository.core.communication.technology.platform.debugging.C" */
    public C getC() {
        return c;
    }

    public void setB(Set b) {
        this.b = b;
    }

    public void setC(C c) {
        this.c = c;
    }
    
    
    public void update(Session hqlSession, boolean createIt) {
        Transaction transaction = hqlSession.beginTransaction();

        Iterator iter = b.iterator();
        while(iter.hasNext()){

            B object = (B)iter.next();
            update(object, hqlSession, createIt);
        }

        update(this.c, hqlSession, createIt);
        update(this, hqlSession, createIt);

        transaction.commit();
        hqlSession.close();
    }

    public void save() {
        this.update(PersistenceConfiguration.createSessionFactory().openSession(), true);
    }

    public void addB(B b2) {
        this.getB().add(b2);
    }    
}


