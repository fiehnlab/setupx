/**
 * ============================================================================ File:    FileExport.java Package: org.setupx.repository.core.communication.export cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.export;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.setupx.repository.Config;
import org.setupx.repository.web.DownloadServlet;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public abstract class FileExport extends Export {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected File targetFile = null;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new FileExport object.
   *
   * @param targetFile 
   *
   * @throws FileExportException 
   */
  public FileExport(File targetFile) throws FileExportException {
    this.targetFile = targetFile;

    try {
      try {
        // check if file exists
        org.setupx.repository.core.util.File.checkFile(targetFile);
      } catch (FileNotFoundException e) {
        warning(this, "file " + targetFile.getAbsoluteFile() + " does not exist.");
        targetFile.createNewFile();
      }
    } catch (IOException e) {
      throw new FileExportException("unable to start export - file issues:", e);
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @throws FileExportException TODO
   */
  public abstract void createExportFile() throws FileExportException;

  /**
   * TODO: 
   *
   * @param l
   * @param extension TODO
   *
   * @return TODO
   */
  public static File createFileName(long l, String extension) {
    String file = Config.DIRECTORY_RELATED + File.separator + l + File.separator + "summary_" + l + "." + extension;
    debug(null, "creating a new filename: " + extension);

    return new File(file);
  }

  /**
   * create a downloadlink for this file via DownloadServlet
   *
   * @see DownloadServlet#createDownloadLink(String)
   */
  public String createDownloadLink() {
      return DownloadServlet.createDownloadLink(this.targetFile.getName(), "" + this.experiment.getUOID());
  }
}
