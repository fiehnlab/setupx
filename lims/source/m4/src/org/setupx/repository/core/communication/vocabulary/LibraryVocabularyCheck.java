/**
 * ============================================================================ File:    LibraryVocabularyCheck.java Package: org.setupx.repository.core.communication.vocabulary cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.vocabulary;

import org.setupx.repository.core.CoreObject;


/**
 * Library containing all words stored in a static library
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.9 $
 *
 * @see org.setupx.repository.core.communication.vocabulary.LocalStaticLibrary
 */
class LibraryVocabularyCheck extends CoreObject implements VocabularyCheck {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  Library library = null;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new LibraryVocabularyCheck object.
   */
  public LibraryVocabularyCheck() {
    if (ACTIVE) {
      this.library = LocalStaticLibrary.newInstance();
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.core.communication.vocabulary.VocabularyCheck#getSuggestion(java.lang.String)
   */
  public String getSuggestion(final String word) throws VocabularyCheckException {
    return library.createSuggestion(word);
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.communication.vocabulary.VocabularyCheck#check(java.lang.String)
   */
  public boolean check(final String word) throws VocabularyCheckException {
    try {
      return library.contains(word);
    } catch (Exception e) {
      throw new VocabularyCheckException(e);
    }
  }
}
