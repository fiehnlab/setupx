package org.setupx.repository.core.query.answer;


/**
 * An Experiment has been found twice for a query.
 */
public class FoundExperimentTwiceException extends Exception {

}
