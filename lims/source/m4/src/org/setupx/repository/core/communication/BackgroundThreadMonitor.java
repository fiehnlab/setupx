package org.setupx.repository.core.communication;


import java.util.Hashtable;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;


public class BackgroundThreadMonitor extends CoreObject{

  static Object thiz = new BackgroundThreadMonitor();
  static Hashtable processes = new Hashtable();

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  public static BackgroundThread getBackgroundThread(final int id) {
    try {
      return (BackgroundThread)processes.get("" + id);
    } catch (Exception e) {
      warning(thiz, "unable to find session: " + id);

      return null;
    }
  }
  
  public static Hashtable getBackgroundThreads() {
    return processes;
  }

  public static void add(BackgroundThread scanningProcess) {
    Logger.info(thiz, "starting BackgroundThread: " + Util.getClassName(scanningProcess) + " : " + scanningProcess.getName());
    processes.put("" + scanningProcess.getName(), scanningProcess);
  }
}
