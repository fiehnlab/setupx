package org.setupx.repository.core.communication.technology.platform;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.server.persistence.SXQuery;

public class TechnologyFileTypeFileFilter extends CoreObject implements FilenameFilter {

    private static boolean logging_active = false;
    
    private TechnologyFileType technologyFileType;
    private List sampleIDs;

    private Pattern pattern;

    public TechnologyFileTypeFileFilter(TechnologyFileType technologyFileType, long minsampleID) {

        // debug("init " + this.getClass().getName() + ".");
        this.technologyFileType = technologyFileType;

        String basic = "select sampleID from samples where sampleID > " + minsampleID;

        // debug("init internal list of sampleIDs.");
        Query query = CoreObject.createSession().createSQLQuery(basic).addScalar("sampleID", Hibernate.INTEGER);
        this.sampleIDs = query.setCacheable(SXQuery.caching).list();
        // debug("finished init internal list of sampleIDs : " + this.sampleIDs.size() + " entries.");
        
        // init the pattern
        this.pattern = Pattern.compile(this.technologyFileType.pattern);

    }
    /**
     * accepts all files matching the pattern and filenames containing an existing sampleID
     * 
     * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
     */
    public boolean accept(File dir, String name) {
        try {
            // check vs the pattern
            if (pattern.matcher(name.toLowerCase()).matches() || pattern.matcher(name.toUpperCase()).matches()){
                debug("OK  - Match pattern: " + this.technologyFileType.pattern);
                
                boolean sampleIDfound = (checkForSampleID(name) > 0);
                
                if (sampleIDfound){
                    debug("OK  - matches pattern and sampleID found");
                } else {
                    debug("ERR - matches pattern and sampleID found");
                }
                // matches the pattern and is sampleID is found
                return (sampleIDfound);
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public long checkForSampleID(String name) {
        // check if any if the sampleIDs are in the name
        Iterator sampleIDIterator = this.sampleIDs.iterator();
        long sampleID = -1;
        
        while (sampleIDIterator.hasNext()) {
            sampleID = Long.parseLong(sampleIDIterator.next().toString());
            
            // debug("parsing " + name + " indexOf("+ sampleID + ") :" + name.indexOf("" + sampleID));
            
            if (name.indexOf("" + sampleID) > -1) {
                // debug("Found sampleID : " + sampleID + " ");
                return sampleID;
            }
        }
        return -1;
    }
    
    
    public void debug(String string) {
        if (logging_active){
            super.debug(string);
        }
    }
}
