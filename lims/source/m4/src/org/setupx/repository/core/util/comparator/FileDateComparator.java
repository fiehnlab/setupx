/**
 * ============================================================================ File:    FileDateComparator.java Package: org.setupx.repository.core.util.comparator cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.comparator;

import java.io.File;


/**
 * compares the modification date of two files.
 * 
 * <p>
 * usage: <code>Arrays.sort(files, new FileDateComparator());</code>
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class FileDateComparator implements java.util.Comparator {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  public int compare(Object o1, Object o2) {
    return new Long(((File) o1).lastModified()).compareTo(new Long(((File) o2).lastModified()));
  }
}
