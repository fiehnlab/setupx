package org.setupx.repository.core.communication.kerberos;

import java.io.IOException;

import java.util.Locale;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.LanguageCallback;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.TextOutputCallback;
import javax.security.auth.callback.UnsupportedCallbackException;


/**
 * checks a password - username combination on the kerberosserver
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
class PlainCallbackHandler implements CallbackHandler {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String password;
  private String username;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new PlainCallbackHandler object.
   */
  public PlainCallbackHandler() {
    throw new UnsupportedClassVersionError();
  }

  /**
   * Creates a new PlainCallbackHandler object.
   *
   * @param username 
   * @param password 
   */
  public PlainCallbackHandler(String username, String password) {
    super();
    this.username = username;
    this.password = password;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param callbacks TODO
   *
   * @throws IOException TODO
   * @throws UnsupportedCallbackException TODO
   */
  public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
    for (int i = 0; i < callbacks.length; i++) {
      if (callbacks[i] instanceof TextOutputCallback) {
        // This callback delivers messages from a login module
        TextOutputCallback toCb = (TextOutputCallback) callbacks[i];

        // Get message
        String msg = toCb.getMessage();

        // Get message type
        switch (toCb.getMessageType()) {
        case TextOutputCallback.INFORMATION:
          break;

        case TextOutputCallback.ERROR:
          break;

        case TextOutputCallback.WARNING:
          break;
        }

        // Display message to the user ...
      } else if (callbacks[i] instanceof NameCallback) {
        // A login module is requesting the name of the user
        NameCallback nCb = (NameCallback) callbacks[i];

        // Get the prompt to display to the user
        String prompt = nCb.getPrompt();

        // Set the name
        nCb.setName(username);
      } else if (callbacks[i] instanceof PasswordCallback) {
        // A login module is requesting the user's password
        PasswordCallback pCb = (PasswordCallback) callbacks[i];

        // Get the prompt to display to the user
        String prompt = pCb.getPrompt();

        // Set the password
        pCb.setPassword(password.toCharArray());
      } else if (callbacks[i] instanceof LanguageCallback) {
        // A login module is requesting the user's locale
        LanguageCallback lCb = (LanguageCallback) callbacks[i];

        // Set the locale
        lCb.setLocale(Locale.US);
      } else {
        throw new UnsupportedCallbackException(callbacks[i], "Unrecognized Callback");
      }
    }
  }
}
