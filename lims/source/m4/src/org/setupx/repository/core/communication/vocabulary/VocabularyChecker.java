/**
 * ============================================================================ File:    VocabularyChecker.java Package: org.setupx.repository.core.communication.vocabulary cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.vocabulary;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.File;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;

import java.net.URL;

import java.util.Vector;


/**
 * meta checker for a singel word. The word is validated by all registrated checkers. The only method is <code>check(String)</code>  that checks a single word for its existense.
 * 
 * <p>
 * all the dictionaries are loaded dynamicly using <code>reflection</code> - so avoid using to often
 * </p>
 * <pre>TODO : check the check4suggestion method with only one library</pre>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.12 $
 *
 * @see VocabularyChecker#check(String)
 */
public class VocabularyChecker extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static Object thiZ = new VocabularyChecker();
  public static final int NCBI = 4;

  /**
   * @label registrated vocabulary checks
   */
  private static VocabularyCheck[] checks = new VocabularyCheck[0];
  private static String[] noStringArray = {  };

  /**
   * @label library to store the new added words at
   */
  private static LocalLibrary localLibrary = LocalLibrary.instance();

  static {
    registerChecks();
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private VocabularyChecker() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param word TODO
   * @param check TODO
   *
   * @return TODO
   */
  public static boolean check4existence(String word, VocabularyCheck check) {
    try {
      if (check.check(word)) {
        // add this word to local dictionary
        localLibrary.add(word);

        return true;
      }
    } catch (VocabularyCheckException e) {
      warning(thiZ, e.toString());
    }

    return false;
  }

  /**
   * TODO: 
   *
   * @param string TODO
   *
   * @return TODO
   */
  public static boolean check4existence(String string) {
    log(thiZ, "---- check4existence on vocabularies ------");

    for (int i = 0; i < checks.length; i++) {
      VocabularyCheck check = checks[i];
      log(thiZ, " |- checking " + Util.getClassName(check));

      try {
        if (check.check(string)) {
          return true;
        }
      } catch (Exception e) {
        warning(null, e.toString());
      }
    }

    return false;
  }

  /**
   * checks if the word exists in a given libray
   *
   * @param word the word that will be checked
   * @param libraryID the id of the library that is used for the validation
   *
   * @return true if known
   *
   * @throws VocabularyCheckException TODO
   */
  public static boolean check4existence(final String word, final String libraryID)
    throws VocabularyCheckException {
    VocabularyCheck check = getVocCheck(libraryID);

    return check.check(word);
  }

  /**
   * checks a word in all available checks. if it is unknown an array of suggestions is returned
   * <pre>deactivated previois checkgin inside the method itself</pre>
   *
   * @param word the word that is checked
   *
   * @return an array of Strings that contains suggestions. The number of suggestions depends on the number of checkers and <b>if</b> the checker found a suggestion
   */
  public static String[] check4suggestion(final String word) {
    Vector suggestions = new Vector();

    // format word
    format(word);

    // before starting to look for suggestions just check if any of them knows the word
    /* deactivated for performance
       for (int i = 0; i < checks.length; i++) {
         VocabularyCheck check = checks[i];
         check4existence(word, check);
       }
     */
    // take all checks
    log(thiZ, "---- check4suggestion on vocabularies ------");

    for (int i = 0; i < checks.length; i++) {
      VocabularyCheck check = checks[i];
      log(thiZ, " |- checking " + Util.getClassName(check));

      try {
        String vocCheck = check.getSuggestion(word);

        // get rid of empty values
        debug(thiZ, "result from: " + Util.getClassName(check) + " --> " + vocCheck);

        if ((vocCheck != null) && (vocCheck.length() > 1) && !suggestions.contains(vocCheck)) {
          suggestions.add(vocCheck);
        }
      } catch (NullPointerException e) {
        warning(thiZ, "unable to check a word using checker: " + Util.getClassName(check) + "  cause: " + e);
      } catch (VocabularyCheckException e) {
        warning(thiZ, "unable to check a word using vocchecker: " + Util.getClassName(check) + "  cause: " + e);
      }
    }

    String[] result = Util.convert2StringArr(suggestions);

    // each word of the suggestions is automaticly added to the local dictionary
    for (int i = 0; i < result.length; i++) {
      // debug(thiZ, "result " + i + " : " + result[i] + " " + word);
      localLibrary.add(result[i]);
    }

    debug(thiZ, "final result size: " + result.length);

    return result;
  }

  /**
   * same method like <code>check4suggestion(String)</code> but checks only on <b>the given library</b>.
   *
   * @param entry the entry which will be checked
   * @param string the id of the database
   *
   * @return an array containing <b>one</b> entry
   *
   * @throws VocabularyCheckException
   *
   * @see VocabularyChecker#check4suggestion(String)
   */
  public static String[] check4suggestion(String entry, String id) {
    try {
      return new String[] { getVocCheck(id).getSuggestion(entry) };
    } catch (NullPointerException e) {
      warning(thiZ, "unable to check a word using checker: " + id + "  cause: " + e);
    } catch (VocabularyCheckException e) {
      warning(thiZ, "unable to check a word using vocchecker: " + id + "  cause: " + e);
    }

    return null;
  }

  //   D E B U G G I N G
  public static void main(final String[] args) {
    /*debug(thiZ, VocabularyChecker.check("thecoglossate"));
       debug(thiZ, VocabularyChecker.check("Braunschweig"));
       debug(thiZ, VocabularyChecker.check("Braunbaer"));
       debug(thiZ, VocabularyChecker.check("Staatsexamen"));
       VocabularyChecker.check("pentaerythritol");
       VocabularyChecker.check("Scholz");
       VocabularyChecker.check("Braunschweig");
       VocabularyChecker.check("Nomadidae");
       VocabularyChecker.check("ultrabay");
     */
    VocabularyChecker.check4suggestion("Braunschwei");
    VocabularyChecker.check4suggestion("Braunschweyg");
  }

  /**
   * @param libraryID the classname of the library you want to test with
   *
   * @return
   *
   * @throws VocabularyCheckException
   */
  private static VocabularyCheck getVocCheck(String libraryID)
    throws VocabularyCheckException {
    for (int i = 0; i < checks.length; i++) {
      VocabularyCheck check = checks[i];

      if (Util.getClassName(check).compareTo(libraryID) == 0) {
        return check;
      }
    }

    throw new VocabularyCheckException("unable to find the requiered voccehcker for the the libraryID : " + libraryID);
  }

  /**
   * find all implementations of VocabularyCheck inside the same package
   * <pre>FIXME it doesent work on the jboss ....</pre>
   *
   * @return an array of all classes that are in the same package like VocabularyCheck
   *
   * @see VocabularyCheck
   */
  private static Class[] findImplementations() {
    boolean WORKING_ON_JBOSS = false;

    if (WORKING_ON_JBOSS) {
      Class[] classes = new Class[3];
      classes[0] = LibraryLocalVocabularyCheck.class;
      classes[1] = LibraryVocabularyCheck.class;
      classes[2] = LibraryNCBICheck.class;

      //classes[0] = null; //GoogleCheck.class;
      //classes[1] = null; //DictWebServiceCheck.class;
      return classes;
    }

    Package packagge = VocabularyCheck.class.getPackage();
    String name = packagge.getName();
    debug(null, "looking for instances of " + VocabularyCheck.class.getName() + " in package: " + name);

    // vector containing all classes that match
    Vector classes = new Vector();
    String packageName = name;

    if (!name.startsWith("/")) {
      name = "/" + name;
    }

    name = name.replace('.', '/');

    // Get a File object for the package
    URL url = File.class.getResource(name);

    if (url == null) {
      throw new RuntimeException("unable to create an url for Packagename: " + name);
    }

    java.io.File directory = new java.io.File(url.getFile());
    debug(null, directory.getAbsolutePath());

    if (directory.exists()) {
      // Get the list of the files contained in the package
      String[] files = directory.list();

      for (int i = 0; i < files.length; i++) {
        // we are only interested in .class files
        if (files[i].endsWith(".class")) {
          // removes the .class extension
          String classname = files[i].substring(0, files[i].length() - 6);

          try {
            Class myClass = Class.forName(packageName + "." + classname);

            //debug(thiZ, "working on " + myClass.getName());
            // Try to create an instance of the object
            Object o = myClass.newInstance();

            if (o instanceof VocabularyCheck) {
              debug(thiZ, "found derivate of " + VocabularyCheck.class.getName() + " called:  " + myClass);
              classes.add(myClass);
            }
          } catch (ClassNotFoundException cnfex) {
            System.err.println(cnfex);
            Logger.warning(thiZ, "unable to instanciate " + classname + "  ", cnfex);
          } catch (InstantiationException iex) {
            // We try to instanciate an interface
            // or an object that does not have a
            // default constructor
            Logger.warning(thiZ, "unable to instanciate " + classname + "  ", iex);
          } catch (IllegalAccessException iaex) {
            // The class is not public
            Logger.warning(thiZ, "unable to instanciate " + classname + "  ", iaex);
          } catch (Throwable iaex) {
            // The class is not public
            Logger.warning(thiZ, "unable to instanciate " + classname + "  ", iaex);
          }
        }
      }
    }

    Class[] result = new Class[classes.size()];

    for (int i = 0; i < classes.size(); i++) {
      Class clazz = (Class) classes.get(i);
      result[i] = clazz;
    }

    return result;
  }

  /**
   * TODO: 
   *
   * @param word TODO
   */
  private static void format(String word) {
    // remove every space
  }

  /**
   * registrates all checks, so that they can be used in the checks.
   * <pre>if there is an instance of LibraryLocalVocabularyCheck it will <b>allways</b> be placed at the first position of the checks</pre>
   *
   * @param check the check that will be registered
   */
  private static void registerCheck(final VocabularyCheck check) {
    debug(thiZ, "REG: registrating: " + Util.getClassName(check));

    try {
      VocabularyCheck[] oldChecks = checks;
      VocabularyCheck[] newChecks = new VocabularyCheck[oldChecks.length + 1];
      debug(thiZ, "expanding array from size: " + oldChecks.length + "  to new size: " + newChecks.length);

      // expand the array
      int i;

      for (i = 0; i < oldChecks.length; i++) {
        newChecks[i] = oldChecks[i];
      }

      // attatch the new field at the end
      newChecks[i] = check;
      checks = newChecks;
    } catch (Exception e) {
      Logger.warning(thiZ, "unable to register " + Util.getClassName(check) + " cause: " + e);
    }

    // if there is an instance of the Locallibrary inside - bring it to first position
    for (int i = 0; i < checks.length; i++) {
      VocabularyCheck tempCheck = checks[i];

      if (tempCheck instanceof LibraryLocalVocabularyCheck && (i != 0)) {
        // flipping positions
        VocabularyCheck a = checks[0];
        checks[0] = tempCheck;
        checks[i] = a;
      }
    }
  }

  /**
   * registrates all instances of VocabularyChecker in the same package as VocabularyChecker.
   * <pre>The VocabularyCheckes must all implement the <code>interface <B>VocabularyChecker</B></code></pre>
   *
   * @see VocabularyChecker#findImplementations()
   * @see VocabularyCheck
   */
  private static void registerChecks() {
    debug(thiZ, "registrating the checks.");

    Class[] classes = null;
    classes = VocabularyChecker.findImplementations();

    debug(thiZ, "found " + classes.length + " implementations of the " + VocabularyChecker.class.getName());

    // create instances these classes
    for (int i = 0; i < classes.length; i++) {
      try {
        registerCheck((VocabularyCheck) classes[i].newInstance());
      } catch (IllegalAccessException e) {
        throw new RuntimeException("unable to init all classes ", e);
      } catch (InstantiationException e) {
        warning(thiZ, e.toString());
        //throw new RuntimeException("unable to init all classes ", e);
      } catch (NullPointerException e) {
        throw new RuntimeException("unable to init all classes ", e);
      }
    }

    log(thiZ, "---- init vocabularies ------");
    log(thiZ, "the following vocabulary checks have been initalized: ");

    for (int i = 0; i < checks.length; i++) {
      log(thiZ, "  - " + i + ":" + Util.getClassName(checks[i]));
    }

    log(thiZ, "-----------------------------");
  }
}
