package org.setupx.repository.core.communication.status;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.hibernate.Session;

import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;

public class FilecreatedStatus extends DynamicStatus implements Timestamped{
    public static String[] endings = new String[]{"peg","smp", "txt", "cdf"};  
    public static String[] paths = new String[]{"/mnt/gctof/ucdavis/data", "/mnt/gctof/ucdavis/data", "/mnt/binbase/data","/mnt/binbase/data"};
    private File file;
    private String fileExtension; 

    public FilecreatedStatus(long sampleUOID, String fileExtension, File file) {
        super(sampleUOID);
        this.file = file;
        this.fileExtension = fileExtension;
    }
    
    public Date getDate() {
        return new Date(file.lastModified());
    }
    
    public String createDisplayString() {
        return fileExtension.toUpperCase() + "-File created.";
    }

    public String getMessage() {
        return file.getName() + " was created. It is stored at " + file.getPath();
    }

    public static Collection find(long sampleUOID, Session hqlSession) {
        Vector vector = new Vector();
        String _name;
        try {
            _name = new SXQuery().findAcquisitionNameBySampleID(sampleUOID);
        } catch (PersistenceActionFindException e) {
            e.printStackTrace();
            return new Vector();
        }

        File file = null;
        boolean fileExists = false;
        
        // take each extension
        for (int i = 0; i < endings.length; i++) {
        // check if file exists
            String ending = endings[i];
            String path = paths[i];

            _name = _name.replace(':','_');
            
            file = new File(path + File.separator + _name + "." + ending.toLowerCase());
            
            if (!file.exists()){
                file = new File(path + File.separator + _name + "." + ending.toUpperCase());
            }
            
            if (file.exists()){
                // create a new event 
                
                // add it.
                vector.add(new FilecreatedStatus(sampleUOID, ending.toLowerCase(), file));
            }
            
            
        }
        return vector;
    }
}
