/**
 * ============================================================================ File:    Acquisition.java Package: org.setupx.repository.core.communication.exporting cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.exporting;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample;
import org.setupx.repository.core.communication.exporting.sampletypes.Blank;
import org.setupx.repository.core.communication.exporting.sampletypes.MethodBlank;
import org.setupx.repository.core.communication.exporting.sampletypes.QC;
import org.setupx.repository.core.communication.exporting.sampletypes.RegionBlank;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;

import java.util.Vector;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class Acquisition extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public AcquisitionSample[] samples = null;

  //private Randomisation randomisation = null;
  private AcquisitionFile acquisitionFile;
  private AcquisitionVisualistation visualistation = null;
  private JobInformation jobInformation;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected Acquisition(long[] sampleIDs, JobInformation jobInformation)
    throws PersistenceActionUpdateException, MappingError, ExportException {
    this.init(AcquisitionSample.map(sampleIDs, jobInformation), jobInformation);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * converts from AcquisitionSample[] to Vector
   */
  public static Vector convert(AcquisitionSample[] samples2) {
    Vector vec = new Vector();

    for (int i = 0; i < samples2.length; i++) {
      vec.add(samples2[i]);
    }

    return vec;
  }

  /**
   * convert from Vector to AcquisitionSample[]
   */
  public static AcquisitionSample[] convert(Vector samplesVec) {
    AcquisitionSample[] result = new AcquisitionSample[samplesVec.size()];

    for (int i = 0; i < samplesVec.size(); i++) {
      result[i] = (AcquisitionSample) samplesVec.get(i);
    }

    return result;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public JobInformation getJobInformation() {
    return jobInformation;
  }

  /**
   * create a createAcquisitionFile for this Acquisition
   *
   * @return the new AcquisitionFile
   *
   * @throws UnsupportedMachine if the machine given in the Jobinformation can not be found
   */
  public AcquisitionFile createAcquisitionFile() throws UnsupportedMachine {
    if (acquisitionFile == null) {
      acquisitionFile = new AcquisitionFile(this, this.jobInformation.getMachine());
    }

    return acquisitionFile;
  }

  /**
   * create a AcquisitionVisualistation for this Acquisition
   *
   * @return the new AcquisitionVisualistation
   *
   * @throws AcquisitionVisualistationException
   */
  public AcquisitionVisualistation createAcquisitionVisualistation()
    throws AcquisitionVisualistationException {
    if (this.visualistation == null) {
      this.visualistation = AcquisitionVisualistation.newInstance(this);
    }

    return this.visualistation;
  }

  /**
   * show the Acquistition
   */
  public void showAcquisition() {
    // check each sample
    for (int i = 0; i < this.samples.length; i++) {
      debug("pos of sample " + i + " : " + this.samples[i]);
    }
  }

  /**
   * init the Acquisition
   * 
   * <ol>
   * <li>
   * adding quality controles
   * </li>
   * <li>
   * adding Method Blanks
   * </li>
   * <li>
   * adding Region Blanks
   * </li>
   * <li>
   * adding a Blank for every 10 Samples
   * </li>
   * <li>
   * rearanging the samples on the Tray
   * </li>
   * <li>
   * rearanging the sequence of the samples in the Acquisition itself
   * </li>
   * <li>
   * randomisation of the samples
   * </li>
   * <li>
   * relabeling the samples
   * </li>
   * </ol>
   * 
   * <p>
   * <B>most of the parameters who the different jobs (random,...) are done, are all defined in the jobinformation</b>
   * </p>
   * 
   * <p><b>Modification:</b>
   * may1st_2007: additinal blank and qc added after every 10 runs.

   * </p>
   *
   * @param jobInformation information how to process the samples
   * @param i_acquisitionSamples the samples that will be exported in this acquisition
   *
   * @throws ExportException
   * @throws PersistenceActionUpdateException
   * @throws ParameterException
   */
  private final void init(AcquisitionSample[] i_acquisitionSamples, JobInformation jobInformation)
    throws ExportException, PersistenceActionUpdateException {
    log(this, "init");
    this.samples = i_acquisitionSamples;
    this.jobInformation = jobInformation;

    // add qc
    addSample(new QC("qc5", this.jobInformation));
    addSample(new QC("qc10", this.jobInformation));
    addSample(new QC("qc30", this.jobInformation));
    addSample(new QC("qc50", this.jobInformation));
    addSample(new QC("qc100", this.jobInformation));

    // add method blank
    addSample(new MethodBlank(this.jobInformation));

    // add region blank
    addSample(new RegionBlank(this.jobInformation));

    // add blank !! one for each 10 samples !!

    int numberOfBlanks = determineBlanks();
    for (int i = 0; i < numberOfBlanks; i++) {
      addSample(new Blank(this.jobInformation));
    }

    //int numOfNonSamples = this.countNonSamples();

    boolean inclSamples;

    if (numberOfBlanks >= this.jobInformation.getVialPositionOffset()) {
      // meaning that the first sample would be on a position that is filled with on of
      // the blanks, ...
      // so rearrange the samples too.
      inclSamples = true;
    } else {
      inclSamples = false;
    }

    // HOTFIX:
    debug("HOTFIX inclSamples:" + numberOfBlanks + "   " + this.jobInformation.getVialPositionOffset());
    inclSamples = true;

    try {
      debug("before doArrangement()");
      this.showAcquisition();

      // rearrange
      this.doArrangement(inclSamples);
      debug("after doArrangement()");
      this.showAcquisition();

      // reorder the schedule
      this.doResequence();
      debug("after doResequence()");
      this.showAcquisition();

      // randomise Samples
      this.doRandomasition();
      debug("after doRandomasition()");
      this.showAcquisition();

      // reorder the schedule - due to the fact that the randomisation is changing vials too - the sequence has to be resequenced
      this.doVialCheck();
      debug("after doVialCheck()");
      this.showAcquisition();

      // check on the last vial
      this.doCheckTrayLimit(98);
      this.showAcquisition();

      // relabel them
      this.doReLabel();
      debug("after doReLabel()");
      this.showAcquisition();

      // done - connect to the real samples
      this.doConnectAllSamples();
    } catch (ParameterException e) {
      throw new MappingError(e);
    } catch (PersistenceActionException e) {
      throw new MappingError(e);
    }
  }

  /**
   * adds a single sample to the set of samples
   *
   * @param newSample the new Samples
   */
  private void addSample(AcquisitionSample newSample) {
    AcquisitionSample[] acquisitionSamples = new AcquisitionSample[samples.length + 1];

    for (int i = 0; i < this.samples.length; i++) {
      AcquisitionSample acquisitionSample = this.samples[i];
      acquisitionSamples[i] = acquisitionSample;
    }

    acquisitionSamples[acquisitionSamples.length - 1] = newSample;
    this.samples = acquisitionSamples;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  private int countNonSamples() {
    int nonSamples = 0;

    for (int i = 0; i < this.samples.length; i++) {
      if (this.samples[i].positionImportance != AcquisitionSample.POSITIONIMPORTANCE_SAMPLE_DEFAULT) {
        nonSamples++;
      }
    }

    return nonSamples;
  }

  /**
   * craete a subset of acqusitionsamples
   *
   * @param samples2
   * @param i
   * @param j
   *
   * @return new subset
   */
  private AcquisitionSample[] createSubset(AcquisitionSample[] samples2, int start, int end) {
    AcquisitionSample[] subset = new AcquisitionSample[start - end];

    int j = 0;

    for (int i = start; i < end; i++) {
      subset[j] = samples2[i];
    }

    return subset;
  }

  /**
   * calculates the requiered number of Blanks <br> normal: 1 per 10 samples  <br> modification: when there are no sourouding blanks - then there will be just a single blank
   *
   * @return
   */
  private int determineBlanks() {
    // when there are no sourouding blanks - then there will be just a single blank
    if (!this.getJobInformation().isSurroundingBlanks()) {
      return 1;
    }

    int factor = 10;
    int numberCal = Math.round(this.samples.length / factor);

    if ((numberCal * factor) != this.samples.length) {
      numberCal++;
    }

    return numberCal;
  }

  /**
   * rearagnges all Vials on the Tray.  Only Non-Samples (sample.importance less then MAX_IMPORTANCE) are beeing rearranged.
   *
   * @param inclSamples
   *
   * @throws ParameterException
   */
  private void doArrangement(boolean inclSamples) throws ParameterException {
    int currentPosition = 0;

    // arrange 
    int limitOfPositonImportance = AcquisitionSample.MAX_IMPORTANCE;

    // in case samples will be repositioned too, then the limit of importance is set to default sample_importance
    if (inclSamples) {
      limitOfPositonImportance = AcquisitionSample.POSITIONIMPORTANCE_SAMPLE_DEFAULT;
    }

    // i is representing the importance of each sample on the plate. 
    // If importance is low - sample will get positon at the start of the vial.
    for (int i = 0; i < limitOfPositonImportance; i++) {
      for (int j = 0; j < this.samples.length; j++) {
        AcquisitionSample sample = this.samples[j];

        if (sample.positionImportance == i) {
          currentPosition = currentPosition + 1;
          sample.addParameter(new AcquisitionParameter(AcquisitionParameter.VIAL, "" + currentPosition));
        }
      }
    }
  }

  /**
   * check that non of the samples is outside the tray - in case a samples is beeing placed at a spot that is not within the limit of the tray it will be removed from the acquisition.
   *
   * @param maxPosOnTray last possible position on the tray.
   */
  private void doCheckTrayLimit(final int maxPosOnTray) {
    for (int j = 0; j < this.samples.length; j++) {
      AcquisitionSample acquisitionSample = this.samples[j];

      try {
        if (Integer.parseInt(acquisitionSample.getParamter(AcquisitionParameter.VIAL).getValue()) > maxPosOnTray) {
          // the samples is outside the tray
          // take all samples that are below this and create a new set out of them.
          AcquisitionSample[] subSetAcquisitionSamples = new AcquisitionSample[j - 1];
          this.samples = createSubset(this.samples, 0, j - 1);
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * @throws PersistenceActionUpdateException
   * @throws PersistenceActionFindException
   *
   * @see AcquisitionSample#connectToSample()
   */
  private void doConnectAllSamples() throws PersistenceActionException {
    for (int i = 0; i < this.samples.length; i++) {
      this.samples[i].connectToSample();
    }
  }

  /**
   * randomise samples - specificly for the settings in the jobinformation(machine, ...)
   */
  private void doRandomasition() {
    // find SOP for randomisation
    samples = this.jobInformation.getMachine().randomise(this.samples, jobInformation);
  }

  /**
   * relabels all samples
   */
  private void doReLabel() {
    for (int i = 0; i < samples.length; i++) {
      try {
        this.samples[i].addParameter(new AcquisitionParameter(AcquisitionParameter.NAME, this.samples[i].generateName(jobInformation)));
      } catch (ParameterException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * changes the sequence which is used for this run
   */
  private void doResequence() {
    int currentPosition = 0;
    AcquisitionSample[] samplesResequenced = new AcquisitionSample[this.samples.length];

    // arrange 
    for (int i = 0; i < AcquisitionSample.MAX_VIAL_POS; i++) {
      // loop over all samples and compare them to the current   
      for (int j = 0; j < this.samples.length; j++) {
        AcquisitionSample sample = this.samples[j];

        if (sample.sequenceImportance == i) {
          samplesResequenced[currentPosition] = sample;
          currentPosition = currentPosition + 1;
        }
      }
    }

    this.samples = samplesResequenced;
  }

  /**
   * checks if the sequence has only vials in the correct order. 1,2,3,4,... if the sequence can contain samples on randomised spots they can be changed.
   *
   * @throws ParameterException
   * @throws NumberFormatException
   */
  private void doVialCheck() throws NumberFormatException, ParameterException {
    int vialPos = this.jobInformation.getVialPositionOffset();

    for (int i = 0; i < this.samples.length; i++) {
      // only move the samples - all others stay where thay are
      if (this.samples[i].typeShortcut.compareTo(AcquisitionSample.AcquisitionLabel) == 0) {
        // check each sample and reset the vial 
        int vial = Integer.parseInt(this.samples[i].getParamter(AcquisitionParameter.VIAL).getValue());
        Logger.log(this, "changing vialposition " + vial + " to " + vialPos + " for sample " + this.samples[i].getParamter(AcquisitionParameter.NAME).getValue());
        this.samples[i].addParameter(new AcquisitionParameter(AcquisitionParameter.VIAL, "" + vialPos));
        vialPos++;
      }
    }
  }
}
