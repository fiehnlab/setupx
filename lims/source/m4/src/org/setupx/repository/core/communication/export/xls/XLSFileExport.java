/**
 * ============================================================================ File:    XLSFileExport.java Package: org.setupx.repository.core.communication.export.xls cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.export.xls;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.setupx.repository.core.communication.export.Entry;
import org.setupx.repository.core.communication.export.ExportPart;
import org.setupx.repository.core.communication.export.FileExport;
import org.setupx.repository.core.communication.export.FileExportException;
import org.setupx.repository.core.util.logging.Logger;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class XLSFileExport extends FileExport {
    //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private HSSFCellStyle cellStyleHeader;
    private HSSFCellStyle cellStyleRegular;
    private HSSFFont f;
    private HSSFWorkbook workbook;

    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new XLSFileExport object.
     *
     * @param targetFile 
     *
     * @throws FileExportException 
     */
    public XLSFileExport(File targetFile) throws FileExportException {
        super(targetFile);
        workbook = new HSSFWorkbook();

        f = this.workbook.createFont();

        //        make it blue
        f.setColor((short) f.COLOR_RED);

        //         make it bold
        //        arial is the default font
        f.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        f.setFontHeightInPoints((short) 15);
        

        cellStyleHeader = workbook.createCellStyle();
        cellStyleHeader.setFillPattern((short) HSSFCellStyle.SOLID_FOREGROUND);
        cellStyleHeader.setFont(f);
        cellStyleHeader.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        cellStyleRegular = workbook.createCellStyle();
        cellStyleRegular.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        //cellStyleRegular.setFillPattern((short) HSSFCellStyle.SOLID_FOREGROUND);
        //cellStyleRegular.setFont(f);

        // turn 90Deg
        short angle = 90;
        cellStyleHeader.setRotation(angle);
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* (non-Javadoc)
     * @see org.setupx.repository.core.communication.export.FileExport#createExportFile()
     */
    public void createExportFile() throws FileExportException {
        this.gatherExportInformation();

        Iterator iterator = this.exportParts.iterator();

        while (iterator.hasNext()) {
            ExportPart element = (ExportPart) iterator.next();

            if (element.getLines().size() != 0) {
                createSheet(element);
            }
        }

        try {
            FileOutputStream out = new FileOutputStream(this.targetFile);
            workbook.write(out);
            out.close();
            debug("worte export to " + this.targetFile.getName());
        } catch (IOException e) {
            throw new FileExportException(e);
        }
    }

    /**
     * TODO: 
     *
     * @throws IOException TODO
     */
    private void asdf() throws IOException {
        int rownum;

        //      create a new file
        FileOutputStream out = new FileOutputStream(this.targetFile);

        //         create a new workbook
        HSSFWorkbook wb = new HSSFWorkbook();

        //         create a new sheet
        HSSFSheet s = wb.createSheet();

        //         declare a row object reference
        HSSFRow r = null;

        //         declare a cell object reference
        HSSFCell c = null;

        //         create 3 cell styles
        HSSFCellStyle cs = wb.createCellStyle();
        HSSFCellStyle cs2 = wb.createCellStyle();
        HSSFCellStyle cs3 = wb.createCellStyle();
        HSSFDataFormat df = wb.createDataFormat();

        //         create 2 fonts objects
        HSSFFont f = wb.createFont();
        HSSFFont f2 = wb.createFont();

        //        set font 1 to 12 point type
        f.setFontHeightInPoints((short) 12);

        //        make it blue
        f.setColor((short) 0xc);

        //         make it bold
        //        arial is the default font
        f.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

        //        set font 2 to 10 point type
        f2.setFontHeightInPoints((short) 10);
        //        make it red
        f2.setColor((short) HSSFFont.COLOR_RED);
        //        make it bold
        f2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

        f2.setStrikeout(true);

        //        set cell stlye
        cs.setFont(f);
        //        set the cell format 
        cs.setDataFormat(df.getFormat("#,##0.0"));

        //        set a thin border
        cs2.setBorderBottom(cs2.BORDER_THIN);
        //        fill w fg fill color
        cs2.setFillPattern((short) HSSFCellStyle.SOLID_FOREGROUND);
        //        set the cell format to text see HSSFDataFormat for a full list
        cs2.setDataFormat(HSSFDataFormat.getBuiltinFormat("text"));

        //         set the font
        cs2.setFont(f2);

        //         set the sheet name in Unicode
        wb.setSheetName(0, "\u0422\u0435\u0441\u0442\u043E\u0432\u0430\u044F " + "\u0421\u0442\u0440\u0430\u043D\u0438\u0447\u043A\u0430", HSSFWorkbook.ENCODING_UTF_16);

        //         in case of compressed Unicode
        //         wb.setSheetName(0, "HSSF Test", HSSFWorkbook.ENCODING_COMPRESSED_UNICODE );
        //         create a sheet with 30 rows (0-29)
        for (rownum = (short) 0; rownum < 30; rownum++) {
            // create a row
            r = s.createRow(rownum);

            // on every other row
            if ((rownum % 2) == 0) {
                // make the row height bigger  (in twips - 1/20 of a point)
                r.setHeight((short) 0x249);
            }

            //r.setRowNum(( short ) rownum);
            // create 10 cells (0-9) (the += 2 becomes apparent later
            for (short cellnum = (short) 0; cellnum < 10; cellnum += 2) {
                // create a numeric cell
                c = r.createCell(cellnum);

                // create a string cell (see why += 2 in the
                c = r.createCell((short) (cellnum + 1));

                // on every other row
                if ((rownum % 2) == 0) {
                    // set this cell to the first cell style we defined
                    c.setCellStyle(cs);
                    // set the cell's string value to "Test"
                    c.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                    c.setCellValue("Test");
                } else {
                    c.setCellStyle(cs2);
                    // set the cell's string value to "\u0422\u0435\u0441\u0442"
                    c.setEncoding(HSSFCell.ENCODING_UTF_16);
                    c.setCellValue("\u0422\u0435\u0441\u0442");
                }

                // make this column a bit wider
                s.setColumnWidth((short) (cellnum + 1), (short) ((50 * 8) / ((double) 1 / 20)));
            }
        }

        //        draw a thick black border on the row at the bottom using BLANKS
        //         advance 2 rows
        rownum++;
        rownum++;

        r = s.createRow(rownum);

        //         define the third style to be the default
        //         except with a thick black border at the bottom
        cs3.setBorderBottom(cs3.BORDER_THICK);

        //        create 50 cells
        for (short cellnum = (short) 0; cellnum < 50; cellnum++) {
            //create a blank type cell (no value)
            c = r.createCell(cellnum);
            // set it to the thick black border style
            c.setCellStyle(cs3);
        }

        //        end draw thick black border
        //         demonstrate adding/naming and deleting a sheet
        //         create a sheet, set its title then delete it
        s = wb.createSheet();
        wb.setSheetName(1, "DeletedSheet");
        wb.removeSheetAt(1);
        //        end deleted sheet
        //         write the workbook to the output stream
        //         close our file (don't blow out our file handles
        wb.write(out);
        out.close();
    }

    /**
     * @param element
     */
    private void createSheet(ExportPart element) {
        debug("exporting sheet: " + element.getLabel() + "   lines: " + element.getLines().size());

        HSSFSheet sheet = workbook.createSheet(element.getLabel());

        Iterator lines = element.getLines().iterator();

        // create rows and labels 
        int lineNumber = 1;

        while (lines.hasNext()) {
            short x = 0;

            debug("line: " + lineNumber);

            Vector line = (Vector) lines.next();
            HSSFRow row = sheet.createRow(lineNumber);

            for (int i = 0; i < line.size(); i++) {
                Entry entry = (Entry) line.get(i);

                // while (keys.hasMoreElements()) {
                // first line - create a header
                if (lineNumber == 1) {
                    debug("creating label " + x + " " + entry.key);

                    // creaing first row - meaning labels
                    HSSFRow lables;
                    try {
                        lables = sheet.getRow(0);
                        if (lables == null){
                            throw new Exception("lables are still null.");
                        }
                    } catch (Exception e) {
                        lables = sheet.createRow(0);
                    }

                    // creating labels
                    HSSFCell cell = lables.createCell(x);
                    cell.setCellValue(entry.key);
                    
                    short neededHight = 200;
                    // short neededHight = (short) (entry.key.length() * 92);
                    if (lables.getHeight() < neededHight){
                        lables.setHeightInPoints((short) neededHight);
                    }
                    
                    // set font 1 to 12 point type
                    //lables.setHeightInPoints((short) 40);
                    
                    cell.setCellStyle(cellStyleHeader);
                }

                //HSSFCell cell = row.createCell(x);
                // NEW:
                // changed to match all attributes of one type in the same column
                short colPos = findCol(sheet.getRow(0), entry.key);

                HSSFCell cell;
                if (colPos < 0) {
                    warning(new Throwable("Problem: the key:" + entry.key + " can not be found in the header column. "));

                    // in case it is not found - take the default column
                    cell = row.createCell(x);
                } else {
                    cell = row.createCell(colPos);
                }

                try {
                    // parsing for different types
                    int valueInt = Integer.parseInt(entry.value);
                    cell.setCellValue(valueInt);
                    cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
                } catch (Exception e) {
                    cell.setCellValue(entry.value);
                    cell.setCellType(HSSFCell.CELL_TYPE_STRING);
                }

                cell.setCellStyle(cellStyleRegular);

                // adopt the width
                short size = sheet.getColumnWidth(x);

                short neededSize;

                switch (cell.getCellType()) {
                case HSSFCell.CELL_TYPE_NUMERIC:
                    neededSize = (short) (("" + cell.getNumericCellValue()).length() * 256);

                    break;

                case HSSFCell.CELL_TYPE_STRING:
                    neededSize = (short) (cell.getStringCellValue().length() * 256);

                    break;

                default:
                    // dont do anything
                    neededSize = 0;

                break;
                }

                if (size < neededSize) {
                    sheet.setColumnWidth(x, neededSize);
                }

                // counter for lines
                x++;
            }

            lineNumber++;
        }
    }

    private static short findCol(final HSSFRow row, final String key) {
        Logger.debug(null, "checking row for \"" + key + "\" nr. of cells " + row.getLastCellNum());
        for (short i = 0; i < 100; i++) {
            HSSFCell cell = row.getCell(i);
            if (cell != null){
                Logger.debug(null, "value at pos(" + i + "): " + cell.getStringCellValue());
                String cellValue = cell.getStringCellValue();
                Logger.debug(null, "comparing : " + cellValue + " vs. " + key + "   --> " + (cellValue.compareTo(key)==0));
                if (cellValue.compareTo(key) == 0 ) return i;
            } else {
                Logger.debug(null, "value at pos(" + i + "): cell is null!    ");
                //return -1;
            }
        }
        return -1;
    }
}
