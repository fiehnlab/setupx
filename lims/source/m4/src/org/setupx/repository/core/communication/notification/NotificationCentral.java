/**
 * ============================================================================ File:    NotificationCentral.java Package: org.setupx.repository.core.communication.notification cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.notification;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.Mail;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.server.logging.Message;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;

import javax.mail.MessagingException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class NotificationCentral extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final long serialVersionUID = 1L;
  private static final Object thiZ = new NotificationCentral();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private NotificationCentral() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param notification TODO
   */
  public static void notify(Notification notification) {
      Message.createMessage(notification);
    
      /*
    Message message = new Message();
    message.setUserID(0);
    message.setClassName("");
    message.setLabel(notification.getMessage());
    message.setMessage(notification.getMessage());
    
    try {
      message.update(true);
    } catch (PersistenceActionUpdateException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }

    debugS("new " + org.setupx.repository.core.util.Util.getClassName(notification) + ": " + notification.getMessage());

    // find relevent recipients
    debug(thiZ, "requesting recipients");

    String[] mailaddresses = notification.getRecipients();
    debug(thiZ, "recipients: " + mailaddresses.length);

    try {
      if ((mailaddresses == null) || (mailaddresses.length == 0)) {
        Mail.postMail(new String[] { Config.OPERATOR_ADMIN.getEmailAddress() }, "serious config problem", "mail recipients are not configured for " + Util.getClassName(notification), Config.OPERATOR_ADMIN.toString());
      }

      Mail.postMail(mailaddresses, Config.SYSTEM_NAME + ":" + Util.getClassName(notification), notification.getMessage(), Config.OPERATOR_ADMIN.toString());
    } catch (MessagingException e) {
      e.printStackTrace();
    }
       */
  }

  /**
   * TODO: 
   *
   * @param message TODO
   */
  private static void debugS(String message) {
    debug(thiZ, message);
  }
}
