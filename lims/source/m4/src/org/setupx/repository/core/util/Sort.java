/**
 * ============================================================================ File:    Sort.java Package: org.setupx.repository.core.util cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util;

/**
 * sorts an array of strings in alphabetial order
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class Sort {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  boolean isSorted = false;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Sort object.
   *
   * @param list the array of strings which will be sorted
   */
  public Sort(String[] list) {
    do
      pass(list);
    while (!isSorted);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param list TODO
   */
  private void pass(String[] list) {
    isSorted = true;

    for (int i = 1; i < list.length; i++)
      if (list[i - 1].toLowerCase().compareTo(list[i].toLowerCase()) > 0) {
        swap(list, i - 1, i);
        isSorted = false;
      }
  }

  /**
   * change pos of two elements
   */
  private void swap(String[] list, int a, int b) {
    String temp = list[a];
    list[a] = list[b];
    list[b] = temp;
  }
}
