package org.setupx.repository.core.communication.status;

import java.util.Date;

public interface Timestamped {
    public Date getDate();

}
