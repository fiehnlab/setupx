package org.setupx.repository.core.user;

import org.setupx.repository.Config;
import org.setupx.repository.core.Connectable;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.ServiceNotAvailableException;


/**
 * connecting the userfactory to the system
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.1 $
 */
public class UserConnector extends CoreObject implements Connectable {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private UserDO userDO = null;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /** defining all admins that are being notified for new samples */
  public static final short USERGRP_NEW_SAMPLE = 1;
  private static Object thiz = new UserConnector();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new UserConnector object.
   */
  public UserConnector() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param usergrp_new_sample2
   *
   * @return
   */
  public static UserDO[] getUser(int usergrp_new_sample2) {
    UserDO[] users = new UserDO[] { new UserLocal("admin", "admin", Config.OPERATOR_ADMIN) };

    return users;
  }

  /**
   * TODO: 
   *
   * @param user TODO
   *
   * @throws UserFactoryException TODO
   */
  public static void addUser(UserDO user) throws UserFactoryException {
    UserFactory.createUser(user);
  }


  /**
   * TODO: 
   *
   * @param username TODO
   * @param password TODO
   *
   * @return TODO
   *
   * @throws UserNotFoundException
   * @throws WrongPasswordException
   * @throws UserFactoryException
   */
  public static UserDO findUser(String username, String password)
    throws UserNotFoundException, WrongPasswordException, UserFactoryException {
    /*
     * boolean exists = UserFactory.isExistingUser(username, password);
     * if(!exists) throw new UserNotFoundException("user does not exist");
     */
    return UserFactory.findUser(username, password);
  }



  /* (non-Javadoc)
   * @see org.setupx.repository.core.Connectable#isAvailable()
   */
  public void checkAvailability() throws ServiceNotAvailableException {
  }
}
