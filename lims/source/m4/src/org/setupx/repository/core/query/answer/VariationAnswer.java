package org.setupx.repository.core.query.answer;

import java.util.HashSet;
import java.util.Vector;

import org.setupx.repository.core.query.QueryMasterAnswer;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.user.access.PromtUserAccessRight;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.util.Link;
import org.setupx.repository.web.forms.InsufficientUserRightException;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.pub.data.cache.VariationsCache;

public class VariationAnswer extends QueryMasterAnswer {

    public static HashSet usedExperiments = new HashSet();
    private long classID;
    private long experienmentID;
    private String _abstract = "";
    private String title;
    private String value;
    /** species has been deactivated - it ll stay "" */
    private String question;
    private VariationsCache variationsCache;
    private static String icon;

    
    static {
        icon = "pics/cube.png";
    }

    public VariationAnswer(VariationsCache variationsCache, final String searchTerm, final String variation) throws PersistenceActionFindException, FoundExperimentTwiceException {
        this.classID = variationsCache.getClassid();
        this.experienmentID = new SXQuery().findPromtIDbyClazz((int)this.classID);

        if (! usedExperiments.contains(experienmentID + "")){
            usedExperiments.add(experienmentID + "");

            this.value = variationsCache.getValue();
            this.title = new SXQuery().findPromtTitleByPromtID(experienmentID);
            this._abstract = new SXQuery().findPromtAbstractByPromtID(experienmentID);
            //String design = new SXQuery().findPromtStructure(experienmentID);

            //this.species = new SXQuery().findSpeciesbyPromtID(experienmentID).get(0).toString();

            //String classDef = new SXQuery().findClassInformationString(classID);
            this.question = variationsCache.getQuestion();
            this.variationsCache = variationsCache;
        } else {
            throw new FoundExperimentTwiceException();
        }
    }

    public String getIcon() {
        return icon;
    }

    public String getInfo1() {
        return "SearchTerm was found in field: [" + this.question + "]"; 
    }

    public String getInfo2() {
        return "Experiment Title: [" + this.title + "]";
    }

    public String getInfo3() {
        if (_abstract.length() > 150){ 
            return "Experiment abstract: [" + _abstract.substring(0, 140) + "..."  + "]";
        }else{
            return "Experiment abstract: [" + _abstract + "]";
        }
    }

    public String getLabel() {
        /**
         * deactivated cause HTML support is not givven.
         * 
        int posSearchTerm = value.toLowerCase().indexOf(searchTerm.toLowerCase());
        Logger.debug(this, posSearchTerm +"");
        int start = 0;
        String startAdd = "";
        int end = value.length();
        String endAdd = "";
        Logger.debug(this, end +"");
        if (posSearchTerm > 20) {
            start = posSearchTerm-10;
            startAdd = "...";
        }
        Logger.debug(this, start +"");
        if (value.length() - posSearchTerm - searchTerm.length() > 20) {
            end = posSearchTerm + searchTerm.length() + 10;
            endAdd = "...";
        }
        Logger.debug(this, end +"");

        String returnValue = 
            startAdd + 
            value.substring(start, posSearchTerm) + 
            value.substring(posSearchTerm, posSearchTerm + this.searchTerm.length()) + 
            value.substring(posSearchTerm+searchTerm.length(),end);
        return returnValue;
         */
        return value;
    }

    public Object getAnswerSource() {
        return this.variationsCache;
    }

    public Class getAnswerSourceType() {
        return this.variationsCache.getClass();
    }

    public Vector getRelatedObjectLinks() {
        Vector vector = new Vector();
        vector.add(new Link("open experiment [" + this.experienmentID + "]", Promt.class, "" + this.experienmentID, ("load?id="+ this.experienmentID + "&action=" + PromtUserAccessRight.READ)));
        return vector;
    }

    public boolean checkAccessRestriction(UserDO userDO) {
        try {
            PromtUserAccessRight.checkAccess(userDO.getUOID() ,this.experienmentID, PromtUserAccessRight.READ);
            debug("access for " + userDO.getDisplayName() + " on " + this.experienmentID +  " granted.");
            return true;
        } catch (InsufficientUserRightException e) {
            debug("access for " + userDO.getDisplayName() + " on " + this.experienmentID + " not granted.");
            return false;
        }
    }

    /*
     * (non-Javadoc)
     * @see org.setupx.repository.core.query.QueryMasterAnswer#isAccessRestricted()
     */
    public boolean isAccessRestricted() {
        return true;
    }
}


