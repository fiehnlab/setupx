package org.setupx.repository.core.communication;

import org.setupx.repository.core.Connectable;


public class ServiceNotAvailableException extends Exception {

	/**
	 * @param e
	 */
	public ServiceNotAvailableException(Throwable e) {
		super(e);
	}

	/**
	 * @param test
	 */
	public ServiceNotAvailableException(Connectable connector) {
		super("the service: " + org.setupx.repository.core.util.Util.getClassName(connector) + " is not available.");
	}

    /**
     * @param library
     * @param e
     */
    public ServiceNotAvailableException(Connectable connector, Exception e) {
		super("the service: " + org.setupx.repository.core.util.Util.getClassName(connector) + " is not available.", e);
    }

    /**
     * @param library
     * @param string
     */
    public ServiceNotAvailableException(Connectable connector, String string) {
		super("the service: " + org.setupx.repository.core.util.Util.getClassName(connector) + " is not available cause: " + string);
    }

    public ServiceNotAvailableException(String string, Throwable e) {
        super(string, e);
    }
}
