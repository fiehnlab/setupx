/**
 * ============================================================================ File:    Mail.java Package: org.setupx.repository.core.util cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util;


import java.io.PrintWriter;
import java.io.StringWriter;

import javax.mail.MessagingException;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.util.SXMail;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class Mail extends CoreObject {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Mail() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param e
   *
   * @throws MessagingException
   */
  public static void postError(Exception e) {
    StringWriter stringWriter = new StringWriter();
    PrintWriter printWriter = new PrintWriter(stringWriter);

    e.printStackTrace(printWriter);

    try {
      postMail(new String[] { Config.OPERATOR_ADMIN.getEmailAddress() }, "[error] " + Util.getClassName(e), stringWriter.getBuffer().toString(), Config.OPERATOR_ADMIN.getEmailAddress());
    } catch (MessagingException e1) {
      e1.printStackTrace();
    }
  }

  /**
   * @param string
   */
  public static void postError(Object object, String string) {
    try {
      postMail(new String[] { Config.OPERATOR_ADMIN.getEmailAddress() }, "[error] " + Util.getClassName(object), string, Config.OPERATOR_ADMIN.getEmailAddress());
    } catch (MessagingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public static void postMail(String[] recipients, String subject, String message, String from)
    throws MessagingException {
    if (!Config.MAIL_ACTIVE) {
      return;
    }
    new SXMail().sendSSLMessage(recipients, subject, message, from);
  }
}
