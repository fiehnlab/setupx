/**
 * ============================================================================ File:    SortSamples.java Package: org.setupx.repository.core.util.hotfix.oldsamples cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.hotfix.oldsamples;

import org.setupx.repository.core.util.File;
import org.setupx.repository.core.util.hotfix.fsa.PotatoImporter;
import org.setupx.repository.core.util.xml.XMLFile;
import org.setupx.repository.core.util.xml.XPath;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class SortSamples extends PotatoImporter {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new SortSamples object.
   */
  public SortSamples() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param args TODO
   */
  public static void main(String[] args) {
    new SortSamples().sortByClass(new java.io.File("/mnt/metabolomics/Scholz/sampleinformation/ok"), PotatoImporter.xpath_expression_classID, PotatoImporter.xpath_expression_sampleID);
    new SortSamples().sortByClass(new java.io.File("/mnt/metabolomics/Scholz/sampleinformation/fsa/fieldtrails2003"), PotatoImporter.xpath_expression_classID, PotatoImporter.xpath_expression_sampleID);
    new SortSamples().sortByClass(new java.io.File("/mnt/metabolomics/Scholz/sampleinformation/new/wenzel/xml_samples"), "/meta/sample/class", "/meta/sample/id");
    new SortSamples().sortByClass(new java.io.File("/mnt/metabolomics/Scholz/sampleinformation/new/fsa/xml_samples"), "/meta/sample/class", "/meta/sample/id");
  }

  /**
   * TODO: 
   *
   * @param sourceDirectory TODO
   * @param xPathClassID TODO
   * @param xPathSampleID TODO
   */
  public void sortByClass(java.io.File sourceDirectory, final String xPathClassID, final String xPathSampleID) {
    debug(sourceDirectory.getAbsolutePath());

    java.io.File[] files = sourceDirectory.listFiles();

    String classID = "";
    String sampleID = "";

    for (int i = 0; i < files.length; i++) {
      if (files[i].isDirectory()) {
        sortByClass(files[i], xPathClassID, xPathSampleID);
      } else {
        // open file
        try {
          XMLFile xmlFile = new XMLFile(files[i]);

          // take class
          classID = XPath.pathValue(xmlFile.getDocument(), xPathClassID);
          sampleID = XPath.pathValue(xmlFile.getDocument(), xPathSampleID);

          classID = cutSpace(classID);
          sampleID = cutSpace(sampleID);

          //debug(" \"" + files[i].getName() + "\" \t \"" + sampleID + "\" \t \"" + classID + "\"");
          java.io.File directory = new java.io.File(sourceDirectory.toString() + java.io.File.separator + classID);

          //java.io.File directory = new java.io.File("/mnt/metabolomics/Scholz/sampleinformation/new_sorted_by_class/" + createYear(sampleID) + "/" + classID);
          java.io.File tmpFile = File.copy(directory, files[i]);

          //File.replace(tmpFile, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n <?xml-stylesheet type=\"text/xsl\" href=\"../../sample.xsl\" ?>");
        } catch (Exception e) {
          debug("error on: " + files[i].getName());
        }
      }
    }
  }

  /**
   * TODO: 
   *
   * @param sampleID TODO
   *
   * @return TODO
   */
  private String createYear(String sampleID) {
    return "200" + sampleID.substring(0, 1);
  }
}
