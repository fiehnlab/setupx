package org.setupx.repository.core.communication.technology.platform;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;

public abstract class TechnoCoreObject {
    public abstract long getUOID();
    protected long uoid = 0;
    
    public void persistenceChilds(Session session, boolean createIt) throws PersistenceActionException {
        
    }
    
    public void debug(String comment){
        Logger.debug(this, comment);
    }
    
    public static Session createSession(){
        return PersistenceConfiguration.createSessionFactory().openSession();
    }
    

    private final static void myUpdate(TechnoCoreObject object, Session hqlSession, boolean createIt) {
        if (object.getUOID() == 0 && createIt){
            hqlSession.save(object);
//            Logger.debug(object, "saved. uoid:" + object.getUOID());
        } else {
            hqlSession.update(object);
//            Logger.debug(object, "updated. uoid:" + object.getUOID());
        }
    }
    
    public void myUpdateMeAndChilds(Session hqlSession, boolean createIt){
        myUpdate(this, hqlSession, createIt);
    }

    
    public static TechnoCoreObject persistence_loadByID(Class class1, long id){
        return persistence_loadByID(class1, createSession(), id);
    }

    public static TechnoCoreObject persistence_loadByID(Class class1, Session s, long id){
        try {
            return (TechnoCoreObject) s.createCriteria(class1).add(Restrictions.eq("id", new Long(id))).list().get(0);
        } catch (Exception e) {
            return null;
        }
    }
    
    public static void remove(Class class1,long uoid){
        Session s = PersistenceConfiguration.createSessionFactory().openSession();
            TechnoCoreObject technoCoreObject = persistence_loadByID(class1, s, uoid);
            
            if (technoCoreObject.uoid == 0) {
                Logger.warning(technoCoreObject, "trying to remove myself, even so I am not even persistent.");
                return;
            }

            Transaction transaction = s.beginTransaction();

            // store this object
            try {

                s.delete(technoCoreObject);
                transaction.commit();

            } catch (Exception e) {
                Logger.err(technoCoreObject, e.toString());
                Logger.warning(technoCoreObject, "rolling transaction back !! - cause: " + e.getLocalizedMessage());
                transaction.rollback();
                s.close();
                return;
            }

            // if no err - finish transaction
            s.close();
        }
}
