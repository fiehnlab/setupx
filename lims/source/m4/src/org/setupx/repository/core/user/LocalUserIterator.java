/**
 * ============================================================================ File:    LocalUserIterator.java Package: org.setupx.repository.core.user cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.user;

import java.io.File;
import java.util.Iterator;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.logging.Logger;
import org.w3c.dom.Element;


/**
 * Iterator containg all iterators from the local configuration file
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.13 $
 */
class LocalUserIterator extends CoreObject implements Iterator {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private UserDO[] userDOs = new UserDO[0];
  private int pos = 0;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new LocalUserIterator object.
   */
  public LocalUserIterator() {
    super();

    if (Config.FILE_USERS_ACTIVE) {
      try {
        determineUsers(new File(Config.FILE_USERS));
      } catch (UserFactoryException e) {
        e.printStackTrace();
      }
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see java.util.Iterator#hasNext()
   */
  public boolean hasNext() {
    return (pos != userDOs.length);
  }

  /*
   *  (non-Javadoc)
   * @see java.util.Iterator#next()
   */
  public Object next() {
    return userDOs[pos++];
  }

  /*
   *  (non-Javadoc)
   * @see java.util.Iterator#remove()
   */
  public void remove() {
  }

  /**
   * @param element
   * @param elementName
   *
   * @return
   *
   * @deprecated use the XPATH class to find elements
   */
  private String getChildValue(Element element, String elementName) {
    try {
      return element.getElementsByTagName(elementName).item(0).getFirstChild().getNodeValue();
    } catch (NullPointerException e) {
      return "";
    }
  }

  /**
   * iterator containing all users stored in the configuration_file
   *
   * @param file
   *
   * @throws UserFactoryException
   */
  private void determineUsers(File file) throws UserFactoryException {
    Logger.warning(this, "xml support deactivated ");

    /*
       XMLFile xmlFile = null;
       try {
         xmlFile = new XMLFile(file);
       } catch (SAXException e) {
         throw new UserFactoryException("unable to init " + this.getClass().getName() + " cause: " + e.getMessage() + "  " + e);
       } catch (UTFDataFormatException e) {
         err(e);
       }
       Document doc = xmlFile.getDocument();
       NodeList nodeList = doc.getElementsByTagName(SystemConstants.XML_ELEMENT_USER);
       // set userArray to new Size
       userDOs = new UserDO[nodeList.getLength()];
       // loop over all the users ...
       for (int i = 0; i < nodeList.getLength(); i++) {
         // find userElement. from this get all the attributes....
         Element userElement = (Element) nodeList.item(i);
         // create tempuser for each
         UserDO _tempUser = new UserLocal("", "", new EMailAddress("unknown@test.com"));
         // set the paramateres of all the attributes
         _tempUser.password = (getChildValue(userElement, SystemConstants.XML_ELEMENT_PASSWORD));
         _tempUser.username = (getChildValue(userElement, SystemConstants.XML_ELEMENT_USERID));
         userDOs[i] = _tempUser;
       }
     */
  }
}
