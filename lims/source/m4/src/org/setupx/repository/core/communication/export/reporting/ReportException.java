package org.setupx.repository.core.communication.export.reporting;

public class ReportException extends Exception {

    public ReportException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public ReportException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ReportException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public ReportException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
