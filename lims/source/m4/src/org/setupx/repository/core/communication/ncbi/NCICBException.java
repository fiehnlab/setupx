package org.setupx.repository.core.communication.ncbi;

public class NCICBException extends Exception {

    public NCICBException(String string) {
        super(string);
    }

    /**
     * @param string
     * @param e
     */
    public NCICBException(String string, Exception e) {
        super(string, e);
    }

    /**
     * @param e
     */
    public NCICBException(Throwable e) {
        super(e);
    }

}
