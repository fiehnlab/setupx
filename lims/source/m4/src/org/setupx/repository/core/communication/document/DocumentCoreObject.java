package org.setupx.repository.core.communication.document;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.setupx.repository.core.PersistenceActionRemoveException;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;

public abstract class DocumentCoreObject {

    private final static void update(DocumentCoreObject object, Session hqlSession, boolean createIt) {
        org.hibernate.Transaction transaction = hqlSession.beginTransaction();
        if (object.getUOID() == 0 && createIt) {
            hqlSession.save(object);
            Logger.debug(object, "saved. uoid:" + object.getUOID());
        } else {
            hqlSession.update(object);
            Logger.debug(object, "updated. uoid:" + object.getUOID());
        }
        transaction.commit();
    }

    public abstract long getUOID();

    public static DocumentCoreObject persistence_loadByID(Class class1, long id) {
        return persistence_loadByID(class1, createSession(), id);
    }

    public static DocumentCoreObject persistence_loadByID(Class class1, Session s, long id) {
        try {
            return (DocumentCoreObject) s.createCriteria(class1).add(Restrictions.eq("id", new Long(id))).list().get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public static Session createSession() {
        return PersistenceConfiguration.createSessionFactory().openSession();
    }

    public void update(Session session) {
        update(this, session, false);
        org.setupx.repository.core.util.logging.Logger.debug(this, "updated. uoid:" + this.getUOID());
    }
    

    public void save(Session session) {
        update(this, session, true);
        Logger.debug(this, "saved. uoid:" + this.getUOID());
    }

    public static List persistence_loadAll(Class c, Session s) throws PersistenceActionFindException {
        List list = s.createCriteria(c).addOrder(Order.asc("id")).setCacheable(true).list();
        return list;
    }
    

    public static void remove(Class class1, long uoid) throws PersistenceActionRemoveException{
        
        Session session = createSession();

        Transaction transaction = session.beginTransaction();
        
        DocumentCoreObject coreObject;
        coreObject = DocumentCoreObject.persistence_loadByID(class1, session, uoid);
        coreObject.remove(session);
        transaction.commit();        
    }
    
    /**
     * @see #remove()
     */
    public void remove(Session s) throws PersistenceActionRemoveException {
        try {
            s.delete(this);
        } catch (Exception e) {
            throw new PersistenceActionRemoveException("unable to remove ", e);
        }
    }
}
