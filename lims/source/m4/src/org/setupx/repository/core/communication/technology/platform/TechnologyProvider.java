package org.setupx.repository.core.communication.technology.platform;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.hibernate.Hibernate;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;

/**
 * provied all access to a technology and the related data in the lab.
 */
public class TechnologyProvider extends CoreObject{
    
    public static Technology createTechnology(String label, String description) throws PersistenceActionException {
        Technology technology = new Technology(label, description);
        return technology;
    }

    /**
     * gets all datafiles for a certain sample with the ID: sampleID
     * @throws PersistenceActionFindException 
     */
    public static List getDatafiles(final long sampleID) throws PersistenceActionFindException {
        // loop over all technologies and get the files
        List list = new Vector();
  
        Iterator techIterator = getTechnologies().iterator();
        while(techIterator.hasNext()){
            Technology technology = (Technology)techIterator.next();
            list.addAll(technology.getFilesForSample(sampleID));
        }
        
        return list;
    }

    public static Collection getTechnologies() throws PersistenceActionFindException{
        return CoreObject.persistence_loadAll(Technology.class, CoreObject.createSession());
    }

    public static List getPossibleDatafiles(long sampleID) throws PersistenceActionFindException {
        // loop over all technologies and get the files
        List list = new Vector();
  
        Iterator techIterator = getTechnologies().iterator();
        while(techIterator.hasNext()){
            Technology technology = (Technology)techIterator.next();
            list.addAll(technology.getPossibleDatafilesForSample(sampleID));
        }
        return list;
    }

    public static List getPossibleDatafiles(Date start, Date end) throws PersistenceActionFindException {
        // loop over all technologies and get the files
        List list = new Vector();
  
        Iterator techIterator = getTechnologies().iterator();
        while(techIterator.hasNext()){
            Technology technology = (Technology)techIterator.next();
            list.addAll(technology.getPossibleDatafile(start, end));
        }
        return list;
    }

    public static List getDatafiles(Date start, Date end) throws PersistenceActionFindException {
        // loop over all technologies and get the files
        List list = new Vector();
  
        Iterator techIterator = getTechnologies().iterator();
        while(techIterator.hasNext()){
            Technology technology = (Technology)techIterator.next();
            list.addAll(technology.getFiles(start, end));
        }
        return list;
    }

    public static List determineSampleIDs(String sampleName) {
        String query = "select sampleID from datafile where source like \"%" + sampleName + "%\" group by sampleID";
        try {
            List elements = CoreObject.createSession().createSQLQuery(query).addScalar("sampleID", Hibernate.LONG).list();
            return elements;

        } catch (Exception e) {
            Logger.err(null, "unable to determineSampleIDs(" + sampleName + ") cause: "  + e.getMessage());
            return null;
        }
    }
}
