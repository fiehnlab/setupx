/**
 * ============================================================================ File:    SystemNotification.java Package: org.setupx.repository.core.communication.notification cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.notification;

import org.setupx.repository.Config;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class SystemNotification extends Notification {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new SystemNotification object.
   */
  public SystemNotification() {
    super();
  }

  /**
   * Creates a new SystemNotification object.
   *
   * @param message 
   */
  public SystemNotification(String message) {
    super();
    this.setMessage(message);
  }

  /**
   * Creates a new SystemNotification object.
   *
   * @param className 
   * @param ex 
   */
  public SystemNotification(String className, Throwable ex) {
    super();
    setMessage(ex.getClass().toString() + "  " + ex.getLocalizedMessage() + " - " + ex.getMessage());
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String[] getRecipients() {
    return new String[] { Config.OPERATOR_ADMIN.getEmailAddress() };
  }
}
