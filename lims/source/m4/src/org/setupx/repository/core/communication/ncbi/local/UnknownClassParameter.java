package org.setupx.repository.core.communication.ncbi.local;

import org.setupx.repository.core.util.Util;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class UnknownClassParameter extends Exception {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new UnknownClassParameter object.
   *
   * @param clasS 
   */
  public UnknownClassParameter(Class clasS) {
    super("unknown: " + Util.getClassName(clasS));
  }
}
