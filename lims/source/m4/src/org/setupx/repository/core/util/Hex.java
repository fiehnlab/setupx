/**
 * ============================================================================ File:    Hex.java Package: org.setupx.repository.core.util cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util;

import java.util.Hashtable;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class Hex {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  static Hashtable ht = new Hashtable();

  static {
    for (int i = 0; i < 30000; i++) {
      ht.put(Integer.toHexString(i), new Integer(i));
    }
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Hex object.
   */
  public Hex() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param string TODO
   *
   * @return TODO
   */
  public int getCode(String string) {
    return ((Integer) ht.get(string)).intValue(); //  Integer.toHexString(j).equals(value);
  }
}


// 
