/**
 * ============================================================================ File:    DocumentConverter.java Package: org.setupx.repository.core.util.xml cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.xml;

import org.jdom.Document;
import org.jdom.JDOMException;

import org.jdom.input.DOMBuilder;

import org.jdom.output.DOMOutputter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


/**
 * Converter for different types of Documents. Like converting from w3c document to jdom doc, ...
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class DocumentConverter {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static DOMOutputter converter = new DOMOutputter();
  private static DOMBuilder builder = new DOMBuilder();

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected DocumentConverter() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * converts a w3c-document into an JDOM document
   */
  public static Document convert(org.w3c.dom.Document domDocument) {
    return builder.build(domDocument);
  }

  /**
   * converts a  JDOM document into an w3c-document
   */
  public static org.w3c.dom.Document convert(Document jdomDocument)
    throws JDOMException {
    return converter.output(jdomDocument);
  }

  /**
   * creates a new w3c-document
   *
   * @return a new w3c-document
   *
   * @throws ParserConfigurationException problems creating a new w3c-document
   */
  public static org.w3c.dom.Document createNewDocument()
    throws ParserConfigurationException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    org.w3c.dom.Document _document = builder.newDocument();

    return _document;
  }
}
