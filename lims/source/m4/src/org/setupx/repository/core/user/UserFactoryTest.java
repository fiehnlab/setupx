package org.setupx.repository.core.user;

import java.util.Date;

import junit.framework.TestCase;

import org.setupx.repository.core.communication.mail.EMailAddress;
import org.setupx.repository.core.util.logging.Logger;


/**
 * testcase
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 */
public class UserFactoryTest extends TestCase {


  /*
   * @see TestCase#setUp()
   */
  protected void setUp() throws Exception {
    super.setUp();
  }

  /*
   * @see TestCase#tearDown()
   */
  protected void tearDown() throws Exception {
    super.tearDown();
  }

  /**
   * TODO: 
 * @throws UserNotFoundException
 * @throws UserFactoryException
 * @throws WrongPasswordException
   */
  public void testLoginUNKNOWNUSER() throws UserNotFoundException, WrongPasswordException, UserFactoryException {
    UserDO userDO = UserFactory.findUser("sx", "sx");
    assertTrue(userDO.username.compareTo("sx") == 0);
  }

  /**
   * TODO: 
 * @throws UserNotFoundException
 * @throws UserFactoryException
 * @throws WrongPasswordException
   */
  public void testLoginlocaluser() throws UserNotFoundException, WrongPasswordException, UserFactoryException {
    UserDO userDO = UserFactory.findUser("local", "local");
    assertTrue(userDO.password.compareTo("local") == 0);
  }

  /**
   * TODO: 
 * @throws UserNotFoundException
 * @throws UserFactoryException
 * @throws WrongPasswordException
   */
  public void testLoginLDAP_USER() throws UserNotFoundException, WrongPasswordException, UserFactoryException {
    UserDO userDO = UserFactory.findUser("mscholz", "bla");
    assertTrue(userDO.password.compareTo("bla") == 0);
    Logger.log(this, userDO.toString());
  }


  public void testCreateUser() throws UserFactoryException, UserNotFoundException, WrongPasswordException{
      String username = "";
      for (int i = 0; i < 2000; i++) {
          username = "username" + new Date().getTime() + i;
          UserFactory.createUser(new UserLocal(username, "password",new EMailAddress(i+"@perc.de")));

//          UserDO userDO = UserFactory.login(username, "password");
//          UserFactory.remove(userDO);

      }
  }

  


}
