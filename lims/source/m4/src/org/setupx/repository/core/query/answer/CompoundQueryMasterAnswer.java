package org.setupx.repository.core.query.answer;

import java.util.Vector;

import org.setupx.repository.core.query.QueryMasterAnswer;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.util.Link;

/**
 * Answer representing a single compound found in the repository.
 */
public class CompoundQueryMasterAnswer extends QueryMasterAnswer {

    private static String icon = "pics/compound.png";
    private String label;
    private String occourance;
    
    public CompoundQueryMasterAnswer(String label2, String occourance) {
        this.label = label2;
        this.occourance = occourance;
    }

    /** todo - add real image */
    public String getIcon() {
        return this.icon;
    }

    public String getInfo1() {
        return "";
    }

    public String getInfo2() {
        return "compound was found in " + occourance + " samples.";    
    }

    public String getInfo3() {
        return "";    
    }

    public String getLabel() {
        return this.label;    
    }

    public Vector getRelatedObjectLinks() {
        Vector vector = new Vector();
        vector.add(new Link("samples containing " + this.label ,"q2.jsp?type=1&term=" + this.getLabel()));
        vector.add(new Link("experiments containing " + this.label ,"q3.jsp?type=1&term=" + this.getLabel()));
        vector.add(new Link("add " + this.label + " to the selection of compounds","pubsample.jsp?" + this.getLabel()));
        return vector;
    }

    public boolean checkAccessRestriction(UserDO userDO) {
        throw new UnsupportedOperationException("Operation unsupported cause access is not restricted.");
    }

    /*
     * (non-Javadoc)
     * @see org.setupx.repository.core.query.QueryMasterAnswer#isAccessRestricted()
     */
    public boolean isAccessRestricted() {
        return false;
    }
}
