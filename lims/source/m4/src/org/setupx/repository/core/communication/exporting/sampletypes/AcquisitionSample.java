/**
 * ============================================================================ File: AcquisitionSample.java Package:
 * org.setupx.repository.core.communication.exporting.sampletypes cvs: $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06
 * 21:54:21 scholz Exp $ ============================================================================ Martin Scholz
 * Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/
 * ============================================================================
 */
package org.setupx.repository.core.communication.exporting.sampletypes;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.communication.exporting.ACQMachineConfigurationIterator;
import org.setupx.repository.core.communication.exporting.AcquisitionParameter;
import org.setupx.repository.core.communication.exporting.ExportMapping4Samples;
import org.setupx.repository.core.communication.exporting.JobInformation;
import org.setupx.repository.core.communication.exporting.Machine;
import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.hibernate.SXSerializable;

import org.hibernate.Session;

import java.text.DecimalFormat;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

/**
 * 
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 * @hibernate.class table = "acquisitionsample"
 * @hibernate.discriminator column = "discriminator"
 */
public class AcquisitionSample extends CoreObject implements SXSerializable {
    // ~ Instance fields
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public String typeShortcut = AcquisitionLabel;

    /* importance of beeing postioned first on the tray */
    public int positionImportance = POSITIONIMPORTANCE_SAMPLE_DEFAULT;
    public int sequenceImportance = 20;
    public long tempSampleID = 0;
    protected JobInformation jobInformation = null;

    /** hashtable containing all parameters _ key: paramater.getLabel - value: parameter */
    private Hashtable parameters = new Hashtable();

    // ~ Static fields/initializers
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public static final int POSITIONIMPORTANCE_SAMPLE_DEFAULT = 99;
    public static Object thiz = new AcquisitionSample();
    public static final int MAX_IMPORTANCE = 20;
    public static int MAX_VIAL_POS = 98;
    public static String AcquisitionLabel = "sa";
    private static Hashtable ids = new Hashtable();

    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new AcquisitionSample object.
     */
    public AcquisitionSample() {
        super();
    }

    /**
     * Creates a new AcquisitionSample object.
     * 
     * @param jobInformation
     *                
     * @param label
     *                
     * @throws MappingError
     *                 
     */
    protected AcquisitionSample(JobInformation jobInformation, String label) throws MappingError {
        this.add(jobInformation);

        debug("overwriting ");

        try {
            this.addParameter(new AcquisitionParameter(AcquisitionParameter.TRAY, label));
            this.addParameter(new AcquisitionParameter(AcquisitionParameter.VIAL, "" + 0));
        } catch (ParameterException e) {
            throw new MappingError(e);
        }
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * TODO: 
     * 
     * @param set
     *                TODO
     */
    public final void setParameterHibernate(Set set) {
        this.parameters = map(set);
    }

    /**
     * @hibernate.set role="ParameterHibernate"
     * @hibernate.collection-key column="acqsample"
     * @hibernate.collection-one-to-many class="org.setupx.repository.core.communication.exporting.AcquisitionParameter"
     */
    public final Set getParameterHibernate() {
        return map(this.parameters);
    }

    /**
     * @throws ParameterException
     *                 when there is paramter with this label
     */
    public AcquisitionParameter getParamter(String label) throws ParameterException {
        // Logger.debug(this,"looking for a parameter with the label: "+ label);
        AcquisitionParameter parameter = (AcquisitionParameter) parameters.get(label);

        if (parameter == null) {
            String error = "";
            Enumeration enumeration = parameters.keys();

            while (enumeration.hasMoreElements()) {
                error.concat((String) enumeration.nextElement());
            }

            throw new ParameterException("unable to find parameter (label: " + label + ")  number of parameters: "
                    + parameters.size() + " error: " + error);
        }

        return parameter;
    }

    /**
     * The setter method for Unique Object identifier.
     */
    public void setUOID(long lId) {
        uoid = lId;
    }

    /**
     * The getter method for Unique Object identifier.
     * <p>
     * The id is generated by the persistence layer.
     * </p>
     * 
     * @return unique id for this object
     * @hibernate.id column = "uoid" generator-class="native"
     */
    public long getUOID() {
        return (uoid);
    }

    /**
     * TODO: 
     * 
     * @param parameter
     *                TODO
     */
    public void addParameter(AcquisitionParameter parameter) {
        this.parameters.put(parameter.getLabel(), parameter);
    }

    /**
     * @param sampleIDs
     *                samples that will be mapped
     * @param jobInformation
     *                information about how to process
     * @param vialPositionOffset
     *                starting postiotion of the samples on the vial
     * @return an array of mapped AcquisitionSample
     * @throws MappingError
     * @throws PersistenceActionUpdateException
     */
    public static final AcquisitionSample[] map(long[] sampleIDs, JobInformation jobInformation) throws MappingError,
            PersistenceActionUpdateException {
        AcquisitionSample[] results = new AcquisitionSample[sampleIDs.length];

        for (int i = 0; i < results.length; i++) {
            results[i] = AcquisitionSample.map(sampleIDs[i], jobInformation, (i + jobInformation
                    .getVialPositionOffset()));
        }

        return results;
    }

    /**
     * blabla and attaches an Event to the each of the samples which contains the AcquisitionSample and information
     * about the Acquisition
     * 
     * @param l
     *                TODO
     * @param jobInformation
     *                TODO
     * @param vialPosition
     *                TODO
     * @return TODO
     * @throws MappingError
     * @throws PersistenceActionUpdateException
     */
    public static final AcquisitionSample map(long sampleID, JobInformation jobInformation, int vialPosition)
            throws MappingError, PersistenceActionUpdateException {
        AcquisitionSample acquisitionSample;

        try {
            acquisitionSample = newInstance(sampleID, jobInformation, vialPosition);
        } catch (PersistenceActionFindException e) {
            throw new PersistenceActionUpdateException("unable to update - didnt find " + sampleID, e);
        }

        return acquisitionSample;
    }

    /**
     * TODO: 
     * 
     * @param jobInformation
     *                TODO
     * @return TODO
     */
    public final String generateName(JobInformation jobInformation) {
        java.util.Date date = new java.util.Date();

        String fmt = "#00";
        DecimalFormat df = new DecimalFormat(fmt);

        String day = "" + df.format(date.getDate());
        String month = "" + df.format(date.getMonth() + 1);
        String year = "" + df.format((date.getYear() - 100));
        char machine = jobInformation.getMachine().getMachineLetter();
        String operator = jobInformation.getOperator();
        String sampleType = this.typeShortcut;
        String counter = df.format(this.generateCounterID());
        String generatedName = year.concat(month.concat(day.concat("" + machine).concat(
                operator.concat(sampleType.concat("" + counter)))));

        if (generatedName.length() != 13) {
            warning(this, "the generated name " + generatedName + " does not have the requiered length.");
        }

        return generatedName;
    }

    /**
     * TODO: 
     * 
     * @throws PersistenceActionFindException
     *                 TODO
     * @throws PersistenceActionUpdateException
     *                 TODO
     */
    public void connectToSample() throws PersistenceActionFindException, PersistenceActionUpdateException {
        warning(this, this.parameters.get(AcquisitionParameter.NAME) + " - " + this.tempSampleID);

        debug(thiz, "updating sample and adding events to them");

        /*
         * TODO urgent !!!! Session session = PersistenceConfiguration.createSessionFactory().openSession(); Transaction
         * transaction = session.beginTransaction(); Promt promt = Promt.loadBySampleID(session, this.tempSampleID);
         * Sample sample = promt.findSample(this.tempSampleID); sample.addEvent(new Event4Acquisition(this,
         * jobInformation)); debug(thiz, "updating sample and adding events to them"); try { sample.update(session,
         * true); } catch (Throwable e) { e.printStackTrace(); err(this, e); // try this instead promt.update(session,
         * true); } transaction.commit(); session.close();
         */
        debug(thiz, "done updating sample and adding events to them");
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.setupx.repository.core.CoreObject#persistenceChilds(org.hibernate.Session, boolean)
     */
    public void persistenceChilds(Session session, boolean createIt) throws PersistenceActionException {
        Iterator iter = this.getParameterHibernate().iterator();

        while (iter.hasNext()) {
            ((AcquisitionParameter) iter.next()).update(session, createIt);
        }

        super.persistenceChilds(session, createIt);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();

        buffer.append("id: [" + this.getUOID() + "] " + "sample:[" + this.tempSampleID + "]");

        try {
            buffer.append(this.getParamter(AcquisitionParameter.NAME));
            buffer.append("  ");
            buffer.append(this.getParamter(AcquisitionParameter.TRAY));
            buffer.append("  ");
            buffer.append(this.getParamter(AcquisitionParameter.VIAL));
            buffer.append("  ");
        } catch (ParameterException e) {
            e.printStackTrace();
        }

        return buffer.toString();
    }

    /**
     * TODO: 
     * 
     * @param jobInformation
     *                TODO
     * @throws MappingError
     *                 TODO
     */
    protected final void add(JobInformation jobInformation) throws MappingError {
        Machine machine = jobInformation.getMachine();

        ACQMachineConfigurationIterator configurationIterator = machine.getConfiguration();

        String parameterLabel = "";
        String parameterValue = "";

        try {
            while (configurationIterator.hasNext()) {
                parameterLabel = (String) configurationIterator.next();
                parameterValue = jobInformation.get(parameterLabel);
                this.addParameter(new AcquisitionParameter(parameterLabel, parameterValue));
            }
        } catch (ParameterException e) {
            throw new MappingError("unable to map. The value of parameter:\"" + parameterLabel
                    + "\" is not valid in the configuration for machine :\"" + machine.getComment() + "\"", e);
        }
    }

    /**
     * TODO: 
     * 
     * @return TODO
     */
    protected int generateCounterID() {
        int id = 00;

        try {
            id = Integer.valueOf("" + ids.get(Util.getClassName(this))).intValue();
        } catch (Exception e) {
            debug("creating initial counter value for " + Util.getClassName(this));
        }

        id = id + 1;
        ids.put(org.setupx.repository.core.util.Util.getClassName(this), "" + (id));

        return id;
    }

    /**
     * @param hashtable
     * @return
     */
    private static Set map(Hashtable hashtable) {
        Set set = new HashSet();
        Enumeration enumeration = hashtable.elements();

        while (enumeration.hasMoreElements()) {
            set.add(enumeration.nextElement());
        }

        return set;
    }

    /**
     * @param set
     * @return
     */
    private static Hashtable map(Set set) {
        Hashtable result = new Hashtable();
        Iterator iterator = set.iterator();

        while (iterator.hasNext()) {
            AcquisitionParameter parameter = (AcquisitionParameter) iterator.next();
            result.put(parameter.getLabel(), parameter);
        }

        return result;
    }

    /**
     * TODO: 
     * 
     * @param sampleID
     *                TODO
     * @param jobInformation
     *                TODO
     * @param vialPosition
     *                TODO
     * @return TODO
     * @throws MappingError
     *                 TODO
     * @throws PersistenceActionUpdateException
     *                 TODO
     * @throws PersistenceActionFindException
     *                 TODO
     */
    private static AcquisitionSample newInstance(long sampleID, JobInformation jobInformation, int vialPosition)
            throws MappingError, PersistenceActionUpdateException, PersistenceActionFindException {
        AcquisitionSample acquisitionSample = new ExportMapping4Samples(sampleID, jobInformation, vialPosition).map();
        acquisitionSample.tempSampleID = sampleID;

        return acquisitionSample;
    }
}
