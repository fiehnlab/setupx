/**
 * ============================================================================ File:    MetaDataFile.java Package: org.setupx.repository.core.communication.importing.metadata cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.importing.metadata;

import org.setupx.repository.core.util.Util;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.util.Iterator;


class MetaDataFile extends MDCore {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private File file;
  private HSSFWorkbook workbook;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  MetaDataFile(File file) throws IOException {
    this.file = file;

    // open the xls file 
    FileInputStream fileInputStream = new FileInputStream(this.file);
    this.workbook = new HSSFWorkbook(fileInputStream);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param queryString
   *
   * @return null if nothing was found - otherwise a dataelement containing the information
   */
  public DataElement query(String queryString) {
    log(this, "query for " + queryString);

    DataElement dataElement = new DataElement();
    dataElement.setSourcefile(this.file);

    int numberOfSheets = workbook.getNumberOfSheets();

    for (int i = 0; i < numberOfSheets; i++) {
      debug("query on page " + i + " " + workbook.getSheetName(i));

      HSSFSheet sheet = this.workbook.getSheetAt(i);
      int validRow = query(sheet, queryString);

      if (validRow > -1) {
        debug("found data in " + workbook.getSheetName(i) + ".");
        dataElement.add(sheet, validRow, workbook.getSheetName(i));
      }
    }

    return dataElement;
  }

  /**
   * TODO: 
   *
   * @param sheet TODO
   * @param queryString TODO
   *
   * @return TODO
   */
  private static int query(HSSFSheet sheet, String queryString) {
    int numberOfRows = sheet.getLastRowNum();

    for (int i = 0; i < numberOfRows; i++) {
      if (query(sheet.getRow(i), queryString)) {
        return i;
      }
    }

    return -1;
  }

  /**
   * TODO: 
   *
   * @param row TODO
   * @param queryString TODO
   *
   * @return TODO
   */
  private static boolean query(HSSFRow row, String queryString) {
    try {
      Iterator iterator = row.cellIterator();

      while (iterator.hasNext()) {
        HSSFCell cell = (HSSFCell) iterator.next();
        String value = Util.getCellValue(cell);

        if (value.compareTo(queryString) == 0) {
          return true;
        }

        /*
           int similarity = Util.getSimilarity(value, queryString);
           if (similarity == 256) {
           } else {
             debug(MetaDataFile.class, "similarity: " + value + "  " + queryString + "  " + similarity);
             if (similarity < 1) {
               return true;
             }
           }
         */
      }
    } catch (Exception e) {
      warning(null, e.toString());
    }

    return false;
  }
}
