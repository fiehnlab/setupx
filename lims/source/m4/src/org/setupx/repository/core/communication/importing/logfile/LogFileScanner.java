/**
 * ============================================================================ File:    LogFileScanner.java Package: org.setupx.repository.core.communication.importing.logfile cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.importing.logfile;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.InitException;
import org.setupx.repository.core.communication.importing.filescanner.FileScannerException;
import org.setupx.repository.core.communication.importing.filescanner.FilescannerAbstractThread;

import java.io.File;

import java.util.Vector;


/**
 * Scanner for Logfils written by the LECO machines.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @deprecated
 */
public class LogFileScanner extends FilescannerAbstractThread {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private LogFile logfile;
  private String validExtension;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new LogFileScanner object.
   *
   * @throws InitException 
   */
  public LogFileScanner() throws InitException {
    // debugging settings.
    super(new File("/mnt/gctof/setupx/samplelogTEST"), 2);
    //super(new File(Config.DIRECTORY_LOGFILES_MACHINE), 20);
    this.validExtension = ".bin";
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.server.ximporter.FilescannerAbstractThread#action(java.util.Vector)
   */
  public void action(Vector modifiedFiles) throws FileScannerException {
    CoreObject.log(this, "number of modified files: " + modifiedFiles.size());

    for (int i = 0; i < modifiedFiles.size(); i++) {
      File file = (File) modifiedFiles.get(i);

      if (checkExtension(file)) {
        // -->logfile = new LogFile(file);
        //logfile.getNewSampleIDs();
      }
    }
  }

  /**
   * main
   */
  public static void main(String[] args) throws InitException {
    Thread scannerThread = new LogFileScanner();
  }

  /**
   * checks a file if its extension is valid
   *
   * @param file
   */
  private boolean checkExtension(File file) {
    String extension = org.setupx.repository.core.util.File.getExtension(file);

    // compare them
    if (extension.compareTo(this.validExtension) == 0) {
      CoreObject.debug(this, "found modified file: " + file.getName());

      return true;
    } else {
      return false;
    }
  }
}
