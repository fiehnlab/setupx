package org.setupx.repository.core.communication.technology.platform;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.server.persistence.SXQueryCacheObject;

/**
 * @hibernate.class table = "datafile"
 * @hibernate.discriminator column = "discriminator"
 */
public class Datafile extends TechnoCoreObject{
    
    private long sampleID;
    private String sourcePath;
    private TechnologyFileType technologyfiletype;
    private long submitterUserID;
    private Date detected;
    private Date created;
    
    
    private final static void myUpdate(TechnoCoreObject object, Session hqlSession, boolean createIt) {
        boolean existed = (object.getUOID() != 0);
        if (object.getUOID() == 0 && createIt){
            hqlSession.save(object);
            Logger.debug(object, "saved. uoid:" + object.getUOID());
        } else {
            hqlSession.update(object);
        }
        
        
        // update the SXQueryCacheobject on which a lot of the status messages relay
        if (object instanceof Datafile){
            Datafile datafile = (Datafile)object;
            // updating the cache
            long promtID;
            
            // in case it is an addition update the cache
            if (!existed){
                try {
                    promtID = new SXQuery(hqlSession).findPromtIDbySample(datafile.getSampleID());
                    int numberOfSamples = new SXQuery(hqlSession).determineNumberOfSamplesFinished(promtID);
                    new SXQueryCacheObject(promtID, numberOfSamples);
                } catch (PersistenceActionFindException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public void myUpdateMeAndChilds(Session hqlSession, boolean createIt){
        myUpdate(this, hqlSession, createIt);
    }
    
    public static TechnoCoreObject persistence_loadByID(Class class1, long id){
        return persistence_loadByID(class1, createSession(), id);
    }

    public static TechnoCoreObject persistence_loadByID(Class class1, Session s, long id){
        try {
            return (TechnoCoreObject) s.createCriteria(class1).add(Restrictions.eq("id", new Long(id))).list().get(0);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @deprecated 
     */
    public Datafile(){
        // only for persistence layer
    }
    
    public Datafile(long sampleID, File sourceFile) {
        this.sampleID = sampleID;
        this.setSourceFile(sourceFile);
        this.detected = new Date();
        this.created = new Date(sourceFile.lastModified());
        debug("created");
    }

    public static Datafile create(final long sampleID, File sourceFile) throws DatafileException{
        Datafile datafile = new Datafile(sampleID, sourceFile);
        datafile.checkFile();
        return datafile;
    }

    /**
     * checks that the datafile is not a directoy and can be read
     * @return
     * @throws DatafileException 
     */
    public void checkFile() throws DatafileException{
        if (this.getSourceFile().isDirectory()) throw new DatafileException("Datafile can not be a directory.");
        if (!this.getSourceFile().canRead()) throw new DatafileException("Datafile can not be read");
    }

    /**
     * @hibernate.property
     */
    public long getSampleID() {
        return sampleID;
    }

    /**
     * @hibernate.property
     */
    public String getSource() {
        return sourcePath;
    }

    /**
     * @hibernate.property
     */
    public long getSubmitterUserID() {
        return submitterUserID;
    }

    
    public void setSource(String source) {
        this.setSourceFile(new File(source));
    }

    public File getSourceFile() {
        return new File(sourcePath);
    }

    public void setSampleID(long sampleID) {
        this.sampleID = sampleID;
    }

    public void setSubmitterUserID(long submitterUserID) {
        this.submitterUserID = submitterUserID;
    }

    public void setSourceFile(File sourceFile) {
        this.sourcePath = sourceFile.getAbsolutePath();
    }
    
    /**
     * @deprecated - use {@link Datafile#getCreated()}
     * @return
     */
    public Date getCreationDate(){
        return this.getDateCreated();
        // return new Date(this.getSourceFile().lastModified());
    }
    
    public String toString(){
        return this.getSourceFile().getName() + " " + this.getCreationDate().toLocaleString();
    }
    
    /**
     * The setter method for Unique Object identifier.
     */
    public void setUOID(long lId) {
        uoid = lId;
    }

    /**
     * The getter method for Unique Object identifier.
     * 
     * <p>
     * The id is generated by the persistence layer.
     * </p>
     *
     * @return unique id for this object
     *
     * @hibernate.id column = "uoid" generator-class="native"
     */
    public long getUOID() {
        return (uoid);
    }
    
    public static List findDataFilesBySampleID(long sampleID, Session session){
        String query = "select * from datafile where sampleID = " + sampleID;
        return session.createSQLQuery(query).addEntity(Datafile.class).list();
    }

    /**
     * parent item
     *
     * @hibernate.many-to-one column="technologyfiletype" class = "org.setupx.repository.core.communication.technology.platform.TechnologyFileType"
     */
    public TechnologyFileType getTechnologyFileType() {
        return this.technologyfiletype;
    }
 
    public void setTechnologyFileType(TechnologyFileType technologyfiletype){
        this.technologyfiletype = technologyfiletype;
    }

    /**
     * @hibernate.property
     */
    public Date getDateDetected() {
        return detected;
    }

    /**
     * @hibernate.property
     */
    public Date getDateCreated() {
        return created;
    }

    public void setDateDetected(Date detected) {
        this.detected = detected;
    }

    public void setDateCreated(Date created) {
        this.created = created;
    }
}
