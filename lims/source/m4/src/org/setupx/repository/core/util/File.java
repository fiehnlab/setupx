/**

 * ============================================================================ File:    File.java Package: org.setupx.repository.core.util cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util;

//import Acme.JPM.Encoders.GifEncoder;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.ftp.FtpProtocolException;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.core.util.xml.XMLFile;
import org.setupx.repository.web.DownloadServlet;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

import sun.net.ftp.FtpClient;

import java.awt.Image;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UTFDataFormatException;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.8 $
 */
public class File extends CoreObject {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private File() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * the extension of a file
   *
   * @param file the file
   *
   * @return the extension
   */
  public static String getExtension(java.io.File file) {
    // determine doctype by finding the file extension
    String filename = file.getName();
    int dotPos = filename.lastIndexOf(".");
    String fileExtension = filename.substring(dotPos + 1, filename.length());

    // Logger.debug(thiz, fileExtension);
    return fileExtension;
  }

  /**
   * reads the content of a file
   *
   * @param i_file the file that will be read
   *
   * @return the content of the file
   *
   * @throws java.io.IOException there were problems accessing the file
   */
  public static String getFileContent(java.io.File i_file)
    throws java.io.IOException {
    if (!i_file.exists()) {
      throw new IOException("File " + i_file.toString() + " does not exist.");
    }

    FileReader inFile = getFileContentReader(i_file);

    // file size
    int _sizeFile = (int) i_file.length();

    //  number of character
    int _char_read = 0;

    char[] readData = new char[_sizeFile];

    while (inFile.ready()) {
      _char_read += inFile.read(readData, _char_read, _sizeFile - _char_read);
    }

    inFile.close();

    String _text_read = new String(readData, 0, _char_read);

    return _text_read;
  }

  /**
   * checks if the Filenames given in the Vector are available and return a vector of all the Filenames that do not exist
   *
   * @param fileNames the Filenames will be looked for
   *
   * @return a Vector containing all FilesNames that were not found
   */
  public static Vector available(Vector fileNames) {
    java.io.File file = null;

    //Logger.debug(thiz, " length: " + fileNames.size());
    Vector vectorFound = new Vector();
    Vector vectorNotFound = new Vector();

    for (int i = 0; i < fileNames.size(); i++) {
      String filename = (String) fileNames.get(i);
      String realFilename = filename.substring(0, filename.length() - 1); //filename.substring(0,
                                                                          // filename.lastIndexOf(".xml")
                                                                          // -
                                                                          // 1).concat("
                                                                          // .xml");

      file = new java.io.File(realFilename);

      //Logger.debug(thiz,">" + file.getAbsolutePath() + "< " + file.exists());
      if (file.exists()) {
        XMLFile xmlFile = null;

        try {
          xmlFile = new XMLFile(file);
        } catch (SAXException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } catch (UTFDataFormatException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }

        NodeList list = xmlFile.getDocument().getElementsByTagName("class");

        if (list.getLength() == 1) {
          System.out.print('.');
        } else {
          System.out.print('c'); // list.getLength());
        }

        vectorFound.add(fileNames.get(i));
      } else {
        System.out.print(' ');
        vectorNotFound.add(fileNames.get(i));

        //Logger.debug(thiz, realFilename);
      }

      if ((i % 100) == 0) {
        System.out.print("\n");
      }
    }

    return vectorNotFound;
  }

  /**
   * @param file
   *
   * @throws IOException
   */
  public static void checkFile(java.io.File file) throws IOException {
    if (file == null) {
      throw new IOException("filename is null");
    }

    if (!file.exists()) {
      throw new FileNotFoundException("File " + file.getAbsolutePath() + " does not exist");
    }

    if (!file.canRead()) {
      throw new IOException("unable to read  " + file.getAbsolutePath() + " ");
    }
  }

  /**
   * checks if the files are available
   *
   * @param directoryName TODO
   * @param filenamesFile TODO
   *
   * @return TODO
   *
   * @throws IOException TODO
   * @throws NullPointerException TODO
   */
  public static Vector checkFileExisting(String directoryName, java.io.File filenamesFile)
    throws IOException {
    if (!(new java.io.File(directoryName).isDirectory())) {
      throw new NullPointerException("no directory! ");
    }

    // determine the files that will be looked for
    String fileContent = getFileContent(filenamesFile);
    String lineSeperator = "\n";

    // cuts the file into tokens representing a single line in the file
    Vector filenames = new Vector();
    StringTokenizer stringTokenizerAll = new StringTokenizer(fileContent, lineSeperator);

    while (stringTokenizerAll.hasMoreElements()) {
      String filename = stringTokenizerAll.nextToken();
      filenames.add(directoryName + java.io.File.separator + filename);

      //Logger.debug(thiz, directoryName + java.io.File.separator +
      // stringTokenizerAll.nextToken());
    }

    Vector result = available(filenames);

    debug(null, "num of files in directory: " + filenames.size() + "    found fields " + result.size() + "  " + ((result.size() * 100) / filenames.size()));

    return result;
  }

  /**
   * checks if there is a subfolder with this name - if not - it will be <b>created </b>
   *
   * @param backupFolder
   * @param year
   *
   * @throws UtilException
   * @throws
   */
  public static void checkFolder(java.io.File backupFolder, int subfoldername)
    throws UtilException {
    /*
     * slow way File[] array = backupFolder.listFiles();
     *
     * boolean exists = false;
     *
     * for (int i = 0; i < array.length; i++) { Logger.debug(null,
     * (array[i].getName().compareTo(subfoldername+"")) + " " +
     * array[i].getName() + " " + subfoldername+"" ); if (array[i].isDirectory() &&
     * (array[i].getName().compareTo(subfoldername+"") == 0)) exists = true; }
     * if (exists) return;
     */
    backupFolder = new java.io.File(backupFolder.getAbsolutePath() + java.io.File.separator + subfoldername);

    if (backupFolder.exists() && backupFolder.isDirectory()) {
      return;
    }

    if (backupFolder.exists() && backupFolder.isFile()) {
      throw new UtilException("the name for an backupfolder is already used by an other File" + backupFolder);
    }

    // if it does not exist, it will be created
    debug(null, "creating: " + backupFolder);
    backupFolder = new java.io.File(backupFolder.getAbsolutePath());

    backupFolder.mkdir();
  }

  /**
     * COPY a file.
     * <p>
     * creates a copy of a file at a different location. the source file will be deleted.
     * </p>
     * 
     * @param targetDirectory
     *                folder the file will be copied to.
     * @param file
     *                the file that will be copied.
     * @return the new File in the new directory
     * @throws FileNotFoundException
     *                 unable to find file
     * @throws IOException
     */
    public static java.io.File copy(java.io.File targetFolder, java.io.File file) throws FileNotFoundException,
            IOException {
        if (!targetFolder.exists()) {
            targetFolder.mkdir();
        }
        java.io.File newFile = new java.io.File(targetFolder.getAbsoluteFile() + java.io.File.separator
                + file.getName());
        copyFile(file, newFile);

        return newFile;
        //return moveFile(targetFolder, file, false);
    }

  /**
   * TODO: 
   *
   * @param in TODO
   * @param out TODO
   *
   * @throws IOException TODO
   */
  public static void copyFile(java.io.File in, java.io.File out)
    throws IOException {
    FileInputStream fis = new FileInputStream(in);
    FileOutputStream fos = new FileOutputStream(out);
    byte[] buf = new byte[1024];
    int i = 0;

    while ((i = fis.read(buf)) != -1) {
      fos.write(buf, 0, i);
    }

    fis.close();
    fos.close();
  }

  /**
   * TODO: 
   *
   * @param file TODO
   *
   * @return TODO
   */
  public static String determineContentType(java.io.File file) {
    if (getExtension(file).toLowerCase().compareTo("xls") == 0) {
      return DownloadServlet.CONTENT_TYPE_XLS;
    }

    if (getExtension(file).toLowerCase().compareTo("zip") == 0) {
      return DownloadServlet.CONTENT_TYPE_ZIP;
    }

    if (getExtension(file).toLowerCase().compareTo("doc") == 0) {
      return DownloadServlet.CONTENT_TYPE_DOC;
    }

    if (getExtension(file).toLowerCase().compareTo("txt") == 0) {
      return DownloadServlet.CONTENT_TYPE_TXT;
    }

    if (getExtension(file).toLowerCase().compareTo("pdf") == 0) {
      return DownloadServlet.CONTENT_TYPE_PDF;
    }

    return DownloadServlet.CONTENT_TYPE_TXT;
  }

  /**
   * TODO: 
   *
   * @param file TODO
   *
   * @return TODO
   */
  public static String determineFileIMG(java.io.File file) {
    if (getExtension(file).toLowerCase().compareTo("xls") == 0) {
      return "pics/xls.gif";
    }

    if (getExtension(file).toLowerCase().compareTo("zip") == 0) {
      return "pics/zip.gif";
    }

    if (getExtension(file).toLowerCase().compareTo("doc") == 0) {
      return "pics/txt.gif";
    }

    if (getExtension(file).toLowerCase().compareTo("txt") == 0) {
      return "pics/txt.gif";
    }

    if (getExtension(file).toLowerCase().compareTo("pdf") == 0) {
      return "pics/pdf.gif";
    }

    return "pics/txt.gif";
  }

  /**
   * @deprecated use wget instead
   */
  public static void download(URL url, java.io.File file)
    throws IOException {
    // Open a stream to the file using the URL.
    InputStream in = url.openStream();

    BufferedReader dis = new BufferedReader(new InputStreamReader(in));
    StringBuffer fBuf = new StringBuffer();
    String line;

    while ((line = dis.readLine()) != null) {
      fBuf.append(line + "\n");
    }

    in.close();

    File.storeData2File(file, fBuf.toString());
  }

  /**
   * checks how old a file is
   *
   * @param file
   *
   * @return the age of the file in milliseconds
   */
  public static long howOld(java.io.File file) {
    long lastModi = file.lastModified();
    long today = new Date().getTime();
    debug(null, "last modified: " + lastModi);
    debug(null, "today : " + today);

    long diff = today - lastModi;
    debug(null, "diff: " + diff);

    return diff;
  }

  /**
   * MOVE a file.
   * 
   * <p>
   * creates a copy of a file at a different location and removes the source.
   * </p>
   *
   * @param targetDirectory folder the file will be copied / moved to.
   * @param file the file that will be moved.
   *
   * @return the new File in the new directory
   *
   * @throws FileNotFoundException unable to find file
   * @throws IOException
   */
  public static void move(java.io.File targetFolder, java.io.File file)
    throws FileNotFoundException, IOException {
    moveFile(targetFolder, file, true);
  }

  /**
   * reads a csv file and stores every single line in the vector
   *
   * @param file the csv file
   *
   * @return the vector containing all lines
   *
   * @throws IOException
   */
  public static Vector readCSV(java.io.File file) throws IOException {
    String lineSeperator = "\n";
    Vector result = new Vector();

    StringTokenizer tokenizerAll = new StringTokenizer(File.getFileContent(file), lineSeperator);

    while (tokenizerAll.hasMoreElements()) {
      String line = (String) tokenizerAll.nextElement();
      line = line.substring(0, line.length() - 1);
      result.add(line);
    }

    return result;
  }

  /**
   * TODO: 
   *
   * @param directory TODO
   * @param searchString TODO
   * @param replaceString TODO
   *
   * @throws FileNotFoundException TODO
   * @throws IOException TODO
   */
  public static void rename(java.io.File directory, String searchString, String replaceString)
    throws FileNotFoundException, IOException {
    java.io.File[] files = directory.listFiles();

    for (int i = 0; i < files.length; i++) {
      java.io.File file = files[i];
      String name = file.getName();
      String newName = Util.replace(name, searchString, replaceString);
      debug(File.class, "rename " + name + " to " + newName);

      java.io.File targetFile = new java.io.File(file.getParentFile() + java.io.File.separator + newName);

      //Logger.debug(thiz, targetFile.getAbsolutePath());
      storeData2File(targetFile, getFileContent(file));
    }
  }

  /**
   * replace a string in a file by another string.
   *
   * @param file file for the search and replace
   * @param search searchstring
   * @param replace replacestring
   *
   * @throws IOException
   */
  public static void replace(java.io.File file, String search, String replace)
    throws IOException {
    String s = getFileContent(file);

    int start = 0;
    int pos = 0;
    StringBuffer result = new StringBuffer(s.length());

    while ((pos = s.indexOf(search, start)) >= 0) {
      result.append(s.substring(start, pos));
      result.append(replace);
      start = pos + search.length();
    }

    result.append(s.substring(start));

    storeData2File(file, result.toString());
  }

  /**
   * Writes a String into a File.
   *
   * @param i_File target where the String will be written to
   * @param i_content String to write
   *
   * @throws java.io.FileNotFoundException File was not found
   * @throws java.io.IOException Unable to open File
   */
  public static void storeData2File(java.io.File i_File, String i_content)
    throws java.io.FileNotFoundException, java.io.IOException {
    FileWriter fileWriter = new FileWriter(i_File);
    BufferedWriter buffWriter = new BufferedWriter(fileWriter);
    buffWriter.write(i_content);
    buffWriter.close();
    fileWriter.close();
  }

  /**
   * @param file
   * @param errors
   *
   * @throws IOException
   * @throws FileNotFoundException
   */
  public static void storeData2File(java.io.File file, HashSet errors)
    throws FileNotFoundException, IOException {
    StringBuffer buffer = new StringBuffer();
    Iterator iterator = errors.iterator();

    while (iterator.hasNext()) {
      buffer.append((String) iterator.next());
    }

    storeData2File(file, buffer.toString());
  }

  /**
   * stores a document in a file.
   *
   * @param file
   * @param document
   *
   * @throws TransformerConfigurationException
   * @throws TransformerFactoryConfigurationError
   * @throws FileNotFoundException
   * @throws TransformerException
   * @throws IOException
   */
  public static void storeDocument(java.io.File file, Document document)
    throws TransformerConfigurationException, TransformerFactoryConfigurationError, FileNotFoundException, TransformerException, IOException {
    Transformer transformer = TransformerFactory.newInstance().newTransformer();
    DOMSource source = new DOMSource(document);
    FileOutputStream os = new FileOutputStream(file);
    StreamResult result = new StreamResult(os);
    transformer.transform(source, result);
    os.close();
  }

  /**
   * store an image in a GIF-file - deactivated cause ACME libs are not in
   *
   * @param file the file the image will be stored in
   * @param img the image itself
   *
   * @throws IOException problems storing the file
   */
  public static void storeImageGIF(java.io.File file, Image img)
    throws IOException {
    OutputStream output = new BufferedOutputStream(new FileOutputStream(file));
//    new GifEncoder(img, output);
  }

  /**
   * like wget under linux
   * 
   * <p>
   * in case you want request a password protected file - use <code>wgetFTP</code>
   * </p>
   *
   * @param remoteFile the file on the server
   * @param target the local file where the remote file will be stored at
   * @param password2
   * @param username2
   *
   * @see File#wgetFTP(URL, java.io.File, String, String)
   */
  public static void wget(URL remoteFile, java.io.File target, String username2, String password2)
    throws IOException {
    String host = remoteFile.getHost();
    String path = remoteFile.getPath();

    //String username = Config.FTP_SYSTEM_USERNAME;
    //String password = Config.FTP_SYSTEM_PASSWORD;
    int lastSlash = path.lastIndexOf('/');
    String filename = path.substring(lastSlash + 1);
    String directory = path.substring(0, lastSlash);

    FtpClient client = new FtpClient(host);
    if (username2.length() != 0){
        Logger.debug(File.class, "logging in as user: " + username2);
        client.login(username2, password2);
    }
    client.binary();
    client.cd(directory);

    InputStream is = client.get(filename);
    BufferedInputStream bis = new BufferedInputStream(is);

    OutputStream os = new FileOutputStream(target);
    BufferedOutputStream bos = new BufferedOutputStream(os);

    byte[] buffer = new byte[1024];
    int readCount;

    System.out.println("receiving: " + filename + " from " + remoteFile);

    while ((readCount = bis.read(buffer)) > 0) {
      bos.write(buffer, 0, readCount);
    }

    bos.close();
  }

  /**
   * ftp transfer of a file
   *
   * @param remoteFile the file on the server
   * @param target the local file where the remote file will be stored at
   * @param username username used to log in
   * @param password password used to log in
   *
   * @throws IOException problems storing the file
   * @throws FtpProtocolException the url given is illegal
   */
  public static void wgetFTP(final URL remoteFile, final java.io.File target, final String username, final String password)
    throws IOException {
    if (remoteFile.getProtocol().compareTo("ftp") != 0) {
      throw new FtpProtocolException("requieres an ftp protocol - your protocol is: " + remoteFile.getProtocol());
    }

    try {
      String port = "";

      if (remoteFile.getPort() == -1) {
        port = "";
      } else {
        port = ":" + port;
      }

      String urlString = remoteFile.getProtocol() + "://" + username + ":" + password + "@" + remoteFile.getHost() + port + remoteFile.getFile();
      debug(null, urlString);

      URL url = new URL(urlString);
      wget(url, target, username, password);
    } catch (MalformedURLException e) {
      throw new FtpProtocolException("unable to create ftp url based on your url: " + remoteFile);
    }
  }

  /**
   * TODO: 
   *
   * @param i_file TODO
   *
   * @return TODO
   *
   * @throws IOException TODO
   * @throws NullPointerException TODO
   */
  private static FileReader getFileContentReader(java.io.File i_file)
    throws IOException {
    if (i_file == null) {
      throw new NullPointerException("the file " + i_file.getName() + " is null");
    }

    if (i_file.isDirectory()) {
      throw new IOException("given File is a directory");
    }

    FileReader inFile = new FileReader(i_file);

    return inFile;
  }

  /**
   * MOVE or COPY of a file.
   * 
   * <p>
   * creates a copy of a file at a different location. in case you set removeSource <code>true</code> the source file will be deleted, meaning it works like a <code>copy</code>.
   * </p>
   *
   * @param targetDirectory folder the file will be copied / moved to.
   * @param file the file that will be moved.
   * @param removeSource if true the original source will be deleted after copy was successful
   *
   * @return the new File in the new directory
   *
   * @throws FileNotFoundException unable to find file
   * @throws IOException
   */
  private static java.io.File moveFile(java.io.File targetDirectory, java.io.File file, boolean removeSource)
    throws FileNotFoundException, IOException {
    // create a copy of the sourcefile
    java.io.File targetFile = new java.io.File(targetDirectory.getAbsolutePath() + java.io.File.separator + file.getName());

    if (!targetDirectory.exists()) {
      targetDirectory.mkdir();
    }

    //Logger.debug(thiz, targetFile.getAbsolutePath());
    storeData2File(targetFile, getFileContent(file));

    // remove the old one from the importing directory
    if (removeSource) {
      file.delete();
    }

    return targetFile;
  }

  
  /**
   * sort the entires of a file
   * @param file
 * @throws IOException 
   */
    public static void sortEntries(java.io.File file) throws IOException {
        
        // open the file
        // get the content
        String content = getFileContent(file);
        
        StringTokenizer stringTokenizer = new StringTokenizer(content, "\n");
        
        String[] stringArray = new String[stringTokenizer.countTokens()];
        
        for (int i = 0; i < stringArray.length; i++) {
            stringArray[i] = stringTokenizer.nextToken();
        }

        // order line by line
        java.util.Arrays.sort(stringArray);
        
        
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < stringArray.length; i++) {
            String string = stringArray[i];
            buffer.append(string + "\n");
        }
        
        // store to the same file
        storeData2File(file, buffer.toString());
    }
}
