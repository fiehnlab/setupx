package org.setupx.repository.core.query;

import java.io.File;
import java.util.Vector;

import org.hibernate.Session;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.technology.platform.Datafile;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.util.Link;

public class DatafileQueryMasterAnswer extends QueryMasterAnswer {

    
    private Datafile datafile;


    public DatafileQueryMasterAnswer(Datafile datafile) {
        this.datafile = datafile;
    }


    public boolean checkAccessRestriction(UserDO userDO) {
        if (!userDO.isLabtechician()){
            Session session = CoreObject.createSession();
            
            boolean access = false;
            try {
                if (new SXQuery(session).findPromtUserAccessRightForUserID(
                        userDO.getUOID(), 
                        (new SXQuery(session).findPromtIDbySample(this.datafile.getSampleID()))
                        ).getAccessCode() > 20){
                    access = true;
                }
            } catch (PersistenceActionFindException e) {
            }
            return access;
        } else {
            return true;
        }
    }

    
    public String getIcon() {
        return "pics/Publication-pic.gif";
    }

    public String getInfo1() {
        return "created: " + this.datafile.getCreationDate().toLocaleString();
    }

    
    public String getInfo2() {
        return this.datafile.getTechnologyFileType().getTechnology().getLabel() + " - " + this.datafile.getTechnologyFileType().getLabel();
    }

    
    public String getInfo3() {
        return "" + this.datafile.getTechnologyFileType().getDescribtion();
    }

    
    public String getLabel() {
        return "Datafile: " + new File(this.datafile.getSource()).getName() + " [sample: " + this.datafile.getSampleID() + "]";
    }

    
    public Vector getRelatedObjectLinks() {
        Vector vector = new Vector();
        
        
        vector.add(new Link("Sample: " + this.datafile.getSampleID(), "sample_detail.jsp?id=" + this.datafile.getSampleID()));
        vector.add(new Link("Datafiles ", "techno_sampleFiles.jsp?id=" + this.datafile.getSampleID()));
        vector.add(new Link("Technology: " + this.datafile.getTechnologyFileType().getTechnology().getLabel(), 
                "techno_technology.jsp?id=" + this.datafile.getTechnologyFileType().getTechnology().getUOID()));
        return vector;
    }

    
    public boolean isAccessRestricted() {
        return false;
    }

}
