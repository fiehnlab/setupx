package org.setupx.repository.core.communication.technology.scanner;


public class ScanningProcessException extends Exception {

    public ScanningProcessException(String string, Exception e) {
        super(string, e);
    }

}
