/**
 * ============================================================================ File:    SimpleScannerThread.java Package: org.setupx.repository.core.communication.importing.filescanner cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.importing.filescanner;

import org.setupx.repository.core.InitException;
import org.setupx.repository.core.util.logging.Logger;

import java.io.File;

import java.util.Vector;


/**
 * just a dummy to test the functions of the FilescannerAbstractThread
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.2 $
 */
public class SimpleScannerThread extends FilescannerAbstractThread {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new FirstScanner object.
   *
   * @param i_directoryToScan 
   * @param timeBetweenScanning 
   *
   * @throws InitException 
   */
  public SimpleScannerThread(File i_directoryToScan, int timeBetweenScanning)
    throws InitException {
    super(i_directoryToScan, timeBetweenScanning);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param modifiedFiles TODO
   */
  public void action(Vector modifiedFiles) {
    Logger.log(this, "got NEW ones " + modifiedFiles.size());
  }

  /**
   * TODO: 
   *
   * @param args TODO
   *
   * @throws InitException TODO
   */
  public static void main(String[] args) throws InitException {
    Thread scannerThread = new SimpleScannerThread(new File("C:\\testing"), 1000);
  }
}
