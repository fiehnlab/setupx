package org.setupx.repository.core.communication.technology.platform.debugging;

import org.hibernate.Session;

public abstract class MyObject {

    public abstract long getUOID();
    
    public final static void update(MyObject object, Session hqlSession, boolean createIt) {
        if (object.getUOID() == 0 && createIt){
            hqlSession.save(object);
        } else {
            hqlSession.update(object);
        }
    }
}
