package org.setupx.repository.core.communication.status;

import org.setupx.repository.core.CoreObject;

/**
 * Status for a single Samples. The Status defines a certain stage that a sample is in 
 * while being in the lab or even while it is being shipped.
 * 
 * @author scholz
 */
public abstract class Status extends CoreObject{

    // related sample
    protected long sampleID;
    
    public Status(long sampleID) {
        this.sampleID = sampleID;
    }

    public Status() {
    }

    /**
     * create a short message - what is the status
     * @return
     */
    public abstract String getMessage();
    
    public abstract String createDisplayString();
    
    /*
     * (non-Javadoc)
     * @see org.setupx.repository.core.CoreObject#toString()
     */
    public String toString() {
        return "message: " + this.getMessage() + "    " + this.createDisplayString();
    }
}
