package org.setupx.repository.core;

import org.setupx.repository.server.persistence.PersistenceActionException;

public class PersistenceActionInconsistentException extends PersistenceActionException {

    public PersistenceActionInconsistentException(String string, Throwable e) {
        super(string, e);
    }

    public PersistenceActionInconsistentException(String string) {
        super(string);
    }

    public PersistenceActionInconsistentException(CoreObject coreObjectThatCanNotBeUpdated, String s) {
        super(org.setupx.repository.core.util.Util.getClassName(coreObjectThatCanNotBeUpdated) + "  :  " + s);
    }

}
