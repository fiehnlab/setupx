package org.setupx.repository.core.communication;


public class BackgroundThread extends Thread {
    
    public BackgroundThread(String string) {
        super(string);
        // register each thread
        BackgroundThreadMonitor.add(this);
    }
    
    public BackgroundThread() {
        super();
        // register each thread
        BackgroundThreadMonitor.add(this);
    }
    
}
