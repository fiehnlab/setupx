package org.setupx.repository.core.communication.technology.platform;

public class LocationException extends Exception {

    /**
     * 
     */
    public LocationException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param arg0
     * @param arg1
     */
    public LocationException(String arg0, Throwable arg1) {
        super(arg0, arg1);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param arg0
     */
    public LocationException(String arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param arg0
     */
    public LocationException(Throwable arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    }


}
