/**
 * ============================================================================ File:    OBOTerm.java Package: org.setupx.repository.core.communication.ontology.obo cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ontology.obo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.setupx.repository.core.util.Util;


/**
 * Title:   This class is represents a term, as read in through an OBO file. It is designed to work with the OBOFileReader class. Copyright:    Copyright (c) 2005 Company: University of Washington
 *
 * @author: Adam Silberfein
 * @version 1.0
 */
public class OBOTerm {
    //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public boolean isObsolete = false;
    public boolean isRoot = true; // they are all a root until they get a parent...
    private ArrayList isAParents = new ArrayList(); // An ArrayList of ids
    private ArrayList synonyms = new ArrayList(); // A list of Strings
    private ArrayList xrefs = new ArrayList(); // The DAG-EDIT concept of DbxRefs
    private HashMap relationships = new HashMap(); // The key is relationship name (e.g. "part_of"), value is an ArrayList of Strings (ids)  
    private String comment = "";
    private String defReference = "";
    private String definition = "";
    private String id = ""; // e.g. "000104"
    private String prefix = ""; // Something like "GO" or "FMA"
    private String subset = ""; // This refers to the idea of "Category" in DAG-EDIT
    private String term = ""; // e.g. "Cell nucleus"
    private String[] defTokens;
    public HashSet reverseRelated;

    //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new OBOTerm object.
     */
    public OBOTerm() {
    }

    /**
     * Creates a new OBOTerm object.
     *
     * @param prefix 
     * @param id 
     * @param term 
     */
    public OBOTerm(String prefix, String id, String term) {
        this.prefix = prefix;
        this.id = id;
        this.term = term;
    }

    //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * TODO: 
     *
     * @param comment TODO
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public String getComment() {
        return this.comment;
    }

    /**
     * TODO: 
     *
     * @param ref TODO
     */
    public void setDefReference(String ref) {
        this.defReference = ref;
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public String getDefReference() {
        return this.defReference;
    }

    /**
     * TODO: 
     *
     * @param def TODO
     */
    public void setDefinition(String def) {
        this.definition = def;
        this.defTokens = def.split(" ");

        //System.out.println("Def: " + defTokens[2]);
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public String getDefinition() {
        return this.definition;
    }

    /**
     * TODO: 
     *
     * @param i TODO
     */
    public void setId(String i) {
        this.id = i;

        if (Integer.parseInt(i) != 0) {
            this.isRoot = false;
        }
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public String getId() {
        return this.id;
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public ArrayList getIsAParents() {
        return this.isAParents;
    }

    // set functions
    public void setPrefix(String p) {
        this.prefix = p;
    }

    // get functions
    public String getPrefix() {
        return this.prefix;
    }

    /**
     * TODO: 
     *
     * @param relName TODO
     *
     * @return TODO
     */
    public ArrayList getRelationship(String relName) {
        Object object = this.relationships.get(relName);
        if (object == null) return new ArrayList();
        return (ArrayList) object;
    }

    /**
     * TODO: 
     *
     * @param subset TODO
     */
    public void setSubset(String subset) {
        this.subset = subset;
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public String getSubset() {
        return this.subset;
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public ArrayList getSynonyms() {
        return this.synonyms;
    }

    /**
     * TODO: 
     *
     * @param t TODO
     */
    public void setTerm(String t) {
        this.term = t;
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public String getTerm() {
        return this.term;
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public ArrayList getXrefs() {
        return this.xrefs;
    }

    // Add functions
    public void addParent(String p) {
        this.isAParents.add(p);
        this.isRoot = false;
    }

    /**
     * TODO: 
     *
     * @param relName TODO
     * @param val TODO
     */
    public void addRelationship(String relName, String val) //(K,V)
    {
        // martin scholz:
        // some have identifiers for the ontologie in front of them
        if (val.lastIndexOf(':') > 0){
            val = val.substring(val.lastIndexOf(':') + 1);
        }

        ArrayList rel = (ArrayList) relationships.get(relName); //Returns the value to which the specified key is mapped in this identity hash map

        if (rel == null) {
            rel = new ArrayList();
        }

        rel.add(val);
        relationships.put(relName, rel); //Associates the specified value with the specified key in this map. If the map previously contained a mapping for this key, the old value is replaced.   
    }

    /**
     * TODO: 
     *
     * @param s TODO
     */
    public void addSynonym(String s) {
        this.synonyms.add(s);
    }

    /**
     * TODO: 
     *
     * @param x TODO
     */
    public void addXref(String x) {
        this.xrefs.add(x);
    }

    /**
     * TODO: 
     *
     * @param col TODO
     *
     * @return TODO
     */
    public String createHTML(int col) {
        StringBuffer buf = new StringBuffer();

        buf.append("\n<!-- " + Util.getClassName(this) + " -->\n<tr>\n");
        buf.append("<td><br><font size=\"-3\">&nbsp &nbsp &nbsp " + this.getTerm() + "</font></span></td>");
        buf.append("\n<td colspan=\"" + (col - 1) + "\">");
        buf.append("<br>");
        buf.append("\nPrefix: " + prefix + "\n");
        buf.append("ID: " + id + "\n");
        buf.append("<br>");
        buf.append("Term: " + term + "\n");
        buf.append("<br>");
        buf.append("Def: " + definition + "\n");
        buf.append("<br>");
        buf.append("Ref: " + defReference + "\n");
        buf.append("<br>");
        buf.append("Comment: " + comment + "\n");
        buf.append("<br>");
        buf.append("Subset: " + subset + "\n");
        buf.append("<br>");
        buf.append("Is-a parent(s): " + isAParents + "\n");
        buf.append("<br>");
        buf.append("Synonyms: " + synonyms + "\n");
        buf.append("<br>");
        buf.append("xrefs: " + xrefs + "\n");
        buf.append("<br>");
        buf.append("Relationships: " + relationships + "\n\n");
        buf.append("</td></tr>\n");

        return buf.toString();
    }

    /**
     * TODO: 
     *
     * @return TODO
     */
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("\nPrefix: " + prefix + "\n");
        buf.append("ID: " + id + "\n");
        buf.append("Term: " + term + "\n");
        buf.append("Def: " + definition + "\n");
        buf.append("Ref: " + defReference + "\n");
        buf.append("Comment: " + comment + "\n");
        buf.append("Subset: " + subset + "\n");
        buf.append("Is-a parent(s): " + isAParents + "\n");
        buf.append("Synonyms: " + synonyms + "\n");
        buf.append("xrefs: " + xrefs + "\n");
        buf.append("Relationships: " + relationships + "\n\n");

        return buf.toString();
    }
}
