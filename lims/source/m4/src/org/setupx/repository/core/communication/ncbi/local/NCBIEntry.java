/**
 * ============================================================================ File:    NCBIEntry.java Package: org.setupx.repository.core.communication.ncbi.local cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ncbi.local;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.ncbi.NCBIConnector;
import org.setupx.repository.core.communication.ncbi.NCBIException;
import org.setupx.repository.core.communication.ncbi.NCBIFindException;
import org.setupx.repository.core.communication.ncbi.NCBI_Tree;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.validation.ValidationAnswer;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.19 $
 */
public class NCBIEntry extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private NCBIEntry parent;
  private NCBI_Name name;
  private NCBI_Node node;
  private Vector cacheSplitpointChilds = null;
  private Vector childs;
  private int speciesType = 0;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final int HUMAN = 1;
  public static final int PLANT = 2;
  public static final int ANIMAL = 3;
  public static final int MIRCOORGANISM = 4;
  public static final int UNKNOWN = 99;

  /** definition for species in the ncbi */
  public static final String SPECIES = "species";
  public static final String SUBSPECIES = "subspecies";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new NCBIEntry object.
   *
   * @param name 
   * @param node 
   */
  protected NCBIEntry(NCBI_Name name, NCBI_Node node) throws NCBICreateException {
    this.name = name;
    this.node = node;

    if ((node == null) || (name == null)) {
      throw new NCBICreateException("Can not create object with null references.  name:" + name + "  node:" + node);
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param pos TODO
   *
   * @return TODO
   *
   * @throws NCBIFindException TODO
   */
  public NCBIEntry getChild(int pos) throws NCBIFindException {
    return (NCBIEntry) this.getChilds().get(pos);
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws NCBIFindException TODO
   */
  public Vector getChilds() throws NCBIFindException {
    if (this.childs == null) {
      //log("init the child objects");
      Vector childIDs = LocalDatabaseInstance.determineChildIDs(this);
      this.childs = new Vector();

      for (int i = 0; i < childIDs.size(); i++) {
        try {
          this.childs.add(NCBIConnector.determineNCBI_Information(Integer.parseInt((String) childIDs.get(i))));
        } catch (NumberFormatException e) {
          throw new NCBIFindException(e);
        } catch (NCBIException e) {
          throw new NCBIFindException(e);
        }
      }
    }

    return this.childs;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getName() {
    return this.name.getName();
  }

  /**
   * get the parent element
   *
   * @return the parent entry - null if the rootelement is already reached.
   *
   * @throws NCBIFindException
   */
  public NCBIEntry getParent() throws NCBIFindException {
    //debug ("parentid: " + node.getParentID());
    if (this.parent == null) {
      //debug("requesting parent - parentid: " + node.getParentID());
      try {
        parent = NCBIConnector.determineNCBI_Information(Integer.parseInt(node.getParentID()));
      } catch (NCBIFindException e) {
        throw e;
      } catch (NumberFormatException e) {
        throw new NCBIFindException(e);
      } catch (NCBIException e) {
        throw new NCBIFindException(e);
      }
    }

    return this.parent;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getRank() {
    return node.getRank();
  }

  /**
   * if this is a species or a subspecies.
   *
   * @return
   */
  public boolean isSpecies() {
    //debug("species: " + (this.getRank().compareTo(NCBIEntry.SPECIES) == 0) + "  subspecies: " + (this.getRank().compareTo(NCBIEntry.SUBSPECIES) == 0));
    //debug("result: " + ((this.getRank().compareTo(NCBIEntry.SPECIES) == 0) || (this.getRank().compareTo(NCBIEntry.SUBSPECIES) == 0)));
    return (this.getRank().compareTo(NCBIEntry.SPECIES) == 0) || (this.getRank().compareTo(NCBIEntry.SUBSPECIES) == 0);
  }

  /**
   * determine the type of species.
   *
   * @return an int representing the type of species.
   *
   * @see NCBIEntry#ANIMAL
   * @see NCBIEntry#HUMAN
   * @see NCBIEntry#PLANT
   * @see NCBIEntry#UNKNOWN
   */
  public int getSpeciesType() {
    if (!this.isSpecies()) {
      return UNKNOWN;
    }

    if ((this.speciesType == 0)) {
      this.speciesType = NCBI_Classifier.classify(this);
    }

    return this.speciesType;
  }

  /**
   * @return String representing the Species
   */
  public String getSpeciesTypeName() {
      return getSpeciesTypeName(this.getSpeciesType());
  }
  
  /**
   * @return String representing the Species
   */
  public static String getSpeciesTypeName(int speciesType) {
    if (speciesType == NCBIEntry.ANIMAL) {
      return "animal";
    }

    if (speciesType == NCBIEntry.PLANT) {
      return "plant";
    }

    if (speciesType == NCBIEntry.HUMAN) {
      return "human";
    }

    if (speciesType == NCBIEntry.MIRCOORGANISM) {
      return "microorganism";
    }

    if (speciesType != NCBIEntry.UNKNOWN) {
      Logger.log(null, "unidentified organism");
    }

    return "unknown";
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public boolean isSplitpoint() {
    return NCBI_Tree.isSplitpoint(this.getTaxIdInt());
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public List getSplitpointChilds() {
    if (this.isSplitpoint()) {
      if (this.cacheSplitpointChilds == null) {
        Logger.debug(this, "SplitpointChilds not cached.");
        initCacheSplitpointChilds();
      }

      return cacheSplitpointChilds;
    } else {
      return new Vector();
    }
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws NCBIException TODO
   */
  public NCBIEntry getSplitpointParent() throws NCBIException {
    return NCBI_Tree.findGroup(this.getParent().getName());
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getTaxID() {
    return this.node.getTAX_id();
  }

  /**
   * @return
   */
  public int getTaxIdInt() {
    return Integer.parseInt(this.getTaxID());
  }

  /**
   * @return
   */
  public Hashtable toHashtable() {
    try {
      Hashtable hashtable = new Hashtable();
      hashtable.put("ScientificName", "" + getName());
      hashtable.put("Parent", "" + getParent());
      hashtable.put("Rank", "" + getRank());
      hashtable.put("Taxonomy ID", "" + getTaxID());
      hashtable.put("Number of Childs", "" + getChilds().size());

      return hashtable;
    } catch (NCBIFindException e) {
      e.printStackTrace();

      return null;
    }
  }

  /**
   * creates a link - if this link is activated - the value will be set to the given child.
   *
   * @param posOfChild the position of the child
   *
   * @return a link setting the child- if parent not found ""
   *
   * @throws NCBIFindException
   */
  public String toLinkSettingChild(InputField inputfield, int posOfChild) {
    //return ValidationAnswer.toLink(this.getName(), this.getChild(posOfChild).getTaxID(), this.getChild(posOfChild).getName() ) ;
    try {
      return ValidationAnswer.toLink(inputfield.getName(), this.getChild(posOfChild).getTaxID(), this.getChild(posOfChild).getName());
    } catch (NCBIFindException e) {
      warning(this, e.toString());

      return "";
    }
  }

  /**
   * creates a link - if this link is activated - the value will be set.
   *
   * @param inputfield
   *
   * @return a link setting the parent - if parent not found ""
   */
  public String toLinkSettingParent(InputField inputfield) {
    try {
      return ValidationAnswer.toLink(inputfield.getName(), this.getParent().getTaxID(), this.getParent().getName());
    } catch (NCBIFindException e) {
      warning(this, e.toString());

      return "";
    }
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String toString() {
    return this.getName() + " [" + this.getTaxID() + "]  " + this.getRank() + " nameNCBI: " + this.name + "  nodeNCBI: " + this.node;
  }

  /**
   * inits the splitpointchilds. All splitpoint that have this entry as their parentsplitpoint.
   */
  private void initCacheSplitpointChilds() {
    Logger.debug(this, "caching SplitpointChilds");
    this.cacheSplitpointChilds = new Vector();

    // take all splitpointids 
    Iterator splitpointidIterator = NCBI_Tree.splitpointIDs.iterator();

    while (splitpointidIterator.hasNext()) {
      int splitpointID = Integer.parseInt("" + splitpointidIterator.next());

      // check if the parents splitpoint is this
      NCBIEntry entry;

      try {
        entry = NCBIConnector.determineNCBI_Information(splitpointID);

        if ((entry.getSplitpointParent().getTaxID() == this.getTaxID()) && (entry.getTaxIdInt() != this.getTaxIdInt())) {
          Logger.debug(this, this.getName() + ": caching parent:" + entry.getName());
          this.cacheSplitpointChilds.add(entry);
        }
      } catch (NCBIException e) {
        warning(e);
      }
    }

    Logger.debug(this, "caching SplitpointChilds done - number of childs: " + this.cacheSplitpointChilds.size());
  }
}
