package org.setupx.repository.core.util.xml;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class CreateException extends Exception {
  /**
   *
   */
  public CreateException() {
    super();

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public CreateException(String arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public CreateException(Throwable arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   */
  public CreateException(String arg0, Throwable arg1) {
    super(arg0, arg1);

    // TODO Auto-generated constructor stub
  }
}
