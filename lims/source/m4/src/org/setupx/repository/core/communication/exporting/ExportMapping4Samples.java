/**
 * ============================================================================ File:    ExportMapping4Samples.java Package: org.setupx.repository.core.communication.exporting cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.exporting;

import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class ExportMapping4Samples extends ExportMapping {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private JobInformation jobinformation;
  private int vialPosition = 0;
  private long sampleID = 0;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ExportMapping4Samples object.
   *
   * @param l 
   * @param jobInformation 
   * @param vialPosition 
   */
  public ExportMapping4Samples(long sampleID, JobInformation jobInformation, int vialPosition) {
    super();

    this.jobinformation = jobInformation;
    this.vialPosition = vialPosition;
    this.sampleID = sampleID;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws MappingError TODO
   * @throws org.setupx.repository.server.persistence.PersistenceActionUpdateException
   */
  public AcquisitionSample map() throws MappingError, PersistenceActionUpdateException {
    AcquisitionSample acquisitionSample = new AcquisitionSample();
    Machine machine = jobinformation.getMachine();

    ACQMachineConfigurationIterator configurationIterator = machine.getConfiguration();

    String parameterLabel = "";
    String parameterValue = "";

    try {
      while (configurationIterator.hasNext()) {
        parameterLabel = (String) configurationIterator.next();
        parameterValue = jobinformation.get(parameterLabel);
        acquisitionSample.addParameter(new AcquisitionParameter(parameterLabel, parameterValue));
      }

      debug("overwriting ");
      acquisitionSample.addParameter(new AcquisitionParameter(AcquisitionParameter.TRAY, "" + sampleID)); // + " " + sample.parent().getId() + " "));
      acquisitionSample.addParameter(new AcquisitionParameter(AcquisitionParameter.VIAL, "" + vialPosition));

      acquisitionSample.tempSampleID = sampleID;
    } catch (ParameterException e) {
      throw new MappingError("unable to create AcquisitionSample. The value of parameter:\"" + parameterLabel + "\" is not valid in the configuration for machine :\"" + machine.getComment() + "\"", e);
    }

    return acquisitionSample;
  }
}
