package org.setupx.repository.core.communication.status;

import java.util.Date;
import java.util.List;

class CachedStatusCollection {

    private List list;
    private Date created;

    public CachedStatusCollection(List list) {
        this.list = list;
        this.created = new Date();
    }

    /** diff between now and when it was created */
    public long getAge() {
        return new Date().getTime() - created.getTime();
        
    }

    public List getList() {
        return this.list;
    }

}
