/**
 * ============================================================================ File:    NCICB_connector.java Package: org.setupx.repository.core.communication.ncbi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ncbi;

import org.setupx.repository.core.Connectable;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.ServiceNotAvailableException;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.net.URL;
import java.net.URLConnection;

import javax.wsdl.Output;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * Connection for Organs at NCI Center for Bioinformatics
 * 
 * <p>
 * used for outside connections - also checkout the local instance of NCBI
 * </p>
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class NCICB_connector extends CoreObject implements Connectable {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  static String base_url = null;
  static private DocumentBuilderFactory factory;
  static private DocumentBuilder builder;
  static Class thiz = NCICB_connector.class;
  private static NCICB_Result result;

  static {
    try {
      base_url = "http://cabio.nci.nih.gov/servlet/GetXML?query=Organ&crit_name=";
      org.setupx.repository.core.util.Util.checkURL(new URL(base_url));
      factory = DocumentBuilderFactory.newInstance();
      builder = factory.newDocumentBuilder();
    } catch (Exception e) {
      warning(thiz, e.toString());
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.core.Connectable#checkAvailability()
   */
  public void checkAvailability() throws ServiceNotAvailableException {
    try {
      query("ear");
    } catch (NCICBException e) {
      throw new ServiceNotAvailableException(e);
    }
  }

  /**
   * @param string
   *
   * @return the id of the oran in the NCICB-Database
   *
   * @throws NCICBException
   */
  public static NCICB_Result query(String string) throws NCICBException {
    URL url = null;

    try {
      url = new URL(base_url + java.net.URLEncoder.encode(string));

      debug(thiz, url.getPath());
      debug(thiz, url.toExternalForm());

      // connect
      URLConnection connection = url.openConnection();
      connection.connect();

      String contentType = connection.getContentType();

      if (contentType.compareTo("XML") != 0) {
        warning(thiz, "" + new NCICBException("the service is responding with the wrong type of content intstead of XML : " + contentType));
        warning(thiz, "" + new NCICBException("will treat it like an XML doc"));
      }

      // create a doc from it
      InputStream in = connection.getInputStream();

      Document doc = builder.parse(in);
      result = new NCICB_Result(doc);
    } catch (NCICBException e) {
      throw e;
    } catch (SAXException e) {
      throw new NCICBException(e);
    } catch (IOException e) {
      throw new NCICBException(e);
    }

    return result;
  }
}
