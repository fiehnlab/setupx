/**
 * ============================================================================ File:    OBOHashtable.java Package: org.setupx.repository.core.communication.ontology.obo cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ontology.obo;

import java.util.Hashtable;


/**
 * Title:   This class stores, access, and manipulate the OBO terms hashtable for an OBO file
 *
 * @author: Li-Kuan Chen
 * @version 1.1
 */
public class OBOHashtable extends Hashtable {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String namespace = "";

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new OBOHashtable object.
   *
   * @param namespace 
   */
  public OBOHashtable(String namespace) {
    this.namespace = namespace;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getNamespace() {
    return this.namespace;
  }

  /**
   * TODO: 
   *
   * @param term TODO
   *
   * @return TODO
   */
  public String returnId(String term) {
    return (String) this.get(term);
  }
}
