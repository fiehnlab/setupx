package org.setupx.repository.core.util.web;

public class WebException extends Exception{

    public WebException(String string) {
        super(string);
    }

    public WebException(Throwable throwable){
        super(throwable);
    }

    public WebException(Throwable throwable, String comment) {
        super(comment, throwable);
    }
}

