package org.setupx.repository.core.query.answer;

import java.util.Vector;

import org.hibernate.Session;

import org.setupx.repository.core.query.QueryMasterAnswer;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.util.Link;
import org.setupx.repository.web.forms.inputfield.multi.Clazz;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.Sample;

public class FormobjectAnswer extends QueryMasterAnswer {

    private long relatedObjectID;
    private Session session;
    
    /** string representing the Class. ClassForName should not be used cause some of the related classes do not have a default constructor. */
    private String classString;
    private String label;
    private String info1;
    private String info2;
    private String info3;
    private Vector links = new Vector();

    /*
    if (classString.compareTo(Sample.class.getName()) == 0){
        return "sample_detail.jsp?id=" + this.relatedObjectID;
    } else if (classString.compareTo(Promt.class.getName()) == 0){
        return "load?id=" + this.relatedObjectID + "&action=20";
    } else {
        return "";
    }
    */

    /**
     * Create an Answer just based on the ID.
     * <p>
     * Depending on the class of the object found (represented by the {@link #classString}) different values are set for the 
     * info fields and the label.
     * <p>
     * @param id the ID of the object that is beeing looked for.
     * @throws FormobjectAnswerCreationException when the type of the object that is referenced is unknown. Currently supported objects are {@link Promt}, {@link Clazz} and {@link Sample}.
     */
    public FormobjectAnswer(long id) throws FormobjectAnswerCreationException {
        log("creating new instance for " + id);
        this.relatedObjectID = id;
        this.session = createSession();
        this.classString = this.session.createSQLQuery("select discriminator from formobject where uoid = " + id).addScalar("discriminator", org.hibernate.Hibernate.STRING).list().get(0).toString();
        log("class: " + classString);
        
        if (classString.compareTo(Sample.class.getName()) == 0){
            this.label = "sample" + id;
            try {
                long expID = new SXQuery().findPromtIDbySample(id);
                this.info1 = "part of experiment <b>" + new SXQuery().findPromtTitleByPromtID(expID) + "</b> (" + expID + ")";
                this.info2 = "Samplelabel: " + new SXQuery().findSampleLabelBySampleID(id);
                this.info3 = "Experiment contains " + new SXQuery().findSampleIDsByPromtID(expID).size() + " samples.";
                this.links.add(new Link("view sample [" + this.relatedObjectID + "]" , "sample_detail.jsp?id=" + this.relatedObjectID) );
                this.links.add(new Link("view experiment [" + expID + "]",  "load?id=" + expID + "&action=20"));
            } catch (PersistenceActionFindException e) {
                e.printStackTrace();
            }
        } else if (classString.compareTo(Promt.class.getName()) == 0){
            this.label = "experiment";
            try {
                this.info1 = "title: " + new SXQuery().findPromtTitleByPromtID(id);
                this.info2 = "abstract: " + new SXQuery().findPromtAbstractByPromtID(id);
                this.info3 = "collaborators: " + new SXQuery().findPromtCollaborationString(id);
                this.links.add(new Link("view experiment [" + this.relatedObjectID + "]",  "load?id=" + this.relatedObjectID + "&action=20"));
            } catch (PersistenceActionFindException e) {
                e.printStackTrace();
            }
        } else if (classString.compareTo(Clazz.class.getName()) == 0){
            this.label = "class";
            try {
                long promtID = new SXQuery().findPromtIDbyClazz((int)id);
                this.info1 = "Class " + this.relatedObjectID + " is part of experiment " + promtID;
                this.info2 = "title: " + new SXQuery().findPromtAbstractByPromtID(promtID);
                this.info3 = "abstract: " + new SXQuery().findPromtCollaborationString(promtID);
                this.links.add(new Link("view experiment [" + promtID + "]",  "load?id=" + promtID + "&action=20"));
            } catch (PersistenceActionFindException e) {
                e.printStackTrace();
            }
        } else {
            throw new FormobjectAnswerCreationException("unable to create FormobjectAnswer for object " + id + "  [" + classString + "].");
        }
        
    }

    public String getIcon() {
        return "pics/Publication-pic.gif";
    }

    public String getInfo1() {
        return this.info1;
    }

    public String getInfo2() {
        return this.info2;
    }

    public String getInfo3() {
        return this.info3;
    }

    public String getLabel() {
        return label;
    }

    public Vector getRelatedObjectLinks() {
        return this.links;
    }

    public boolean checkAccessRestriction(UserDO userDO) {
        return userDO.isMasterUser(userDO.getUOID());
    }

    public boolean isAccessRestricted() {
        return true;
    }
}
