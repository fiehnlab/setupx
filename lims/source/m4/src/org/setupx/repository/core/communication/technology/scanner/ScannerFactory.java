package org.setupx.repository.core.communication.technology.scanner;

import java.util.Iterator;

import org.setupx.repository.Config;
import org.setupx.repository.ConfigurationException;
import org.setupx.repository.core.communication.technology.platform.Technology;
import org.setupx.repository.core.communication.technology.platform.TechnologyFileType;
import org.setupx.repository.core.communication.technology.platform.TechnologyProvider;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;

public class ScannerFactory {

    private static Class thiz = ScannerFactory.class;

    /**
     * scanning for matches related to all {@link Technology}
     * 
     * @throws ScanningProcessException
     */
    public static void createScanningProcess() throws ScanningProcessException {
        Logger.info(thiz, "creating scanner for all technologies");
        try {
            Iterator technologies = TechnologyProvider.getTechnologies().iterator();
            while (technologies.hasNext()) {
                Technology technology = (Technology) technologies.next();
                createScanningProcess(technology);
            }
        } catch (PersistenceActionFindException e) {
            throw new ScanningProcessException("unable to find all technologies", e);
        }
    }

    /**
     * scanning for matches related to one {@link Technology}
     */
    public static void createScanningProcess(Technology technology) {
        Logger.debug(thiz, "creating scanner for technology: " + technology.getLabel());
        Iterator technologyFileTypes = technology.getTechnologyFileTypes().iterator();
        while (technologyFileTypes.hasNext()) {
            TechnologyFileType technologyFileType = (TechnologyFileType) technologyFileTypes.next();
            createScanningProcess(technologyFileType);
        }
    }

    /**
     * scanning for matches related to one {@link TechnologyFileType}
     */
    public static void createScanningProcess(TechnologyFileType technologyFileType) {
        Logger.debug(thiz, "scanning technologyFileType: " + technologyFileType.getLabel());
        new ScanningProcess(technologyFileType, Config.FILESCANNER_AUTOASSIGNED_MIN_SAMPLEID);
    }
    
    
    public String bla(){
        /*
            Thread.currentThread().
            this.setName(technologyFileType2 + "_" + technologyFileType2.getUOID());
        */
        return "";
    }

    public static void init() {
        Logger.info(ScannerFactory.class , "init techno scanner:"  + Config.FILESCANNER_TECHNOSCANNER_ACTIVE);
        if (Config.FILESCANNER_TECHNOSCANNER_ACTIVE){
            try {
                ScannerFactory.createScanningProcess();
            } catch (ScanningProcessException e1) {
                Logger.info(ScannerFactory.class , "err: unable to init techno scanner:" + e1.getMessage());
            }
        }
    }
}
