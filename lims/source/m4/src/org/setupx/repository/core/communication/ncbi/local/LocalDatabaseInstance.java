/**
 * ============================================================================ File:    LocalDatabaseInstance.java Package: org.setupx.repository.core.communication.ncbi.local cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ncbi.local;

import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.ncbi.NCBIFindException;
import org.setupx.repository.core.communication.ncbi.NCBI_Config;
import org.setupx.repository.core.util.File;
import org.setupx.repository.core.util.logging.Logger;

import java.io.IOException;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;


/**
 * Local-Instance of the remote Database of NCBI-Taxonomy.
 * <pre>
 * The instance creates itself out of the dumpfiles downloaded form the NCBI webserver.
 * </pre>
 * TODO add ftp connection
 *
 * @author <a href=lto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.26 $
 */
public class LocalDatabaseInstance extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // Hashtable containing nodes 
  private static Class thiz = LocalDatabaseInstance.class;
  public static Hashtable names_real = new Hashtable();
  public static Hashtable names_common = new Hashtable();
  public static Hashtable names_equi = new Hashtable();
  public static Hashtable names_misspelling = new Hashtable();
  public static Hashtable names_synonym = new Hashtable();
  public static Hashtable nodes = new Hashtable();

  static {
    // reading all information from the files 
    // TODO add FTP access...
    if (!NCBI_Config.NCBI_local_database_acitve) {
      warning(thiz, "NCBI verification is deactivated !!");
    } else {
      try {
        readFile(new java.io.File(Config.NCBI_DIRECTORY + java.io.File.separator + "names.dmp"), NCBI_Name.class);
        readFile(new java.io.File(Config.NCBI_DIRECTORY + java.io.File.separator + "nodes.dmp"), NCBI_Node.class);

        log(thiz, LocalDatabaseInstance.summary());
      } catch (Exception e) {
        err(thiz, e);
      }
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * finds the ncbi entry for an element.
   * 
   * <p>
   * checks <b>ALL </b>the hashtables containing any names for the existence of the searchstring
   * </p>
   * 
   * <p>
   * all information related to this object will be resturned in an NCBIentry object
   * </p>
   *
   * @param searchword the word that is used to look for the word
   *
   * @return an object containing all information to this searchstring
   *
   * @throws NCBIFindException unable to find the element
   */
  public static NCBIEntry getNCBI_Entry(String searchword)
    throws NCBIFindException {
    if ((searchword == null) || (searchword.length() < 2)) {
      throw new NCBIFindException("unable to look for NCBI code.");
    }

    NCBI_dump_Information result = null;

    boolean loop = true;

    debug(thiz, "starting query on all local dataHashtables\n");

    for (int i = 0; (loop == true) && (i < getNamesHashtables().length); i++) {
      //debug(thiz, "checking on " + getNamesHashtables()[i].size());
      result = get(searchword, getNamesHashtables()[i]);

      //debug(thiz, "result " + result);
      if (result != null) {
        loop = false;
      }
    }

    debug(thiz, "done query on all local dataHashtables\n");

    if (result == null) {
      throw new NCBIFindException("unable to find id for " + searchword);
    }

    // ok we got an ID  - now getting the related information
    String id = result.getTAX_id();
    debug(thiz, "! found id: " + id);

    NCBI_Node node = getNode(id);

    if (node == null) {
      throw new NCBIFindException("unable to find related information for " + id);
    }

    NCBIEntry entry;

    try {
      entry = new NCBIEntry((NCBI_Name) result, node);
    } catch (NCBICreateException e) {
      throw new NCBIFindException(e);
    }

    return entry;
  }

  /**
   * @param parentID
   *
   * @return
   *
   * @throws NCBIFindException
   */
  public static NCBIEntry getNCBI_Entry_byID(String id)
    throws NCBIFindException {
    try {
      NCBI_Node node = getNode(id);
      NCBI_Name name = getNameByID(id);

      return new NCBIEntry(name, node);
    } catch (NCBICreateException e) {
      throw new NCBIFindException("unable to find NCBI entry for the id: \"" + id + "\"  [cached objects: nodes: " + nodes.size() + "  names_real: " + names_real.size(), e);
    }
  }

  /**
   * TODO: 
   */
  public static void setToNull() {
    names_common = null;
    names_equi = null;
    names_misspelling = null;
    names_real = null;
    names_synonym = null;
    nodes = null;
  }

  /**
   * TODO: 
   *
   * @param entry TODO
   *
   * @return TODO
   */
  protected static Vector determineChildIDs(NCBIEntry entry) {
    //debug(thiz, "requested childs for " + entry.getName());
    Enumeration enumeration = nodes.elements();
    Vector vector = new Vector();

    while (enumeration.hasMoreElements()) {
      NCBI_Node element = (NCBI_Node) enumeration.nextElement();

      if (element.getParentID().compareTo(entry.getTaxID()) == 0) {
        vector.add(element.getTAX_id());
      }
    }

    //debug(thiz, "found " + vector.size() + " entries.");
    return vector;
  }

  /**
   * use the key - it is faaaaster!!!
   *
   * @param searchstring
   *
   * @return
   */
  private static final NCBI_Name getNameByID(String id) {
    for (int i = 0; i < getNamesHashtables().length; i++) {
      Enumeration enumeration = getNamesHashtables()[i].elements();

      while (enumeration.hasMoreElements()) {
        NCBI_Name element = (NCBI_Name) enumeration.nextElement();

        if (element.getTAX_id().compareTo(id) == 0) {
          return element;
        }
      }
    }

    return null;
  }

  /**
   * TODO: 
   *
   * @param searchstring TODO
   *
   * @return TODO
   */
  private static final NCBI_Name getNameByName(String searchstring) {
    searchstring = searchstring.toLowerCase();

    try {
      NCBI_Name name = ((NCBI_Name) names_real.get(searchstring));

      return name;
    } catch (Exception e) {
      // TODO add search in the other hashs
      warning(thiz, " " + e);

      return null;
    }
  }

  /**
   * returns all the hashtables as an array - whith the hashtable ordered by the possibility that it contains words that  are looked for.
   *
   * @return all the hashtables as an array
   */
  private static final Hashtable[] getNamesHashtables() {
    return new Hashtable[] { names_real, names_common, names_equi, names_misspelling, names_synonym };
  }

  /**
   * finds the node with the defined NCBI id
   *
   * @param id the id related to an NCBI object
   *
   * @return
   */
  private static NCBI_Node getNode(String id) {
    return (NCBI_Node) nodes.get(id);
  }

  /**
   * get dumpinformation form a defined hashtable
   *
   * @param searchword
   * @param names_common2
   *
   * @return
   */
  private static NCBI_dump_Information get(String searchword, Hashtable hashtable) {
    //debug(thiz, "looking inside " + hashtable.size() + " for " + searchword);
    return (NCBI_dump_Information) hashtable.get(searchword.toLowerCase());
  }

  /**
   * reads a files into the different hashtables - requiered while init
   *
   * @param file TODO
   * @param clasS TODO
   *
   * @throws IOException TODO
   * @throws UnknownClassParameter TODO
   */
  private static void readFile(java.io.File file, Class clasS)
    throws IOException, UnknownClassParameter {
    Logger.info(LocalDatabaseInstance.class, "loading database instance.");
    debug(thiz, "reading " + file.getName() + " " + file.getAbsolutePath());

    StringTokenizer stringTokenizer = new StringTokenizer(File.getFileContent(file), "\n");

    if (clasS == NCBI_Name.class) {
      NCBI_Name iname = null;

      while (stringTokenizer.hasMoreElements()) {
        try {
          iname = (NCBI_Name) NCBI_Name.setUpInstance((String) stringTokenizer.nextElement(), new NCBI_Name());

          // debug(thiz, "tax id: "  + inode);
          if (iname.isScientificName()) {
            names_real.put(iname.getName().toLowerCase(), iname);
          } else if (iname.isCommonName()) {
            names_common.put(iname.getName().toLowerCase(), iname);
          } else if (iname.isEquivalentName()) {
            names_equi.put(iname.getName().toLowerCase(), iname);
          } else if (iname.isMisspelling()) {
            names_misspelling.put(iname.getName().toLowerCase(), iname);
          } else if (iname.isSynonym()) {
            names_synonym.put(iname.getName().toLowerCase(), iname);
          } else {
            //warning(thiz, "unknown entry: " + iname);
          }
        } catch (NullPointerException e) {
          //e.printStackTrace();
          warning(thiz, iname.toString());
        }
      }

      log(thiz, "filled NCBI names");
    } else if (clasS == NCBI_Node.class) {
      NCBI_Node inode = null;

      while (stringTokenizer.hasMoreElements()) {
        try {
          inode = (NCBI_Node) NCBI_Name.setUpInstance((String) stringTokenizer.nextElement(), new NCBI_Node());
          nodes.put(inode.getTAX_id(), inode);
        } catch (NullPointerException e) {
          //e.printStackTrace();
          warning(thiz, inode.toString());
        }
      }

      debug(thiz, "filled NCBI nodes");
    } else {
      throw new UnknownClassParameter(clasS);
    }
  }

  /**
   * @return a summary of all information stored in this object as a String
   */
  private static String summary() {
    return " names_common: " + names_common.size() + " names_equi: " + names_equi.size() + " names_misspelling: " + names_misspelling.size() + " names_real: " + names_real.size() + " names_synonym: " + names_synonym.size() + " nodes: " + nodes.size();
  }
}
