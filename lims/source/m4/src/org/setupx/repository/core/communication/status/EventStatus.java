package org.setupx.repository.core.communication.status;

import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.hibernate.Session;

import org.setupx.repository.web.forms.event.Event;

/**
 * based on the deprecated events that were attached to each sample.
 */
public class EventStatus extends DynamicStatus implements Timestamped {
    
    private Event event;

    public EventStatus(final long sampleUOID, Event event) {
        super(sampleUOID);
        this.event = event;
    }

    public String createDisplayString() {
        return this.event.getComment();
    }

    public String getMessage() {
        return this.event.getComment();
    }


    public Date getDate() {
        return this.event.getTimestamp();
    }

    
    public static Collection find(long sampleUOID, Session hqlSession) {
        Vector vector = new Vector();
        
        // load event for this sampleID
        Event event = null;
        
        warning(null, "TODO: implement find for EventStatus");
        if (event != null) {
            event = null; //Event.persistence_loadByID(Event.class, hqlSession, sampleUOID);
            vector.add(new EventStatus(sampleUOID, event));
        }
        
        return vector;
    }

    
}
