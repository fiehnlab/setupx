/**
 * ============================================================================ File:    PromtConverter.java Package: org.setupx.repository.core.util.hotfix.oldsamples cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.hotfix.oldsamples;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.experimentgeneration.MetadataSource;
import org.setupx.repository.core.communication.experimentgeneration.template.ClazzTemplate;
import org.setupx.repository.core.communication.experimentgeneration.template.SampleTemplate;
import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.core.communication.importing.logfile.ScannedPair;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.hotfix.oldsamples.mapping.Mapping;
import org.setupx.repository.core.util.hotfix.oldsamples.mapping.MappingException;
import org.setupx.repository.core.util.xml.XMLFile;
import org.setupx.repository.core.util.xml.XPath;
import org.setupx.repository.core.util.xml.XPathException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionStoreException;
import org.setupx.repository.web.forms.ArrayExpandException;
import org.setupx.repository.web.forms.CloneException;
import org.setupx.repository.web.forms.FormObject;
import org.setupx.repository.web.forms.PromtCreateException;
import org.setupx.repository.web.forms.PromtCreator;
import org.setupx.repository.web.forms.inputfield.InputField;
import org.setupx.repository.web.forms.inputfield.multi.MultiField;
import org.setupx.repository.web.forms.inputfield.multi.MultiField4Clazz;
import org.setupx.repository.web.forms.inputfield.multi.MultiFieldExpanding;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.SampleScheduledException;
import org.setupx.repository.web.forms.inputfield.multi.UnsupportedClazzMappingException;

import java.io.File;
import java.io.IOException;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class PromtConverter extends CoreObject implements MetadataSource {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private File folder;
  private Hashtable valueHashsets = new Hashtable();
  private ClazzTemplate[] clazztemplates;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final long serialVersionUID = 4744527230062361771L;
  private static final int MIN_NUMBER_OF_VALUES = 1;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new PromtConverter object.
   *
   * @param promtID 
   * @param folder 
   *
   * @throws PromtConverterException 
   * @throws PersistenceActionFindException 
   */
  public PromtConverter(final long promtID, final File folder)
    throws PromtConverterException, PersistenceActionFindException {
    this.folder = folder;

    if (!folder.isDirectory()) {
      throw new PromtConverterException("folder " + folder + " is not a directory");
    }

    // grap all xmlfiles from one folder
    File[] files = folder.listFiles();

    for (int i = 0; i < files.length; i++) {
      File file = files[i];
      XMLFile xmlfile = null;

      try {
        xmlfile = new XMLFile(file);
        findValues(xmlfile);
      } catch (Exception e) {
        warning(this, "problems opening " + file);
        e.printStackTrace();
        //err(e); //throw new PromtConverterException(file, e);
      }
    }

    // create clazzTemplates incl samples
    this.createClassTemplates();

    // show templates
    this.showTemplates();

    // create a file containing the classinformations
    createClassInformation();

    showData();
  }

  /**
   * Creates a new PromtConverter object.
   *
   * @param folder 
   *
   * @throws PersistenceActionFindException 
   * @throws PromtConverterException 
   */
  public PromtConverter(File folder) throws PersistenceActionFindException, PromtConverterException {
    this(0, folder);
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public ClazzTemplate[] getClazzTemplates() {
    return this.clazztemplates;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public final String[] getPath() {
    return OldSampleMapping.getPath();
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.communication.experimentgeneration.MetadataSource#isFinishedRun()
   */
  public boolean isFinishedRun() {
    return true;
  }

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws PromtCreateException TODO
   * @throws MappingError
   * @throws MappingException TODO
   */
  public Promt createPromt() throws PromtCreateException, MappingError {
    return PromtCreator.create(this);
  }

  /**
   * @deprecated 
   */
  public void createScannedPairs() {
    for (int i = 0; i < this.clazztemplates.length; i++) {
      ClazzTemplate clazzTemplate = this.clazztemplates[i];

      for (int j = 0; j < clazzTemplate.getSamples().length; j++) {
        SampleTemplate sampleTemplate = clazzTemplate.getSamples()[j];

        // create a pair
        try {
          new ScannedPair(sampleTemplate.getValue(OldSampleMapping.SAMPLENAME), sampleTemplate.getAssignedSample().getUOID() + "").update(true);
        } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * TODO: 
   *
   * @param multiField4Clazz TODO
   */
  public void map(MultiField4Clazz multiField4Clazz) {
    HashSet values = new HashSet();

    try {
      values = this.values(multiField4Clazz.getPath());
    } catch (UnsupportedClazzMappingException e) {
      warning(this, "this field does not support a mapping");
    }

    Iterator iterator = values.iterator();

    debug("---- starting mapping " + Util.getClassName(multiField4Clazz) + " ---- ");

    while (iterator.hasNext()) {
      String value = (String) iterator.next();
      debug("mapping value: " + value);
    }

    try {
      multiField4Clazz.setSingleValue(values);
    } catch (MappingError e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (UnsupportedClazzMappingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CloneException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /*
   *  (non-Javadoc)
   * @see org.setupx.repository.core.communication.experimentgeneration.MetadataSource#map(org.setupx.repository.web.forms.inputfield.InputField, java.lang.String)
   */
  public void map(InputField formObject, String path) throws MappingError {
    // example: source.map(inSex, ClassTemplate.SEX);
    HashSet _values = (HashSet) this.values(path);

    if ((_values == null) || (_values.size() == 0)) {
      debug("no mapping for " + Util.getClassName(formObject) + " and path " + path);

      return;
    }

    debug("mapping " + path + " [" + _values.size() + " entries] onto an " + Util.getClassName(formObject));

    // in case it is a multifield
    if (formObject instanceof MultiFieldExpanding) {
      MultiFieldExpanding multifield = (MultiFieldExpanding) formObject;

      // check the size of the formobject
      if (multifield.size() != _values.size()) {
        debug("the number of existing inputfields in " + Util.getClassName(formObject) + " does not match number of values ( " + _values.size() + ").");

        try {
          multifield.expand(_values.size() - multifield.size());
          debug("expanded " + Util.getClassName(multifield) + " to " + multifield.size());
        } catch (ArrayExpandException e) {
          throw new MappingError(e);
        }
      }

      Iterator iterator = _values.iterator();

      // setting values in a multifield
      FormObject[] formObjects = multifield.getFields();

      for (int i = 0; i < formObjects.length; i++) {
        Object value = iterator.next();
        debug("updating value of " + Util.getClassName(formObjects[i]) + ": " + value);
        ((InputField) formObjects[i]).setValue(value);
      }
    } else if (formObject instanceof MultiField4Clazz) {
      // the field is a dimension for a clazz
      // map the values to the parent 
      MultiField parent = (MultiField) formObject.getParent();
      debug("mapping parent of " + Util.getClassName(formObject));
      map(parent, path);
    } else {
      if (_values.size() > 1) {
        throw new MappingError("unable to map " + _values.size() + " elements onto a " + Util.getClassName(formObject));
      }

      // in case it is a normal field
      String value = (String) _values.iterator().next();
      debug("updating value of " + Util.getClassName(formObject) + ": " + value);
      formObject.setValue(value);
    }
  }

  /**
   * TODO: 
   */
  public void showData() {
    String[] pathes = this.getPath();

    for (int i = 0; i < pathes.length; i++) {
      debug(" - - - - - - - - - - - - ");

      HashSet hashSet = this.values(pathes[i]);
      debug(pathes[i] + " : " + hashSet.size() + " elements.");

      Iterator iterator = hashSet.iterator();

      while (iterator.hasNext()) {
        debug(pathes[i] + " : " + iterator.next());
      }
    }
  }

  /**
   * TODO: 
   *
   * @param path TODO
   *
   * @return TODO
   */
  public HashSet values(String path) {
    if (path == null) {
      return null;
    }

    if (!this.valueHashsets.containsKey(path)) {
      // create a new Hashset 
      this.valueHashsets.put(path, new HashSet());
    }

    return (HashSet) this.valueHashsets.get(path);
  }

  /**
   * TODO: 
   *
   * @param clazztemplatesVec TODO
   *
   * @return TODO
   */
  private static final ClazzTemplate[] convert(HashSet clazztemplatesVec) {
    ClazzTemplate[] tmp = new ClazzTemplate[clazztemplatesVec.size()];
    Iterator iterator = clazztemplatesVec.iterator();
    int i = 0;

    while (iterator.hasNext()) {
      tmp[i] = OldClassTemplate.createInstance((Hashtable) iterator.next());
      i++;
    }

    return tmp;
  }

  /**
   * find all samples - that are related to a specific clazztemplate and add them as a sampletemplate
   */
  private void assignSamples() {
    // take each clazztemplate
    for (int i = 0; i < this.clazztemplates.length; i++) {
      ClazzTemplate clazzTemplate = this.clazztemplates[i];

      // take each path of the template 
      String[] pathes = clazzTemplate.getPathes();

      for (int j = 0; j < pathes.length; j++) {
        // find a file containing all pathes with the specific values and get a sampletemplate out of it
        SampleTemplate[] sampleTemplates = findTemplate(clazzTemplate);

        // assign the new template to the clazztemplate
        clazzTemplate.setSamples(sampleTemplates);
      }
    }
  }

  /**
   * TODO: 
   *
   * @param xmlfile TODO
   * @param values TODO
   *
   * @return TODO
   */
  private boolean compare(XMLFile xmlfile, Hashtable values) {
    // open file and look for each of the key value combinations
    org.w3c.dom.Document doc = xmlfile.getDocument();

    for (Enumeration iter = values.keys(); iter.hasMoreElements();) {
      String path = (String) iter.nextElement();
      String value = "" + values.get(path);

      String valueInDoc = "";

      try {
        valueInDoc = XPath.pathValue(doc, path);
      } catch (Exception e) {
        warning(this, e.toString());
      }

      if (value.compareTo(valueInDoc) != 0) {
        //debug("didnt find \"" + valueInDoc "\" at \"" + path + "\".");
        return false;
      }
    }

    // found all values
    debug("found all values in " + xmlfile.getSourceFile().getName());

    return true;
  }

  /**
   * creates a file containing information about the class and stores it in the same directory
   */
  private void createClassInformation() {
    StringBuffer buffer = new StringBuffer();

    // take all paths and their values
    for (int i = 0; i < getPath().length; i++) {
      HashSet my_values = this.values(getPath()[i]);

      // limit of values that have to be different
      if (my_values.size() >= MIN_NUMBER_OF_VALUES) {
        debug(this.folder.getName() + " number of values for [" + getPath()[i] + "]: " + my_values.size());
        buffer.append("\n--------------------------------");
        buffer.append("\n\n" + getPath()[i]);

        Iterator iterator = my_values.iterator();

        while (iterator.hasNext()) {
          buffer.append("\n" + iterator.next());
        }
      }
    }

    File file = new File(this.folder + File.separator + "classInformation.txt");

    try {
      org.setupx.repository.core.util.File.storeData2File(file, buffer.toString());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * create Classtemplates based on the information form values using a set of pathes form getPath(). the set of classtemplates is a combination of all possible different combinations of values.
   *
   * @see #clazztemplates
   * @see #values(String)
   * @see #getPath()
   */
  private void createClassTemplates() {
    this.clazztemplates = null;

    // for each combination create one species
    String[] pathes = getPath();
    debug("working with " + pathes.length + " pathes.");

    int[] positions = new int[pathes.length];

    boolean loop = true;

    // set of all clazztemplates
    HashSet clazztemplatesVec = new HashSet();

    debug("current number of clazztemplates: " + clazztemplatesVec.size());

    while (loop) {
      // hashset containing all values for ONE clazztemplate
      Hashtable values4classtemplate = new Hashtable();

      for (int i = 0; i < pathes.length; i++) {
        // take first each value 
        HashSet values = this.values(pathes[i]);
        int pos = positions[i];

        // take value at position pos 
        Iterator iterator = values.iterator();

        for (int j = 0; (j <= pos) && iterator.hasNext(); j++) {
          if (j == pos) {
            values4classtemplate.put(pathes[i], (String) iterator.next());
            //debug("pos:" + (pos+1) + "/"+ values.size()  + " path: \t" + pathes[i] + "\tvalue:\t" + values4classtemplate.get(pathes[i]) + " this.size: " + values4classtemplate.size());
            //debug("tmplate: "  + clazztemplatesVec.size() + "\t value for (path: " + i + ")");
          } else {
            iterator.next();
          }
        }
      }

      // change postion
      positions = increase(positions, pathes);

      if (positions != null) {
        debug("creating new clazztmp");
        clazztemplatesVec.add(values4classtemplate);
      } else {
        loop = false;
      }
    }

    this.clazztemplates = convert(clazztemplatesVec);
    log("created " + this.clazztemplates.length + " ClazzTemplates.");

    this.assignSamples();
  }

  /**
   * TODO: 
   *
   * @param values TODO
   *
   * @return TODO
   */
  private SampleTemplate[] createTemplateFromFiles(Hashtable values) {
    File[] files = folder.listFiles();
    String[] pathes = OldSampleMapping.getPath();

    HashSet samplesSet = new HashSet();

    for (int i = 0; i < files.length; i++) {
      File file = files[i];
      XMLFile xmlfile = null;

      try {
        xmlfile = new XMLFile(file);

        if (compare(xmlfile, values)) {
          SampleTemplate sampleTemplate = new OldSampleMapping();

          for (int j = 0; j < pathes.length; j++) {
            debug("new value: " + pathes[j] + " path: " + xmlfile.pathValue(pathes[j]));
            sampleTemplate.setValue(pathes[j], xmlfile.pathValue(pathes[j]));
          }

          // and the name
          String namePath = OldSampleMapping.SAMPLENAME;
          sampleTemplate.setValue(namePath, xmlfile.pathValue(namePath));

          samplesSet.add(sampleTemplate);
        }
      } catch (Exception e) {
        warning(this, e.toString());
      }
    }

    return (SampleTemplate[]) samplesSet.toArray(new SampleTemplate[samplesSet.size()]);
  }

  /**
   * find matching file
   *
   * @param clazzTemplate
   */
  private SampleTemplate[] findTemplate(ClazzTemplate clazzTemplate) {
    String[] pathes = clazzTemplate.getPathes();
    Hashtable values = new Hashtable();

    for (int i = 0; i < pathes.length; i++) {
      String path = pathes[i];
      String value = clazzTemplate.getValue(path);
      values.put(path, value);
    }

    return createTemplateFromFiles(values);
  }

  /**
   * TODO: 
   *
   * @param xmlfile TODO
   *
   * @throws XPathException TODO
   */
  private void findValues(XMLFile xmlfile) {
    String path;
    String value;

    for (int i = 0; i < getPath().length; i++) {
      path = getPath()[i];

      // take each value for the each path
      try {
        value = xmlfile.pathValue(path);
        // add the value to that label
        values(path).add(value);
        //debug("file: " + xmlfile.getSourceFile().getName() + "  \tpath:" + path + " \tvalue:" + value);
      } catch (XPathException e) {
        debug("file: " + xmlfile.getSourceFile().getName() + "  \tpath:" + path + " \tvalue NOT FOUND");
      }
    }
  }

  /**
   * TODO: 
   *
   * @param positions TODO
   * @param pathes TODO
   *
   * @return TODO
   */
  private int[] increase(int[] positions, String[] pathes) {
    short _i = 0;

    while (true) {
      if ((positions.length == _i) || (values(pathes[_i]) == null)) {
        debug("end reached (1)");

        return null;
      }

      /*
         log("--------------------");
         log(_i + " ");
         log(pathes[_i] + " ");
         log(values(pathes[_i]) + " ");
         log(values(pathes[_i]).size() + " ");
       */
      if (positions[_i] < (values(pathes[_i]).size() - 1)) {
        positions[_i] = positions[_i] + 1;

        for (int i = 0; i < positions.length; i++) {
          debug("pos: " + (positions[i] + 1) + "  " + values(pathes[i]).size() + " " + pathes[i]);
        }

        return positions;
      } else {
        positions[_i] = 0;

        // check if max pos of last pos reached
        if (_i < (pathes.length - 1)) {
          _i++;
        } else {
          debug("end reached (2)");

          return null;
        }
      }
    }
  }

  /**
   * TODO: 
   *
   * @param experimentID TODO
   *
   * @throws MappingException TODO
   * @throws PersistenceActionFindException TODO
   *
   * @deprecated
   */
  private void mapInformation(long experimentID) throws MappingException, PersistenceActionFindException {
    String question;

    // take a path
    for (int i = 0; i < getPath().length; i++) {
      // get related formobject in promt.
      question = Mapping.getQuestion(getPath()[i]);

      Promt promt;

      try {
        promt = Promt.load(experimentID);
      } catch (PersistenceActionFindException e) {
        throw e;
      } catch (PersistenceActionStoreException e) {
        throw new PersistenceActionFindException(e);
      } catch (PromtCreateException e) {
        throw new PersistenceActionFindException(e);
      } catch (SampleScheduledException e) {
        throw new PersistenceActionFindException(e);
      }

      InputField[] fields = promt.determineInputfields(question);

      // check if a field for each value exists
      HashSet _values = (HashSet) this.valueHashsets.get(getPath()[i]);
      Iterator iterator = _values.iterator();

      while (iterator.hasNext()) {
        String valueRequiered = (String) iterator.next();

        boolean valueExists = false;

        for (int j = 0; (j < fields.length) && !valueExists; j++) {
          String valueExisting = (String) fields[j].getValue();

          if (valueExisting.compareTo(valueRequiered) == 0) {
            // there is an existing one
            valueExists = true;
          }
        }

        if (!valueExists) {
          // there is no matching one, gonna create a new one
          debug("didnt find a matching value [" + valueRequiered + "] at " + getPath()[i]);
          //TODO
        }
      }
    }
  }

  /**
   * TODO: 
   */
  private void showTemplates() {
    log("----clazztemplates----");

    for (int i = 0; i < this.clazztemplates.length; i++) {
      ClazzTemplate clazzTemplate = this.clazztemplates[i];
      log(i + "/" + this.getClazzTemplates().length + " " + clazzTemplate.toString());
    }
  }
}
