package org.setupx.repository.core.communication.vocabulary;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.web.forms.PersistenceException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class LibaryCreater extends CoreObject {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new LibaryCreater object.
   */
  public LibaryCreater() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param args TODO
   *
   * @throws PersistenceException
   */
  public static void main(final String[] args) throws PersistenceException {
    LocalLibrary library = LocalLibrary.instance();
    library.add("Braunschweig");
    library.add("synechoccocus");
    library.store();
  }
}
