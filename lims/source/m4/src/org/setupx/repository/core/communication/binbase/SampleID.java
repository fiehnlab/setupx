/**
 * ============================================================================ File:    SampleID.java Package: org.setupx.repository.core.communication.binbase cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.binbase;

/**
 * ID related to a single sample on binbasesystem
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.9 $
 */
public class SampleID {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String id;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new SID object.
   *
   * @param id 
   */
  public SampleID(String id) {
    if (id == null) {
      id = "undefined";
    }

    this.id = id;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @return the id as string
   */
  public String getID() {
    return id;
  }

  /**
   * @param samplesAsArray
   *
   * @return
   */
  public static SampleID[] toSampleIDArray(String[] samplesStrings) {
    SampleID[] sampleIDs = new SampleID[samplesStrings.length];

    for (int i = 0; i < samplesStrings.length; i++) {
      sampleIDs[i] = new SampleID(samplesStrings[i]);
    }

    return sampleIDs;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return this.id;
  }

  /**
   * converts an Array of ids into an array of strings
   *
   * @param ids TODO
   *
   * @return TODO
   */
  protected static String[] asStringArray(SampleID[] ids) {
    String[] result = new String[ids.length];

    for (int i = 0; i < ids.length; i++) {
      try {
        result[i] = ids[i].getID();
      } catch (NullPointerException e) {
        e.printStackTrace();
      }
    }

    return result;
  }
}
