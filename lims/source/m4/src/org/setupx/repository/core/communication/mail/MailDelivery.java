/**
 * ============================================================================ File:    MailDelivery.java Package: org.setupx.repository.core.communication.mail cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.mail;

import org.setupx.repository.core.ServiceMediator;
import org.setupx.repository.core.util.Mail;
import org.setupx.repository.core.util.logging.Logger;

import javax.mail.MessagingException;


/**
 * reprensts a <B>single</B> email.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.10 $
 */
class MailDelivery {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private EMailAddress address;
  private String msg;
  private String subject;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * create a new MailDelivery object. the mail is not send until the notifymethod is called
   *
   * @param i_address receivers mail address
   * @param i_subject subject of the mail
   * @param i_msg message of this mail
   */
  protected MailDelivery(EMailAddress i_address, String i_subject, String i_msg) {
    this.address = i_address;
    this.subject = i_subject;
    this.msg = i_msg;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * send this mail.
   *
   * @throws MessagingException problems when sending the mail
   */
  public void notifyUser() throws MessagingException {
    String[] addresses = { this.address.getEmailAddress() };

    if (ServiceMediator.DEBUGGING_MAIL_ACTIVATED) {
      Mail.postMail(addresses, this.subject, this.msg, this.address.getEmailAddress());
    }
  }
}
