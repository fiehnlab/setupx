package org.setupx.repository.core.communication.exporting;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class UnsupportedMachine extends ExportException {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   *
   */
  public UnsupportedMachine() {
    super();

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public UnsupportedMachine(String arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public UnsupportedMachine(Throwable arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   */
  public UnsupportedMachine(String arg0, Throwable arg1) {
    super(arg0, arg1);

    // TODO Auto-generated constructor stub
  }
}
