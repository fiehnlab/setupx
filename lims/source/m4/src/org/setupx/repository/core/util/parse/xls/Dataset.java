/**
 * ============================================================================ File:    Dataset.java Package: org.setupx.repository.core.util.parse.xls cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.parse.xls;

import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.core.util.parse.ParsingException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

import java.util.Vector;


class Dataset {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // created from x or y axis 
  public int axis;
  protected String[] data;
  protected String[] key;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final int ROW = 1;
  public static final int COL = 2;
  public static final int THRESHOLD = 100;
  public static final int MIN_MEAN_LENGTH = 4;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Dataset object.
   *
   * @param sheet 
   * @param pos 
   * @param COLorROW 
   *
   * @throws ParsingException
   */
  public Dataset(HSSFSheet sheet, final int pos, final int COLorROW)
    throws ParsingException {
    this.setAxis(COLorROW);

    if (COLorROW == ROW) {
      //debug("new instance for row " + pos + "/" + sheet.getLastRowNum());
      //            for (int i = 0; i < sheet.getLastRowNum() ; i++) {
      HSSFRow row = sheet.getRow(pos);

      if (row == null) {
        throw new ParsingException("row was not found in sheet at pos: " + pos);
      }

      this.data = new String[row.getLastCellNum() - row.getFirstCellNum()];
      this.key = new String[row.getLastCellNum() - row.getFirstCellNum()];

      for (short j = row.getFirstCellNum(); j < row.getLastCellNum(); j++) {
        HSSFCell value = row.getCell(j);
        this.data[j - row.getFirstCellNum()] = convertToString(value);
        this.key[j - row.getFirstCellNum()] = findMatchingKey(sheet, j, row.getRowNum());
        //debug(j + "/" + row.getLastCellNum() + ": " + this.data[j-row.getFirstCellNum()]);
      }

      //            }
    } else if (COLorROW == COL) {
      // TODO no offset supported
      int size = sheet.getLastRowNum(); // - sheet.getFirstRowNum();
      this.data = new String[size];
      this.key = new String[size]; //row.getLastCellNum()- row.getFirstCellNum();

      for (int j = 0; j < sheet.getLastRowNum(); j++) {
        HSSFCell value = sheet.getRow(j).getCell((short) pos);

        this.data[j] = convertToString(value);
        this.key[j] = ""; //findMatchingKey(sheet, j, row.getRowNum());

        //debug(j + "/" + row.getLastCellNum() + ": " + this.data[j-row.getFirstCellNum()]);
      }
    }

    // debugging
    StringBuffer buffer = new StringBuffer();

    for (int i = 0; i < this.data.length; i++) {
      buffer.append(" - " + this.data[i]);
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public int getAxis() {
    return this.axis;
  }

  /**
   * @return
   */
  public boolean isIncreasingIDs() {
    return ((averageLength() > MIN_MEAN_LENGTH) && (similariry() > THRESHOLD));
  }

  /**
   * TODO: 
   *
   * @param mySheet TODO
   * @param myAxis TODO
   *
   * @return TODO
   */
  public static Vector filterDatasets(HSSFSheet mySheet, int myAxis) {
    Vector result = new Vector();

    // number of bad entried or gaps that will be skipped
    int tolerance = 10;

    for (int counter = 0; tolerance != 0; counter++) {
      Dataset dataset = null;

      try {
        dataset = new Dataset(mySheet, counter, myAxis);
        Logger.debug(null, "created dataset with " + dataset.data.length + " Values  -  valid: " + dataset.isValid());
      } catch (ParsingException e) {
        Logger.debug(null, "didnt find dataset at " + counter + "  axis:" + myAxis);
      }

      if ((dataset == null) || !dataset.isValid()) {
        tolerance--;
      }
      // else add it to the array
      else {
        result.add(dataset);
      }
    }

    return result;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String toString() {
    return ("is ids: " + this.isIncreasingIDs() + "    similarity:" + this.similariry() + "  meanLength:" + this.averageLength() + "  firstElement:" + this.data[0]);
  }

  /**
   * check the lenght of all entries
   */
  protected int averageLength() {
    if (this.data.length == 0) {
      return 0;
    }

    int totalLength = 0;

    for (int i = 0; i < this.data.length; i++) {
      totalLength = totalLength + this.data[i].length();
    }

    return totalLength / this.data.length;
  }

  /**
   * calculates the similarity of all entries in this dataset.
   *
   * @return the avarage similarity between two fields located next to each other - the value is between 0 and 256
   *
   * @see Util#stringCompare(String, String)
   * @see Dataset#THRESHOLD
   */
  protected int similariry() {
    if (data.length < 1) {
      return 0;
    }

    int similariry = 0;

    for (int i = 1; i < data.length; i++) {
      String value1 = data[i - 1];
      String value2 = data[i];

      // compare them
      int temp_sim = Util.stringCompare(value1, value2);

      //debug("temp_similarity: " + temp_sim);
      similariry = similariry + temp_sim;
    }

    similariry = similariry / data.length;

    //debug("similarity: " + similariry + "  number of objects: " + data.length);
    return (similariry);
  }

  /**
   * TODO: 
   *
   * @param lorROW TODO
   */
  private void setAxis(int lorROW) {
    this.axis = lorROW;
  }

  /**
   * @return
   */
  private boolean isValid() {
    return (this.averageLength() > MIN_MEAN_LENGTH);
  }

  /**
   * SCHMUTZIG
   */
  private String convertToString(HSSFCell value) {
    try {
      return value.getStringCellValue();
    } catch (Exception e) {
      try {
        return value.getNumericCellValue() + "";
      } catch (Exception e2) {
        return "";
      }
    }
  }

  /**
   * TODO: 
   *
   * @param sheet TODO
   * @param j TODO
   * @param rowNum TODO
   *
   * @return TODO
   */
  private String findMatchingKey(HSSFSheet sheet, short j, int rowNum) {
    return "TODO";
  }
}
