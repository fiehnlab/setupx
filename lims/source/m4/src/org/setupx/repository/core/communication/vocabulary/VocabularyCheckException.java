
package org.setupx.repository.core.communication.vocabulary;

public class VocabularyCheckException extends Exception {
  /**
   *
   */
  public VocabularyCheckException() {
    super();

    // TODO Auto-generated constructor stub
  }

  /**
   * @param message
   */
  public VocabularyCheckException(String message) {
    super(message);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param cause
   */
  public VocabularyCheckException(Throwable cause) {
    super(cause);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param message
   * @param cause
   */
  public VocabularyCheckException(String message, Throwable cause) {
    super(message, cause);

    // TODO Auto-generated constructor stub
  }
}
