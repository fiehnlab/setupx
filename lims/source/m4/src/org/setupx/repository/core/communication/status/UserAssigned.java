package org.setupx.repository.core.communication.status;

import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.server.persistence.PersistenceActionFindException;

public interface UserAssigned {
    
    public long getUserID();
    
    public void setUserID(long userUOID);
    
    /**
     * determine which user is assigned to the given userID <br>
     * should be <code>static</code>.
     * 
     * @param userID
     * @return
     * @throws PersistenceActionFindException
     */
    public UserDO getUser(long userID) throws PersistenceActionFindException;
}
