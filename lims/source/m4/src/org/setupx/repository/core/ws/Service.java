/**
 * ============================================================================ File:    Service.java Package: org.setupx.repository.core.ws cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.ws;

/**
 * SuperClass of all Classes, that are beeing implemented as a WebService.
 */
abstract class Service {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public final String name = this.getClass().toString();
  protected WSAccess wsAccess = null;
}
