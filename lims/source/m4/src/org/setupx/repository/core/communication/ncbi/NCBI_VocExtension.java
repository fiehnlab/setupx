/**
 * ============================================================================ File:    NCBI_VocExtension.java Package: org.setupx.repository.core.communication.ncbi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ncbi;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.xml.XPath;

import org.w3c.dom.Document;

import java.io.InputStream;

import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * extension to the NCBIconnector - connects to the remote spell check service
 * 
 * <p>
 * <B>reason:</b> the extesion for spell checking is not implemented as a SOAP service yet - so it is beeing realized as a URLconnection - receiving an XML document.
 * </p>
 */
class NCBI_VocExtension extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  protected DocumentBuilder builder;
  protected DocumentBuilderFactory factory;
  protected URL baseUrl = null;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public final static String NCBI_URL_STRING = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/espell.fcgi";
  public final static String URL_SEPERATOR = "?";
  public final static String URL_ARGUMENT_SEPERATOR = "&";
  public final static String URL_ARGUMENT_EQUAL = "=";

  {
    try {
      baseUrl = new URL(NCBI_URL_STRING);
      Util.checkURL(baseUrl);

      factory = DocumentBuilderFactory.newInstance();
      builder = factory.newDocumentBuilder();
    } catch (Exception e) {
      warning(this, e.toString());
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * debugging
   */
  public static void main(String[] args) throws Exception {
    System.out.println(new NCBI_VocExtension().request("brest+cancer"));
    System.out.println(new NCBI_VocExtension().request("Arabidopsiss"));
    System.out.println(new NCBI_VocExtension().request("human"));
    System.out.println(new NCBI_VocExtension().request("Berlin"));
    System.out.println(new NCBI_VocExtension().request("birt"));
  }

  /**
   * <b>method is not supported by ncbi !!</b>
   */
  public boolean vocabularyCheck(String word) {
    warning(this, "the check method is not supported by NCBI in " + Util.getClassName(this));

    return false;
  }

  /**
   * @see NCBIConnector#vocabularySuggestion(String)
   */
  public String vocabularySuggestion(String word) {
    try {
      return this.request(word);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return "";
  }

  /**
   * creates a request to ncbi server. parses the corrected value out of the response and returns it.
   *
   * @param word the word that is going to be used for the suggestion
   *
   * @return the corrected spelling of the word
   *
   * @throws NCBIException anything went wrong
   */
  private String request(String word) throws NCBIException {
    try {
      // create the whole url
      URL url = new URL(NCBI_URL_STRING + URL_SEPERATOR + "db" + URL_ARGUMENT_EQUAL + "pubmed" + URL_ARGUMENT_SEPERATOR + "term" + URL_ARGUMENT_EQUAL + word + URL_ARGUMENT_SEPERATOR + "email" + URL_ARGUMENT_EQUAL + NCBIConnector.EMAIL); //"tool" + URL_ARGUMENT_EQUAL + NCBIConnector.TOOL + URL_ARGUMENT_SEPERATOR + 

      debug(url);

      // connect
      URLConnection connection = url.openConnection();
      connection.connect();

      String contentType = connection.getContentType();

      /** ignoring that - is not gonna change  if (contentType.compareTo("XML") != 0) { warning(new NCBIException("the service is responding with the wrong type of content intstead of XML : " + contentType)); warning(new NCBIException("will treat it like an XML doc")); } */

      // create a doc from it
      InputStream in = connection.getInputStream();

      Document doc = builder.parse(in);

      // parse it for the answer
      String value;

      try {
        value = XPath.pathValue(doc, "/eSpellResult/CorrectedQuery");
      } catch (NullPointerException e) {
        value = "";
      }

      debug("value: " + value + "      \told one:" + word);

      // create the answer
      return value;
    } catch (Exception e) {
      throw new NCBIException(e);
    }
  }
}
