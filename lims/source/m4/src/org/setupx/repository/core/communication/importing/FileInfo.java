package org.setupx.repository.core.communication.importing;

import java.io.File;

/**
 * 
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class FileInfo {
    // ~ Instance fields
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private long lastModified;
    private long length;

    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new FileInfo object.
     * 
     * @param file
     *                
     */
    public FileInfo(File file) {
	this.lastModified = file.lastModified();
	this.length = file.length();
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * TODO: 
     * 
     * @return TODO
     */
    public long lastModified() {
	return this.lastModified;
    }

    /**
     * TODO: 
     * 
     * @return TODO
     */
    public long length() {
	return this.length;
    }
}
