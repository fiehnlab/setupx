package org.setupx.repository.core.communication.status;

import java.util.Iterator;
import java.util.List;

import com.pavelvlasov.persistence.PersistenceException;

import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.SXQuery;

/**
 * @hibernate.subclass
 */

public class ResultReceivedStatus extends PostprocessEventStatus {

    public ResultReceivedStatus(long sampleID, UserDO userDO, String message) throws StatusPersistenceException {
        super(sampleID, userDO, message);
    }
    
    public ResultReceivedStatus(){
        super();
    }

    public String createDisplayString() {
        return "result has been received.";
    }

    public PersistentStatus createClone() {
        try {
            return new ResultReceivedStatus(this.sampleID, this.getUser(getUserID()), this.getMessage());
        } catch (StatusPersistenceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (PersistenceActionFindException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static void createInstancesForPromt(String promtID, UserDO userDO) throws PersistenceException, StatusPersistenceException, PersistenceActionFindException, PersistenceActionUpdateException{
        long promtIDL = Long.parseLong(promtID);
        Iterator sampleIDs = new SXQuery().findSampleIDsByPromtID(promtIDL).iterator();
        while (sampleIDs.hasNext()) {
            long sampleID = Long.parseLong("" + sampleIDs.next());
            ResultReceivedStatus s6 = new ResultReceivedStatus(sampleID, userDO, "Status created by StatusCurator - therefore the date is incorrect. ");
            s6.update(true);
        }
    }
}
