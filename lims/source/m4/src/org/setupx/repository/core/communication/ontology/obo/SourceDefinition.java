package org.setupx.repository.core.communication.ontology.obo;

public class SourceDefinition {

    String ontologyName = "";
    String ontologyLink = "";

    /**
     * @param ontologyName
     * @param ontologyLink
     */
    public SourceDefinition(String ontologyName, String ontologyLink) {
        super();
        this.ontologyName = ontologyName;
        this.ontologyLink = ontologyLink;
    }

    public String getOntologyName() {
        return this.ontologyName;
    }

    public String getOntologyLink() {
        return this.ontologyLink;
    }

    public void setOntologyName(String ontologyName) {
        this.ontologyName = ontologyName;
    }

    public void setOntologyLink(String ontologyLink) {
        this.ontologyLink = ontologyLink;
    }

}
