package org.setupx.repository.core.util.print;
import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

/*
 * DymoPrinter.java uses these files:
 *   images/Open16.gif
 *   images/Save16.gif
 */
public class DymoPrinter extends JPanel implements ActionListener {
    static private final String newline = "\n";
    JButton openButton;
    JTextArea log;
    JFileChooser fc;
    JTextField lab, operator;

    public DymoPrinter() {
        super(new BorderLayout());

        //Create the log first, because the action listeners
        //need to refer to it.
        log = new JTextArea(12,35);
        log.setMargin(new Insets(5,5,5,5));
        log.setEditable(false);
        JScrollPane logScrollPane = new JScrollPane(log);

        //Create a file chooser
        fc = new JFileChooser();

        //Uncomment one of the following lines to try a different
        //file selection mode.  The first allows just directories
        //to be selected (and, at least in the Java look and feel,
        //shown).  The second allows both files and directories
        //to be selected.  If you leave these lines commented out,
        //then the default mode (FILES_ONLY) will be used.
        //
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fc.setFileFilter(new FileFilter(){
            public String getDescription() {
                return "Excel File";
            }
            public boolean accept(File f) {
                String filename = f.getName();
                int dotPos = filename.lastIndexOf(".");
                String fileExtension = filename.substring(dotPos + 1, filename.length());
                
                return (fileExtension.toLowerCase().compareTo("xls") == 0);
            }
        });
        
        //fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        //Create the open button.  We use the image from the JLF
        //Graphics Repository (but we extracted it from the jar).
        openButton = new JButton("Print File");
        openButton.addActionListener(this);


        //For layout purposes, put the buttons in a separate panel
        JPanel buttonPanel = new JPanel(); //use FlowLayout
        
        
        lab = new JTextField(7);
        lab.setToolTipText("enter the lab that you are working for - it will be printed in the second line.");
        lab.setDocument(new TextFieldLimiter(12));
        lab.setText("your labname");
        
        operator = new JTextField(7);
        operator.setToolTipText("enter your short name so that everyone knows who these samples belong to.");
        operator.setDocument(new TextFieldLimiter(6));
        operator.setText("user");
        
        buttonPanel.add(lab);
        buttonPanel.add(operator);
        buttonPanel.add(openButton);
        

        //Add the buttons and the log to this panel.
        add(buttonPanel, BorderLayout.PAGE_START);
        add(logScrollPane, BorderLayout.CENTER);
        
        log.append("Print labels from your excelsheet. \n\n1. Enter the name of the lab and your name.\n2. Press print File & Select your XLS sheet.\n3.The first column is taken from the XLSsheet and printed in the middle.\n  While the second column is printed below that ID.\n  Do not use formulas and the labels should be Strings and NOT numeric.\n4.Enjoy your samples.\n   Questions? write Martin Scholz\n         mscholz@ucdavis.edu"+ newline);
        
        
    }

    public void actionPerformed(ActionEvent e) {

        //Handle open button action.
        if (e.getSource() == openButton) {
            int returnVal = fc.showOpenDialog(DymoPrinter.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                //This is where a real application would open the file.
                log.append("Opening: " + file.getName() + "." + newline);
                
                log.append("Sending Printjob to printer." + newline);
                DYMOLabelPrintConnector4XLS labelprinter =new DYMOLabelPrintConnector4XLS(file, lab.getText(), operator.getText(), true, true, log);
                log.append("printing " + labelprinter.getPageNumbers() + " pages." +newline);
            } else {
                log.append("Open command cancelled by user." + newline);
            }
            log.setCaretPosition(log.getDocument().getLength());
        }
    }



    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("SetupX Orbiter LabPrinter BETA 0.8");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        JComponent newContentPane = new DymoPrinter();
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
