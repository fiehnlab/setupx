package org.setupx.repository.core.communication.vocabulary;

import junit.framework.TestCase;

public class GoogleCheckTest extends TestCase {

    public GoogleCheckTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testGetSuggestion() throws VocabularyCheckException {
        assertTrue(new GoogleCheck().getSuggestion("mispelled").length() > 5);
    }
    public void testGetSuggestion2() throws VocabularyCheckException {
        assertTrue(new GoogleCheck().getSuggestion("mispelled").compareTo("misspelled") == 0);
        
    }

    public void testCheck() throws VocabularyCheckException {
        new GoogleCheck().check("misspelled");
    }
}
