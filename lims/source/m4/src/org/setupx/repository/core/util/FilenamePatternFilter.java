package org.setupx.repository.core.util;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.setupx.repository.core.util.logging.Logger;

public class FilenamePatternFilter implements FilenameFilter {

    private String patternString;

    public FilenamePatternFilter(String pattern) {
        this.patternString = pattern;
    }

    public boolean accept(File dir, String name) {
        try {
            // String patternString = "(.{6})([abz])([a-z]{2})sa(.{1,4})"; // (:)(.*)
            // Logger.debug(this, "checking " + name + " vs. " + patternString);
            Pattern pattern = Pattern.compile(patternString);
            Matcher m = pattern.matcher(name);
            return m.matches();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
