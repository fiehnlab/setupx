package org.setupx.repository.core.communication.exporting;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class ExportException extends Exception {
  /**
   * Creates a new ExportException object.
   */
  public ExportException() {
    super();
  }

  /**
   * Creates a new ExportException object.
   *
   * @param arg0 
   */
  public ExportException(Throwable arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param string
   * @param ex
   */
  public ExportException(String string, Exception ex) {
    super(string, ex);
  }

  /**
   * @param string
   */
  public ExportException(String string) {
    super(string);
  }

/**
 * @param arg0
 * @param arg1
 */
public ExportException(String arg0, Throwable arg1) {
    super(arg0,arg1);
    // TODO Auto-generated constructor stub
}
}
