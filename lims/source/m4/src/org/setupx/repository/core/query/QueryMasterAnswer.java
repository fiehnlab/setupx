package org.setupx.repository.core.query;

import java.util.Vector;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.user.UserDO;


/**
 * Object representing an Answer from the {@link QueryMaster}. 
 * <p>The {@link QueryMasterAnswer} is a unique way of discribing a different set of possible answers.
 * <p> {@link QueryMasterAnswer} is used in order to be able to place a very general query and get different
 * types of answers to that query. The {@link QueryMasterAnswer} contains all information needed to use the 
 * answer in a linked webfrontend and create a visual representation.
 *  
 * @author Martin Scholz
 *
 */
public abstract class QueryMasterAnswer extends CoreObject {

  
    /**
     * @return url of an icon   
     */
    public abstract String getIcon();
    
    public abstract String getLabel();

    /**
     * label disribing where the search term was found in or why the result was returned
     * @return
     */
    public abstract String getInfo1();
    
    public abstract String getInfo2();

    public abstract String getInfo3();
    
    public String toString(){
        return ("label: " + this.getLabel() + "  info1:" + this.getInfo1() + " info2"  + this.getInfo2() + " info3 " +   this.getInfo3() + "  " + this.getRelatedObjectLinks().size() + "links " + this.getIcon());
    }
        
    /**
     * @return link to the related object (sample - linked to sample_detail.jsp, ...)
     */
    public abstract Vector getRelatedObjectLinks();
  
    /**
     * @return true if the acces to this information is restricted
     */
    public abstract boolean isAccessRestricted();
    
    /**
     * @return true if the acces to this information is accessible to the specific user
     */
    public abstract boolean checkAccessRestriction(UserDO userDO);
}
