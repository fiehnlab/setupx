package org.setupx.repository.core.communication.technology.scanner;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.hibernate.Session;
import org.setupx.repository.Config;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.technology.platform.Datafile;
import org.setupx.repository.core.communication.technology.platform.TechnologyFileType;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;

public class ScanningProcess extends org.setupx.repository.core.communication.BackgroundThread {

    private boolean externalPaused = false;
    
    private TechnologyFileType technologyFileType = null;

    // ms between scans
    private long timeBetweenScan = Config.FILESCANNER_PAUSE;

    private long minimumSXIDNumber;

    private Date lastScan = new Date();

    public ScanningProcess(TechnologyFileType technologyFileType2, long minimumSXidNumber) {
        super("--- Init scanner for Technology : " + technologyFileType2.getLabel());
        this.technologyFileType = technologyFileType2;
        this.minimumSXIDNumber = minimumSXidNumber;
        this.setName(technologyFileType2 + "_" + technologyFileType2.getUOID());
        this.setExternalPaused(technologyFileType2.isAutomaticAssignment());
        this.setDaemon(true);
        this.start();
        ScanningProcessMonitor.add(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Thread#run()
     */
    public void run() {
        for (; true;) {
            try {
                Thread.sleep(this.timeBetweenScan);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (!this.isExternalPaused()){
                Logger.info(this, this.getTechnologyFileType().getLabel() +  " - scanning: " + Util.getDateString());
                scan();
            } else {
                Logger.info(this, this.getTechnologyFileType().getLabel() +  " - not scanning: -pausedscanning-  " + Util.getDateString());
            }

        }
    }

    /**
     * performing the actual scan
     */
    private void scan() {
        // trigger to store the datafiles or simply scan them
        boolean storeDatafiles = true;
        Session s = CoreObject.createSession();

        debug("===== staring scan " + this.technologyFileType.getTechnology().getLabel()  + "-" + this.technologyFileType.getLabel() + " =====");

        // take each location of the
        if (this.technologyFileType.isAutomaticAssignment()){
            debug("Scan process in progress - Automatic datafile assignment for " + this.technologyFileType.getLabel() + " active.");
            Collection possibleDatafiles = this.technologyFileType.getPossibleDatafiles(this.minimumSXIDNumber);
            Iterator iterator = possibleDatafiles.iterator();
    
            while (iterator.hasNext()) {
                Datafile datafile = (Datafile) iterator.next();
                datafile.myUpdateMeAndChilds(s, storeDatafiles);
                debug("Found datafiles for sample: " + datafile.getSampleID() + "   file: "
                        + datafile.getSourceFile().getName());
            }
            debug("Number of new Datafiles found: " + possibleDatafiles.size());
        } else {
            debug("Scan process chancelled - Automatic datafile assignment for " + this.technologyFileType.getLabel() + " deactivated.");
        }

        this.lastScan  = new Date();
        s.close();
        debug("==== finished scan ====");
        
    }

    private void debug(String string) {
        Logger.debug(this, this.technologyFileType.getLabel() + ": " + string);
    }

    public Date getLastScan() {
        return lastScan;
    }

    public long getTimeBetweenScan() {
        return timeBetweenScan;
    }

    public long getMinimumSXIDNumber() {
        return minimumSXIDNumber;
    }

    public TechnologyFileType getTechnologyFileType() {
        return technologyFileType;
    }

    public boolean isExternalPaused() {
        return externalPaused;
    }

    public void setExternalPaused(boolean externalPaused) {
        this.externalPaused = externalPaused;
    }

    public void setTimeBetweenScan(long timeBetweenScan) {
        this.timeBetweenScan = timeBetweenScan;
    }
    
    
}
