package org.setupx.repository.core.communication.importing;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.hibernate.Session;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.file.SampleFile;
import org.setupx.repository.core.communication.technology.platform.Datafile;
import org.setupx.repository.core.communication.technology.platform.TechnoCoreObject;
import org.setupx.repository.core.communication.technology.platform.Technology;
import org.setupx.repository.core.communication.technology.platform.TechnologyFileType;
import org.setupx.repository.core.util.logging.Logger;

public class SSMerger extends CoreObject {
    public short[] ids = SampleFile.TYPES;
    private Session hqlSession;
    private Technology technology;
    private Hashtable scannedList;

    public static void main(String[] args) {
        long technologyID = 2;
        Logger.log(null, "working on " + technologyID);
        new SSMerger(technologyID, readScannedList()).createDatafiles();
    }

    /**
     * @return Hashtable (containing key: label value: sampleID)
     */
    private static Hashtable readScannedList() {
        File sourceFile = new File("/tmp/expScannedSamples");
        Hashtable ht = new Hashtable();
        
        StringTokenizer tokenizerAll;
        try {
            tokenizerAll = new StringTokenizer(org.setupx.repository.core.util.File.getFileContent(sourceFile), "\n");

            while (tokenizerAll.hasMoreElements()) {
                String line = (String) tokenizerAll.nextElement();
                StringTokenizer lineTokenizer = new StringTokenizer(line, ",");
                String name = lineTokenizer.nextToken();
                String id = lineTokenizer.nextToken();
                ht.put(name, id);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        

        /*
        ht.put("081125abrsa223:2", "589420");
        ht.put("080523abrsa16:1", "335618");
        ht.put("080422abrsa67:1", "487962");
        ht.put("080708abrsa127:1", "336266");
        ht.put("081031abrsa10:1", "669225");
        */Logger.log(null, "number of elements: " + ht.size());
        
        return ht;
    }

    public SSMerger(long techID, Hashtable scannedList) {
        this.hqlSession = CoreObject.createSession();
        this.scannedList = scannedList;
        this.technology = (Technology) TechnoCoreObject.persistence_loadByID(Technology.class, hqlSession, techID);
    }

    private void createDatafiles() {
        Enumeration keys = this.scannedList.keys();
        String sampleLabel = "";
        long sampleID = 0;

        while (keys.hasMoreElements()) {
            try {
                sampleLabel = keys.nextElement().toString();
                sampleID = Long.parseLong(this.scannedList.get(sampleLabel).toString());
                Logger.log(this, "received from list: " + sampleID + ": " + sampleLabel);
                this.createGCMS(sampleLabel, sampleID);
            }catch (Exception e) {
                log("problem " + e.getMessage());
            }
        }
        complete();
    }

    public void createGCMS(String sampleName, long sampleID) {
        log("--------start: " + sampleID + "---------");
        for (int aa = 0; aa != this.ids.length; aa++) {
            short type = this.ids[aa];

            try {
                TechnologyFileType technologyFileType = this.determineTechnofiletype(type);
                log(sampleID + ": " + technologyFileType.getLabel());
                SampleFile sampleFile = new SampleFile(sampleName);

                File file = sampleFile.getFile(type);

                List l = Datafile.findDataFilesBySampleID(sampleID, hqlSession);
                if (l.size() != 0){
                    Iterator iterator = l.iterator();
                    while (iterator.hasNext()) {
                        Datafile datafile = (Datafile) iterator.next();
                        if (datafile.getSourceFile().getAbsolutePath().compareTo(file.getAbsolutePath()) == 0){
                            throw new MergerException("Datafile " + datafile.toString() + " exists.");
                        }
                    }
                }
                Datafile datafile = Datafile.create(sampleID, file);


                technologyFileType.addDatafile(datafile);
                Logger.log(this, " ok: " + datafile.getSampleID() + " " + datafile.getSource());
            } catch (Exception e) {
                Logger.log(this, "problem: " + sampleID + "  " + sampleName + "  " + e.getMessage());
                //e.printStackTrace();
            }
        }
        log("-------------------e-n-d---------");
    }

    /** 
     * find the technofiletype to a certain extension 
     * */
    private TechnologyFileType determineTechnofiletype(short type) throws MergerException {
        Iterator techIterator = this.technology.getTechnologyFileTypes().iterator();
        while (techIterator.hasNext()) {
            TechnologyFileType technologyFileType = (TechnologyFileType) techIterator.next();
            if (technologyFileType.getPattern().toLowerCase().indexOf(SampleFile.getExtension(type).toLowerCase()) > -1) {        
                return technologyFileType;
            }
        }
        throw new MergerException("unable to find matching technologyfiletype for " + SampleFile.getExtension(type));
    }

    protected void complete(){
        this.technology.updatingMyself(this.hqlSession, true);
        this.hqlSession.close();
    }
}
