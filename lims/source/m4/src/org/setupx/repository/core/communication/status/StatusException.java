package org.setupx.repository.core.communication.status;


public class StatusException extends Exception{

    public StatusException(Exception e) {
        super(e);
    }

}
