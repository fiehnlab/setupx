/**
 * ============================================================================ File:    PrepSOP.java Package: org.setupx.repository.core.communication.sop cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.sop;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class PrepSOP extends SOP {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String[][] getValues() {
    return new String[][] {
      new String[] { "Metabolomics Vol. 1, No. 1, January 2005 ( 2005)", "http://metacore.ucdavis.edu/services/protocols/Metabolomics%20Vol.%201%2C%20No.%201%2C%20January%202005%20%28%202005%29.pdf" },
      new String[] { "Proteomics 2004, 4, 78-83", "Plant Physiol. Vol. 138, 2005", "http://metacore.ucdavis.edu/services/protocols/Proteomics%202004%2C%204%2C%2078-83.pdf" },
      new String[] { "Plant Physiol. Vol. 138, 2005", "Plant Physiol. Vol. 138, 2005", "http://metacore.ucdavis.edu/services/protocols/SulfurDeprivation.pdf" }
    };
  }
}
