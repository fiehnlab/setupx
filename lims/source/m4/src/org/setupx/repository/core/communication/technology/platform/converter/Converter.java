package org.setupx.repository.core.communication.technology.platform.converter;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.file.SampleFile;
import org.setupx.repository.core.communication.technology.platform.Datafile;
import org.setupx.repository.core.communication.technology.platform.Location;
import org.setupx.repository.core.communication.technology.platform.LocationException;
import org.setupx.repository.core.communication.technology.platform.Technology;
import org.setupx.repository.core.communication.technology.platform.TechnologyFileType;
import org.setupx.repository.core.communication.technology.platform.TechnologyFileTypeNotRegisteredException;
import org.setupx.repository.core.communication.technology.platform.TechnologyProvider;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.SXQuery;


/**
 * converts existing samplefiles into Datafiles
 * @author scholz
 *
 */
public class Converter {
    
    public Converter(){
        
    }

    public void run() {
        // take each existing filetype and create a matching one for this
        short[] fileTypesOld = SampleFile.TYPES;
        for (int i = 0; i < fileTypesOld.length; i++) {
            short filetTypeShortOld = fileTypesOld[i];
            
            TechnologyFileType technologyFileTypeNew = determineTechnologyFileType(filetTypeShortOld);
            
            // take all files for each sample 
            Iterator sampleIDs = findAllSampleIDs().iterator();
            while (sampleIDs.hasNext()) {
                long sampleID = Long.parseLong("" + sampleIDs.next());
                    
                SampleFile sampleFileOld;
                try {
                    sampleFileOld = new SampleFile(sampleID);
    
                    File sourceFile = sampleFileOld.getFile(filetTypeShortOld);
                    
                    // create a new Datafileobject out of this
                    Datafile datafileNew = new Datafile(sampleID, sourceFile);
                        
                    // add it and save it
                    technologyFileTypeNew.addDatafile(datafileNew);
                } catch (PersistenceActionFindException e) {
                
                }
            }
        }
        // missing update !!
    }

    private List findAllSampleIDs() {
        return CoreObject.createSession().createSQLQuery("select uoid from formobject where discriminator like \"%Sample\" ").list();
    }

    
    private TechnologyFileType determineTechnologyFileType(short filetTypeShortOld) {
        // load the technologie that we want to assign the existing files to
        Technology technologyNew = new Technology("GC", "Technology replaces current SampleFiles. All attached datafiles are automaticly added.");
        try {
            return technologyNew.getTechnologyFileType(SampleFile.getExtension(filetTypeShortOld));
        } catch (TechnologyFileTypeNotRegisteredException e) {
            // technologyfiletype not registerd yet
            // so creating on
            String pattern = "";
            try {
                TechnologyFileType technologyFileType = new TechnologyFileType(SampleFile.getExtension(filetTypeShortOld), "converted files" , new Location("/mnt"), pattern);
                return technologyFileType;
            } catch (LocationException e1) {
                throw new RuntimeException("no such location");
            }
        }
    }
}
