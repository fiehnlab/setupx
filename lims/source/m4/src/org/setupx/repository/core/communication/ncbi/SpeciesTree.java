package org.setupx.repository.core.communication.ncbi;

import java.util.HashSet;
import java.util.Iterator;

import org.setupx.repository.core.communication.ncbi.local.NCBIEntry;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.SXQuery;


/**
 * 
 *
 * @author $author$
 * @version $Revision$
 */
public class SpeciesTree {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final SpeciesTree thiz = new SpeciesTree();
  private static HashSet speciesCodes = new HashSet();
  private static HashSet splitpoints = new HashSet();

  static {
    init();

    try {
      group();
    } catch (NCBIException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public static String status() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("  \n\n Splitpoints:");

    Iterator iterator = splitpoints.iterator();

    while (iterator.hasNext()) {
      buffer.append("  \n\n-->" + iterator.next());
    }

    Iterator promtIDs = new SXQuery().findPromtIDs().iterator();

    while (promtIDs.hasNext()) {
      long promtID = Long.parseLong("" + promtIDs.next());
      Iterator iteratorA = new SXQuery().findSpeciesbyPromtID(promtID).iterator();

      while (iteratorA.hasNext()) {
        buffer.append(promtID + "  " + iteratorA.next());
      }
    }

    return buffer.toString();
  }

  /**
   * TODO: 
   *
   * @param ncbiSpeciesID TODO
   *
   * @return TODO
   */
  public NCBIEntry findMyGroup(int ncbiSpeciesID) {
    NCBIEntry object;

    try {
      object = NCBIConnector.determineNCBI_Information(ncbiSpeciesID);
    } catch (NCBIException e) {
      return null;
    }

    if (splitpoints.contains(object)) {
      return object;
    } else {
      return findMyGroup(object.getTaxIdInt());
    }
  }

  /**
   * checking which of the species codes are childs of a given {@link NCBIEntry}
   *
   * @param ncbiEntryParent
   *
   * @return
   *
   * @throws NCBIFindException
   */
  private static HashSet checkForExistingExperiment(NCBIEntry ncbiEntry) {
    HashSet selection = new HashSet();

    Iterator childs;

    try {
      childs = ncbiEntry.getChilds().iterator();
    } catch (NCBIFindException e) {
      // (( cant get childs for this ))
      Logger.warning(thiz, "unable to find childs for : " + ncbiEntry.getName());

      return new HashSet();
    }

    while (childs.hasNext()) {
      // in case two are found we declare it a brench ;-)
      if (selection.size() > 1) {
        return selection;
      }

      NCBIEntry child = (NCBIEntry) childs.next();

      // the codes
      Iterator iterator = speciesCodes.iterator();

      while (iterator.hasNext()) {
        String id = "" + iterator.next();

        //Logger.log(thiz, "checking child: " + child.getName() + " " + child.getTaxID() + " vs. " + id);
        if (child.getTaxID().compareTo(id) == 0) {
          // we got one
          Logger.log(thiz, "got one match: " + child.getName() + " is a child of " + ncbiEntry);
          selection.add(id);
        } else {
          // check the child itself
          selection.addAll(checkForExistingExperiment(child));
        }
      }
    }

    return selection;
  }

  /**
   * TODO: 
   *
   * @param splitpoints2 TODO
   * @param taxID TODO
   *
   * @return TODO
   */
  private static boolean containedId(HashSet splitpoints2, String taxID) {
    Iterator iterator = splitpoints2.iterator();

    while (iterator.hasNext()) {
      NCBIEntry element = (NCBIEntry) iterator.next();

      if (element.getTaxID().compareTo(taxID) == 0) {
        Logger.log(thiz, "its a splitploint - so keep going");

        return true;
      }
    }

    return false;
  }

  /**
   * defining splitpoints - ncbielements that have more then one childelement
   *
   * @throws NCBIException
   */
  private static void group() throws NCBIException {
    Iterator speciesCodesiterator = speciesCodes.iterator();

    // take each element 
    while (speciesCodesiterator.hasNext()) {
      try {
        Object object = speciesCodesiterator.next();
        int speciesCode = Integer.parseInt(object + "");
        NCBIEntry ncbiEntry = NCBIConnector.determineNCBI_Information(speciesCode);

        // not a splitpoint yet
        group(ncbiEntry);
      } catch (java.util.ConcurrentModificationException e) {
        // dat is schon lange wech
        speciesCodesiterator = speciesCodes.iterator();
      }
    }
  }

  /**
   * TODO: 
   *
   * @param ncbiEntry TODO
   *
   * @throws NCBIFindException TODO
   */
  private static void group(NCBIEntry ncbiEntry) throws NCBIFindException {
    NCBIEntry ncbiEntryParent = ncbiEntry.getParent();

    if (!splitpoints.contains(ncbiEntryParent)) {
      //check number of totalchilds
      HashSet experiments = checkForExistingExperiment(ncbiEntryParent);
      int numOfExp = experiments.size();

      if (numOfExp > 1) {
        Logger.log(thiz, "number of for existing experiments " + numOfExp + " for " + ncbiEntry.getName());
        // there is more then the element that we started with as a known entry in there
        splitpoints.add(ncbiEntryParent);
      } else {
        Logger.log(thiz, "not enough for " + ncbiEntry.getName() + " I keep searching for my parent " + ncbiEntryParent.getName());
        group(ncbiEntryParent);
      }
    } else {
      Logger.log(thiz, "parent is an existing slitpoint");
    }
  }

  /**
   * TODO: 
   */
  private static void init() {
    Iterator speciesNamesIterator = new SXQuery().findSpecies().iterator();

    /*Vector dummy = new Vector();
       dummy.add("Homo sapiens neanderthalensis");
       dummy.add("Pan troglodytes verus");
       dummy.add("Pongo pygmaeus pygmaeus ");
    
       Iterator speciesNamesIterator = dummy.iterator();
     */

    // mapping them to ncbi codes
    while (speciesNamesIterator.hasNext()) {
      String speciesName = (String) speciesNamesIterator.next();
      long ncbiCode = 0;

      try {
        ncbiCode = NCBIConnector.determineNCBI_Id(speciesName);
        speciesCodes.add("" + ncbiCode);
      } catch (NCBIException e) {
        e.printStackTrace();
      }
    }
  }
}
