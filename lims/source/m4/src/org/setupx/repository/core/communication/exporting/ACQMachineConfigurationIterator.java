/**
 * ============================================================================ File:    ACQMachineConfigurationIterator.java Package: org.setupx.repository.core.communication.exporting cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.exporting;

import org.setupx.repository.core.CoreObject;

import java.util.Iterator;


/**
 * iterator containing all Parameters in the correct order for a <b>single </b>Machine
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.1 $
 */
public class ACQMachineConfigurationIterator extends CoreObject implements Iterator {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String[] valuesInOrder;
  private int pos = 0;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new MachineConfigurationIterator object.
   *
   * @param values 
   */
  protected ACQMachineConfigurationIterator(String[] values) {
    debug(this, "creating a new MachineConfigurationIterator - size: " + values.length);
    this.valuesInOrder = values;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /*
   *  (non-Javadoc)
   * @see java.util.Iterator#hasNext()
   */
  public boolean hasNext() {
    //Logger.log(this, "hasNext " + !(valuesInOrder.length == pos) + " pos: " +pos+ "   - iterator size: " + this.valuesInOrder.length);
    return !(valuesInOrder.length == pos);
  }

  /*
   *  (non-Javadoc)
   * @see java.util.Iterator#next()
   */
  public Object next() {
    return valuesInOrder[pos++];
  }

  /*
   *  (non-Javadoc)
   * @see java.util.Iterator#remove()
   */
  public void remove() {
  }

  /**
   * resets the iterator to pos 0 !
   */
  public void reset() {
    //Logger.log(this, "resetting pos: " +pos+ "   - iterator size: " + this.valuesInOrder.length);
    pos = 0;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String toString() {
    StringBuffer buffer = new StringBuffer();

    for (int i = 0; i < this.valuesInOrder.length; i++) {
      buffer.append(this.valuesInOrder[i]).append(" - ");
    }

    return buffer.toString();
  }

  /**
   * creates an iterator containing all Parameters in the correct order for a Machine
   *
   * @param machine the machine for which the configuration will be creates
   *
   * @return an iterator containing all Parameters in the correct order for a Machine
   *
   * @throws UnsupportedMachine if the machine is not supported by this version
   */
  protected static ACQMachineConfigurationIterator iterator(Machine machine) {
    // throws UnsupportedMachine {
    switch ((int) machine.getUOID()) {
    case Machine.E_MACHINE:
      return new ACQMachineConfigurationIterator(new String[] {
          // new Leco Version 2
        AcquisitionParameter.NAME, AcquisitionParameter.TYPE, AcquisitionParameter.FOLDER, AcquisitionParameter.QC_METHOD, AcquisitionParameter.AS_METHOD, AcquisitionParameter.GC_METHOD, AcquisitionParameter.MS_METHOD, AcquisitionParameter.DP_METHOD, AcquisitionParameter.TRAY,
          AcquisitionParameter.VIAL, AcquisitionParameter.REPETITIONS
        });

    default:
      warning(null, new UnsupportedMachine("the machinetype " + machine.getUOID() + " is not supported.").toString());

      return new ACQMachineConfigurationIterator(new String[] {
          // new Leco Version 2
        AcquisitionParameter.NAME, AcquisitionParameter.TYPE, AcquisitionParameter.FOLDER, AcquisitionParameter.QC_METHOD, AcquisitionParameter.AS_METHOD, AcquisitionParameter.GC_METHOD, AcquisitionParameter.MS_METHOD, AcquisitionParameter.DP_METHOD, AcquisitionParameter.TRAY,
          AcquisitionParameter.VIAL, AcquisitionParameter.REPETITIONS
        });
    }
  }
}
