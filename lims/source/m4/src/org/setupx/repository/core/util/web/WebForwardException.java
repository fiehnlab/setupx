package org.setupx.repository.core.util.web;
public class WebForwardException extends WebException {

    public WebForwardException(String string) {
        super(string);
    }

    public WebForwardException(String string, Throwable throwable) {
        super(throwable, string);
    }

    public WebForwardException(Exception e) {
        super(e);
    }
}
