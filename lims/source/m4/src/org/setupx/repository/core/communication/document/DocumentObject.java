package org.setupx.repository.core.communication.document;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.setupx.repository.Config;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;

/**
 * Abstract Document object / missing persistence and a jsp to create and display
 * 
 * @author scholz
 * @hibernate.class table = "document"
 * @hibernate.discriminator column = "discriminator"
 */
public abstract class DocumentObject extends DocumentCoreObject{

    /**
     * 
     */
    public DocumentObject() {

    }

    public static Date NEVER = new Date(200, 12, 12);

    private String label = "";
    private String organisation = "";
    private String areaValid = "";

    private String version = "";

    private Date dateIssued = new Date();
    private Date dateValidStart = NEVER;

    private UserDO definedBy = null;
    private UserDO responsible = null;

    private DocumentCategory category = null;

    private String modificationsToLastVersion = "";
    private DocumentObject replcaesDocument = null;

    private boolean publiclyAvailable = false;

    private String comment = "";
    private String content = "";

    private String filePath = "";

    // unique identifier
    private long uoid;

    /**
     * @hibernate.property
     */
    public String getLabel() {
        return label;
    }

    /**
     * @hibernate.property
     */
    public String getOrganisation() {
        return organisation;
    }

    /**
     * @hibernate.property
     */
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @hibernate.property
     */
    public String getAreaValid() {
        return areaValid;
    }

    /**
     * @hibernate.property
     */
    public String getVersion() {
        return version;
    }

    /**
     * @hibernate.property
     */
    public Date getDateIssued() {
        return dateIssued;
    }

    /**
     * @hibernate.property
     */
    public Date getDateValidStart() {
        return dateValidStart;
    }

    public Date getDateValidEnd() {
        // no following document
        if (this.isReplacedBy() == null) {
            return NEVER;
        }
        // only ONE following document
        else {
            // 
            if (this.isReplacedBy().getDateValidStart().before(this.isReplacedBy().getDateValidEnd())) {
                return this.isReplacedBy().getDateValidStart();
            } else {
                return this.isReplacedBy().getDateValidEnd();
            }
        }
    }

    /**
     * User that created the document
     * 
     * @hibernate.many-to-one column="creator" class = "org.setupx.repository.core.user.UserDO"
     */
    public UserDO getDefinedBy() {
        return definedBy;
    }

    /**
     * {@link UserDO} responsible for this document
     * 
     * @hibernate.many-to-one column="userresponsible" class = "org.setupx.repository.core.user.UserDO"
     */
    public UserDO getResponsible() {
        return responsible;
    }

    /**
     * @hibernate.property
     */
    public String getModificationsToLastVersion() {
        return modificationsToLastVersion;
    }

    /**
     * replaced document
     * 
     * @hibernate.many-to-one column="replcesDocument" class =
     *                        "org.setupx.repository.core.communication.document.DocumentObject"
     */
    public DocumentObject getReplcaesDocument() {
        return replcaesDocument;
    }

    /**
     * @hibernate.property
     */
    public boolean isPubliclyAvailable() {
        return publiclyAvailable;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public void setAreaValid(String areaValid) {
        this.areaValid = areaValid;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setDateIssued(Date dateIssued) {
        this.dateIssued = dateIssued;
    }

    public void setDateValidStart(Date dateValidStart) {
        this.dateValidStart = dateValidStart;
    }

    public void setDefinedBy(UserDO definedBy) {
        this.definedBy = definedBy;
    }

    public void setResponsible(UserDO responsible) {
        this.responsible = responsible;
    }

    public void setModificationsToLastVersion(String modificationsToLastVersion) {
        this.modificationsToLastVersion = modificationsToLastVersion;
    }

    public void setReplcaesDocument(DocumentObject replcaesDocument) {
        this.replcaesDocument = replcaesDocument;
    }

    public void setPubliclyAvailable(boolean publiclyAvailable) {
        this.publiclyAvailable = publiclyAvailable;
    }

    /**
     * uploads a document wich will be referenced from this document
     * 
     * @return the path where the file is located at
     * @throws DocumentObjectException
     */
    public static String uploadDocument(File file, DocumentObject relatedDocument) throws DocumentObjectException {
        // upload it
        File targetFolder = new File(Config.DIRECTORY_DOCUMENTOBJECTS);
        File newFile;
        try {
            newFile = org.setupx.repository.core.util.File.copy(targetFolder, file);
            // set the path in the document
            Logger.debug(null, "copied file to" + targetFolder.getPath());
            relatedDocument.setFilePath(newFile.getPath());
            return newFile.getPath();
        } catch (IOException e) {
            throw new DocumentObjectException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return this.getUOID() + ": " + this.getLabel() + "(Version " + this.getVersion() + ") " + this.getComment()
                + " " + this.getContent() + " active: " + isActive() + " public: " + isPubliclyAvailable()
                + " expires: " + this.getDateValidEnd();
    }

    /**
     * is active: - has not been replaced - start of valid time is before now
     * <li>is active when validstart before now
     * <li>has not been replaced or the replacing document is not active yet
     * 
     * @return active or not
     */
    public boolean isActive() {
        return (this.getDateValidStart().before(new Date()) && (this.getDateValidEnd().after(new Date())));
    }

    /**
     * returns the document that this document was replaced by.
     */
    public DocumentObject isReplacedBy() {
        try {
            return (DocumentObject) this.createSession().createSQLQuery(
                    "select * from document where replcesDocument = " + this.getUOID() + " order by uoid desc limit 1")
                    .addEntity(DocumentObject.class).list().get(0);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @hibernate.property
     */
    public String getComment() {
        return comment;
    }

    /**
     * @hibernate.property
     */
    public String getContent() {
        return content;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setContent(String content) {
        this.content = content;
    }

    /**
     * The setter method for Unique Object identifier.
     */
    public void setUOID(long lId) {
        this.uoid = lId;
    }

    /**
     * The getter method for Unique Object identifier.
     * <p>
     * The id is generated by the persistence layer.
     * </p>
     * 
     * @return unique id for this object
     * @hibernate.id column = "uoid" generator-class="native"
     */
    public long getUOID() {
        return (this.uoid);
    }


    /**
     * @hibernate.many-to-one column="category" class =
     *                        "org.setupx.repository.core.communication.document.DocumentCategory"
     */
    public DocumentCategory getCategory() {
        return category;
    }

    public void setCategory(DocumentCategory category) {
        this.category = category;
    }
}
