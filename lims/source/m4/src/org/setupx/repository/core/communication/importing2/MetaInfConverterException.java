/*
 * Created on 14.11.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.setupx.repository.core.communication.importing2;

/**
 * @author scholz
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MetaInfConverterException extends Exception {

    /**
     * 
     */
    public MetaInfConverterException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public MetaInfConverterException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public MetaInfConverterException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     */
    public MetaInfConverterException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
