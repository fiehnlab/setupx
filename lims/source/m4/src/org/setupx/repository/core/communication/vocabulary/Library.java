/**
 * ============================================================================ File:    Library.java Package: org.setupx.repository.core.communication.vocabulary cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.vocabulary;

import org.setupx.repository.core.Connectable;


/**
 * Library containing lots of words.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.9 $
 */
public interface Library extends Connectable {
  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * checks if the Library contains the defined word
   *
   * @param word the word to look for
   *
   * @return true if it is known - false if not
   */
  public boolean contains(final String word);

  /**
   * returns the <B>best</B> suggestion for the word
   *
   * @param word the word the suggestion is looked for
   *
   * @return the suggestion
   */
  public String createSuggestion(final String word);
}
