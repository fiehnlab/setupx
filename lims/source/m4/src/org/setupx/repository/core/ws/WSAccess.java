package org.setupx.repository.core.ws;

import org.setupx.repository.Config;
import org.setupx.repository.core.user.UserNotFoundException;
import org.setupx.repository.core.util.logging.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.Date;

import javax.servlet.ServletContext;


/**
 * Descripes ONE access from one client to any of the webservices. It contains some of the informations about the Client, ...
 */
public class WSAccess {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Date date;
  private ServletContext context;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new WSAccess object.
   *
   * @param accessedService 
   * @param username 
   * @param password 
   *
   * @throws UserNotFoundException 
   */
  public WSAccess(String emailAccess, String password, String requieredLevel, String method_decription)
    throws LocalUnauthorizedAccessException, LocalServiceException {
    /*
       //Logger.log(this, "user is trying to connect: " + emailAccess + "  " + password + "   ");
       try {
         HttpServlet srv = (HttpServlet) org.apache.axis.MessageContext.getCurrentContext()
                                                                       .getProperty(HTTPConstants.MC_HTTP_SERVLET);
         context = srv.getServletContext();
         Logger.log(this, "Server: " + context.getServerInfo());
         Enumeration enu = context.getAttributeNames();
         while (enu.hasMoreElements()) {
           String _attName = (String) enu.nextElement();
           String _msg = "---> " + _attName + ": " + context.getAttribute(_attName);
           Logger.log(this, _msg);
           createLogEntry(_msg);
                      request.getRemoteAddr();
            }
          } catch (NullPointerException ex) {
            // here might be some exceptions, if the service is beeing run as a local service - so
            // there will be no Servlet-Context. Can be ignored.
            Logger.debug(thiz, ex);
          }*/
    /*
     * try {
                                user = determineUser(emailAccess, password);
                               } catch (UserNotFoundException e) {
                                 createLogEntry("!!!!: unauthorized access !!!    parameter: username: " + emailAccess +
                                   " password: " + password + "  method: " + method_decription);
                                 throw new LocalUnauthorizedAccessException(e);
                               } catch (UserFactoryException e) {
                                 createLogEntry("!!!!: unauthorized access !!!    parameter: username: " + emailAccess +
                                   " password: " + password + "  method: " + method_decription);
                                 throw new LocalServiceException("unable to look for a user:", e);
                               }
                               // everything is well - so log the access and continue
                               String msg = "ok..: user : " + user.getLastname() + "(" + user.getEMail() +
                                 ") is accessing the system " + " Admin: " + user.isAdmin() + "  - method:" +
                                 method_decription;
                               createLogEntry(msg);
     */
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param msg TODO
   */
  public static void createLogEntry(String msg) throws LocalServiceException {
    try {
      Logger.log2File(new File(Config.IMPORT_FILENAME_ACCESS), msg);
    } catch (FileNotFoundException e) {
      throw new LocalServiceException("unable to find logfile", e);
    } catch (IOException e) {
      throw new LocalServiceException("unable to find logfile", e);
    }
  }
}
