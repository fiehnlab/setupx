/**
 * ============================================================================ File:    Data.java Package: org.setupx.repository.core.communication.importing.metadata cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.importing.metadata;

import java.util.HashSet;
import java.util.Iterator;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class Data extends MDCore {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private HashSet elements = new HashSet();

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * return an iterator filled with data elements
   *
   * @return an iterator filled with data elements
   */
  public Iterator getDataElements() {
    return this.elements.iterator();
  }

  /**
   * adds an DataElement
   *
   * @param element TODO
   */
  public void add(DataElement element) {
    if (element != null) {
      //debug(this, "found " + element);
      this.elements.add(element);
    }
  }

  /*
   *  (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    StringBuffer buffer = new StringBuffer();
    Iterator iterator = this.elements.iterator();

    while (iterator.hasNext()) {
      DataElement dataElement = (DataElement) iterator.next();
      buffer.append("\n");
      buffer.append(dataElement.toString());
    }

    return buffer.toString();
  }
}
