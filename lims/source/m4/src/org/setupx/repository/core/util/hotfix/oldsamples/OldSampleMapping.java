/**
 * ============================================================================ File:    OldSampleMapping.java Package: org.setupx.repository.core.util.hotfix.oldsamples cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.hotfix.oldsamples;

import org.setupx.repository.core.communication.experimentgeneration.mapping.MetadataMapping;
import org.setupx.repository.core.communication.experimentgeneration.template.ClazzTemplate;
import org.setupx.repository.core.communication.experimentgeneration.template.SampleTemplate;
import org.setupx.repository.core.communication.importing.logfile.ScannedPair;
import org.setupx.repository.server.logging.Message;
import org.setupx.repository.server.persistence.PersistenceActionException;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;
import org.setupx.repository.server.persistence.SXQuery;
import org.setupx.repository.web.forms.inputfield.multi.Sample;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class OldSampleMapping implements MetadataMapping, SampleTemplate {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Hashtable values = new Hashtable();
  private Sample assignedSample;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static Object thiz = new OldSampleMapping();

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @return pathinformation for mapping the xmlfiles
   */
  public final static String[] getPath() {
    return new String[] { ClazzTemplate.LINE, ClazzTemplate.OPERATOR_COMMENT };
    //, ClazzTemplate.SPECIES, ClazzTemplate.OPERATOR_COMMENT , ClazzTemplate.OPERATOR_COMMENT, ClazzTemplate.ORGAN,  
    //, ClazzTemplate.DEV_STAGE, ClazzTemplate.SPECIES, ClazzTemplate.ORGAN_DETAIL, ClazzTemplate.FUNCTION, ClazzTemplate.BACKGOUND };
  }

  /**
   * TODO: 
   *
   * @param sampleID TODO
   *
   * @throws PersistenceActionException TODO
   */
  public static void createScannedPair4Sample(String sampleID)
    throws PersistenceActionException {
    // take the comment 
    String label = new SXQuery().findSampleLabelBySampleID(Long.parseLong(sampleID));
    System.out.println("found sampleID " + sampleID + "  label " + label + " ");

    // creating scanned pair
    if (label.indexOf('_') > 0) {
      // nothin
    } else {
      label = label + "_1";
    }

    new ScannedPair(label, sampleID).update(true);

    Message message = new Message();
    message.setClassName(Sample.class.getName());
    message.setRelatedObjectID(Long.parseLong(sampleID));
    message.setMessage("finished: " + sampleID + " - " + label + " " + sampleID);
    message.update(true);
  }

  /**
   * creates ScannedPairs for old existing samples - so that they can be processed on binbase. the comment field is used as the ACQname <br> experiment:<code> 23670</code> (Arabidopsis thaliana C24XCol 4-days development)<br>
   *
   * @param promtID the experiment number
   *
   * @throws NumberFormatException
   * @throws PersistenceActionFindException
   * @throws PersistenceActionUpdateException
   */
  public static void createScannedPairs(long promtID) throws NumberFormatException, PersistenceActionFindException, PersistenceActionUpdateException {
    List sampleIDs = new SXQuery().findSampleIDsByPromtID(promtID);

    for (Iterator iter = sampleIDs.iterator(); iter.hasNext();) {
      String sampleID = (String) ("" + iter.next());

      try {
        createScannedPair4Sample(sampleID);
      } catch (PersistenceActionException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * TODO: 
   *
   * @param sample TODO
   */
  public void setAssignedSample(Sample sample) {
    this.assignedSample = sample;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public Sample getAssignedSample() {
    return this.assignedSample;
  }

  /**
   * TODO: 
   *
   * @param path TODO
   * @param value TODO
   */
  public void setValue(String path, String value) {
    this.values.put(path, value);
  }

  /**
   * TODO: 
   *
   * @param path TODO
   *
   * @return TODO
   */
  public String getValue(String path) {
    return (String) this.values.get(path);
  }

  /**
   * TODO: 
   *
   * @param classID TODO
   *
   * @throws PersistenceActionException TODO
   */
  public static void createScannedPair4Class(int classID)
    throws PersistenceActionException {
    List list = new SXQuery().findSampleIDsByClazzID(classID);
    Iterator iterator = list.iterator();

    while (iterator.hasNext()) {
      Long element = (Long) iterator.next();
      createScannedPair4Sample("" + element.longValue());
    }
  }

  /*
   *  (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    StringBuffer buffer = new StringBuffer();

    for (Enumeration enumeration = values.keys(); enumeration.hasMoreElements();) {
      String path = (String) enumeration.nextElement();
      String value = this.getValue(path);
      buffer.append(path + ": " + value + "\n");
    }

    return buffer.toString();
  }
}
