package org.setupx.repository.core.communication.technology.platform.converter;

import java.io.File;
import java.util.Iterator;

import org.hibernate.Session;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.file.SampleFile;
import org.setupx.repository.core.communication.importing.logfile.ScannedPair;
import org.setupx.repository.core.communication.technology.platform.Datafile;
import org.setupx.repository.core.communication.technology.platform.TechnoCoreObject;
import org.setupx.repository.core.communication.technology.platform.TechnologyFileType;
import org.setupx.repository.core.util.logging.Logger;

public class Migrator extends CoreObject {
    Session session = CoreObject.createSession();

    public static void main(String[] args) throws Exception {
        new Migrator().migrate();
    }

    private void migrate() throws Exception {

        short[] ids = SampleFile.TYPES;

        for (int aa = 0; aa != ids.length; aa++) {
            short type = ids[aa];
        }

        // techno_migrate.jsp

        Session hqlSession = CoreObject.createSession();

        for (int aa = 0; aa != ids.length; aa++) {
            short type = ids[aa];
            // determine if technology exists
            Session _prSession = TechnoCoreObject.createSession();
            TechnologyFileType technologyFileType = (TechnologyFileType) _prSession.createSQLQuery(
                    "select * from technologyfiletype where pattern like \"%" + SampleFile.getExtension(type) + "\"")
                    .addEntity(TechnologyFileType.class).list().get(0);

            Iterator iterator = hqlSession.createSQLQuery(
                    "select * from scanned_samples where sampleID != \"\" ORDER BY RAND() desc").addEntity(
                    ScannedPair.class).list().iterator();

            int c = 0;
            while (iterator.hasNext()) {
                try {
                    ScannedPair scannedPair = (ScannedPair) iterator.next();
                    Logger.log(this, "id: " + scannedPair.getSampleID());

                    SampleFile sampleFile = new SampleFile(scannedPair.getLabel());

                    // determine if technology datafile exists
                    String q = "select count(uoid) from datafile as d where d.sampleID = " + scannedPair.getSampleID()
                            + " and d.source = \"" + sampleFile.getFile(type) + "\" and d.technologyfiletype = "
                            + technologyFileType.getUOID() + "";
                    // Logger.log(this, q);
                    int number = Integer.parseInt(hqlSession.createSQLQuery(q).list().get(0).toString());
                    // Logger.log(this, ": " + number);

                    if (number < 1) { // .getDatafiles(Long.parseLong(scannedPair.getSampleID())).size() == 0) {

                        // create it
                        File file = sampleFile.getFile(type);

                        Datafile datafile;
                        try {
                            datafile = Datafile.create(Long.parseLong(scannedPair.getSampleID()), file);
                            technologyFileType.addDatafile(datafile);
                            c++;
                            debug(datafile.getSampleID() + " - " + datafile.getSource());
                            Logger.log(this, " " + datafile.getSampleID() + " " + datafile.getSource());
                        } catch (Exception e) {
                            Logger.log(this, e.getMessage());
                        }
                    } else {
                    }
                } catch (Exception e) {

                }
            }
            technologyFileType.getTechnology().updatingMyself(_prSession, true);
            _prSession.close();
        }
    }
}
