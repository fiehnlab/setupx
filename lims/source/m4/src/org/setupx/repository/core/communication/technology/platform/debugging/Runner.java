package org.setupx.repository.core.communication.technology.platform.debugging;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.hibernate.PersistenceConfiguration;

public class Runner {

    public static void main(String[] args) {
        
        A a = new A();
        a.setC(new C());
        
        Set b = new HashSet();
        b.add(new B());
        b.add(new B());
        a.setB(b);
        
        a.save();
        
        long uoid = a.getUOID();
        
        Logger.debug(null, "done - saved object as " + uoid);
        
        Session hibernateSession = PersistenceConfiguration.createSessionFactory().openSession();
        A aLoaded = (A)hibernateSession.createSQLQuery("select * from debug_a where uoid = " + uoid).addEntity(A.class).list().get(0);
        
        aLoaded.addB(new B());
        aLoaded.setC(new C());
        aLoaded.update(hibernateSession, true);
    }
}
