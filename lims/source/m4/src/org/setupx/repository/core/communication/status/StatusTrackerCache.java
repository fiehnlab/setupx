package org.setupx.repository.core.communication.status;

import java.util.Date;

import org.setupx.repository.Config;
import org.setupx.repository.core.communication.BackgroundThread;
import org.setupx.repository.core.communication.notification.NotificationCentral;
import org.setupx.repository.core.communication.notification.SystemNotification;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.core.util.logging.Logger;

public class StatusTrackerCache extends BackgroundThread {

    private long pause;
    public Date lastScanDate;


    public StatusTrackerCache(long statusRefresh) {
        this.pause = statusRefresh;
    }


    public static void init() {
        Logger.info(StatusTrackerCache.class, "creating new " + StatusTrackerCache.class.getName());
        Thread thread = new StatusTrackerCache(Config.STATUS_REFRESH);
        thread.setDaemon(true);
        thread.start();
    }


    /*
     *  (non-Javadoc)
     * @see java.lang.Thread#run();
     */
    public final void run() {
        NotificationCentral.notify(new SystemNotification("starting " + Util.getClassName(this)));

        for (; true;) {
            

            try {
                Logger.log(this, "sleeping for " + pause + ".");
                this.lastScanDate = new Date();
                this.sleep(pause);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Logger.info(this, "refreshing cache now- " + Util.getDateString());

            try {
                findEverything();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            
            Logger.info(this, "stored : " + StatusTracker.findStored().size());
            Logger.info(this, "postpro: " + StatusTracker.findPostProcessing().size());
            Logger.info(this, "prepard: " + StatusTracker.findPrepared().size());
            Logger.info(this, "run    : " + StatusTracker.findRun().size());
            Logger.info(this, "schedul: " + StatusTracker.findScheduled().size());
            Logger.info(this, "shipped: " + StatusTracker.findShipped().size());

        }
    }


    private void findEverything() {

        StatusTracker.findStored();
        StatusTracker.findPostProcessing();
        StatusTracker.findPrepared();
        StatusTracker.findRun();
        StatusTracker.findScheduled();
        StatusTracker.findShipped();
        StatusTracker.findStored();

    }
}
