package org.setupx.repository.core.util.hotfix.oldsamples;

import java.io.File;

import org.setupx.repository.core.util.hotfix.oldsamples.mapping.MappingException;

public class PromtConverterException extends Exception {
    private static final long serialVersionUID = 5989772009629872831L;

    public PromtConverterException(File file, Exception e) {
        super("unable to process " + file.getName(), e);
    }

    public PromtConverterException(String string) {
        super(string);
    }

    public PromtConverterException(String string, MappingException e) {
        super(string, e);
    }
}
