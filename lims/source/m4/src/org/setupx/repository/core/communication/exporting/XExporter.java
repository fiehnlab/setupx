/**
 * ============================================================================ File:    XExporter.java Package: org.setupx.repository.core.communication.exporting cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.exporting;

import org.setupx.repository.core.Connectable;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;

import java.util.Date;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class XExporter extends CoreObject implements Connectable {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  //private Machine machine = null;
  private Acquisition acquisition;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * writing information to a defined format
   */
  public XExporter() {
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /* (non-Javadoc)
   * @see org.setupx.repository.core.Connectable#isAvailable()
   */
  public void checkAvailability() {
    warning(this, "check todo");
  }

  /**
   * creates the AT for a specific machine.
   *
   * @param sampleIDs samples that will be send into the machine
   * @param jobInformation information containing infos about how to run the samples
   *
   * @return a complete acquisition with ...
   *
   * @throws ExportException
   * @throws MappingError
   * @throws PersistenceActionUpdateException
   */
  public Acquisition createAcquisition(long[] sampleIDs, JobInformation jobInformation)
    throws ExportException, MappingError, PersistenceActionUpdateException {
    debug("creating Acquisition for " + sampleIDs.length + " samples.");

    long _startingTime = new Date().getTime();

    acquisition = new Acquisition(sampleIDs, jobInformation);

    long _endTime = new Date().getTime();

    debug("creating an acq with " + sampleIDs.length + " samples took " + (_endTime - _startingTime) + " miliseconds.");

    return acquisition;
  }
}
