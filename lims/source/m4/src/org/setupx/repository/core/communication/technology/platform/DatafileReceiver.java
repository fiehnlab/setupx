package org.setupx.repository.core.communication.technology.platform;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DatafileReceiver {

    /**
     * 
     * @param soureFile location where the file is coming from - if coming from an upload servlet usually a tmp folder
     * @param technologyFileType the filetype the new datafile will be assigned to
     * @param sampleID the sample that the file belongs to
     * @throws DatafileException 
     */
    public static void receive(File sourceFile, TechnologyFileType technologyFileType, long sampleID) throws DatafileException {
        // take the actual file and copy it to the default location of the technologyfiletype
        Location defaultLocation =(Location)technologyFileType.getDefaultLocations().iterator().next();

        try {
            org.setupx.repository.core.util.File.move(new File(defaultLocation.getPath()), sourceFile);
        } catch (Exception e) {
            throw new DatafileException("Can not move datafile " + sourceFile + " ", e);
        }
        
        // create a new datafile using the new copied file and the sampleID
        
        // assign it to the technologyfiletype
        
        // done
    }
}
