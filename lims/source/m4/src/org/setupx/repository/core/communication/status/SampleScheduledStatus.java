package org.setupx.repository.core.communication.status;

import java.util.Collection;
import java.util.List;
import java.util.Vector;

import org.hibernate.Session;

import org.setupx.repository.core.ParameterException;
import org.setupx.repository.core.communication.exporting.AcquisitionParameter;
import org.setupx.repository.core.communication.leco.AcquisitionFileQueryException;
import org.setupx.repository.core.communication.leco.LecoACQFile;

public class SampleScheduledStatus extends DynamicStatus {

    private String scheduledName;

    public SampleScheduledStatus(long sampleUOID, String scheduledName) {
        super(sampleUOID);
        this.scheduledName = scheduledName;
    }

    public String createDisplayString() {
        return "scheduled as " + this.scheduledName + " ";
    }

    public String getMessage() {
        return this.scheduledName;
    }
    
    public static Collection find(final long sampleUOID, Session hqlSession) {
        List result = new Vector();
        String scheduledName;
        try {
            scheduledName = LecoACQFile.determine(sampleUOID + "" , AcquisitionParameter.NAME);
            result.add( new SampleScheduledStatus(sampleUOID, scheduledName));
        } catch (ParameterException e) {
            e.printStackTrace();
        } catch (AcquisitionFileQueryException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } 
        
        
        return result;
    }
}
