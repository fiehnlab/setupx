package org.setupx.repository.core.communication.importing.logfile;

import org.setupx.repository.ConfigurationException;

public class ScannerConfigurationException extends ConfigurationException{

    public ScannerConfigurationException(String extension) {
        super("illegal extension: " + extension + " (should be like \"bin\".)");
    }
}
