/**
 * ============================================================================ File:    DictionaryWebService.java Package: org.setupx.repository.core.util.spellcheck2 cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.spellcheck2;

import org.setupx.repository.core.CoreObject;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 *
 * @deprecated - will not be maintained anymore - functions are take over by google
 */
public class DictionaryWebService extends CoreObject {
  /*
     private DictServiceSoapStub binding;
     private Dictionary[] dictionaries;
     {
       try {
         binding = (DictServiceSoapStub) new DictServiceLocator().getDictServiceSoap();
         binding.setTimeout(10000);
         ArrayOfDictionary directoryList = binding.dictionaryList();
         dictionaries = directoryList.getDictionary();
       } catch (Exception e) {
         err("unable to init the Dictionaries", e);
         dictionaries = new Dictionary[0];
       }
       for (int i = 0; i < dictionaries.length; i++) {
         Dictionary dictionary = dictionaries[i];
         log(this, "init: lib #" + i + ": " + dictionary.getId() + "  " + dictionary.getName() + " ");
       }
     }
     public Definition[] define(String word, String id) {
       Definition[] defs = null;
       try {
         defs = binding.defineInDict(id, word).getDefinitions().getDefinition();
       } catch (RemoteException e) {
         e.printStackTrace();
       }
       return defs;
     }
     public Definition[] define(String word) {
       Definition[] defs = null;
       try {
         defs = binding.define(word).getDefinitions().getDefinition();
       } catch (RemoteException e) {
         e.printStackTrace();
       }
       return defs;
     }
     public Definition[] defineByWordNet(String word) {
       Definition[] defs = null;
       try {
         defs = binding.defineInDict("wn", word).getDefinitions().getDefinition();
       } catch (RemoteException e) {
         e.printStackTrace();
       }
       return defs;
     }
   */
}
