package org.setupx.repository.core.ws.template;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class ExperimentCreateException extends Exception {
  /**
   *
   */
  public ExperimentCreateException() {
    super();

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public ExperimentCreateException(String arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public ExperimentCreateException(Throwable arg0) {
    super(arg0);

    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   */
  public ExperimentCreateException(String arg0, Throwable arg1) {
    super(arg0, arg1);

    // TODO Auto-generated constructor stub
  }
}
