package org.setupx.repository.core.communication.technology.platform;

import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.SXQuery;

public class Dummy extends CoreObject {

    public static void main(String[] args) throws Exception{

        // 
        // new Dummy().testSXQuery();
        
        // sMigrator.main(null);

        // testRemove();
    
        /*for (int i = 0; i < 30; i++){
            testPersistence();
        }*/
        
        // testOthers();
        
        // Runner.main(null);
        
        testSearchFilesTime();
        
    }

    /*
    private static void testOthers() throws Exception{
        Technology technologyA = TechnologyProvider.createTechnology("techA", "my GCMS Platform");
        
        TechnologyFileType fileTypeA1 = new TechnologyFileType("txt-Files", "standard txt files", Location.createLocation("/test/sx/technologyA/location1"), "(.*).txt");
        technologyA.addTechnologyFileType(fileTypeA1);

        TechnologyFileType fileTypeA2 = new TechnologyFileType("peg-Files", "pegasus fiels", Location.createLocation("/test/sx/technologyA/location2"), "(.*).txt");
        technologyA.addTechnologyFileType(fileTypeA2);
        
        Logger.debug(null, "------------START--- -----------");
        technologyA.update(true);
        Logger.debug(null, "------------END--------------");

        Technology technologyB = TechnologyProvider.createTechnology("techB", "my LCMS Platform");
        
        TechnologyFileType fileTypeB1 = new TechnologyFileType("txt-files", "stardard", Location.createLocation("/test/sx/technologyB/location1"), "(.*).txt");
        technologyB.addTechnologyFileType(fileTypeB1);

        TechnologyFileType fileTypeB2 = new TechnologyFileType("special-files", "speical files", Location.createLocation("/test/sx/technologyB/location2"), "(.*).txt");
        technologyB.addTechnologyFileType(fileTypeB2);

        Logger.debug(null, "------------START--------------");
        technologyB.update(true);
        Logger.debug(null, "------------END--------------");
        
        
        
        long sampleID = 123;

        
        Datafile datafile = new Datafile(123, new File("/test/ab.txt"));
        technologyA.getTechnologyFileType("peg-Files").addDatafile(datafile);

        
        // show assigned datafiles
        debug(null, "---- assigned samples ----");
        Iterator iterator = TechnologyProvider.getTechnologies().iterator();
        
        while (iterator.hasNext()) {
            Technology technology = (Technology) iterator.next();
            
            debug(null, "datafiles for " + technology.getLabel() + ": ");
            Iterator iterator2 = technology.getTechnologyFileTypes().iterator();
            
            while (iterator2.hasNext()) {
                TechnologyFileType technologyFileType = (TechnologyFileType) iterator2.next();
                Iterator iterator4 = technologyFileType.getDatafiles().iterator();
                while(iterator4.hasNext()){
                    datafile = (Datafile)iterator4.next();
                    debug(null,"datafile: " + datafile);
                }
            }
        }
        
        // show possible matches
        debug(null, "---- possible samples ----");
        iterator = TechnologyProvider.getTechnologies().iterator();
        
        while (iterator.hasNext()) {
            Technology technology = (Technology) iterator.next();
            
            debug(null, "possible datafiles for " + technology.getLabel() + ": ");
            Iterator iterator2 = technology.getPossibleFilesForSample(sampleID).iterator();
            while(iterator2.hasNext()){
                datafile = (Datafile)iterator2.next();
                debug(null," possible match:: " + datafile);
            }
        }
        List datafiles = TechnologyProvider.determineDatafiles(sampleID);   
    }
    */

    private void testSXQuery() {
        
        
        Iterator iterator = new SXQuery().findPromtIDs().iterator();

        while (iterator.hasNext()){
            long promtID = Long.parseLong("" + iterator.next());
            debug("" + new SXQuery().determineNumberOfSamples(promtID));
        }
    }

        
        
    

    private static void testSearchFiles() throws PersistenceActionFindException {
        Iterator iterator = TechnologyProvider.getPossibleDatafiles(123).iterator();
        while (iterator.hasNext()) {
            Datafile datafile  = (Datafile) iterator.next();
            Logger.log(null, "" + datafile);
        }
    }
    

    private static void testSearchFilesTime() throws PersistenceActionFindException {
        java.util.Date end = GregorianCalendar.getInstance().getTime();
        java.util.Date start = end;
        start.setMonth(10);

        Iterator iterator = TechnologyProvider.getPossibleDatafiles(start,end).iterator();
        while (iterator.hasNext()) {
            Datafile datafile  = (Datafile) iterator.next();
            Logger.log(null, "" + datafile);
        }
    }

    private static void testPersistence() throws Exception{
        Technology technologyA = TechnologyProvider.createTechnology("techA", "my GCMS Platform");
        technologyA.addTechnologyFileType(new TechnologyFileType("a","b",new Location("/mnt"),"STAR"));
        
        technologyA.updatingMyself(TechnoCoreObject.createSession(), true);
        
        long technoID = technologyA.getUOID();
        
        // ---------

        Session s = Technology.createSession();
        Session hqlSession = createSession();
        Transaction transaction = hqlSession.beginTransaction();
        Logger.debug(null, "-transaction started-");

        Technology technologyC = (Technology )s.createSQLQuery("select * from technology where uoid = " + technoID).addEntity(Technology.class).list().get(0);

        TechnologyFileType fileType = new TechnologyFileType("ccc","ccc",new Location("/mnt"),"pattern");
        
        technologyC.addTechnologyFileType(fileType);
        
        technologyC.updatingMyself(s, true);

        
        java.util.Date end = GregorianCalendar.getInstance().getTime();
        java.util.Date start = end;
        start.setMonth(10);
        
        List files = TechnologyProvider.getPossibleDatafiles(start, end);
        
        
        
        transaction.commit();
        Logger.debug(null, "-transaction committed-");
        hqlSession.close();
        Logger.debug(null, "----- finished update ---------");
        

    }

    /*
    private static void testRemove() throws PersistenceActionException {
        Technology technology = new Technology("asdfasdf","asdfasdf");
        technology.update(true);
        long id = technology.getUOID();
        
        technology = null;
        
        CoreObject.remove(Technology.class, id);    
    }
     */
}