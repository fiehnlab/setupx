/**
 * ============================================================================ File:    NCICB_Result.java Package: org.setupx.repository.core.communication.ncbi cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.ncbi;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.xml.XPath;

import org.w3c.dom.Document;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class NCICB_Result extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private String id;
  private String name;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * @param doc
   *
   * @throws NCICBException
   */
  public NCICB_Result(Document doc) throws NCICBException {
    try {
      this.name = XPath.pathValue(doc, "/nci-core/gov.nih.nci.caBIO.bean.Organ/name");
      this.id = XPath.pathValue(doc, "/nci-core/gov.nih.nci.caBIO.bean.Organ/id");
    } catch (Exception e) {
      warning(e);
      log(doc);
      throw new NCICBException("unable to create result", e);
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param id TODO
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getId() {
    return id;
  }

  /**
   * TODO: 
   *
   * @param name TODO
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String getName() {
    return name;
  }
}
