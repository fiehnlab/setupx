package org.setupx.repository.core.communication.technology.platform;

import java.io.File;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.technology.platform.converter.Converter;
import org.setupx.repository.server.persistence.SXQuery;


public class Dummy2 {

    public static void main(String[] args) throws Exception {

        // runConverter();
        
        // test upload of Datafiles
        //uploadDatafiles();
        
        
        
    }

    private static void uploadDatafiles() throws DatafileException {
        // data is 
        File soureFile = new File("/mnt/binbase/data/222222asdf23.gif");
        TechnologyFileType technologyFileType = null;
        long sampleID = 82643;
        
        DatafileReceiver.receive(soureFile, technologyFileType, sampleID);
    }

    private static void runConverter() {
        // convert existing ScannedSampels into Datafiles
        Converter converter = new Converter();
        converter.run();
    }
}
