/**
 * ============================================================================ File:    Util.java Package: org.setupx.repository.core.util cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $ ============================================================================
 * Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.poi.hssf.usermodel.HSSFCell;

import org.setupx.repository.Config;
import org.setupx.repository.ConfigurationException;
import org.setupx.repository.core.user.UnvalidEmailAddressException;
import org.setupx.repository.core.util.logging.Logger;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp; </a>
 * @version $Revision: 1.15 $
 */
public class Util { //extends CoreObject {

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final Object thiZ = new Util();

  /** max differnece between two strings when compared */
  public static final int MAXDIFF = 1;
  private static final double dateMin = new Date(110, 1, 1).getTime();
  private static final double dateMax = new Date(80, 1, 1).getTime();
  private static Calendar calendar = Calendar.getInstance();

  static {
    initXMLParser();
  }

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Util() {
    super();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param cell TODO
   *
   * @return TODO
   */
  public final static String getCellValue(HSSFCell cell) {
    String value = "";

    try {
      value = cell.getDateCellValue().toLocaleString();
    } catch (Exception e) {
      try {
        double valueD = cell.getNumericCellValue();

        if ((valueD > dateMin) && (valueD < dateMax)) {
          // date value
          Logger.log(thiZ, "making it a date:" + valueD + " cause bigger than " + dateMin + " and smaller than " + dateMax);

          return new Date(Long.parseLong("" + valueD)).toLocaleString();
        }
      } catch (Exception e1) {
        try {
          value = cell.getStringCellValue();
        } catch (Exception e2) {
          //log(null, "unable to find value in cell" ); 
        }
      }
    }

    return value;
  }

  /**
   * @return just the class name -- no package info.
   */
  public static final String getClassName(Object o) {
    String classString = o.getClass().getName();

    return classString(classString);
  }

  /**
   * creates a string (RED, BLUE, ..) based on an int
   *
   * @param i
   *
   * @return
   */
  public static final String getColor(int i) {
    String[] colors = new String[] { "#808387", "#05509d", "#4e6276", "#0b335c", "#376696", "#484c96", "#17880c", "#b4d2b1", "#4ba442", "#797808", "#cbca9f", "#686733", "#791131", "#c18b9b", "#c1476c" };

    //" "BLACK", "#18518C", "#31496B", "#84B6E7", "#31496B", "#BEE85E" }; //"#84B6E7", "#BEE85E",  
    int pos = i % colors.length;

    return colors[pos];
  }

  /**
   * Find out which XML parser to use.
   */
  public static final void initXMLParser() {
    System.out.println("initXMLParser");

    String propertyName1 = "org.xml.sax.parser";

    if (xmlParserFoundAndSet(propertyName1, "-none")) {
      // none
    } else if (xmlParserFoundAndSet(propertyName1, "com.jclark.xml.sax.Driver")) {
      // Crimson
    } else if (xmlParserFoundAndSet(propertyName1, "org.apache.xerces.parsers.SAXParser")) {
      // Xerces / XML4J
    } else if (xmlParserFoundAndSet(propertyName1, "org.apache.crimson.jaxp.SAXParserImpl")) {
      // James Clark XP
    } else if (xmlParserFoundAndSet(propertyName1, "com.microstar.xml.SAXDriver")) {
      // Aelfred
    } else {
      throw new RuntimeException("ERROR: No valid " + propertyName1 + " found!");
    }
  }

  /**
   * @return a String reprensentation of the current Date
   */
  public static String getDate() {
    calendar = Calendar.getInstance(Locale.US);

    return calendar.get(Calendar.MONTH) + " " + calendar.get(Calendar.DAY_OF_MONTH) + ", " + calendar.get(Calendar.YEAR);
    //return calendar.get(Calendar.DAY_OF_MONTH) + "." + calendar.get(Calendar.MONTH) + "." + calendar.get(Calendar.YEAR);
  }

  /**
   * @return
   *
   * @deprecated not precise enough
   */
  public static String getDateString() {
    return new Date().toLocaleString();
  }

  /**
   * get the package name of an object
   */
  public static String getPackageName(Object object) {
    String classname = getClassName(object);
    int pos = classname.lastIndexOf('.');

    return classname.substring(0, classname.length());
  }

  /**
   * Returns a similarity value of the two names. The range of is from 0 to 256. no similarity is negative
   *
   * @return true it names are equal
   */
  public static boolean isSimilarName(String name1, String name2) {
    return getSimilarity(name1, name2) >= 0;
  }

  /**
   * Returns a similarity value of the two names. The range of is from 0 to 256. no similarity is negative.
   *
   * @return 0 to 256
   *
   * @see LevenshteinDistanceMetric
   * @see Util#MAXDIFF
   */
  public static int getSimilarity(String name1, String name2) {
    // the sizedifferenze between these two can not be more then diff !!!!
    int diff = name1.length() - name2.length();

    if ((diff > MAXDIFF) || (diff < -MAXDIFF)) {
      return 256;
    }

    int result1 = LevenshteinDistanceMetric.distance(name1, name2);

    //double result2 = 256 - (EuclidDistance.compare(name1,name2,2)*256);
    //if (result1 < 3) Logger.log(thiZ, "Levenshtein:   " + result1 + " \t Euclid: " + result2 + "  " + name1 + "  " + name2);
    return result1;
  }

  /**
   * @return a String reprensentation of the current Time incuding hour and minute.
   */
  public static String getTime() {
    //calendar = Calendar.getInstance();
    // format it to 14:20
    //return DateFormat.getTimeInstance().format(new Date());
    return calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE);
  }

  /**
   * TODO: 
   *
   * @param seperator TODO
   * @param completeString TODO
   * @param oldStringBeginn TODO
   * @param newString TODO
   *
   * @return TODO
   *
   * @throws UtilException TODO
   */
  public static String addFirst(String seperator, String completeString, String oldStringBeginn, String newString)
    throws UtilException {
    StringTokenizer tokenizer = new StringTokenizer(completeString);
    boolean loop = true;

    Logger.debug(thiZ, "starting with a lenght of: " + completeString.length());
    Logger.debug(thiZ, "looking for " + oldStringBeginn + " seperator: _" + seperator + "_");

    while (loop && tokenizer.hasMoreTokens()) {
      String token = tokenizer.nextToken(seperator);

      Logger.debug(thiZ, token);

      if (token.startsWith(oldStringBeginn)) {
        completeString = completeString.replaceFirst(token, newString + token);

        Logger.debug(thiZ, "found it !!!!" + token);
        loop = false;
      }
    }

    // if not found throw ex
    if (loop) {
      throw new UtilException(oldStringBeginn + " was not found in " + completeString);
    }

    Logger.debug(thiZ, "finished with a lenght of: " + completeString.length());

    return completeString;
  }

  /**
   * @param file_messages
   *
   * @throws ConfigurationException
   */
  public static void checkFile(String fileName) throws ConfigurationException {
    java.io.File file = new java.io.File(fileName);
    Logger.debug(thiZ, "checking file: " + fileName);

    if (!file.exists()) {
      throw new ConfigurationException("file " + fileName + " does not exist");
    }
  }

  /**
   * checking a url. if the content at the url is existing everything is fine - if not exception is thrown
   *
   * @param url
   *
   * @throws ConnectException if it was not possible to connect
   */
  public static void checkURL(URL url) throws ConnectException {
    Logger.debug(thiZ, "checkURL(" + url + ")");

    // TODO add timeout - is suported in 1.5 not 1.4 - grrrrr
    if (org.setupx.repository.Config.URLCHECK_ACTIVE) {
      try {
        URLConnection connection = url.openConnection();

        connection.connect();

        String contentType = connection.getContentType();
        Logger.debug(thiZ, "  got a connection to " + url.toString() + " - the content " + contentType); //+ " recieved is: " + connection.getContent().toString());
      } catch (IOException e) {
        throw new ConnectException(e);
      }
    }
  }

  /**
   * checks the url for an taminoconnection
   *
   * @param url
   *
   * @throws MalformedURLException
   * @throws MalformedURLException
   * @throws ConnectException
   * @throws IOException
   */
  public static void checkURLtaminoPing(URL url) throws MalformedURLException, ConnectException {
    if (Config.URLCHECK_ACTIVE) {
      URL taminoURL = new URL(url.toString() + "/?_diagnose=ping");
      URLConnection taminoConnection;

      try {
        taminoConnection = taminoURL.openConnection();
        taminoConnection.connect();
        Logger.debug(thiZ, "got connection to " + taminoURL.toString());
        Logger.debug(thiZ, "recieved is: " + taminoConnection.getContentType());
      } catch (IOException e) {
        throw new ConnectException("There is a problem connecting to " + url.toString() + "  - maybe the database was not started.");
      }
    }
  }

  /**
   * @param classString
   *
   * @return
   */
  public static String classString(String classString) {
    int dotIndex = classString.lastIndexOf(".");

    return classString.substring(dotIndex + 1);
  }

  /**
   * converts the values inside a vector into an hashset
   *
   * @param values2 vector containing the values
   *
   * @return a new hashset containing all values from the vector
   */
  public static HashSet convert2Hashset(Vector values2) {
    HashSet set = new HashSet();

    for (int i = 0; i < values2.size(); i++) {
      set.add(values2.get(i));
    }

    return set;
  }

  /**
   * converts a vector to an string array
   * <pre>the vector can just contain strings</pre>
   *
   * @param idsVec the vector that will be converted
   *
   * @return an array of the values in the vector
   */
  public static String[] convert2StringArr(Vector idsVec) {
    String[] result = new String[idsVec.size()];

    for (int i = 0; i < idsVec.size(); i++) {
      String id = (String) idsVec.get(i);
      result[i] = id;
    }

    return result;
  }

  /**
   * @param file
   *
   * @return
   */
  public static String[] convert2StringArr(java.io.File file)
    throws IOException {
    String testWordsFile = File.getFileContent(file);

    StringTokenizer stringTokenizer = new StringTokenizer(testWordsFile, "\n");

    Vector vector = new Vector();

    while (stringTokenizer.hasMoreElements()) {
      String element = (String) stringTokenizer.nextElement();
      vector.add(element);
    }

    return org.setupx.repository.core.util.Util.convert2StringArr(vector);
  }

  /**
   * converts an array of strings to a Vector filled with the values
   *
   * @param values the sring array
   *
   * @return the new vector containing all
   */
  public static Vector convert2vec(String[] values) {
    Vector vector = new Vector();

    for (int i = 0; i < values.length; i++) {
      vector.add(values[i]);
    }

    return vector;
  }

  /**
   * converts a hashtable in a String
   * 
   * <p>
   * <code>key: TAB value</code>
   * </p>
   * 
   * <p>
   * <code>key: TAB value</code>
   * </p>
   *
   * @param hashtable
   * @param i
   *
   * @return
   */
  public static Object convertString(Hashtable hashtable, int i) {
    String dummyString = "                               ";
    StringBuffer stringBuffer = new StringBuffer();
    Enumeration enumeration = hashtable.keys();

    while (enumeration.hasMoreElements()) {
      String key = (String) enumeration.nextElement();
      stringBuffer.append((key + dummyString).substring(0, i) + ":\t" + hashtable.get(key) + "\n");
    }

    return stringBuffer.toString();
  }

  /**
   * creates a new URL and checks the URL
   *
   * @param string
   *
   * @return
   *
   * @throws ConfigurationException
   * @throws MalformedURLException
   * @throws MalformedURLException
   * @throws
   * @throws ConnectException
   * @throws ConfigurationException
   */
  public static URL createURL(String string) throws ConfigurationException {
    URL url = null;

    try {
      // creating the url
      url = new URL(string);

      // checking if the databases exist and if they are running ...
      Util.checkURL(url);
    } catch (MalformedURLException e) {
      throw new ConfigurationException("url (" + string + " ) is not available");
    } catch (ConnectException e) {
      throw new ConfigurationException("unable to connect to " + string + " ", e);
    }

    return url;
  }

  /**
   * expands an array
   *
   * @param values TODO
   * @param string_other TODO
   *
   * @return TODO
   */
  public static final String[] expand(String[] values, String string_other) {
    String[] result = new String[values.length + 1];

    // copy the values
    for (int i = 0; i < values.length; i++) {
      result[i] = values[i];
    }

    result[values.length] = string_other;

    return result;
  }

  /**
   * TODO: 
   *
   * @param url TODO
   *
   * @throws Exception TODO
   */
  public static void get(URL url) throws Exception {
    URLConnection con = url.openConnection();
    BufferedInputStream in = new BufferedInputStream(con.getInputStream());
    FileOutputStream out = new FileOutputStream("C:\\file.zip");

    int i = 0;
    byte[] bytesIn = new byte[1024];

    while ((i = in.read(bytesIn)) >= 0) {
      out.write(bytesIn, 0, i);
    }

    out.close();
    in.close();
  }

  /**
   * @return
   */
  public static long potenz(int b, int e) { // berechne b hoch e

    // if ( e < 0 ) { b = 1/b; e = -e; }
    if (e == 0) {
      return 1L;
    }

    if (e == 1) {
      return (long) b;
    }

    return b * potenz(b, e - 1);
  }

  /**
   * removes the envolope that is mounted around an XQuery result ... <code>xq:object</code>
   *
   * @param string
   *
   * @return
   */
  public static String removeXqEnvelope(String string) {
    // remove opentag
    String startingTag = "<xq:object>";
    String endTag = "</xq:object>";

    int firstPos = string.indexOf(startingTag);

    if (firstPos >= 0) {
      string = string.substring(firstPos + startingTag.length(), string.length());
    }

    firstPos = string.indexOf(endTag);

    if (firstPos >= 0) {
      string = string.substring(0, firstPos);
    }

    return string;
  }

  /**
   * @param source text containing the searchstring
   * @param searchstring string to be looked for
   * @param replacestring replacemnet
   *
   * @return new string
   */
  public static String replace(String source, char search, String replacestring) {
    return replace(source, "" + search, replacestring);
  }

  /**
   * @param source text containing the searchstring
   * @param searchstring string to be looked for
   * @param replacestring replacemnet
   *
   * @return new string
   */
  public static String replace(String source, String searchstring, String replacestring) {
    int start = source.indexOf(searchstring);

    if (start == -1) {
      return source;
    }

    String firstpart = source.substring(0, start);
    String endpart = source.substring(start + searchstring.length(), source.length());

    String result = firstpart + replacestring + endpart;

    if (result.indexOf(searchstring) > 0) {
      result = replace(result, searchstring, replacestring);
    }

    return result;
  }

  /**
   * TODO: 
   *
   * @param completeString TODO
   * @param oldStringBeginn TODO
   * @param newString TODO
   *
   * @return TODO
   *
   * @throws UtilException TODO
   */
  public static String replaceFirst(String seperator, String completeString, String oldStringBeginn, String newString)
    throws UtilException {
    StringTokenizer tokenizer = new StringTokenizer(completeString);
    boolean loop = true;

    Logger.debug(thiZ, "starting with a lenght of: " + completeString.length());
    Logger.debug(thiZ, "looking for " + oldStringBeginn + " seperator: _" + seperator + "_");

    while (loop && tokenizer.hasMoreTokens()) {
      String token = tokenizer.nextToken(seperator);

      Logger.debug(thiZ, token);

      if (token.startsWith(oldStringBeginn)) {
        completeString = completeString.replaceFirst(token, newString);

        Logger.debug(thiZ, "found it !!!!" + token);
        loop = false;
      }
    }

    // if not found throw ex
    if (loop) {
      throw new UtilException(oldStringBeginn + " was not found in " + completeString);
    }

    Logger.debug(thiZ, "finished with a lenght of: " + completeString.length());

    return completeString;
  }

  /**
   * compares two strings.
   *
   * @param name1 string to compare
   * @param name2 string to compare
   *
   * @return integer 0 to 256 - low is not very similar and 256 ist very similar
   */
  public static int stringCompare(String name1, String name2) {
    /**
     * IBM implementaion
     */
    if (name1.length() > name2.length()) {
      String tmp = name1;
      name1 = name2;
      name2 = tmp;
    }

    int name1len = name1.length();
    int name2len = name2.length();

    int nMatched = 0;

    int i = 0;

    while ((i < name1len) && isSimilarChar(name1.charAt(i), name2.charAt(i))) {
      i++;
      nMatched++;
    }

    int k = name1len;
    int diff = name2len - name1len;

    while ((k > i) && isSimilarChar(name1.charAt(k - 1), name2.charAt((k + diff) - 1))) {
      k--;
      nMatched++;
    }

    if (nMatched == name2len) {
      return 200;
    }

    if ((name2len - nMatched) > nMatched) {
      return -1;
    }

    int tolerance = (name2len / 4) + 1;

    return ((tolerance - (k - i)) * 256) / tolerance;
  }

  /**
   * @param emailAddress2
   *
   * @throws UnvalidEmailAddressException if the emailaddress doesnt contain an at sign and is less that 6 characters long
   */
  public static void validateEmail(String emailAddress2)
    throws UnvalidEmailAddressException {
      try {
          if (emailAddress2.lastIndexOf("@") < 0) {
              throw new UnvalidEmailAddressException("email " + emailAddress2 + " doesnt containg an at sign");
            }

            if (emailAddress2.length() < 6) {
              throw new UnvalidEmailAddressException("email " + emailAddress2 + " is to short");
            }
      } catch (Exception e) {
          throw new UnvalidEmailAddressException("invalid email " + emailAddress2 + " ");
    }
  }

  /**
   * TODO: 
   *
   * @param url TODO
   *
   * @throws ConnectException TODO
   */
  public static void wget(URL url) throws ConnectException {
    try {
      URLConnection connection = url.openConnection();

      connection.connect();

      String contentType = connection.getContentType();
      Logger.debug(thiZ, "  got a connection to " + url.toString() + " - the content " + contentType); //+ " recieved is: " + connection.getContent().toString());

      InputStream in = connection.getInputStream();
      BufferedReader dis = new BufferedReader(new InputStreamReader(in));
      StringBuffer fBuf = new StringBuffer();
      String line;

      while ((line = dis.readLine()) != null) {
        fBuf.append(line + "\n");
      }

      in.close();

      org.setupx.repository.core.util.File.storeData2File(new java.io.File("C:\\test"), fBuf.toString());
    } catch (IOException e) {
      throw new ConnectException(e);
    }
  }

  /**
   * compares two characters ignoring if the upper- or lowercase
   *
   * @param ch1 character to compare
   * @param ch2 character to compare
   *
   * @return true if equal
   */
  private static boolean isSimilarChar(char ch1, char ch2) {
    return Character.toLowerCase(ch1) == Character.toLowerCase(ch2);
  }

  /**
   * Code to specify the appropriate parser.
   */
  private static boolean xmlParserFoundAndSet(final String propertyName, String qualifiedName) {
      Logger.debug(thiZ, "checking for " + qualifiedName);
    try {
      Class.forName(qualifiedName, false, Thread.currentThread().getContextClassLoader());
    } catch (ClassNotFoundException cnfe) {
      Logger.log(thiZ, "not found.");
      return false;
    }

    System.setProperty(propertyName, qualifiedName);
    Logger.debug(thiZ, "Using " + propertyName + " : " + qualifiedName);

    return true;
  }

 
  /**
   * removes what ever is located between "<" and ">"
   * @param content string containing tags
   * @return the clean string without the content and brackets
   */
  public static String removeHTMLtags(String content) {
      boolean loop = true;
      String stringStart = "";
      String stringEnd = "";
      int posOpen ;
      int posClose;
      
      while (loop) {
          // check if < is in there
          posOpen = content.indexOf('<');
          if (posOpen >= 0){
              Logger.log(Util.class, "current: " + content);
              
              // there is some stuff in here
              // find the ending
              posClose = content.indexOf('>');
              
              // cut everything out that is inside the brackets
              stringStart = content.substring(0, posOpen);
              stringEnd = content.substring(posClose + 1, content.length());
              
              content = stringStart.concat(stringEnd);
              Logger.log(Util.class, "updated: " + content);
          } else {
              loop = false;
          }
      }
      return content;
}}
