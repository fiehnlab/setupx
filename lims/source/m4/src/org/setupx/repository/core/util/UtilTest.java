package org.setupx.repository.core.util;

import junit.framework.TestCase;
import org.setupx.repository.Config;
import org.setupx.repository.core.user.UnvalidEmailAddressException;

public class UtilTest extends TestCase {

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testGetColor() {
        if (Util.getColor(12).indexOf('#') < 0){
            fail("no valid html color");
        }
    }

    public void testGetDate() {
        if (Util.getDate().length() < 5) fail("no valid date");
    }

    public void testGetPackageName() {
        if (Util.getPackageName(new Config()).length() < 5) fail("no valid packagename");
    }

    public void testGetTime() {
        fail("Not yet implemented");
    }

    public void testGet() {
        fail("Not yet implemented");
    }

    public void testPotenz() {
        if (Util.potenz(2, 2) != 4) fail(); 
    }

    public void testReplaceStringCharString() {
        String source = "abcdefghd";
        String target = "abczefghz";
        if (target.compareTo(Util.replace("d", 'z', source)) != 0) fail();
    }

    public void testReplaceStringStringString() {
        String source = "abcdefghd";
        String target = "abczefghz";
        if (target.compareTo(Util.replace("d", "z", source)) != 0) fail();
    }

    public void testReplaceFirst() {

    }

    public void testStringCompare() {
        fail("Not yet implemented");
    }

    public void testValidateEmail1() throws UnvalidEmailAddressException {
        Util.validateEmail("mscholz@ucdavis.edu");
    }

    public void testValidateEmail2() {
        try {
            Util.validateEmail("mscholz(at)ucdavis.edu");
            fail("incorrect email accepted");
        } catch (UnvalidEmailAddressException e) {
        }
    }

    public void testWget() {
        fail("Not yet implemented");
    }

    public void testRemoveHTMLtags() {
        fail("Not yet implemented");
    }

}
