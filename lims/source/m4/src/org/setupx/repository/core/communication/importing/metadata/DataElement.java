/**
 * ============================================================================ File:    DataElement.java Package: org.setupx.repository.core.communication.importing.metadata cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.importing.metadata;

import org.apache.poi.hssf.usermodel.HSSFSheet;

import java.io.File;

import java.util.HashSet;
import java.util.Iterator;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public final class DataElement extends MDCore {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private File sourcefile = null;
  private HashSet datasets = new HashSet();

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public Iterator getRowDatas() {
    return this.datasets.iterator();
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public File getSourcefile() {
    return sourcefile;
  }

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String toString() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("sourcefile: " + this.sourcefile.getName());

    Iterator iterator = this.datasets.iterator();

    while (iterator.hasNext()) {
      RowData rowData = (RowData) iterator.next();
      buffer.append("\n" + rowData.toString());
    }

    return buffer.toString();
  }

  /**
   * TODO: 
   *
   * @param sourcefile TODO
   */
  void setSourcefile(File sourcefile) {
    this.sourcefile = sourcefile;
  }

  /**
   * TODO: 
   *
   * @param sheet TODO
   * @param validRow TODO
   * @param sheetlabel TODO
   */
  void add(HSSFSheet sheet, int validRow, String sheetlabel) {
    RowData dataset = new RowData(sheet, validRow);
    dataset.setSheetName(sheetlabel);
    this.add(dataset);
  }

  /**
   * TODO: 
   *
   * @param dataset TODO
   */
  void add(RowData dataset) {
    this.datasets.add(dataset);
  }
}
