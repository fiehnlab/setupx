/**
 * ============================================================================ File:    Billing.java Package: org.setupx.repository.core.communication.billing cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.billing;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.web.forms.inputfield.multi.Promt;


/**
 * Billinginformation for an Experiment
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.4 $
 */
public class Billing extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private Promt promt;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final int SAMPLE = 1;
  public static final double SAMPLE_PRICE_USD = 49.99;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new Billing object.
   *
   * @param promt the promt used to calculate the prices
   */
  public Billing(Promt promt) {
    debug("creating new Billing");
    this.promt = promt;
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * calculates the total price of the experiment
   *
   * @return totalprice of the experiment
   */
  public Price getTotalPrice() {
    int sum = promt.numberSamples();
    Price price = new Price(sum, SAMPLE);

    return price;
  }
}
