/**
 * ============================================================================ File:    SampleTemplate.java Package: org.setupx.repository.core.communication.experimentgeneration.template cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.experimentgeneration.template;

import org.setupx.repository.core.util.hotfix.fsa.PotatoImporter;
import org.setupx.repository.web.forms.inputfield.multi.Sample;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public interface SampleTemplate extends Template {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final String OPERATOR_COMMENT = "/sample/trayname_generic/Operator_comments";
  public static final String SAMPLENAME = PotatoImporter.xpath_expression_sampleID;

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param sampleID TODO
   */
  public void setAssignedSample(Sample sample);

  /**
   * TODO: 
   *
   * @return TODO
   */
  public Sample getAssignedSample();

  /**
   * TODO: 
   *
   * @param path TODO
   * @param value TODO
   */
  public void setValue(String path, String value);

  /**
   * TODO: 
   *
   * @param path TODO
   *
   * @return TODO
   */
  public String getValue(String path);
}
