/**
 * ============================================================================ File:    ExcelParser.java Package: org.setupx.repository.core.util.parse.xls cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.parse.xls;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.parse.ParsingException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.6 $
 */
public class ExcelParser extends CoreObject {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ParsingResult result = null;
  private File file;
  private HSSFSheet[] sheets;

  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final int MAX = 2000;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ExcelParser object.
   *
   * @param file 
   *
   * @throws ParsingException 
   */
  public ExcelParser(File file) throws ParsingException {
    this.file = file;
    parse();
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param args TODO
   *
   * @throws ParsingException TODO
   */
  public static void main(String[] args) throws ParsingException {
    new ExcelParser(new File("share" + File.separator + "test.xls"));
  }

  /**
   * initalizes the array of sheets by reading them from the related file
   */
  private void determineSheets() {
    POIFSFileSystem fs = null;
    HSSFWorkbook wb = null;

    try {
      debug("reading " + file.getName());
      fs = new POIFSFileSystem(new FileInputStream(file));
      wb = new HSSFWorkbook(fs);
    } catch (Exception e) {
      warning(this, "problems with " + file.getName());
      e.printStackTrace();
    }

    int numberOfSheets = wb.getNumberOfSheets();
    this.sheets = new HSSFSheet[numberOfSheets];

    for (int i = 0; i < numberOfSheets; i++) {
      this.sheets[i] = wb.getSheetAt(i);
    }
  }

  /**
   * @throws ParsingException
   */
  private void findDatasets() throws ParsingException {
    debug(">> findDatasets()");

    // take the other dimension - cause ids are in one dimension
    int myAxis = 0;

    // check x or y axis
    if (result.ids.getAxis() == Dataset.COL) {
      myAxis = Dataset.ROW;
    } else if (result.ids.getAxis() == Dataset.ROW) {
      myAxis = Dataset.COL;
    } else {
      throw new ParsingException("dimension (axis) in Dataset is not set");
    }

    // take sheet for parsing
    HSSFSheet mySheet = this.sheets[result.sheetnumber];

    debug("looking for datasets in sheet " + result.sheetnumber + "/" + sheets.length);
    result.datasets = Dataset.filterDatasets(mySheet, myAxis);

    // check if the first dataset ist the set with the labels
    Dataset indexDS = (Dataset) result.datasets.get(0);
    String[] labels = indexDS.data;

    // take every dataset and add labels
    this.result.updateLabels(labels);

    debug("<< findDatasets()");
  }

  /**
   * find the ids
   * 
   * <p>
   * by finding ids it is possible to say it values are in x or in y axis
   * </p>
   */
  private void findID_Dataset() {
    // read all sheets
    determineSheets();

    // check each sheet for a dataset
    boolean loop = true;
    int i = 0;

    while (loop && (this.sheets.length != i)) {
      debug("working on sheet: " + i);
      findID_Dataset(i);
      i++;
    }
  }

  /**
   * TODO: 
   *
   * @param sheetnumber TODO
   */
  private void findID_Dataset(int sheetnumber) {
    debug("start findID_Dataset() in sheet: " + sheetnumber);

    // parsing this sheet 
    HSSFSheet sheet = this.sheets[sheetnumber];

    int maxValue = sheet.getLastRowNum();

    for (int i = 0; i < maxValue; i++) {
      Dataset dsX = null;
      Dataset dsY = null;

      try {
        dsX = new Dataset(sheet, i, Dataset.ROW);
        dsY = new Dataset(sheet, i, Dataset.COL);
      } catch (ParsingException e) {
        warning(this, e.getLocalizedMessage());
      }

      log(i + "X:" + dsX);
      log(i + "Y:" + dsY);

      // setting the ids
      Dataset tmp = null;

      if (dsX.isIncreasingIDs()) {
        tmp = dsX;
      } else if (dsY.isIncreasingIDs()) {
        tmp = dsY;
      }

      try {
        int oldvalue = result.ids.similariry();
      } catch (NullPointerException e) {
        result.ids = tmp;
      }

      // in case tmp was set
      if (tmp != null) {
        if ((tmp.similariry() > result.ids.similariry()) && tmp.isIncreasingIDs()) {
          result.ids = tmp;
          result.sheetnumber = sheetnumber;
        }
      }
    }

    try {
      debug("finished findID_Dataset() - result: firstElement of ids:" + result.ids.data[0] + result.ids.key[0]); //Y: " + dsY.similariry() + "
    } catch (Exception e) {
      debug("finished findID_Dataset() - result: NOT FOUND"); //Y: " + dsY.similariry() + "
    }
  }

  /**
   * TODO: 
   *
   * @throws ParsingException TODO
   */
  private void parse() throws ParsingException {
    try {
      // check file, if exist, if xls ,...
      result = new ParsingResult(this.file);
    } catch (IOException e) {
      throw new ParsingException("unable to read file", e);
    }

    // find the ids - by finding ids it is possible to say it values are in x or in y axis
    findID_Dataset();

    // find if the different samples are in x or in y axis
    debug("found ids in " + this.result.ids.getAxis() + "  sheet nr: " + result.sheetnumber);

    // take each dataset and combine it in a dataset object
    findDatasets();

    // Debugging
    debug("number of datasets found: " + result.datasets.size());

    for (int i = 0; i < result.datasets.size(); i++) {
      for (int j = 0; j < result.get(i).data.length; j++) {
        String value = result.get(i).data[j];
        String key = "undefined";

        try {
          key = result.get(i).key[j];
        } catch (ArrayIndexOutOfBoundsException e) {
        }

        debug("dataset " + i + "/" + result.datasets.size() + "   [" + key + "] " + value);
      }
    }
  }
}
