/**
 * ============================================================================ File: Blank.java Package:
 * org.setupx.repository.core.communication.exporting.sampletypes cvs: $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06
 * 21:54:21 scholz Exp $ ============================================================================ Martin Scholz
 * Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/
 * ============================================================================
 */
package org.setupx.repository.core.communication.exporting.sampletypes;

import org.setupx.repository.core.communication.exporting.JobInformation;
import org.setupx.repository.core.communication.exporting.MappingError;
import org.setupx.repository.core.util.Util;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionUpdateException;

/**
 * 
 * 
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 * @hibernate.subclass
 */
public class Blank extends AcquisitionSample {
    // ~ Static fields/initializers
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private static final String NAME = "Blank";

    // ~ Constructors
    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new Blank object.
     */
    public Blank() {
        super();
    }

    /**
     * Creates a new Blank object.
     * 
     * @param information
     *                
     * @throws MappingError
     *                 
     */
    public Blank(JobInformation information) throws MappingError {
        super(information, NAME);
        this.positionImportance = 3;
        this.sequenceImportance = 19;
        this.typeShortcut = "bl";
    }

    // ~ Methods
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /*
     * (non-Javadoc)
     * 
     * @see org.setupx.repository.core.communication.exporting.sampletypes.AcquisitionSample#connectToSample()
     */
    public void connectToSample() throws PersistenceActionFindException, PersistenceActionUpdateException {
        debug(Util.getClassName(this) + " are not connected to any samples. - so will be ignored.");
    }
}
