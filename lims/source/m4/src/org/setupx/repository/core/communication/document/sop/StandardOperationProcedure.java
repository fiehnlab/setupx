package org.setupx.repository.core.communication.document.sop;

import java.io.File;
import java.util.Date;

import org.hibernate.Session;
import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.document.DocumentCategory;
import org.setupx.repository.core.communication.document.DocumentObject;
import org.setupx.repository.core.communication.document.DocumentObjectException;
import org.setupx.repository.core.user.UserDO;
import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.web.forms.FormObject;


/**
 * 
 * @author scholz
 * 
 * @hibernate.subclass
 */
public class StandardOperationProcedure extends DocumentObject {

    /**
     * @deprecated constructor only for persistence layer
     */
    public StandardOperationProcedure(){
        super();
    }
    
    public StandardOperationProcedure(String label, UserDO userDOdefine, UserDO userDOresponsible, DocumentCategory category) {
        
        this.setLabel(label);
        this.setDateIssued(new Date());
        
        this.setDateValidStart(NEVER);

        this.setDefinedBy(userDOdefine);
        this.setModificationsToLastVersion("--");
        this.setPubliclyAvailable(false);
        this.setReplcaesDocument(null);
        this.setResponsible(userDOresponsible);
        this.setVersion("0.0");

        this.setCategory(category);
    }
    
    
    
    
    public static void main(String[] args) throws PersistenceActionFindException {
        Session s = CoreObject.createSession();
        UserDO me = UserDO.load("mscholz@ucdavis.edu");
        UserDO boss = UserDO.load("asdfasdf");
        
        DocumentCategory category = new DocumentCategory("category1 ", "blafoo" , false, false);
        
        StandardOperationProcedure sop = new StandardOperationProcedure("my sop",me, boss, category);
        sop.setAreaValid("UC Davis");
        sop.setComment("test doc");
        sop.setContent("content");
        
        System.out.println(sop.toString());
        
        
        String url = "/Users/scholz/Desktop/npr6143.smil";
        try {
            DocumentObject.uploadDocument(new File(url), sop);
        } catch (DocumentObjectException e) {
            e.printStackTrace();
        }
        sop.save(s);

        Logger.log(null, "load " + StandardOperationProcedure.load(StandardOperationProcedure.createSession(), sop.getUOID()));
        
        Logger.log(null, "replaced by: " + StandardOperationProcedure.load(StandardOperationProcedure.createSession(),  sop.getUOID()).isReplacedBy());
        
    }
    
    public static StandardOperationProcedure load(Session session, final long uoid){
        return (StandardOperationProcedure) persistence_loadByID(StandardOperationProcedure.class, uoid);
    }

    /**
     * creates a Formobject that displays all relevant sops that are active and availble in the SX frontend
     * @return
     */
    public static FormObject createInputfield() {
        return StandardOperationProcedureFormObjectFactory.createInputfields();
    } 
}
