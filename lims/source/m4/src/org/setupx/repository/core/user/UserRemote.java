package org.setupx.repository.core.user;

import org.setupx.repository.core.communication.mail.EMailAddress;


/**
 * a remote user stored on a system that is NOT running on this machine
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.7 $
 */
public class UserRemote extends UserDO {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new UserRemote object.
   */
  public UserRemote() {
    super();
  }

  protected UserRemote(String username2, String password2, EMailAddress email) {
    super(username2, password2, email);
  }
}
