/**
 * ============================================================================ File:    LibraryNCBICheck.java Package: org.setupx.repository.core.communication.vocabulary cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.vocabulary;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.communication.ncbi.NCBIConnector;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.9 $
 */
class LibraryNCBICheck extends CoreObject implements VocabularyCheck {
  //~ Instance fields --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private NCBIConnector connector = null;

  {
    if (ACTIVE) {
      this.connector = new NCBIConnector();
    }
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @param word TODO
   *
   * @return TODO
   *
   * @throws VocabularyCheckException TODO
   */
  public String getSuggestion(String word) throws VocabularyCheckException {
    return connector.vocabularySuggestion(word);
  }

  /**
   * TODO: 
   *
   * @param word TODO
   *
   * @return TODO
   *
   * @throws VocabularyCheckException TODO
   */
  public boolean check(String word) throws VocabularyCheckException {
    return connector.vocabularyCheck(word);
  }
}
