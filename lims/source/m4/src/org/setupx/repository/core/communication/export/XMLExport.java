package org.setupx.repository.core.communication.export;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import org.setupx.repository.core.util.logging.Logger;
import org.setupx.repository.core.util.xml.DocumentConverter;
import org.setupx.repository.core.util.xml.XMLFile;
import org.setupx.repository.server.persistence.PersistenceActionFindException;
import org.setupx.repository.server.persistence.PersistenceActionStoreException;
import org.setupx.repository.web.forms.PromtCreateException;
import org.setupx.repository.web.forms.inputfield.multi.Promt;
import org.setupx.repository.web.forms.inputfield.multi.SampleScheduledException;

public class XMLExport extends Export {

    
    /**
     * creatin a String representaion of the {@link Document} this is inside this object.
     */
    public String toString() {
        try {
            return new XMLFile(this.toXML()).content();
        } catch (TransformerConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TransformerFactoryConfigurationError e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TransformerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (XMLConvertException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return super.toString();
    }

    
    /**
     * create an XML representaiton of this export.
     */
    private Document toXML() throws XMLConvertException {
        debug("creating XML representaion.");
        try {
            Document doc = DocumentConverter.createNewDocument();
            doc.appendChild(doc.createElement("export"));
            XMLFile xmlfile = null;
            
            // root element
            Element element = doc.getDocumentElement();
            debug("created root element: " + element.toString());

            // loop over all the different export parts and attach them as nodes to the existing root node
            debug("appending element childs for " + this.getExportParts().size());
            Iterator exportIterator = this.getExportParts().iterator();
            
            while (exportIterator.hasNext()) {
                ExportPart exportPart = (ExportPart) exportIterator.next();
                
                debug("appending childs.");
                element.appendChild(exportPart.toXML(doc));
            }
            
            // create xml file
            xmlfile = new XMLFile(doc);
            
            return xmlfile.getDocument();   
        } catch (Exception e) {
            throw new XMLConvertException("unable to convert to XML" , e);
        }
    }

    
    
    public static void testIt() throws PersistenceActionFindException, PersistenceActionStoreException, PromtCreateException, SampleScheduledException, XMLConvertException{
        
        long promtID = 327934;

        XMLExport export = new XMLExport();
        export.export(Promt.load(promtID));
                
        Logger.log(null, export.toString());
    }



}
