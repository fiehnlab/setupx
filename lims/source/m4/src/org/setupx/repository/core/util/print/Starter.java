package org.setupx.repository.core.util.print;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.setupx.repository.core.util.print.remote.DYMOLabelPrintConnector4SetupX;



public class Starter {
    
    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String value = null;

        try {
            System.out.println("   ");
            System.out.println("   ");
            System.out.println("   ");
            System.out.println("  Please enter the ID of the experiment that you need labels for. ");
            System.out.print("                      id:");
            value = br.readLine();
            DYMOLabelPrintConnector4SetupX.print(Long.parseLong(value));
        } catch (IOException ioe) { 
           System.out.println("IO error");
           System.exit(1);
        } catch (Exception exception){
            System.out.println("the ID is invalid.");
            System.exit(1);
        }
        
        //new DYMOLabelPrintConnector4XLS(new File("C:\\myFile.xls"),"my lab","martin");
    }

}
