package org.setupx.repository.core.communication.exporting;

/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.1 $
 */
public class MachineUnknownException extends ExportException {
  /**
   * Creates a new MachineUnknownException object.
   *
   * @param string 
   */
  public MachineUnknownException(String string) {
    super(string);
  }
}
