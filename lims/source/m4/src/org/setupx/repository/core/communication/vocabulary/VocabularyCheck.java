/**
 * ============================================================================ File:    VocabularyCheck.java Package: org.setupx.repository.core.communication.vocabulary cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.vocabulary;

/**
 * Checks if a word is known and if there is a suggestion what the correct spelling might be.
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.9 $
 */
public interface VocabularyCheck {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public static final boolean ACTIVE = true;

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * finds a suggestion for a word
   *
   * @param word the word that might be spelled wrong
   *
   * @return <b>suggestion</B> for the word how it might be written correctly<p>
   *
   * @throws VocabularyCheckException unable to find a suggestion for this word
   */
  public String getSuggestion(final String word) throws VocabularyCheckException;

  /**
   * Checks if a word is known - if the word is known return value is true if not false
   *
   * @param word the word that will be checked
   *
   * @return if the word is known return value is true if not false
   *
   * @throws VocabularyCheckException unable to check the word
   */
  boolean check(final String word) throws VocabularyCheckException;
}
