package org.setupx.repository.core.communication.technology.platform;

public class TechnologyFileTypeNotRegisteredException extends Exception {

    /**
     * 
     */
    public TechnologyFileTypeNotRegisteredException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param arg0
     * @param arg1
     */
    public TechnologyFileTypeNotRegisteredException(String arg0, Throwable arg1) {
        super(arg0, arg1);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param arg0
     */
    public TechnologyFileTypeNotRegisteredException(String arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param arg0
     */
    public TechnologyFileTypeNotRegisteredException(Throwable arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

}
