package org.setupx.repository.core.communication.importing;

public class MergerException extends Exception {

    public MergerException(String string) {
        super(string);
    }
}
