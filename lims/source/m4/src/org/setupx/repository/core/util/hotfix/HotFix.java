/**
 * ============================================================================ File:    HotFix.java Package: org.setupx.repository.core.util.hotfix cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.util.hotfix;

import org.setupx.repository.core.CoreObject;
import org.setupx.repository.core.util.hotfix.fsa.PotatoExporter;
import org.setupx.repository.core.util.hotfix.fsa.PotatoImporter;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class HotFix extends CoreObject {
  //~ Static fields/initializers ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  private static final int IMPORT_POTATOSAMPLES = 31;
  private static final int EXPORT_SUBSET_POTATOSAMPLES = 32;

  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new HotFix object.
   *
   * @param serviceCode 
   *
   * @throws HotFixException 
   */
  public HotFix(int serviceCode) throws HotFixException {
    try {
      switch (serviceCode) {
      case HotFix.IMPORT_POTATOSAMPLES:
        PotatoImporter.start();

        break;

      case HotFix.EXPORT_SUBSET_POTATOSAMPLES:
        PotatoExporter.start();

        break;

      default:
        throw new HotFixException("unknown serviceCode - @GERT: ASK ME ;-) ");
      }
    } catch (HotFixException e) {
      throw e;
    } catch (Exception e) {
      throw new HotFixException(e);
    }
  }

  private HotFix() {
    super();
  }
}
