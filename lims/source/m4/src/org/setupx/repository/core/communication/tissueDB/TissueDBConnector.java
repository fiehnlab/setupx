/**
 * ============================================================================ File:    TissueDBConnector.java Package: org.setupx.repository.core.communication.tissueDB cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.tissueDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision: 1.8 $
 * @deprecated
 */
public class TissueDBConnector {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   *
   */
  public TissueDBConnector() {
    super();
    // TODO Auto-generated constructor stub
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   *
   * @throws SQLException TODO
   * @throws RuntimeException TODO
   */
  public Connection fetchConnection() throws SQLException {
    try {
      Class.forName("org.postgresql.Driver");
    } catch (Exception e) {
      e.printStackTrace(System.err);
      throw new RuntimeException(e.toString());
    }

    return DriverManager.getConnection("jdbc:postgresql://javasrv/test", "postgres", "postgres");
  }

  /**
   * TODO: 
   *
   * @param args TODO
   */
  public static void main(String[] args) {
    TissueDBConnector connector = new TissueDBConnector();
  }
}
