/**
 * ============================================================================ File:    ExperimentSubmittedNotification.java Package: org.setupx.repository.core.communication.notification cvs:     $Revision: 1.4 $ $Id: scholz.xml,v 1.4 2005/07/06 21:54:21 scholz Exp $
 * ============================================================================ Martin Scholz  Copyright (C) 2005 Martin Scholz. All rights reserved. For more information please see http://fiehnlab.ucdavis.edu/ ============================================================================
 */
package org.setupx.repository.core.communication.notification;

import org.setupx.repository.Config;
import org.setupx.repository.web.forms.inputfield.multi.Promt;


/**
 * 
 *
 * @author <a href=mailto:scholz@zeypher.com>&nbsp;Martin Scholz&nbsp;</a>
 * @version $Revision$
 */
public class ExperimentSubmittedNotification extends Notification {
  //~ Constructors -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Creates a new ExperimentSubmittedNotification object.
   *
   * @param promt 
   */
  public ExperimentSubmittedNotification(Promt promt) {
    this.setMessage("Experiment " + promt.getUOID() + " has been submitted.");
  }

  //~ Methods ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * TODO: 
   *
   * @return TODO
   */
  public String[] getRecipients() {
    return new String[] { Config.OPERATOR_ADMIN.getEmailAddress() };
  }
}
