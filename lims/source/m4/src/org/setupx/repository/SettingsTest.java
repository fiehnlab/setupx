package org.setupx.repository;

import junit.framework.TestCase;


public class SettingsTest extends TestCase{
    
    public SettingsTest() {
        // TODO Auto-generated constructor stub
    }

    

    public void testRefresh() throws SettingsUnavailableException {
        Settings.refresh();
    }

    public void testKeys() {
        assertTrue(Settings.keys().hasMoreElements());
    }

    public void testGetFile() {
        assertTrue(Settings.getFile().exists());
    }
}
