package com.zeypher.tools.code;

import java.io.File;

public class Link {

    private File target;
    private File sourceFile;

    public Link(File target, File file) {
        this.sourceFile = file;
        this.target = target;
    }

    public File getTarget() {
        return this.target;
    }

}
