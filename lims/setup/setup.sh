
/etc/init.d/mysql start

export ANT_OPTS=-Xmx2000m

## update the taxonomy
cd ../data/taxonomy
## deactivated
##./refresh.sh
cd ../../setup

## init the system
ant -buildfile ../source/m4/build.xml -Dbuild=../source/m4export 

## init the users inside the system
ant -buildfile ../source/m4/build.xml -Dbuild=../source/m4export run_hibernate

## create views on the database - needs to be done last - cause actual tables are present already
ant db_view
