cd /lims
rm source/m4/testresult -r
rm source/m4/apidoc -r
rm source/m4/build -r
rm source/m4/dist -r
filename=setupX-`date +"%Y%m%d"`-`hostname`
echo $filename;
tar -cvf /mnt/storage/backup/setupx/$filename.tar .